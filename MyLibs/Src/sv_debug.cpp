/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <Dbghelp.h>
#include <Tlhelp32.h>
#include <Psapi.h>
#include <algorithm>

#include "sv_debug.h"
#include "sv_callstack.h"
#include "sv_strutils.h"
#include "sv_ver.h"
#include "sv_exceptions.h"
#include "sv_toolhelp.h"

#define MAX_PDB_STR_LENGTH			32

namespace SVDebug
{

/************************************************************************/
/* ��������������� �������                                              */
/************************************************************************/

__declspec(naked) PVOID __cdecl GetEIP()
{
	_asm mov eax, [esp]
	_asm ret
}

__declspec(naked) PVOID __cdecl GetESP()
{
	_asm lea eax, [esp+4]
	_asm ret
}

__declspec(naked) PVOID __cdecl GetEBP()
{
	_asm mov eax, ebp
	_asm ret
}

PNT_TIB	NtCurrentTIB()
{
	typedef VOID* (__stdcall *FnNtCurrentTeb)(void);
	static FnNtCurrentTeb fnNtCurrentTeb = 
		(FnNtCurrentTeb)GetProcAddress(::GetModuleHandleW(L"NTDLL.DLL"), "NtCurrentTeb");
	return (PNT_TIB)(fnNtCurrentTeb)();
};

void SetThreadName(LPCSTR pszThreadName, DWORD dwThreadID)
{
	class THREADNAME_INFO
	{
		DWORD		dwType;			// must be 0x1000
		LPCSTR		psName;			// pointer to name (in same addr space)
		DWORD		dwThreadID;		// thread ID (-1 caller thread)
		DWORD		dwFlags;		// reserved for future use, most be zero
	public:
		THREADNAME_INFO(PCSTR psName, DWORD dwThreadID)
			: dwType(0x1000),
			psName(psName),
			dwThreadID(dwThreadID),
			dwFlags(0)
		{}
	};

	THREADNAME_INFO	info(pszThreadName, dwThreadID);

	__try
	{
		::RaiseException(
			0x406d1388,	//MS_VC_EXCEPTION
			0,
			sizeof(info) / sizeof(DWORD),
			(DWORD *)&info);
	}
	__except (EXCEPTION_CONTINUE_EXECUTION)
	{
	}
}

std::wstring WriteMiniCallStack(PCONTEXT pContext, DWORD dwMaxDeep)
{
	std::wstring sCallStack;
	LPDWORD EBP = (LPDWORD)pContext->Ebp;

	try
	{
		for (DWORD i = 0; i < dwMaxDeep; i++)
		{
			if (::IsBadReadPtr(EBP + 1, 4) ||
				::IsBadReadPtr((LPVOID)*(EBP + 1), 4))
			{
				break;
			}

			sCallStack += format_wstring(L"0x%p\n", *(EBP + 1));

			if (::IsBadReadPtr(EBP, 4))
			{	
				break;
			}
			EBP = (LPDWORD)*EBP;
		}
	}
	catch (...)
	{
	}

	return sCallStack;
}

wstring WriteMiniCallStackRemote(HANDLE hProcess, 
								 HANDLE hThread, 
								 DWORD dwMaxDeep)
{
	CONTEXT ctx = {0};
	PCONTEXT pContext = &ctx;
	std::wstring sCallStack;

	::SuspendThread(hThread);

	try
	{
		ctx.ContextFlags = CONTEXT_CONTROL;
		::GetThreadContext(hThread, &ctx);
		LPDWORD EBP = (LPDWORD)pContext->Ebp;
		for (DWORD i = 0; i < dwMaxDeep; i++)
		{
			DWORD dwRead = 0;

/*			if (::IsBadReadPtr(EBP + 1, 4) ||
				::IsBadReadPtr((LPVOID)*(EBP + 1), 4))
			{
				break;
			}*/

			DWORD EBP1 = 0;
			::ReadProcessMemory(hProcess, EBP + 1, &EBP1, sizeof(DWORD), &dwRead);
			if (dwRead != sizeof(DWORD))
			{
				break;
			}

			sCallStack += format_wstring(L"0x%p\r\n", EBP1);

/*			if (::IsBadReadPtr(EBP, 4))
			{	
				break;
			}
			
			EBP = (LPDWORD)*EBP;*/
			dwRead = 0;
			::ReadProcessMemory(hProcess, EBP, &EBP, sizeof(DWORD), &dwRead);
			if (dwRead != sizeof(*EBP))
			{
				break;
			}
		}
	}
	catch (...)
	{
	}

	::ResumeThread(hThread);

	return sCallStack;
}

std::wstring WriteThreadCallStack(PCONTEXT pContext, 
							 DWORD dwFlags,
							 DWORD dwThreadId,
							 DWORD dwProcessId)
{
	std::wstring s;

	try
	{
		if ((dwFlags & CALLSTACK_FLAG_TIMES) != 0)
		{
			s += L"---- Thread times ----\n";

			HANDLE hThread = ::OpenThread(THREAD_QUERY_INFORMATION, FALSE, dwThreadId);
			if (!hThread)
			{
				s += format_wstring(L"Cannot open thread (0x%p)\n", ::GetLastError());
			}
			else
			{
				FILETIME ftCreate = {0};
				FILETIME ftExit = {0};
				FILETIME ftKernel = {0};
				FILETIME ftUser = {0};
				
				if (!::GetThreadTimes(hThread, &ftCreate, &ftExit, &ftKernel, &ftUser))
				{
					s += format_wstring(L"Cannot get thread times (0x%p)\n", ::GetLastError());
				}
				else
				{
					SYSTEMTIME st = {0};

					::FileTimeToSystemTime(&ftCreate, &st);
					ULONGLONG ullUser = ((PULARGE_INTEGER)&ftUser)->QuadPart / 10;
					ULONGLONG ullKernel = ((PULARGE_INTEGER)&ftKernel)->QuadPart / 10;

					s += format_wstring(
						L"Creation time: %02d:%02d:%02d.%03d %02d/%02d/%04d\n"
						L"User time:     %I64i usec\n"
						L"Kernel time:   %I64i usec\n",
						st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, st.wDay, st.wMonth, st.wYear,
						ullUser, ullKernel);
				}		
			}
		}

		if ((dwFlags & CALLSTACK_FLAG_REGS) != 0)
		{
			s += L"---- Registers ----\n";
			s += DumpBasicRegisters(pContext);
		}

		if ((dwFlags & CALLSTACK_FLAG_TIB) != 0)
		{
			s += L"---- Thread info block ----\n";

			PNT_TIB pTIB = NtCurrentTIB();

			s += format_wstring(
				L"StackBase:    0x%p\n"
				L"StackLimit:   0x%p\n"
				L"SubSystemTib: 0x%p\n",
				pTIB->StackBase,
				pTIB->StackLimit,
				pTIB->SubSystemTib
			);
		}

		if (dwFlags)
		{
			s += L"---- Callstack ----\n";
		}

		SVCallStack::CStackWalker stk;
		stk.Init(pContext, dwThreadId, dwProcessId);
		stk.CallStackSnapshot((dwFlags & CALLSTACK_FLAG_VARS) != 0);
		s += stk.GetCallStackInfo();
	}
	catch (...)
	{
	}

	return s;
}

std::wstring WriteProcessSnapshot(DWORD dwPID, DWORD dwFlags)
{
	SVToolHelp32::CThreadList thList;
	THREADENTRY32 te;
	std::wstring s;

	te.dwSize = sizeof(te);
	if (thList.First(te, 0))
	{
		do 
		{
			if (te.th32OwnerProcessID != dwPID || te.th32ThreadID == ::GetCurrentThreadId())
			{
				continue;
			}

			s += format_wstring(L"==== Thread %d (%04X) ====\n", te.th32ThreadID, te.th32ThreadID);

			try
			{
				CONTEXT	thContext = {0};
				HANDLE	hThread = ::OpenThread(
					THREAD_SUSPEND_RESUME | THREAD_GET_CONTEXT | THREAD_QUERY_INFORMATION, 
					FALSE, te.th32ThreadID);

				thContext.ContextFlags = 0xFFFFFFFF;

				if (!hThread)
				{
					s += format_wstring(L"Cannot open thread (0x%p)\n", ::GetLastError());
					continue;
				}

				if (::SuspendThread(hThread) == 0xFFFFFFFF)
				{
					s += format_wstring(L"Cannot suspend thread (0x%p)\n", ::GetLastError());
					continue;
				}

				if (!::GetThreadContext(hThread, &thContext))
				{
					s += format_wstring(L"Cannot get thread context (0x%p)\n", ::GetLastError());
					continue;
				}

				s += WriteThreadCallStack(&thContext, dwFlags, te.th32ThreadID, dwPID);

				::ResumeThread(hThread);
			}
			catch (...)
			{
				s += L"Exception during writing thread callstack\n";
			}
			s += L"\n";
		} 
		while (thList.Next(te));
	}

	return s;
}

std::wstring WriteProcessSnapshot2(HANDLE hProcess, DWORD dwFlags)
{
	// ��� ��� ���� ������ ������������� ������� GetProcessId. 
	// �.�. ��� �������� ������ � Windows XP SP1 � Windows Server 2003, 
	// �� ���� ������� �� ����� ������, �������� ��� � ���������� ������ ������

	typedef DWORD (WINAPI* GETPROCESSID)(HANDLE);
	GETPROCESSID pGetProcessId = (GETPROCESSID)GetProcAddress(
		GetModuleHandleW(L"kernel32.dll"), "GetProcessId");

	if (!pGetProcessId)
	{
		return L"";
	}

	return WriteProcessSnapshot(pGetProcessId(hProcess), dwFlags);
}

std::wstring WriteAccessViolationInfo(PEXCEPTION_POINTERS pExc)
{
	try
	{
		if (pExc->ExceptionRecord->ExceptionCode == EXCEPTION_ACCESS_VIOLATION)
		{
			return format_wstring(
				L"The memory could not be %s at 0x%p", 
				pExc->ExceptionRecord->ExceptionInformation[0] ? L"written" : L"read", 
				pExc->ExceptionRecord->ExceptionInformation[1]);
		}
	}
	catch (...)
	{
	}
	return L"";
}

std::wstring WriteFullExceptionInfo(PEXCEPTION_POINTERS pExc,
							   DWORD dwFlags,
							   DWORD dwThreadId,
							   DWORD dwProcessId)
{
	SYSTEMTIME st = {0};

	try
	{
		::GetLocalTime(&st);
		return format_wstring(
			L"==== Exception in thread %d (%04X) ====\n"
			L"+Code:            0x%p\n"
			L"+Name:            %s\n"
			L"+Address:         0x%p\n"
			L"+Date:            %02d/%02d/%04d\n"
			L"+Time:            %02d:%02d:%02d.%03d\n"
			L"+Additional info: %s\n"
			L"%s",
			dwThreadId, dwThreadId,
			pExc->ExceptionRecord->ExceptionCode,
			SVExceptions::GetExceptionName(pExc->ExceptionRecord->ExceptionCode),
			pExc->ExceptionRecord->ExceptionAddress,
			st.wDay, st.wMonth, st.wYear,
			st.wHour, st.wMinute, st.wSecond, st.wMilliseconds,
			(pExc->ExceptionRecord->ExceptionCode == EXCEPTION_ACCESS_VIOLATION) ? 
				WriteAccessViolationInfo(pExc).c_str() : L"<none>",
			WriteThreadCallStack(
				pExc->ContextRecord, 
				dwFlags | CALLSTACK_FLAG_REGS | CALLSTACK_FLAG_TIB, 
				dwThreadId, 
				dwProcessId).c_str()
		);
	}
	catch (...)
	{
	}

	return L"";
}

std::wstring WriteModuleList(DWORD dwPID, bool bDetailed)
{
	std::wstring sModList;

	struct ModuleSorter
	{
		static bool Less(const MODULEENTRY32W& m1, const MODULEENTRY32W& m2)
		{
			return m1.modBaseAddr < m2.modBaseAddr;
		}
	};

	try
	{
		SVToolHelp32::CModuleList::LIST modList = SVToolHelp32::CModuleList::GetList(dwPID);

		modList.sort(ModuleSorter::Less);

		SVToolHelp32::CModuleList::LIST::const_iterator it = modList.begin();
		for (; it != modList.end(); ++it)
		{
			sModList += format_wstring(L"0x%p %s\n", it->modBaseAddr, it->szExePath);

			if (bDetailed)
			{
				SVVer::CModuleVersion mv(it->szExePath);
				try
				{
					sModList += format_wstring(
						L"\tLocation: %s\n"	
						L"\tBaseAddress: %08X\n"
						L"\tVersion: %s\n"	
						L"\tCompany: %s\n"
						L"\tProduct: %s\n"
						L"\tFileDescription: %s\n",	
						it->szExePath,
						it->modBaseAddr,				
						mv.GetDotVersion(),				
						mv.GetCompany(),				
						mv.GetProduct(),				
						mv.GetFileDescr()
					);
				}
				catch (...)
				{
				}
			}
		}
	}
	catch (...)
	{
	}
	
	return sModList;
}

std::wstring WriteUptimes(DWORD dwPID)
{
	try
	{
		HANDLE hProcess = ::OpenProcess(
			PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 
			FALSE, 
			dwPID);

		if (!hProcess)
		{
			return L"";
		}

		FILETIME	ftCreationTime = {0};
		SYSTEMTIME	stCreationTime = {0};
		FILETIME	ftExitTime = {0};
		ULONGLONG	nKernelTime = {0};
		ULONGLONG	nUserTime = {0};

		::GetProcessTimes(
			hProcess,
			&ftCreationTime,
			&ftExitTime,
			(LPFILETIME)&nKernelTime,
			(LPFILETIME)&nUserTime);

		::CloseHandle(hProcess);

		::FileTimeToLocalFileTime(&ftCreationTime, &ftCreationTime);
		::FileTimeToSystemTime(&ftCreationTime, &stCreationTime);
		nKernelTime /= 10000000;
		nUserTime /= 10000000;

		SYSTEMTIME stProcessUptime;
		FILETIME ftProcessUptime;
		::GetLocalTime(&stProcessUptime);
		::SystemTimeToFileTime(&stProcessUptime, &ftProcessUptime);
		ULONGLONG nProcessUptime = 
			(((PULARGE_INTEGER)&ftProcessUptime)->QuadPart - 
			((PULARGE_INTEGER)&ftCreationTime)->QuadPart) / 10000000;

		ULONGLONG nSystemUptime, nFreq;
		::QueryPerformanceCounter((PLARGE_INTEGER)&nSystemUptime);
		::QueryPerformanceFrequency((PLARGE_INTEGER)&nFreq);
		nSystemUptime /= nFreq;

		return format_wstring(
			L"Creation time:    %02d.%02d.%04d %02d:%02d:%02d\n"
			L"Kernel time:      %d d %d h %d m %d s\n"
			L"User time:        %d d %d h %d m %d s\n"
			L"Process uptime:   %d day(s) %d hour(s) %d minute(s) %d second(s)\n"
			L"System uptime:    %d day(s) %d hour(s) %d minute(s) %d second(s)\n",
			stCreationTime.wDay, stCreationTime.wMonth, stCreationTime.wYear, stCreationTime.wHour, stCreationTime.wMinute, stCreationTime.wSecond,
			int(nKernelTime / 86400), int((nKernelTime % 86400) / 3600), int((nKernelTime % 3600) / 60), int(nKernelTime % 60),
			int(nUserTime / 86400), int((nUserTime % 86400) / 3600), int((nUserTime % 3600) / 60), int(nUserTime % 60),
			int(nProcessUptime / 86400), int((nProcessUptime % 86400) / 3600), int((nProcessUptime % 3600) / 60), int(nProcessUptime % 60),
			int(nSystemUptime / 86400), int((nSystemUptime % 86400) / 3600), int((nSystemUptime % 3600) / 60), int(nSystemUptime % 60)
		);
	}
	catch (...)
	{
	}

	return L"";
}

std::wstring WriteWinVer()
{
	SVVer::CWinVer ver;

	if (ver.IsValid())
	{
		return format_wstring(
			L"Windows version: %s (%s %s)", 
			ver.GetDotVersion(), 
			ver.GetVersionStr(), 
			ver.GetSuite());
	}

	return L"";
}

std::wstring DumpBasicRegisters(PCONTEXT pContext)
{
	try
	{
		return format_wstring(
			L"EAX=%08X EBX=%08X ECX=%08X EDX=%08X\n"
			L"ESI=%08X EDI=%08X ESP=%08X EBP=%08X\n"
			L"CS=%04X DS=%04X\n"
			L"SS=%04X ES=%04X\n"
			L"FS=%04X GS=%04X\n"
			L"EIP=%08X\n"
			L"EFLAGS=%08X\n",
			pContext->Eax, pContext->Ebx, pContext->Ecx, pContext->Edx,
			pContext->Esi, pContext->Edi, pContext->Esp, pContext->Ebp,
			pContext->SegCs, pContext->SegDs, 
			pContext->SegSs, pContext->SegEs, 
			pContext->SegFs, pContext->SegGs,
			pContext->Eip, 
			pContext->EFlags
			);
	}
	catch (...)
	{
	}

	return L"";
}

wstring DumpProcessException(PEXCEPTION_POINTERS pExc,
							 DWORD dwFlags,
							 DWORD dwTID, 
							 DWORD dwPID, 
							 bool bRunInThread)
{
	struct ThreadRunner
	{
		struct THREADPARAMS
		{
			PEXCEPTION_POINTERS pExc;
			DWORD dwPID; 
			DWORD dwTID;
			DWORD dwFlags;
			wstring sDump;
		};

		THREADPARAMS Params;
		
		static void Work(THREADPARAMS* pParams)
		{
			HANDLE hProcess = ::OpenProcess(
				PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 
				FALSE, 
				pParams->dwPID);

			if (hProcess == NULL)
			{
				return;
			}

			PROCESS_MEMORY_COUNTERS pmc = {0};
			::GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc));

			::CloseHandle(hProcess);
			hProcess = NULL;

			PROCESSENTRY32W pe = {0};
			pe.dwSize = sizeof(pe);
			HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

			if (hSnapshot != INVALID_HANDLE_VALUE)
			{
				if (::Process32FirstW(hSnapshot, &pe))
				{
					do
					{
						if (pe.th32ProcessID == pParams->dwPID)
						{
							break;
						}
					}
					while (::Process32NextW(hSnapshot, &pe));
				}
				::CloseHandle(hSnapshot);
			}

			SVVer::CWinVer wv;

			pParams->sDump = format_wstring(
				L"==== Process information ====\n"
				L"Image name:       %s\n"
				L"PID:              %d (%04X)\n"
				L"Working set size: %d Kb\n"
				L"Pagefile usage:   %d Kb\n"
				L"==== Up times ====\n"
				L"%s\n"
				L"==== OS version ====\n"
				L"%s\n",
				pe.szExeFile,
				pParams->dwPID, pParams->dwPID,
				pmc.WorkingSetSize / 1024,
				pmc.PagefileUsage / 1024,
				WriteUptimes(pParams->dwPID).c_str(),
				WriteWinVer().c_str());

			pParams->sDump += format_wstring(L"==== Exception information ====\n");
			pParams->sDump += WriteFullExceptionInfo(
				pParams->pExc, 
				pParams->dwFlags,
				pParams->dwTID, 
				pParams->dwPID);

			pParams->sDump += format_wstring(L"==== Process snapshot ====\n");
			pParams->sDump += WriteProcessSnapshot(pParams->dwPID, pParams->dwFlags);

			pParams->sDump += L"==== Module list ====\n";
			pParams->sDump += WriteModuleList(pParams->dwPID, true);

			::CloseHandle(hProcess);
		}
		
		static DWORD WINAPI ThreadProc(THREADPARAMS* pParams)
		{
			try
			{
				Work(pParams);
			}
			catch (...)
			{
			}
			return 0;
		}
		
		void Run()
		{
			__try
			{
				DWORD dwTID = 0;
				HANDLE hThread = ::CreateThread(
					NULL, 
					0, 
					(LPTHREAD_START_ROUTINE)ThreadProc, 
					&Params, 
					0, 
					&dwTID);

				if (::WaitForSingleObject(hThread, 10000) != WAIT_OBJECT_0)
				{
					::TerminateThread(hThread, (DWORD)-1);
				}

				::CloseHandle(hThread);
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
			}
		}
	};

	ThreadRunner tr;

	tr.Params.pExc		= pExc;
	tr.Params.dwPID		= dwPID;
	tr.Params.dwTID		= dwTID;
	tr.Params.dwFlags	= dwFlags;

	if (bRunInThread)
	{
		tr.Run();
	}
	else
	{
		tr.Work(&tr.Params);
	}

	return tr.Params.sDump;
}

void InstantContext(CONTEXT* pContext, DWORD dwSkipLevels)
{
	DWORD dwEIP = 0, dwESP = 0, dwEBP = 0, dwEFlags = 0;

	__asm
	{
		mov		dwEBP, ebp
	}

	for (DWORD i = 0; i < dwSkipLevels + 1; i++)
	{
		__try
		{
			__asm
			{
				pusha
				mov		ebx, dwEBP
				mov		eax, dword ptr [ebx + 0]
				mov		dwEBP, eax
				mov		eax, dword ptr [ebx + 4]
				mov		dwEIP, eax
				mov		eax, ebx
				sub		eax, 8
				mov		dwESP, eax
				pushfd
				pop		dwEFlags
				popa
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			break;
		}
	}

	pContext->ContextFlags = CONTEXT_CONTROL;
	pContext->Eip		= dwEIP;
	pContext->Esp		= dwESP;
	pContext->Ebp		= dwEBP;
	pContext->EFlags	= dwEFlags;
}

wstring InstantCallstack(DWORD dwFlags, DWORD dwSkipLevels)
{
	CONTEXT ctx = {0};
	InstantContext(&ctx, dwSkipLevels);
	return WriteThreadCallStack(&ctx, dwFlags);
}

}
