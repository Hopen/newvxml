/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <Dbghelp.h>
#include <Psapi.h>
#include <malloc.h>
#include <memory>

#include "sv_syminfo.h"
#include "sv_strutils.h"

namespace SVSymInfo
{

/************************************************************************/
/* ���������� ������ CSymInfo                                           */
/************************************************************************/

bool CSymInfo::Init(HANDLE hProcess, PVOID pAddr)
{
	char szBuf[MAX_PATH] = {0};
	PIMAGEHLP_SYMBOL pSym = (PIMAGEHLP_SYMBOL)szBuf;

	try
	{
		pSym->SizeOfStruct	= sizeof(IMAGEHLP_SYMBOL);
		pSym->MaxNameLength	= sizeof(szBuf) - sizeof(IMAGEHLP_SYMBOL);

		while (!::SymGetSymFromAddr(hProcess, (DWORD)pAddr, &dwOffs, pSym))
		{
			if (::GetLastError() == ERROR_MOD_NOT_FOUND &&
				SymLoadModuleByAddr(pAddr, hProcess))
			{
				continue;
			}
			return false;
		}

		sName = StrA2wstring(pSym->Name);
		this->dwSize = pSym->Size;
	}
	catch (...)
	{
	}

	return true;
}

/************************************************************************/
/* ���������� ������ CLineInfo                                          */
/************************************************************************/

bool CLineInfo::Init(HANDLE hProcess, PVOID pAddr)
{
	char szBuf[MAX_PATH] = {0};
	PIMAGEHLP_LINE pLine = (PIMAGEHLP_LINE)szBuf;

	try
	{
		pLine->SizeOfStruct	= sizeof(IMAGEHLP_LINE);

		while (!::SymGetLineFromAddr(hProcess, (DWORD)pAddr, &dwOffs, pLine))
		{
			if (::GetLastError() == ERROR_MOD_NOT_FOUND && 
				SymLoadModuleByAddr(pAddr, hProcess))
			{
				continue;
			}
			return false;
		}

		sFileName = StrA2wstring(pLine->FileName);
		this->dwLine = pLine->LineNumber;
	}
	catch (...)
	{
	}

	return true;
}

/************************************************************************/
/* ���������� ������ CModuleInfo                                        */
/************************************************************************/

bool CModuleInfo::Init(HANDLE hProcess, PVOID pAddr)
{
	IMAGEHLP_MODULEW im = { sizeof(im) };

	try
	{
		while (!::SymGetModuleInfoW(hProcess, (DWORD)pAddr, &im))
		{
			if (::GetLastError() == ERROR_MOD_NOT_FOUND &&
				SymLoadModuleByAddr(pAddr, hProcess))
			{
				continue;
			}
			return false;
		}

		WCHAR szBuf[MAX_PATH] = {0};
		::GetModuleFileNameW((HMODULE)dwImageBase, szBuf, sizeof(szBuf) / sizeof(WCHAR));

		dwImageBase	= im.BaseOfImage;
		dwImageSize	= im.ImageSize;
		sModuleName	= im.ModuleName;
		sImageName	= im.ImageName;
		sFileName	= szBuf;
	}
	catch (...)
	{
	}

	return true;
}

PCIMAGE_SECTION_HEADER CModuleInfo::GetSection(int nSection) const
{
	if (!dwImageBase)
	{
		return NULL;
	}

	PIMAGE_DOS_HEADER pDosHdr	= (PIMAGE_DOS_HEADER)dwImageBase;
	PIMAGE_NT_HEADERS pNtHdr	= (PIMAGE_NT_HEADERS)(dwImageBase + pDosHdr->e_lfanew);

	return ((pNtHdr->FileHeader.NumberOfSections <= nSection) ? 
		NULL : IMAGE_FIRST_SECTION(pNtHdr) + nSection);
}

int	CModuleInfo::GetSectionCount() const
{
	if (!dwImageBase)
	{
		return NULL;
	}

	PIMAGE_DOS_HEADER pDosHdr	= (PIMAGE_DOS_HEADER)dwImageBase;
	PIMAGE_NT_HEADERS pNtHdr	= (PIMAGE_NT_HEADERS)(dwImageBase + pDosHdr->e_lfanew);

	return pNtHdr->FileHeader.NumberOfSections;
}

const IMAGE_SECTION_HEADER* CModuleInfo::GetSectionByAddress(PVOID pAddr) const
{
	if (!dwImageBase)
	{
		return NULL;
	}

	try
	{
		if (((DWORD)pAddr < dwImageBase) || 
			((DWORD)pAddr >= dwImageBase + dwImageSize))
		{
			return NULL;
		}

		DWORD dwRVA = (DWORD)pAddr - dwImageBase;
		int i = 0, k = GetSectionCount();

		for (; i < k; i++)
		{
			PCIMAGE_SECTION_HEADER pSection = GetSection(i);
			DWORD dwSectionStart = pSection->VirtualAddress;
			DWORD dwSectionEnd = dwSectionStart + max(pSection->SizeOfRawData, pSection->Misc.VirtualSize);

			// Is the address in this section???
			if ((dwRVA >= dwSectionStart) && (dwRVA <= dwSectionEnd))
			{
				return pSection;
			}
		}
	}
	catch (...)
	{
	}

	return NULL;
}

/************************************************************************/
/* ��������������� �������                                              */
/************************************************************************/

DWORD SymLoadModuleByAddr(PVOID pAddr, HANDLE hProcess)
{
	MODULEENTRY32W me = GetModuleInfoByAddress(pAddr, hProcess);

	if (!me.dwSize)
	{
		return false;
	}

	return ::SymLoadModule(
		hProcess,
		NULL,
		STRW2A(me.szExePath),
		STRW2A(me.szModule),
		(DWORD)me.modBaseAddr,
		me.modBaseSize);
}

MODULEENTRY32W GetModuleInfoByAddress(PVOID pAddr, HANDLE hProcess)
{
	MODULEENTRY32W me = {0};
	DWORD cbNeeded = 0;

	try
	{
		::EnumProcessModules(hProcess, NULL, 0, &cbNeeded);

		DWORD dwModCount = cbNeeded / sizeof(HMODULE);
		std::auto_ptr<HMODULE> modules(new HMODULE[dwModCount]);
		HMODULE* pModules = modules.get();

		if (::EnumProcessModules(
			hProcess, 
			pModules, 
			cbNeeded, 
			&cbNeeded))
		{
			for (DWORD i = 0; i < dwModCount; i++)
			{
				MODULEINFO mi;

				if (!::GetModuleInformation(hProcess, pModules[i], &mi, sizeof(mi)))
				{
					continue;
				}

				if ((pAddr >= mi.lpBaseOfDll) && 
					((LPBYTE)pAddr < (LPBYTE)mi.lpBaseOfDll + mi.SizeOfImage))
				{
					me.dwSize = sizeof(me);
					me.modBaseAddr = (BYTE*)mi.lpBaseOfDll;
					me.modBaseSize = mi.SizeOfImage;
					me.hModule = pModules[i];

					::GetModuleFileNameExW(
						hProcess, 
						me.hModule, 
						me.szExePath, 
						sizeof(me.szExePath));

					LPCWSTR p = me.szExePath + wcslen(me.szExePath);
					while (*p != '\\' && p > me.szExePath) p--;
					if (*p == '\\') p++;

					wcscpy(me.szModule, p);

					break;
				}
			}
		}
	}
	catch (...)
	{
	}

	return me;
}

}
