/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "Common.h"
#include <windows.h>
#include <stdio.h>
#include <oleauto.h>
#include "sv_variant.h"
#include "sv_strutils.h"


// SVVariant namespace

__int64 SVVariant::VariantToInt(const VARIANT& v)
{
	switch (v.vt)
	{
		// By val
		case VT_I1: return v.cVal;
		case VT_I2: return v.iVal;
		case VT_I4: return v.lVal;
		case VT_I8: return v.llVal;
		case VT_INT: return v.intVal;
		case VT_UI1: return v.bVal;
		case VT_UI2: return v.uiVal;
		case VT_UI4: return v.ulVal;
		case VT_UI8: return v.ullVal;
		case VT_UINT: return v.uintVal;
		case VT_R4: return v.fltVal;
		case VT_R8: return v.dblVal;
		case VT_BOOL: return (v.boolVal == VARIANT_TRUE) ? 1 : 0;
		case VT_DECIMAL:
			{
				long retval; 
				VarI4FromDec((DECIMAL*)&v.decVal, &retval);
				return retval;
			}
		case VT_BSTR: return _wtoi(v.bstrVal);
		case VT_DATE: return v.date;
		case VT_CY: return v.cyVal.Hi;

		// By ref
		case VT_I1 | VT_BYREF: return *v.pcVal;
		case VT_I2 | VT_BYREF: return *v.piVal;
		case VT_I4 | VT_BYREF: return *v.plVal;
		case VT_I8 | VT_BYREF: return *v.pllVal;
		case VT_INT | VT_BYREF: return *v.pintVal;
		case VT_UI1 | VT_BYREF: return *v.pbVal;
		case VT_UI2 | VT_BYREF: return *v.puiVal;
		case VT_UI4 | VT_BYREF: return *v.pulVal;
		case VT_UI8 | VT_BYREF: return *v.pullVal;
		case VT_UINT | VT_BYREF: return *v.puintVal;
		case VT_R4 | VT_BYREF: return *v.pfltVal;
		case VT_R8 | VT_BYREF: return *v.pdblVal;
		case VT_BOOL | VT_BYREF: return (*v.pboolVal == VARIANT_TRUE) ? 1 : 0;
		case VT_DECIMAL | VT_BYREF:
			{
				long retval; 
				VarI4FromDec((DECIMAL*)v.pdecVal, &retval);
				return retval;
			}
		case VT_BSTR | VT_BYREF: return _wtoi(*v.pbstrVal);
		case VT_DATE | VT_BYREF: return *v.pdate;
		case VT_CY | VT_BYREF: return v.pcyVal->Hi;

		case VT_VARIANT | VT_BYREF:
			return VariantToInt(*v.pvarVal);

		default:
			throw 0;
	}
}

double SVVariant::VariantToFloat(const VARIANT& v)
{
	switch (v.vt)
	{
		// By val
		case VT_I1: return v.cVal;
		case VT_I2: return v.iVal;
		case VT_I4: return v.lVal;
		case VT_I8: return v.llVal;
		case VT_INT: return v.intVal;
		case VT_UI1: return v.bVal;
		case VT_UI2: return v.uiVal;
		case VT_UI4: return v.ulVal;
		case VT_UI8: return (__int64)v.ullVal;
		case VT_UINT: return v.uintVal;
		case VT_R4: return v.fltVal;
		case VT_R8: return v.dblVal;
		case VT_BOOL: return (v.boolVal == VARIANT_TRUE) ? 1.0 : 0.0;
		case VT_DECIMAL:
			{
				double retval; 
				VarR8FromDec((DECIMAL*)&v.decVal, &retval);
				return retval;
			}
		case VT_BSTR: 
			{
				double retval;
				swscanf(v.bstrVal, L"%Lf", &retval);
				return retval;
			}
		case VT_DATE: return v.date;
		case VT_CY: return v.cyVal.Hi;

		// By ref
		case VT_I1 | VT_BYREF: return *v.pcVal;
		case VT_I2 | VT_BYREF: return *v.piVal;
		case VT_I4 | VT_BYREF: return *v.plVal;
		case VT_I8 | VT_BYREF: return *v.pllVal;
		case VT_INT | VT_BYREF: return *v.pintVal;
		case VT_UI1 | VT_BYREF: return *v.pbVal;
		case VT_UI2 | VT_BYREF: return *v.puiVal;
		case VT_UI4 | VT_BYREF: return *v.pulVal;
		case VT_UI8 | VT_BYREF: return (__int64)*v.pullVal;
		case VT_UINT | VT_BYREF: return *v.puintVal;
		case VT_R4 | VT_BYREF: return *v.pfltVal;
		case VT_R8 | VT_BYREF: return *v.pdblVal;
		case VT_BOOL | VT_BYREF: return (*v.pboolVal == VARIANT_TRUE) ? 1.0 : 0.0;
		case VT_DECIMAL | VT_BYREF:
			{
				double retval; 
				VarR8FromDec((DECIMAL*)v.pdecVal, &retval);
				return retval;
			}
		case VT_BSTR | VT_BYREF:
			{
				double retval;
				swscanf(*v.pbstrVal, L"%Ld", &retval);
				return retval;
			}
		case VT_DATE | VT_BYREF: return *v.pdate;
		case VT_CY | VT_BYREF: return v.pcyVal->Hi;

		case VT_VARIANT | VT_BYREF:
			return VariantToFloat(*v.pvarVal);

		default:
			throw 0;
	}
}

bool SVVariant::VariantToBool(const VARIANT& v)
{
	return (VariantToInt(v) != 0) ? true : false;
}

wstring SVVariant::VariantToString(const VARIANT& v)
{
	switch (v.vt)
	{
		// By val
		case VT_I1: return format_wstring(L"%i", (INT32)v.cVal);
		case VT_I2: return format_wstring(L"%i", (INT32)v.iVal);
		case VT_I4: return format_wstring(L"%i", v.lVal);
		case VT_I8: return format_wstring(L"%I64i", v.llVal);
		case VT_INT: return format_wstring(L"%i", (INT32)v.intVal);
		case VT_UI1: return format_wstring(L"%u", (UINT32)v.bVal);
		case VT_UI2: return format_wstring(L"%u", (UINT32)v.uiVal);
		case VT_UI4: return format_wstring(L"%u", v.ulVal);
		case VT_UI8: return format_wstring(L"%I64u", v.ullVal);
		case VT_UINT: return format_wstring(L"%u", (UINT32)v.uintVal);
		case VT_R4: return format_wstring(L"%f", v.fltVal);
		case VT_R8: return format_wstring(L"%lf", v.dblVal);
		case VT_CY:
			return format_wstring(
				L"%15Lu.%04Lu", 
				v.cyVal.int64 / 10000, 
				v.cyVal.int64 % 10000);
		case VT_BSTR: return v.bstrVal;
		case VT_ERROR:
			return format_wstring(L"0x%08X", v.scode);
		case VT_BOOL:
			return (v.boolVal == VARIANT_TRUE) ? L"True" : L"False";
		case VT_DATE:
			{
				BSTR bstr = NULL;
				if (SUCCEEDED(VarBstrFromDate(v.date, LANG_USER_DEFAULT, 0, &bstr)))
				{
					wstring s(bstr);
					SysFreeString(bstr);
					return s;
				}
			}
			return L"";
		case VT_DISPATCH:
			return format_wstring(L"0x%08x", v.pdispVal);
		case VT_UNKNOWN:
			return format_wstring(L"0x%08x", v.punkVal);

		// By ref
		case VT_I1 | VT_BYREF: return format_wstring(L"%i", (INT32)*v.pcVal);
		case VT_I2 | VT_BYREF: return format_wstring(L"%i", (INT32)*v.piVal);
		case VT_I4 | VT_BYREF: return format_wstring(L"%i", *v.plVal);
		case VT_I8 | VT_BYREF: return format_wstring(L"%I64i", *v.pllVal);
		case VT_INT | VT_BYREF: return format_wstring(L"%i", (INT32)*v.pintVal);
		case VT_UI1 | VT_BYREF: return format_wstring(L"%u", (UINT32)*v.pbVal);
		case VT_UI2 | VT_BYREF: return format_wstring(L"%u", (UINT32)*v.puiVal);
		case VT_UI4 | VT_BYREF: return format_wstring(L"%u", *v.pulVal);
		case VT_UI8 | VT_BYREF: return format_wstring(L"%I64u", *v.pullVal);
		case VT_UINT | VT_BYREF: return format_wstring(L"%u", (UINT32)*v.puintVal);
		case VT_R4 | VT_BYREF: return format_wstring(L"%f", *v.pfltVal);
		case VT_R8 | VT_BYREF: return format_wstring(L"%lf", *v.pdblVal);
		case VT_CY | VT_BYREF:
			return format_wstring(
				L"%15Lu.%04Lu", 
				v.pcyVal->int64 / 10000, 
				v.pcyVal->int64 % 10000);
		case VT_BSTR | VT_BYREF: return *v.pbstrVal;
		case VT_ERROR | VT_BYREF:
			return format_wstring(L"0x%08X", *v.pscode);
		case VT_BOOL | VT_BYREF:
			return (*v.pboolVal == VARIANT_TRUE) ? L"True" : L"False";
		case VT_DATE | VT_BYREF:
			{
				BSTR bstr = NULL;
				if (SUCCEEDED(VarBstrFromDate(*v.pdate, LANG_USER_DEFAULT, 0, &bstr)))
				{
					wstring s(bstr);
					SysFreeString(bstr);
					return s;
				}
			}
			return L"";
		case VT_DISPATCH | VT_BYREF:
			return format_wstring(L"0x%08x", *v.ppdispVal);
		case VT_UNKNOWN | VT_BYREF:
			return format_wstring(L"0x%08x", *v.ppunkVal);

		case VT_VARIANT | VT_BYREF:
			return VariantToString(*v.pvarVal);

		case VT_NULL: 
			return L"<NULL>";

		default: 
			return L"";
	}
}

void SVVariant::IntToVariantRef(VARIANT& v, __int64 val)
{
	switch (v.vt)
	{
		case VT_I1 | VT_BYREF: *v.pcVal = val; break;
		case VT_I2 | VT_BYREF: *v.piVal = val; break;
		case VT_I4 | VT_BYREF: *v.plVal = val; break;
		case VT_I8 | VT_BYREF: *v.pllVal = val; break;
		case VT_INT | VT_BYREF: *v.pintVal = val; break;
		case VT_UI1 | VT_BYREF: *v.pbVal = val; break;
		case VT_UI2 | VT_BYREF: *v.puiVal = val; break;
		case VT_UI4 | VT_BYREF: *v.pulVal = val; break;
		case VT_UI8 | VT_BYREF: *v.pullVal = val; break;
		case VT_UINT | VT_BYREF: *v.puintVal = val; break;
		case VT_R4 | VT_BYREF: *v.pfltVal = val; break;
		case VT_R8 | VT_BYREF: *v.pdblVal = val; break;
		case VT_BOOL | VT_BYREF: *v.pboolVal = (val != 0) ? VARIANT_TRUE : VARIANT_FALSE;
		case VT_DECIMAL | VT_BYREF:
			VarDecFromUI4(val, v.pdecVal);
			break;
		case VT_BSTR | VT_BYREF:
			*v.pbstrVal = SysAllocString(format_wstring(L"%I64d", val).c_str());
			break;
		case VT_DATE | VT_BYREF: *v.pdate = val; break;
		case VT_CY | VT_BYREF:
			v.pcyVal->Lo = 0;
			v.pcyVal->Hi = val;
			break;

		case VT_VARIANT | VT_BYREF:
			IntToVariantRef(*v.pvarVal, val);
			break;

		default:
			throw 0;
	}
}

void SVVariant::FloatToVariantRef(VARIANT& v, double val)
{
	switch (v.vt)
	{
		case VT_I1 | VT_BYREF: *v.pcVal = val; break;
		case VT_I2 | VT_BYREF: *v.piVal = val; break;
		case VT_I4 | VT_BYREF: *v.plVal = val; break;
		case VT_I8 | VT_BYREF: *v.pllVal = val; break;
		case VT_INT | VT_BYREF: *v.pintVal = val; break;
		case VT_UI1 | VT_BYREF: *v.pbVal = val; break;
		case VT_UI2 | VT_BYREF: *v.puiVal = val; break;
		case VT_UI4 | VT_BYREF: *v.pulVal = val; break;
		case VT_UI8 | VT_BYREF: *v.pullVal = val; break;
		case VT_UINT | VT_BYREF: *v.puintVal = val; break;
		case VT_R4 | VT_BYREF: *v.pfltVal = val; break;
		case VT_R8 | VT_BYREF: *v.pdblVal = val; break;
		case VT_BOOL | VT_BYREF: *v.pboolVal = (val != 0.0) ? VARIANT_TRUE : VARIANT_FALSE;
		case VT_DECIMAL | VT_BYREF:
			VarDecFromR8(val, v.pdecVal);
			break;
		case VT_BSTR | VT_BYREF:
			*v.pbstrVal = SysAllocString(format_wstring(L"%Lf", val).c_str());
			break;
		case VT_DATE | VT_BYREF: *v.pdate = val; break;
		case VT_CY | VT_BYREF:
			v.pcyVal->Lo = 0;
			v.pcyVal->Hi = val;
			break;

		case VT_VARIANT | VT_BYREF:
			FloatToVariantRef(*v.pvarVal, val);
			break;

		default:
			throw 0;
	}
}

void SVVariant::StringToVariantRef(VARIANT& v, LPCWSTR val)
{
	if (v.vt != (VT_BSTR | VT_BYREF))
	{
		throw 0;
	}
	*v.pbstrVal = SysAllocString(val);
}

void SVVariant::VariantTypeToString(WORD vt, 
									LPWSTR pwszType, 
									int cchType)
{
	pwszType[--cchType] = L'\0';
	switch (vt & VT_TYPEMASK)
	{
		case VT_EMPTY:
			wcsncpy(pwszType, L"VT_EMPTY", cchType);
			break;
		case VT_NULL:
			wcsncpy(pwszType, L"VT_NULL", cchType);
			break;
		case VT_I2:
			wcsncpy(pwszType, L"VT_I2", cchType);
			break;
		case VT_I4:
			wcsncpy(pwszType, L"VT_I4", cchType);
			break;
		case VT_I8:
			wcsncpy(pwszType, L"VT_I8", cchType);
			break;
		case VT_UI2:
			wcsncpy(pwszType, L"VT_UI2", cchType);
			break;
		case VT_UI4:
			wcsncpy(pwszType, L"VT_UI4", cchType);
			break;
		case VT_UI8:
			wcsncpy(pwszType, L"VT_UI8", cchType);
			break;
		case VT_R4:
			wcsncpy(pwszType, L"VT_R4", cchType);
			break;
		case VT_R8:
			wcsncpy(pwszType, L"VT_R8", cchType);
			break;
		case VT_CY:
			wcsncpy(pwszType, L"VT_CY", cchType);
			break;
		case VT_DATE:
			wcsncpy(pwszType, L"VT_DATE", cchType);
			break;
		case VT_BSTR:
			wcsncpy(pwszType, L"VT_BSTR", cchType);
			break;
		case VT_ERROR:
			wcsncpy(pwszType, L"VT_ERROR", cchType);
			break;
		case VT_BOOL:
			wcsncpy(pwszType, L"VT_BOOL", cchType);
			break;
		case VT_VARIANT:
			wcsncpy(pwszType, L"VT_VARIANT", cchType);
			break;
		case VT_DECIMAL:
			wcsncpy(pwszType, L"VT_DECIMAL", cchType);
			break;
		case VT_I1:
			wcsncpy(pwszType, L"VT_I1", cchType);
			break;
		case VT_UI1:
			wcsncpy(pwszType, L"VT_UI1", cchType);
			break;
		case VT_INT:
			wcsncpy(pwszType, L"VT_INT", cchType);
			break;
		case VT_UINT:
			wcsncpy(pwszType, L"VT_UINT", cchType);
			break;
		case VT_VOID:
			wcsncpy(pwszType, L"VT_VOID", cchType);
			break;
		case VT_SAFEARRAY:
			wcsncpy(pwszType, L"VT_SAFEARRAY", cchType);
			break;
		case VT_USERDEFINED:
			wcsncpy(pwszType, L"VT_USERDEFINED", cchType);
			break;
		case VT_RECORD:
			wcsncpy(pwszType, L"VT_RECORD", cchType);
			break;
		case VT_BLOB:
			wcsncpy(pwszType, L"VT_BLOB", cchType);
			break;
		case VT_STREAM:
			wcsncpy(pwszType, L"VT_STREAM", cchType);
			break;
		case VT_STORAGE:
			wcsncpy(pwszType, L"VT_STORAGE", cchType);
			break;
		case VT_STREAMED_OBJECT:
			wcsncpy(pwszType, L"VT_STREAMED_OBJECT", cchType);
			break;
		case VT_STORED_OBJECT:
			wcsncpy(pwszType, L"VT_BLOB_OBJECT", cchType);
			break;
		case VT_CF:
			wcsncpy(pwszType, L"VT_CF", cchType);
			break;
		case VT_DISPATCH:
			wcsncpy(pwszType, L"VT_DISPATCH", cchType);
			break;
		case VT_UNKNOWN:
			wcsncpy(pwszType, L"VT_UNKNOWN", cchType);
			break;
		default:
			_snwprintf(pwszType, cchType, L"Unknown (%d)", vt & VT_TYPEMASK);
			break;
	}

	cchType -= wcslen(pwszType);

	if (vt & VT_VECTOR)
	{
		wcsncat(pwszType, L" | VT_VECTOR", cchType);
	}

	if (vt & VT_ARRAY)
	{
		wcsncat(pwszType, L" | VT_ARRAY", cchType);
	}

	if (vt & VT_BYREF)
	{
		wcsncat(pwszType, L" | VT_BYREF", cchType);
	}

	if (vt & VT_RESERVED)
	{
		wcsncat(pwszType, L" | VT_RESERVED", cchType);
	}
}

void SVVariant::VariantValueToString(const VARIANT& v, 
									 LPWSTR pwszValue, 
									 int cchValue)
{
	pwszValue[--cchValue] = L'\0';
	switch (v.vt)
	{
		case VT_EMPTY:
			wcsncpy(pwszValue, L"", cchValue);
			break;
		case VT_NULL:
			wcsncpy(pwszValue, L"", cchValue);
			break;
		case VT_I2:
			_snwprintf(pwszValue, cchValue, L"%i", v.iVal);
			break;
		case VT_I2 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%i", *v.piVal);
			break;
		case VT_I4:
		case VT_INT:
			_snwprintf(pwszValue, cchValue, L"%li", v.lVal);
			break;
		case VT_I4 | VT_BYREF:
		case VT_INT | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%li", *v.plVal);
			break;
		case VT_I8:
			_snwprintf(pwszValue, cchValue, L"%I64i", v.llVal);
			break;
		case VT_I8 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%I64i", *v.pllVal);
			break;
		case VT_UI2:
			_snwprintf(pwszValue, cchValue, L"%u", v.uiVal);
			break;
		case VT_UI2 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%u", *v.puiVal);
			break;
		case VT_UI4:
		case VT_UINT:
			_snwprintf(pwszValue, cchValue, L"%lu", v.ulVal);
			break;
		case VT_UI4 | VT_BYREF:
		case VT_UINT | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%lu", *v.pulVal);
			break;
		case VT_UI8:
			_snwprintf(pwszValue, cchValue, L"%I64u", v.ullVal);
			break;
		case VT_UI8 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%I64u", *v.pullVal);
			break;
		case VT_R4:
			_snwprintf(pwszValue, cchValue, L"%f", v.fltVal);
			break;
		case VT_R4 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%f", *v.pfltVal);
			break;
		case VT_R8:
			_snwprintf(pwszValue, cchValue, L"%lf", v.dblVal);
			break;
		case VT_R8 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%lf", *v.pdblVal);
			break;
		case VT_CY:
			_snwprintf(
				pwszValue, 
				cchValue,
				L"%15Lu.%04Lu", 
				v.cyVal.int64 / 10000, 
				v.cyVal.int64 % 10000);
			break;
		case VT_CY | VT_BYREF:
			_snwprintf(
				pwszValue, 
				cchValue,
				L"%15Lu.%04Lu", 
				v.pcyVal->int64 / 10000, 
				v.pcyVal->int64 % 10000);
			break;
		case VT_BSTR:
			_snwprintf(pwszValue, cchValue, L"\"%s\"", v.bstrVal);
			break;
		case VT_BSTR | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"\"%s\"", *v.pbstrVal);
			break;
		case VT_ERROR:
			_snwprintf(pwszValue, cchValue, L"0x%08X", v.scode);
			break;
		case VT_BOOL:
			_snwprintf(pwszValue, cchValue, L"%s",
				v.boolVal == VARIANT_TRUE? L"True" : L"False");
			break;
		case VT_BOOL | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%s",
				*v.pboolVal == VARIANT_TRUE? L"True" : L"False");
			break;
		case VT_DATE:
			{
				BSTR bstr = 0;
				if (SUCCEEDED(VarBstrFromDate(v.date, LANG_USER_DEFAULT, 0, &bstr)))
				{
					wcscpy(pwszValue, bstr);
					SysFreeString(bstr);
				}
			}
			break;
		case VT_DATE | VT_BYREF:
			{
				BSTR bstr = 0;
				if (SUCCEEDED(VarBstrFromDate(*v.pdate, LANG_USER_DEFAULT, 0, &bstr)))
				{
					wcscpy(pwszValue, bstr);
					SysFreeString(bstr);
				}
			}
			break;
		case VT_I1:
			_snwprintf(pwszValue, cchValue, L"%i", v.cVal);
			break;
		case VT_I1 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%i", *v.pcVal);
			break;
		case VT_UI1:
			_snwprintf(pwszValue, cchValue, L"%u", v.bVal);
			break;
		case VT_UI1 | VT_BYREF:
			_snwprintf(pwszValue, cchValue, L"%u", *v.pbVal);
			break;
		case VT_VOID:
			wcsncpy(pwszValue, L"", cchValue);
			break;
		case VT_DISPATCH:
			_snwprintf(pwszValue, cchValue, L"0x%08x", v.pdispVal);
			break;
		case VT_UNKNOWN:
			_snwprintf(pwszValue, cchValue, L"0x%08x", v.punkVal);
			break;
		default:
			wcsncpy(pwszValue, L"...", cchValue);
			break;
	}
}

void SVVariant::VariantToString(const VARIANT& v, 
								LPWSTR pwszBuf, 
								int cchBuf)
{
	if (cchBuf > 64)
	{
		wcscpy(pwszBuf, L"(");
		VariantTypeToString(v.vt, pwszBuf + wcslen(pwszBuf), 60);
		wcscat(pwszBuf, L", ");
		VariantValueToString(v, pwszBuf + wcslen(pwszBuf), cchBuf - 64);
		wcscat(pwszBuf, L")");
	}
}

HRESULT SVVariant::VariantCopyByRef(VARIANTARG* pvargDest, 
									VARIANTARG* pvargSrc)
{
	if (pvargDest->vt != pvargSrc->vt && !(pvargSrc->vt & VT_BYREF))
	{
		return E_FAIL;
	}
	switch (pvargSrc->vt & VT_TYPEMASK)
	{
		case VT_I1:			*pvargDest->pcVal = *pvargSrc->pcVal;			break;
		case VT_UI1:		*pvargDest->pbVal = *pvargSrc->pbVal;			break;
		case VT_I2:			*pvargDest->piVal = *pvargSrc->piVal;			break;
		case VT_UI2:		*pvargDest->puiVal = *pvargSrc->puiVal;		break;
		case VT_I4:			*pvargDest->plVal = *pvargSrc->plVal;			break;
		case VT_UI4:		*pvargDest->pulVal = *pvargSrc->pulVal;		break;
		case VT_I8:			*pvargDest->pllVal = *pvargSrc->pllVal;		break;
		case VT_UI8:		*pvargDest->pullVal = *pvargSrc->pullVal;		break;
		case VT_INT:		*pvargDest->pintVal = *pvargSrc->pintVal;		break;
		case VT_UINT:		*pvargDest->puintVal = *pvargSrc->puintVal;	break;
		case VT_R4:			*pvargDest->pfltVal = *pvargSrc->pfltVal;		break;
		case VT_R8:			*pvargDest->pdblVal = *pvargSrc->pdblVal;		break;

		case VT_CY:			*pvargDest->pcyVal = *pvargSrc->pcyVal;		break;
		case VT_BSTR:	
			if (*pvargDest->pbstrVal)
			{
				SysFreeString(*pvargDest->pbstrVal);
			}
			*pvargDest->pbstrVal = SysAllocString(*pvargSrc->pbstrVal);
			break;
		case VT_DECIMAL:	*pvargDest->pdecVal = *pvargSrc->pdecVal;		break;
		case VT_ERROR:		*pvargDest->pscode = *pvargSrc->pscode;		break;
		case VT_BOOL:		*pvargDest->pboolVal = *pvargSrc->pboolVal;	break;
		case VT_DATE:		*pvargDest->pdate = *pvargSrc->pdate;			break;
		case VT_DISPATCH:	*pvargDest->ppdispVal = *pvargSrc->ppdispVal;	break;
		case VT_UNKNOWN:	*pvargDest->ppunkVal = *pvargSrc->ppunkVal;	break;
			
		case VT_VARIANT:
			VariantCopyByRef(pvargDest->pvarVal, pvargSrc->pvarVal);
			break;
	}
	return S_OK;
}
