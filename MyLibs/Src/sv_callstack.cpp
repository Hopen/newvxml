/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <Dbghelp.h>

#include "sv_callstack.h"
#include "sv_strutils.h"
#include "sv_memory.h"

#define MAX_PDB_STR_LENGTH			32

namespace SVCallStack
{

using namespace SVMemory;

// User context for SymEnumSymbols
struct ESCB_PARAM
{
	STACKFRAME*			pSF;
	CCallstackFrame*	pFrame;
	bool				bTrimSTDTypes;
	HANDLE				hProcess;
};

// Forward declarations

BOOL CALLBACK EnumerateSymbolsCallback(PSYMBOL_INFO pSymInfo,
	ULONG SymbolSize,
	PVOID UserContext);
void DumpTypeIndex(CCallstackFrame::LocalVarInfo* pVarInfo,
	DWORD64 dwModBase,
	DWORD dwTypeIndex,
	DWORD_PTR dwOffs,
	ESCB_PARAM* pParams);
BOOL FormatSymbolValue(CCallstackFrame::LocalVarInfo* pVarInfo,
	PSYMBOL_INFO pSym,
	ESCB_PARAM* pParams);
wstring CompactTypeName(const wstring& sTypeName);

/************************************************************************/
/* ���������� ������ LocationInfo                                       */
/************************************************************************/

void LocationInfo::GetAddressInfo(HANDLE hProcess, PVOID pAddr)
{
	SVSymInfo::CSymInfo		si;
	SVSymInfo::CLineInfo	li;
	SVSymInfo::CModuleInfo	mi;

	try
	{
		if (si.Init(hProcess, pAddr))
		{
			this->Function.sName = si.sName;
			this->Function.dwOffs = si.dwOffs;
		}

		if (li.Init(hProcess, pAddr))
		{
			this->Line.sFile = li.sFileName;
			this->Line.nLineNum = li.dwLine;
			this->Line.dwOffs = li.dwOffs;
		}

		if (mi.Init(hProcess, pAddr))
		{
			this->Module.sName = mi.sModuleName;
			const IMAGE_SECTION_HEADER* pSection = mi.GetSectionByAddress(pAddr);
			if (pSection)
			{
				char szSectionName[9] = {0};
				strncpy(szSectionName, (const char*)pSection->Name, 8);
				this->Module.sSection = StrA2wstring(szSectionName);
				this->Module.dwOffs = (DWORD)(pAddr) - (DWORD)mi.dwImageBase - (DWORD)pSection->VirtualAddress;
			}
		}
	}
	catch (...)
	{
	}
}

/************************************************************************/
/* ���������� ������ CCallstackFrame::LocalVarInfo                      */
/************************************************************************/

wstring CCallstackFrame::LocalVarInfo::GetTypeStr() const
{
	wstring sType;

	switch (this->SymTag)
	{
		case SymTagTypedef:
			sType = L"typedef ";
			break;
		case SymTagEnum:
			sType = L"enum ";
			break;
		case SymTagFunction:
			sType = L"function ";
			break;
		case SymTagLabel:
			sType = L"label ";
			break;
	}

	switch (this->DataKind)
	{
		case DataIsStaticLocal:
		case DataIsStaticMember:
			sType = L"static ";
			break;
		case DataIsConstant:
			sType = L"const ";
			break;
	}

	switch (this->UdtKind)
	{
		case UdtClass:
			sType += L"class ";
			break;
		case UdtStruct:
			sType += L"struct ";
			break;
		case UdtUnion:
			sType += L"union ";
			break;
	}

	if (!this->sTypeName.empty())
	{
		sType += this->sTypeName;
	}
	else
	{
		switch (this->BasicType)
		{
			case btNoType:									break;
			case btVoid:		sType += L"void";			break;
			case btChar:		sType += L"char";			break;
			case btWChar:		sType += L"wchar_t";		break;
			case btInt:			
				switch (dwValueSize)
				{
					case 1:
						sType += L"char";			break;
					case 2:
						sType += L"short";			break;
					case 4:
						sType += L"int";			break;
					case 8:
						sType += L"__int64";		break;
				}
				break;
			case btUInt:
				switch (dwValueSize)
				{
					case 1:
						sType += L"unsigned char";	break;
					case 2:
						sType += L"unsigned short";	break;
					case 4:
						sType += L"unsigned int";	break;
					case 8:
						sType += L"unsigned __int64";	break;
				}
				break;
			case btFloat:
				switch (dwValueSize)
				{
					case 4:
						sType += L"float";			break;
					case 8:
						sType += L"double";			break;
					case 10:
						sType += L"long double";	break;
				}
				break;
			case btBCD:			sType += L"BCD";			break;
			case btBool:		sType += L"bool";			break;
			case btLong:		sType += L"long";			break;
			case btULong:		sType += L"unsigned long";	break;
			case btCurrency:	sType += L"CURRENCY";		break;
			case btDate:		sType += L"DATE";			break;
			case btVariant:		sType += L"VARIANT";		break;
			case btComplex:		sType += L"complex";		break;
			case btBit:			sType += L"bit";			break;
			case btBSTR:		sType += L"BSTR";			break;
			case btHresult:		sType += L"HRESULT";		break;
		}
	}

	for (int i = 0; i < this->nPtrCount; i++)
	{
		sType += L"*";
	}

	for (int i = 0; i < this->nArrCount; i++)
	{
		sType += L"[]";
	}

	return sType;
}

bool my_isgraph(char c)
{
	return isgraph((unsigned char)c) != 0;
}

bool my_isgraph(wchar_t c)
{
	return (iswgraph(c) != 0) && (c < 255);
}

template <class Char>
int MakePrintableStr(Char* pStr, Char cDefault = '?')
{
	int nCount = 0;
	Char* p = pStr;

	while (*p)
	{
		if (!my_isgraph(*p) && *p != ' ')
		{
			*p = cDefault;
			nCount++;
		}
		p++;
	}

	return nCount;
}

wstring CCallstackFrame::LocalVarInfo::GetValueStr(HANDLE hProcess) const 
{
	try
	{
		if (this->nPtrCount || this->nArrCount)
		{
			// level 1 pointers only
			if (this->nPtrCount + this->nArrCount == 1)
			{
				switch (this->BasicType)
				{
					case btChar:
						{
							char szBuf[MAX_PDB_STR_LENGTH + 1] = {0};
							if (ReadRemoteStr<char>(hProcess, pValue, szBuf, MAX_PDB_STR_LENGTH))
							{
								MakePrintableStr<char>(szBuf);
								return format_wstring(L" = \"%S\" (0x%p)", szBuf, pValue);
							}
							else
							{
								return format_wstring(L" = 0x%p", pValue);
							}
						}
						break;

					case btUInt:
						if (dwValueSize != 2)
						{
							break;
						}

print_bstr:
					case btWChar:
						{
							wchar_t wszBuf[MAX_PDB_STR_LENGTH + 1] = {0};
							if (ReadRemoteStr<wchar_t>(hProcess, pValue, wszBuf, MAX_PDB_STR_LENGTH * sizeof(wchar_t)))
							{
								MakePrintableStr<wchar_t>(wszBuf);
								return format_wstring(L" = \"%s\" (0x%p)", wszBuf, pValue);
							}
							else
							{
								return format_wstring(L" = 0x%p", pValue);
							}
						}
						break;
					}
			}
			return format_wstring(L" = 0x%p", pValue);
		}
		else if (this->SymTag == SymTagBaseType || this->SymTag == SymTagEnum)
		{				
			switch (this->BasicType)
			{
				case btChar:
					{
						unsigned char c = ReadRemotePtr<char>(hProcess, pValue);
						if (isgraph(c) || c == ' ')
						{
							return format_wstring(L" = '%c' (0x%02X)", c, c);
						}
						else
						{
							return format_wstring(L" = 0x%02X", c);
						}
					}
					break;

				case btWChar:
					{
						wchar_t c = ReadRemotePtr<wchar_t>(hProcess, pValue);
						if (iswgraph(c) || c == L' ')
						{
							return format_wstring(L" = '%c' (0x%04X)", c, c);
						}
						else
						{
							return format_wstring(L" = 0x%04X", c);
						}
					}
					break;

				case btInt:
				case btLong:
					switch (dwValueSize)
					{
						case 1:
							{
								char val = ReadRemotePtr<char>(hProcess, pValue);
								return format_wstring(L" = %i (0x%02x)", val, (unsigned char)val);
							}
						case 2:
							{
								short val = ReadRemotePtr<short>(hProcess, pValue);
								return format_wstring(L" = %i (0x%04x)", val, (unsigned short)val);
							}
						case 4:
							{
								int val = ReadRemotePtr<int>(hProcess, pValue);
								return format_wstring(L" = %i (0x%08x)", val, (unsigned int)val);
							}
						case 8:
							{
								__int64 val = ReadRemotePtr<__int64>(hProcess, pValue);
								return format_wstring(L" = %I64i (0x%016I64x)", val, (unsigned __int64)val);
							}
							break;
					}
					break;

				case btUInt:
				case btULong:
					switch (dwValueSize)
					{
						case 1:
							{
								unsigned char val = ReadRemotePtr<unsigned char>(hProcess, pValue);
								return format_wstring(L" = %u (0x%02x)", val, val);
							}
						case 2:
							{
								unsigned short val = ReadRemotePtr<unsigned short>(hProcess, pValue);
								return format_wstring(L" = %u (0x%04x)", val, val);
							}
						case 4:
							{
								unsigned int val = ReadRemotePtr<unsigned int>(hProcess, pValue);
								return format_wstring(L" = %u (0x%08x)", val, val);
							}
						case 8:
							{
								unsigned __int64 val = ReadRemotePtr<unsigned __int64>(hProcess, pValue);
								return format_wstring(L" = %I64u (0x%016I64x)", val, val);
							}
							break;
					}
					break;

				case btFloat:
					switch (dwValueSize)
					{
						case 4:
							return format_wstring(L" = %e", ReadRemotePtr<float>(hProcess, pValue));
						case 8:
							return format_wstring(L" = %le", ReadRemotePtr<double>(hProcess, pValue));
						case 10:
							return format_wstring(L" = %le", ReadRemotePtr<long double>(hProcess, pValue));
					}
					break;

				case btBool:
					return format_wstring(L" = %s", ReadRemotePtr<bool>(hProcess, pValue) ? L"true" : L"false");

				case btHresult:
					{
						HRESULT val = ReadRemotePtr<HRESULT>(hProcess, pValue);
						return format_wstring(L" = 0x%08x", val);
					}

				case btBSTR:
					goto print_bstr;

				case btNoType:
					return format_wstring(L" = 0x%p (no typeinfo)", ReadRemotePtr<LPCVOID>(hProcess, pValue));
			}
		}
		else if (this->SymTag == SymTagArrayType)
		{
			switch (this->BasicType)
			{
				case btChar:
					{
						char szBuf[MAX_PDB_STR_LENGTH + 1] = {0};
						wchar_t wszBuf[MAX_PDB_STR_LENGTH + 1] = {0};
						if (ReadRemoteStr<char>(hProcess, pValue, szBuf, MAX_PDB_STR_LENGTH))
						{
							MakePrintableStr<char>(szBuf);
							StrA2WHelper(wszBuf, szBuf);
							return format_wstring(L" = \"%s\"", wszBuf);
						}
					}
					break;

				case btUInt:
					if (dwValueSize != 2)
					{
						break;
					}

				case btWChar:
					{
						wchar_t wszBuf[MAX_PDB_STR_LENGTH + 1] = {0};
						if (ReadRemoteStr<wchar_t>(hProcess, pValue, wszBuf, MAX_PDB_STR_LENGTH * sizeof(wchar_t)))
						{
							MakePrintableStr<wchar_t>(wszBuf);
							return format_wstring(L" = \"%s\"", wszBuf);
						}
					}
					break;
			}
		}
	}
	catch (...)
	{
		return L" (cannot read value)";
	}

	return L"";
}

wstring CCallstackFrame::LocalVarInfo::GetFullInfo(HANDLE hProcess,
	int nRecurse) const
{
	wstring sInfo;

	try
	{
		sInfo += wstring(nRecurse * 4, L' ');
		sInfo += format_wstring(L"%s %s", GetTypeStr().c_str(), sName.c_str());
		sInfo += GetValueStr(hProcess);

		// FILETIME struct handling
		if (!wcscmp(sTypeName.c_str(), L"_FILETIME"))
		{
			SYSTEMTIME st;
			FILETIME ft = ReadRemotePtr<FILETIME>(hProcess, pValue);
			::FileTimeToSystemTime(&ft, &st);
			sInfo += format_wstring(L" (%02d.%02d.%04d %02d:%02d:%02d.%03d)",
				st.wDay, st.wMonth, st.wYear, 
				st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}

		if (this->SymTag != SymTagFunction)
		{
			CCallstackFrame::LOCALS::const_iterator it = children.begin();
			for (; it != children.end(); ++it)
			{
				sInfo += L"\n";
				sInfo += it->GetFullInfo(hProcess, nRecurse + 1);
			}
		}
	}
	catch (...)
	{
	}

	return sInfo;
}

/************************************************************************/
/* ���������� ������ CCallstackFrame                                    */
/************************************************************************/

wstring CCallstackFrame::GetFullInfo(HANDLE hProcess) const
{
	wstring sInfo;

	try
	{
		sInfo = format_wstring(
			L"# 0x%p, %s+%d, %s, %s\n",
			Location.dwAddr, 
			Location.Function.sName.c_str(), Location.Function.dwOffs,
			Location.Line.sFile.empty() ? L"<no source info>" : 
		format_wstring(
			L"%s [%s+0x%p] %d+%d", 
			Location.Line.sFile.c_str(), 
			Location.Module.sSection.c_str(), Location.Module.dwOffs,
			Location.Line.nLineNum, 
			Location.Line.dwOffs).c_str(), 
			Location.Module.sName.c_str()
			);

		LOCALS::const_iterator it = Locals.begin();
		for (; it != Locals.end(); ++it)
		{
			if (it->bParameter)
			{
				sInfo += L"[Param] ";
			}
			else
			{
				sInfo += L"[Local] ";
			}
			sInfo += it->GetFullInfo(hProcess);
			sInfo += L"\n";
		}
	}
	catch (...)
	{
	}

	return sInfo;
}

/************************************************************************/
/* ������� ��� �������� �����                                           */
/************************************************************************/

CStackWalker::CStackWalker() :
m_hProcess(NULL),
	m_hThread(NULL)
{
	ZeroMemory(&m_Context, sizeof(m_Context));
}

bool CStackWalker::Init(PCONTEXT pContext, 
	HANDLE hThread, 
	HANDLE hProcess)
{
	if (m_hProcess)
	{
		return false;
	}

	try
	{
		m_Context = *pContext;
		m_hThread = hThread;
		m_hProcess = hProcess;

		if (!::SymInitialize(m_hProcess, NULL, false))
		{
			return false;
		}
		::SymSetOptions(::SymGetOptions() & ~SYMOPT_UNDNAME);
	}
	catch (...)
	{
		return false;
	}

	return true;
}

bool CStackWalker::Init(PCONTEXT pContext, 
	DWORD dwThreadId, 
	DWORD dwProcessId)
{
	if (m_hProcess)
	{
		return false;
	}

	try
	{
		m_Context = *pContext;

		m_hProcess = ::OpenProcess(
			PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | READ_CONTROL, 
			FALSE, 
			dwProcessId);

		if (!m_hProcess)
		{
			return false;
		}

		m_hThread = ::OpenThread(
			THREAD_QUERY_INFORMATION | THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME | READ_CONTROL, 
			FALSE, 
			dwThreadId);

		if (!m_hThread)
		{
			CloseHandle(m_hProcess);
			return false;
		}

		if (!::SymInitialize(m_hProcess, NULL, false))
		{
			CloseHandle(m_hThread);
			CloseHandle(m_hProcess);
			return false;
		}
		::SymSetOptions(::SymGetOptions() & ~SYMOPT_UNDNAME);
	}
	catch (...)
	{
		return false;
	}

	return true;
}

void CStackWalker::Destroy()
{
	try
	{
		if (m_hProcess)
		{
			::CloseHandle(m_hProcess);
			::SymCleanup(m_hProcess);
		}

		ZeroMemory(&m_Context, sizeof(m_Context));
		::CloseHandle(m_hThread);
		m_CallStack.clear();
	}
	catch (...)
	{
	}
}

wstring CStackWalker::GetCallStackInfo() const
{
	wstring sInfo;

	try
	{
		RETCALLSTACK::const_iterator it = m_CallStack.begin();
		for (; it != m_CallStack.end(); ++it)
		{
			sInfo += it->GetFullInfo(m_hProcess);
		}
	}
	catch (...)
	{
	}

	return sInfo;
}

void CStackWalker::CallStackSnapshot(bool bWriteVariables,
	bool bTrimSTDTypes,
	DWORD dwMaxDeep)
{
	DWORD dwMachineType = 0;
	STACKFRAME sf = {0};
	DWORD dwDeep = 0;

#ifdef _M_IX86
	// Initialize the STACKFRAME structure for the first call.  This is only
	// necessary for Intel CPUs, and isn't mentioned in the documentation.
	sf.AddrPC.Offset	= m_Context.Eip;
	sf.AddrPC.Mode		= AddrModeFlat;
	sf.AddrStack.Offset	= m_Context.Esp;
	sf.AddrStack.Mode	= AddrModeFlat;
	sf.AddrFrame.Offset	= m_Context.Ebp;
	sf.AddrFrame.Mode	= AddrModeFlat;

	dwMachineType		= IMAGE_FILE_MACHINE_I386;
#else
#error Invalid processor
#endif

	try
	{
		while (
			::StackWalk(
			dwMachineType, 
			m_hProcess, 
			m_hThread,
			&sf,
			&m_Context,
			NULL,
			SymFunctionTableAccess,
			SymGetModuleBase,
			0) 
			&& sf.AddrFrame.Offset)
		{
			CCallstackFrame stackFrame;

			try
			{
				stackFrame.Location.dwAddr = sf.AddrPC.Offset;
				stackFrame.Location.dwFrame = sf.AddrFrame.Offset;

				stackFrame.Location.GetAddressInfo(m_hProcess, (PVOID)sf.AddrPC.Offset);

				// Write out the variables, if desired
				if (bWriteVariables)
				{
					IMAGEHLP_STACK_FRAME ihStackFrame = {0};

					ihStackFrame.InstructionOffset = sf.AddrPC.Offset;
					::SymSetContext(m_hProcess, &ihStackFrame, 0);

					ESCB_PARAM params;

					params.hProcess = m_hProcess;
					params.pFrame = &stackFrame;
					params.pSF = &sf;
					params.bTrimSTDTypes = bTrimSTDTypes;

					// Enumerate the locals/parameters
					::SymEnumSymbols(
						m_hProcess, 
						0, 0, 
						EnumerateSymbolsCallback,
						&params);
				}

				m_CallStack.push_back(stackFrame);
			}
			catch (...)
			{
			}

			if (++dwDeep > dwMaxDeep)
			{
				break;
			}
		}
	}
	catch (...)
	{
	}
}

/************************************************************************/
/* ��������������� �������                                              */
/************************************************************************/

#pragma warning(push)
#pragma warning(disable:4100)

BOOL CALLBACK EnumerateSymbolsCallback(PSYMBOL_INFO pSymInfo,
									   ULONG SymbolSize,
									   PVOID UserContext)
{
	try
	{
		ESCB_PARAM* pParams = (ESCB_PARAM*)UserContext;
		CCallstackFrame::LocalVarInfo varInfo;
		FormatSymbolValue(&varInfo, pSymInfo, pParams);
		pParams->pFrame->Locals.push_back(varInfo);
	}
	catch (...)
	{
	}

	return TRUE;
}

#pragma warning(pop)

BOOL FormatSymbolValue(CCallstackFrame::LocalVarInfo* pVarInfo,
					   PSYMBOL_INFO pSym,
					   ESCB_PARAM* pParams)
{
	// Will point to the variable's data in memory
	DWORD_PTR pVariable = NULL;		

	pVarInfo->bParameter = (pSym->Flags & IMAGEHLP_SYMBOL_INFO_PARAMETER) != 0;
	pVarInfo->sName = StrA2wstring(pSym->Name);
	pVarInfo->dwTypeId = pSym->TypeIndex;
	pVarInfo->SymTag = (enum SymTagEnum)pSym->Tag;

	if (pSym->Flags & IMAGEHLP_SYMBOL_INFO_REGISTER)
	{
		pVarInfo->LocationType = LocIsEnregistered;
	}
	else if (pSym->Flags & IMAGEHLP_SYMBOL_INFO_REGRELATIVE)
	{
		pVarInfo->LocationType = LocIsRegRel;
	}
	else if (pSym->Flags & IMAGEHLP_SYMBOL_INFO_TLSRELATIVE)
	{
		pVarInfo->LocationType = LocIsTLS;
	}
	else if (pSym->Flags & IMAGEHLP_SYMBOL_INFO_CONSTANT)
	{
		pVarInfo->LocationType = LocIsConstant;
	}
	else
	{
		pVarInfo->LocationType = LocIsNull;
	}

	// If it's a function, don't do anything.
	if (pSym->Tag == SymTagFunction)
	{
		return FALSE;
	}

	if (pSym->Flags & IMAGEHLP_SYMBOL_INFO_REGRELATIVE)
	{
		pVariable = pParams->pSF->AddrFrame.Offset;
		pVariable += (DWORD_PTR)pSym->Address;
	}
	else if (pSym->Flags & IMAGEHLP_SYMBOL_INFO_REGISTER)
	{
		// Don't try to report register variable
		return FALSE;
	}
	else 
	{
		// It must be a global variable
		pVariable = (DWORD_PTR)pSym->Address;
	}

	pVarInfo->pValue = (LPCVOID)pVariable;
	pVarInfo->dwValueSize = pSym->Size;

	DumpTypeIndex(
		pVarInfo,
		pSym->ModBase,
		pSym->TypeIndex,
		0,
		pParams);

	return TRUE;
}

void DumpTypeIndex(CCallstackFrame::LocalVarInfo* pVarInfo,
				   DWORD64 dwModBase,
				   DWORD dwTypeIndex,
				   DWORD_PTR dwOffs,
				   ESCB_PARAM* pParams)
{
	LPCWSTR pwszTypeName = NULL;	
	DWORD dwChildrenCount = 0, dwType;
	ULONG64 ulAddr = 0;
	HANDLE hProcess = pParams->hProcess;
	bool bStd = false;

	try
	{
		pVarInfo->BasicType = btNoType;
		pVarInfo->UdtKind = (UdtKind)-1;
		pVarInfo->DataKind = (DataKind)DataIsUnknown;

		if (pVarInfo->SymTag != SymTagData)
		{
			return;
		}

		dwType = dwTypeIndex;

		::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_GET_SYMTAG, &pVarInfo->SymTag);

		for (;;)
		{
			switch (pVarInfo->SymTag)
			{
				case SymTagBaseType:
					break;

				case SymTagArrayType:
				case SymTagPointerType:
					if (pVarInfo->SymTag == SymTagPointerType)
					{
						pVarInfo->nPtrCount++;
					}
					else
					{
						pVarInfo->nArrCount++;
					}

					::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_GET_TYPE, &dwType);
					::SymGetTypeInfo(hProcess, dwModBase, dwType, TI_GET_LENGTH, &pVarInfo->dwValueSize);
					::SymGetTypeInfo(hProcess, dwModBase, dwType, TI_GET_SYMTAG, &pVarInfo->SymTag);
					::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_GET_TYPEID, &dwTypeIndex);

					continue;

				case SymTagUDT:
					::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_GET_UDTKIND, &pVarInfo->UdtKind);
					break;
			}
			break;
		}

		::SymGetTypeInfo(hProcess, dwModBase, dwType, TI_GET_BASETYPE, &pVarInfo->BasicType);
		::SymGetTypeInfo(hProcess, dwModBase, dwType, TI_GET_DATAKIND, &pVarInfo->DataKind);

		if (::SymGetTypeInfo(hProcess, dwModBase, dwType, TI_GET_SYMNAME, &pwszTypeName))
		{
			if (pParams->bTrimSTDTypes)
			{
				pVarInfo->sTypeName = CompactTypeName(pwszTypeName);
				bStd = ::wcsstr(pwszTypeName, L"std::") == pwszTypeName;
			}
			else
			{
				pVarInfo->sTypeName = pwszTypeName;
			}
		}

		// Determine how many children this type has.
		::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_GET_CHILDRENCOUNT, &dwChildrenCount);

		if (!dwChildrenCount ||
			::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_GET_ADDRESS, &ulAddr))
		{
			return;
		}

		// Don't evaluate children for array and pointer types
		if (pVarInfo->nPtrCount || pVarInfo->nArrCount)
		{
			return;
		}

		if (pVarInfo->SymTag == SymTagEnum)
		{
			// We don't need enum's children
			return;
		}

		struct FINDCHILDREN : TI_FINDCHILDREN_PARAMS
		{
			ULONG MoreChildIds[1024];
		} children;

		children.Count = dwChildrenCount;
		children.Start = 0;

		// Get the array of TypeIds, one for each child type
		if (!::SymGetTypeInfo(hProcess, dwModBase, dwTypeIndex, TI_FINDCHILDREN, &children))
		{
			return;
		}

		// Iterate through each of the children
		for (DWORD i = 0; i < dwChildrenCount; i++)
		{
			DWORD dwChildId = children.ChildId[i], dwTypeId, dwMemberOffs;

			::SymGetTypeInfo(hProcess, dwModBase, dwChildId, TI_GET_SYMNAME, &pwszTypeName);

			// std::string handling
			if (bStd && pParams->bTrimSTDTypes && wcscmp(pwszTypeName, L"_Ptr"))
			{
				continue;
			}

			CCallstackFrame::LocalVarInfo childVarInfo;

			::SymGetTypeInfo(hProcess, dwModBase, dwChildId, TI_GET_SYMTAG, &childVarInfo.SymTag);
			::SymGetTypeInfo(hProcess, dwModBase, dwChildId, TI_GET_OFFSET, &dwMemberOffs);
			::SymGetTypeInfo(hProcess, dwModBase, dwChildId, TI_GET_TYPEID, &dwTypeId);
			::SymGetTypeInfo(hProcess, dwModBase, dwTypeId, TI_GET_LENGTH, &childVarInfo.dwValueSize);

			childVarInfo.bParameter = false;
			childVarInfo.sName = pwszTypeName ? pwszTypeName : L"";
			childVarInfo.LocationType = LocIsThisRel;
			childVarInfo.pValue = (childVarInfo.SymTag == SymTagData) ? ((LPBYTE)pVarInfo->pValue + dwMemberOffs) : NULL;
			childVarInfo.dwTypeId = dwTypeId;

			if (childVarInfo.SymTag == SymTagData)
			{
				DumpTypeIndex(&childVarInfo, dwModBase, dwTypeId, dwOffs, pParams);
				pVarInfo->children.push_back(childVarInfo);
			}
		}
	}
	catch (...)
	{
	}
}

wstring CompactTypeName(const wstring& sTypeName)
{
	try
	{
		if (sTypeName.length() < 30)
		{
			return sTypeName;
		}

		size_t nLeftBracketIndex = sTypeName.find(L'<');
		if (nLeftBracketIndex != -1)
		{
			return sTypeName.substr(0, nLeftBracketIndex) + L"<...>";
		}
	}
	catch (...)
	{
	}

	return sTypeName;
}

}
