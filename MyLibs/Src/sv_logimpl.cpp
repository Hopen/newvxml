/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include "sv_utils.h"
#include "sv_strutils.h"
#include "sv_logimpl.h"
#include "ce_xml.hpp"

namespace SVLog
{

/************************************************************************/
/* CLogFile class implementation                                        */
/************************************************************************/

CLogFile::CLogFile()
{
	m_Splitter.SetBinaryWriter(new CSimpleWriter());
	InitDefault(
		(SVUtils::GetModuleFileName() + L".log").c_str(),
		CStdLogFormatter::ptDate	|
		CStdLogFormatter::ptTime	|
		CStdLogFormatter::ptPID		|
		CStdLogFormatter::ptTID);
}

bool CLogFile::InitDefault(LPCWSTR pwszFileNameFormat, DWORD dwPrefixFlags)
{
	Destroy();
	
	m_Splitter.AddSplitCondition(new CDaySplitCondition());
	m_Splitter.SetNameGenerator(new CDateTimeNameGenerator(pwszFileNameFormat));
	m_Formatter.SetPrefix(dwPrefixFlags);
	
	return true;
}

bool CLogFile::InitFromXML(LiteXML::TElem& root)
{
	LiteXML::TElem config;

	struct TimeParser
	{
		static FILETIME Parse(LPCWSTR pwszTime)
		{
			SYSTEMTIME st = {0};
			FILETIME ft = {0};
			int nHour = 0, nMin = 0, nSec = 0;

			swscanf(pwszTime, L"%02d:%02d:%02d", &nHour, &nMin, &nSec);
			::GetLocalTime(&st);

			st.wHour = (WORD)nHour;
			st.wMinute = (WORD)nMin;
			st.wSecond = (WORD)nSec;
			st.wMilliseconds = 0;

			::SystemTimeToFileTime(&st, &ft);

			return ft;
		}
	};

	Destroy();
	LiteXML::TElem splittersRoot = root.subnode(L"Splitters");

	for (LiteXML::TElem el = splittersRoot.begin(); el != splittersRoot.end(); el++)
	{
		if (!el.attrBool(L"Active", false))
		{
			continue;
		}

		if (el.name() == L"TimeOfTheDay")
		{
			std::wstring val = el.attr(L"Value");
			m_Splitter.AddSplitCondition(new CTimeOfDaySplitCondition(TimeParser::Parse(val.c_str())));
		}
		else if (el.name() == L"SizeLimit")
		{
			ULONGLONG nSize = 0;
			std::wstring val = el.attr(L"Value");
			swscanf(val.c_str(), L"%I64u", &nSize);
			if (nSize)
			{
				m_Splitter.AddSplitCondition(new CSizeLimitSplitCondition(nSize));
			}
		}
		else if (el.name() == L"TimeSpan")
		{
			std::wstring val = el.attr(L"Value");
			m_Splitter.AddSplitCondition(new CTimeSpanSplitCondition(TimeParser::Parse(val.c_str())));
		}
		else if (el.name() == L"Day")
		{
			m_Splitter.AddSplitCondition(new CDaySplitCondition());
		}
	}

	// filename generator
	std::wstring sFormat = root.attr(L"FileNameGenerator/Format");
	if (!sFormat.length())
	{
		return false;
	}

	WCHAR wszBuf[MAX_PATH] = {0};
	::ExpandEnvironmentStringsW(sFormat.c_str(), wszBuf, MAX_PATH);
	m_Splitter.SetNameGenerator(new CDateTimeNameGenerator(wszBuf));

	LiteXML::TElem fmtRoot = root.subnode(L"Format");
	DWORD dwPrefixFlags = 0;

	for (LiteXML::TElem el = fmtRoot.begin(); el != fmtRoot.end(); el++)
	{
		if (el.name() == L"Prefix")
		{
			std::wstring val = el.attr(L"Value");
			if (val == L"Date")			dwPrefixFlags |= CStdLogFormatter::ptDate;
			else if (val == L"Time")	dwPrefixFlags |= CStdLogFormatter::ptTime;
			else if (val == L"PID")		dwPrefixFlags |= CStdLogFormatter::ptPID;
			else if (val == L"TID")		dwPrefixFlags |= CStdLogFormatter::ptTID;
		}
	}

	m_Formatter.SetPrefix(dwPrefixFlags ? dwPrefixFlags : DEF_PREFIX);

	return true;
}

void CLogFile::Destroy()
{
	m_Splitter.Close();
}

void CLogFile::Log(LPCWSTR pwszFmt, ...)
{
	va_list	args;
	va_start(args, pwszFmt);
	LogV(pwszFmt, args);
	va_end(args);
}

void CLogFile::LogV(LPCWSTR pwszFmt, va_list args)
{
	try
	{
		CStdLogFormatter fmt(m_Formatter);
		fmt.WritePrefix();
		fmt.FormatV(pwszFmt, args);
		fmt.WriteEndl();
//		fmt.WriteConsole();
		m_Splitter.Write(&fmt);
	}
	catch (...)
	{
	}
}

void CLogFile::LogOSErr(LPCWSTR pwszFmt)
{
	DWORD dwErr = GetLastError();
	Log(L"%s (0x%p,%s)", 
		pwszFmt,
		dwErr, 
		SVUtils::WinErrToStr(dwErr).c_str());
}

void CLogFile::LogException(LPCSTR	pwszFile, 
							int		nLine, 
							LPCSTR	pwszFunc, 
							const	SVExceptions::CException& e)
{
	Log(L"Exception in File: %S, Line: %d, Func: %S. %s", pwszFile, nLine, pwszFunc, e.Format().c_str());
}

void CLogFile::ClsInfoLog(LPCSTR pszClassInfo, LPCWSTR pwszFmt, ...)
{
	va_list	args;
	va_start(args, pwszFmt);
	ClsInfoLogV(pszClassInfo, pwszFmt, args);
	va_end(args);
}

void CLogFile::ClsInfoLogV(LPCSTR pszClassInfo, LPCWSTR pwszFmt, va_list args)
{
	LogV((FormatClassInfo(pszClassInfo) + pwszFmt).c_str(), args);
}

void CLogFile::ClsInfoLogOSErr(LPCSTR pszClassInfo, LPCWSTR pwszFmt, ...)
{
	va_list	args;
	va_start(args, pwszFmt);
	DWORD dwErr = GetLastError();
	Log(L"%s%s (0x%p,%s)", 
		FormatClassInfo(pszClassInfo).c_str(),
		formatv_wstring(pwszFmt, args).c_str(),
		dwErr, 
		SVUtils::WinErrToStr(dwErr).c_str());
	va_end(args);
}

void CLogFile::ClsInfoLogException(LPCSTR	pwszFile, 
								   int		nLine, 
								   LPCSTR	pwszFunc, 
								   const	SVExceptions::CException& e)
{
	Log(L"%sException in File: %S, Line: %d, Func: %S. %s", 
		FormatClassInfo(pwszFunc).c_str(), 
		pwszFile, nLine, pwszFunc, e.Format().c_str());
}

void CLogFile::RawLog(LPCWSTR pwszFmt, ...)
{
	try
	{
		CStdLogFormatter fmt(m_Formatter);
		va_list	args;
		va_start(args, pwszFmt);
		fmt.FormatV(pwszFmt, args);
		va_end(args);
		fmt.WriteEndl();
		m_Splitter.Write(&fmt);
	}
	catch (...)
	{
	}
}

std::wstring CLogFile::FormatClassInfo(LPCSTR pszClassInfo)
{
	// take only last <id>::<id>
	size_t len = strlen(pszClassInfo);
	const char* p = pszClassInfo + len - 1;
	while (p > pszClassInfo && *p != ':') p--;
	if (p > pszClassInfo) p -= 2;
	while (p > pszClassInfo && *p != ':') p--;
	if (p > pszClassInfo) p++;

	wchar_t wszBuf[1024] = {0};
	swprintf(wszBuf, L"[%-40S] ", p);
	return std::wstring(wszBuf);
}

/************************************************************************/
/* CDebugLogger class implementation                                    */
/************************************************************************/

CDebugLogger::CDebugLogger(const char* pszFileName)
{
	fp = fopen(pszFileName, "at");
}

void CDebugLogger::Log(const char* pszFmt, ...)
{
	if (!fp)
	{
		return;
	}

	SYSTEMTIME st;
	::GetLocalTime(&st);
	va_list	args;
	va_start(args, pszFmt);
	fprintf(
		fp, "[%02d/%02d/%04d %02d:%02d:%02d.%03d] ", 
		st.wDay, st.wMonth, st.wYear, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	vfprintf(fp, pszFmt, args);
	_fputchar('\n');
	va_end(args);
}

}
