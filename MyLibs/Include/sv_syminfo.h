/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <TlHelp32.h>

namespace SVSymInfo
{

using namespace std;

/************************************************************************/
/* ������ ��� ������ � ����������� � ��������                           */
/************************************************************************/

struct CSymInfo
{
	wstring	sName;
	DWORD	dwSize;
	DWORD	dwOffs;
	CSymInfo() :
	dwSize(0), dwOffs(0)
	{
	}
	bool Init(HANDLE hProcess, PVOID pAddr);
};

struct CLineInfo
{
	wstring		sFileName;
	DWORD		dwLine;
	DWORD		dwOffs;
	CLineInfo() :
	dwLine(0), dwOffs(0)
	{
	}
	bool Init(HANDLE hProcess, PVOID pAddr);
};

typedef	const IMAGE_SECTION_HEADER* PCIMAGE_SECTION_HEADER;

struct CModuleInfo
{
	DWORD		dwImageBase;
	DWORD		dwImageSize;
	wstring		sModuleName;
	wstring		sImageName;
	wstring		sFileName;

	CModuleInfo() :
		dwImageBase(0), dwImageSize(0)
	{
	}

	bool Init(HANDLE hProcess, PVOID pAddr);
	PCIMAGE_SECTION_HEADER GetSection(int nSection) const;
	int	GetSectionCount() const;
	PCIMAGE_SECTION_HEADER GetSectionByAddress(PVOID pAddr) const;
};

/************************************************************************/
/* ��������������� �������                                              */
/************************************************************************/

DWORD SymLoadModuleByAddr(PVOID pAddr, HANDLE hProcess);
MODULEENTRY32W GetModuleInfoByAddress(PVOID pAddr, HANDLE hProcess);

}
