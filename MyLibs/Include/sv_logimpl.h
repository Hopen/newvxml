/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_log.h"
#include "sv_exceptions.h"
#include "ce_xml.hpp"

namespace SVLog
{

const DWORD DEF_PREFIX = 	
	CStdLogFormatter::ptDate	|
	CStdLogFormatter::ptTime	|
	CStdLogFormatter::ptPID		|
	CStdLogFormatter::ptTID;

class CLogFile
{
private:
	CStdLogFormatter		m_Formatter;
	CBinarySplitter			m_Splitter;

	CLogFile(const CLogFile&) { }

public:
	CLogFile();
	~CLogFile()
	{
		Destroy();
	}

	bool InitDefault(LPCWSTR pwszFileNameFormat, DWORD dwPrefixFlags = DEF_PREFIX);
	bool InitFromXML(LiteXML::TElem& config);
	
	void Destroy();

	void Log(LPCWSTR pwszFmt, ...);
	void LogV(LPCWSTR pwszFmt, va_list args);
	void LogOSErr(LPCWSTR pwszFmt);
	void LogException(LPCSTR pwszFile, int nLine, LPCSTR pwszFunc, const SVExceptions::CException& e);

	void ClsInfoLog(LPCSTR pszClassInfo, LPCWSTR pwszFmt, ...);
	void ClsInfoLogV(LPCSTR pszClassInfo, LPCWSTR pwszFmt, va_list args);
	void ClsInfoLogOSErr(LPCSTR pszClassInfo, LPCWSTR pwszFmt, ...);
	void ClsInfoLogException(LPCSTR pwszFile, int nLine, LPCSTR pwszFunc, const SVExceptions::CException& e);

	void RawLog(LPCWSTR pwszFmt, ...);
	
	static std::wstring FormatClassInfo(LPCSTR pszClassInfo);
};

class CDebugLogger
{
private:
	FILE* fp;

public:
	CDebugLogger(const char* pszFileName);
	void Log(const char* pszFmt, ...);
};

}
