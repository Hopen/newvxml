/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

namespace SVCrypt
{

void SHA1(const void* pData, unsigned nSize, void* pHash);
void MD5(const void* pData, unsigned nSize, void* pHash);

}
