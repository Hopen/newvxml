/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

namespace SVHandle
{

/************************************************************************/
/* ����� CBasicHandle - ����� ��������� Win32                           */
/************************************************************************/

template <class HNDL, BOOL (WINAPI *CloseFunc)(HNDL)>
class CBasicHandle
{
protected:
	struct Handle
	{
		HNDL	hHandle;
		DWORD	dwRefCount;
		Handle(HNDL _hHandle = NULL)
		{
			hHandle = _hHandle;
			dwRefCount = 1;
		}
		~Handle()
		{
			if (*this)
			{
				CloseFunc(hHandle);
			}
		}
		operator bool() const
		{
			return hHandle && hHandle != INVALID_HANDLE_VALUE;
		}
		void AddRef()
		{
			dwRefCount++;
		}
		void Release()
		{
			if (!--dwRefCount)
			{
				delete this;
			}
		}
	};

	void Release()
	{
		if (m_pHandle)
		{
			m_pHandle->Release();
			m_pHandle = NULL;
		}
	}

	Handle* m_pHandle;

public:
	CBasicHandle(HNDL hHandle = NULL)
	{
		m_pHandle = hHandle ? new Handle(hHandle) : NULL;
	}
	CBasicHandle(const CBasicHandle& handle)
	{
		m_pHandle = NULL;
		*this = handle;
	}
	~CBasicHandle()
	{
		Release();
	}
	void operator =(HNDL hHandle)
	{
		Attach(hHandle);
	}
	const CBasicHandle& operator =(const CBasicHandle& handle)
	{
		if (m_pHandle != handle.m_pHandle)
		{
			Release();
			m_pHandle = handle.m_pHandle;
			if (m_pHandle)
			{
				m_pHandle->AddRef();
			}
		}
		return *this;
	}
	operator bool() const
	{
		return m_pHandle ? ((bool)*m_pHandle) : NULL;
	}
	bool operator !() const
	{
		return !operator bool();
	}
	operator HNDL() const
	{
		return m_pHandle->hHandle;
	}
	HNDL* operator &()
	{
		Close();
		m_pHandle = new Handle();
		return &m_pHandle->hHandle;
	}
	bool IsValid() const
	{
		return operator bool();
	}
	void Close()
	{
		Release();
	}
	CBasicHandle& Attach(HNDL hHandle)
	{
		Release();
		m_pHandle = hHandle ? new Handle(hHandle) : NULL;
		return *this;
	}
	HNDL Detach()
	{
		if (!m_pHandle)
		{
			return NULL;
		}
		HNDL h = m_pHandle->hHandle;
		Release();
		return h;
	}
};

/************************************************************************/
/* ����� CBasicHandle - ��������� ������� ���� Win32                    */
/************************************************************************/

class CHandle
{
private:
	HANDLE m_hHandle;

public:
	CHandle(HANDLE hHandle = NULL, bool bDuplicate = false)
	{
		if (bDuplicate)
		{
			DuplicateHandle(
				GetCurrentProcess(), 
				hHandle, 
				GetCurrentProcess(), 
				&m_hHandle, 
				0, 
				FALSE, 
				DUPLICATE_SAME_ACCESS);
		}
		else
		{
			m_hHandle = hHandle;
		}
	}
	CHandle(const CHandle& handle)
	{
		m_hHandle = NULL;
		*this = handle;
	}
	~CHandle()
	{
		Close();
	}
	HANDLE operator =(HANDLE hHandle)
	{
		Attach(hHandle);
		return m_hHandle;
	}
	const CHandle& operator =(const CHandle& handle)
	{
		if (this != &handle)
		{
			Attach(handle.Duplicate());
		}
		return *this;
	}
	operator bool() const
	{
		return m_hHandle && m_hHandle != INVALID_HANDLE_VALUE;
	}
	bool operator !() const
	{
		return !operator bool();
	}
	operator HANDLE() const
	{
		return m_hHandle;
	}
	HANDLE* operator &()
	{
		if (*this)
		{
			return NULL;
		}
		return &m_hHandle;
	}
	bool IsValid() const
	{
		return operator bool();
	}
	void Close()
	{
		if (*this)
		{
			::CloseHandle(m_hHandle);
			m_hHandle = NULL;
		}
	}
	CHandle& Attach(HANDLE hHandle)
	{
		if (m_hHandle != hHandle)
		{
			Close();
			m_hHandle = hHandle;
		}
		return *this;
	}
	HANDLE Detach()
	{
		HANDLE hHandle = m_hHandle;
 		Close();
		return hHandle;
	}
	HANDLE Duplicate(DWORD dwDesiredAccess = 0,
					 BOOL bSameAccess = TRUE,
					 BOOL bInherit = FALSE, 
					 HANDLE hTargetProcess = GetCurrentProcess()) const
	{
		HANDLE hHandle = NULL;
		::DuplicateHandle(
			GetCurrentProcess(), 
			m_hHandle, 
			hTargetProcess, 
			&hHandle, 
			dwDesiredAccess, 
			bInherit, 
			bSameAccess ? DUPLICATE_SAME_ACCESS : 0);
		return hHandle;
	}
};

inline BOOL WINAPI _RegCloseKey(HKEY hKey)
{
	return (::RegCloseKey(hKey) == ERROR_SUCCESS) ? TRUE : FALSE;
}

typedef CBasicHandle<SC_HANDLE,::CloseServiceHandle> CSvcHandle;
typedef CBasicHandle<HANDLE,::FindClose> CFindHandle;
typedef CBasicHandle<HKEY,_RegCloseKey> CRegKeyHandle;

}

