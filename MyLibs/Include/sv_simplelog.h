/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

namespace SVLog
{

class CSimpleLogFile
{
private:
	WCHAR				m_wszFileNameFormat[MAX_PATH];
	HANDLE				m_hFile;
	CRITICAL_SECTION	m_CS;
	DWORD				m_dwPID;
	SYSTEMTIME			m_stLastSplit;
	bool				m_bLogFunc;

	CSimpleLogFile(const CSimpleLogFile&);
	void operator =(const CSimpleLogFile&);

	bool Create();
	void LogOSErr(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args, DWORD dwErr, LPCWSTR pwszDescr);

public:
	CSimpleLogFile();
	~CSimpleLogFile();

	bool Init(LPCWSTR pwszFileNameFormat, bool bLogFunc = false);
	bool InitFromXML(LPCWSTR pwszXMLFile);
	void Destroy();

	void Log(LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...);
	void LogV(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args);
	void LogOSErr(LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...);
//	void LogException(LPCWSTR pwszInFunc, LPCSTR pwszFile, int nLine, LPCSTR pwszFunc, const SVExceptions::CException& e);

	void LogV(LPCWSTR pwszPrefix, LPCWSTR pwszFunc, LPCWSTR pwszFormat, va_list prefixArgs, va_list formatArgs);	
	void Write(LPCSTR pszInfo, DWORD dwLen);
};

}
