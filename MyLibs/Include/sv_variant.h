/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#ifndef _CVARIANT_H_
#define _CVARIANT_H_

#include <math.h>
#include <string>

using namespace std;


namespace SVVariant {


__int64 VariantToInt(const VARIANT& v);
double VariantToFloat(const VARIANT& v);
bool VariantToBool(const VARIANT& v);
wstring VariantToString(const VARIANT& v);
void IntToVariantRef(VARIANT& v, __int64 val);
void FloatToVariantRef(VARIANT& v, double val);
void StringToVariantRef(VARIANT& v, LPCWSTR val);

void VariantTypeToString(WORD vt, LPWSTR pwszType, int cchType);
void VariantValueToString(const VARIANT& vt, LPWSTR pwszValue, int cchValue);
void VariantToString(const VARIANT& vt, LPWSTR pwszBuf, int cchBuf);
inline HRESULT VariantCopyEx(VARIANTARG* pvargDest, VARIANTARG* pvargSrc);
HRESULT VariantCopyByRef(VARIANTARG* pvargDest, VARIANTARG* pvargSrc);

inline wstring Variant2wstring(const VARIANT& vt)
{
	WCHAR szBuf[4096];
	VariantToString(vt, szBuf, sizeof(szBuf) / sizeof(WCHAR));
	return wstring(szBuf);
}

inline HRESULT VariantCopyEx(VARIANTARG* pvargDest, VARIANTARG* pvargSrc)
{
	VariantClear(pvargDest);
	switch (pvargSrc->vt)
	{
		case VT_I8:
		case VT_UI8:
			CopyMemory(pvargDest,pvargSrc,sizeof(VARIANT));
			return S_OK;
		default:
			return ::VariantCopy(pvargDest,pvargSrc);
	}
}

// CVariant class

class CVariant
{
private:
	VARIANT m_v;

public:
	CVariant()
	{
		VariantInit(&m_v);
	}
	CVariant(const CVariant& v)
	{
		Value = v.Value;
	}
	template <class T>
	CVariant(T val)
	{
		VariantInit(&m_v);
		*this = val;
	}
	const CVariant& operator =(VARIANT& v)
	{
		Value = v;
		return *this;
	}
	const CVariant& operator =(int iVal)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_INT);
		m_v.intVal = iVal;
		return *this;
	}
	const CVariant& operator =(__int64 i64Val)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_I8);
		m_v.llVal = i64Val;
		return *this;
	}
	const CVariant& operator =(float fVal)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_R4);
		m_v.fltVal = fVal;
		return *this;
	}
	const CVariant& operator =(double dVal)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_R8);
		m_v.dblVal = dVal;
		return *this;
	}
	const CVariant& operator =(bool bVal)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_BOOL);
		m_v.boolVal = bVal;
		return *this;
	}
	const CVariant& operator =(PCWSTR pszStr)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_BSTR);
		m_v.bstrVal = SysAllocString(pszStr);
		return *this;
	}
	const CVariant& operator =(IDispatch* disp)
	{
		VariantChangeType(&m_v, &m_v, 0, VT_DISPATCH);
		m_v.pdispVal = disp;
		return *this;
	}
	operator VARIANT() const
	{
		return m_v;
	}
	~CVariant()
	{
		VariantClear(&m_v);
	}
	operator int()
	{
		return (int)AsInt();
	}
	operator LPCWSTR()
	{
		return AsString();
	}
	operator double()
	{
		return AsFloat();
	}
	operator __int64()
	{
		return AsInt();
	}
	operator bool()
	{
		return AsBool();
	}
	__int64 AsInt() const
	{
		return VariantToInt(m_v);
	}
	INT8 AsInt8() const { return (INT8)AsInt(); }
	INT16 AsInt16() const { return (INT16)AsInt(); }
	INT32 AsInt32() const { return (INT32)AsInt(); }
	INT64 AsInt64() const { return (INT64)AsInt(); }
	UINT8 AsUInt8() const { return (UINT8)AsInt(); }
	UINT16 AsUInt16() const { return (UINT16)AsInt(); }
	UINT32 AsUInt32() const { return (UINT32)AsInt(); }
	UINT64 AsUInt64() const { return (UINT64)AsInt(); }
	double AsDouble() const
	{
		return VariantToFloat(m_v);
	}
	float AsFloat() const { return (float)AsDouble(); }
	bool AsBool() const
	{
		return VariantToBool(m_v);
	}
	PCWSTR AsString() const
	{
		if (m_v.vt != VT_BSTR)
		{
			throw 0;
		}
		return (PCWSTR)m_v.bstrVal;
	}
	FILETIME AsDateTime()
	{
		switch (m_v.vt)
		{
			case VT_DATE:
				{
					SYSTEMTIME st;
					FILETIME ft;
					VariantTimeToSystemTime(m_v.date, &st);
					SystemTimeToFileTime(&st, &ft);
					return ft;
				}
			case VT_BSTR:
				{
					double date;
					SYSTEMTIME st;
					FILETIME ft;
					if (FAILED(VarDateFromStr(
						const_cast<wchar_t*>(m_v.bstrVal), 
						LANG_USER_DEFAULT, 
						0, 
						&date)))
					{
						FILETIME ft = {0,0};
						return ft;
					}
					VariantTimeToSystemTime(date, &st);
					SystemTimeToFileTime(&st, &ft);
					return ft;
				}
		}
		FILETIME ft;
		*(__int64*)&ft = AsInt();
		return ft;
	}
	LARGE_INTEGER AsCurrency()
	{
		LARGE_INTEGER val;
		switch (m_v.vt)
		{
			case VT_CY:
				val.LowPart = LONG(m_v.cyVal.int64 % 10000);
				val.HighPart = LONG(m_v.cyVal.int64 / 10000 / 100);
				return val;
			default:
/*			case VT_R4:
				val.HighPart = LONG(floor(m_v.fltVal));
				val.LowPart = LONG(floor(100 * (m_v.fltVal - val.HighPart) + 0.5));
				return val;
			case VT_R8:
				val.HighPart = LONG(floor(m_v.dblVal));
				val.LowPart = LONG(floor(100 * (m_v.dblVal - val.HighPart) + 0.5));
				return val;
			case VT_BSTR:*/
				{
					double v = AsFloat();
					val.HighPart = LONG(floor(v));
					val.LowPart = LONG(floor(100 * (v - val.HighPart) + 0.5));
				}
				return val;
		}
		val.QuadPart = AsInt();
		return val;
	}
	IDispatch* AsIDispatch()
	{
		if (m_v.vt != VT_DISPATCH)
		{
			throw 0;
		}
		return m_v.pdispVal;
	}
	bool IsInteger() const
	{
		switch (m_v.vt)
		{
			case VT_UI1:
			case VT_I2:
			case VT_I4:
			case VT_I1:
			case VT_UI2:
			case VT_UI4:
			case VT_INT:
			case VT_UINT:
			case VT_I8:
			case VT_UI8:
				return true;
		}
		return false;
	}
	bool IsFloat() const
	{
		switch (m_v.vt)
		{
			case VT_R4:
			case VT_R8:
				return true;
		}
		return false;
	}
	bool IsBool() const
	{
		return m_v.vt == VT_BOOL || IsInteger();
	}
	bool IsString() const
	{
		return m_v.vt == VT_BSTR;
	}
	bool IsIDispatch() const
	{
		return m_v.vt == VT_DISPATCH;
	}
	const VARIANT& GetValue() const
	{
		return m_v;
	}
	void SetValue(const VARIANT& v)
	{
		VariantCopyEx(&m_v, (VARIANT*)&v);
	}
	__declspec(property(get=GetValue,put=SetValue)) VARIANT Value;
};

}


#endif // _CVARIANT_H_
