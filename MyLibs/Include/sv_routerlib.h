/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <map>
#include "CGRouterClient.h"
#include "EvtModel.h"

namespace SVRouterLib
{

/************************************************************************/
/* CRouterClient class - abstract router client                         */
/************************************************************************/

class CRouterClient : private CRouterMsgHandler, public CRouterConnector
{
private:
	typedef	std::map<DWORD, std::wstring> EVENTHASHMAP;
	
	EVENTHASHMAP		m_EventHashMap;

private:
	// CRouterMsgHandler methods
	virtual void NotifyRead(CRouterConnector* pConnector, CMessage& Msg, CLIENT_ADDRESS From, CLIENT_ADDRESS To);
	virtual void NotifyWrite(CRouterConnector *pConnector, CMessage &Msg, CLIENT_ADDRESS From, CLIENT_ADDRESS To);
	virtual void NotifyDeliverError(CRouterConnector* pConnector, CMessage& Msg, CLIENT_ADDRESS* pTo);
	virtual void NotifyConnect(CRouterConnector* pConnector, LPCWSTR pwszHost, WORD wPort);
	virtual void NotifyDisconnect(CRouterConnector* pConnector, bool& bConnect);
	virtual void NotifyError(CRouterConnector* pConnector, HRESULT hError);
	virtual void NotifyStatusChange(CRouterConnector* pConnector, CLIENT_ADDRESS client, int nStatus);
	virtual void NotifyEventSubscribe(CRouterConnector* pConnector, DWORD dwEventHash, CLIENT_ADDRESS client, int nStatus, int nConsumers);
	virtual void NotifyMonitorEvent(CRouterConnector* pConnector, CMessage& Msg, CLIENT_ADDRESS* pFrom, CLIENT_ADDRESS* pTo);

protected:
	virtual void OnReceive(CMessage &Msg, CLIENT_ADDRESS From, CLIENT_ADDRESS To) = NULL;
	virtual void OnConnect(LPCWSTR pwszHost, WORD wPort) = NULL;
	virtual void OnDisconnect(bool &bConnectAgain) = NULL;
	virtual void OnError(HRESULT hr, LPCWSTR pwszDescr) = NULL;
	virtual void OnStatusChange(CLIENT_ADDRESS client, int nStatus) = NULL;
	virtual void OnEventSubscribe(DWORD dwEventHash, CLIENT_ADDRESS client, int nStatus, int nConsumers) = NULL;

protected:
	// Helper methods
	LPCWSTR ClientStatusToString(int nStatus);
	LPCWSTR EventStatusToString(int nStatus);
	CLIENT_ADDRESS ClientToAddr(DWORD dwClient);

	// Logs
	void LogFromTo(LPCWSTR pwszFunc, CLIENT_ADDRESS From, CLIENT_ADDRESS To, const CMessage& Msg, bool bRead, DWORD dwRes);
	virtual void Log(LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...)
	{
	}

public:
	CRouterClient();
	~CRouterClient();

	DWORD RegisterEvent(LPCWSTR pwszEvent);
	std::wstring EventByHash(DWORD dwHash);

	void Connect();
	void EventSubscribe(LPCWSTR pszEvent);
	void EventUnsubscribe(LPCWSTR pszEvent);

public:
	// Send methods
	DWORD SendTo(CLIENT_ADDRESS To, const CMessage& Msg);
	DWORD SendFromTo(CLIENT_ADDRESS From, CLIENT_ADDRESS To, const CMessage& Msg);
	DWORD SendFromToAny(CLIENT_ADDRESS From, const CMessage& Msg);
	DWORD SendFromToAll(CLIENT_ADDRESS From, const CMessage& Msg);
	DWORD SendToAny(const CMessage& Msg, const CMessage* pAck);
	DWORD SendToAll(const CMessage& Msg);
	bool ConnectorThreadActive() const;
};

/************************************************************************/
/* CRouterClientDelegate - delegate router client                       */
/************************************************************************/

class CRouterClientDelegate : public CRouterClient
{
private:
	typedef void (__cdecl *MsgHandler)(CMessage& inMsg, CLIENT_ADDRESS From, CLIENT_ADDRESS To);
	typedef void (__cdecl *ConnectedHandler)(LPCWSTR pHost, WORD wPort);
	typedef void (__cdecl *DisconnectedHandler)(bool& bConnect);
	typedef void (__cdecl *StatusChangeHandler)(CLIENT_ADDRESS client, int nStatus);
	typedef void (__cdecl *SubscribeHandler)(DWORD dwEventHash, CLIENT_ADDRESS client, int nCount, int nStatus);
	typedef void (__cdecl *ReceiveHandler)(CMessage &Msg, CLIENT_ADDRESS From, CLIENT_ADDRESS To);

	struct MsgHandlerEx
	{
		MsgHandler	pHandler;
		bool		bAutoSubscribe;
	};

	typedef std::map<std::wstring, MsgHandlerEx> HANDLERSMAP;

private:
	virtual void OnReceive(CMessage &Msg, CLIENT_ADDRESS From, CLIENT_ADDRESS To);
	virtual void OnConnect(LPCWSTR pwszHost, WORD wPort);
	virtual void OnDisconnect(bool &bConnectAgain);
	virtual void OnError(HRESULT hr, LPCWSTR pwszDescr);
	virtual void OnStatusChange(CLIENT_ADDRESS client, int nStatus);
	virtual void OnEventSubscribe(DWORD dwEventHash, CLIENT_ADDRESS client, int nStatus, int nConsumers);

private:
	ConnectedHandler	m_ConnectedHandler;
	DisconnectedHandler	m_DisconnectedHandler;
	StatusChangeHandler	m_StatusChangeHandler;
	SubscribeHandler	m_SubscribeHandler;
	ReceiveHandler		m_ReceiveHandler;

	HANDLERSMAP			m_HandlersMap;

public:
	CRouterClientDelegate() :
		m_ConnectedHandler(NULL),
		m_DisconnectedHandler(NULL),
		m_StatusChangeHandler(NULL),
		m_SubscribeHandler(NULL),
		m_ReceiveHandler(NULL)
	{
	}

	void RegisterNamedReceiveHandler(LPCWSTR pszName, MsgHandler pHandler, bool bAutoSubscribe = true, bool bRegister = true);
	void RegisterConnectedHandler(ConnectedHandler pHandler, bool bRegister = true)
	{
		m_ConnectedHandler = bRegister ? pHandler : NULL;
	}
	void RegisterDisconnectedHandler(DisconnectedHandler pHandler, bool bRegister = true)
	{
		m_DisconnectedHandler = bRegister ? pHandler : NULL;
	}
	void RegisterReceiveHandler(ReceiveHandler pHandler, bool bRegister = true)
	{
		m_ReceiveHandler = bRegister ? pHandler : NULL;
	}
	void RegisterStatusChangeHandler(StatusChangeHandler pHandler, bool bRegister = true)
	{
		m_StatusChangeHandler = bRegister ? pHandler : NULL;
	}
	void RegisterEventSubscribe(SubscribeHandler pHandler, bool bRegister = true)
	{
		m_SubscribeHandler = bRegister ? pHandler : NULL;
	}
};

}
