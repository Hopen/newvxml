/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <list>
#include <TlHelp32.h>

namespace SVToolHelp32
{

/************************************************************************/
/* CModuleList - Module32First/Module32Next wrapper                     */
/************************************************************************/

typedef BOOL (__stdcall* tagLISTFN)(HANDLE, PVOID);
typedef tagLISTFN LISTFN;

template <class Type, DWORD Flags, LISTFN FirstFn, LISTFN NextFn>
class CToolHelp32List
{
public:
	typedef std::list<Type> LIST;

private:
	HANDLE	m_hSnapshot;

	bool CreateSnapshot(DWORD dwParam)
	{
		DestroySnapshot();
		return (m_hSnapshot = ::CreateToolhelp32Snapshot(
			Flags, 
			dwParam)) != INVALID_HANDLE_VALUE;
	}

	void DestroySnapshot()
	{
		if (m_hSnapshot && m_hSnapshot != INVALID_HANDLE_VALUE)
		{
			::CloseHandle(m_hSnapshot);
		}
		m_hSnapshot = NULL;
	}

public:
	CToolHelp32List()
	{
		m_hSnapshot = NULL;
	}
	~CToolHelp32List()
	{
		DestroySnapshot();
	}
	bool First(Type& entry, DWORD dwParam)
	{
		if (!CreateSnapshot(dwParam))
		{
			return false;
		}
		return FirstFn(m_hSnapshot, &entry) == TRUE;
	}
	bool Next(Type& entry)
	{
		if (!m_hSnapshot)
		{
			return false;
		}
		return NextFn(m_hSnapshot, &entry) == TRUE;
	}
	static LIST GetList(DWORD dwParam)
	{
		CToolHelp32List<Type, Flags, FirstFn, NextFn> ml;
		Type entry;
		LIST l;
		entry.dwSize = sizeof(entry);
		if (ml.First(entry, dwParam))
		{
			do {
				l.push_back(entry);
			} while(ml.Next(entry));
		}
		return l;
	}
};

typedef CToolHelp32List<MODULEENTRY32W,	TH32CS_SNAPMODULE,	(LISTFN)::Module32FirstW,	(LISTFN)::Module32NextW>	CModuleList;
typedef CToolHelp32List<THREADENTRY32,	TH32CS_SNAPTHREAD,	(LISTFN)::Thread32First,	(LISTFN)::Thread32Next>		CThreadList;
typedef CToolHelp32List<PROCESSENTRY32W,TH32CS_SNAPPROCESS,	(LISTFN)::Process32FirstW,	(LISTFN)::Process32NextW>	CProcessList;

}
