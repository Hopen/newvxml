/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <vector>
#include <string>

namespace SVCOMTypeInfo {

struct ParamInfo 
{
	DWORD dwFlags;
	DWORD dwType;
	DWORD dwSubType;
	std::wstring sName;
};

struct MethodInfo 
{
	DWORD dwFlags;
	DWORD dwRetValType;
	DWORD dwRetValSubType;
	std::wstring sName;
	MEMBERID memid;
	std::vector<ParamInfo> Params;
	std::wstring GetDesc(int nIndex, bool bIDLstyle) const;
};

class CCOMMethodsInfo
{
private:
	std::vector<MethodInfo*> m_Methods;

	void Destroy();

public:
	CCOMMethodsInfo(IDispatch* pDisp = NULL);
	CCOMMethodsInfo(BSTR bstrProgId);
	~CCOMMethodsInfo();
	HRESULT Init(IDispatch* pDisp);
	HRESULT Init(BSTR bstrProgId);
	int GetMethodCount() const;
	const MethodInfo* GetMethodInfo(int nIndex) const;
};

}
