/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

// Meyer singleton
template <typename CLASS>
class CMeyerSingleton
{
protected:
	CMeyerSingleton() { }
	~CMeyerSingleton() { }

public:
	static CLASS& Instance()
	{
		static CLASS s_Instance;
		return s_Instance;
	}
};

// TLS helper class
template <class T>
class CTlsIndex
{
private:
	DWORD m_dwTlsIndex;

public:
	CTlsIndex() : m_dwTlsIndex(::TlsAlloc())
	{
	}
	~CTlsIndex()
	{
		::TlsFree(m_dwTlsIndex);
	}
	T* GetValue()
	{
		return (T*)::TlsGetValue(m_dwTlsIndex);
	}
	void SetValue(T* pVal)
	{
		::TlsSetValue(m_dwTlsIndex, pVal);
	}
};

// TLS mapper
template <class T>
class TlsMapper : public CMeyerSingleton<TlsMapper<T> >
{
private:
	CTlsIndex<T> m_TlsIndex;

public:
	static T* Get()
	{
		return Instance().m_TlsIndex.GetValue();
	}
	static void Register(T* p)
	{
		Instance().m_TlsIndex.SetValue(p);
	}
	static void Unregister()
	{
		Instance().m_TlsIndex.SetValue(NULL);
	}
};
