/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include "sv_exhandlers.h"

#define CALLSTACK_FLAG_REGS		0x00000001
#define CALLSTACK_FLAG_VARS		0x00000002
#define CALLSTACK_FLAG_TIMES	0x00000004
#define CALLSTACK_FLAG_TIB		0x00000008
#define CALLSTACK_FLAG_ALL		0x0000000F

namespace SVDebug
{

using namespace std;

/************************************************************************/
/* ��������������� ������ � �������                                     */
/************************************************************************/

PVOID __cdecl GetEIP();
PVOID __cdecl GetESP();
PVOID __cdecl GetEBP();

PNT_TIB NtCurrentTIB();

void SetThreadName(LPCSTR pszThreadName, DWORD dwThreadID = ::GetCurrentThreadId());

wstring WriteMiniCallStack(PCONTEXT pContext, DWORD dwMaxDeep = 32);
wstring WriteMiniCallStackRemote(HANDLE hProcess, HANDLE hThread, DWORD dwMaxDeep = 32);

wstring WriteThreadCallStack(PCONTEXT pContext, 
							 DWORD dwFlags = 0,
							 DWORD dwThreadId = ::GetCurrentThreadId(),
							 DWORD dwProcessId = ::GetCurrentProcessId());
wstring WriteProcessSnapshot(DWORD dwPID = ::GetCurrentProcessId(), DWORD dwFlags = 0);
wstring WriteProcessSnapshot2(HANDLE hProcess = ::GetCurrentProcess(), DWORD dwFlags = 0);
wstring WriteAccessViolationInfo(PEXCEPTION_POINTERS pExc);
wstring WriteFullExceptionInfo(PEXCEPTION_POINTERS pExc, 
							   DWORD dwFlags = 0,
							   DWORD dwThreadId = ::GetCurrentThreadId(),
							   DWORD dwProcessId = ::GetCurrentProcessId());
wstring WriteModuleList(DWORD dwPID = ::GetCurrentProcessId(), bool bDetailed = false);
wstring WriteUptimes(DWORD dwPID = ::GetCurrentProcessId());
wstring WriteWinVer();

wstring DumpBasicRegisters(PCONTEXT pContext);
wstring DumpProcessException(PEXCEPTION_POINTERS pExc, 
							 DWORD dwFlags = 0,
							 DWORD dwTID = ::GetCurrentThreadId(), 
							 DWORD dwPID = ::GetCurrentProcessId(), 
							 bool bRunInThread = false);

template <class T>
wstring WriteMemoryDump(LPCVOID pDump,
						int nSize,
						LPCWSTR pwszFormat,
						int nLineSize = min(1, 16 / sizeof(T)),
						bool bWriteAddr = true,
						bool bWriteChar = false)
{
	LPBYTE pData = (LPBYTE)pDump;
	wstring sDump;
	LPWSTR pChars = new WCHAR[nLineSize * sizeof(T) + 1];

	try
	{
		for (; pData < (LPBYTE)pDump + nSize;)
		{
			if (bWriteAddr)
			{
				sDump += format_wstring(L"0x%p: ", pData);
			}

			LPBYTE pOldData = pData;

			for (int i = 0; (i < nLineSize) && (pData < (LPBYTE)pDump + nSize); pData += sizeof(T), i++)
			{
				sDump += format_wstring(pwszFormat, *(const T*)pData);
				if (i < nLineSize - 1)
				{
					sDump += L' ';
				}
			}

			if (bWriteChar)
			{
				wchar_t* q = pChars;
				for (LPBYTE p = pOldData; p < pData; p++)
				{
					*q++ = isgraph(*p) ? *p : '.';
				}
				*q = L'\0';
				sDump += wstring(L" ") + pChars;
			}

			sDump += L'\n';
		}
	}
	catch (...)
	{
	}

	delete pChars;
	return sDump;
}

void InstantContext(CONTEXT* pContext, DWORD dwSkipLevels = 0);
wstring InstantCallstack(DWORD dwFlags = CALLSTACK_FLAG_REGS | CALLSTACK_FLAG_VARS, DWORD dwSkipLevels = 0);

}
