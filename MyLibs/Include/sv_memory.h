/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

namespace SVMemory
{

template <class T>
bool IsBadReadPointer(const T* p)
{
	return ::IsBadReadPtr(p, sizeof(T)) != 0;
}

template <class T>
bool IsBadWritePointer(T* p)
{
	return ::IsBadWritePtr(p, sizeof(T)) != 0;
}

template <class T>
inline T ReadRemotePtr(HANDLE hProcess, LPCVOID pAddr)
{
	DWORD dwRead = 0;
	T val;
	ZeroMemory(&val, sizeof(val));
	if (!::ReadProcessMemory(hProcess, pAddr, &val, sizeof(val), &dwRead))
	{
		throw 0;
	}
	return val;
}

template <class Char>
bool ReadRemoteStr(HANDLE hProcess, LPCVOID pAddr, Char* pBuf, DWORD cbBuf)
{
	DWORD dwRead = 0;
	Char* pStr = NULL;
	ZeroMemory(pBuf, cbBuf);
	if (::ReadProcessMemory(hProcess, pAddr, &pStr, sizeof(pStr), &dwRead) == 0)
	{
		return false;
	}
	return ::ReadProcessMemory(hProcess, pStr, pBuf, cbBuf, &dwRead) != 0;
}

}
