// TestTranslator.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "translator.h"

LPVOID TRANS_API TranslatorCreate()
{
	try
	{
		CTranslator* pTrans = new CTranslator();
		if (pTrans)
			return static_cast<void*>(pTrans);
		return NULL;
	}
	catch (...)
	{
		return NULL;
	}
}

BOOL TRANS_API TranslatorDestory(LPCVOID pTranslator)
{
	try
	{
		const CTranslator* pTrans = reinterpret_cast<const CTranslator*>(pTranslator);
		delete pTrans;
		pTrans = 0;
	}
	catch (...)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL TRANS_API TranslatorTranslate(LPCVOID pTranslator,
	LPCTRANSLATORPARAMS pParams)
{
	try
	{
		const CTranslator* pTrans = reinterpret_cast<const CTranslator*>(pTranslator);
		pTrans->Translate(pParams);
	}
	catch (...)
	{
		return FALSE;
	}
	return TRUE;

}

DWORD TRANS_API TranslatorGetErrorCount(LPCVOID pTranslator)
{
	try
	{
		const CTranslator* pTrans = reinterpret_cast<const CTranslator*>(pTranslator);
		return pTrans->GetErrorCount();
	}
	catch (...)
	{
		return 0;
	}

	return 0;
}

BOOL TRANS_API TranslatorGetError(LPCVOID pTranslator, 
	ULONG nIndex, 
	LPTRANSLATORERROR pError)
{
	try
	{
		const CTranslator* pTrans = reinterpret_cast<const CTranslator*>(pTranslator);
		return pTrans->GetError(nIndex,pError);
	}
	catch (...)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL TRANS_API TranslatorGetErrorText(LPCVOID pTranslator, 
	ULONG nIndex, 
	LPWSTR pszBuf, 
	DWORD cbBuf)
{
	try
	{
		const CTranslator* pTrans = reinterpret_cast<const CTranslator*>(pTranslator);
		return pTrans->GetErrorText(nIndex,pszBuf,cbBuf);
	}
	catch (...)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL TRANS_API TranslatorGetCodeCompleteInfo(LPCODECOMPLETE pData)
{
	return FALSE;
}