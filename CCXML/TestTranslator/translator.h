/************************************************************************/
/* Name     : Translator\translator.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 28 Nov 2011                                               */
/************************************************************************/
#ifndef _TRANSLATOR_H
#define _TRANSLATOR_H

#pragma once

#include <vector>
#include "TranslatorExport.h"
#include "SAXXmlElementHandler.h"
#include "SAXXmlParser.h"
#include "EngineLog.h"
#include <boost/shared_ptr.hpp>

class CTranslator: public ISAXXmlElementHandler
{
	enum E_ERROR_TYPE
	{
		E_WARNING = 0,
		E_ERROR,
		E_FATAL
	};

	struct TError{
		int           line; 
		int           column; 
		std::wstring  errorText; 
		unsigned long errorCode;
		std::wstring  fileName;
		E_ERROR_TYPE  errLevel;
		std::wstring  formatErrText;

		TError(const int& _line, 
			   const int& _column, 
			   const std::wstring& _errorText, 
			   const unsigned long& _errorCode,
			   const std::wstring& _fileName,
			   const E_ERROR_TYPE& _level,
			   const std::wstring& _formatErrText)
		{
			line          = _line          ;
			column        = _column        ;
			errorCode     = _errorCode     ;
			errorText     = _errorText     ;
			fileName      = _fileName      ;
			errLevel      = _level         ;
			formatErrText = _formatErrText ;

		}
	};

	typedef std::vector <TError> ErrorQueue;

public:
	CTranslator();
	bool  Translate(LPCTRANSLATORPARAMS pParams)const;
	DWORD GetErrorCount()const;
	bool  GetError(ULONG nIndex, TRANSLATORERROR* pError)const;
	bool  GetErrorText(ULONG nIndex, LPWSTR pszBuf, DWORD cbBuf)const;

	// Handle XML content events during parsing.
	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
	virtual void OnXmlElementData(const std::wstring& elementData, int depth);
	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);

	// Handle XML error events during parsing.
	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);

	// Return true to stop parsing earlier.
	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);

private:

	bool File2File (LPCTRANSLATORPARAMS pParams)const;
	bool File2Mem  (LPCTRANSLATORPARAMS pParams)const;
	bool Mem2File  (LPCTRANSLATORPARAMS pParams)const;
	bool Mem2Mem   (LPCTRANSLATORPARAMS pParams)const;

	typedef bool (CTranslator::*pTR)(LPCTRANSLATORPARAMS pParams)const;

	void AddValidationSchema(const std::wstring& _filename)const;
	void ReadStream (const CComPtr<IStream>& _stream,void** _buff, DWORD& _size)const;
	void CheckFilePath(const std::wstring& _filepath)const;

private:
	//std::auto_ptr< CSAXXmlParser > m_xmlParser   ;
	boost::shared_ptr< CSAXXmlParser > m_xmlParser   ;
	//std::auto_ptr< CEngineLog	 > m_pLog        ;
	boost::shared_ptr< CEngineLog	 > m_pLog        ;
	mutable ErrorQueue             m_errors      ;

};
#endif // _TRANSLATOR_H

/******************************* eof *************************************/