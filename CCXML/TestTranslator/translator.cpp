/************************************************************************/
/* Name     : Translator\translator.cpp                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 28 Nov 2011                                               */
/************************************************************************/

#include "stdafx.h"
#include "translator.h"
#include "EngineConfig.h"
#include "utils.h"

using namespace enginelog;

/************************************************************************/
/*                         GLOBALS                                      */
/************************************************************************/
HINSTANCE g_hInst;
std::wstring gFileName;

class CTranslatorEngineConfig : public CEngineConfig <CTranslatorEngineConfig>
{
public:
	std::wstring m_sVXMLSchema;
	std::wstring m_sCCXMLSchema;
	std::wstring m_sEncoding;

protected:
	void LoadEngineConfig()
	{
		try
		{ 
			__super::LoadEngineConfig();
			//LiteXML::TElem config   = LiteXML::LoadXML(m_sFileName.c_str()); 
			this->m_sVXMLSchema		= m_config.attr(L"//configuration/Schema/VXML/Value");
			this->m_sCCXMLSchema	= m_config.attr(L"//configuration/Schema/CCXML/Value");
			this->m_sEncoding       = m_config.attr(L"//configuration/OutputFileEncoding/Value");
		} 
		catch (...) 
		{ 
		} 
	}

public:
	CTranslatorEngineConfig()
	{
		try
		{
			//m_sFileName = L"TranslatorEx.dll.config";
			CXmlConfigSingleton::LoadFromFile(m_sFileName);
			LoadEngineConfig();
		}
		catch (...)
		{
		}
	}
	~CTranslatorEngineConfig()
	{
	}
};

static std::wstring GetFileExtension(const std::wstring& cFileName)
{
	int pos = -1;
	WCHAR fileExt[MAX_PATH] ; ZeroMemory(fileExt ,sizeof(fileExt ));
	std::wstring filename = cFileName;
	if ((pos = filename.rfind('.')) != -1)
	{
		filename._Copy_s(fileExt,MAX_PATH,filename.length() - pos, pos);
	}
	return std::wstring (fileExt);
}

CTranslator::CTranslator()
{
	//m_xmlParser = std::auto_ptr< CSAXXmlParser > (new CSAXXmlParser());
	m_xmlParser .reset(new CSAXXmlParser());
	//m_xmlParser->AttachElementHandler(this);
	//m_pLog = std::auto_ptr< CEngineLog > (new CEngineLog());
	m_pLog  .reset(new CEngineLog());
	m_pLog->Init(CTranslatorEngineConfig::GetInstance()->sLevel.c_str(), CTranslatorEngineConfig::GetInstance()->sLogFileName.c_str());
}

void CTranslator::AddValidationSchema(const std::wstring& _filename)const
{
	std::wstring sSchema;
	std::wstring sFileExt(GetFileExtension(_filename));

	if ( (!wcscmp(sFileExt.c_str(),L".vxml") ) && (!wcscmp(sFileExt.c_str(),L".ccxml") ))
	{
		CAtlString sErr;
		sErr.Format(L"Error: cannot translate from \"*%s\" file, \"*.vxml/*.ccxml\" only", sFileExt.c_str());
		m_pLog->Log(LEVEL_WARNING, __FUNCTIONW__, sErr.GetString());

		TError err(0,0,sErr.GetString(),0,gFileName,E_FATAL,sErr.GetString());
		throw err;
	}

	if (!wcscmp(sFileExt.c_str(),L".vxml") )
		sSchema = CTranslatorEngineConfig::GetInstance()->m_sVXMLSchema;

	if (!wcscmp(sFileExt.c_str(),L".ccxml") )
		sSchema = CTranslatorEngineConfig::GetInstance()->m_sCCXMLSchema;

	if (sSchema.empty())
	{
		CAtlString sErr;
		sErr.Format(L"Error: \"*%s\" schema is not specified in config file", sFileExt.c_str());
		m_pLog->Log(LEVEL_WARNING, __FUNCTIONW__, sErr.GetString());

		TError err(0,0,sErr.GetString(),0,gFileName,E_FATAL,sErr.GetString());
		throw err;
	}
	CheckFilePath (sSchema);

	m_xmlParser->SetParserFeature(L"schema-validation", true);
	m_xmlParser->SetParserFeature(L"use-schema-location", false);

	sFileExt.erase(0,1); // erase "."
	std::wstring schema_namespace=std::wstring(L"x-schema:")+sFileExt;
	m_xmlParser->AddValidationSchema(schema_namespace, sSchema);
}

void CTranslator::ReadStream(const CComPtr<IStream>& _stream,void** _buff, DWORD& _size)const
{
	if (!_stream)
		throw 1;

	LARGE_INTEGER iLNull = {0, 0};
	ULARGE_INTEGER uLLen = {0, 0};
	//Take data size...
	HRESULT hr = _stream->Seek(iLNull, STREAM_SEEK_CUR, &uLLen);
	if(FAILED(hr))
		throw 1;
	//IStream -> to begin...
	hr = _stream->Seek(iLNull, STREAM_SEEK_SET, NULL);
	if(FAILED(hr))
		throw 1;
	//Size of buffer...
	DWORD Size = (DWORD)uLLen.QuadPart;

	char * szBuf = new char[Size+1];
	ZeroMemory (szBuf, sizeof(szBuf));
	_stream->Read(szBuf, Size, NULL);
	szBuf[Size] = 0;

	// output
	*_buff = szBuf;
	_size  = Size;
}

void CTranslator::CheckFilePath(const std::wstring& _uri)const
{
	WIN32_FIND_DATA fileData;

	if (Utils::GetFile(_uri,&fileData))
	{
		CAtlString sErr;
		sErr.Format(L"Error: cannot open file: \"%s\" ", _uri.c_str());
		m_pLog->Log(LEVEL_WARNING, __FUNCTIONW__, sErr.GetString());

		TError err(0,0,sErr.GetString(),0,_uri,E_FATAL, sErr.GetString());
		throw err;

	}

}

bool CTranslator::Mem2Mem(LPCTRANSLATORPARAMS pParams)const
{
	if (!pParams->Input.Code)
	{
		CAtlString sErr;
		sErr.Format(L"Error: input buffer is empty");
		m_pLog->Log(LEVEL_WARNING, __FUNCTIONW__, sErr.GetString());

		TError err(0,0,sErr.GetString(),0,gFileName,E_FATAL, sErr.GetString());
		throw err;
	}
	std::string sInputMemory;
	sInputMemory.append(pParams->Input.Code);

	m_pLog->Log(LEVEL_INFO, __FUNCTIONW__, L"Translation from memory to memory starts");

	// set content handler
	MSXML2::IMXWriterPtr pWriter;
	HRESULT hr = pWriter.CreateInstance(__uuidof(MSXML2::MXXMLWriter60));

	CComPtr<IStream> pIStream;
	if ( SUCCEEDED (hr))
	{
		//Set properties on the XML writer.
		pWriter->put_byteOrderMark(VARIANT_FALSE);
		pWriter->put_omitXMLDeclaration(VARIANT_FALSE);
		pWriter->put_indent(VARIANT_TRUE);
		pWriter->put_encoding(L"WINDOWS-1251");

		//Set the XML writer to the SAX content handler.
		m_xmlParser->PutContentHandler((MSXML2::ISAXContentHandlerPtr)pWriter);

		hr = CreateStreamOnHGlobal(NULL, TRUE, &pIStream);

		if ( SUCCEEDED(hr) )
		{
			pWriter->put_output(_variant_t(pIStream));
		}

	}

	// set error handler
	CComPtr<CSAXErrorHandler> errorHandler (new CSAXErrorHandler());
	if ( m_xmlParser->PutErrorHandler(errorHandler) )
	{
		errorHandler->AttachElementHandler((ISAXXmlElementHandler*)this);
	}

	if (!m_xmlParser->Parse(_bstr_t(sInputMemory.c_str()).GetBSTR()))
		return false;

	ReadStream(pIStream,&pParams->Output.Mem.Ptr,pParams->Output.Mem.Size);
	//if (!pIStream)
	//	return false;

	//LARGE_INTEGER iLNull = {0, 0};
	//ULARGE_INTEGER uLLen = {0, 0};
	////Take data size...
	//hr = pIStream->Seek(iLNull, STREAM_SEEK_CUR, &uLLen);
	//if(FAILED(hr))
	//	return false;
	////IStream -> to begin...
	//hr = pIStream->Seek(iLNull, STREAM_SEEK_SET, NULL);
	//if(FAILED(hr))
	//	return false;
	////Size of buffer...
	//DWORD Size = (DWORD)uLLen.QuadPart;

	//char * szBuf = new char[Size+1];
	//ZeroMemory (szBuf, sizeof(szBuf));
	//pIStream->Read(szBuf, Size, NULL);
	//szBuf[Size] = 0;

	//// output
	//pParams->Output.Mem.Ptr  = szBuf;
	//pParams->Output.Mem.Size = Size;

	return true;
}

bool CTranslator::Mem2File(LPCTRANSLATORPARAMS pParams)const
{
	if (!pParams->Input.Code)
	{
		CAtlString sErr;
		sErr.Format(L"Error: input buffer is empty");
		m_pLog->Log(LEVEL_WARNING, __FUNCTIONW__, sErr.GetString());

		TError err(0,0,sErr.GetString(),0,gFileName,E_FATAL, sErr.GetString());
		throw err;
	}
	std::wstring sFileName = pParams->Output.File;
	AddValidationSchema(sFileName);

	std::string sInputMemory;
	sInputMemory.append(pParams->Input.Code);

	m_pLog->Log(LEVEL_INFO, __FUNCTIONW__, L"Translation from memory to file: \"%s\" starts", sFileName.c_str());

	// set content handler
	MSXML2::IMXWriterPtr pWriter;
	HRESULT hr = pWriter.CreateInstance(__uuidof(MSXML2::MXXMLWriter60));

	CComPtr<IStream> pIStream;
	if ( SUCCEEDED (hr))
	{
		//Set properties on the XML writer.
		pWriter->put_byteOrderMark(VARIANT_FALSE);
		pWriter->put_omitXMLDeclaration(VARIANT_FALSE);
		pWriter->put_indent(VARIANT_TRUE);
		pWriter->put_encoding(/*L"WINDOWS-1251"*/ _bstr_t(CTranslatorEngineConfig::GetInstance()->m_sEncoding.c_str()).GetBSTR());

		//Set the XML writer to the SAX content handler.
		m_xmlParser->PutContentHandler((MSXML2::ISAXContentHandlerPtr)pWriter);

		//hr = CreateStreamOnHGlobal(NULL, TRUE, &pIStream);
		DWORD grfMode = STGM_WRITE | STGM_SHARE_EXCLUSIVE | STGM_CREATE;
		hr = SHCreateStreamOnFile(sFileName.c_str(),grfMode,&pIStream);

		if ( SUCCEEDED(hr) )
		{
			pWriter->put_output(_variant_t(pIStream));
		}

	}

	// set error handler
	CComPtr<CSAXErrorHandler> errorHandler (new CSAXErrorHandler());
	if ( m_xmlParser->PutErrorHandler(errorHandler) )
	{
		errorHandler->AttachElementHandler((ISAXXmlElementHandler*)this);
	}

	if (!m_xmlParser->Parse(_bstr_t(sInputMemory.c_str()).GetBSTR()))
		return false;

	return true;
}

bool CTranslator::File2Mem(LPCTRANSLATORPARAMS pParams)const
{

	//std::wstring sSchema;
	std::wstring sFileName = gFileName = pParams->Input.File;
	CheckFilePath(sFileName);
	//std::wstring sFileExt(GetFileExtension(sFileName));
	//if (!wcscmp(sFileExt.c_str(),L".vxml") )
	//	sSchema   = CTranslatorEngineConfig::Instance().m_sVXMLSchema;

	//if (!wcscmp(sFileExt.c_str(),L".ccxml") )
	//	sSchema   = CTranslatorEngineConfig::Instance().m_sCCXMLSchema;

	//if (sSchema.empty())
	//{
	//	m_pLog->Log(__FUNCTIONW__, L"Error: cannot translate from \"*.%s\" file, \"*.vxml/*.ccxml\" only", sFileExt.c_str());
	//	return false;
	//}

	//m_pLog->Log(__FUNCTIONW__, L"Translation from file: \"%s\" to memory starts", sFileName.c_str());
	//m_xmlParser->SetParserFeature(L"schema-validation", true);
	//m_xmlParser->SetParserFeature(L"use-schema-location", false);

	//if (!sSchema.empty())
	//{
	//	sFileExt.erase(0,1); // erase "."
	//	std::wstring schema_namespace=std::wstring(L"x-schema:")+sFileExt;
	//	m_xmlParser->AddValidationSchema(schema_namespace, sSchema);
	//}
	AddValidationSchema(sFileName);
	m_pLog->Log(LEVEL_INFO, __FUNCTIONW__, L"Translation from file: \"%s\" to memory starts", sFileName.c_str());

	// set content handler
	MSXML2::IMXWriterPtr pWriter;
    HRESULT hr = pWriter.CreateInstance(__uuidof(MSXML2::MXXMLWriter60));

	CComPtr<IStream> pIStream;
	if ( SUCCEEDED (hr))
	{
		   //Set properties on the XML writer.
		pWriter->put_byteOrderMark(VARIANT_FALSE);
		pWriter->put_omitXMLDeclaration(VARIANT_FALSE);
		pWriter->put_indent(VARIANT_TRUE);
		pWriter->put_encoding(L"WINDOWS-1251");

		//Set the XML writer to the SAX content handler.
		m_xmlParser->PutContentHandler((MSXML2::ISAXContentHandlerPtr)pWriter);

		hr = CreateStreamOnHGlobal(NULL, TRUE, &pIStream);

		if ( SUCCEEDED(hr) )
		{
			pWriter->put_output(_variant_t(pIStream));
		}
		
	}

	// set error handler
	CComPtr<CSAXErrorHandler> errorHandler (new CSAXErrorHandler());
	if ( m_xmlParser->PutErrorHandler(errorHandler) )
	{
		errorHandler->AttachElementHandler((ISAXXmlElementHandler*)this);
	}


	if (!m_xmlParser->ParseUrl(sFileName))
		return false;


	ReadStream(pIStream,&pParams->Output.Mem.Ptr,pParams->Output.Mem.Size);

	//if (!pIStream)
	//	return false;

	//LARGE_INTEGER iLNull = {0, 0};
	//ULARGE_INTEGER uLLen = {0, 0};
	////Take data size...
	//hr = pIStream->Seek(iLNull, STREAM_SEEK_CUR, &uLLen);
	//if(FAILED(hr))
	//	return false;
	////IStream -> to begin...
	//hr = pIStream->Seek(iLNull, STREAM_SEEK_SET, NULL);
	//if(FAILED(hr))
	//	return false;
	////Size of buffer...
	//DWORD Size = (DWORD)uLLen.QuadPart;
	//   
	//char * szBuf = new char[Size+1];
	//ZeroMemory (szBuf, sizeof(szBuf));
	//pIStream->Read(szBuf, Size, NULL);
	//szBuf[Size] = 0;

	//// output
	//pParams->Output.Mem.Ptr  = szBuf;
	//pParams->Output.Mem.Size = Size;

	return true;
}

bool CTranslator::File2File(LPCTRANSLATORPARAMS pParams)const
{
	std::wstring sInputFileName = gFileName = pParams->Input.File;
	CheckFilePath(sInputFileName);
	AddValidationSchema(sInputFileName);
	std::wstring sOutputFileName = pParams->Output.File;
	m_pLog->Log(LEVEL_INFO, __FUNCTIONW__, L"Translation from file: \"%s\" to file: \"%s\" starts", sInputFileName.c_str(), sOutputFileName.c_str());

	// set content handler
	MSXML2::IMXWriterPtr pWriter;
	HRESULT hr = pWriter.CreateInstance(__uuidof(MSXML2::MXXMLWriter60));

	CComPtr<IStream> pIStream;
	if ( SUCCEEDED (hr))
	{
		//Set properties on the XML writer.
		pWriter->put_byteOrderMark(VARIANT_FALSE);
		pWriter->put_omitXMLDeclaration(VARIANT_FALSE);
		pWriter->put_indent(VARIANT_TRUE);
		pWriter->put_encoding(/*L"WINDOWS-1251"*/ _bstr_t(CTranslatorEngineConfig::GetInstance()->m_sEncoding.c_str()).GetBSTR());

		//Set the XML writer to the SAX content handler.
		m_xmlParser->PutContentHandler((MSXML2::ISAXContentHandlerPtr)pWriter);

		//hr = CreateStreamOnHGlobal(NULL, TRUE, &pIStream);
		DWORD grfMode = STGM_WRITE | STGM_SHARE_EXCLUSIVE | STGM_CREATE;
		hr = SHCreateStreamOnFile(sOutputFileName.c_str(),grfMode,&pIStream);

		if ( SUCCEEDED(hr) )
		{
			pWriter->put_output(_variant_t(pIStream));
		}

	}

	// set error handler
	CComPtr<CSAXErrorHandler> errorHandler (new CSAXErrorHandler());
	if ( m_xmlParser->PutErrorHandler(errorHandler) )
	{
		errorHandler->AttachElementHandler((ISAXXmlElementHandler*)this);
	}

	if (!m_xmlParser->ParseUrl(sInputFileName))
		return false;

	return true;
}

bool CTranslator::Translate(LPCTRANSLATORPARAMS pParams)const
{
	try
	{
		if (!pParams) return false;

		pTR TranslateFunctions[4] = {&CTranslator::File2File,
			&CTranslator::File2Mem , 
			&CTranslator::Mem2File , 
			&CTranslator::Mem2Mem   };
		return (this->*TranslateFunctions[pParams->dwOutputType + (pParams->dwInputType << 1)])(pParams);
	}
	catch (TError &_err)
	{
		m_errors.push_back(_err);
		return false;
	}
}


// PARSER INTERFACE
void CTranslator::OnXmlStartElement(const ISATXMLElement& xmlElement)
{
	int test = 0;
}

void CTranslator::OnXmlElementData(const std::wstring& elementData, int depth)
{
	int test = 0;
}

void CTranslator::OnXmlEndElement(const ISATXMLElement& xmlElement)
{
	int test = 0;
}

void CTranslator::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
{
	CAtlString sErr;
	sErr.Format(L"Error parsing XML document. Line %d, char %d, reason \"%s\"", line, column, errorText.c_str());
	m_pLog->Log(LEVEL_WARNING, __FUNCTIONW__, sErr.GetString());

	m_errors.push_back(TError(line, column, errorText, errorCode,gFileName,E_ERROR, sErr.GetString()));
}

bool CTranslator::OnXmlAbortParse(const ISATXMLElement& xmlElement)
{
	return false;
}

DWORD CTranslator::GetErrorCount()const
{
	return static_cast<DWORD>(m_errors.size());
}

bool CTranslator::GetError(ULONG nIndex, TRANSLATORERROR* pError)const
{
	if (nIndex >= m_errors.size())
		return false;

	pError->Level = m_errors[nIndex].errLevel;
	_snwprintf(pError->File,sizeof(pError->File)/(sizeof(WCHAR)) - 1,L"%s", m_errors[nIndex].fileName.c_str());
	//wcsncpy(
	//	pError->File, 
	//	m_errors[nIndex].fileName.c_str(), 
	//	sizeof(pError->File)/(sizeof(WCHAR)) - 1);

	pError->Line = m_errors[nIndex].line;
	_snwprintf(pError->Code, sizeof(pError->Code)/(sizeof(WCHAR)) - 1, L"%X", m_errors[nIndex].errorCode);
	//int lenght = m_errors[nIndex].errorText.length();
	//CAtlString sErrCode;
	//sErrCode.Format(L"%x",m_errors[nIndex].errorCode);
	//wcsncpy(
	//	pError->Code, 
	//	sErrCode.GetString(), 
	//	sErrCode.GetLength()-1);

	int bytes = _snwprintf(pError->Msg, sizeof(pError->Msg)/(sizeof(WCHAR)) - 1, L"%s", m_errors[nIndex].errorText.c_str());
	//wcsncpy(
	//	pError->Msg, 
	//	m_errors[nIndex].errorText.c_str(), 
	//	sizeof(pError->Msg)/(sizeof(WCHAR)) - 1);

	
	//pError->File[sizeof(pError->File)/(sizeof(WCHAR))] = 0;
	//pError->Code[sizeof(pError->Code)/(sizeof(WCHAR))] = 0;
	//pError->Msg [sizeof(pError->Msg )/(sizeof(WCHAR))] = 0;
	return true;
}

bool CTranslator::GetErrorText(ULONG nIndex, LPWSTR pszBuf, DWORD cbBuf)const
{
	if (nIndex >= m_errors.size())
		return false;
	//wcsncpy(
	//	pszBuf, 
	//	m_errors[nIndex].formatErrText.c_str(), 
	//	cbBuf - 1);
	_snwprintf(pszBuf,cbBuf - 1,L"%s", m_errors[nIndex].formatErrText.c_str());
	//pszBuf[cbBuf] = 0;
	return true;
}
/******************************* eof *************************************/