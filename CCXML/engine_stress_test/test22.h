/************************************************************************/
/* Name     : engine_stress_test\test22.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 19 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest22: public CEngineTester
{
public:
	CTest22(){};
	~CTest22(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/