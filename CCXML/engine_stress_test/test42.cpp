/************************************************************************/
/* Name     : engine_stress_test\test42.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 02 Feb 2011                                               */
/************************************************************************/
#include "test42.h"

#define TEST_SCRIPT		"D:\\Projects\\ccxml_test\\test42.ccxml"

void CTest42::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest42::Work()
{
	CreateEngine();
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());
	Sleep(INFINITE);

	for (int i = 0; i < 10000; i++)
	{
		m_pEngine->DoStep();
	}
	DestroyEngine();
	
}

BOOL CTest42::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
/*	if (init_msg == L"TERMINATE")
	{
		CMessage msg(init_msg);
		msg[L"DialogID"] = ((__int64)0x0093D80C << 32) | m_nEngineCount;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else */if (CParam* param = init_msg.ParamByName(L"DestinationAddress"))
	{
		if (param->AsInt64() == m_nScriptID)
			m_pEngine->SetEvent((CPackedMsg(init_msg))());
	}
	//if (init_msg == L"END_DIALOG")
	//{
	//	m_pEngine->SetEvent((CPackedMsg(init_msg))());
	//}
	return TRUE;
}

/******************************* eof *************************************/