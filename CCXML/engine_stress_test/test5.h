/************************************************************************/
/* Name     : engine_stress_test\test5.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest5: public CEngineTester
{
public:
	CTest5(){};
	~CTest5(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/