/************************************************************************/
/* Name     : engine_stress_test\test36.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 06 Dec 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest36: public CEngineTester
{
public:
	CTest36(){};
	~CTest36(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/