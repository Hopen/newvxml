/************************************************************************/
/* Name     : engine_stress_test\test13.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 26 Feb 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest13: public CEngineTester
{
public:
	CTest13(){};
	~CTest13(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/