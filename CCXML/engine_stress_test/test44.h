/************************************************************************/
/* Name     : engine_stress_test\test44.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 25 Nov 2011                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest44: public CEngineTester
{
public:
	CTest44(){};
	~CTest44(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/