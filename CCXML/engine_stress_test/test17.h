/************************************************************************/
/* Name     : engine_stress_test\test17.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 01 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest17: public CEngineTester
{
public:
	CTest17(){};
	~CTest17(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/