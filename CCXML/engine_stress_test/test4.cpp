/************************************************************************/
/* Name     : engine_stress_test\test4.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 04 Jun 2010                                               */
/************************************************************************/
#include "test4.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test4.vxml"

//const std::wstring mrcp_answer = L"<?xml version = \"1.0\"?> \r\n" \
//L"<result> "\
//	L"<interpretation> "\
//		L"<instance name = \"Person\"> "\
//			L"<Person> "\
//				L"<Name> Andre Roy </Name> "\
//			L"</Person> "\
//		L"</instance> "\
//		L"<input confidence=\"66\">   may I speak to Andre Roy </input> "\
//	L"</interpretation> "\
//L"</result>";

const std::wstring mrcp_answer = L"<?xml version=\"1.0\" encoding=\"utf-8\"?>" \
L"<result>" \
	L"<interpretation grammar=\"session:asr - grammar0@forte - ct.ru\" confidence=\"1\">" \
		L"<input mode=\"speech\" confidence=\"1\" timestamp-start=\"2015 - 02 - 05T13:13 : 47.000\" timestamp-end=\"2015 - 02 - 05T13 : 13 : 47.960\">" \
		L"San Francisco" \
		L"</input>" \
		L"<instance>SFO<SWI_meaning>SFO</SWI_meaning></instance>" \
	L"</interpretation>" \
L"</result>";


void CTest4::InitEngine()
{
	HRESULT hr = S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"53";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"] = m_nScriptID;
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";
	//msg[L"NextExecuteMenuID"] = L"menu2";


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest4::Work()
{
	if (1)
	{
		CreateEngine();
		Sleep(100);

		for (int i = 0; i < 100; i++)
		{
			m_pEngine->DoStep();
			//Sleep(1000);
		}
		DestroyEngine();
	}
}

BOOL CTest4::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"PLAY_WAV")
	{
		//CMessage msg1(L"DISCONNECTED");
		//m_pEngine->SetEvent((CPackedMsg(msg1))());


		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"msg1");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		//msg[L"TerminationReason"] = L"TM_DIGIT";
		msg[L"DigitsBuffer"] = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		//int test = 0;
		//if (!test)
		//msg[L"DigitsBuffer"] = L"1";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"MRCP_RECOGNIZE")
	{
		CMessage msg(L"MRCP_RECOGNIZE_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		int test = 1;
		if (test)
		{
			msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
			msg[L"Result"] = mrcp_answer.c_str();
			//msg[L"Result"] = L"<error/>";
			msg[L"CompletionCause"] = L"0";
			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}

	}
	else if (init_msg == L"MRCP_TEXT_TO_SPEECH")
	{
		CMessage msg(L"MRCP_TEXT_TO_SPEECH_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"StreamName"] = L"Stream_22_223588948513864";
		//msg[L"ErrorCode"] = -1;
		//msg[L"ErrorDescription"] = "Failed to create TTS Engine. Check [Text], [CallID] parameters of message";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"MRCP_TEXT_TO_SPEECH_COMPLETED");
		msg[L"Result"] = L"";
		m_pEngine->SetEvent((CPackedMsg(msg))());

		int test = 1;
		if (test)
		{
			msg.SetName(L"STREAM_COMPLETED");
			msg[L"DigitsBuffer"] = L"";
			msg[L"VoxID"] = 22;
			msg[L"Digits"] = L"";
			msg[L"TermDigit"] = L"";
			msg[L"TerminationReason"] = L"TM_EOD";
			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}
	}

	return TRUE;
}

/******************************* eof *************************************/