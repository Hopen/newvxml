/************************************************************************/
/* Name     : engine_stress_test\test57.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Jun 2017                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest57 : public CEngineTester
{
public:
	CTest57() {};
	~CTest57() {};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/