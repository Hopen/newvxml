/************************************************************************/
/* Name     : engine_stress_test\test57.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Jun 2017                                               */
/************************************************************************/
#include "test57.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test57.vxml"

//const std::wstring mrcp_answer = L"<?xml version = \"1.0\"?> \r\n" \
//L"<result> "\
//	L"<interpretation> "\
//		L"<instance name = \"Person\"> "\
//			L"<Person> "\
//				L"<Name> Andre Roy </Name> "\
//			L"</Person> "\
//		L"</instance> "\
//		L"<input confidence=\"66\">   may I speak to Andre Roy </input> "\
//	L"</interpretation> "\
//L"</result>";

const std::wstring mrcp_answer = L"<?xml version=\"1.0\" encoding=\"utf-8\"?>" \
L"<result>" \
L"<interpretation grammar=\"session:asr - grammar0@forte - ct.ru\" confidence=\"1\">" \
L"<input mode=\"speech\" confidence=\"66\" timestamp-start=\"2015 - 02 - 05T13:13 : 47.000\" timestamp-end=\"2015 - 02 - 05T13 : 13 : 47.960\">" \
L"��������" \
L"</input>" \
L"<instance>SFO<SWI_meaning>SFO</SWI_meaning></instance>" \
L"</interpretation>" \
L"</result>";

const std::wstring mrcp_nomatch = L"<?xml version=\"1.0\" encoding=\"utf-8\"?>" \
L"<result>" \
L"<interpretation confidence=\"100\">" \
L"<input>" \
L"<nomatch />" \
L"</input>" \
L"<instance><SWI_meaning/></instance>" \
L"</interpretation>" \
L"</result>";

//03:35:20.428 P16C0 T222C S000054DD-5571F88D FT IN Name: MRCP_RECOGNIZE_COMPLETED; CompletionCause = "3"; WaveformUri = ""; CompletionDescription = "recognition-
//timeout"; RecognitionData = "Completion-Cause: 003"; CallID = 0x000054C8-00015D2D; CallbackID = 0x000054C8-00015D2D; SourceAddress = 0x000054C8-00015D2D

void CTest57::InitEngine()
{
	HRESULT hr = S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"53";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"] = m_nScriptID;
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";
	//msg[L"NextExecuteMenuID"] = L"menu2";


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest57::Work()
{
	if (1)
	{
		CreateEngine();
		Sleep(100);

		for (int i = 0; i < 100; i++)
		{
			m_pEngine->DoStep();
			//Sleep(1000);
		}
		DestroyEngine();
	}
}

BOOL CTest57::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"msg1");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		//msg[L"TerminationReason"] = L"TM_DIGIT";
		//msg[L"DigitsBuffer"] = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
		//msg[L"RecognitionData"] = mrcp_answer.c_str();
		////msg[L"Result"] = L"<error/>";
		//msg[L"WaveformUri"] = L"http://10.31.132.232:8080/recog/2017-06-18-17-58-32-918_a6327d30-0055-48bf-af21-4d4012d20700_332.wav";
		//msg[L"CompletionCause"] = L"0";
		//msg[L"CompletionDescription"] = L"success";
		//msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		//int test = 0;
		//if (!test)
		//msg[L"DigitsBuffer"] = L"1";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"MRCP_RECOGNIZE")
	{
		/*
		17:58:42.505 P0BC0 T0FA4 S00003048-0030342D FT Dump CMessage: "Name: MRCP_RECOGNIZE_COMPLETED; CompletionCause = "0"; WaveformUri = "http://10.31.132.232:8080/r
		ecog/2017-06-18-17-58-32-918_a6327d30-0055-48bf-af21-4d4012d20700_332.wav"; CompletionDescription = "success"; RecognitionData = "<?xml version="1.0" encoding="
		utf-8"?><result><interpretation grammar="session:http://localhost/theme:beeline" confidence="83"><input mode="speech" confidence="83" timestamp-start="2017-06-1
		8T17:58:36.200" timestamp-end="2017-06-18T17:58????; CallID = 0x00003044-0000001D; CallbackID = 0x00003044-0000001D; SourceAddress = 0x00003044-0000001D"

		10:25:00.280 P13A0 T1768 S0000999A-42D3BCA7 FT Dump CMessage: "Name: MRCP_RECOGNIZE_COMPLETED; CompletionCause = "1"; WaveformUri = "http://10.31.132.232:8080/r
		ecog/2017-07-01-10-24-56-853_a6327d30-0055-48bf-af21-4d4012d20700_498.wav"; CompletionDescription = "no-match"; RecognitionData = "<?xml version="1.0" encoding=
		"utf-8"?><result><interpretation confidence="100"><input><nomatch /></input><instance><SWI_meaning /></instance></interpretation></result>"...; CallID = 0x00009
		98C-0000004D; CallbackID = 0x0000998C-0000004D; SourceAddress = 0x0000998C-0000004D"
		*/
		//CMessage msg1(L"TERMINATE");
		//msg1[L"immediate"] = false;
		//msg1[L"namelist"] = L"telnum1 telnum2";
		//m_pEngine->SetEvent((CPackedMsg(msg1))());


		CMessage msg(L"MRCP_RECOGNIZE_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		int test = 1;
		if (test)
		{
			// success
			msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
			msg[L"RecognitionData"] = mrcp_answer.c_str();
			msg[L"WaveformUri"] = L"http://10.31.132.232:8080/recog/2017-06-18-17-58-32-918_a6327d30-0055-48bf-af21-4d4012d20700_332.wav";
			msg[L"CompletionCause"] = L"0";
			msg[L"CompletionDescription"] = L"success";
			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());

			////nomatch
			//msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
			//msg[L"RecognitionData"] = mrcp_nomatch.c_str();
			//msg[L"WaveformUri"] = L"http://10.31.132.232:8080/recog/2017-06-18-17-58-32-918_a6327d30-0055-48bf-af21-4d4012d20700_332.wav";
			//msg[L"CompletionCause"] = L"1";
			//msg[L"CompletionDescription"] = L"no-match";
			//msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			//m_pEngine->SetEvent((CPackedMsg(msg))());

			////recognition-timeout
			//msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
			//msg[L"RecognitionData"] = L"Completion-Cause: 003";// mrcp_nomatch.c_str();
			//msg[L"WaveformUri"] = L"http://10.31.132.232:8080/recog/2017-06-18-17-58-32-918_a6327d30-0055-48bf-af21-4d4012d20700_332.wav";
			//msg[L"CompletionCause"] = L"3";
			//msg[L"CompletionDescription"] = L"recognition-timeout";
			//msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			//m_pEngine->SetEvent((CPackedMsg(msg))());

			////no-input-timeout
			//msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
			//msg[L"RecognitionData"] = L"Completion-Cause: 002";// mrcp_nomatch.c_str();
			//msg[L"WaveformUri"] = L"";
			//msg[L"CompletionCause"] = L"2";
			//msg[L"CompletionDescription"] = L"no-input-timeout";
			//msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			//m_pEngine->SetEvent((CPackedMsg(msg))());
		}

	}
	else if (init_msg == L"MRCP_TEXT_TO_SPEECH")
	{
		CMessage msg(L"MRCP_TEXT_TO_SPEECH_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"StreamName"] = L"Stream_22_223588948513864";
		//msg[L"ErrorCode"] = -1;
		//msg[L"ErrorDescription"] = "Failed to create TTS Engine. Check [Text], [CallID] parameters of message";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"MRCP_TEXT_TO_SPEECH_COMPLETED");
		msg[L"Result"] = L"";
		m_pEngine->SetEvent((CPackedMsg(msg))());

		int test = 1;
		if (test)
		{
			msg.SetName(L"STREAM_COMPLETED");
			msg[L"DigitsBuffer"] = L"";
			msg[L"VoxID"] = 22;
			msg[L"Digits"] = L"";
			msg[L"TermDigit"] = L"";
			msg[L"TerminationReason"] = L"TM_EOD";
			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}
	}
	else if (init_msg == L"MRCP_SPEAK")
	{
		CMessage msg(L"MRCP_SPEAK_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"StreamName"] = L"Stream_22_223588948513864";
		//msg[L"ErrorCode"] = -1;
		//msg[L"ErrorDescription"] = "Failed to create TTS Engine. Check [Text], [CallID] parameters of message";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"MRCP_SPEAK_COMPLETED");
		msg[L"Result"] = L"";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}

	return TRUE;
}

/******************************* eof *************************************/