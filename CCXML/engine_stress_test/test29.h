/************************************************************************/
/* Name     : engine_stress_test\test29.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 10 Jun 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest29: public CEngineTester
{
public:
	CTest29(){};
	~CTest29(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/