/************************************************************************/
/* Name     : engine_stress_test\test58.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Feb 2018                                               */
/************************************************************************/
#include "test58.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test58.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test2.vxml"
/************************************************************************/

void CTest58::InitEngine()
{
	HRESULT hr = S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"9067837625";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	msg[L"namelist"] = L"state0 somevar";
	msg[L"state0"] = L"init";
	msg[L"somevar"] = L"1";
	m_nScriptID = ((__int64)0xA80D3958 << 32) | m_nEngineCount;

	msg[L"ScriptID"] = m_nScriptID;
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";

	CPackedMsg pMsg(msg);
	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest58::Work()
{
	CreateEngine();
	for (int i = 0; i < 100; i++)
	{
		m_pEngine->DoStep();
	}
	DestroyEngine();

}

BOOL CTest58::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);

	int user_drop = 0;
	if (user_drop)
	{
		CMessage disconnect(L"DISCONNECTED");
		disconnect[L"CallID"] = init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(disconnect))());

		return TRUE;
	}

	if (init_msg == L"PLAY_WAV")
	{
		if (mIsAsyncPlayWav)
		{
			static bool playWavOneMoreTime = false;

			if (!playWavOneMoreTime)
			{
				playWavOneMoreTime = true;
				CMessage msg(L"PLAY_WAV_ACK");
				msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
				m_pEngine->SetEvent((CPackedMsg(msg))());
				msg.SetName(L"PLAY_WAV_COMPLETED");
				msg[L"TerminationReason"] = L"TM_DIGIT";
				msg[L"DigitsBuffer"] = L"1";
				m_pEngine->SetEvent((CPackedMsg(msg))());
			}
			else
			{
				CMessage msg(L"AC2R_asyncHttpResponse");
				msg[L"Response"] = L"Some response...";
				msg[L"Status"] = 3;
				msg[L"RequestId"] = 0xC6B6F7E36E657690;
				msg[L"StatusString"] = L"Finished";
				m_pEngine->SetEvent((CPackedMsg(msg))());
			}

		}
		else
		{
			CMessage msg(L"PLAY_WAV_ACK");
			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());
			//msg.SetName(L"PLAY_WAV_COMPLETED");
			//msg[L"TerminationReason"] = L"TM_DIGIT";
			//msg[L"DigitsBuffer"] = L"1";
			//m_pEngine->SetEvent((CPackedMsg(msg))());
		}

		//CMessage disconnect(L"DISCONNECTED");
		//disconnect[L"CallID"] = init_msg[L"CallID"].Value;
		//m_pEngine->SetEvent((CPackedMsg(disconnect))());

	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"TerminationReason"] = L"TM_DIGIT";
		msg[L"DigitsBuffer"] = L"5#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"STOP_CHANNEL")
	{
		if (mIsAsyncPlayWav)
		{
			mIsAsyncPlayWav = false;
		}

		CMessage msg(L"STOP_CHANNEL_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"] = init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"STOP_CHANNEL_COMPLETED");
		m_pEngine->SetEvent((CPackedMsg(msg))());
//		msg.SetName(L"GET_DIGITS_COMPLETED");
//		msg[L"DigitsBuffer"] = L"1#";
//		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ANY2AC_asyncHttpRequest")
	{
		if (const auto param = init_msg.ParamByName(L"IsAsync"))
		{
			mIsAsync = param->AsBool();
		}

		if (const auto param = init_msg.ParamByName(L"IsFetchAudio"))
		{
			mIsAsyncPlayWav = true;
		}

		CMessage msg(L"ANY2AC_asyncHttpRequestAck");
		msg[L"RequestId"] = 0xC6B6F7E36E657690;
		msg[L"Status"] = 5;
		msg[L"Reason"] = L"Parameter [RequestId]. Parameter doesn't exist";

		m_pEngine->SetEvent((CPackedMsg(msg))());


		if (!mIsAsync)
		{
			msg.SetName(L"AC2R_asyncHttpResponse");
			msg[L"Response"] = L"Some response...";
			msg[L"Status"] = 3;
			msg[L"StatusString"] = L"Finished";
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}
	}
	else if (init_msg == L"ANY2AC_getResult")
	{
		CMessage msg(L"AC2R_asyncHttpResponse");
		msg[L"RequestId"] = 0xC6B6F7E36E657690;
		msg[L"Response"] = L"Some response...";
		msg[L"Status"] = 3;
		msg[L"StatusString"] = L"Finished";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}

	return TRUE;
}

/******************************* eof *************************************/