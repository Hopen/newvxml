/************************************************************************/
/* Name     : engine_stress_test\test51.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 15 Dec 2014                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest51 : public CEngineTester
{
public:
	CTest51(){};
	~CTest51(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/