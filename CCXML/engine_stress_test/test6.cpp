/************************************************************************/
/* Name     : engine_stress_test\test6.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/
#include "test6.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test6.vxml"

void CTest6::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest6::Work()
{
	CreateEngine();
	for (int i = 0; i < 100; i++)
	{
		m_pEngine->DoStep();
	}
	DestroyEngine();
}

BOOL CTest6::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	int user_drop = 0;
	if (user_drop)
	{
		CMessage disconnect(L"DISCONNECTED");
		disconnect[L"CallID"]		= init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(disconnect))());
	}

	if (init_msg == L"ANY2TA_RUN_SCRIPT")
	{
		CMessage msg(L"VXML_RUN_SCRIPT_OK");
		//CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		__int64 _nScriptID = init_msg[L"ScriptID"];

		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		//::Sleep(300);
		msg.SetName(L"END_DIALOG");
		msg[L"namelist"] = L"somevar";
		msg[L"somevar"] = L"somevalue";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		msg[L"DigitsBuffer"]	  = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DigitsBuffer"]		= L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"V2SC_RUN_FUNCTION")
	{
		//CMessage msg(L"SC2V_RUN_FUNCTION_OK");
		CMessage msg(L"SC2V_RUN_FUNCTION_ACK");
		__int64 _nScriptID = init_msg[L"ScriptID"];
		//msg[L"ErrorCode"] = 12;
		//msg[L"ErrorDescription"] = L"blablabla";
		msg[L"function"] = init_msg[L"function"].Value;

		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";

		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"SC2V_RUN_FUNCTION_COMPLETED");
		msg[L"namelist"] = L"somevar";
		msg[L"somevar"] = L"somevalue";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CHANGE_CALL_CONTROL")
	{
		CMessage msg(L"CHANGE_CALL_CONTROL_ACK");
		msg[L"CallID"] = init_msg[L"CallID"].Value;

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"CHANGE_CALL_CONTROL_COMPLETED");
		msg[L"Success"] = 0x0000000000000001;
		msg[L"OldScriptID"] = init_msg[L"OldScriptID"];
		msg[L"NewScriptID"] = init_msg[L"NewScriptID"];

		m_pEngine->SetEvent((CPackedMsg(msg))());
	}

	//if (init_msg == L"END_DIALOG")
	//{
	//	m_pEngine->SetEvent((CPackedMsg(init_msg))());
	//}
	if (CParam* param = init_msg.ParamByName(L"DestinationAddress"))
	{
		if (param->AsInt64() == m_nScriptID)
			m_pEngine->SetEvent((CPackedMsg(init_msg))());
	}

	return TRUE;
}

/******************************* eof *************************************/