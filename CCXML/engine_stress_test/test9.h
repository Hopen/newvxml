/************************************************************************/
/* Name     : engine_stress_test\test9.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 Feb 2010                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest9: public CEngineTester
{
public:
	CTest9(){};
	~CTest9(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/