/************************************************************************/
/* Name     : engine_stress_test\test41.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Jan 2011                                               */
/************************************************************************/
#include "test41.h"

#define TEST_SCRIPT		"D:\\Projects\\ccxml_test\\test1.ccxml"

void CTest41::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest41::Work()
{
	CreateEngine();
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());
	Sleep(INFINITE);

	for (int i = 0; i < 10000; i++)
	{
		m_pEngine->DoStep();
	}
	DestroyEngine();
	
}

BOOL CTest41::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"hello.jack")
	{
		/************************************************************************/
		/* 
		hello.jack; target = 0x00053168-0007DE90; targettype = "ccxml"; 
		jacksvar = "I am Jacks complete lack of surprise."; 
		DestinationAddress = 0x00053168-0007DE90; CallbackID = 0x00065900-4E1C383F; 
		ScriptID = 0x00053168-0007DE90; SourceAddress = 0x0007DE90-00053168
		*/
		/************************************************************************/
		CMessage msg(pMsgHeader);
		//CMessage msg(L"hello.jack");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

	}
	return TRUE;
}

/******************************* eof *************************************/