/************************************************************************/
/* Name     : engine_stress_test\test58.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 17 Feb 2018                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest58 : public CEngineTester
{
public:
	CTest58() {};
	~CTest58() {};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);

	bool mIsAsyncPlayWav = false;
	bool mIsAsync = false;
};

/******************************* eof *************************************/
