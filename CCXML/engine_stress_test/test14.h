/************************************************************************/
/* Name     : engine_stress_test\test14.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 26 Feb 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest14: public CEngineTester
{
public:
	CTest14(){};
	~CTest14(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/