/************************************************************************/
/* Name     : engine_stress_test\test39.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 17 Jan 2011                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest39: public CEngineTester
{
public:
	CTest39(){};
	~CTest39(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/