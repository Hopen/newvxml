/************************************************************************/
/* Name     : engine_stress_test\test_engine.h                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/
#pragma once

#include <memory>
#include "stdafx.h"


#include "..\EvtModel\EvtModel.h"
#include "..\Engine\Engine.h"
#include "..\CCXMLCom\CCXMLCom.h"
#include "..\VXMLCom\VXMLCom_i.h"

//__int64 CEngineTester::m_nEngineCount = 0;

class CEngineTester : public IEngineCallback
{
protected:
	IEngineVM*	m_pEngine;
	HANDLE		m_hThread;
	DWORD		m_dwThreadId;
	 __int64		m_nEngineId;
	 __int64		m_nScriptID;

	static unsigned __int64 m_nEngineCount;

protected:
	static DWORD WINAPI ThreadProc(CEngineTester* pTester)
	{
		::CoInitialize(NULL);
		pTester->Work();
		::CoUninitialize();
		return 0;
	}
	virtual void Work()=0;

	void CreateEngine();
	void DestroyEngine()
	{
		if (m_pEngine)
		{
			m_pEngine->Release();
			m_pEngine = NULL;
			printf("Engine %I64i destroyed\n", m_nEngineId);
		}
	}

	virtual void InitEngine()=0;

	// IEngineCallback methods

	virtual BOOL WINAPI PostMessage(PMESSAGEHEADER pMsgHeader){return TRUE;}
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader)=0;
	virtual BOOL WINAPI SendMessage(PMESSAGEHEADER pInMsgHeader, PMESSAGEHEADER* pOutMsgHeader, DWORD dwTimeOut){return TRUE;}
	virtual BOOL WINAPI SendAuxMessage(PMESSAGEHEADER pInMsgHeader, PMESSAGEHEADER* pOutMsgHeader, DWORD dwTimeOut){return TRUE;}
	virtual BOOL WINAPI TimerMessage(PMESSAGEHEADER pMsgHeader, DWORD dwMilliSeconds){return TRUE;}
	virtual void WINAPI	MarkDamaged(){}
	virtual BOOL WINAPI GetScriptName(BSTR* pbstrScriptName){return TRUE;}

	//binding extention
	virtual void WINAPI Bind(ULONGLONG qwDestination, PMESSAGEHEADER pMsg)
	{
	}
	virtual void WINAPI Unbind(ULONGLONG qwDestination, PMESSAGEHEADER pMsg)
	{
	}
	virtual void WINAPI RejectBound(ULONGLONG qwDestination, PMESSAGEHEADER pMsg)
	{
	}
	virtual void WINAPI RejectAllBounds(PMESSAGEHEADER pMsg)
	{
	}

public:
	CEngineTester(): 
		m_pEngine(NULL), 
		m_hThread(NULL), 
		m_dwThreadId(0),
		m_nScriptID(0)
	{
		m_hThread = ::CreateThread(
			NULL, 
			0, 
			(LPTHREAD_START_ROUTINE)ThreadProc, 
			this, 
			0, 
			&m_dwThreadId);
		m_nEngineId = ++m_nEngineCount;
	}
	virtual ~CEngineTester()
	{
		::CloseHandle(m_hThread);
	}
};

/******************************* eof *************************************/