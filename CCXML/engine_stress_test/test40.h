/************************************************************************/
/* Name     : engine_stress_test\test40.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 25 Jan 2011                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest40: public CEngineTester
{
public:
	CTest40(){};
	~CTest40(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/