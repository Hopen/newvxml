/************************************************************************/
/* Name     : engine_stress_test\test9.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 Feb 2010                                               */
/************************************************************************/
#include "test9.h"

#define TEST_SCRIPT		"C:\\is3\\scripts\\vxml_test\\test9.vxml"

void CTest9::InitEngine()
{
	HRESULT hr = S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"9067837625";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	msg[L"namelist"] = L"state0 somevar";
	msg[L"state0"] = L"init";
	msg[L"somevar"] = L"1";
	m_nScriptID = ((__int64)0xA80D3958 << 32) | m_nEngineCount;

	msg[L"ScriptID"] = m_nScriptID;
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest9::Work()
{
	CreateEngine();
	for (int i = 0; i < 100; i++)
	{
		m_pEngine->DoStep();
	}
	DestroyEngine();
	
}

BOOL CTest9::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		msg[L"DigitsBuffer"]	  = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DigitsBuffer"]		= L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	return TRUE;
}

/******************************* eof *************************************/