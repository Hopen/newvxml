/************************************************************************/
/* Name     : engine_stress_test\test_engine.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/
#include "test_engine.h"

unsigned __int64 CEngineTester::m_nEngineCount = 0;

void CEngineTester::CreateEngine()
{
	HRESULT hr;
	if (FAILED(hr = CoCreateInstance(
		//CLSID_CCXMLComInterface,
		CLSID_VXMLComInterface,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IEngineVM,
		(LPVOID*)&m_pEngine)))
	{
		printf("Cannot create engine: 0x%p\n", hr);
		return;
	}
	else
	{
		printf("Engine %I64i created\n", m_nEngineId);
	}

	InitEngine();
}

/******************************* eof *************************************/