/************************************************************************/
/* Name     : engine_stress_test\test52.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 24 Dec 2014                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest52 : public CEngineTester
{
public:
	CTest52(){};
	~CTest52(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/