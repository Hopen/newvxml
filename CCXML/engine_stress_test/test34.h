/************************************************************************/
/* Name     : engine_stress_test\test34.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 Aug 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest34: public CEngineTester
{
public:
	CTest34(){};
	~CTest34(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/