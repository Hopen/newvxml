/************************************************************************/
/* Name     : engine_stress_test\test53.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 21 Jan 2015                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest53 : public CEngineTester
{
public:
	CTest53(){};
	~CTest53(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/