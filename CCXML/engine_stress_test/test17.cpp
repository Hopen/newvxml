/************************************************************************/
/* Name     : engine_stress_test\test17.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 01 Mar 2010                                               */
/************************************************************************/
#include "test17.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test17.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\ccxml_test\\0611_center_VRN.ccxml"

void CTest17::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"] = m_nScriptID;
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"SigInfo"] = L"0x0601100702600109010A020100040504106018F20A07031369937147151D038090A33F088417970628909909";
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";
	msg[L"predefine"] = L"c:\\is3\\scripts\\vxml_test\\test17_1.vxml;c:\\is3\\scripts\\vxml_test\\test17_2.vxml";
	//msg[L"NextExecuteMenuID"] = L"menu_test";

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest17::Work()
{
	CreateEngine();
	//Sleep(5000);
	//CMessage msg(L"OFFERED");
	//msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//msg[L"A"]			= L"53";
	//msg[L"B"]			= L"6277";

	//CPackedMsg pMsg(msg);
	//m_pEngine->SetEvent(pMsg());

	//Sleep(1000);
	//CMessage msg2(L"CONNECTED");
	//msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//CPackedMsg pMsg2(msg2);
	//m_pEngine->SetEvent(pMsg2());
	////Sleep(INFINITE);

	for (int i = 0; i < 1000; i++)
	{
		m_pEngine->DoStep();
	}
	DestroyEngine();
	
}

static std::size_t counter = 1;

BOOL CTest17::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//CMessage msg(L"TS2D_INVALID_CALL");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		if (counter++ > 1)
		{
			msg[L"DigitsBuffer"] = L"2#";
		}
		else
		{
			msg[L"DigitsBuffer"] = L"1#";
		}
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		msg[L"DigitsBuffer"]		= L"2#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	return TRUE;
}

/******************************* eof *************************************/