/************************************************************************/
/* Name     : engine_stress_test\test54.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 28 Jun 2015                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest54 : public CEngineTester
{
public:
	CTest54(){};
	~CTest54(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/