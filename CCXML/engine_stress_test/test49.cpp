/************************************************************************/
/* Name     : engine_stress_test\test49.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 Oct 2013                                               */
/************************************************************************/
#include "test49.h"
#include <crtdbg.h>
//#define TEST_SCRIPT		"D:\\Projects\\ccxml_test\\test43.ccxml"
#define TEST_SCRIPT		"c:\\is3\\scripts\\ccxml_test\\test30_b.ccxml"
//#define TEST_SCRIPT		"D:\\Projects\\vxml_test\\IVRSS.vxml"
//#define TEST_SCRIPT		"D:\\Projects\\vxml_test\\main.vxml"
//#define TEST_SCRIPT		"D:\\Projects\\vxml_test\\myTest.vxml"
//#define TEST_SCRIPT		"D:\\Projects\\vxml_test\\test6.vxml"
//#define TEST_SCRIPT		"D:\\Projects\\vxml_test\\mylog.vxml"

int CTest49::s_playWavCounter = 0;

void CTest49::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	//msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"CallID"]					= (__int64)0xC57DD39D024402B6;// 0xC57DD39D024402B6
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;
	msg[L"SigInfo"]     = L"0x0601100702600109010A020100040504106018F20A07031369937147151D038090A33F088417970628909909";
	msg[L"ParentScriptID"]     = 0;
	msg[L"parenttype"] = L"ccxml";

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest49::Work()
{
	CreateEngine();
/*
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= / *double* /(((__int64)0x00063C40 << 32) | m_nEngineCount);
	CComVariant val = msg[L"CallID"].Value;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";
	//msg[L"SigInfo"]     = L"0x0601100702600109010A020100040504106018F20A07031369937147151D038090A33F088417970628909909";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());
	Sleep(INFINITE);*/

	////IN Name: dialog.transfer; uri = "79067837625"; bridge = true; maxtime = "10000"; connecttimeout = "60s"; aai = ""; targettype = "ccxml"; type = "bridge"; CallID = 0x5F4048CA-0A985269; CallbackID = 0x5F4048CA-0A985269; ScriptID = 0xC0103F40-07BB9F9E; SaveScriptID = 0xC0103F40-07BB9F9E; DestinationAddress = 0xC0103F40-07BB9F9D; SourceAddress = 0xC0103F40-07BB9F9E
	//CMessage msg(L"dialog.transfer");
	//msg[L"CallID"]		= (((__int64)0x00063C40 << 32) | m_nEngineCount);
	//msg[L"uri"] = L"79067837625";
	//msg[L"bridge"] = true;
	//msg[L"maxtime"] = 10000;
	//msg[L"connecttimeout"] = L"60s";
	//msg[L"targettype"] = L"ccxml";
	//msg[L"type"] = L"bridge";
	//msg[L"aai"] = L"";
	////msg[L""] = L"";

	//CPackedMsg pMsg(msg);
	//m_pEngine->SetEvent(pMsg());


	Sleep(100);
	CMessage msg(L"OFFERED");
	//msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"CallID"]					= (__int64)0xC57DD39D024402B6;// 0xC57DD39D024402B6
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";
	msg[L"Host"]					= L"ERIDAN";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	//msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg2[L"CallID"]					= (__int64)0xC57DD39D024402B6;// 0xC57DD39D024402B6
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());

	for (int i = 0; i < 130; i++)
	{
		if (i == 1)
		{
			CMessage msg(L"MAIN");
			//msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
			msg[L"CallID"]					= (__int64)0xC57DD39D024402B6;// 0xC57DD39D024402B6
			msg[L"A"]			= L"53";
			msg[L"B"]			= L"6277";
			msg[L"SigInfo"]     = L"0x0601100702600109010A020100040504106018F20A07031369937147151D038090A33F088417970628909909";

			CPackedMsg pMsg(msg);
			m_pEngine->SetEvent(pMsg());

		}
		m_pEngine->DoStep();
		Sleep(100);
	}
	DestroyEngine();
	
	//_CrtDumpMemoryLeaks();
}

static __int64 CalleeID = 0;



BOOL CTest49::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"MAKE_CALL")
	{
		// connection successful
		// proceed call
		CMessage msg(L"TS2D_MakeCallOk");
		//CalleeID = ((__int64)0x00063C40 << 32) | ++m_nEngineCount; 
		CalleeID					= (__int64)0xC57DD39D024402B5;// | ++m_nEngineCount;
		msg[L"CallID"]		= CalleeID;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		// hand up call
		msg.SetName(L"CONNECTED");
		m_pEngine->SetEvent((CPackedMsg(msg))());


		//// connection failed
		//CMessage msg(L"TS2D_MakeCallFailed");
		//msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ReasonDescription"] = L"Script already terminated, MAKE_CALL ignored";
		//m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ConferenceCreateEmpty")
	{
		CMessage msg(L"ConferenceCreateEmptyAck");
		__int64 nConferenceID = (((__int64)0x00063C40 << 32) | ++m_nEngineCount); 
		msg[L"ConferenceID"]  = nConferenceID;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		msg[L"VoxID"]  = (((__int64)0x00063C40 << 32) | ++m_nEngineCount);
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}		
	else if (init_msg == L"ConferenceDestroy")
	{
		CMessage msg(L"ConferenceDestroyAck");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		msg[L"ConferenceID"]  = init_msg[L"ConferenceID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ConferenceJoinParty")
	{
		CMessage msg(L"ConferenceUserJoined");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		msg[L"ConferenceID"]  = init_msg[L"ConferenceID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"ConferenceJoinPartyAck");
		m_pEngine->SetEvent((CPackedMsg(msg))());


		//// drop call
		//msg.SetName(L"DISCONNECTED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//m_pEngine->SetEvent((CPackedMsg(msg))());

	}		
	else if (init_msg == L"ConferenceRemoveParty")
	{
		CMessage msg(L"ConferenceRemovePartyAck");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		msg[L"ConferenceID"]  = init_msg[L"ConferenceID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

	}
	else if (init_msg == L"ROUTE_DEV")
	{
		CMessage msg(L"ROUTE_DEV_ACK");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//// drop call
		//msg.SetName(L"DISCONNECTED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//m_pEngine->SetEvent((CPackedMsg(msg))());

	}
	else if (init_msg == L"UNROUTE_DEV")
	{
		CMessage msg(L"UNROUTE_DEV_ACK");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		if (CParam* param = init_msg.ParamByName(L"CallID"))
			msg[L"CallID"]		= init_msg[L"CallID"].Value;
		if (CParam* param = init_msg.ParamByName(L"VoxID"))
			msg[L"VoxID"]		= init_msg[L"VoxID"].Value;

		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		//msg.SetName(L"PLAY_WAV_COMPLETED");
		msg.SetName(L"DISCONNECTED");
		msg[L"TerminationReason"] = /*L"TM_DIGIT";*/ L"TM_EOD";
		msg[L"DigitsBuffer"]	  = L"1";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		// nomatch condition
		//msg[L"DigitsBuffer"]		= L"2#";
		// noinput condition
		//msg[L"TerminationReason"] = L"TM_EOD";

		msg[L"DigitsBuffer"]		= L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"dialog.transfer")
	{
		//m_pEngine->SetEvent((CPackedMsg(init_msg))());

		CMessage msg(L"dialog.transfer.complete");
		//CMessage msg(L"DISCONNECTED");
		msg[L"target"]     = init_msg[L"DialogID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"targettype"] = L"dialog";

		m_pEngine->SetEvent((CPackedMsg(msg))());

	}
	else if (init_msg == L"vxml_maxtime")
	{
		//m_pEngine->SetEvent((CPackedMsg(init_msg))());
		/************************************************************************/
		/* 
		IN Name: DISCONNECTED; Error_GC = "GCRV_NORMAL"; 
		Error_ccLib = "GC_SS7_LIB"; Error_ccValue = 16; Error_ccMsg = "No description available"; 
		Error_gcMsg = "Normal completion"; SigInfo = "0x12028290"; 
		CallID = 0x52645719-5265DC91; SourceAddress = 0x0501005A-52645719
		*/
		/************************************************************************/
		CMessage msg(L"DISCONNECTED");
		//msg[L"CallID"]  = init_msg[L"CallbackID"].Value; // Caller hang up
		msg[L"CallID"] = CalleeID; // Callee hang up
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"dialog.transfer.complete")
	{
		m_pEngine->SetEvent((CPackedMsg(init_msg))());
	}
	else if (init_msg == L"connection.disconnect.transfer")
	{
		m_pEngine->SetEvent((CPackedMsg(init_msg))());
	}

	//else if (init_msg == L"GET_DIGITS")
	//{
	//	CMessage msg(L"GET_DIGITS_COMPLETED");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	msg[L"DigitsBuffer"]		= L"1#";
	//	//m_pEngine->SetEvent((CPackedMsg(msg))()); // successful input

	//}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	//else if (init_msg == L"STOP_VIDEO")
	//{
	//	CMessage msg(L"STOP_VIDEO_ACK");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"STOP_VIDEO_COMPLETED");
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}
	else if (init_msg == L"STOP_CHANNEL")
	{
		CMessage msg(L"STOP_CHANNEL_ACK");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"STOP_CHANNEL_COMPLETED");
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_USRSTOP";
		//msg[L"DigitsBuffer"]		= L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}

	return TRUE;
}

/******************************* eof *************************************/