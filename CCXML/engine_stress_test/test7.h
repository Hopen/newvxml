/************************************************************************/
/* Name     : engine_stress_test\test7.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 24 Feb 2010                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest7: public CEngineTester
{
public:
	CTest7(){};
	~CTest7(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/