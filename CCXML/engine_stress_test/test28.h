/************************************************************************/
/* Name     : engine_stress_test\test28.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 08 Jun 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest28: public CEngineTester
{
public:
	CTest28(){};
	~CTest28(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/