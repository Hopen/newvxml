/************************************************************************/
/* Name     : engine_stress_test\test55.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 Feb 2015                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest55 : public CEngineTester
{
public:
	CTest55(){};
	~CTest55(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/