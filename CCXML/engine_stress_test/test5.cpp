/************************************************************************/
/* Name     : engine_stress_test\test5.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/
#include "test5.h"

//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test2s.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test2.vxml"
#define TEST_SCRIPT		"C:\\is3\\scripts\\vxml_test\\test5.vxml"
//#define PREDEFINE_SCRIPT	"c:\\is3\\scripts\\vxml_test\\test2_nomatch.vxml"
// test5.ccxml
/************************************************************************/
/* <?xml version="1.0" encoding="UTF-8"?>
<ccxml version="1.0" xmlns="http://www.w3.org/2002/09/ccxml">
	<!-- Lets declare our state var -->
	<var name="state0" expr="'init'"/>

	<eventprocessor statevariable="state0">
		<!-- Process the incoming call -->  
		<transition state="init" event="connection.alerting">
			<accept/>      
		</transition>
		<!-- Call has been answered -->  
		<transition state="init" event="connection.connected" name="evt">
			<log expr="'Houston, we have liftoff.'"/>
			<dialogstart src="'test5.vxml'"/>
			<assign name="state0" expr="'dialogActive'" />

		</transition>
		<!-- Process the incoming call -->  
		<transition state="dialogActive" event="dialog.exit" name="evt">
			<log expr="'Houston, the dialog returned [' + evt.values.input + ']'" />
			<exit /> 
		</transition>
		<!-- Caller hung up. Lets just go on and end the session -->
		<transition event="connection.disconnected" name="evt">
			<exit/>
		</transition>
		<!-- Something went wrong. Lets go on and log some info and end the call -->
		<transition event="error.*" name="evt">
			<log expr="'Houston, we have a problem: (' + evt.reason + ')'"/>
			<exit/>
		</transition>
	</eventprocessor>    
</ccxml>                                                                     */
/************************************************************************/

// test5.vxml
/************************************************************************/
/* <?xml version="1.0" encoding="UTF-8"?>
<vxml xmlns="http://www.w3.org/2001/vxml"
xmlns:xsi="http://www.w3.org/2001/vxml http://www.w3.org/TR/voicexml20/vxml.xsd"
version="2.0"
xsi:noNamespaceSchemaLocation="vxml.xsd">
>

	<property name="termchar" value=""/>

	<form id="intro">
		<field name="input">
			<grammar src="builtin:dtmf/1" mode="dtmf" term="#"/>
			<prompt>
				<audio src="helloday.wav"/>
			</prompt>
			<filled>
				<goto next="#menu1"/>
			</filled>
		</field>    
	</form>

	<menu id="menu1">
		<exit namelist="input"/>
	</menu>

</vxml>                                                                     */
/************************************************************************/


void CTest5::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"9067837625";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	msg[L"namelist"]				= L"state0 somevar";
	msg[L"state0"]					= L"init";
	msg[L"somevar"]					= L"1";
	//m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	m_nScriptID = ((__int64)0xA80D3958 << 32) | m_nEngineCount;
	
	msg[L"ScriptID"]				= m_nScriptID;
	//__int64 nScriptID_TEST = msg.ParamByName(L"ScriptID")->AsInt64();
	msg[L"Timeslot"]				= m_nEngineCount % 30;
	msg[L"ParentScriptID"]			= 0;
	msg[L"parenttype"]				= L"ccxml";
	//msg[L"predefine"] = PREDEFINE_SCRIPT;


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest5::Work()
{
	CreateEngine();
	//Sleep(100);
	//CMessage msg(L"OFFERED");
	//msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//msg[L"A"]			= L"53";
	//msg[L"B"]			= L"6277";
	//msg[L"Host"]					= L"ERIDAN";

	//CPackedMsg pMsg(msg);
	//m_pEngine->SetEvent(pMsg());

	//CMessage msg2(L"CONNECTED");
	//msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//CPackedMsg pMsg2(msg2);
	//m_pEngine->SetEvent(pMsg2());
	////Sleep(INFINITE);

	for (int i = 0; i < 100; i++)
	{
		m_pEngine->DoStep();
		//Sleep(1000);
	}
	DestroyEngine();
	
}

BOOL CTest5::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);

	int user_drop = 0;
	if (user_drop)
	{
		CMessage disconnect(L"DISCONNECTED");
		disconnect[L"CallID"] = init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(disconnect))());

		return TRUE;
	}

	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ErrorCode"] = 12;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//msg.SetName(L"DIGIT_GOT");
		//msg[L"Digit"] = L"2";
		//msg[L"DigitsBuffer"] = L"2";
		//m_pEngine->SetEvent((CPackedMsg(msg))());


		msg.SetName(L"PLAY_WAV_COMPLETED");

		msg[L"TerminationReason"] = L"TM_DIGIT";
		//msg[L"TerminationReason"] = L"TM_EOD";
		//int test = 0;
		//if (!test)
		msg[L"DigitsBuffer"]	  = L"2";
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//CMessage disconnect(L"DISCONNECTED");
		//disconnect[L"CallID"] = init_msg[L"CallID"].Value;
		//m_pEngine->SetEvent((CPackedMsg(disconnect))());

	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"TerminationReason"] = L"TM_DIGIT";
		msg[L"DigitsBuffer"]	  = L"2#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	return TRUE;
}

/******************************* eof *************************************/