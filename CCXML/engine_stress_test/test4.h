/************************************************************************/
/* Name     : engine_stress_test\test4.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 04 Jun 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest4 : public CEngineTester
{
public:
	CTest4() {};
	~CTest4() {};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/