/************************************************************************/
/* Name     : engine_stress_test\test30.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 21 Jun 2010                                               */
/************************************************************************/
#include "test30.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\ccxml_test\\test30.ccxml"

void CTest30::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest30::Work()
{
	CreateEngine();
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= /*double*/(((__int64)0x00063C40 << 32) | m_nEngineCount);
	CComVariant val = msg[L"CallID"].Value;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";
	//msg[L"SigInfo"]     = L"0x0601100702600109010A020100040504106018F20A07031369937147151D038090A33F088417970628909909";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());

	for (int i = 0; i < 10000; i++)
	{
		m_pEngine->DoStep();
		Sleep(1000);
	}
	DestroyEngine();
	
}

BOOL CTest30::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"MAKE_CALL")
	{
		// connection successful
		// proceed call
		CMessage msg(L"TS2D_MakeCallOk");
		msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | ++m_nEngineCount; 
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		// hand up call
		msg.SetName(L"CONNECTED");
		m_pEngine->SetEvent((CPackedMsg(msg))());


		//// connection failed
		//CMessage msg(L"TS2D_MakeCallFailed");
		//msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ReasonDescription"] = L"Script already terminated, MAKE_CALL ignored";
		//m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ConferenceJoinParty")
	{
		CMessage msg(L"ConferenceJoinPartyAck");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

	}		
	else if (init_msg == L"ROUTE_DEV")
	{
		CMessage msg(L"ROUTE_DEV_ACK");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		// hand up call
		msg.SetName(L"DISCONNECTED");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

	}
	//else if (init_msg == L"GET_DIGITS")
	//{
	//	CMessage msg(L"GET_DIGITS_COMPLETED");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	msg[L"DigitsBuffer"]		= L"1#";
	//	//m_pEngine->SetEvent((CPackedMsg(msg))()); // successful input

	//}
	//else if (init_msg == L"CLEAR_DIGITS")
	//{
	//	CMessage msg(L"CLEAR_DIGITS_COMPLETED");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}
	//else if (init_msg == L"STOP_VIDEO")
	//{
	//	CMessage msg(L"STOP_VIDEO_ACK");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"STOP_VIDEO_COMPLETED");
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}
	//else if (init_msg == L"STOP_CHANNEL")
	//{
	//	CMessage msg(L"STOP_CHANNEL_ACK");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"STOP_CHANNEL_COMPLETED");
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"GET_DIGITS_COMPLETED");
	//	msg[L"DigitsBuffer"]		= L"1#";
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}

	return TRUE;
}

/******************************* eof *************************************/