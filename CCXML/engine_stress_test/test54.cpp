/************************************************************************/
/* Name     : engine_stress_test\test54.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 28 Jun 2015                                               */
/************************************************************************/
#include "StdAfx.h"
#include "test54.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test54.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test56.vxml"

void CTest54::InitEngine()
{
	HRESULT hr = S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"53";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"] = m_nScriptID;
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest54::Work()
{
	CreateEngine();
	//Sleep(100);
	//CMessage msg(L"OFFERED");
	//msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//msg[L"A"]			= L"53";
	//msg[L"B"]			= L"6277";

	//CPackedMsg pMsg(msg);
	//m_pEngine->SetEvent(pMsg());

	//CMessage msg2(L"CONNECTED");
	//msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//CPackedMsg pMsg2(msg2);
	//m_pEngine->SetEvent(pMsg2());
	//Sleep(INFINITE);

	for (int i = 0; i < 10000; i++)
	{
		m_pEngine->DoStep();
		Sleep(1000);
	}
	DestroyEngine();
	delete this;
}

static int count = 0;
const std::wstring mrcp_answer = L"<?xml version=\"1.0\" encoding=\"utf-8\"?>" \
L"<result>" \
L"<interpretation grammar=\"session:asr - grammar0@forte - ct.ru\" confidence=\"1\">" \
L"<input mode=\"speech\" confidence=\"1\" timestamp-start=\"2015 - 02 - 05T13:13 : 47.000\" timestamp-end=\"2015 - 02 - 05T13 : 13 : 47.960\">" \
L"San Francisco" \
L"</input>" \
L"<instance>SFO<SWI_meaning>SFO</SWI_meaning></instance>" \
L"</interpretation>" \
L"</result>";


BOOL CTest54::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	//if (init_msg == L"PLAY_WAV")
	//{
	//	CMessage msg(L"PLAY_WAV_ACK");
	//	//msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
	//	//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"PLAY_WAV_COMPLETED");
	//	msg[L"TerminationReason"] = L"TM_EOD";
	//	//msg[L"DigitsBuffer"]	  = L"1#";
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}

	//else 
	if (init_msg == L"MRCP_RECOGNIZE")
	{
		CMessage msg(L"MRCP_RECOGNIZE_ACK");
		msg[L"CallID"]     = init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//int test = 1;
		//if (test)
		//{
		//	msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
		//	msg[L"Result"] = mrcp_answer.c_str();
		//	//msg[L"Result"] = L"<error/>";
		//	msg[L"CompletionCause"] = L"001	no-match";
		//	msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//	m_pEngine->SetEvent((CPackedMsg(msg))());
		//}

	}
	else if (init_msg == L"PLAY_WAV")
	{

		CMessage msg(L"PLAY_WAV_ACK");
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//msg.SetName(L"MRCP_RECOGNIZE_ACK");
		//msg[L"CallID"] = init_msg[L"CallID"].Value;
		//m_pEngine->SetEvent((CPackedMsg(msg))());


		int test = 1;

		if (test) //A
		{
			msg.SetName(L"PLAY_WAV_COMPLETED");
			msg[L"TerminationReason"] = L"TM_EOD";
			//msg[L"DigitsBuffer"]	  = L"1#";
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}

		if (test) //B
		{
			msg.SetName(L"MRCP_RECOGNIZE_COMPLETED");
			msg[L"Result"] = mrcp_answer.c_str();
			msg[L"CompletionCause"] = L"000	success";

			//msg[L"Result"] = L"<error/>";
			//msg[L"CompletionCause"] = L"001	no-match";

			//msg[L"Result"] = L"<error/>";
			//msg[L"CompletionCause"] = L"002	no-input";

			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}

	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"TerminationReason"] = L"TM_DIGIT";
		//msg[L"DigitsBuffer"]		= L"1#";
		++count;
		if (count == 1)
			msg[L"DigitsBuffer"] = L"1#";
		if (count == 2)
		//	msg[L"DigitsBuffer"] = L"2#";
		//if (count == 3)
			msg[L"DigitsBuffer"] = L"3#";
		else
			msg[L"TerminationReason"] = L"TM_EOD";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	if (init_msg == L"END_DIALOG")
	{
		m_pEngine->SetEvent((CPackedMsg(init_msg))());
	}
	return TRUE;
}

/******************************* eof *************************************/