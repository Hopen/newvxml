/************************************************************************/
/* Name     : VXMLCom\KeepAlive.cpp                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 29 Jul 2013                                               */
/************************************************************************/

#include "stdafx.h"
//#include "KeepAlive.h"
//
//
//CKeepAlive::CKeepAlive(
//	const int& _max_wait, 
//	const __int64& scriptID, 
//	const __int64& parentScriptID,
//	boost::asio::io_service& _io, 
//	IEngineCallbackPtr pExec) :
//m_iMaxWait(_max_wait), 
//m_engine(pExec), 
//m_scriptID(scriptID),
//m_parentScriptID(parentScriptID),
//bStillKeepAlive(true),
//m_pStrand(_io),
//m_timer(_io, boost::posix_time::minutes(m_iMaxWait))
//{
//	if (m_iMaxWait)
//	{
//		m_timer.async_wait(m_pStrand.wrap(boost::bind(&CKeepAlive::OnWakeUp, this)));
//	}
//	//m_thread = boost::thread(boost::bind(&boost::asio::io_service::run, _io));
//}
//
//CKeepAlive::~CKeepAlive()
//{
//	StopKeepAlive();
//}
//
//void CKeepAlive::StopKeepAlive()
//{
//	bStillKeepAlive = false;
//	m_timer.cancel();
//	//m_thread.join();
//}
//
//void CKeepAlive::OnWakeUp()
//{
//	if (!bStillKeepAlive)
//		return;
//	MakeKeepAliveMsg(m_scriptID);
//	MakeKeepAliveMsg(m_parentScriptID);
//	m_timer.expires_from_now(boost::posix_time::minutes(m_iMaxWait));
//	m_timer.async_wait(m_pStrand.wrap(boost::bind(&CKeepAlive::OnWakeUp, this)));
//}
//
////void CKeepAlive::OnTimeOut()
////{
////	// nothing todo here
////	int test = 0;
////}
//
//
//void CKeepAlive::MakeKeepAliveMsg(const __int64& scriptID)
//{
//	CMessage msg(L"KEEP_ALIVE");
//	msg[L"DestinationAddress"] = scriptID;
//	CPackedMsg pm(msg);
//	//m_pExec->pEvtPrc->PostAuxMessage(pm());
//	//m_pExec->pLog->Log(__FUNCTIONW__, L"OUT %s", msg.Dump().c_str());
//	IEngineCallbackPtr2 engine = m_engine.lock();
//	if (engine)
//	{
//		engine->PostAuxMessage(pm());
//	}
//}

/******************************* eof *************************************/