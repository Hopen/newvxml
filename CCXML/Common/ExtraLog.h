/************************************************************************/
/* Name     : Common\ExtraLog.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solution                                           */
/* Date     : 14 Apr 2019                                               */
/************************************************************************/
#pragma once
#include <memory>
#include "..\Engine\EngineLog.h"

//class CEngineLog;

class ExtraLog
{
public:
	ExtraLog();
	void Init(ULONGLONG ullSID, LPCWSTR pwszLevel, LPCWSTR pwszFormat, bool extraDate = false);

	template <class... TArgs>
	void LogFinest(TArgs&&... aArgs)
	{
		mLog->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogInfo(TArgs&&... aArgs)
	{
		mLog->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogWarning(TArgs&&... aArgs)
	{
		mLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogExtra(TArgs&&... aArgs)
	{
		mLog->LogLarge(enginelog::LEVEL_WARNING, __FUNCTIONW__, std::forward<TArgs>(aArgs)...);
	}

	//void LogInfo(LPCWSTR pwszFmt, ...);
	//void LogWarning(LPCWSTR pwszFmt, ...);
	//void LogExtra(LPCWSTR pwszFmt, ...);

	void InitDefault(const std::wstring& uri);

private:
	std::shared_ptr<CEngineLog> mLog;
};

/******************************* eof *************************************/