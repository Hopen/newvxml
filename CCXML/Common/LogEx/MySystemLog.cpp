#include "stdafx.h"
#include <thread>
#include "MySystemLog.h"
//#include "../Configuration/SystemConfiguration.h"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace expr = boost::log::expressions;
namespace attr = boost::log::attributes;

template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (
	std::basic_ostream< CharT, TraitsT >& strm, LogLevel lvl)
{
	static const char* const str[] =
	{
		"FT",
		"FI",
		"IN",
		"WN",
		"SV",
		"FA"
	};

	if (static_cast<std::size_t>(lvl) < (sizeof(str) / sizeof(*str)))
		strm << str[lvl];
	else
		strm << static_cast<int>(lvl);
	return strm;
}


CMySystemLog::CMySystemLog(
	const std::wstring& aFileName, 
	LogLevel aLevel, 
	bool aNeedConsole)
	: m_level(aLevel)
{
	//m_scriptId = ObjectIdToStr(aScriptId);

	m_file_sink = logging::add_file_log
		(
			keywords::file_name = aFileName,
			keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
			keywords::format = expr::stream 
			<< expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%H:%M:%S.%f") //-V747
			<< " |" << expr::attr<LogLevel>("Severity") << "| " 
			//<< "P" << _getpid() << " T" << std::this_thread::get_id() << "| "
			//<< "S" << scriptId << " | "
			<< expr::message,
			keywords::rotation_size = 1024 * 1024 * 1024,
			keywords::open_mode = (std::ios::out | std::ios::app),
			keywords::auto_flush = true
			);

	if (aNeedConsole)
	{
		m_console_sink = logging::add_console_log(std::clog, keywords::format = expr::stream
			<< expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%H:%M:%S.%f") //-V747
			<< " | " << expr::attr<LogLevel>("Severity") << " | " << "  "
			<< expr::message);
	}

	logging::add_common_attributes();
}

CMySystemLog::~CMySystemLog()
{
	logging::core::get()->remove_sink(m_file_sink);
	m_file_sink.reset();

	if (m_console_sink)
	{
		logging::core::get()->remove_sink(m_console_sink);
		m_console_sink.reset();
	}
}
