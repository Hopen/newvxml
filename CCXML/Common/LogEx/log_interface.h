#pragma once
#include <memory>
#include <string>
#include <boost/algorithm/string/predicate.hpp>

enum LogLevel
{
	LEVEL_FINEST,
	LEVEL_FINE,
	LEVEL_INFO,
	LEVEL_WARNING,
	LEVEL_SEVERE,
	LEVEL_FATAL
};

inline LogLevel translateLogLevel(const std::wstring& _level)
{
	if (boost::iequals(_level, L"fine"))
	{
		return LEVEL_FINE;
	}
	if (boost::iequals(_level, L"finest"))
	{
		return LEVEL_FINEST;
	}
	if (boost::iequals(_level, L"info"))
	{
		return LEVEL_INFO;
	}
	if (boost::iequals(_level, L"warning"))
	{
		return LEVEL_WARNING;
	}
	if (boost::iequals(_level, L"severe"))
	{
		return LEVEL_SEVERE;
	}
	if (boost::iequals(_level, L"fatal"))
	{
		return LEVEL_FATAL;
	}

	if (boost::iequals(_level, L"none"))
	{
		return LEVEL_FATAL;
	}

	if (boost::iequals(_level, L"all"))
	{
		return LEVEL_FINEST;
	}

	return LEVEL_INFO;
}
 
class log_interface
{
public:
	virtual ~log_interface() {}

	virtual bool check_severity(LogLevel _level)							= 0;
	virtual void write_log(LogLevel _level, const std::wstring& _logEntry)	= 0;
};

using log_pointer = std::shared_ptr<log_interface>;
