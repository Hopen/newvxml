#pragma once

#include "../Patterns/singleton.h"
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/detail/process_id.hpp>
#include <boost/log/detail/thread_id.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/format.hpp>
#include "../Patterns/string_functions.h"
#include "log_interface.h"
#include "ThreadSaveSingleton.h"


#ifndef MODULE_SPACE 
#define MODULE_SPACE L"20"
#endif

namespace src = boost::log::sources;
namespace logging = boost::log;

class CMySystemLog
{
public:
	using log_sink = boost::shared_ptr<boost::log::sinks::synchronous_sink<boost::log::sinks::text_file_backend>>;
	//using console_sink = boost::shared_ptr<boost::log::sinks::synchronous_sink<boost::log::sinks::text_file_backend>>;
	//using console_sink = boost::shared_ptr< boost::log::sinks::synchronous_sink< boost::log::sinks::basic_text_ostream_backend< wchar_t> >>;
	using console_sink = boost::shared_ptr< boost::log::sinks::synchronous_sink<boost::log::sinks::text_ostream_backend> >;

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& aScriptId, const std::wstring& formatMessage, TArgs&&... args)
	{
		if (level <m_level)
			return;

		std::wstring strFormat = (boost::wformat(L"%s|[p%d-t%d]|\t%s") 
			% aScriptId.c_str()
			% GetCurrentProcessId() 
			% GetCurrentThreadId() 
			% formatMessage).str();

		LogString(level, strFormat, std::forward<TArgs>(args)...);
	}

	template<typename... TArgs>
	void LogString(LogLevel lvl, const std::wstring& fmt, TArgs&&... args)
	{
		if (lvl < m_level)
			return;

		boost::wformat message(fmt);
		LogMessage(lvl, message, std::forward<TArgs>(args)...);
	}

public:
	CMySystemLog() = default;
	CMySystemLog(
		const std::wstring& aFileName, 
		LogLevel aLevel, 
		bool aNeedConsole = false);

	~CMySystemLog();

	void LogMessage(LogLevel level, boost::wformat& message) 
	{
		BOOST_LOG_SEV(m_lg, level) << wtos(message.str());
	}

	template<typename TValue, typename... TArgs>
	void LogMessage(LogLevel lvl, boost::wformat& message, TValue&& arg, TArgs&&... args) 
	{
		message % arg;
		LogMessage(lvl, message, std::forward<TArgs>(args)...);
	}

	LogLevel GetLevel() const
	{
		return m_level;
	}

	log_sink m_file_sink;
	console_sink m_console_sink;
	LogLevel m_level;
	src::severity_logger_mt<LogLevel>	m_lg;
};