#pragma once
#include <mutex>
#include <boost/noncopyable.hpp>

template <class T>
class ThreadSaveSingleton : public boost::noncopyable
{
	static T* mInstance;
	static LONG	mCounter;
	static std::mutex mMutex1;
	static std::mutex mMutex2;
public:

	template <class... TArgs>
	static T * GetInstance(TArgs&&... aArgs)
	{
		std::lock_guard<std::mutex> lock1(mMutex1);
		if (mInstance == NULL)
		{
			std::lock_guard<std::mutex> lock2(mMutex2);
			if (mInstance == NULL)
			{
				mInstance = new T(std::forward<TArgs>(aArgs)...);
			}
		}
		
		::InterlockedIncrement(&mCounter);
		return mInstance;
	}

	static DWORD ReleaseInstance()
	{
		::InterlockedDecrement(&mCounter);
		if(mCounter <= 0 && mInstance != NULL)
		{
			delete mInstance;
			mInstance = NULL;
		}
		return mCounter;
	}
};

template <class T>
LONG ThreadSaveSingleton<T>::mCounter = 0;

template <class T>
T * ThreadSaveSingleton<T>::mInstance = NULL;

template <class T>
std::mutex ThreadSaveSingleton<T>::mMutex1;

template <class T>
std::mutex ThreadSaveSingleton<T>::mMutex2;
