#include "StdAfx.h"
#include "binaryfile.h"
#include "..\exceptions\SystemException.h"

CBinaryFile::CBinaryFile(void)
{
	m_hFile = INVALID_HANDLE_VALUE;
}

CBinaryFile::~CBinaryFile(void)
{
	if(m_hFile != INVALID_HANDLE_VALUE)
		Close();
}


void CBinaryFile::Open(LPCTSTR _lpszFileName, DWORD dwFlags)
{
	DWORD dwShare		= (dwFlags == GENERIC_READ) ? FILE_SHARE_READ : FILE_SHARE_WRITE;
	DWORD dwCreation	= (dwFlags == GENERIC_READ) ? OPEN_EXISTING : CREATE_ALWAYS;

	m_hFile = CreateFile(_lpszFileName, dwFlags, dwShare, NULL, dwCreation, FILE_ATTRIBUTE_NORMAL, NULL);
	if(m_hFile == INVALID_HANDLE_VALUE)
		THROW_SYSTEM_EXCEPTION();
}

DWORD CBinaryFile::Read(void * pBuffer, size_t size)
{
	DWORD dwWritten;
	if(!ReadFile(m_hFile, pBuffer, size, &dwWritten, NULL))
		THROW_SYSTEM_EXCEPTION();
	return dwWritten;
}

void CBinaryFile::Write(void * pBuffer, size_t size)
{
	DWORD dwWritten;
	if(!WriteFile(m_hFile, pBuffer, size, &dwWritten, NULL))
		THROW_SYSTEM_EXCEPTION();
}

void CBinaryFile::Close()
{
	if(m_hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
	}
}
