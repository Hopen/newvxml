/************************************************************************/
/* Name     : Common\SystemLogEx.cpp                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solution                                           */
/* Date     : 14 Apr 2019                                               */
/************************************************************************/
#include "stdafx.h"
//#include "Log\SystemLog.h"
#include "SystemLogEx.h"

//void SystemLogEx::Init(ULONGLONG ullSID, LPCWSTR pwszLevel, LPCWSTR pwszFormat, bool extraDate)
//{
//	mLog->SetSID(ullSID);
//	mLog->Init(pwszLevel, pwszFormat, extraDate);
//}

//void SystemLogEx::LogFinest(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->LogString(LEVEL_FINEST, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}
//
//void SystemLogEx::LogInfo(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->LogString(LEVEL_INFO, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}
//
//void SystemLogEx::LogWarning(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->LogString(LEVEL_WARNING, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}
//
//void SystemLogEx::LogExtra(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->LogString(LEVEL_INFO, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}

bool SystemLogExImpl::IfLogAvailable() const
{
	if (this->GetLevel() == LEVEL_FINEST)
	{
		return true;
	}
	return false;
}

//template class log_initializer<CVXMLEngineConfig>;
//template class CSystemLogImpl<CVXMLEngineConfig>;

/******************************* eof *************************************/