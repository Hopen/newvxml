/************************************************************************/
/* Name     : Common\EngineConfig.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 29 Nov 2011                                               */
/************************************************************************/
#pragma once

#include <string>
#include "sv_helpclasses.h"
#include "sv_utils.h"
#include "ce_xml.hpp"
#include "Patterns/singleton.h"

extern HINSTANCE g_hInst;

template <typename CLASS>
//class CEngineConfig : public CMeyerSingleton<CLASS>
class CEngineConfig : public static_singleton<CLASS>
{
	friend class static_singleton<CEngineConfig>;

public:
	std::wstring sLogFileName;
	std::wstring m_sExtraLogName;
	std::wstring sLevel;

protected:
	LiteXML::TElem m_config;
	std::wstring m_sFileName;

	void LoadDefaultParams()
	{
	}

	virtual void LoadEngineConfig()
	{
		try
		{ 
			//LiteXML::TElem config   = LiteXML::LoadXML(m_sFileName.c_str()); 
			m_config                = LiteXML::LoadXML(m_sFileName.c_str()); 
			this->sLogFileName		= m_config.attr(L"//configuration/Log/FileNameGenerator/Format");
			this->m_sExtraLogName   = m_config.attr(L"//configuration/Log/FileNameGenerator/Extra");
			this->sLevel            = m_config.attr(L"//configuration/Log/FileNameGenerator/Level");
		} 
		catch (...) 
		{ 
		} 
	}

public:
	CEngineConfig()
	{
		LoadDefaultParams();

		try
		{
			m_sFileName = SVUtils::GetModuleFileName(g_hInst) + L".config";
			//CXmlConfigSingleton::LoadFromFile(m_sFileName);
			LoadEngineConfig();
		}
		catch (...)
		{
		}
	}

	void Cleanup()
	{
	}

	virtual ~CEngineConfig()
	{
	}
};

/******************************* eof *************************************/