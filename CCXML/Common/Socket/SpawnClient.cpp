/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnClient.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "tgbot/net/HttpParser.h"
#include "sv_strutils.h"
#include "SpawnClient.h"
//#include "MessageParser.h"
//#include "..\Engine\EngineLog.h"
#include "..\Common\SystemLogEx.h"

//using namespace enginelog;

namespace spawn_client
{
	const int TCP_PACK_SIZE = 1024;
	static unsigned int _count = 0;

	bool makeClient(TConnectionClientParams& params, SystemLogEx * pLog)
	{
		auto read_complete_predicate = [&](int data_len)
		{
			if (!data_len)
				return true;
			return 	data_len < TCP_PACK_SIZE;
		};
		std::wstring clientName = L"Client " + std::to_wstring(++_count);
		boost::asio::io_service io_service;

		boost::asio::ip::tcp::resolver resolver(io_service);
		boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

		boost::asio::ip::tcp::socket socket(io_service);
		
		boost::asio::steady_timer timer(io_service);
		boost::asio::io_service::strand strand(io_service);
		timer.expires_from_now(std::chrono::seconds(params._timeout));

		boost::asio::spawn(strand, [&](boost::asio::yield_context yield)
		{
			pLog->LogInfo( L"%s: Connecting to: %s:%s", clientName.c_str(), stow(params._uri).c_str(), stow(params._port).c_str());

			std::string in_data;

			boost::system::error_code errorCode;

			try
			{
				socket.async_connect(endpoint, yield[errorCode]);

				if (errorCode)
				{
					throw errorCode;
				}
				pLog->LogInfo(L"%s: Write: %s", clientName.c_str(), stow(params._request).c_str());

				socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);

				if (errorCode)
				{
					throw errorCode;
				}

				int len = 0;

				boost::asio::streambuf buff_response;
				size_t bytes_transferred = 0;
				do
				{
					char reply_[TCP_PACK_SIZE] = {};
					int len = socket.async_read_some(boost::asio::buffer(reply_), yield);
					if (errorCode)
					{
						throw errorCode;
					}

					bytes_transferred += len;

					in_data.append(reply_, len);
				} while (in_data.find("\r\n\r\n") == std::string::npos);

				size_t num_additional_bytes = bytes_transferred - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));
				TgBot::HttpParser::HeadersMap headers;
				TgBot::HttpParser::getInstance().parseResponse(in_data, headers);

				auto header_it = headers.find("content-length");
				if (header_it == headers.end())
				{
					throw std::runtime_error(format_string("Invalid http header: %s", in_data.c_str()));
				}
				auto content_length = stoull(header_it->second);
				if (content_length > num_additional_bytes)
				{
					do
					{
						char reply_[TCP_PACK_SIZE] = {}; //ToDo: change buffer size in depandence on content-length
						int len = socket.async_read_some(boost::asio::buffer(reply_), yield);
						if (errorCode)
						{
							throw errorCode;
						}

						num_additional_bytes += len;

						in_data.append(reply_, len);

					} while (content_length > num_additional_bytes);
				}

			}
			catch (boost::system::error_code& error)
			{
				params._error = true;

				if (error != boost::asio::error::eof)
					pLog->LogWarning( L"%s: Socket failed with error: %s", clientName.c_str(), stow(error.message()).c_str());
			}
			catch (std::runtime_error & error)
			{
				params._error = true;

				pLog->LogWarning( L"%s: Socket failed with error: %s", clientName.c_str(), stow(error.what()).c_str());
			}
			catch (...)
			{
				params._error = true;

				pLog->LogWarning( L"%s: Socket failed with error: unhandled exception caught", clientName.c_str());
			}

			timer.cancel();
			socket.close();

			pLog->LogInfo( L"%s: Read: %s", clientName.c_str(), stow(in_data).c_str());
			params._response = in_data;
		});

		boost::asio::spawn(strand,
			[&](boost::asio::yield_context yield)
		{
			while (socket.is_open())
			{
				boost::system::error_code ignored_ec;
				timer.async_wait(yield[ignored_ec]);
				if (timer.expires_from_now() <= std::chrono::seconds(0))
				{
					pLog->LogWarning( L"%s: Socket timeout", clientName.c_str());
					if (socket.is_open())
					{
						socket.close();
						pLog->LogWarning( L"%s: Close socket by timeout", clientName.c_str());
					}
				}
			}
		});

		io_service.run();

		int test = 0;
		return false;
	}
}



/******************************* eof *************************************/