/************************************************************************/
/* Name     : MCHAT\MessageParser.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jul 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "MessageParser.h"

#include "codec.h"

std::wstring create_escapes2(const std::wstring &s)
{
	return stow(to_utf8(s));
}

std::wstring create_escapes3(const std::wstring &s)
{
	return s;
}

namespace boost {
	namespace property_tree {
		namespace json_parser {

			template<class Ptree>
			void write_json_helper2(std::basic_ostream<typename Ptree::key_type::value_type> &stream,
				const Ptree &pt,
				int indent, bool pretty)
			{

				typedef typename Ptree::key_type::value_type Ch;
				typedef typename std::basic_string<Ch> Str;

				// Value or object or array
				if (indent > 0 && pt.empty())
				{
					// Write value
					Str data = create_escapes2(pt.template get_value<Str>());
					stream << Ch('"') << data << Ch('"');

				}
				else if (indent > 0 && pt.count(Str()) == pt.size())
				{
					// Write array
					stream << Ch('[');
					if (pretty) stream << Ch('\n');
					typename Ptree::const_iterator it = pt.begin();
					for (; it != pt.end(); ++it)
					{
						if (pretty) stream << Str(4 * (indent + 1), Ch(' '));
						write_json_helper2(stream, it->second, indent + 1, pretty);
						if (boost::next(it) != pt.end())
							stream << Ch(',');
						if (pretty) stream << Ch('\n');
					}
					if (pretty) stream << Str(4 * indent, Ch(' '));
					stream << Ch(']');

				}
				else
				{
					// Write object
					stream << Ch('{');
					if (pretty) stream << Ch('\n');
					typename Ptree::const_iterator it = pt.begin();
					for (; it != pt.end(); ++it)
					{
						if (pretty) stream << Str(4 * (indent + 1), Ch(' '));
						stream << Ch('"') << create_escapes3(it->first) << Ch('"') << Ch(':');
						if (pretty) stream << Ch(' ');
						write_json_helper2(stream, it->second, indent + 1, pretty);
						if (boost::next(it) != pt.end())
							stream << Ch(',');
						if (pretty) stream << Ch('\n');
					}
					if (pretty) stream << Str(4 * indent, Ch(' '));
					stream << Ch('}');
				}

			}

			// Verify if ptree does not contain information that cannot be written to json
			template<class Ptree>
			bool verify_json2(const Ptree &pt, int depth)
			{

				typedef typename Ptree::key_type::value_type Ch;
				typedef typename std::basic_string<Ch> Str;

				// Root ptree cannot have data
				if (depth == 0 && !pt.template get_value<Str>().empty())
					return false;

				// Ptree cannot have both children and data
				if (!pt.template get_value<Str>().empty() && !pt.empty())
					return false;

				// Check children
				typename Ptree::const_iterator it = pt.begin();
				for (; it != pt.end(); ++it)
					if (!verify_json2(it->second, depth + 1))
						return false;

				// Success
				return true;

			}
			// Write ptree to json stream
			template<class Ptree>
			void write_json_internal2(std::basic_ostream<typename Ptree::key_type::value_type> &stream,
				const Ptree &pt,
				const std::string &filename,
				bool pretty)
			{
				if (!verify_json2(pt, 0))
					BOOST_PROPERTY_TREE_THROW(json_parser_error("ptree contains data that cannot be represented in JSON format", filename, 0));
				write_json_helper2(stream, pt, 0, pretty);
				stream << std::endl;
				if (!stream.good())
					BOOST_PROPERTY_TREE_THROW(json_parser_error("write error", filename, 0));
			}


			template<class Ptree>
			void write_json2(std::basic_ostream<
				typename Ptree::key_type::value_type
			> &stream,
				const Ptree &pt,
				bool pretty = true)
			{
				write_json_internal2(stream, pt, std::string(), pretty);
			}

		}
	}
}

namespace CMessageParser
{
	boost::property_tree::wptree _toPropertyTree(const CMessage& msg)
	{
		boost::property_tree::wptree requestTree;

		std::for_each(msg.cbegin(), msg.cend(), [&requestTree](std::pair<const std::wstring, core::message_parameter> basic_tree)
		{
			if (basic_tree.second.IsMessageArray()) // messages list
			{
				auto messages = basic_tree.second.AsMessagesVector();
				boost::property_tree::wptree messagesTree;
				for (const auto& message : messages)
				{
					messagesTree.push_back(std::make_pair(L"", _toPropertyTree(message)));
				}
				requestTree.add_child(basic_tree.first, messagesTree);
			}
			else if (basic_tree.second.IsRawData()) // message
			{
				auto message = basic_tree.second.AsMessage();
				requestTree.add_child(basic_tree.first, _toPropertyTree(message));
			}
			else // simple param
			{
				requestTree.put(basic_tree.first, basic_tree.second.AsWideStr());
			}
		});

		return requestTree;
	}

	std::wstring ToStringUnicode(const CMessage& msg)
	{
		return core::support::ptree_parser::tree_to_json_string(_toPropertyTree(msg));
	}

	std::wstring ToStringUtf(const CMessage& msg)
	{
		//return core::support::ptree_parser::tree_to_json_string();

		boost::property_tree::wptree _tree = _toPropertyTree(msg);

		std::wostringstream os;
		boost::property_tree::json_parser::write_json2(os, _tree, false);
		auto result = os.str();
		return result.substr(0, result.length() - 1);
	}

	CMessage __toMessage(const boost::property_tree::wptree& aTree);

	MessageList __toVector(const boost::property_tree::wptree& aTree)
	{
		MessageList list;
		for (const auto& basic_tree : aTree)
		{
			list.push_back(__toMessage(basic_tree.second));
		}
		return list;
	}

	CMessage __toMessage(const boost::property_tree::wptree& aTree)
	{
		CMessage msg;
		for (const auto& basic_tree : aTree)
		{
			if (basic_tree.second.size())
			{
				if (basic_tree.second.begin()->first.empty()) //vector
				{
					MessageList msg_vector = __toVector(basic_tree.second);
					msg[basic_tree.first] = core::to_raw_data(msg_vector);
				}
				else // single message
				{
					CMessage child_msg = __toMessage(basic_tree.second);
					msg[basic_tree.first] = child_msg;
				}
			}
			else
			{
				msg[basic_tree.first] = basic_tree.second.data();
			}
		}
		return msg;
	}

	CMessage ToMessage(const std::wstring& aData)
	{
		if (aData.empty())
		{
			throw std::runtime_error("Parse string to Message - null string");
		}

		boost::property_tree::wptree requestTree = core::support::ptree_parser::tree_from_string_json(aData);
		return __toMessage(requestTree);
	}
}

/******************************* eof *************************************/