/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnClient.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "VXMLBase.h"

class CEngineLog;

namespace spawn_client
{
	struct TConnectionClientParams
	{
		std::string _uri;
		std::string _port;
		uint32_t  _clientId;

		std::string _request;
		std::string _response;

		size_t _timeout = VXML::DEFAULT_TIMEOUT_SEC;

		bool _error = false;
	};

	bool makeClient(TConnectionClientParams& params, SystemLogEx *pLog);
}

/******************************* eof *************************************/