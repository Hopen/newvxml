/************************************************************************/
/* Name     : Common\alarmmsg.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 23 Oct 2014                                               */
/************************************************************************/

#pragma once

#ifndef _ALARMMSG_H
#define _ALARMMSG_H

#include <ATLComTime.h>
#include <atlstr.h>
#include "ProcessHelper.h"
#include <boost/algorithm/string.hpp>

#include "EvtModel.h"
#include "..\Engine\Engine.h"

namespace alarmmsg{
	namespace CLIENT_ADDRESS{
		const __int64 ANY_CLIENT = ((__int64)0x00000002 << 32);
		const __int64 ALL_CLIENT = ((__int64)0x00000000);
	}
}

__int64 FormatTimeStamp(const SYSTEMTIME& systime)
{
	FILETIME fileTime;
	::SystemTimeToFileTime(&systime, &fileTime);
	return *((__int64 *)&fileTime);
}

std::wstring FormatTime(const SYSTEMTIME& systime)
{
	std::wstring strResult = format_wstring(_T("%02i/%02i/%04i %02i:%02i:%02i"), 
		systime.wDay, systime.wMonth, systime.wYear,
		systime.wHour, systime.wMinute, systime.wSecond);

	return strResult;
}


//CMessage AlarmMsg(const int& iError, const std::wstring& sSubSystem, const std::wstring& _description)
CMessage AlarmMsg(const std::wstring& sModule, const std::wstring& sScriptID, const std::wstring& sSubSystem, const std::wstring& _error_name, const std::wstring& _description, const std::wstring& _probErrorCause, const int& iError)
{
	SYSTEMTIME eventTime, importanceTime;
	::GetSystemTime(&eventTime);
	ZeroMemory(&importanceTime, sizeof(importanceTime));
	importanceTime.wMinute = 2;

	COleDateTime odtNow = COleDateTime::GetCurrentTime();
	SYSTEMTIME formatTime;
	odtNow.GetAsSystemTime(formatTime);

	CMessage message;
	message.Name = L"SYSTEM_ALARM";
	message[L"DestinationAddress"] = alarmmsg::CLIENT_ADDRESS::ALL_CLIENT;
	message[L"AlarmID"] = 0;// (__int64(rand()) << 32) | rand();;
	message[L"Severity"]    = "Major";

	message[L"ActivationTime"]  = FormatTime(formatTime);
	message[L"ActivationTimestamp"] = FormatTimeStamp(eventTime);

	message[L"ImportanceTime"]  = FormatTime(importanceTime);
	message[L"ImportanceTimestamp"] = importanceTime.wMinute * 60 * 1000; 

	message[L"Machine"  ]   = CProcessHelper::GetComputerName();
	message[L"Module"   ]   = sModule.c_str();//CProcessHelper::GetCurrentModuleName();
	message[L"SubModule"]   = sScriptID.c_str();
	message[L"SubSystem"]   = sSubSystem.c_str();
	message[L"ErrorCode"]   = iError;
	message[L"ErrorName"]   = _error_name.c_str();
	message[L"ErrorDescription"]  = _description.c_str();
	
	message[L"ProbErrorCause"] = boost::replace_all_copy(_probErrorCause, L"'", L"").c_str();

	return message;
}

#endif //#ifndef _ALARMMSG_H
