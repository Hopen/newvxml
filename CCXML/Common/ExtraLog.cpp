/************************************************************************/
/* Name     : Common\ExtraLog.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solution                                           */
/* Date     : 14 Apr 2019                                               */
/************************************************************************/
#include "stdafx.h"

//#include "..\Engine\EngineLog.h"
#include "ExtraLog.h"

using namespace enginelog;

ExtraLog::ExtraLog()
	: mLog(std::make_shared<CEngineLog>())
{

}

void ExtraLog::Init(ULONGLONG ullSID, LPCWSTR pwszLevel, LPCWSTR pwszFormat, bool extraDate)
{
	mLog->SetSID(ullSID);
	mLog->Init(pwszLevel, pwszFormat, extraDate);
}

//void ExtraLog::LogFinest(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->Log(LEVEL_FINEST, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}
//
//void ExtraLog::LogInfo(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->Log(LEVEL_INFO, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}
//
//void ExtraLog::LogWarning(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->Log(LEVEL_WARNING, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}
//
//void ExtraLog::LogExtra(LPCWSTR pwszFmt, ...)
//{
//	va_list arglist;
//	va_start(arglist, pwszFmt);
//	mLog->LogLarge(LEVEL_INFO, __FUNCTIONW__, pwszFmt, arglist);
//	va_end(arglist);
//}

void ExtraLog::InitDefault(const std::wstring& uri)
{
	LEVEL level = LEVEL::LEVEL_SEVERE;
	ULONGLONG sid = 0;
	if (mLog)
	{
		level = mLog->GetLevel();
		sid = mLog->GetSID();
	}
	mLog = std::make_shared<CEngineLog>();
	mLog->Init(level, uri.c_str(), true);
	mLog->SetSID(sid);
}

/******************************* eof *************************************/