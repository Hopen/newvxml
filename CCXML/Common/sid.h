/************************************************************************/
/* Name     : Common\sid.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 25 Mar 2015                                               */
/************************************************************************/
#pragma once
#include <vector>

using ObjectId = /*unsigned*/ __int64 ;
using ObjectIdCollector = std::vector<ObjectId>;

///* Global variable */
//namespace Global
//{
//	extern ObjectId g_scriptID;
//}


//class CScriptIDHolder : public singleton < CScriptIDHolder >
//{
//	friend class singleton < CScriptIDHolder > ;
//
//private:
//	CScriptIDHolder() :m_scriptID(0)
//	{
//
//	}
//
//public:
//	void SetScriptID(__int64 _scriptID)
//	{
//		m_scriptID = _scriptID;
//	}
//
//	__int64 GetScriptID()const
//	{
//		return m_scriptID;
//	}
//
//private:
//	__int64 m_scriptID;
//};

ObjectId GetGlobalScriptId();
void SetGlobalScriptId(ObjectId aObjectId);



/******************************* eof *************************************/