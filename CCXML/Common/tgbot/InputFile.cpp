#include "stdafx.h"
#include "tgbot\types\InputFile.h"
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
//#include <boost/filesystem/fstream.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

TgBot::InputFile::InputFile(std::string _fileName):fileName(_fileName)
{
	try
	{
		boost::filesystem::path file(fileName);
		if (boost::filesystem::exists(file))
		{
			boost::iostreams::mapped_file mfile(file);
			data = mfile.data();
		}
	}
	catch (const boost::filesystem::filesystem_error& ex)
	{
		//m_log->LogString(LEVEL_WARNING, /*__FUNCTIONW__,*/ _T("Exception when reading backup file list: \"%s\" "), ex.what());
		throw ex;
	}
}