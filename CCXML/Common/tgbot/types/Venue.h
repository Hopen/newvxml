//
// Created by Andrea Giove on 17/04/16.
//

#ifndef TGBOT_VENUE_H
#define TGBOT_VENUE_H

#include <memory>
#include <string>

#include "tgbot/types/Location.h"
#include "Patterns/string_functions.h"

namespace TgBot {

/**
 * This object represents a venue.
 * @ingroup types
 */
class Venue {
public:
	typedef std::shared_ptr<Venue> Ptr;

	/**
	 * Venue location.
	 */
	Location::Ptr location;

	/**
	 * Name of the venue.
	 */
	std::string title;

	/**
	 * Address of the venue.
	 */
	std::string address;

	/**
	 * Optional. Foursquare identifier of the venue.
	 */
	std::string foursquare_id;

	template <class T>
	std::wstring print(wchar_t name[], const T& t)const
	{
		if (!t)
			return L"";

		std::wostringstream out;
		out << name << L": ";
		out << t->Dump();
		out << L"; ";

		return out.str();
	}

	std::wstring Dump() const
	{
		std::wostringstream out;
		out << L"Venue [";
		out << print(L"location", location);
		out << L"title: " << stow(title) << L"; ";
		out << L"address: " << stow(address) << L"; ";
		out << L"foursquare_id: " << stow(foursquare_id) << L"]; ";

		return out.str();
	}
};
}

#endif //TGBOT_VENUE_H
