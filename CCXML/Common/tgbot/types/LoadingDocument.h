/************************************************************************/
/* Name     : MTELE\...\LoadingDocument.h                               */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 25 Nov 2016                                               */
/************************************************************************/
#pragma once

#include <string>
#include <memory>

#include "Patterns/string_functions.h"

namespace TgBot {

/**
 * This object represents a general file (as opposed to photos and audio files).
 * @ingroup types
 */
class LoadingDocument {

public:
	typedef std::shared_ptr<LoadingDocument> Ptr;

	/**
	 * Unique file identifier.
	 */
	std::string fileId;

	/**
	 * Optional. 
	 */
	std::string filePath;

	/**
	 * Optional. File size.
	 */
	int32_t fileSize;

	template <class T>
	std::wstring print(wchar_t name[], const T& t)const
	{
		if (!t)
			return L"";

		std::wostringstream out;
		out << name << L": ";
		out << t->Dump();
		out << L"; ";

		return out.str();
	}

	std::wstring Dump() const
	{
		std::wostringstream out;
		out << L"LoadingDocument [fileId: " << stow(fileId) << L"; ";
		out << L"fileName: " << stow(filePath) << L"; ";
		out << L"fileSize: " << fileSize << L"]; ";

		return out.str();
	}
};

}

