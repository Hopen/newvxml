/************************************************************************/
/* Name     : CCXML\Common\factory_c.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 09 Apr 2018                                               */
/************************************************************************/

#pragma once

#include <sstream>
#include "SAXXmlDataTypes.h"

namespace Factory_c
{
	class CFTag : public ISATXMLElement
	{
	public:
		template<class Archive>
		void serialize(Archive & archive)
		{
			archive(
				cereal::base_class<ISATXMLElement>(this)
				);
		}

		CFTag() = default;

		explicit CFTag(const ISATXMLElement& rhs)
			: ISATXMLElement(rhs)
		{
			//m_sName = rhs.GetName();
			//m_sText = rhs.GetText();
			//m_iLine = rhs.GetLine();
			//m_Attribs = rhs.GetAttr();
			//m_sXML = rhs.GetXML();
			//m_sParams = rhs.GetParams();
			//m_sUri = rhs.GetUri();
		}
		
		CFTag(const std::wstring& aName,
			const std::wstring& aText,
			const AttrContainer& aAttribs,
			int aLine,
			const std::wstring& aXML,
			const std::wstring& aParams, 
			const std::wstring& aUri)
			: ISATXMLElement(aName,
				aText,
				aAttribs,
				aLine,
				aXML,
				aParams,
				aUri)
		{}

	};

	class CCollector;
	using CollectorPtr = std::shared_ptr<CCollector>;
	using CollectorWPtr = std::weak_ptr<CCollector>;


	template <class... TArgs>
	CollectorPtr CreateCollector(TArgs&&... aArgs)
	{
		return std::make_shared<CCollector>(std::forward<TArgs>(aArgs)...);
	}

	class CCollector : public CFTag, public std::enable_shared_from_this<CCollector>
	{
	public:
		using TagContainer = std::vector<CollectorPtr>;

	public:
		template<class Archive>
		void serialize(Archive & archive)
		{
			archive(
				cereal::base_class<CFTag>(this),
				m_Children,
				m_pParent
				);
		}

		CCollector()
		{}

		explicit CCollector(const CFTag& rhs)
			: CFTag(rhs)
		{
		}

		explicit CCollector(const ISATXMLElement& rhs)
			: CFTag(rhs)
		{
		}

		explicit CCollector(const CCollector& rhs)
		{
			m_sName = rhs.GetName();
			m_sText = rhs.GetText();
			m_iLine = rhs.GetLine();
			m_Attribs = rhs.GetAttr();
			m_sXML = rhs.GetXML();
			m_sParams = rhs.GetParams();
			m_sUri = rhs.GetUri();
			m_Children = rhs.m_Children;
			m_pParent = rhs.m_pParent;
		}

		CollectorPtr Add(const CollectorPtr& _child)
		{
			_child->SetParent(shared_from_this());
			m_Children.push_back(_child);
			return m_Children[m_Children.size() - 1];
		}
		const TagContainer& GetChildren()const { return m_Children; }
		void SetParent(const CollectorPtr& _parent) { m_pParent = _parent; }
		CollectorWPtr GetParent()const { return m_pParent; }

	private:
		TagContainer m_Children;
		CollectorWPtr m_pParent;
	};

	class CMrcpFactory :public CBaseMrcp
	{
	public:
		template<class Archive>
		void serialize(Archive & archive)
		{
			archive(
				cereal::base_class<CBaseMrcp>(this)
				);
		}

		CMrcpFactory() = default;
		~CMrcpFactory() {}

		explicit CMrcpFactory(const CBaseMrcp& rhs)
		{
			m_sInput = rhs.GetInput();
			m_sConfidence = rhs.GetConfidence();
			m_sSWI_meaning = rhs.GetSWImeaning();
		}
		void operator=(const CBaseMrcp& rhs)
		{
			m_sInput = rhs.GetInput();
			m_sConfidence = rhs.GetConfidence();
			m_sSWI_meaning = rhs.GetSWImeaning();
		}
	};
}


/******************************* eof *************************************/