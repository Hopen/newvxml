#ifndef LOCKEDQUEUE_H
#define LOCKEDQUEUE_H

#include <queue>
//#include <thread>
//#include <condition_variable>
//#include <mutex>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

template <typename T>
class locked_queue
{
	typedef boost::mutex		mutex;

	std::queue<T>				m_queue;
	boost::condition_variable	m_cond;
	mutex						m_mut;

public:
	locked_queue() 
	{

	}

	~locked_queue()
	{

	}

	void push(const T& _value)
	{
		boost::lock_guard<mutex> lock(m_mut);
		m_queue.push(_value);
		m_cond.notify_one();
	}

	void push(T&& _value)
	{
		boost::lock_guard<mutex> lock(m_mut);
		m_queue.push(std::move(_value));
		m_cond.notify_one();
	}

	T& front()
	{
		boost::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, boost::bind(&locked_queue<T>::not_empty, this));
		return m_queue.front();
	}

	void pop()
	{
		boost::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, boost::bind(&locked_queue<T>::not_empty, this));
		m_queue.pop();
	}

	void pop_front(T& _value)
	{
		boost::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, boost::bind(&locked_queue<T>::not_empty, this));
		_value = m_queue.front();
		m_queue.pop();
	}

	T pop_front()
	{
		boost::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, boost::bind(&locked_queue<T>::not_empty, this));
		T result = m_queue.front();
		m_queue.pop();
		return result;
	}

	bool empty()
	{
		return m_queue.empty();
	}

	bool not_empty()
	{
		return !m_queue.empty();
	}

	void clear()
	{
		boost::unique_lock<mutex> lock(m_mut);
		std::queue<T> empty_queue;
		m_queue.swap(empty_queue);
	}

};

#endif //LOCKEDQUEUE_H
