/************************************************************************/
/* Name     : CCXML\Base.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : CCXML                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 09 Sep 2018                                               */
/************************************************************************/
#pragma once
#include <string>

using MCCID = long long;
using ChatId = std::wstring;

#pragma once