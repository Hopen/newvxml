/************************************************************************/
/* Name     : CacheInterface\SAXXmlElementHandler.h                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/

#pragma once
#import <msxml6.dll> raw_interfaces_only 
#include "SAXXmlDataTypes.h"


class ISAXXmlElementHandler
{
public:
	// Handle XML content events during parsing.
	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement) = 0;
	//virtual void OnXmlElementData(const std::wstring& elementData, int depth) = 0;
	virtual void OnXmlElementData(const wchar_t* aData, size_t aSize) = 0;
	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement) = 0;

	// Handle XML error events during parsing.
	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode) = 0;

	// Return true to stop parsing earlier.
	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement) = 0;
};

/******************************* eof *************************************/