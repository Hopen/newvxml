/************************************************************************/
/* Name     : CacheInterface\SAXXmlDataTypes.h                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/

#pragma once
#include <string>
#include <map>
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>
#include <boost/optional.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/types/polymorphic.hpp>

#include "sv_strutils.h"

class ISATXMLElement
{
public:
	ISATXMLElement(){}
	//virtual ~ISATXMLElement(){}
	~ISATXMLElement() {} // !!! MAKE VIRTUAL

	using AttrContainer = std::map<std::wstring, std::wstring>;

	ISATXMLElement(const std::wstring& aName,
		const std::wstring& aText,
		const AttrContainer& aAttribs,
		int aLine,
		const std::wstring& aXML,
		const std::wstring& aParams,
		const std::wstring& aUri)
		: m_sName(aName)
		, m_sText(aText)
		, m_Attribs(aAttribs)
		, m_iLine(aLine)
		, m_sXML(aXML)
		, m_sParams(aParams)
		, m_sUri(aUri)
	{}

	ISATXMLElement(const std::wstring& aName,
		const std::wstring& aText,
		int aLine,
		const std::wstring& aUri)
		: m_sName(aName)
		, m_sText(aText)
		, m_iLine(aLine)
		, m_sUri(aUri)
	{}

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			m_sName,
			m_sText,
			m_Attribs,
			m_iLine,
			m_sXML,
			m_sParams,
			m_sUri);
	}

protected:
	std::wstring	m_sName;
	std::wstring	m_sText;
	AttrContainer	m_Attribs;
	int             m_iLine;
	std::wstring	m_sXML;
	std::wstring    m_sParams;
	std::wstring    m_sUri;


public:
	// Set 
	void SetName(const std::wstring& _name){m_sName = _name;}
	void SetText(const std::wstring& _text){m_sText = _text;}
	void SetAttr(const std::wstring&_attrname, const std::wstring& _attrvalue){m_Attribs[_attrname] = _attrvalue;}
	void SetLine(const int& _line){m_iLine = _line;}
	void SetXML (const std::wstring& _xml ){m_sXML  = _xml;}
	void SetParams(const std::wstring& params){ m_sParams = params; }
	void SetUri(const std::wstring& _uri) { m_sUri = _uri; }

	// Get
	std::wstring  GetName() const {return m_sName;}
	std::wstring  GetText() const {return m_sText;}
	AttrContainer GetAttr() const {return m_Attribs;} 
	int           GetLine() const {return m_iLine;}
	std::wstring  GetXML () const {return m_sXML;}
	std::wstring  GetParams() const { return m_sParams; }
	std::wstring  GetUri () const {return m_sUri;}

	std::wstring GetAttrVal(const std::wstring& sName) const
	{
		AttrContainer::const_iterator it = m_Attribs.find(sName);
		if (it == m_Attribs.end())
		{
			throw format_wstring(L"Tag '%s' does not have attribute '%s'", m_sName.c_str(), sName.c_str());
		}
		return it->second;
	}
	bool GetAttrVal(const std::wstring& sName, std::wstring& sValue) const
	{
		AttrContainer::const_iterator it = m_Attribs.find(sName);
		if (it == m_Attribs.end())
		{
			return false;
		}
		sValue = it->second;
		return true;
	}

	boost::optional<std::wstring> GetAttrValSafe(const std::wstring& sName) const
	{
		boost::optional<std::wstring> result = boost::none;

		AttrContainer::const_iterator cit = m_Attribs.find(sName);
		if (cit != m_Attribs.cend())
		{
			result = cit->second;
		}
		return result;
	}


	std::wstring ToString()const
	{
		std::wstring result;
		result += L"{Name: " + m_sName;
		result += L", Text: " + m_sText;
		result += L", Line: " + std::to_wstring(m_iLine);
		result += L", Uri: " + m_sUri;
		result += L"}";
		return result;
	}
	void AppendText(const wchar_t* aData, size_t aSize)
	{
		if (!m_sText.empty())
		{
			m_sText += '\n';
		}
		m_sText.append(aData, aSize);
	}

	void AppendText(const std::wstring& _text)
	{
		m_sText.append(_text);
	}

	void AppendXML(const wchar_t* aData, size_t aSize)
	{
		if (!m_sXML.empty())
		{
			m_sXML += '\n';
		}
		m_sXML.append(aData, aSize);
	}

	void AppendXML(const std::wstring& _text)
	{
		m_sXML.append(_text);
	}
};


class CBaseMrcp
{
public:
	CBaseMrcp() = default;

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			m_sInput,
			m_sConfidence,
			m_sSWI_meaning);
	}
protected:
	std::wstring m_sInput;
	std::wstring m_sConfidence;
	std::wstring m_sSWI_meaning;

public:
	void SetInput      ( const std::wstring& _input      ) { m_sInput = _input;           }
	void SetConfidence ( const std::wstring& _confidence ) { m_sConfidence = _confidence; }
	void SetSWImeaning ( const std::wstring& _meaning    ) { m_sSWI_meaning = _meaning;   }

	std::wstring GetInput      () const { return m_sInput;       }
	std::wstring GetConfidence () const { return m_sConfidence;  }
	std::wstring GetSWImeaning () const { return m_sSWI_meaning; }
};
//class CBasicXmlAttribute
//{
//public:
//	// Constructors / destructor.
//	CBasicXmlAttribute() {}
//	CBasicXmlAttribute(const std::basic_string<T>& name, const std::basic_string<T>& value)
//		: m_name(name), m_value(value) {}
//	~CBasicXmlAttribute() {}
//
//	// Get/set the attribute name.
//	const std::basic_string<T>& GetName() const       { return m_name; }
//	void  SetName(const std::basic_string<T>& name)   { m_name = name; }
//
//	// Get/set the attribute value.
//	const std::basic_string<T>& GetValue() const      { return m_value; }
//	void  SetValue(const std::basic_string<T>& value) { m_value = value; }
//
//	// Get/set both the attribute name and value.
//	void  Get(std::basic_string<T>& name, std::basic_string<T>& value) const;
//	void  Set(const std::basic_string<T>& name, const std::basic_string<T>& value);
//
//	// Return a displayable string representing this attribute.
//	std::basic_string<T> ToString() const;
//
//	// Static helper methods for conversion from SAX2 strings.
//	static void SetAttribute(const wchar_t* pwchAttributeName, wchar_t nameLen,
//		const wchar_t* pwchAttributeValue, wchar_t valueLen,
//		CBasicXmlAttribute<char>& attribute);
//	static void SetAttribute(const wchar_t* pwchAttributeName, wchar_t nameLen,
//		const wchar_t* pwchAttributeValue, wchar_t valueLen,
//		CBasicXmlAttribute<wchar_t>& attribute);
//
//private:
//	std::basic_string<T> m_name;
//	std::basic_string<T> m_value;
//};

/******************************* eof *************************************/