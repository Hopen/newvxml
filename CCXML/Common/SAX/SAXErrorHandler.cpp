/************************************************************************/
/* Name     : CacheInterface\SAXErrorHandler.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "SAXErrorHandler.h"

#define MAX_ERROR_TEXT_SIZE    800

CSAXErrorHandler::CSAXErrorHandler() : m_lRefCount(0)
{
	m_attachElementHandler  = NULL;
}

CSAXErrorHandler::~CSAXErrorHandler()
{
	m_attachElementHandler  = NULL;
}

long __stdcall CSAXErrorHandler::QueryInterface(const struct _GUID &riid,void ** ppvObject)
{
	// Not implemented as this class is not a COM object.
	return 0;
}
/*
unsigned long __stdcall CSAXErrorHandler::AddRef()
{
	// Not implemented as this class is not a COM object.
	return 0;
}

unsigned long __stdcall CSAXErrorHandler::Release()
{
	// Not implemented as this class is not a COM object.
	return 0;
}
*/

HRESULT STDMETHODCALLTYPE CSAXErrorHandler::error( 
	/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator,
	/* [in] */ unsigned short * pwchErrorMessage,
	/* [in] */ HRESULT errCode)
{
	ReportError(pLocator, pwchErrorMessage, errCode);

	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXErrorHandler::fatalError( 
	/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator,
	/* [in] */ unsigned short * pwchErrorMessage,
	/* [in] */ HRESULT errCode)
{
	ReportError(pLocator, pwchErrorMessage, errCode);

	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXErrorHandler::ignorableWarning( 
	/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator,
	/* [in] */ unsigned short * pwchErrorMessage,
	/* [in] */ HRESULT errCode)
{
	return S_OK;
}

void CSAXErrorHandler::AttachElementHandler(ISAXXmlElementHandler* pElementHandler)
{
	m_attachElementHandler  = pElementHandler;
}

void CSAXErrorHandler::DetachElementHandler()
{
	m_attachElementHandler  = NULL;
}

void CSAXErrorHandler::ReportError(
	/* [in] */ MSXML2::ISAXLocator*    pLocator,
	/* [in] */ unsigned short* pwchErrorMessage,
	/* [in] */ HRESULT         errCode)
{
	// Get the line and column where the error occurred.
	int line = 0;
	int column = 0;
	if ( pLocator != NULL )
	{

		pLocator->getLineNumber(&line);
		pLocator->getColumnNumber(&column);
	}

	// Get length of error message.
	int len = wcslen((WCHAR*)pwchErrorMessage);
	len = std::min(len, MAX_ERROR_TEXT_SIZE-1);

	if ( m_attachElementHandler != NULL )
	{
		// Get the error message text.
		wchar_t wszErrorMsg[MAX_ERROR_TEXT_SIZE];
		wcsncpy(wszErrorMsg, (WCHAR*)pwchErrorMessage, len);
		wszErrorMsg[len] = 0;

		// Delegate error to element handler.
		m_attachElementHandler->OnXmlError(line, column, wszErrorMsg, errCode);
	}
}

/******************************* eof *************************************/