/************************************************************************/
/* Name     : CacheInterface\SAXContentHandler.cpp                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "SAXContentHandler.h"

#define MAX_ELEMENT_DATA_SIZE   4096

CSAXContentHandler::CSAXContentHandler() : m_lRefCount(0)
{
	m_depth = 0;

	m_attachElementHandler  = NULL;
}

CSAXContentHandler::~CSAXContentHandler()
{
	//m_pLocator.Release();
	
	m_attachElementHandler  = NULL;
}

long __stdcall CSAXContentHandler::QueryInterface(const struct _GUID &riid,void ** ppvObject)
{
	// Not implemented as this class is not a COM object.
	return 0;
}

// unsigned long __stdcall CSAXContentHandler::AddRef()
// {
// 	// Not implemented as this class is not a COM object.
// 	return 0;
// }
// 
// unsigned long __stdcall CSAXContentHandler::Release()
// {
// 	// Not implemented as this class is not a COM object.
// 	return 0;
// }

HRESULT STDMETHODCALLTYPE CSAXContentHandler::putDocumentLocator( 
	/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator)
{
	m_pLocator = pLocator;
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::startDocument()
{
	m_depth = 0;

	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::endDocument( void)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::startPrefixMapping( 
	/* [in] */ unsigned short * pwchPrefix,
	/* [in] */ int cchPrefix,
	/* [in] */ unsigned short * pwchUri,
	/* [in] */ int cchUri)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::endPrefixMapping( 
	/* [in] */ unsigned short __RPC_FAR *pwchPrefix,
	/* [in] */ int cchPrefix)
{
	return S_OK;
}

//std::wstring Extract(const wchar_t* pwchText, int textLen)
//{
//	return std::wstring(pwchText, pwchText + textLen);
//	
///*
//	std::auto_ptr<wchar_t> wszText(new char(textLen + 1));
//	wcsncpy(wszText.get(), pwchText, textLen);
//	text = wszText.get();
//*/
//}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::startElement( 
	/* [in] */ unsigned short __RPC_FAR *pwchNamespaceUri,
	/* [in] */ int cchNamespaceUri,
	/* [in] */ unsigned short __RPC_FAR *pwchLocalName,
	/* [in] */ int cchLocalName,
	/* [in] */ unsigned short __RPC_FAR *pwchRawName,
	/* [in] */ int cchRawName,
	/* [in] */ MSXML2::ISAXAttributes __RPC_FAR *pAttributes)
{
	int l = 0;

	++m_depth;

	//std::wstring rawName(pwchRawName, pwchRawName + cchRawName);
	//std::wstring NamespaceUri(pwchNamespaceUri, pwchNamespaceUri + cchNamespaceUri);

	int line = 0;
	m_pLocator->getLineNumber(&line);

	if ( m_attachElementHandler != NULL )
	{
		// Factory here!!
		ISATXMLElement xmlElement;

		std::wstring elementName((WCHAR*)pwchLocalName, (WCHAR*)pwchLocalName + cchLocalName);
		xmlElement.SetName(elementName);
		xmlElement.SetLine(line);
		// Add attributes to the start element.
		pAttributes->getLength(&l);
		for (int i = 0; i < l; ++i)
		{
			wchar_t* ln = NULL;
			wchar_t* vl = NULL;
			int lnl = 0, vll = 0;

			pAttributes->getQName(i, (unsigned short**)&ln, &lnl);
			pAttributes->getValue(i, (unsigned short**)&vl, &vll);
			std::wstring attrName(ln, ln + lnl);
			std::wstring attrValue(vl, vl + vll);
			xmlElement.SetAttr(attrName, attrValue);
			if (!attrName.empty())
				xmlElement.SetParams(xmlElement.GetParams() + L" " + attrName + L"=\"" + attrValue + L"\"");

		}

		// Delegate start element to element handler.
		m_attachElementHandler->OnXmlStartElement(xmlElement);
	}

	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::endElement( 
	/* [in] */ unsigned short __RPC_FAR *pwchNamespaceUri,
	/* [in] */ int cchNamespaceUri,
	/* [in] */ unsigned short __RPC_FAR *pwchLocalName,
	/* [in] */ int cchLocalName,
	/* [in] */ unsigned short __RPC_FAR *pwchRawName,
	/* [in] */ int cchRawName)
{
	--m_depth;

	if ( m_attachElementHandler != NULL )
	{
		ISATXMLElement xmlElement;

		std::wstring elementName = ((WCHAR*)pwchLocalName, (WCHAR*)pwchLocalName + cchLocalName);
		xmlElement.SetName(elementName);

		// Delegate end element to element handler.
		m_attachElementHandler->OnXmlEndElement(xmlElement);
	}

	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::characters( 
	/* [in] */ unsigned short __RPC_FAR *pwchChars,
	/* [in] */ int cchChars)
{
	// Let's check if it's all whitespace to avoid unnecessary copying
	// and notifications to the registered element handler.
	bool hasValidChars = false;
	for(int i = 0; i < cchChars; ++i)
	{
		if ( !iswspace(pwchChars[i]) )
		{
			hasValidChars = true;
			break;
		}
	}
	if ( !hasValidChars )
		return S_OK;

	// Get length of characters.
	//int len = std::min(cchChars, MAX_ELEMENT_DATA_SIZE-1);

	//if ( m_attachElementHandler != NULL )
	//{
	//	// Let's do the copying and notify element handler.
	//	wchar_t wszData[MAX_ELEMENT_DATA_SIZE];
	//	wcsncpy(wszData, (WCHAR*)pwchChars, len);
	//	wszData[len] = 0;

	//	// Delegate element data to element handler.
	//	m_attachElementHandler->OnXmlElementData(wszData, m_depth);
	//}

	if (m_attachElementHandler != NULL)
	{
		// Delegate element data to element handler.
		m_attachElementHandler->OnXmlElementData((WCHAR*)pwchChars, cchChars);
	}

	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::ignorableWhitespace( 
	/* [in] */ unsigned short __RPC_FAR *pwchChars,
	/* [in] */ int cchChars)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::processingInstruction( 
	/* [in] */ unsigned short __RPC_FAR *pwchTarget,
	/* [in] */ int cchTarget,
	/* [in] */ unsigned short __RPC_FAR *pwchData,
	/* [in] */ int cchData)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CSAXContentHandler::skippedEntity( 
	/* [in] */ unsigned short __RPC_FAR *pwchVal,
	/* [in] */ int cchVal)
{
	return S_OK;
}

void CSAXContentHandler::AttachElementHandler(ISAXXmlElementHandler* pElementHandler)
{
	m_attachElementHandler  = pElementHandler;
}


void CSAXContentHandler::DetachElementHandler()
{
	m_attachElementHandler  = NULL;
}

/******************************* eof *************************************/