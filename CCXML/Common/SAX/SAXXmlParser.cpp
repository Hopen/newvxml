/************************************************************************/
/* Name     : CacheInterface\SAXXmlParser.cpp                           */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "SAXXmlParser.h"

#define MAX_XML_PATH       1000
#define MAX_XSD_PATH       1000
#define MAX_FEATURE_NAME   200
#define MAX_NAMESPACE_URI  1000

// CSAXXmlParserImpl //////////////////////////////////////////////////////
//class CSAXXmlParserImpl
//{
//public:
//	CSAXXmlParserImpl();
//	~CSAXXmlParserImpl();
//
//	// Is the parser available (e.g. was the COM class created properly?).
//	bool IsReady() const;
//
//	// Attach XML events handler.
//	void AttachElementHandler(ISAXXmlElementHandler* pElementHandler);
//	void DetachElementHandler();
//
//	// Set parser feature options.
//	bool SetParserFeature(const std::wstring& featureName, bool value);
//	bool GetParserFeature(const std::wstring& featureName, bool& value) const;
//
//	// Add/remove XSD schemas for validation.
//	bool AddValidationSchema(const std::wstring& namespaceURI, const std::wstring& xsdPath);
//	bool RemoveValidationSchema(const std::wstring& namespaceURI);
//
//	// Parse a local file path, or a HTTP URL path.
//	bool Parse(const std::wstring& xmlPath);
//
//private:
//	void CreateSaxReader();
//	void CreateSchemaCache();
//
//	// SAX reader.
//	MSXML2::ISAXXMLReader* m_reader;
//
//	// Content and error handlers to be used with the SAX reader.
//	CSAXContentHandler* m_contentHandler;
//	CSAXErrorHandler*   m_errorHandler;
//	// Schema cache for XSD validation.
//	MSXML2::IXMLDOMSchemaCollection2* m_schemaCache;
//
//	// Our own XML events handler.
//	ISAXXmlElementHandler*  m_attachElementHandler;
//};

CSAXXmlParser::CSAXXmlParser()
{
	m_reader = NULL;

	//m_contentHandler = NULL;
	//m_errorHandler   = NULL;

	m_schemaCache = NULL;

	//m_attachElementHandler  = NULL;

	//// Initialize COM.
	//CoInitialize(NULL); 

	// Create the SAX XML reader (COM class).
	CreateSaxReader();

	// Create the schema cache (COM class) for XSD validation.
	CreateSchemaCache();
}

CSAXXmlParser::~CSAXXmlParser()
{
	//DetachElementHandler();

	// Release the SAX XML reader object.
/*	if ( m_reader != NULL )
	{
		m_reader->Release();
		m_reader = NULL;
	}
*/
	//if( m_writer != NULL)
	//{
	//	m_writer->Release();
	//	m_writer = NULL;
	//}

	//if (m_spIStream != NULL)
	//{
	//	m_spIStream = NULL;
	//}

	// Release the schema cache object.
//	if ( m_schemaCache != NULL )
//	{
//		m_schemaCache->Release();
//		m_schemaCache = NULL;
//	}

	//delete m_errorHandler;
	//m_errorHandler = NULL;

	//delete m_contentHandler;
	//m_contentHandler = NULL;

	//// Shutdown COM.
	//CoUninitialize();
}

bool CSAXXmlParser::IsReady() const
{
	return (m_reader != NULL);
}

//void CSAXXmlParser::AttachElementHandler(ISAXXmlElementHandler* pElementHandler)
//{
//	m_attachElementHandler = pElementHandler;
//
//	if ( m_contentHandler != NULL )
//		m_contentHandler->AttachElementHandler(pElementHandler);
//
//	if ( m_errorHandler != NULL )
//		m_errorHandler->AttachElementHandler(pElementHandler);
//}
//
//void CSAXXmlParser::DetachElementHandler()
//{
//	if ( m_errorHandler != NULL )
//		m_errorHandler->DetachElementHandler();
//
//	if ( m_contentHandler != NULL )
//		m_contentHandler->DetachElementHandler();
//
//	m_attachElementHandler  = NULL;
//}

bool CSAXXmlParser::SetParserFeature(const std::wstring& featureName, bool value)
{
	if ( featureName.empty() )
		return false;
	if ( !IsReady() )
		return false;

	VARIANT_BOOL vfValue = (value ? VARIANT_TRUE : VARIANT_FALSE);
	_bstr_t bstrFeature = featureName.c_str();

	HRESULT hr = m_reader->putFeature((unsigned short *)bstrFeature.GetBSTR(), vfValue);
	if ( FAILED(hr) )
		return false;

	return true;
}

bool CSAXXmlParser::GetParserFeature(const std::wstring& featureName, bool& value) const
{
	if ( featureName.empty() )
		return false;
	if ( !IsReady() )
		return false;

	VARIANT_BOOL vfValue = VARIANT_FALSE;
	_bstr_t bstrFeature = featureName.c_str();

	HRESULT hr = m_reader->getFeature((unsigned short *)bstrFeature.GetBSTR(),&vfValue);
	if ( FAILED(hr) )
		return false;

	value = (vfValue == VARIANT_TRUE ? true : false);

	return true;
}

bool CSAXXmlParser::AddValidationSchema(const std::wstring& namespaceURI, const std::wstring& xsdPath)
{
	if ( !IsReady() )
		return false;
	if ( m_schemaCache == NULL )
		return false;

	// Check for existing schema associated with this namespace URI.
	MSXML2::ISchema* pExistingSchema = NULL;
	_bstr_t bstrNamespace = namespaceURI.c_str();
	HRESULT hr = m_schemaCache->getSchema(bstrNamespace, &pExistingSchema);
	if ( SUCCEEDED(hr) )
	{
		// Remove the existing schema.
		hr = m_schemaCache->remove(bstrNamespace);
		if ( FAILED(hr) )
			return false;
	}

	// Add the new schema.
	hr = m_schemaCache->add(bstrNamespace, _variant_t(xsdPath.c_str()));
	if ( FAILED(hr) )
		return false;

	return true;
}

bool CSAXXmlParser::RemoveValidationSchema(const std::wstring& namespaceURI)
{
	if ( !IsReady() )
		return false;
	if ( m_schemaCache == NULL )
		return false;

	// Check for existing schema associated with this namespace URI.
	MSXML2::ISchema* pExistingSchema = NULL;
	_bstr_t bstrNamespace = namespaceURI.c_str();
	HRESULT hr = m_schemaCache->getSchema(bstrNamespace, &pExistingSchema);
	if ( SUCCEEDED(hr) )
	{
		// Remove the existing schema.
		hr = m_schemaCache->remove(bstrNamespace);
		if ( FAILED(hr) )
			return false;
	}
	else
	{
		// Return false if there is no schema to remove.
		return false;
	}

	return true;
}

//bool CSAXXmlParser::ReadBuffer(std::wstring& _buffer)
//{
//	if (!m_spIStream)
//		return false;
//	LARGE_INTEGER iLNull = {0, 0};
//	ULARGE_INTEGER uLLen = {0, 0};
//	//Take data size...
//	HRESULT hr = m_spIStream->Seek(iLNull, STREAM_SEEK_CUR, &uLLen);
//	if(FAILED(hr))
//		return false;
//	//IStream -> to begin...
//	hr = m_spIStream->Seek(iLNull, STREAM_SEEK_SET, NULL);
//	if(FAILED(hr))
//		return false;
//	//Size of buffer...
//	DWORD Size = (DWORD)uLLen.QuadPart;
//	   
//	WCHAR * szBuf = new WCHAR[Size+1];
//	m_spIStream->Read(szBuf, Size, NULL);
//	szBuf[Size] = 0;
//	_buffer.append(szBuf);
//	delete []szBuf;
//	return true;
//}

bool CSAXXmlParser::ParseUrl(const std::wstring& xmlPath)
{
	if ( !IsReady() )
		return false;

	// Do the parse.
	_bstr_t bstrXmlPath = xmlPath.c_str();
	wchar_t * address = (wchar_t*)xmlPath.c_str();
	HRESULT hr = m_reader->parseURL((unsigned short *)address );
	if ( FAILED(hr) )
		return false;

	return true;
}

bool CSAXXmlParser::Parse(const std::wstring& xmlSource)
{
	if ( !IsReady() )
		return false;

	VARIANT source;
	// Make it read-only variable in the global name table as IDispatch
	source.vt = VT_BSTR;
	bstr_t bstr_source(xmlSource.c_str());
	source.bstrVal = bstr_source;//_bstr_t(xmlSource.c_str());

	// Do the parse.
	HRESULT hr = m_reader->parse(source);
	if ( FAILED(hr) )
		return false;

	return true;

}

bool CSAXXmlParser::PutContentHandler(MSXML2::ISAXContentHandler *pHandler)
{
	if (!IsReady())
		return false;
	//m_contentHandler2 = pHandler;
	HRESULT hr =  m_reader->putContentHandler(pHandler);
	if ( FAILED(hr) )
	{
		//delete m_contentHandler2;
		//m_contentHandler2 = NULL;
		return false;
	}
	return true;
}

bool CSAXXmlParser::PutErrorHandler(MSXML2::ISAXErrorHandler *pErrHandler)
{
	//m_errorHandler2 = pErrHandler;
	if (!IsReady())
		return false;
	HRESULT hr =  m_reader->putErrorHandler(pErrHandler);
	if ( FAILED(hr) )
	{
		//delete m_errorHandler2;
		//m_errorHandler2 = NULL;
		return false;
	}
	return true;
}

void CSAXXmlParser::CreateSaxReader()
{
// 	HRESULT hr = CoCreateInstance(
// 		__uuidof(MSXML2::SAXXMLReader60), 
// 		NULL, 
// 		CLSCTX_ALL, 
// 		__uuidof(MSXML2::ISAXXMLReader), 
// 		(void **)&m_reader);
	HRESULT hr = m_reader.CoCreateInstance(__uuidof(MSXML2::SAXXMLReader60), 
		NULL, 
		CLSCTX_ALL);

	if ( SUCCEEDED(hr) )
	{        
		// Set the content handler.
		//m_contentHandler = new CSAXContentHandler();
		//hr = m_reader->putContentHandler(m_contentHandler);
		//if ( FAILED(hr) )
		//{
		//	delete m_contentHandler;
		//	m_contentHandler = NULL;
		//}

		//// Set the error handler.
		//m_errorHandler = new CSAXErrorHandler();
		//hr = m_reader->putErrorHandler(m_errorHandler);
		//if ( FAILED(hr) )
		//{
		//	delete m_errorHandler;
		//	m_errorHandler = NULL;
		//}
	}
	else
	{
		//delete m_errorHandler;
		//m_errorHandler = NULL;

		//delete m_contentHandler;
		//m_contentHandler = NULL;

		m_reader = NULL;
	}

	//if (FAILED(hr))
	//	return;

	//hr = CoCreateInstance(
	//	__uuidof(MSXML2::MXXMLWriter60), 
	//	NULL, 
	//	CLSCTX_ALL, 
	//	__uuidof(MSXML2::IMXWriter), 
	//	(void **)&m_writer);

	//if (SUCCEEDED(hr))
	//{
	//	   //Set properties on the XML writer.
	//	m_writer->put_byteOrderMark(VARIANT_FALSE);
	//	m_writer->put_omitXMLDeclaration(VARIANT_FALSE);
	//	m_writer->put_indent(VARIANT_TRUE);
	//	//m_writer->put_encoding(L"WINDOWS-1251");

	//	//Set the XML writer to the SAX content handler.
	//	m_reader->putContentHandler((ISAXContentHandlerPtr)m_writer);
	//	m_reader->putDTDHandler((ISAXDTDHandlerPtr)m_writer);
	//	m_reader->putErrorHandler((ISAXErrorHandlerPtr)m_writer);


	//	hr = CreateStreamOnHGlobal(NULL, TRUE, &m_spIStream);

	//	if ( FAILED(hr) )
	//	{
	//		//m_spIStream = NULL;
	//		return;
	//	}
	//	m_writer->put_output(_variant_t(m_spIStream));
	//}
	//else
	//{
	//	m_writer = NULL;
	//}

}

void CSAXXmlParser::CreateSchemaCache()
{
	if ( m_reader == NULL )
		return;

// 	HRESULT hr = CoCreateInstance(
// 		__uuidof(MSXML2::XMLSchemaCache60), 
// 		NULL, 
// 		CLSCTX_ALL, 
// 		__uuidof(MSXML2::IXMLDOMSchemaCollection2), 
// 		(void **)&m_schemaCache);
	HRESULT hr = m_schemaCache.CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60), 
		NULL, 
		CLSCTX_ALL);

	if ( SUCCEEDED(hr) )
	{
		// Set the "schemas" property in the reader in order
		// to associate the schema cache with the reader.
		wchar_t wShemas [] = L"schemas";
		hr = m_reader->putProperty((unsigned short*)wShemas, _variant_t(m_schemaCache));
		if ( FAILED(hr) )
		{
			OutputDebugString(L"CXmlParserImpl::CreateSchemaCache(): putProperty(L\"schemas\",...) failed\n");
		}
	}
}

/******************************* eof *************************************/