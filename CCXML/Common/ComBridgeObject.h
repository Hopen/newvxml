#pragma once

#include <atlbase.h>
//#include <boost/shared_ptr.hpp>
#include <memory>

//template<typename T>
//class CComBridge : public IDispatch
//{
//public:
//	using ObjPtr = std::shared_ptr<T>;
//	using WeakObjPtr = std::weak_ptr<T>;
//
//private:
//	ObjPtr	m_value;
//	LONG	m_lRefCount;
//
//	CComBridge<T>(const CComBridge<T>&) { }
//	CComBridge& operator = (const CComBridge<T>&) { return *this; }
//
//public:
//	explicit CComBridge<T>(ObjPtr _ptr) : m_value(_ptr), m_lRefCount(0)
//	{
//
//	}
//
//	~CComBridge<T>() 
//	{
//
//	}
//
//	WeakObjPtr Get() const
//	{
//		return m_value;
//	}
//
//
//	ULONG STDMETHODCALLTYPE AddRef( void)
//	{
//		return ::InterlockedIncrement(&m_lRefCount);
//	}
//
//	ULONG STDMETHODCALLTYPE Release(void)
//	{
//		LONG lResult = ::InterlockedDecrement(&m_lRefCount);
//		if(m_lRefCount == 0)
//			delete this;
//		return lResult;
//	}
//
//	virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
//		/* [in] */ REFIID riid,
//		/* [iid_is][out] */ /*_COM_Outptr_*/ void __RPC_FAR *__RPC_FAR *ppvObject)
//		{
//			HRESULT retCode=S_OK;
//			if (IsEqualIID(riid,IID_IUnknown/*IID_IDispatch*/))
//			{
//				*ppvObject=this;
//				AddRef();
//			}
//			else if (IsEqualIID(riid,IID_IDispatch))
//			{
//					*ppvObject=this;
//					AddRef();
//			}
//			else retCode = E_NOINTERFACE;
//
//			return retCode;
//		}
//
//	virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount( /* [out] */ __RPC__out UINT *pctinfo)
//	{
//		return m_value->GetTypeInfoCount(pctinfo);
//	}
//
//	virtual HRESULT STDMETHODCALLTYPE GetTypeInfo( 
//		/* [in] */ UINT iTInfo,
//		/* [in] */ LCID lcid,
//		/* [out] */ __RPC__deref_out_opt ITypeInfo **ppTInfo)
//		{
//			return m_value->GetTypeInfo(iTInfo, lcid, ppTInfo);
//		}
//
//	virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames( 
//		/* [in] */ __RPC__in REFIID riid,
//		/* [size_is][in] */ __RPC__in_ecount_full(cNames) LPOLESTR *rgszNames,
//		/* [range][in] */ __RPC__in_range(0,16384) UINT cNames,
//		/* [in] */ LCID lcid,
//		/* [size_is][out] */ __RPC__out_ecount_full(cNames) DISPID *rgDispId)
//		{
//			return m_value->GetIDsOfNames(riid, rgszNames, cNames, lcid, rgDispId);
//		}
//
//	virtual /* [local] */ HRESULT STDMETHODCALLTYPE Invoke( 
//		/* [annotation][in] */ 
//		_In_  DISPID dispIdMember,
//		/* [annotation][in] */ 
//		_In_  REFIID riid,
//		/* [annotation][in] */ 
//		_In_  LCID lcid,
//		/* [annotation][in] */ 
//		_In_  WORD wFlags,
//		/* [annotation][out][in] */ 
//		_In_  DISPPARAMS *pDispParams,
//		/* [annotation][out] */ 
//		_Out_opt_  VARIANT *pVarResult,
//		/* [annotation][out] */ 
//		_Out_opt_  EXCEPINFO *pExcepInfo,
//		/* [annotation][out] */ 
//		_Out_opt_  UINT *puArgErr)
//		{
//			return m_value->Invoke(dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
//		}
//
//
//};

template<typename T>
class CComBridge : public IDispatch
{
public:
	using ObjPtr = std::shared_ptr<T>;
	using WeakObjPtr = std::weak_ptr<T>;
	//using ObjPtr = std::unique_ptr<T>;

private:
	ObjPtr	m_value;
	LONG	m_lRefCount;

	CComBridge(const CComBridge<T>&) = delete;
	CComBridge& operator = (const CComBridge<T>&) = delete;// { return *this; }

public:
	explicit CComBridge(const ObjPtr& _ptr) : m_value(_ptr), m_lRefCount(0)
	{

	}

	~CComBridge()
	{

	}

	WeakObjPtr Get() const
	{
		return m_value;
	}

	void Copy(const CComBridge<T>& aRhs)
	{
		m_value->Clone(*aRhs.m_value.get());
	}

	template <class T>
	const T ToMsg()const
	{
		return m_value->ToMsg();
	}

	template <class T, class TLog>
	const T ToMsg(TLog aLog)const
	{
		return m_value->ToMsg(aLog);
	}

	template <class T>
	const T GetValue(const std::wstring& aName)const
	{
		return m_value->GetValue(aName);
	}

	template <class T>
	const T GetSafeValue(const std::wstring& aName)const
	{
		return m_value->GetSafeValue(aName);
	}

	template <class T>
	const T& GetUnsafeValue(const std::wstring& aName)const
	{
		return m_value->GetUnsafeValue(aName);
	}

	template <class Predicate>
	CComVariant FindByPredicate(Predicate pred) const
	{
		return m_value->FindByPredicate(pred);
	}

	template <class Predicate>
	void ForEach(Predicate pred)
	{
		m_value->ForEach(pred);
	}

	bool HasValue() const
	{
		return !!m_value.get();
	}

	//template <class ... TArgs>
	//void SetValue(TArgs&&... aArgs)
	//{
	//	m_value->SetValue(std::forward<TArgs>(aArgs)...);
	//}

	template <class T, class ... TArgs>
	T AddValue(TArgs&&... aArgs)
	{
		return m_value->Add(std::forward<TArgs>(aArgs)...);
	}

	CComVariant GetVariant()
	{
		return m_value->AsVariant();
	}

	ULONG STDMETHODCALLTYPE AddRef(void)
	{
		return ::InterlockedIncrement(&m_lRefCount);
	}

	ULONG STDMETHODCALLTYPE Release(void)
	{
		LONG lResult = ::InterlockedDecrement(&m_lRefCount);
		if (m_lRefCount == 0)
			delete this;
		return lResult;
	}

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(
		/* [in] */ REFIID riid,
		/* [iid_is][out] */ /*_COM_Outptr_*/ void __RPC_FAR *__RPC_FAR *ppvObject)
	{
		HRESULT retCode = S_OK;
		if (IsEqualIID(riid, IID_IUnknown/*IID_IDispatch*/))
		{
			*ppvObject = this;
			AddRef();
		}
		else if (IsEqualIID(riid, IID_IDispatch))
		{
			*ppvObject = this;
			AddRef();
		}
		else retCode = E_NOINTERFACE;

		return retCode;
	}

	virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount( /* [out] */ __RPC__out UINT *pctinfo)
	{
		return m_value->GetTypeInfoCount(pctinfo);
	}

	virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ __RPC__deref_out_opt ITypeInfo **ppTInfo)
	{
		return m_value->GetTypeInfo(iTInfo, lcid, ppTInfo);
	}

	virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames(
		/* [in] */ __RPC__in REFIID riid,
		/* [size_is][in] */ __RPC__in_ecount_full(cNames) LPOLESTR *rgszNames,
		/* [range][in] */ __RPC__in_range(0, 16384) UINT cNames,
		/* [in] */ LCID lcid,
		/* [size_is][out] */ __RPC__out_ecount_full(cNames) DISPID *rgDispId)
	{
		return m_value->GetIDsOfNames(riid, rgszNames, cNames, lcid, rgDispId);
	}

	virtual /* [local] */ HRESULT STDMETHODCALLTYPE Invoke(
		/* [annotation][in] */
		_In_  DISPID dispIdMember,
		/* [annotation][in] */
		_In_  REFIID riid,
		/* [annotation][in] */
		_In_  LCID lcid,
		/* [annotation][in] */
		_In_  WORD wFlags,
		/* [annotation][out][in] */
		_In_  DISPPARAMS *pDispParams,
		/* [annotation][out] */
		_Out_opt_  VARIANT *pVarResult,
		/* [annotation][out] */
		_Out_opt_  EXCEPINFO *pExcepInfo,
		/* [annotation][out] */
		_Out_opt_  UINT *puArgErr)
	{
		return m_value->Invoke(dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
	}


};