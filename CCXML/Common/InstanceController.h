/************************************************************************/
/* Name     : Common\InstanceController.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 12 Jul 2018                                               */
/************************************************************************/
#pragma once
#include "sid.h"
#include "..\Engine\EngineLog.h"

template <class T>
class InstanceController
{
public:
	InstanceController(
		const std::wstring& aSubsystemName,
		const std::wstring& aFilePath)
		: mSubsystemName(aSubsystemName)
	{
#ifdef _DEBUG
		__int64 iScriptID = GetGlobalScriptId();

		mLog.Init(L"finest", aFilePath.c_str());
		mLog.SetSID(iScriptID);
#endif
	}

	~InstanceController() = default;

protected:
	template <class... TArgs>
	void LogPlace(TArgs&&... aArgs)
	{
#ifdef _DEBUG
		mLog.Log(enginelog::LEVEL_FINEST, mSubsystemName.c_str(), std::forward<TArgs>(aArgs)...);
#endif
	}

	//void LogDefaultCtor();
	//void LogCopyCtor();
	//void LogEqualOperator();
	//void LogDtor();

	void LogDefaultCtor()
	{
#ifdef _DEBUG
		mId = ++sCurCount;
		LogPlace(L"Ctor, Id: %i, Active: %i", mId, ++sActiveCount);
#endif
	}

	void LogCopyCtor()
	{
#ifdef _DEBUG
		mId = ++sCurCount;
		LogPlace(L"CopyCtor, Id: %i, Active: %i", mId, ++sActiveCount);
#endif
	}

	void LogEqualOperator()
	{

	}

	void LogDtor()
	{
#ifdef _DEBUG
		--sActiveCount;
		LogPlace(L"Dtor, Id: %i, Active: %i", mId, sActiveCount);
#endif
	}

private:
	static int sCurCount;
	static int sActiveCount;
	static int sTechnical;

	int mId = 0;
	CEngineLog mLog;

	std::wstring mSubsystemName;
	T *t = nullptr;
};

template <class T>
int InstanceController<T>::sCurCount = 0;

template <class T>
int InstanceController<T>::sActiveCount = 0;

template <class T>
int InstanceController<T>::sTechnical = 0;

/******************************* eof *************************************/