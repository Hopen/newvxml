/************************************************************************/
/* Name     : CCXML\Common\factory_c.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 09 Apr 2018                                               */
/************************************************************************/
#include "stdafx.h"
#include "factory_c.h"

//CEREAL_REGISTER_TYPE(ISATXMLElement);
//CEREAL_REGISTER_POLYMORPHIC_RELATION(Factory_c::CFTag, ISATXMLElement);
//CEREAL_REGISTER_POLYMORPHIC_RELATION(Factory_c::CCollector, Factory_c::CFTag);
//
//namespace Factory_c
//{
//	
//	//CEREAL_REGISTER_POLYMORPHIC_RELATION(CFTag, ISATXMLElement);
//}


/******************************* eof *************************************/