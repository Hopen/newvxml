/************************************************************************/
/* Name     : VXML\HasMap.h                                             */
/* Author   : Andrey Alekseev                                           */
/* Project  : CCXML-VXML                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 30 Nov 2018                                               */
/************************************************************************/
#pragma once

#include <string>
#include <boost/functional/hash.hpp>

struct DoubleStringKey
{
	DoubleStringKey() = default;

	DoubleStringKey(const std::wstring& aMenuId,
		const std::wstring& aFormId)
		: MenuId(aMenuId)
		, FormId(aFormId)
	{
	}

	std::wstring MenuId;
	std::wstring FormId;

	bool operator==(const DoubleStringKey &other) const
	{
		return (MenuId == other.MenuId 
			&& FormId == other.FormId);
	}
};

namespace std {

	template <>
	struct hash<DoubleStringKey>
	{
		std::size_t operator()(const DoubleStringKey& k) const
		{
			std::size_t seed = 0;

			boost::hash_combine(seed, boost::hash_value(k.MenuId));
			boost::hash_combine(seed, boost::hash_value(k.FormId));

			return seed;
		}
	};

}

/******************************* eof *************************************/