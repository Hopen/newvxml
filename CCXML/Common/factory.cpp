/************************************************************************/
/* Name     : CacheInterface\factory.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 May 2010                                               */
/************************************************************************/

#pragma once

#include "StdAfx.h"
#include "factory.h"
//#include "sv_strutils.h"

namespace Factory
{
	// CFTag implementation
	//CFTag::CFTag(MSXML2::IXMLDOMNodePtr node) : m_sName((LPCWSTR)node->nodeName)
	//{
	//	MSXML2::IXMLDOMNamedNodeMapPtr attribs = node->attributes;
	//	int j = 0, m = attribs?attribs->length:0;
	//	for (; j < m; j++)
	//	{
	//		MSXML2::IXMLDOMNodePtr attrib = attribs->nextNode();
	//		std::wstring sAttrName = (LPCWSTR)attrib->nodeName;
	//		_variant_t val = attrib->nodeValue;
	//		val.ChangeType(VT_BSTR);
	//		m_Attribs[sAttrName] = val.bstrVal;
	//	}

	//	MSXML2::IXMLDOMNodeListPtr nodeList = node->childNodes;
	//	if (nodeList && nodeList->length == 1)
	//	{
	//		MSXML2::IXMLDOMNodePtr node = nodeList->Getitem(0);
	//		std::wstring sName = (LPCWSTR)node->nodeName;
	//		if (sName == L"#text" || sName == L"#cdata-section")
	//		{
	//			m_sText = (LPCWSTR)node->text;
	//		}
	//	}
	//}

	// CCollector implementation
	// Ctor
	//CCollector::CCollector(MSXML2::IXMLDOMNodePtr _node): CFTag(_node)/*, m_ulRefCount(0)*/
	//{
	//	MSXML2::IXMLDOMNodeListPtr nodeList = _node->childNodes;
	//	int i = 0, n = nodeList->length;
	//	for (; i < n; i++)
	//	{
	//		MSXML2::IXMLDOMNodePtr node = nodeList->Getitem(i);
	//		std::wstring sName = (LPCWSTR)node->nodeName;
	//		if (sName != L"meta" && sName != L"metadata" && sName != L"#text" && sName != L"#comment" && sName != L"#cdata-section")
	//		{
	//			m_Children.push_back(/*new*/ CCollector(node));
	//		}

	//	}
	//}

	std::wostream& operator<<(std::wostream& os, const CFTag& b ) 
	{ 
		UNREFERENCED_PARAMETER(b); 
		return os; 
	}
}

/******************************* eof *************************************/
