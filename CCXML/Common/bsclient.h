/************************************************************************/
/* Name     : bsclient.h                                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 14 Jul 2010                                               */
/************************************************************************/
#pragma once

#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <boost/asio.hpp>
//#include <boost/lexical_cast.hpp>

#include "Patterns\string_functions.h"
//#include "StringFunctions.h"
//#include "sv_strutils.h"
//#include <comutil.h>

using boost::asio::ip::tcp;


class CSyncClient
{
	std::wstring from_utf8(const std::string& _data)
	{
		std::shared_ptr<wchar_t> szResult(new wchar_t[_data.length() * 2]);
		ZeroMemory(szResult.get(), _data.length() * 2);

		int nIndex = MultiByteToWideChar(CP_UTF8, 0, _data.c_str(), _data.length(), szResult.get(), _data.length() * 2);
		return std::wstring(szResult.get(), nIndex);
	}

	std::string to_utf8(const std::wstring& _data)
	{
		int iLength = _data.size() * 2;
		std::shared_ptr<char> szResult(new char[iLength]);
		ZeroMemory(szResult.get(), iLength);

		int nIndex = WideCharToMultiByte(CP_UTF8, 0, _data.c_str(), _data.length(), szResult.get(), iLength, NULL, NULL);
		return std::string(szResult.get(), nIndex);
	}


public:
	CSyncClient()
	{
	}
	virtual ~CSyncClient(){}

	int Send(const std::wstring& _servername, const std::wstring& _port, const std::wstring& _path, std::wstring& sAnswer)
	{
		try
		{
			boost::asio::io_service io_service;

			std::string szServerName = wtos(_servername);
			std::string szPort = wtos(_port);
			// Get a list of endpoints corresponding to the server name.
			tcp::resolver resolver(io_service);
			tcp::resolver::query query(szServerName, szPort.empty() ? "http" : szPort);
			tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
			tcp::resolver::iterator end;

			// Try each endpoint until we successfully establish a connection.
			tcp::socket socket(io_service);
			boost::system::error_code error = boost::asio::error::host_not_found;
			while (error && endpoint_iterator != end)
			{
				socket.close();
				socket.connect(*endpoint_iterator++, error);
			}
			if (error)
				throw boost::system::system_error(error);

			// Form the request. We specify the "Connection: close" header so that the
			// server will close the socket after transmitting the response. This will
			// allow us to treat all data up until the EOF as the content.

			boost::asio::streambuf request;
			std::ostream request_stream(&request);
			request_stream << "GET " << wtos(_path) << " HTTP/1.0\r\n";
			request_stream << "Host: " << szServerName << "\r\n";
			request_stream << "Accept: */*\r\n";
			request_stream << "Connection: close\r\n\r\n";

			// Send the request.
			boost::asio::write(socket, request);

			// Read the response status line.
			boost::asio::streambuf response;
			boost::asio::read_until(socket, response, "\r\n");

			// Check that response is OK.
			std::istream response_stream(&response);
			std::string http_version;
			response_stream >> http_version;
			unsigned int status_code;
			response_stream >> status_code;
			std::string status_message;
			std::getline(response_stream, status_message);
			if (!response_stream || http_version.substr(0, 5) != "HTTP/")
			{
				//printf("Invalid response");
				sAnswer = stow(status_message);// L"Invalid response";
				return 1;
			}
			if (status_code != 200)
			{
				//printf("Response returned with status code: %i\n", status_code);
				sAnswer = stow(status_message);
				return status_code;
			}

			// Read the response headers, which are terminated by a blank line.
			boost::asio::read_until(socket, response, "\r\n\r\n");

			// Process the response headers.

			std::string header, parthead;
			std::string sCharset;
			while (std::getline(response_stream, parthead) && parthead != "\r")
			{
				header += parthead + "\n";
				int pos = parthead.find("charset=");
				if (pos > 0)
				{
					sCharset.erase(pos, std::string("charset=").size());
					//sCharset = CAtlString(parthead.c_str()).Right(parthead.size() - (pos + std::string("charset=").size()));
				}
				
			}

			// Write whatever content we already have to output.
			std::string szReceive;
			if (response.size() > 0)
			{
				std::ostringstream out;
				out << &response;;
				szReceive = out.str();
			}

			// Read until EOF, writing data to output as we go.
			while (boost::asio::read(socket, response,
				boost::asio::transfer_at_least(1), error))
			{
				std::ostringstream out;
				out << &response;;
				szReceive += out.str();
			}

			//sAnswer = boost::lexical_cast<std::wstring>(szReceive);
			//sAnswer = bstr_t(sAnswer.c_str()).GetBSTR();
			//sAnswer = bstr_t(szReceive.c_str()).GetBSTR();
			//inline std::wstring StrA2wstring(LPCSTR lpa)
			//{
			//	size_t nLen = strlen(lpa);
			//	std::wstring str(nLen, L'\0');
			//	StrA2WHelper((LPWSTR)str.c_str(), lpa, (int)nLen);
			//	return str;
			//}
			//sAnswer = from_utf8(szReceive);

			//make lower
			std::transform(sCharset.begin(), sCharset.end(), sCharset.begin(), ::tolower);

			if (!sCharset.find("utf-8"))
			{
				sAnswer = from_utf8(szReceive);
			}
			if (!sCharset.find("windows-1251"))
			{
				//sAnswer = szReceive.c_str();
				sAnswer = stow(szReceive); // save to file
			}

			if (error != boost::asio::error::eof)
				throw boost::system::system_error(error);
		}
		catch (std::exception& e)
		{
			//printf("Exception: %s\n", e.what());
			//return e1002;
			std::string what = e.what();
			throw e;
		}

		return 0;
	}


	int Post(const std::wstring& _servername, const std::wstring& _uri, const std::string& _source, std::wstring& sAnswer)
	{
		try
		{
			boost::asio::io_service io_service;

			std::string szServerName = boost::lexical_cast<std::string>(_servername);
			std::string szSource = _source;
			//std::string szSource     = _bstr_t(_source.c_str());
			std::string szURI        = boost::lexical_cast<std::string>(_uri);
			// Get a list of endpoints corresponding to the server name.
			tcp::resolver resolver(io_service);
			tcp::resolver::query query(szServerName, "http");
			tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
			tcp::resolver::iterator end;

			// Try each endpoint until we successfully establish a connection.
			tcp::socket socket(io_service);
			boost::system::error_code error = boost::asio::error::host_not_found;
			while (error && endpoint_iterator != end)
			{
				socket.close();
				socket.connect(*endpoint_iterator++, error);
			}
			if (error)
				throw boost::system::system_error(error);

			// Form the request. We specify the "Connection: close" header so that the
			// server will close the socket after transmitting the response. This will
			// allow us to treat all data up until the EOF as the content.

			boost::asio::streambuf request;
			std::ostream request_stream(&request);
			request_stream << "POST " << szURI << " HTTP/1.0\r\n";
			request_stream << "Host: " << szServerName << "\r\n";
			request_stream << "User-Agent: IS3 SyncClient\r\n";
			//request_stream << "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n";
			//request_stream << "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3\r\n";
			//request_stream << "Accept-Encoding: gzip, deflate\r\n";
			request_stream << "Content-Type: application/x-www-form-urlencoded; charset=UTF-8\r\n";
			request_stream << "Content-Length: " << szSource.size() << "\r\n";
			//request_stream << "Connection: keep-alive\r\n";
			//request_stream << "Pragma: no-cache\r\n";
			//request_stream << "Cache-Control: no-cache\r\n";
			request_stream << "\r\n";
			request_stream << szSource;// << "\r\n";
			request_stream << "\r\n\r\n";
			// Send the request.
			boost::asio::write(socket, request);
			//boost::asio::write(socket, boost::asio::buffer(request));

			// Read the response status line.
			boost::asio::streambuf response;
			boost::asio::read_until(socket, response, "\r\n");

			// Check that response is OK.
			std::istream response_stream(&response);
			std::string http_version;
			response_stream >> http_version;
			unsigned int status_code;
			response_stream >> status_code;
			std::string status_message;
			std::getline(response_stream, status_message);
			if (!response_stream || http_version.substr(0, 5) != "HTTP/")
			{
				//printf("Invalid response");
				sAnswer = stow(status_message);// L"Invalid response";
				return 1;
			}
			if (status_code != 200)
			{
				//printf("Response returned with status code: %i\n", status_code);
				sAnswer = stow(status_message);
				return status_code;
			}

			// Read the response headers, which are terminated by a blank line.
			boost::asio::read_until(socket, response, "\r\n\r\n");

			// Process the response headers.

			std::string header, parthead;
			while (std::getline(response_stream, parthead) && parthead != "\r")
			{
				header += parthead + "\n";
			}

			// Write whatever content we already have to output.
			std::string szReceive;
			if (response.size() > 0)
			{
				std::ostringstream out;
				out << &response;;
				szReceive = out.str();
			}

			// Read until EOF, writing data to output as we go.
			while (boost::asio::read(socket, response,
				boost::asio::transfer_at_least(1), error))
			{
				std::ostringstream out;
				out << &response;;
				szReceive += out.str();
			}

			//sAnswer = bstr_t(szReceive.c_str()).GetBSTR();
			sAnswer = stow(szReceive);

			if (error != boost::asio::error::eof)
				throw boost::system::system_error(error);
		}
		catch (std::exception& e)
		{
			//printf("Exception: %s\n", e.what());
			//return e1002;
			throw e;
		}

		return 0;
	}

};


