/************************************************************************/
/* Name     : Common\SystemLogEx.h                                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solution                                           */
/* Date     : 14 Apr 2019                                               */
/************************************************************************/
#pragma once
#include <memory>
#include "LogEx\MySystemLog.h"
#include "VXMLEngineConfig.h"

class SystemLogExImpl: public CMySystemLog
{
public:
	SystemLogExImpl(
		const std::wstring& aFileName, 
		const std::wstring& aLogLevel)
		: CMySystemLog(aFileName, translateLogLevel(aLogLevel))
	{}
	//void Init(ULONGLONG ullSID, LPCWSTR pwszLevel, LPCWSTR pwszFormat, bool extraDate = false);

	template <class... TArgs>
	void LogFinest(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		this->LogStringModule(LEVEL_FINEST, __FUNCTIONW__, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogInfo(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		this->LogStringModule(LEVEL_INFO, __FUNCTIONW__, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogWarning(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		this->LogStringModule(LEVEL_WARNING, __FUNCTIONW__, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogExtra(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		this->LogStringModule(LEVEL_INFO, __FUNCTIONW__, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	bool IfLogAvailable() const;

private:
	//std::shared_ptr<CSystemLog> mLog;
	//CSystemLog* mLog;
	//SystemLogEx<TConfig>* mLog;
};

inline std::wstring ObjectIdToStr(__int64 aObjectId)
{
	return (boost::wformat(L"%08X-%08X") % DWORD(aObjectId >> 32) % DWORD(aObjectId & 0xFFFFFFFF)).str();
}

class SystemLogEx
{
public:
	template <class... TArgs>
	SystemLogEx(const __int64& aScriptId, TArgs&&... aArgs)
		: mScriptId(ObjectIdToStr(aScriptId))
	{
		mLog = ThreadSaveSingleton<SystemLogExImpl>::GetInstance(std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogFinest(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		mLog->LogStringModule(LEVEL_FINEST, mScriptId, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogInfo(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		mLog->LogStringModule(LEVEL_INFO, mScriptId, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogWarning(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		mLog->LogStringModule(LEVEL_WARNING, mScriptId, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	template <class... TArgs>
	void LogExtra(LPCWSTR pwszFmt, TArgs&&... aArgs)
	{
		mLog->LogStringModule(LEVEL_INFO, mScriptId, pwszFmt, std::forward<TArgs>(aArgs)...);
	}

	bool IfLogAvailable() const
	{
		return mLog->IfLogAvailable();
	}

private:
	SystemLogExImpl* mLog;
	std::wstring mScriptId;
};

/******************************* eof *************************************/