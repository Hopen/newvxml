// CallingPartyInfo.cpp : Implementation of CCallingPartyInfo

#include ".\stdafx.h"
#include "CallingPartyInfo.h"
#include ".\callingpartyinfo.h"


// CCallingPartyInfo


STDMETHODIMP CCallingPartyInfo::get_NatureOfAddress(LONG* pVal)
{
	try {
		*pVal = m_NatureOfAddress;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_NatureOfAddress(LONG newVal)
{
	try {
		m_NatureOfAddress = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::get_OddEvenIndicator(LONG* pVal)
{
	try {
		*pVal = m_OddEvenIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_OddEvenIndicator(LONG newVal)
{
	try {
		m_OddEvenIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::get_NumberingPlanIndicator(LONG* pVal)
{
	try {
		*pVal = m_NumberingPlanIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_NumberingPlanIndicator(LONG newVal)
{
	try {
		m_NumberingPlanIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::get_ScreeningIndicator(LONG* pVal)
{
	try {
		*pVal = m_ScreeningIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_ScreeningIndicator(LONG newVal)
{
	try {
		m_ScreeningIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::get_PresentationRestrictionIndicator(LONG* pVal)
{
	try {
		*pVal = m_PresentationRestrictionIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_PresentationRestrictionIndicator(LONG newVal)
{
	try {
		m_PresentationRestrictionIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::get_CallingNumberIncompleteIndicator(LONG* pVal)
{
	try {
		*pVal = m_CallingNumberIncompleteIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_CallingNumberIncompleteIndicator(LONG newVal)
{
	try {
		m_CallingNumberIncompleteIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::get_CallingAddressSignals(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_CallingAddressSignals.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyInfo::put_CallingAddressSignals(BSTR newVal)
{
	try {
		m_CallingAddressSignals = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
