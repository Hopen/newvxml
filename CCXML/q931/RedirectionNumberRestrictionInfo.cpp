// RedirectionNumberRestrictionInfo.cpp : Implementation of CRedirectionNumberRestrictionInfo

#include ".\stdafx.h"
#include "RedirectionNumberRestrictionInfo.h"
#include ".\redirectionnumberrestrictioninfo.h"


// CRedirectionNumberRestrictionInfo


STDMETHODIMP CRedirectionNumberRestrictionInfo::get_PresentationRestricted(LONG* pVal)
{
	try {
		*pVal = m_PresentationRestricted;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberRestrictionInfo::put_PresentationRestricted(LONG newVal)
{
	try {
		m_PresentationRestricted = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
