// CallingPartyCategoryInfo.cpp : Implementation of CCallingPartyCategoryInfo

#include ".\stdafx.h"
#include "CallingPartyCategoryInfo.h"
#include ".\callingpartycategoryinfo.h"


// CCallingPartyCategoryInfo


STDMETHODIMP CCallingPartyCategoryInfo::get_CallingPartyCategory(LONG* pVal)
{
	try {
		*pVal = m_CallingPartyCategory;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCallingPartyCategoryInfo::put_CallingPartyCategory(LONG newVal)
{
	try {
		m_CallingPartyCategory = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
