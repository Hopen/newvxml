// RedirectionNumberInfo.cpp : Implementation of CRedirectionNumberInfo

#include ".\stdafx.h"
#include "RedirectionNumberInfo.h"
#include ".\redirectionnumberinfo.h"


// CRedirectionNumberInfo


STDMETHODIMP CRedirectionNumberInfo::get_NatureOfAddress(LONG* pVal)
{
	try {
		*pVal = m_NatureOfAddress;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::put_NatureOfAddress(LONG newVal)
{
	try {
		m_NatureOfAddress = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::get_OddEvenIndicator(LONG* pVal)
{
	try {
		*pVal = m_OddEvenIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::put_OddEvenIndicator(LONG newVal)
{
	try {
		m_OddEvenIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::get_NumberingPlanIndicator(LONG* pVal)
{
	try {
		*pVal = m_NumberingPlanIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::put_NumberingPlanIndicator(LONG newVal)
{
	try {
		m_NumberingPlanIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::get_InternalNetworkNumberIndicator(LONG* pVal)
{
	try {
		*pVal = m_InternalNetworkNumberIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::put_InternalNetworkNumberIndicator(LONG newVal)
{
	try {
		m_InternalNetworkNumberIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::get_RedirectionAddressSignals(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_RedirectionAddressSignals.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionNumberInfo::put_RedirectionAddressSignals(BSTR newVal)
{
	try {
		m_RedirectionAddressSignals = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
