

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sun Apr 21 22:22:04 2019
 */
/* Compiler settings for q931.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __q931_h__
#define __q931_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICorrelationIDInfo_FWD_DEFINED__
#define __ICorrelationIDInfo_FWD_DEFINED__
typedef interface ICorrelationIDInfo ICorrelationIDInfo;
#endif 	/* __ICorrelationIDInfo_FWD_DEFINED__ */


#ifndef __ISCFIDInfo_FWD_DEFINED__
#define __ISCFIDInfo_FWD_DEFINED__
typedef interface ISCFIDInfo ISCFIDInfo;
#endif 	/* __ISCFIDInfo_FWD_DEFINED__ */


#ifndef __IRedirectionNumberRestrictionInfo_FWD_DEFINED__
#define __IRedirectionNumberRestrictionInfo_FWD_DEFINED__
typedef interface IRedirectionNumberRestrictionInfo IRedirectionNumberRestrictionInfo;
#endif 	/* __IRedirectionNumberRestrictionInfo_FWD_DEFINED__ */


#ifndef __ICallingPartyCategoryInfo_FWD_DEFINED__
#define __ICallingPartyCategoryInfo_FWD_DEFINED__
typedef interface ICallingPartyCategoryInfo ICallingPartyCategoryInfo;
#endif 	/* __ICallingPartyCategoryInfo_FWD_DEFINED__ */


#ifndef __ICauseIndicatorsInfo_FWD_DEFINED__
#define __ICauseIndicatorsInfo_FWD_DEFINED__
typedef interface ICauseIndicatorsInfo ICauseIndicatorsInfo;
#endif 	/* __ICauseIndicatorsInfo_FWD_DEFINED__ */


#ifndef __IRedirectionNumberInfo_FWD_DEFINED__
#define __IRedirectionNumberInfo_FWD_DEFINED__
typedef interface IRedirectionNumberInfo IRedirectionNumberInfo;
#endif 	/* __IRedirectionNumberInfo_FWD_DEFINED__ */


#ifndef __IRedirectionInformationInfo_FWD_DEFINED__
#define __IRedirectionInformationInfo_FWD_DEFINED__
typedef interface IRedirectionInformationInfo IRedirectionInformationInfo;
#endif 	/* __IRedirectionInformationInfo_FWD_DEFINED__ */


#ifndef __ILocationNumberInfo_FWD_DEFINED__
#define __ILocationNumberInfo_FWD_DEFINED__
typedef interface ILocationNumberInfo ILocationNumberInfo;
#endif 	/* __ILocationNumberInfo_FWD_DEFINED__ */


#ifndef __ICallingPartyInfo_FWD_DEFINED__
#define __ICallingPartyInfo_FWD_DEFINED__
typedef interface ICallingPartyInfo ICallingPartyInfo;
#endif 	/* __ICallingPartyInfo_FWD_DEFINED__ */


#ifndef __ICalledPartyInfo_FWD_DEFINED__
#define __ICalledPartyInfo_FWD_DEFINED__
typedef interface ICalledPartyInfo ICalledPartyInfo;
#endif 	/* __ICalledPartyInfo_FWD_DEFINED__ */


#ifndef __IUserToUserInfo_FWD_DEFINED__
#define __IUserToUserInfo_FWD_DEFINED__
typedef interface IUserToUserInfo IUserToUserInfo;
#endif 	/* __IUserToUserInfo_FWD_DEFINED__ */


#ifndef __Iq931Info_FWD_DEFINED__
#define __Iq931Info_FWD_DEFINED__
typedef interface Iq931Info Iq931Info;
#endif 	/* __Iq931Info_FWD_DEFINED__ */


#ifndef __CorrelationIDInfo_FWD_DEFINED__
#define __CorrelationIDInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class CorrelationIDInfo CorrelationIDInfo;
#else
typedef struct CorrelationIDInfo CorrelationIDInfo;
#endif /* __cplusplus */

#endif 	/* __CorrelationIDInfo_FWD_DEFINED__ */


#ifndef __SCFIDInfo_FWD_DEFINED__
#define __SCFIDInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class SCFIDInfo SCFIDInfo;
#else
typedef struct SCFIDInfo SCFIDInfo;
#endif /* __cplusplus */

#endif 	/* __SCFIDInfo_FWD_DEFINED__ */


#ifndef __RedirectionNumberRestrictionInfo_FWD_DEFINED__
#define __RedirectionNumberRestrictionInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class RedirectionNumberRestrictionInfo RedirectionNumberRestrictionInfo;
#else
typedef struct RedirectionNumberRestrictionInfo RedirectionNumberRestrictionInfo;
#endif /* __cplusplus */

#endif 	/* __RedirectionNumberRestrictionInfo_FWD_DEFINED__ */


#ifndef __CallingPartyCategoryInfo_FWD_DEFINED__
#define __CallingPartyCategoryInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class CallingPartyCategoryInfo CallingPartyCategoryInfo;
#else
typedef struct CallingPartyCategoryInfo CallingPartyCategoryInfo;
#endif /* __cplusplus */

#endif 	/* __CallingPartyCategoryInfo_FWD_DEFINED__ */


#ifndef __CauseIndicatorsInfo_FWD_DEFINED__
#define __CauseIndicatorsInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class CauseIndicatorsInfo CauseIndicatorsInfo;
#else
typedef struct CauseIndicatorsInfo CauseIndicatorsInfo;
#endif /* __cplusplus */

#endif 	/* __CauseIndicatorsInfo_FWD_DEFINED__ */


#ifndef __RedirectionNumberInfo_FWD_DEFINED__
#define __RedirectionNumberInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class RedirectionNumberInfo RedirectionNumberInfo;
#else
typedef struct RedirectionNumberInfo RedirectionNumberInfo;
#endif /* __cplusplus */

#endif 	/* __RedirectionNumberInfo_FWD_DEFINED__ */


#ifndef __RedirectionInformationInfo_FWD_DEFINED__
#define __RedirectionInformationInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class RedirectionInformationInfo RedirectionInformationInfo;
#else
typedef struct RedirectionInformationInfo RedirectionInformationInfo;
#endif /* __cplusplus */

#endif 	/* __RedirectionInformationInfo_FWD_DEFINED__ */


#ifndef __LocationNumberInfo_FWD_DEFINED__
#define __LocationNumberInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class LocationNumberInfo LocationNumberInfo;
#else
typedef struct LocationNumberInfo LocationNumberInfo;
#endif /* __cplusplus */

#endif 	/* __LocationNumberInfo_FWD_DEFINED__ */


#ifndef __CallingPartyInfo_FWD_DEFINED__
#define __CallingPartyInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class CallingPartyInfo CallingPartyInfo;
#else
typedef struct CallingPartyInfo CallingPartyInfo;
#endif /* __cplusplus */

#endif 	/* __CallingPartyInfo_FWD_DEFINED__ */


#ifndef __CalledPartyInfo_FWD_DEFINED__
#define __CalledPartyInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class CalledPartyInfo CalledPartyInfo;
#else
typedef struct CalledPartyInfo CalledPartyInfo;
#endif /* __cplusplus */

#endif 	/* __CalledPartyInfo_FWD_DEFINED__ */


#ifndef __q931Info_FWD_DEFINED__
#define __q931Info_FWD_DEFINED__

#ifdef __cplusplus
typedef class q931Info q931Info;
#else
typedef struct q931Info q931Info;
#endif /* __cplusplus */

#endif 	/* __q931Info_FWD_DEFINED__ */


#ifndef __UserToUserInfo_FWD_DEFINED__
#define __UserToUserInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class UserToUserInfo UserToUserInfo;
#else
typedef struct UserToUserInfo UserToUserInfo;
#endif /* __cplusplus */

#endif 	/* __UserToUserInfo_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ICorrelationIDInfo_INTERFACE_DEFINED__
#define __ICorrelationIDInfo_INTERFACE_DEFINED__

/* interface ICorrelationIDInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICorrelationIDInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2BF7C8B4-87D2-4F52-93C7-DD518868C00D")
    ICorrelationIDInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CorrelationID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CorrelationID( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICorrelationIDInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICorrelationIDInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICorrelationIDInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICorrelationIDInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICorrelationIDInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICorrelationIDInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICorrelationIDInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICorrelationIDInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CorrelationID )( 
            ICorrelationIDInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CorrelationID )( 
            ICorrelationIDInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ICorrelationIDInfoVtbl;

    interface ICorrelationIDInfo
    {
        CONST_VTBL struct ICorrelationIDInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICorrelationIDInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICorrelationIDInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICorrelationIDInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICorrelationIDInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICorrelationIDInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICorrelationIDInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICorrelationIDInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICorrelationIDInfo_get_CorrelationID(This,pVal)	\
    ( (This)->lpVtbl -> get_CorrelationID(This,pVal) ) 

#define ICorrelationIDInfo_put_CorrelationID(This,newVal)	\
    ( (This)->lpVtbl -> put_CorrelationID(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICorrelationIDInfo_INTERFACE_DEFINED__ */


#ifndef __ISCFIDInfo_INTERFACE_DEFINED__
#define __ISCFIDInfo_INTERFACE_DEFINED__

/* interface ISCFIDInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ISCFIDInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DB9308C4-E8AE-4547-95B8-A2C7E3C89D22")
    ISCFIDInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SCFID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SCFID( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISCFIDInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISCFIDInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISCFIDInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISCFIDInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISCFIDInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISCFIDInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISCFIDInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISCFIDInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SCFID )( 
            ISCFIDInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SCFID )( 
            ISCFIDInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ISCFIDInfoVtbl;

    interface ISCFIDInfo
    {
        CONST_VTBL struct ISCFIDInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISCFIDInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISCFIDInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISCFIDInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISCFIDInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ISCFIDInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ISCFIDInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ISCFIDInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ISCFIDInfo_get_SCFID(This,pVal)	\
    ( (This)->lpVtbl -> get_SCFID(This,pVal) ) 

#define ISCFIDInfo_put_SCFID(This,newVal)	\
    ( (This)->lpVtbl -> put_SCFID(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISCFIDInfo_INTERFACE_DEFINED__ */


#ifndef __IRedirectionNumberRestrictionInfo_INTERFACE_DEFINED__
#define __IRedirectionNumberRestrictionInfo_INTERFACE_DEFINED__

/* interface IRedirectionNumberRestrictionInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRedirectionNumberRestrictionInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("49C716D0-C8A6-45EB-8918-8275277B11AE")
    IRedirectionNumberRestrictionInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PresentationRestricted( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PresentationRestricted( 
            /* [in] */ LONG newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRedirectionNumberRestrictionInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRedirectionNumberRestrictionInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRedirectionNumberRestrictionInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PresentationRestricted )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PresentationRestricted )( 
            IRedirectionNumberRestrictionInfo * This,
            /* [in] */ LONG newVal);
        
        END_INTERFACE
    } IRedirectionNumberRestrictionInfoVtbl;

    interface IRedirectionNumberRestrictionInfo
    {
        CONST_VTBL struct IRedirectionNumberRestrictionInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRedirectionNumberRestrictionInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRedirectionNumberRestrictionInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRedirectionNumberRestrictionInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRedirectionNumberRestrictionInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRedirectionNumberRestrictionInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRedirectionNumberRestrictionInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRedirectionNumberRestrictionInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IRedirectionNumberRestrictionInfo_get_PresentationRestricted(This,pVal)	\
    ( (This)->lpVtbl -> get_PresentationRestricted(This,pVal) ) 

#define IRedirectionNumberRestrictionInfo_put_PresentationRestricted(This,newVal)	\
    ( (This)->lpVtbl -> put_PresentationRestricted(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRedirectionNumberRestrictionInfo_INTERFACE_DEFINED__ */


#ifndef __ICallingPartyCategoryInfo_INTERFACE_DEFINED__
#define __ICallingPartyCategoryInfo_INTERFACE_DEFINED__

/* interface ICallingPartyCategoryInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICallingPartyCategoryInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("69B466F9-A657-4DE2-B67D-329C1AB655B5")
    ICallingPartyCategoryInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallingPartyCategory( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CallingPartyCategory( 
            /* [in] */ LONG newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICallingPartyCategoryInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICallingPartyCategoryInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICallingPartyCategoryInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICallingPartyCategoryInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICallingPartyCategoryInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICallingPartyCategoryInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICallingPartyCategoryInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICallingPartyCategoryInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallingPartyCategory )( 
            ICallingPartyCategoryInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallingPartyCategory )( 
            ICallingPartyCategoryInfo * This,
            /* [in] */ LONG newVal);
        
        END_INTERFACE
    } ICallingPartyCategoryInfoVtbl;

    interface ICallingPartyCategoryInfo
    {
        CONST_VTBL struct ICallingPartyCategoryInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICallingPartyCategoryInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICallingPartyCategoryInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICallingPartyCategoryInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICallingPartyCategoryInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICallingPartyCategoryInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICallingPartyCategoryInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICallingPartyCategoryInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICallingPartyCategoryInfo_get_CallingPartyCategory(This,pVal)	\
    ( (This)->lpVtbl -> get_CallingPartyCategory(This,pVal) ) 

#define ICallingPartyCategoryInfo_put_CallingPartyCategory(This,newVal)	\
    ( (This)->lpVtbl -> put_CallingPartyCategory(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICallingPartyCategoryInfo_INTERFACE_DEFINED__ */


#ifndef __ICauseIndicatorsInfo_INTERFACE_DEFINED__
#define __ICauseIndicatorsInfo_INTERFACE_DEFINED__

/* interface ICauseIndicatorsInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICauseIndicatorsInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("10DBBCF6-ECB8-43A5-B76D-39E2C011593F")
    ICauseIndicatorsInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Location( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Location( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CodingStandart( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CodingStandart( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CauseValue( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CauseValue( 
            /* [in] */ LONG newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICauseIndicatorsInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICauseIndicatorsInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICauseIndicatorsInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICauseIndicatorsInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Location )( 
            ICauseIndicatorsInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Location )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CodingStandart )( 
            ICauseIndicatorsInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CodingStandart )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CauseValue )( 
            ICauseIndicatorsInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CauseValue )( 
            ICauseIndicatorsInfo * This,
            /* [in] */ LONG newVal);
        
        END_INTERFACE
    } ICauseIndicatorsInfoVtbl;

    interface ICauseIndicatorsInfo
    {
        CONST_VTBL struct ICauseIndicatorsInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICauseIndicatorsInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICauseIndicatorsInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICauseIndicatorsInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICauseIndicatorsInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICauseIndicatorsInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICauseIndicatorsInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICauseIndicatorsInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICauseIndicatorsInfo_get_Location(This,pVal)	\
    ( (This)->lpVtbl -> get_Location(This,pVal) ) 

#define ICauseIndicatorsInfo_put_Location(This,newVal)	\
    ( (This)->lpVtbl -> put_Location(This,newVal) ) 

#define ICauseIndicatorsInfo_get_CodingStandart(This,pVal)	\
    ( (This)->lpVtbl -> get_CodingStandart(This,pVal) ) 

#define ICauseIndicatorsInfo_put_CodingStandart(This,newVal)	\
    ( (This)->lpVtbl -> put_CodingStandart(This,newVal) ) 

#define ICauseIndicatorsInfo_get_CauseValue(This,pVal)	\
    ( (This)->lpVtbl -> get_CauseValue(This,pVal) ) 

#define ICauseIndicatorsInfo_put_CauseValue(This,newVal)	\
    ( (This)->lpVtbl -> put_CauseValue(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICauseIndicatorsInfo_INTERFACE_DEFINED__ */


#ifndef __IRedirectionNumberInfo_INTERFACE_DEFINED__
#define __IRedirectionNumberInfo_INTERFACE_DEFINED__

/* interface IRedirectionNumberInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRedirectionNumberInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("66F5A5D1-883A-4199-B71F-D6809B08AFA0")
    IRedirectionNumberInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NatureOfAddress( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NatureOfAddress( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OddEvenIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OddEvenIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NumberingPlanIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NumberingPlanIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InternalNetworkNumberIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InternalNetworkNumberIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectionAddressSignals( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RedirectionAddressSignals( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRedirectionNumberInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRedirectionNumberInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRedirectionNumberInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRedirectionNumberInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRedirectionNumberInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRedirectionNumberInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRedirectionNumberInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRedirectionNumberInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NatureOfAddress )( 
            IRedirectionNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NatureOfAddress )( 
            IRedirectionNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OddEvenIndicator )( 
            IRedirectionNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OddEvenIndicator )( 
            IRedirectionNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberingPlanIndicator )( 
            IRedirectionNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NumberingPlanIndicator )( 
            IRedirectionNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InternalNetworkNumberIndicator )( 
            IRedirectionNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InternalNetworkNumberIndicator )( 
            IRedirectionNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectionAddressSignals )( 
            IRedirectionNumberInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RedirectionAddressSignals )( 
            IRedirectionNumberInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } IRedirectionNumberInfoVtbl;

    interface IRedirectionNumberInfo
    {
        CONST_VTBL struct IRedirectionNumberInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRedirectionNumberInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRedirectionNumberInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRedirectionNumberInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRedirectionNumberInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRedirectionNumberInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRedirectionNumberInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRedirectionNumberInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IRedirectionNumberInfo_get_NatureOfAddress(This,pVal)	\
    ( (This)->lpVtbl -> get_NatureOfAddress(This,pVal) ) 

#define IRedirectionNumberInfo_put_NatureOfAddress(This,newVal)	\
    ( (This)->lpVtbl -> put_NatureOfAddress(This,newVal) ) 

#define IRedirectionNumberInfo_get_OddEvenIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_OddEvenIndicator(This,pVal) ) 

#define IRedirectionNumberInfo_put_OddEvenIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_OddEvenIndicator(This,newVal) ) 

#define IRedirectionNumberInfo_get_NumberingPlanIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_NumberingPlanIndicator(This,pVal) ) 

#define IRedirectionNumberInfo_put_NumberingPlanIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_NumberingPlanIndicator(This,newVal) ) 

#define IRedirectionNumberInfo_get_InternalNetworkNumberIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_InternalNetworkNumberIndicator(This,pVal) ) 

#define IRedirectionNumberInfo_put_InternalNetworkNumberIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_InternalNetworkNumberIndicator(This,newVal) ) 

#define IRedirectionNumberInfo_get_RedirectionAddressSignals(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectionAddressSignals(This,pVal) ) 

#define IRedirectionNumberInfo_put_RedirectionAddressSignals(This,newVal)	\
    ( (This)->lpVtbl -> put_RedirectionAddressSignals(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRedirectionNumberInfo_INTERFACE_DEFINED__ */


#ifndef __IRedirectionInformationInfo_INTERFACE_DEFINED__
#define __IRedirectionInformationInfo_INTERFACE_DEFINED__

/* interface IRedirectionInformationInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRedirectionInformationInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("48817086-F114-4364-B43C-1607DD92AC89")
    IRedirectionInformationInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectingIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RedirectingIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OriginalRedirectionReason( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OriginalRedirectionReason( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectionCounter( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RedirectionCounter( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectingReason( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RedirectingReason( 
            /* [in] */ LONG newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRedirectionInformationInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRedirectionInformationInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRedirectionInformationInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRedirectionInformationInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRedirectionInformationInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRedirectionInformationInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRedirectionInformationInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRedirectionInformationInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectingIndicator )( 
            IRedirectionInformationInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RedirectingIndicator )( 
            IRedirectionInformationInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OriginalRedirectionReason )( 
            IRedirectionInformationInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OriginalRedirectionReason )( 
            IRedirectionInformationInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectionCounter )( 
            IRedirectionInformationInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RedirectionCounter )( 
            IRedirectionInformationInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectingReason )( 
            IRedirectionInformationInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RedirectingReason )( 
            IRedirectionInformationInfo * This,
            /* [in] */ LONG newVal);
        
        END_INTERFACE
    } IRedirectionInformationInfoVtbl;

    interface IRedirectionInformationInfo
    {
        CONST_VTBL struct IRedirectionInformationInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRedirectionInformationInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRedirectionInformationInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRedirectionInformationInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IRedirectionInformationInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IRedirectionInformationInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IRedirectionInformationInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IRedirectionInformationInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IRedirectionInformationInfo_get_RedirectingIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectingIndicator(This,pVal) ) 

#define IRedirectionInformationInfo_put_RedirectingIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_RedirectingIndicator(This,newVal) ) 

#define IRedirectionInformationInfo_get_OriginalRedirectionReason(This,pVal)	\
    ( (This)->lpVtbl -> get_OriginalRedirectionReason(This,pVal) ) 

#define IRedirectionInformationInfo_put_OriginalRedirectionReason(This,newVal)	\
    ( (This)->lpVtbl -> put_OriginalRedirectionReason(This,newVal) ) 

#define IRedirectionInformationInfo_get_RedirectionCounter(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectionCounter(This,pVal) ) 

#define IRedirectionInformationInfo_put_RedirectionCounter(This,newVal)	\
    ( (This)->lpVtbl -> put_RedirectionCounter(This,newVal) ) 

#define IRedirectionInformationInfo_get_RedirectingReason(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectingReason(This,pVal) ) 

#define IRedirectionInformationInfo_put_RedirectingReason(This,newVal)	\
    ( (This)->lpVtbl -> put_RedirectingReason(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRedirectionInformationInfo_INTERFACE_DEFINED__ */


#ifndef __ILocationNumberInfo_INTERFACE_DEFINED__
#define __ILocationNumberInfo_INTERFACE_DEFINED__

/* interface ILocationNumberInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILocationNumberInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0F69FD0E-4087-43BA-85FA-2765FC144110")
    ILocationNumberInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NatureOfAddress( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NatureOfAddress( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OddEvenIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OddEvenIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NumberingPlanIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NumberingPlanIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ScreeningIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ScreeningIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PresentationRestrictionIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PresentationRestrictionIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InternationalNNI( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InternationalNNI( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LocationNumber( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LocationNumber( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILocationNumberInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILocationNumberInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILocationNumberInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILocationNumberInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILocationNumberInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILocationNumberInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILocationNumberInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILocationNumberInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NatureOfAddress )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NatureOfAddress )( 
            ILocationNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OddEvenIndicator )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OddEvenIndicator )( 
            ILocationNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberingPlanIndicator )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NumberingPlanIndicator )( 
            ILocationNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ScreeningIndicator )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ScreeningIndicator )( 
            ILocationNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PresentationRestrictionIndicator )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PresentationRestrictionIndicator )( 
            ILocationNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InternationalNNI )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InternationalNNI )( 
            ILocationNumberInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LocationNumber )( 
            ILocationNumberInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LocationNumber )( 
            ILocationNumberInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ILocationNumberInfoVtbl;

    interface ILocationNumberInfo
    {
        CONST_VTBL struct ILocationNumberInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILocationNumberInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILocationNumberInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILocationNumberInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILocationNumberInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILocationNumberInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILocationNumberInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILocationNumberInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILocationNumberInfo_get_NatureOfAddress(This,pVal)	\
    ( (This)->lpVtbl -> get_NatureOfAddress(This,pVal) ) 

#define ILocationNumberInfo_put_NatureOfAddress(This,newVal)	\
    ( (This)->lpVtbl -> put_NatureOfAddress(This,newVal) ) 

#define ILocationNumberInfo_get_OddEvenIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_OddEvenIndicator(This,pVal) ) 

#define ILocationNumberInfo_put_OddEvenIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_OddEvenIndicator(This,newVal) ) 

#define ILocationNumberInfo_get_NumberingPlanIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_NumberingPlanIndicator(This,pVal) ) 

#define ILocationNumberInfo_put_NumberingPlanIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_NumberingPlanIndicator(This,newVal) ) 

#define ILocationNumberInfo_get_ScreeningIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_ScreeningIndicator(This,pVal) ) 

#define ILocationNumberInfo_put_ScreeningIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_ScreeningIndicator(This,newVal) ) 

#define ILocationNumberInfo_get_PresentationRestrictionIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_PresentationRestrictionIndicator(This,pVal) ) 

#define ILocationNumberInfo_put_PresentationRestrictionIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_PresentationRestrictionIndicator(This,newVal) ) 

#define ILocationNumberInfo_get_InternationalNNI(This,pVal)	\
    ( (This)->lpVtbl -> get_InternationalNNI(This,pVal) ) 

#define ILocationNumberInfo_put_InternationalNNI(This,newVal)	\
    ( (This)->lpVtbl -> put_InternationalNNI(This,newVal) ) 

#define ILocationNumberInfo_get_LocationNumber(This,pVal)	\
    ( (This)->lpVtbl -> get_LocationNumber(This,pVal) ) 

#define ILocationNumberInfo_put_LocationNumber(This,newVal)	\
    ( (This)->lpVtbl -> put_LocationNumber(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILocationNumberInfo_INTERFACE_DEFINED__ */


#ifndef __ICallingPartyInfo_INTERFACE_DEFINED__
#define __ICallingPartyInfo_INTERFACE_DEFINED__

/* interface ICallingPartyInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICallingPartyInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("306027F3-3474-4510-BFE0-F0AAB400D958")
    ICallingPartyInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NatureOfAddress( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NatureOfAddress( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OddEvenIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OddEvenIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NumberingPlanIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NumberingPlanIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ScreeningIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ScreeningIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PresentationRestrictionIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PresentationRestrictionIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallingNumberIncompleteIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CallingNumberIncompleteIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallingAddressSignals( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CallingAddressSignals( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICallingPartyInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICallingPartyInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICallingPartyInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICallingPartyInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICallingPartyInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICallingPartyInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICallingPartyInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICallingPartyInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NatureOfAddress )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NatureOfAddress )( 
            ICallingPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OddEvenIndicator )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OddEvenIndicator )( 
            ICallingPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberingPlanIndicator )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NumberingPlanIndicator )( 
            ICallingPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ScreeningIndicator )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ScreeningIndicator )( 
            ICallingPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PresentationRestrictionIndicator )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PresentationRestrictionIndicator )( 
            ICallingPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallingNumberIncompleteIndicator )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallingNumberIncompleteIndicator )( 
            ICallingPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallingAddressSignals )( 
            ICallingPartyInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CallingAddressSignals )( 
            ICallingPartyInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ICallingPartyInfoVtbl;

    interface ICallingPartyInfo
    {
        CONST_VTBL struct ICallingPartyInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICallingPartyInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICallingPartyInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICallingPartyInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICallingPartyInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICallingPartyInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICallingPartyInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICallingPartyInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICallingPartyInfo_get_NatureOfAddress(This,pVal)	\
    ( (This)->lpVtbl -> get_NatureOfAddress(This,pVal) ) 

#define ICallingPartyInfo_put_NatureOfAddress(This,newVal)	\
    ( (This)->lpVtbl -> put_NatureOfAddress(This,newVal) ) 

#define ICallingPartyInfo_get_OddEvenIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_OddEvenIndicator(This,pVal) ) 

#define ICallingPartyInfo_put_OddEvenIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_OddEvenIndicator(This,newVal) ) 

#define ICallingPartyInfo_get_NumberingPlanIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_NumberingPlanIndicator(This,pVal) ) 

#define ICallingPartyInfo_put_NumberingPlanIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_NumberingPlanIndicator(This,newVal) ) 

#define ICallingPartyInfo_get_ScreeningIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_ScreeningIndicator(This,pVal) ) 

#define ICallingPartyInfo_put_ScreeningIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_ScreeningIndicator(This,newVal) ) 

#define ICallingPartyInfo_get_PresentationRestrictionIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_PresentationRestrictionIndicator(This,pVal) ) 

#define ICallingPartyInfo_put_PresentationRestrictionIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_PresentationRestrictionIndicator(This,newVal) ) 

#define ICallingPartyInfo_get_CallingNumberIncompleteIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_CallingNumberIncompleteIndicator(This,pVal) ) 

#define ICallingPartyInfo_put_CallingNumberIncompleteIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_CallingNumberIncompleteIndicator(This,newVal) ) 

#define ICallingPartyInfo_get_CallingAddressSignals(This,pVal)	\
    ( (This)->lpVtbl -> get_CallingAddressSignals(This,pVal) ) 

#define ICallingPartyInfo_put_CallingAddressSignals(This,newVal)	\
    ( (This)->lpVtbl -> put_CallingAddressSignals(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICallingPartyInfo_INTERFACE_DEFINED__ */


#ifndef __ICalledPartyInfo_INTERFACE_DEFINED__
#define __ICalledPartyInfo_INTERFACE_DEFINED__

/* interface ICalledPartyInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICalledPartyInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AFFF808B-57AD-4531-BFA9-23F03DEC53D2")
    ICalledPartyInfo : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NatureOfAddress( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NatureOfAddress( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OddEvenIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OddEvenIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NumberingPlanIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NumberingPlanIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InternalNetworkNumberIndicator( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InternalNetworkNumberIndicator( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CalledAddressSignals( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CalledAddressSignals( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICalledPartyInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICalledPartyInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICalledPartyInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICalledPartyInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICalledPartyInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICalledPartyInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICalledPartyInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICalledPartyInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NatureOfAddress )( 
            ICalledPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NatureOfAddress )( 
            ICalledPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OddEvenIndicator )( 
            ICalledPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OddEvenIndicator )( 
            ICalledPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberingPlanIndicator )( 
            ICalledPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NumberingPlanIndicator )( 
            ICalledPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InternalNetworkNumberIndicator )( 
            ICalledPartyInfo * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InternalNetworkNumberIndicator )( 
            ICalledPartyInfo * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CalledAddressSignals )( 
            ICalledPartyInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CalledAddressSignals )( 
            ICalledPartyInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ICalledPartyInfoVtbl;

    interface ICalledPartyInfo
    {
        CONST_VTBL struct ICalledPartyInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICalledPartyInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICalledPartyInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICalledPartyInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICalledPartyInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICalledPartyInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICalledPartyInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICalledPartyInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICalledPartyInfo_get_NatureOfAddress(This,pVal)	\
    ( (This)->lpVtbl -> get_NatureOfAddress(This,pVal) ) 

#define ICalledPartyInfo_put_NatureOfAddress(This,newVal)	\
    ( (This)->lpVtbl -> put_NatureOfAddress(This,newVal) ) 

#define ICalledPartyInfo_get_OddEvenIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_OddEvenIndicator(This,pVal) ) 

#define ICalledPartyInfo_put_OddEvenIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_OddEvenIndicator(This,newVal) ) 

#define ICalledPartyInfo_get_NumberingPlanIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_NumberingPlanIndicator(This,pVal) ) 

#define ICalledPartyInfo_put_NumberingPlanIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_NumberingPlanIndicator(This,newVal) ) 

#define ICalledPartyInfo_get_InternalNetworkNumberIndicator(This,pVal)	\
    ( (This)->lpVtbl -> get_InternalNetworkNumberIndicator(This,pVal) ) 

#define ICalledPartyInfo_put_InternalNetworkNumberIndicator(This,newVal)	\
    ( (This)->lpVtbl -> put_InternalNetworkNumberIndicator(This,newVal) ) 

#define ICalledPartyInfo_get_CalledAddressSignals(This,pVal)	\
    ( (This)->lpVtbl -> get_CalledAddressSignals(This,pVal) ) 

#define ICalledPartyInfo_put_CalledAddressSignals(This,newVal)	\
    ( (This)->lpVtbl -> put_CalledAddressSignals(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICalledPartyInfo_INTERFACE_DEFINED__ */


#ifndef __IUserToUserInfo_INTERFACE_DEFINED__
#define __IUserToUserInfo_INTERFACE_DEFINED__

/* interface IUserToUserInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IUserToUserInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("676BF8DA-8DBA-4E45-9FAA-84DA400F9DCE")
    IUserToUserInfo : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_UserToUser( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_UserToUser( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IUserToUserInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IUserToUserInfo * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IUserToUserInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IUserToUserInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IUserToUserInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IUserToUserInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IUserToUserInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IUserToUserInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserToUser )( 
            IUserToUserInfo * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserToUser )( 
            IUserToUserInfo * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } IUserToUserInfoVtbl;

    interface IUserToUserInfo
    {
        CONST_VTBL struct IUserToUserInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IUserToUserInfo_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IUserToUserInfo_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IUserToUserInfo_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IUserToUserInfo_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IUserToUserInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IUserToUserInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IUserToUserInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IUserToUserInfo_get_UserToUser(This,pVal)	\
    ( (This)->lpVtbl -> get_UserToUser(This,pVal) ) 

#define IUserToUserInfo_put_UserToUser(This,newVal)	\
    ( (This)->lpVtbl -> put_UserToUser(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IUserToUserInfo_INTERFACE_DEFINED__ */


#ifndef __Iq931Info_INTERFACE_DEFINED__
#define __Iq931Info_INTERFACE_DEFINED__

/* interface Iq931Info */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_Iq931Info;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("33EE8759-4DF2-4252-B331-173FB59DC994")
    Iq931Info : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CalledPartyNumber( 
            /* [retval][out] */ ICalledPartyInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallingPartyCategory( 
            /* [retval][out] */ ICallingPartyCategoryInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CallingPartyNumber( 
            /* [retval][out] */ ICallingPartyInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CauseIndicators( 
            /* [retval][out] */ ICauseIndicatorsInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CorrelationID( 
            /* [retval][out] */ ICorrelationIDInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LocationNumber( 
            /* [retval][out] */ ILocationNumberInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectionNumber( 
            /* [retval][out] */ IRedirectionNumberInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectionNumberRestriction( 
            /* [retval][out] */ IRedirectionNumberRestrictionInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SCFID( 
            /* [retval][out] */ ISCFIDInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RedirectionInformation( 
            /* [retval][out] */ IRedirectionInformationInfo **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UserToUser( 
            /* [retval][out] */ IUserToUserInfo **pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct Iq931InfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            Iq931Info * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            Iq931Info * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            Iq931Info * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            Iq931Info * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            Iq931Info * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            Iq931Info * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            Iq931Info * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CalledPartyNumber )( 
            Iq931Info * This,
            /* [retval][out] */ ICalledPartyInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallingPartyCategory )( 
            Iq931Info * This,
            /* [retval][out] */ ICallingPartyCategoryInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CallingPartyNumber )( 
            Iq931Info * This,
            /* [retval][out] */ ICallingPartyInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CauseIndicators )( 
            Iq931Info * This,
            /* [retval][out] */ ICauseIndicatorsInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CorrelationID )( 
            Iq931Info * This,
            /* [retval][out] */ ICorrelationIDInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LocationNumber )( 
            Iq931Info * This,
            /* [retval][out] */ ILocationNumberInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectionNumber )( 
            Iq931Info * This,
            /* [retval][out] */ IRedirectionNumberInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectionNumberRestriction )( 
            Iq931Info * This,
            /* [retval][out] */ IRedirectionNumberRestrictionInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SCFID )( 
            Iq931Info * This,
            /* [retval][out] */ ISCFIDInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RedirectionInformation )( 
            Iq931Info * This,
            /* [retval][out] */ IRedirectionInformationInfo **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserToUser )( 
            Iq931Info * This,
            /* [retval][out] */ IUserToUserInfo **pVal);
        
        END_INTERFACE
    } Iq931InfoVtbl;

    interface Iq931Info
    {
        CONST_VTBL struct Iq931InfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define Iq931Info_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define Iq931Info_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define Iq931Info_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define Iq931Info_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define Iq931Info_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define Iq931Info_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define Iq931Info_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define Iq931Info_get_CalledPartyNumber(This,pVal)	\
    ( (This)->lpVtbl -> get_CalledPartyNumber(This,pVal) ) 

#define Iq931Info_get_CallingPartyCategory(This,pVal)	\
    ( (This)->lpVtbl -> get_CallingPartyCategory(This,pVal) ) 

#define Iq931Info_get_CallingPartyNumber(This,pVal)	\
    ( (This)->lpVtbl -> get_CallingPartyNumber(This,pVal) ) 

#define Iq931Info_get_CauseIndicators(This,pVal)	\
    ( (This)->lpVtbl -> get_CauseIndicators(This,pVal) ) 

#define Iq931Info_get_CorrelationID(This,pVal)	\
    ( (This)->lpVtbl -> get_CorrelationID(This,pVal) ) 

#define Iq931Info_get_LocationNumber(This,pVal)	\
    ( (This)->lpVtbl -> get_LocationNumber(This,pVal) ) 

#define Iq931Info_get_RedirectionNumber(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectionNumber(This,pVal) ) 

#define Iq931Info_get_RedirectionNumberRestriction(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectionNumberRestriction(This,pVal) ) 

#define Iq931Info_get_SCFID(This,pVal)	\
    ( (This)->lpVtbl -> get_SCFID(This,pVal) ) 

#define Iq931Info_get_RedirectionInformation(This,pVal)	\
    ( (This)->lpVtbl -> get_RedirectionInformation(This,pVal) ) 

#define Iq931Info_get_UserToUser(This,pVal)	\
    ( (This)->lpVtbl -> get_UserToUser(This,pVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __Iq931Info_INTERFACE_DEFINED__ */



#ifndef __q931Lib_LIBRARY_DEFINED__
#define __q931Lib_LIBRARY_DEFINED__

/* library q931Lib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_q931Lib;

EXTERN_C const CLSID CLSID_CorrelationIDInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("EEBF977A-BABF-4BA3-B0EB-70F8518C8FCD")
CorrelationIDInfo;
#endif

EXTERN_C const CLSID CLSID_SCFIDInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("7F0779BC-5FC3-4699-8D18-8854B43D4771")
SCFIDInfo;
#endif

EXTERN_C const CLSID CLSID_RedirectionNumberRestrictionInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("6248803D-69F5-4257-B94E-9E04F92383C1")
RedirectionNumberRestrictionInfo;
#endif

EXTERN_C const CLSID CLSID_CallingPartyCategoryInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("87CDBB10-272B-4D2C-8F5A-EA8A551422D1")
CallingPartyCategoryInfo;
#endif

EXTERN_C const CLSID CLSID_CauseIndicatorsInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("FB81DE6F-6689-4631-9A8C-853ED5AA5175")
CauseIndicatorsInfo;
#endif

EXTERN_C const CLSID CLSID_RedirectionNumberInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("B12C4510-50DE-472E-8E47-FC60AAE8D919")
RedirectionNumberInfo;
#endif

EXTERN_C const CLSID CLSID_RedirectionInformationInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("3F9D574E-7EE4-4EE9-B206-868972F6A248")
RedirectionInformationInfo;
#endif

EXTERN_C const CLSID CLSID_LocationNumberInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("8CAEC381-860C-4B02-AC2B-7BE681906AB1")
LocationNumberInfo;
#endif

EXTERN_C const CLSID CLSID_CallingPartyInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("EF349A46-305D-46A9-ABAE-CE4247AB33BD")
CallingPartyInfo;
#endif

EXTERN_C const CLSID CLSID_CalledPartyInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("10ED0CF8-0ED1-4F9D-B766-16DDDC40C8E8")
CalledPartyInfo;
#endif

EXTERN_C const CLSID CLSID_q931Info;

#ifdef __cplusplus

class DECLSPEC_UUID("6471F97B-EF44-4E76-BCE6-78AC3F4391A9")
q931Info;
#endif

EXTERN_C const CLSID CLSID_UserToUserInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("DDBFC10D-5852-4179-9BC0-66CA44D939BF")
UserToUserInfo;
#endif
#endif /* __q931Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


