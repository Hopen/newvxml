// CalledPartyInfo.h : Declaration of the CCalledPartyInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CCalledPartyInfo

class ATL_NO_VTABLE CCalledPartyInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCalledPartyInfo, &CLSID_CalledPartyInfo>,
	public IDispatchImpl<ICalledPartyInfo, &IID_ICalledPartyInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCalledPartyInfo()
	{
		m_NatureOfAddress = 0;
		m_OddEvenIndicator = 0;
		m_NumberingPlanIndicator = 0;
		m_InternalNetworkNumberIndicator = 0;
		m_CalledAddressSignals = L"";
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CALLEDPARTYINFO)


BEGIN_COM_MAP(CCalledPartyInfo)
	COM_INTERFACE_ENTRY(ICalledPartyInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_NatureOfAddress;
	long m_OddEvenIndicator;
	long m_NumberingPlanIndicator;
	long m_InternalNetworkNumberIndicator;
	CComBSTR m_CalledAddressSignals;

public:

	STDMETHOD(get_NatureOfAddress)(LONG* pVal);
	STDMETHOD(put_NatureOfAddress)(LONG newVal);
	STDMETHOD(get_OddEvenIndicator)(LONG* pVal);
	STDMETHOD(put_OddEvenIndicator)(LONG newVal);
	STDMETHOD(get_NumberingPlanIndicator)(LONG* pVal);
	STDMETHOD(put_NumberingPlanIndicator)(LONG newVal);
	STDMETHOD(get_InternalNetworkNumberIndicator)(LONG* pVal);
	STDMETHOD(put_InternalNetworkNumberIndicator)(LONG newVal);
	STDMETHOD(get_CalledAddressSignals)(BSTR* pVal);
	STDMETHOD(put_CalledAddressSignals)(BSTR newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(CalledPartyInfo), CCalledPartyInfo)
