// RedirectionNumberRestrictionInfo.h : Declaration of the CRedirectionNumberRestrictionInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CRedirectionNumberRestrictionInfo

class ATL_NO_VTABLE CRedirectionNumberRestrictionInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRedirectionNumberRestrictionInfo, &CLSID_RedirectionNumberRestrictionInfo>,
	public IDispatchImpl<IRedirectionNumberRestrictionInfo, &IID_IRedirectionNumberRestrictionInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CRedirectionNumberRestrictionInfo()
	{
		m_PresentationRestricted = 0;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_REDIRECTIONNUMBERRESTRICTIONINFO)


BEGIN_COM_MAP(CRedirectionNumberRestrictionInfo)
	COM_INTERFACE_ENTRY(IRedirectionNumberRestrictionInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_PresentationRestricted;

public:

	STDMETHOD(get_PresentationRestricted)(LONG* pVal);
	STDMETHOD(put_PresentationRestricted)(LONG newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(RedirectionNumberRestrictionInfo), CRedirectionNumberRestrictionInfo)
