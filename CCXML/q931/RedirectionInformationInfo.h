// RedirectionInformationInfo.h : Declaration of the CRedirectionInformationInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CRedirectionInformationInfo

class ATL_NO_VTABLE CRedirectionInformationInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRedirectionInformationInfo, &CLSID_RedirectionInformationInfo>,
	public IDispatchImpl<IRedirectionInformationInfo, &IID_IRedirectionInformationInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CRedirectionInformationInfo()
	{
		m_RedirectingIndicator = 0;
		m_OriginalRedirectionReason = 0;
		m_RedirectionCounter = 0;
		m_RedirectingReason = 0;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_REDIRECTIONINFORMATIONINFO)


BEGIN_COM_MAP(CRedirectionInformationInfo)
	COM_INTERFACE_ENTRY(IRedirectionInformationInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_RedirectingIndicator;
	long m_OriginalRedirectionReason;
	long m_RedirectionCounter;
	long m_RedirectingReason;

public:

	STDMETHOD(get_RedirectingIndicator)(LONG* pVal);
	STDMETHOD(put_RedirectingIndicator)(LONG newVal);
	STDMETHOD(get_OriginalRedirectionReason)(LONG* pVal);
	STDMETHOD(put_OriginalRedirectionReason)(LONG newVal);
	STDMETHOD(get_RedirectionCounter)(LONG* pVal);
	STDMETHOD(put_RedirectionCounter)(LONG newVal);
	STDMETHOD(get_RedirectingReason)(LONG* pVal);
	STDMETHOD(put_RedirectingReason)(LONG newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(RedirectionInformationInfo), CRedirectionInformationInfo)
