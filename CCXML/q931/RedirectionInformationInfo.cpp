// RedirectionInformationInfo.cpp : Implementation of CRedirectionInformationInfo

#include ".\stdafx.h"
#include "RedirectionInformationInfo.h"
#include ".\redirectioninformationinfo.h"


// CRedirectionInformationInfo


STDMETHODIMP CRedirectionInformationInfo::get_RedirectingIndicator(LONG* pVal)
{
	try {
		*pVal = m_RedirectingIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::put_RedirectingIndicator(LONG newVal)
{
	try {
		m_RedirectingIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::get_OriginalRedirectionReason(LONG* pVal)
{
	try {
		*pVal = m_OriginalRedirectionReason;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::put_OriginalRedirectionReason(LONG newVal)
{
	try {
		m_OriginalRedirectionReason = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::get_RedirectionCounter(LONG* pVal)
{
	try {
		*pVal = m_RedirectionCounter;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::put_RedirectionCounter(LONG newVal)
{
	try {
		m_RedirectionCounter = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::get_RedirectingReason(LONG* pVal)
{
	try {
		*pVal = m_RedirectingReason;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CRedirectionInformationInfo::put_RedirectingReason(LONG newVal)
{
	try {
		m_RedirectingReason = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
