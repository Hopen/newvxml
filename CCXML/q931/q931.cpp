// q931.cpp : Implementation of DLL Exports.

#include ".\stdafx.h"
#include "resource.h"
#include "q931.h"

// #define MYNDEBUG
// #ifndef MYNDEBUG
// #include <crtdbg.h>
// #endif

class Cq931Module : public CAtlDllModuleT< Cq931Module >
{
public :
	DECLARE_LIBID(LIBID_q931Lib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_Q931, "{D0839AF3-567B-416B-B38F-F7FDC5862ACA}")
};

Cq931Module _AtlModule;

#ifndef MYNDEBUG
HANDLE g_hLogFile = INVALID_HANDLE_VALUE;
#endif

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInstance;
	if (dwReason == DLL_PROCESS_ATTACH) {
#ifndef MYNDEBUG
		if (g_hLogFile == INVALID_HANDLE_VALUE) {
			g_hLogFile = CreateFile("c:\\q931debuglog.txt", GENERIC_WRITE, FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE, 
				NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		}

		_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
		_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
		_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
		_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_ALWAYS_DF);
		_CrtSetReportFile(_CRT_WARN, g_hLogFile);
		_CrtSetReportFile(_CRT_ERROR, g_hLogFile);
		_CrtSetReportFile(_CRT_ASSERT, g_hLogFile);
#endif
	}
	if (dwReason == DLL_PROCESS_DETACH) {
#ifndef MYNDEBUG
		if (g_hLogFile != INVALID_HANDLE_VALUE) {
			CloseHandle(g_hLogFile);
		}
#endif
	}

    return _AtlModule.DllMain(dwReason, lpReserved); 
}


// Used to determine whether the DLL can be unloaded by OLE
STDAPI DllCanUnloadNow(void)
{
	HRESULT hRes = _AtlModule.DllCanUnloadNow();
	return hRes;
}


// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _AtlModule.DllGetClassObject(rclsid, riid, ppv);
}


// DllRegisterServer - Adds entries to the system registry
STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    HRESULT hr = _AtlModule.DllRegisterServer();
	return hr;
}


// DllUnregisterServer - Removes entries from the system registry
STDAPI DllUnregisterServer(void)
{
	HRESULT hr = _AtlModule.DllUnregisterServer();
	return hr;
}
