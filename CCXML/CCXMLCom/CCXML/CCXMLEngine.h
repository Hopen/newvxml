/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once
#include <activscp.h>
#include "CCXMLNameTable.h"
#include "..\EventBridge.h"
#include "..\HackedScriptControl.h"
#include <list>
#include <map>
#include "CCXMLEvents.h"
#include "CCXMLSession.h"
#include"boost/format.hpp"

//#include "..\Common\EngineScriptSite.h"

namespace CCXML
{
//class CScriptControl : public CHackedScriptControl
//	{
//	private:
//		CScopedNameTable* m_pNameTbl;
//
//	public:
//		CScriptControl(CScopedNameTable* pNameTbl);
//		virtual HRESULT OnGetAddr(SYM* pSym, VAR* pVar);
//		virtual HRESULT OnAssignVar(CSession* pSes, VAR* pVarDst, VAR* pVarSrc, DWORD dw);
//	};

	//class CScriptEngine
	//{
	//private:
	//	CScopedNameTable*	m_pNameTbl;
	//	CScriptControl		m_ScControl;
	//	
	//private:

	//public:
	//	CScriptEngine(CScopedNameTable* pNameTbl);
	//	~CScriptEngine();
	//	CComVariant ExprEval(const std::wstring& sExpr);
	//	bool ExprEvalToBool(const std::wstring& sExpr);
	//	std::wstring ExprEvalToStr(const std::wstring& sExpr);
	//	CComVariant ExprEvalToType(const std::wstring& sExpr, VARTYPE vt);
	//	void ExecScript(const std::wstring& sExpr);
	//	void ExecStatement(const std::wstring& sExpr);
	//	void Assign(const std::wstring& sExpr, const CComVariant& value);
	//	void AddObject(const CCXMLString& sName, IDispatch* object);
	//};

class CXMLScriptSite : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IActiveScriptSite 
{
public:

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CXMLScriptSite)
		COM_INTERFACE_ENTRY(IActiveScriptSite)
	END_COM_MAP()

	CXMLScriptSite() 
	{
	}
	~CXMLScriptSite() 
	{
		//ScriptObjects::iterator it = m_objs.begin();
		//while(it!=m_objs.end())
		//{
		//	CComPtr<IDispatch> spDisScriptObject;
		//	it->second->QueryInterface(IID_IDispatch, (void**)&spDisScriptObject);
		//	delete dynamic_cast<CEvent*>(spDisScriptObject.Detach());
		//	++it;
		//}
		m_pSes.reset();
		m_objs.clear();
	}

	// ������ IActiveScriptSite...
	virtual HRESULT __stdcall GetLCID(LCID *plcid) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall GetItemInfo(LPCOLESTR pstrName,
		DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppti) 
	{
		if(ppti) 
		{
			*ppti = NULL;

			// ���� ������ ITypeInfo... 
			if(dwReturnMask & SCRIPTINFO_ITYPEINFO) {
				*ppti = m_spTypeInfo;
				(*ppti)->AddRef();
			}
		}

		// ���� Windows Scripting ��������� ppunkItem...
		if(ppunkItem)
		{
			*ppunkItem = NULL;

			// ���� Windows Scripting ������� IUnknown ������ �������...
			if(dwReturnMask & SCRIPTINFO_IUNKNOWN) 
			{
				// ...� ��������� ������ � ������ pstrName...
				USES_CONVERSION;
				//if (!lstrcmpi(_T("event1"), pstrName)) 
				ScriptObjects::iterator it = m_objs.find(pstrName);
				if (it != m_objs.end())
				{
					// ...�� ����������� ��� ������.
					*ppunkItem = it->second.pdispVal;
					// ...� AddRef'�� ��� ������...
					(*ppunkItem)->AddRef();
				}
			}
		}
		return S_OK;
	}

	virtual HRESULT __stdcall GetDocVersionString(BSTR *pbstrVersion) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnScriptTerminate(
		const VARIANT *pvarResult, const EXCEPINFO *pexcepInfo) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnStateChange(SCRIPTSTATE ssScriptState) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnScriptError(
		IActiveScriptError *pscriptError) 
	{
		// ��� ��������� �������� � ������ ������ � �������.
		// ����� ��������� ���������� � pscriptError.
		EXCEPINFO ei;
		pscriptError->GetExceptionInfo(&ei);

		//USES_CONVERSION;
		//::MessageBox(::GetActiveWindow(), ei.bstrDescription, L"Script Error", MB_SETFOREGROUND);

		CMessage evt(L"error.semantic");
		evt[L"reason"]	= CCXMLString(ei.bstrDescription).c_str();
		evt[L"tagname"]	= L"Script_Error";
		evt[L"description"] = L"Runtime vbscript error";

		SessionPtr2 pSes = m_pSes.lock();
		pSes->DoError(evt, L"VBENGINE");

		return S_OK;
	}

	virtual HRESULT __stdcall OnEnterScript(void) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnLeaveScript(void) 
	{
		return S_OK;
	}

	HRESULT AddObject(const std::wstring _name, const CComVariant& object)
	{
		LockGuard<std::mutex> lock(L"CXMLScriptSite::AddObject", m_mutex);
		//std::lock_guard<std::mutex> lock(m_mutex);
 
		m_objs[_name] = object;
		return S_OK;
	}

	HRESULT AddObject2(const std::wstring _name, const CComVariant& aNewObject)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CXMLScriptSite::AddObject2", m_mutex);

		auto it = m_objs.find(_name);
		if (it == m_objs.end()) // add new
		{
			m_objs[_name] = aNewObject;
		}
		else
		{
			NameTableDispatch* bridgeCur = static_cast<NameTableDispatch*>(it->second.pdispVal);
			NameTableDispatch* bridgeNew = static_cast<NameTableDispatch*>(aNewObject.pdispVal);
			if (bridgeCur)
			{
				bridgeCur->Copy(*bridgeNew);
			}
		}
	
		return S_OK;
	}


	CComVariant GetMyObject(const std::wstring _name)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CXMLScriptSite::GetMyObject", m_mutex);

		ScriptObjects::iterator it = m_objs.find(_name);
		if (it == m_objs.end()) // add new
		{
			//CMessage msg(L"empty");
			//EventObject evt (new DEBUG_NEW_PLACEMENT CEvent(msg));
			//return evt->AsVariant();
			
			throw std::runtime_error((boost::format("CXMLScriptSite: cannot find object \"%s\"") % _name.c_str()).str());
		}
		return it->second;
	}


	HRESULT DelObject(const std::wstring _name)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CXMLScriptSite::DelObject", m_mutex);

		ScriptObjects::iterator it = m_objs.find(_name);
		if (it!=m_objs.end())
		{
			//delete it->second;
			m_objs.erase(it);
		}

		return S_OK;
	}

	HRESULT Clear()
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CXMLScriptSite::Clear", m_mutex);

		m_objs.clear();
		return S_OK;
	}

	void SetSession(SessionPtr  pSes)
	{
		m_pSes = pSes;
	}

	SessionPtr GetSessionPtr() const
	{
		return m_pSes;
	}


public:   
	//CComPtr<IUnknown> m_spUnkScriptObject;
	CComPtr<ITypeInfo> m_spTypeInfo;

private:
	typedef std::map<std::wstring, CComVariant> ScriptObjects;
	ScriptObjects  m_objs;

	SessionPtr m_pSes;

	mutable std::mutex m_mutex;

}; 

	extern const GUID CLSID_VBScript; 

	class CScriptEngine
	{
	private:

		CComObject<CXMLScriptSite> * m_pMySite;
		CComPtr<IActiveScriptParse>    m_spASP;
		CComPtr<IActiveScript>         m_spAS;

		EngineLogPtr			m_Log;
		
		CCXMLString                  m_scriptSource;
		void Normalization(std::wstring& sExpr, std::wstring simbolsIn[],std::wstring simbolsOut[]);

		mutable std::mutex m_mutex;
	public:
		CScriptEngine(SessionPtr2 pSes, EngineLogPtr aLogPtr);
		~CScriptEngine();

		void Clear();

		HRESULT AddObject(const CCXMLString& sName, /*IDispatch* object*/const CComVariant& object);
		HRESULT AddObject2(const CCXMLString& sName, /*IDispatch* object*/const CComVariant& object);
		HRESULT DelObject(const CCXMLString& sName);
		HRESULT ExecStatement(const std::wstring& sExpr);
		void Assign(const std::wstring& sExpr, const CComVariant& value);
		bool ExprEvalToBool(const std::wstring& sExpr);
		std::wstring ExprEvalToStr(const std::wstring& sExpr);
		CComVariant ExprEvalToType(const std::wstring& sExpr, VARTYPE vt);
		CComVariant ExprEvalToVariant(const std::wstring& sExpr);
		void ExecScript(const std::wstring& sExpr);

		CCXMLString ScriptSource()const;
	private:
		CComVariant ExprEval(const std::wstring& sExpr);

	private:
		template <class...TArgs>
		void Log(TArgs&& ... aArgs)const
		{
			auto pLog = m_Log.lock();
			if (pLog)
			{
				pLog->Log(std::forward<TArgs>(aArgs)...);
			}

		}

	};
}
