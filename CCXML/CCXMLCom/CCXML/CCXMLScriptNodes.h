/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "location.hh"
#include <atlbase.h>

namespace CCXML
{
/*	template <class T>
	class CNodeSharedPtr
	{
	private:
		int*	m_pUseCount;
		T*		m_pNode;

		inline void reset()
		{
			if (!--m_pUseCount[0])
			{
				delete m_pUseCount;
				if (m_pNode)
				{
					delete m_pNode;
				}
			}
		}

	public:
		CNodeSharedPtr() : m_pUseCount(new int(1)), m_pNode(NULL)
		{
		}
		CNodeSharedPtr(T* p) : m_pUseCount(new int(1)), m_pNode(p)
		{
		}
		CNodeSharedPtr(const CNodeSharedPtr& p) : m_pUseCount(p.m_pUseCount), m_pNode(p.m_pNode)
		{
			m_pUseCount[0]++;
		}
		~CNodeSharedPtr()
		{
			reset();
		}
		void operator =(const CNodeSharedPtr& p)
		{
			if (m_pUseCount != p.m_pUseCount)
			{
				reset();
				m_pUseCount = p.m_pUseCount;
				m_pNode = p.m_pNode;
				m_pUseCount[0]++;
			}
		}
		void operator =(T* p)
		{
			if (p != m_pNode)
			{
				reset();
				m_pUseCount = new int(1);
				m_pNode = p;
			}
		}
		T* operator ->()
		{
			return m_pNode;
		}
		T* get()
		{
			return m_pNode;
		}
	};

	class CScriptNode;

	typedef yy::location CNodeLocation;
	typedef CNodeSharedPtr<CScriptNode> CScriptNodePtr;

	class CScriptNode
	{
	public:
		CScriptNodePtr	nodes[4];
		CNodeLocation	loc;

		CScriptNode* Init(const CNodeLocation& _loc, 
						  const CScriptNodePtr& n1, 
						  const CScriptNodePtr& n2,
						  const CScriptNodePtr& n3,
						  const CScriptNodePtr& n4)
		{
			loc = _loc;
			nodes[0] = n1;
			nodes[1] = n2;
			nodes[2] = n3;
			nodes[3] = n4;
			return this;
		}

		template <typename NODE>
		NODE* Node(int index)
		{
			return (NODE*)nodes[index].get();
		}
	};

	class CNode_Lexeme : public CScriptNode
	{
	private:
		std::string m_sData;

	public:
		CNode_Lexeme(const std::string& data) : m_sData(data)
		{
		}
		const std::string& GetData() const
		{
			return m_sData;
		}
	};

	template <typename NODE>
	CScriptNodePtr MakeNode(const CNodeLocation& loc, 
							const CScriptNodePtr& n1 = NULL, 
							const CScriptNodePtr& n2 = NULL, 
							const CScriptNodePtr& n3 = NULL, 
							const CScriptNodePtr& n4 = NULL)
	{
		CScriptNode* node = new NODE();
		return CScriptNodePtr(node->Init(loc, n1, n2, n3, n4));
	}

	template <typename NODE, class PARAM>
	CScriptNodePtr MakeNode(const CNodeLocation& loc, 
							const PARAM& data, 
							const CScriptNodePtr& n1 = NULL, 
							const CScriptNodePtr& n2 = NULL, 
							const CScriptNodePtr& n3 = NULL, 
							const CScriptNodePtr& n4 = NULL)
	{
		CScriptNode* node = new NODE(data);
		return CScriptNodePtr(node->Init(loc, n1, n2, n3, n4));
	}

	typedef CScriptNode		CNode_ConstDeclList;
	typedef CScriptNode		CNode_VarDeclList;
	typedef CScriptNode		CNode_ArrayRankList;
	typedef CScriptNode		CNode_IdDecl;*/

	typedef yy::location CNodeLocation;

	// Nodes

	class CScriptNode
	{
	public:
		CNodeLocation loc;

		CScriptNode(const CNodeLocation& _loc) : loc(_loc)
		{
		}
	};

	// Lists

	template <typename ITEM>
	class CNode_LinkedList : public CScriptNode
	{
	protected:
		ITEM item;
		CNode_LinkedList<ITEM>* pNext;

	public:
		CNode_LinkedList(const CNodeLocation& loc, const ITEM& _item, CScriptNode* next) 
			: CScriptNode(loc), item(_item), pNext((CNode_LinkedList<ITEM>*)next)
		{
		}
	};

	class CNode_Expression;
	typedef CNode_LinkedList<CNode_Expression*> CNode_Params;

	typedef std::pair<CScriptNode*,CScriptNode*> InvokePair;
	typedef CNode_LinkedList<InvokePair> CNode_InvokeSeq;

	// Expressions

	class CNode_Expression : public CScriptNode
	{
	public:
		CNode_Expression(const CNodeLocation& loc) : CScriptNode(loc)
		{
		}
		virtual CComVariant Calculate() = NULL;
	};

	class CNode_UnaryExpr : public CNode_Expression
	{
	public:
		typedef HRESULT (__stdcall *UNARYFN)(LPVARIANT, LPVARIANT);
		typedef UNARYFN PUNARYFN;

	private:
		PUNARYFN pUnaryFn;

	protected:
		CNode_Expression* pExpr;

		CNode_UnaryExpr(const CNodeLocation& loc, CScriptNode* p, PUNARYFN pFn) 
			: CNode_Expression(loc), pExpr((CNode_Expression*)p), pUnaryFn(pFn)
		{
		}

		virtual CComVariant Calculate();
	};

	class CNode_BinaryExpr : public CNode_Expression
	{
	protected:
		CNode_Expression* pLeftExpr;
		CNode_Expression* pRightExpr;

		CNode_BinaryExpr(const CNodeLocation& loc, CScriptNode* pl, CScriptNode* pr) 
			: CNode_Expression(loc), 
			pLeftExpr((CNode_Expression*)pl), pRightExpr((CNode_Expression*)pr)
		{
		}
	};

	class CNode_BinaryExprOp : public CNode_BinaryExpr
	{
	public:
		typedef HRESULT (__stdcall *BINARYFN)(LPVARIANT, LPVARIANT, LPVARIANT);
		typedef BINARYFN PBINARYFN;

	private:
		PBINARYFN pBinaryFn;

	protected:
		CNode_BinaryExprOp(const CNodeLocation& loc, 
			CScriptNode* pl, CScriptNode* pr, PBINARYFN pFn) 
			: CNode_BinaryExpr(loc, pl, pr), pBinaryFn(pFn)
		{
		}

		virtual CComVariant Calculate();
	};

	class CNode_ExprList : public CNode_BinaryExpr
	{
	public:
		CNode_ExprList(const CNodeLocation& loc, CScriptNode* pl, CScriptNode* pr) 
			: CNode_BinaryExpr(loc, pl, pr)
		{
		}
		virtual CComVariant Calculate()
		{
			pLeftExpr->Calculate();
			return pRightExpr->Calculate();
		}
	};

	// Unary expressions

	template <CNode_UnaryExpr::PUNARYFN FUNC>
	class CNode_UnaryExprTempl : public CNode_UnaryExpr
	{
	public:
		CNode_UnaryExprTempl(const CNodeLocation& loc, CScriptNode* p) 
			: CNode_UnaryExpr(loc, p, FUNC)
		{
		}
	};

	typedef CNode_UnaryExprTempl<&::VarNeg>		CNode_NegExpr;
	typedef CNode_UnaryExprTempl<&::VarNot>		CNode_NotExpr;

	// Binary expressions

	template <CNode_BinaryExprOp::PBINARYFN FUNC>
	class CNode_BinaryExprTempl : public CNode_BinaryExprOp
	{
	public:
		CNode_BinaryExprTempl(const CNodeLocation& loc, CScriptNode* pl, CScriptNode* pr) 
			: CNode_BinaryExprOp(loc, pl, pr, FUNC)
		{
		}
	};

	typedef CNode_BinaryExprTempl<&::VarAdd>	CNode_AddExpr;
	typedef CNode_BinaryExprTempl<&::VarAnd>	CNode_AndExpr;
	typedef CNode_BinaryExprTempl<&::VarCat>	CNode_CatExpr;
	typedef CNode_BinaryExprTempl<&::VarDiv>	CNode_DivExpr;
	typedef CNode_BinaryExprTempl<&::VarEqv>	CNode_EqvExpr;
	typedef CNode_BinaryExprTempl<&::VarIdiv>	CNode_IdivExpr;
	typedef CNode_BinaryExprTempl<&::VarImp>	CNode_ImpExpr;
	typedef CNode_BinaryExprTempl<&::VarMod>	CNode_ModExpr;
	typedef CNode_BinaryExprTempl<&::VarMul>	CNode_MulExpr;
	typedef CNode_BinaryExprTempl<&::VarOr>		CNode_OrExpr;
	typedef CNode_BinaryExprTempl<&::VarPow>	CNode_PowExpr;
	typedef CNode_BinaryExprTempl<&::VarSub>	CNode_SubExpr;
	typedef CNode_BinaryExprTempl<&::VarXor>	CNode_XorExpr;

	// Comparison expressions

	class CNode_CompareExpr : public CNode_BinaryExpr
	{
	private:
		int nRelation;
		bool bNegate;

	protected:
		CNode_CompareExpr(const CNodeLocation& loc, 
			CScriptNode* pl, CScriptNode* pr, int _nRelation, bool _bNegate) 
			: CNode_BinaryExpr(loc, pl, pr), nRelation(_nRelation), bNegate(_bNegate)
		{
		}

		virtual CComVariant Calculate();
	};

	template <int RELATION, bool NEGATE>
	class CNode_CompareExprTempl : public CNode_CompareExpr
	{
	public:
		CNode_CompareExprTempl(const CNodeLocation& loc, CScriptNode* pl, CScriptNode* pr) 
			: CNode_CompareExpr(loc, pl, pr, RELATION, NEGATE)
		{
		}
	};

	typedef CNode_CompareExprTempl<VARCMP_LT,true>	CNode_GreaterEqExpr;
	typedef CNode_CompareExprTempl<VARCMP_GT,true>	CNode_LessEqExpr;
	typedef CNode_CompareExprTempl<VARCMP_GT,false>	CNode_GreaterExpr;
	typedef CNode_CompareExprTempl<VARCMP_LT,false>	CNode_LessExpr;
	typedef CNode_CompareExprTempl<VARCMP_EQ,true>	CNode_NotEqExpr;
	typedef CNode_CompareExprTempl<VARCMP_EQ,false>	CNode_EqualExpr;

	class CNode_IsExpr : public CNode_BinaryExpr
	{
	public:
		CNode_IsExpr(const CNodeLocation& loc, CScriptNode* pl, CScriptNode* pr) 
			: CNode_BinaryExpr(loc, pl, pr)
		{
		}

		virtual CComVariant Calculate();
	};

	// Leaves

	class CLeaf : public CNode_Expression
	{
	public:
		CLeaf(const CNodeLocation& loc) : CNode_Expression(loc)
		{
		}
	};

	// Constants

	class CLeaf_Const : public CLeaf
	{
	protected:
		CComVariant m_Value;

	public:
		CLeaf_Const(const CNodeLocation& loc, const CComVariant& value = CComVariant()) 
			: CLeaf(loc), m_Value(value)
		{
		}
		virtual CComVariant Calculate()
		{
			return m_Value;
		}
	};

	template <int TYPE>
	class CLeaf_ConstTempl : public CLeaf_Const
	{
	public:
		CLeaf_ConstTempl(const CNodeLocation& loc, const char* text = "") 
			: CLeaf_Const(loc, CComVariant(text))
		{
			::VariantChangeTypeEx(&m_Value, &m_Value, MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), 0, TYPE);
		}
	};

	typedef CLeaf_ConstTempl<VT_R8>		CLeaf_FloatConst;
	typedef CLeaf_ConstTempl<VT_BSTR>	CLeaf_StringConst;
	typedef CLeaf_ConstTempl<VT_BOOL>	CLeaf_BoolConst;
	typedef CLeaf_ConstTempl<VT_EMPTY>	CLeaf_EmptyConst;
	typedef CLeaf_ConstTempl<VT_NULL>	CLeaf_NullConst;

	class CLeaf_DateConst : public CLeaf_Const
	{
	public:
		CLeaf_DateConst(const CNodeLocation& loc, const char* text) 
			: CLeaf_Const(loc, CComVariant(text + 1))
		{
			::VariantChangeTypeEx(&m_Value, &m_Value, MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), 0, VT_DATE);
		}
	};

	template <const char ScanfStr[]>
	class CLeaf_IntConstTempl : public CLeaf_Const
	{
	public:
		CLeaf_IntConstTempl(const CNodeLocation& loc, const char* text) 
			: CLeaf_Const(loc)
		{
			m_Value.vt = VT_I4;
			sscanf_s(text, ScanfStr, &m_Value.lVal);
		}
	};

	extern const char IntConstStr[];
	extern const char HexConstStr[];
	extern const char OctConstStr[];

	typedef CLeaf_IntConstTempl<IntConstStr>	CLeaf_IntConst;
	typedef CLeaf_IntConstTempl<HexConstStr>	CLeaf_HexConst;
	typedef CLeaf_IntConstTempl<OctConstStr>	CLeaf_OctConst;

	// Identifier

	class CLeaf_Ident : public CLeaf
	{
	private:
		std::string m_sText;

	public:
		CLeaf_Ident(const CNodeLocation& loc, const char* text)
			: CLeaf(loc), m_sText(text)
		{
		}
		virtual CComVariant Calculate();
	};

	// Make functions

	template <typename NODE>
	CLeaf* MkLeaf(const CNodeLocation& loc)
	{
		return new NODE(loc);
	}

	template <typename NODE, class DATA>
	CLeaf* MkLeaf(const CNodeLocation& loc, DATA data)
	{
		return new NODE(loc, data);
	}

	template <typename NODE>
	CScriptNode* MkNode(const CNodeLocation& loc, CScriptNode* n1)
	{
		return new NODE(loc, n1);
	}

	template <typename NODE, typename PARAM1, typename PARAM2>
	CScriptNode* MkNode(const CNodeLocation& loc, const PARAM1& p1, const PARAM2& p2)
	{
		return new NODE(loc, p1, p2);
	}
}
