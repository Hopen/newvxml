/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "CCXMLBase.h"
#include "EvtModel.h"
#include <list>
#include <map>
#include <mutex>
#include <vector>
#include <atlbase.h>

#include "InstanceController.h"

namespace CCXML
{
	class CNameTable;

	struct VAR : public InstanceController<VAR>
	{
		CComVariant	vValue;
		CCXMLString	sName;
		bool		bReadOnly;
	private:
		bool		bIsEmpty;
	public:

		VAR();
		VAR(const CCXMLString& name, const CComVariant& value, bool readonly);
		VAR(const VAR& rhs);

		VAR& operator=(VAR const& rhs);

		virtual ~VAR();
		//VAR()
		//{
		//	::VariantInit(&vValue);
		//}
		//void SetValue(VARIANT* pNewVal);
		void SetValue(const CComVariant& newVal);
		//void SetInitialValue(const CComVariant& newVal);

		bool IsEmpty()const { return bIsEmpty; }
	};



	class CScopedNameTable
	{
	private:
		//typedef CComPtr<CNameTable> CComTable;
		typedef std::pair<CCXMLString,SessionObject > ScopePair;
		typedef std::list<ScopePair> Scopes;
		//typedef std::vector<CNameTable* /*SessionObject*/> StackGlobalNames;
		typedef std::vector<SessionObject> StackGlobalNames;

		//CNameTable* /*SessionObject*/	m_pGlobalNames;
		//SessionObject	m_pGlobalNames;
		//CComTable	m_pGlobalNames;
		Scopes		m_Scopes;
		//StackGlobalNames m_gStack;
		mutable std::mutex m_mutex;

		int m_count;

	public:
		CScopedNameTable();
		~CScopedNameTable();
		void PushScope(const CCXMLString& sName);
		void PopScope();
		//void PushStack();
		//void PopStack();
		const VAR AddVar(const CCXMLString& sName, const CComVariant& val, bool bReadOnly);
		//void DelVar(const CCXMLString& sName);
		const VAR GetValue(const CCXMLString& sName);

		//CComVariant	GetGlobal()const;
	};
}