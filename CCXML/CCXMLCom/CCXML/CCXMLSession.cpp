/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "..\StdAfx.h"
#include <algorithm>
#include <future>
#include <tuple>
//#include <boost\archive\xml_wiarchive.hpp>
//#include <boost\archive\binary_iarchive.hpp>
#include "CCXMLSession.h"
#include "CCXMLEngine.h"
#include "CCXMLResCache.h"
#include "CCXMLDocument.h"
#include "CCXMLTags.h"
#include "Engine.h"
#include "sv_strutils.h"
#include "CCXMLDebug.h"
#include "CCXMLNameTable.h"
#include "..\Common\alarmmsg.h"
#include "common.h"
//#include <boost/asio/steady_timer.hpp>
//#include <boost/asio/spawn.hpp>
#include "Patterns/string_functions.h"
#include "eventdispatcher.h"
#include <boost/any.hpp>

static ObjectId s_nDialogCount = 1;

using namespace enginelog;

namespace CCXML
{
	void __DumpMessage(
		const CMessage& aMessage,
		const EngineLogPtr2& aLog)
	{
		try
		{
			aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump Message (%i) - Name: %s", aMessage.ParamsCount(), aMessage.GetName());
			for (const auto& param : aMessage.Params())
			{
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L">> %s", param.Dump().c_str());
			}
		}
		catch (std::exception& aException)
		{
			aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"__DumpMessage - Exception: %s", stow(aException.what()));
		}
		catch (...)
		{
			aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"__DumpMessage - Exception: Unknown");
		}
	}


	std::atomic<int> lock_counter = 0;

	//class CThreadWrapper
	//{
	//public:
	//	CThreadWrapper(boost::thread&& _t)
	//		: t(std::move(_t))
	//	{

	//	}
	//	~CThreadWrapper()
	//	{
	//		if (t.joinable())
	//		{
	//			t.join();
	//		}
	//	}

	//private:
	//	boost::thread t;
	//};

	template <class T>
	class RefLock
	{
	public:
		RefLock(T* aRef) :
			mRef(aRef)
		{
			mRef->AddRef();
		}
		~RefLock()
		{
			mRef->Release();
		}
	private:
		T* mRef;
	};

	template <class T>
	class FunctionWrapper
	{
	public:
		FunctionWrapper(T aFunction)
			: mFunction(aFunction)
		{
		}

		~FunctionWrapper()
		{
			mFunction();
		}

	private:
		T mFunction;
	};

	void AddPredefineToMsgInternal(const CMessage& aInitialMessage, const CCXMLString& aVXMLFolder, CMessage& outMsg)
	{
		if (const CParam* pPredefine = aInitialMessage.ParamByName(L"predefine"))
		{
			CCXMLString expr = pPredefine->AsWideStr();
			boost::replace_all(expr, "'", "");
			std::vector <CCXMLString> variables;
			boost::split(variables, expr, std::bind2nd(std::equal_to<wchar_t>(), ';'));

			CCXMLString predefine;
			for (auto& name : variables)
			{
				if (name.empty())
				{
					continue;
				}
				boost::trim(name);
				WCHAR szBuf[1024];
				PathCombineW(szBuf, aVXMLFolder.c_str(), name.c_str());
				predefine += szBuf;
				predefine += L";";
			}

			if (!predefine.empty())
			{
				predefine.erase(predefine.size() - 1); // erase last ';'
				outMsg[L"predefine"] = predefine.c_str();
			}
		}
	}

	static void AddNamelistToMsgInternal(EnginePtr aEngine, CMessage& outMsg)
	{
		if (const CParam* paramName = outMsg.ParamByName(L"namelist"))
		{
			std::vector <CCXMLString> variables;
			boost::split(variables, paramName->AsWideStr(), std::bind2nd(std::equal_to<wchar_t>(), ' '));

			for (const auto& name : variables)
			{
				if (name.empty())
				{
					continue;
				}
				outMsg[name.c_str()] = aEngine->ExprEvalToVariant(name);
			}
		}
	}

	static void CopyNamelist(const CMessage& aSource, CMessage& aDestin)
	{
		if (const CParam* param = aSource.ParamByName(L"namelist"))
		{
			std::vector <CCXMLString> variables;
			boost::split(variables, param->AsWideStr(), std::bind2nd(std::equal_to<wchar_t>(), ' '));

			for (const auto& name : variables)
			{
				if (name.empty())
				{
					continue;
				}
				aDestin[name.c_str()] = aSource[name.c_str()];
			}
		}
	}

	static SessionObject ExtractNameList(const CMessage& aInitialMsg)
	{
		SessionObject values = std::make_shared<CNameTable>(L"values");
		
		if (const CParam* paramName = aInitialMsg.ParamByName(L"namelist"))
		{
			std::vector <CCXMLString> variables;
			boost::split(variables, paramName->AsWideStr(), std::bind2nd(std::equal_to<wchar_t>(), ' '));
		
			for (const auto& name : variables)
			{
				if (name.empty())
				{
					continue;
				}
		
				if (aInitialMsg.ParamByName(name.c_str()))
				{
					values->Add(name, aInitialMsg[name.c_str()].Value, false);
				}
			}
		}

		return values;
	}

	class CSessionObjectImpl
	{
	public:
		CSessionObjectImpl() = default;

		CSessionObjectImpl(
			ObjectId aObjectId,
			ObjectId aScriptId,
			const CCXMLString& aState)
			: mObjectId(aObjectId)
			, mScriptId(aScriptId)
			, mState(aState)
		{}

		virtual  ~CSessionObjectImpl() = default;

		virtual CComVariant AsVariant() const = 0;
		virtual CMessage AsMessage() const = 0;

		void Dump(EngineLogPtr2 aLog) const
		{
			aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"ObjectId: %s", ObjectIdToWideStr(mObjectId));
		}

		void SetState(const CCXMLString& aState)
		{
			mState = aState;
		}

	protected:
		ObjectId mObjectId = 0;
		ObjectId mScriptId;
		CCXMLString mState;
	};

	class CConnection: public CSessionObjectImpl
	{
		friend class SessionsStore2;
	public:
		CConnection() = default;

		CConnection(
			ObjectId aObjectId,
			ObjectId aScriptId,
			const CCXMLString& aAnum,
			const CCXMLString& aBnum, 
			const CCXMLString& aSigInfo,
			const CCXMLString& aHost)
			: CSessionObjectImpl(aObjectId, aScriptId, L"undefined")
			, mLocal(aBnum)
			, mRemote(aAnum)
			, mSigInfo(aSigInfo)
			, mHost(aHost)
			, mConnectioId(Utils::toStr(aObjectId))
		{}

		~CConnection() = default;

		virtual CComVariant AsVariant() const override
		{
			SessionObject connection = std::make_shared<CNameTable>
				(L"Connection_" + std::to_wstring(static_cast<unsigned __int64>(mObjectId)));

			connection->Add(L"connectionid", mConnectioId.c_str(), false);
			connection->Add(L"info", mSigInfo.c_str(), false);
			connection->Add(L"local", mLocal.c_str(), false);
			connection->Add(L"remote", mRemote.c_str(), false);
			connection->Add(L"host", mHost.c_str(), false);
			connection->Add(L"scriptid", mScriptId, false);

			return connection->AsVariant();
		}

		virtual CMessage AsMessage() const override
		{
			CMessage msg((CCXMLString(L"connection.") + mState).c_str());
			msg[L"connectionid"] = mConnectioId.c_str();
			msg[L"info"] = mSigInfo.c_str();
			msg[L"A"] = mLocal.c_str();
			msg[L"B"] = mRemote.c_str();
			msg[L"ScriptID"] = mScriptId;

			msg[L"connection"] = AsVariant(); // ?? need this ???

			return msg;
		}

		CCXMLString GetHost() const { return mHost; }
		CCXMLString GetRemote() const { return mRemote; }
		CCXMLString GetLocal() const { return mLocal; }

	private:
		CCXMLString mLocal;
		CCXMLString mRemote;
		CCXMLString mSigInfo;
		CCXMLString mHost;
		CCXMLString mConnectioId;
	};

	class CDialog: public CSessionObjectImpl
	{
		friend class SessionsStore2;
	public:
		CDialog() = default;

		CDialog(ObjectId aDialogId,
			ObjectId aScriptId,
			ObjectId aParentScriptId,
			ObjectId aRecipientId,
			ObjectId aVoxId,
			const CCXMLString& aHost,
			bool aIsConference)
			: CSessionObjectImpl(aDialogId, aScriptId, L"undefined")
			, mParentScriptId(aParentScriptId)
			, mRecipientId(aRecipientId)
			, mVoxId(aVoxId)
			, mHost(aHost)
			, mIsConference(aIsConference)
		{}

		~CDialog() = default;

		virtual CComVariant AsVariant() const override
		{
			SessionObject dialog = std::make_shared<CNameTable>
				(L"Dialog_" + std::to_wstring(static_cast<unsigned __int64>(mObjectId)));

			dialog->Add(L"dialogid", mScriptId, false);
			dialog->Add(L"parentscriptid", mParentScriptId, false);
			dialog->Add(L"recipientid", mRecipientId, false);
			dialog->Add(L"voxid", mVoxId, false);
			dialog->Add(L"host", mHost.c_str(), false);
			dialog->Add(L"ScriptID", mScriptId, false);

			return dialog->AsVariant();
		}

		virtual CMessage AsMessage() const override
		{
			CMessage msg((CCXMLString(L"Dialog.") + mState).c_str());
			msg[L"DialogID"] = mObjectId;
			msg[L"ScriptID"] = mScriptId;
			msg[L"ParentScriptID"] = mParentScriptId;

			if (mIsConference)
			{
				msg[L"VoxID"] = mVoxId;
			}
			else
			{
				msg[L"CallID"] = mRecipientId;
			}

			msg[L"RecipientId"] = mRecipientId;
			msg[L"IsConference"] = mIsConference;

			msg[L"dialog"] = AsVariant(); // ?? need this ???

			return msg;
		}

		bool IsConference() const { return mIsConference; }
		ObjectId GetRecipientId() const { return mRecipientId; }
		ObjectId GetParentScriptId() const { return mParentScriptId; }
		ObjectId GetVoxId() const { return mVoxId; }

	private:
		ObjectId mParentScriptId;
		ObjectId mRecipientId;
		ObjectId mVoxId;
		CCXMLString mHost;
		bool mIsConference = false;
	};

	class CConference : public CSessionObjectImpl
	{
		friend class SessionsStore2;
	public:
		CConference() = default;

		CConference(
			ObjectId aDialogId,
			ObjectId aScriptId,
			ObjectId aVoxId)
			: CSessionObjectImpl(aDialogId, aScriptId, L"undefined")
			, mVoxId(aVoxId)
		{}

		~CConference() = default;

		virtual CComVariant AsVariant() const override
		{
			SessionObject conference = std::make_shared<CNameTable>
				(L"Conference_" + std::to_wstring(static_cast<unsigned __int64>(mObjectId)));


			conference->Add(L"state", mState.c_str(), false);
			conference->Add(L"scriptid", mScriptId, false);
			conference->Add(L"voxid", mVoxId, false);
			conference->Add(L"conferenceid", mObjectId, false);

			return conference->AsVariant();
		}

		virtual CMessage AsMessage() const override
		{
			CMessage msg((CCXMLString(L"Conference.") + mState).c_str());
			msg[L"State"] = mState;
			msg[L"ScriptID"] = mScriptId;
			msg[L"VoxID"] = mVoxId;
			msg[L"ConferenceId"] = mObjectId;

			msg[L"conference"] = AsVariant(); // ?? need this ???

			return msg;
		}

		ObjectId GetVoxId() const { return mVoxId; }

	private:
		ObjectId mVoxId;
	};

	class SessionsStore2
	{
		template<class TSessionObject>
		using ObjectContainer = std::map<ObjectId, TSessionObject>;

		template <class TSessionObject>
		void AddObjectImpl(
			const ObjectId& aObjectId,
			const TSessionObject& aObject,
			ObjectContainer<TSessionObject>& outContainer)
		{
			auto result = outContainer.emplace(aObjectId, aObject);
			if (result.second == false)
			{
				result.first->second = aObject;
			}
		}

		template <class TSessionObject>
		TSessionObject GetObjectImpl(
			const ObjectId& aObjectId,
			const ObjectContainer<TSessionObject>& aContainer) const
		{
			auto cit = aContainer.find(aObjectId);
			if (cit == aContainer.cend())
			{
				throw std::logic_error(std::string("Cannot find object with id: ") + ObjectIdToStr(aObjectId));
			}

			return cit->second;
		}

		template <class TSessionObject>
		const TSessionObject SetStateImpl(
			ObjectId aObjectId,
			const CCXMLString& aState,
			ObjectContainer<TSessionObject>& outContainer)
		{
			auto cit = outContainer.find(aObjectId);
			if (cit == outContainer.cend())
			{
				throw std::logic_error(std::string("Cannot find object with id: ") + ObjectIdToStr(aObjectId));
			}
			cit->second.SetState(aState);

			return cit->second;
		}
		
		static CConnection MakeConnection(const CMessage& aInitMsg)
		{
			CCXMLString callId = aInitMsg[L"CallID"].AsWideStr();
			CCXMLString aNum = aInitMsg.SafeReadParam(L"A", CMessage::String, L"").AsWideStr();
			CCXMLString bNum = aInitMsg.SafeReadParam(L"B", CMessage::String, L"").AsWideStr();
			CCXMLString host = aInitMsg.SafeReadParam(L"Host", CMessage::String, L"").AsWideStr();
			ObjectId scriptId = aInitMsg.SafeReadParam(L"ScriptID", CMessage::Int64, 0).AsInt64();
			CCXMLString sigInfo = aInitMsg.SafeReadParam(L"SigInfo", CMessage::String, L"").AsWideStr();

			return CConnection(
				Utils::toNum<ObjectId>(callId),
				scriptId,
				aNum,
				bNum,
				sigInfo,
				host);
		}

		static CConference MakeConference(const CMessage& aInitMsg)
		{
			ObjectId confId = aInitMsg[L"ConferenceID"].AsInt64();
			ObjectId voxId = aInitMsg.SafeReadParam(L"VoxID", CMessage::Int64, 0).AsInt64();
			ObjectId scriptId = aInitMsg.SafeReadParam(L"ScriptID", CMessage::Int64, 0).AsInt64();

			return CConference(confId,
				scriptId,
				voxId);
		}

		static CDialog MakeDialog(const CMessage& aInitMsg)
		{
			ObjectId scriptId = aInitMsg[L"ScriptID"].AsInt64();
			ObjectId dialogId = scriptId;

			ObjectId parentScriptId = aInitMsg.SafeReadParam(L"ParentScriptID", CMessage::Int64, 0).AsInt64();
			ObjectId conferenceId = aInitMsg.SafeReadParam(L"ConferenceID", CMessage::Int64, 0).AsInt64();
			ObjectId callId = aInitMsg.SafeReadParam(L"CallID", CMessage::Int64, 0).AsInt64();
			CCXMLString host = aInitMsg.SafeReadParam(L"Host", CMessage::String, L"").AsWideStr();

			ObjectId recipientId;
			bool isConference = false;
			if (conferenceId)
			{
				isConference = true;
				recipientId = conferenceId;
			}
			else if (callId)
			{
				recipientId = callId;
			}
			else
			{
				throw std::runtime_error(std::string("SessionStore: cannot find connection from dialog:" ) + ObjectIdToStr(dialogId));
			}

			return CDialog(dialogId,
				scriptId,
				parentScriptId,
				recipientId,
				isConference ? recipientId : 0,
				host,
				isConference);
		}

	public:
		void AddConnection(const ObjectId& aObjectId, const CMessage& aInitMsg)
		{
			LockGuard<std::mutex> lock(L"SessionsStore::AddConnection", mMutex);

			AddObjectImpl(aObjectId, MakeConnection(aInitMsg), mConnections);
		}

		void AddConference(const ObjectId& aObjectId, const CMessage& aInitMsg)
		{
			LockGuard<std::mutex> lock(L"SessionsStore::AddConference", mMutex);

			AddObjectImpl(aObjectId, MakeConference(aInitMsg), mConferences);
		}

		void AddDialog(const ObjectId& aObjectId, const CMessage& aInitMsg)
		{
			LockGuard<std::mutex> lock(L"SessionsStore::AddDialog", mMutex);

			AddObjectImpl(aObjectId, MakeDialog(aInitMsg), mDialogs);
		}

		const CConnection SetConnectionState(
			ObjectId aObjectId,
			const CCXMLString& aState)
		{
			LockGuard<std::mutex> lock(L"SessionsStore::SetConnectionState", mMutex);

			return SetStateImpl(aObjectId, aState, mConnections);
		}

		const CConference SetConferenceState(
			ObjectId aObjectId,
			const CCXMLString& aState)
		{
			LockGuard<std::mutex> lock(L"SessionsStore::SetConferenceState", mMutex);

			return SetStateImpl(aObjectId, aState, mConferences);
		}

		const CDialog SetDialogState(
			ObjectId aObjectId,
			const CCXMLString& aState)
		{
			LockGuard<std::mutex> lock(L"SessionsStore::SetDialogState", mMutex);

			return SetStateImpl(aObjectId, aState, mDialogs);
		}

		bool IsDialog(ObjectId aObjectId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::IsDialog", mMutex);

			return mDialogs.find(aObjectId) != mDialogs.cend();
		}

		bool IsConnection(ObjectId aObjectId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::IsConnection", mMutex);

			return mConnections.find(aObjectId) != mConnections.cend();
		}

		bool IsConference(ObjectId aObjectId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::IsConference", mMutex);

			return mConferences.find(aObjectId) != mConferences.cend();
		}

		CConnection GetConnection(ObjectId aObjectId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::GetConnection", mMutex);

			return GetObjectImpl(aObjectId, mConnections);
		}

		CConference GetConference(ObjectId aObjectId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::GetConference", mMutex);

			return GetObjectImpl(aObjectId, mConferences);
		}
		
		CDialog GetDialog(ObjectId aObjectId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::GetDialog", mMutex);

			return GetObjectImpl(aObjectId, mDialogs);
		}

		ObjectIdCollector KillDialogs(ObjectId aCallId, ObjectId aOriginScriptId) const
		{
			LockGuard<std::mutex> lock(L"SessionsStore::KillDialogs", mMutex);

			ObjectIdCollector result;

			for (const auto& dialogPair : mDialogs)
			{
				const CDialog& dialog = dialogPair.second;
				if (dialogPair.first == aOriginScriptId
					|| aCallId != dialog.mRecipientId)
				{
					continue;
				}
				result.emplace_back(dialogPair.first);
			}

			return result;
		}

	public:
		CMessage MakeCommonConnectionMessage(
			ObjectId aConnectionId, 
			const CCXMLString& aNewState)
		{
			return SetConnectionState(aConnectionId, aNewState).AsMessage();
		}

	private:
		ObjectContainer<CConnection> mConnections;
		ObjectContainer<CConference> mConferences;
		ObjectContainer<CDialog> mDialogs;

		mutable std::mutex mMutex;
	};

	/****************************************************************************************************/
	/*                                          SessionsStore                                           */
	/****************************************************************************************************/
//	class SessionsStore
//	{
//		class SessionObjectStore
//		{
//			using ObjectContainer = std::map<CCXMLString, NameTableDispatch>;
//		public:
//
//			SessionObjectStore() = default;
//
//			CComVariant GetVariant()
//			{
//				LockGuard<std::mutex> lock(L"SessionsStore::GetVariant", mMutex);
//
//				SessionObject allObjects = std::make_shared<CNameTable>(L"CCXML");
//
//				for (auto& objectPair : mObjects)
//				{
//					allObjects->Add(objectPair.first, objectPair.second.GetVariant(), true);
//				}
//
//				return allObjects->AsVariant();
//			}
//
//			void AddObject(const CCXMLString& aObjectId, const SessionObject& aObject)
//			{
//				LockGuard<std::mutex> lock(L"SessionsStore::AddObject", mMutex);
//				auto result = mObjects.emplace(std::piecewise_construct,
//					std::forward_as_tuple(aObjectId),
//					std::forward_as_tuple(aObject));
//
//				if (result.second == false)
//				{
//					throw std::runtime_error("Object already exist");
//				}
//			}
//
//			void Dump(EngineLogPtr2 aLog) const
//			{
//				LockGuard<std::mutex> lock(L"SessionsStore::Dump", mMutex);
//
//				for (const auto& objectPair : mObjects)
//				{
//					const auto& object = objectPair.second;
//					if (!object.HasValue())
//					{
//						aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionObjectStore::Dump nullptr");
//						throw std::runtime_error("SessionObjectStore::Dump nullptr");
//					}
//
//					__DumpMessage(object.ToMsg<CMessage>(aLog), aLog);
//				}
//			}
//
//			CCXMLString ToString(EngineLogPtr2 aLog)const
//			{
//				LockGuard<std::mutex> lock(L"SessionsStore::ToString2", mMutex);
//
//				CCXMLString result;
//
//				for (const auto& objectPair : mObjects)
//				{
//					const auto& object = objectPair.second;
//					if (!object.HasValue())
//					{
//						aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionObjectStore::Dump nullptr");
//						throw std::runtime_error("SessionObjectStore::Dump nullptr");
//					}
//
//					result += object.ToMsg<CMessage>(aLog).Dump();
//				}
//
//				return result;
//			}
//
//			template <class TPredicate>
//			void FindObjectById(const CCXMLString& aObjectId, TPredicate aFunction) const
//			{
//				LockGuard<std::mutex> lock(L"SessionsStore::FindObjectById", mMutex);
//
//				const auto cit = mObjects.find(aObjectId);
//				if (cit == mObjects.cend())
//				{
//					ObjectId objectId = Utils::toNum<ObjectId>(aObjectId);
//					throw std::runtime_error((boost::format("FindObjectById: object with id == \"%08X-%08X\" does not exist") % DWORD(objectId >> 32) % DWORD(objectId & 0xFFFFFFFF)).str());
//				}
//
//				aFunction(std::remove_const_t<NameTableDispatch&>(cit->second));
//			}
//
//			VAR FindAttrEvent(const CCXMLString& aObjectId, const CCXMLString& aAttrName) const
//			{
//				VAR result;
//				FindObjectById(aObjectId, [&result, aAttrName](const CComBridge<CNameTable>& aObject)
//				{
//					result = aObject.GetSafeValue<VAR>(aAttrName);
//				});
//
//				return result;
//			}
//
//			CComVariant FindObject(const CCXMLString & aObjectId) const
//			{
//				LockGuard<std::mutex> lock(L"SessionsStore::FindObject", mMutex);
//
//				auto it = mObjects.find(aObjectId);
//				if (it == mObjects.end())
//				{
//					ObjectId objectId = Utils::toNum<ObjectId>(aObjectId);
//					return CComVariant();
//				}
//
//				NameTableDispatch& object = std::remove_const_t<NameTableDispatch&>(it->second);
//
//				return object.GetVariant();
//			}
//
//			template <class TPredicate>
//			void Process(TPredicate aFunction)
//			{
//				//std::lock_guard<std::mutex> lock(mMutex);
//				LockGuard<std::mutex> lock(L"SessionsStore::Process", mMutex);
//				//mObjects.ForEach(aFunction);
//
//				for (const auto& objectPair : mObjects)
//				{
//					aFunction(objectPair.second);
//				}
//			}
//
//		private:
//
//			ObjectContainer mObjects;
//
//			mutable std::mutex mMutex;
//		};
//
//		
//
//	public:
//
//		SessionsStore(EngineLogPtr2 aLog)
//			: mLog(aLog)
//		{
//		}
//
//		CMessage MakeCommonConnectionMessage(const CCXMLString& aConnectionId, const CCXMLString& aNewState, const CMessage& aInitialMsg)
//		{
//
//			/*
//			07:06:37.122 P1BA4 T27C0 S0000D246-028E58BD FT IN Name: OFFERED; RunRequestID = 0x0000D23B-00003C15; InitialMessage = "OFFERED"; MonitorDisplayedName = "777_A
//"; Host = "KD-IVR811"; B = "777"; Board = "DTI4"; FileName = "C:\IS3\scripts\vxmlccxml\ccxml\777.ccxml"; A = "9618182321"; SAM0 = "777"; TimeSlot = 0x00000000
//0000005; CallID = 0x0000D241-0488DABE; SigInfo = "0x0601000702600109010A0201000404019077F70A07031369818132120801801D038090A33102005A390631D03AD03FC03A06420700
//00FF3F088493979666609909"...; SourceAddress = 0x0000D23B-00000000; ScriptID = 0x0000D246-028E58BD; RMQ_FileMap = 0x00000000-000001A8; RMQ_Event = 0x00000000-0
//001AC; RMQ_Mutex = 0x00000000-000001B0; WMQ_FileMap = 0x00000000-000001B4; WMQ_Event = 0x00000000-000001B8; WMQ_Mutex = 0x00000000-000001BC; SurrogateNumber =
//2
//			*/
//			mLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionsStore::MakeCommonConnectionMessage: %s, aNewState = %s", aConnectionId.c_str(), aNewState.c_str());
//			CMessage commonConnectionMsg((CCXMLString(L"connection.") + aNewState).c_str());
//			commonConnectionMsg[L"connectionid"] = aConnectionId;
//			commonConnectionMsg[L"info"] = aInitialMsg.SafeReadParam(L"SigInfo", CMessage::CheckedType::String, L"").AsWideStr();
//			if (const CParam* param = aInitialMsg.ParamByName(L"A"))
//			{
//				commonConnectionMsg[L"A"] = param->AsWideStr();
//			}
//
//			if (const CParam* param = aInitialMsg.ParamByName(L"B"))
//			{
//				commonConnectionMsg[L"B"] = param->AsWideStr();
//			}
//
//			if (const CParam* param = aInitialMsg.ParamByName(L"ScriptID"))
//			{
//				commonConnectionMsg[L"ScriptID"] = aInitialMsg[L"ScriptID"];
//			}
//			mLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"6");
//
//			mLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionsStore::MakeCommonConnectionMessage - commonConnectionMsg");
//			__DumpMessage(commonConnectionMsg, mLog);
//
//			mLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionsStore::MakeCommonConnectionMessage::FindObjectById: %s", aConnectionId.c_str());
//			mConnections.FindObjectById(aConnectionId, [&commonConnectionMsg, &aNewState, this](NameTableDispatch& aConnection)
//			{
//				CComVariant vState;
//				vState.vt = VT_BSTR;
//				vState.bstrVal = _bstr_t(aNewState.c_str());
//
//				aConnection.AddValue<VAR>(L"state", vState, false);
//
//				this->mLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionsStore::MakeCommonConnectionMessage::FindObjectById: add state: %s", aNewState.c_str());
//				aConnection.ToMsg<CMessage>(this->mLog);
//
//				commonConnectionMsg[L"connection"] = aConnection.GetVariant();
//			});
//
//			mLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"SessionsStore::MakeCommonConnectionMessage - commonConnectionMsg");
//			__DumpMessage(commonConnectionMsg, mLog);
//
//			return commonConnectionMsg;
//		}
//
//		CMessage MakeCommonConferenceMessage(const CCXMLString& aConferenceId, const CCXMLString& aNewState, const CMessage& aInitialMsg)
//		{
//			CMessage commonConferenceMsg((CCXMLString(L"conference.") + aNewState).c_str());
//			commonConferenceMsg[L"connectionid"] = aInitialMsg[L"CallbackID"].AsWideStr();
//			commonConferenceMsg[L"ReasonDescription"] = aInitialMsg[L"ReasonDescription"].Value;
//			commonConferenceMsg[L"CallbackID"] = aInitialMsg[L"CallbackID"].Value;
//
//			mConferences.FindObjectById(aConferenceId, [&commonConferenceMsg, &aNewState](NameTableDispatch& aConference)
//			{
//				CComVariant vState;
//				vState.vt = VT_BSTR;
//				vState.bstrVal = _bstr_t(aNewState.c_str());
//
//				aConference.AddValue<VAR>(L"state", vState, false);
//				commonConferenceMsg[L"conference"] = aConference.GetVariant();
//			});
//
//			return commonConferenceMsg;
//		}
//
//		CMessage MakeCommonDialogMessage(
//			const CCXMLString& aDialogId, 
//			const CCXMLString& aRecipientId,
//			const CCXMLString& aNewState, 
//			const CMessage& aInitialMsg)
//		{
//			ObjectId recipientId = Utils::toNum<ObjectId>(aRecipientId);
//
//			CMessage commonDialogMsg(CCXMLString(L"dialog." + aNewState).c_str());
//
//			commonDialogMsg[L"CallbackID"] = aInitialMsg[L"CallbackID"].Value;
//			commonDialogMsg[L"ScriptID"] = aInitialMsg[L"ScriptID"].Value;
//			commonDialogMsg[L"dialogid"] = aInitialMsg[L"ScriptID"].Value; // for <dialogprepare>
//			commonDialogMsg[L"eventsourcetype"] = L"ccxml";
//			commonDialogMsg[L"connectionid"] = aRecipientId;
//
//			CComVariant connection = FindConnection(aRecipientId);
//			if (connection.vt == VT_EMPTY)
//			{
//				connection = FindConference(aRecipientId);
//				if (connection.vt == VT_EMPTY)
//				{
//					throw std::runtime_error((boost::format("SessionStore: cannot find connection by: \"%08X-%08X\"") % DWORD(recipientId >> 32) % DWORD(recipientId & 0xFFFFFFFF)).str());
//				}
//			}
//
//			mDialogs.FindObjectById(aDialogId, 
//				[&aInitialMsg, 
//				&aRecipientId,
//				&connection, 
//				&aNewState, 
//				&commonDialogMsg]
//			(NameTableDispatch& aDialog)
//			{
//				CComVariant vState;
//				vState.vt = VT_BSTR;
//				vState.bstrVal = _bstr_t(aNewState.c_str());
//
//				aDialog.AddValue<VAR>(L"state", vState, false);
//			});
//
//			SessionObject values = std::make_shared<CNameTable>(L"values");
//
//			if (const CParam* paramName = aInitialMsg.ParamByName(L"namelist"))
//			{
//				std::vector <CCXMLString> variables;
//				boost::split(variables, paramName->AsWideStr(), std::bind2nd(std::equal_to<wchar_t>(), ' '));
//
//				for (const auto& name : variables)
//				{
//					if (name.empty())
//					{
//						continue;
//					}
//
//					if (aInitialMsg.ParamByName(name.c_str()))
//					{
//						values->Add(name, aInitialMsg[name.c_str()].Value, false);
//					}
//				}
//			}
//
//			commonDialogMsg[L"connection"] = connection;
//			commonDialogMsg[L"values"] = values->AsVariant();
//
//			return commonDialogMsg;
//		}
//
//		void AddConnection(const CMessage& aInitialMsg, const CCXMLString& sOrig)
//		{
//			SessionObject pConn = std::make_shared<CNameTable>(CCXMLString(L"connection_") + aInitialMsg.GetName());
//
//			CCXMLString callId = aInitialMsg[L"CallID"].AsWideStr();
//
//			CComVariant vCallID;
//			vCallID.vt = VT_BSTR;
//			vCallID.bstrVal = _bstr_t(callId.c_str());
//			pConn->Add(L"connectionid", vCallID, true);
//			pConn->Add(L"state", L"undefined", true);
//			pConn->Add(L"endpoint", L"undefined", true);
//			pConn->Add(L"duplexmode", L"undefined", true);
//
//			if (const CParam* param = aInitialMsg.ParamByName(L"B"))
//			{
//				pConn->Add(L"local", aInitialMsg[L"B"].Value, true);
//			}
//			if (const CParam* param = aInitialMsg.ParamByName(L"A"))
//			{
//				pConn->Add(L"remote", aInitialMsg[L"A"].Value, true);
//			}
//
//			pConn->Add(L"originator", sOrig.c_str(), true);
//			if (const CParam * param = aInitialMsg.ParamByName(L"Host"))
//			{
//				pConn->Add(L"host", aInitialMsg[L"Host"].Value, true);
//			}
//
//			mConnections.AddObject(callId, pConn);
//		}
//
//		void AddConference(const CMessage& aInitialMsg)
//		{
//			SessionObject pConf = std::make_shared<CNameTable>(CCXMLString(L"conference_") + aInitialMsg.GetName());
//			CCXMLString confId = aInitialMsg[L"ConferenceID"].AsWideStr();
//
//			CComVariant vConfID;
//			vConfID.vt = VT_BSTR;
//			vConfID.bstrVal = _bstr_t(confId.c_str());
//
//			pConf->Add(L"state", L"undefined", true);
//			pConf->Add(L"voxid", aInitialMsg[L"VoxID"].AsInt64(), true);
//
//			pConf->Add(L"conferenceid", vConfID, true);
//			mConferences.AddObject(confId, pConf);
//		}
//
//		void AddDialog(const CMessage& aInitialMsg)
//		{
//			CCXMLString dialogId = aInitialMsg[L"ScriptID"].AsWideStr();
//
//			SessionObject pDialog = std::make_shared<CNameTable>(CCXMLString(L"dialog_") + dialogId);
//
//			pDialog->Add(L"state", L"undefined", true);
//			pDialog->Add(L"scriptid", dialogId.c_str(), true);
//			
//			if (const CParam* param = aInitialMsg.ParamByName(L"ParentScriptID"))
//			{
//				pDialog->Add(L"parentscriptid", aInitialMsg[L"ParentScriptID"].Value, true);
//			}
//
//			CCXMLString recipientId;
//			bool isConference = false;
//			if (const CParam* param = aInitialMsg.ParamByName(L"ConferenceID"))
//			{
//				isConference = true;
//				recipientId = param->AsWideStr();
//			}
//			else if (const CParam* param = aInitialMsg.ParamByName(L"CallID"))
//			{
//				recipientId = param->AsWideStr();
//			}
//
//			if (recipientId.empty())
//			{
//				ObjectId errorDialogId = Utils::toNum<ObjectId>(dialogId);
//				throw std::runtime_error((boost::format("SessionStore: cannot find connection from dialog: \"%08X-%08X\"") % DWORD(errorDialogId >> 32) % DWORD(errorDialogId & 0xFFFFFFFF)).str());
//			}
//
//			CComVariant vRecipientID;
//			vRecipientID.vt = VT_BSTR;
//			vRecipientID.bstrVal = _bstr_t(recipientId.c_str());
//
//			pDialog->Add(L"recipientid", vRecipientID, true);
//
//			if (isConference)
//			{
//				if (const CParam* param = aInitialMsg.ParamByName(L"ConferenceID"))
//				{
//					pDialog->Add(L"voxid", param->AsInt64(), true);
//				}
//			}
//			else
//			{
//				if (const CParam* param = aInitialMsg.ParamByName(L"Host"))
//				{
//					pDialog->Add(L"host", param->AsWideStr().c_str(), true);
//				}
//			}
//
//			mDialogs.AddObject(dialogId, pDialog);
//		}
//
//		CComVariant GetAllConnections()
//		{
//			return mConnections.GetVariant();
//		}
//
//		CComVariant GetAllConferences()
//		{
//			return mConferences.GetVariant();
//		}
//
//		CComVariant GetAllDialogs()
//		{
//			return mDialogs.GetVariant();
//		}
//
//		CComVariant FindConnection(const CCXMLString & aAttrName) const
//		{
//			return mConnections.FindObject(aAttrName);
//		}
//
//		CComVariant FindConference(const CCXMLString & aAttrName) const
//		{
//			return mConferences.FindObject(aAttrName);
//		}
//
//		CComVariant FindDialog(const CCXMLString & aDialogId) const
//		{
//			return mDialogs.FindObject(aDialogId);
//		}
//
//		bool IsConnection(const CCXMLString & aConnId) const
//		{
//			return mConnections.FindObject(aConnId).vt != VT_EMPTY;
//		}
//
//		bool IsConference(const CCXMLString & aConfId) const
//		{
//			return mConferences.FindObject(aConfId).vt != VT_EMPTY;
//		}
//
//		bool IsDialog(const CCXMLString & aDialogId) const
//		{
//			return mDialogs.FindObject(aDialogId).vt != VT_EMPTY;
//		}
//
//		VAR FindConnectionAttrEvent(const CCXMLString& aConnectionId, const CCXMLString& aAttrName) const
//		{
//			return mConnections.FindAttrEvent(aConnectionId, aAttrName);
//		}
//
//		VAR FindConferenceAttrEvent(const CCXMLString& aConferenceId, const CCXMLString & aAttrName) const
//		{
//			return mConferences.FindAttrEvent(aConferenceId, aAttrName);
//		}
//
//		VAR FindDialogAttrEvent(const CCXMLString& aDialogId, const CCXMLString & aAttrName) const
//		{
//			return mDialogs.FindAttrEvent(aDialogId, aAttrName);
//		}
//
//		ObjectIdCollector KillDialogs(const CMessage& aInitialMsg)
//		{
//			ObjectIdCollector result;
//
//			CCXMLString originCallId = aInitialMsg.ParamByName(L"CallID")->AsWideStr();
//			CCXMLString originScriptId = aInitialMsg.ParamByName(L"ScriptID")->AsWideStr();
//
//			mDialogs.Process([originScriptId, originCallId, &result]
//				(const NameTableDispatch& aBridge)
//			{
//				const VAR scriptId = aBridge.GetSafeValue<VAR>(L"scriptid");
//				if (scriptId.IsEmpty())
//				{
//					return;
//				}
//
//				CCXMLString scripDialogID = scriptId.vValue.bstrVal;
//				if (scripDialogID == originScriptId)
//				{
//					return; // skip dialog, which has send DISCONNECTED message
//				}
//
//				const VAR callId = aBridge.GetSafeValue<VAR>(L"recipientid");
//				if (callId.IsEmpty())
//				{
//					return;
//				}
//
//				CCXMLString scriptCallID = callId.vValue.bstrVal;
//				if (scriptCallID != originCallId)
//				{
//					return;
//				}
//
//				result.emplace_back(scripDialogID);
//			});
//
//			return result;
//		}
//
//		void DumpConnections(EngineLogPtr2 aLog) const
//		{
//			mConnections.Dump(aLog);
//		}
//
//		void DumpConferences(EngineLogPtr2 aLog) const
//		{
//			mConnections.Dump(aLog);
//		}
//
//		void DumpDialogs(EngineLogPtr2 aLog) const
//		{
//			mDialogs.Dump(aLog);
//		}
//
//		CMessage MakeRecipientMsg(const CCXMLString& aDialogId, const CMessage& aInitialMsg) const
//		{
//			CCXMLString recipientId;
//			CCXMLString host, voxId;
//			CMessage recipientMsg(aInitialMsg);
//
//			mDialogs.FindObjectById(aDialogId, [&recipientId, &host, &voxId](NameTableDispatch& aDialog)
//			{
//				auto value = aDialog.GetSafeValue<VAR>(L"recipientid");
//				if (!value.IsEmpty())
//				{
//					recipientId = value.vValue.bstrVal;
//				}
//				value = aDialog.GetSafeValue<VAR>(L"host");
//				if (!value.IsEmpty())
//				{
//					host = value.vValue.bstrVal;
//				}
//
//				value = aDialog.GetSafeValue<VAR>(L"voxid");
//				if (!value.IsEmpty())
//				{
//					voxId = value.vValue.bstrVal;
//				}
//			});
//
//			if (FindConference(recipientId).vt != VT_EMPTY)
//			{
//				recipientMsg[L"VoxID"] = voxId;
//				recipientMsg[L"ConferenceID"] = recipientId;
//			}
//			else
//			{
//				recipientMsg[L"Host"] = host;
//				recipientMsg[L"CallID"] = recipientId;
//			}
//
//			return recipientMsg;
//		}	
//
//	private:
//		SessionObjectStore mConnections;
//		SessionObjectStore mConferences;
//		SessionObjectStore mDialogs;
//		EngineLogPtr2 mLog;
//	};

	/****************************************************************************************************/
	/*                                          CStateDispatcher                                        */
	/****************************************************************************************************/

	class CStateDispatcher
	{
	public:
		CStateDispatcher(DWORD aSurrThread,
			bool aLogGetStep,
			EngineLogPtr2 aLog)
		{
			m_surrThread = aSurrThread;
			m_logGetStep = aLogGetStep;
			m_log = aLog;
		}

		~CStateDispatcher()
		{
			m_log.reset();
		}

		void ChangeState(DWORD dwState)
		{
			SetState(dwState);
		}

		DWORD GetState() const
		{
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex> lock(L"CStateDispatcher::GetState", m_mutex);
			return m_state;
		}
		void SetState(DWORD dwState)
		{
			if (GetState() == dwState) // has already set
				return;

			if (m_logGetStep)
			{
				LogState();
			}

			if (true)
			{
				if (m_surrThread == 0)
				{
					throw std::runtime_error("CStateDispatcher: m_surrThread uninitialized");
				}
				//std::lock_guard<std::mutex> lock(m_mutex);
				LockGuard<std::mutex> lock(L"CStateDispatcher::SetState", m_mutex);
				m_state = dwState;
				//::InterlockedExchange((LONG*)&m_state, dwState);
				::PostThreadMessageW(m_surrThread, WM_USER, 0, 0);
			}
		}
	private:
		void LogState() const
		{
			switch (m_state)
			{
			case VMS_BUSY:
				m_log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_BUSY");
				break;
			case VMS_READY:
				m_log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_READY");
				break;
			case VMS_WAITING:
				m_log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_WAITING");
				break;
			case VMS_ENDSCRIPT:
				m_log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_ENDSCRIPT");
				break;
			}
		}
	private:
		mutable std::mutex m_mutex;
		DWORD m_surrThread = 0;
		DWORD m_state = VMS_WAITING;
		bool m_logGetStep = false;
		EngineLogPtr2 m_log;
	};

	///****************************************************************************************************/
	///*                                          CEventDispatcher                                        */
	///****************************************************************************************************/

	//class CEventDispatcher
	//{
	//public:
	//	void PushFront(EventObject _event)
	//	{
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::PushFront", m_mutex);
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		m_EvtQ.push_front(_event);
	//	}

	//	void PushBack(EventObject _event)
	//	{
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::PushBack", m_mutex);
	//		m_EvtQ.push_back(_event);
	//	}

	//	EventObject GetEvent(const CCXMLString& aName, const ObjectId aCallBackId)
	//	{
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::GetEvent", m_mutex);

	//		EventObject evt;
	//		if (m_EvtQ.empty())
	//		{
	//			return evt;
	//		}

	//		const auto citEvent = std::find_if(m_EvtQ.cbegin(), m_EvtQ.cend(), 
	//			[aName, aCallBackId](const EventObject& _event)
	//		{
	//			if (_event->GetName() != aName)
	//			{
	//				return false;
	//			}

	//			auto callback = _event->GetSafeValue(L"CallbackID");
	//			if (!callback.IsEmpty() && callback.vValue.llVal == aCallBackId)
	//			{
	//				return true;
	//			}
	//			return false;
	//		});
	//		
	//		if (citEvent != m_EvtQ.cend())
	//		{
	//			evt = *citEvent;
	//			m_EvtQ.erase(citEvent);
	//		}

	//		return evt;
	//	}

	//	EventObject GetFirstEvent()
	//	{
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::GetFirstEvent", m_mutex);

	//		EventObject evt;
	//		if (m_EvtQ.empty())
	//		{
	//			return evt;
	//		}

	//		evt = *(m_EvtQ.begin());
	//		m_EvtQ.pop_front();

	//		return evt;
	//	}

	//	void DeleteEvent(EventObject aEvent)
	//	{
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::DeleteEvent", m_mutex);

	//	}

	//	size_t GetQueueSize() const
	//	{
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::GetQueueSize", m_mutex);
	//		return m_EvtQ.size();
	//	}

	//	const CComVariant FindAttrEvent(const CCXMLString& aAttrName) const
	//	{
	//		CComVariant result;
	//		//std::lock_guard<std::mutex> lock(m_mutex);
	//		LockGuard<std::mutex> lock(L"CEventDispatcher::FindAttrEvent", m_mutex);
	//		for (const auto& evt : m_EvtQ)
	//		{
	//			const VAR value = evt->GetValue(aAttrName);
	//			if (!value.IsEmpty())
	//			{
	//				result = value.vValue;
	//				break;
	//			}
	//		}
	//		return result;
	//	}

	//private:
	//	using EventQueue = std::list<EventObject>;
	//	EventQueue m_EvtQ;
	//	mutable std::mutex m_mutex;
	//};

	/****************************************************************************************************/
	/*                                            CSession                                              */
	/****************************************************************************************************/

	CSession::CSession():
		m_pTagEvtPrc(nullptr),
		m_nStepCounter(0),
		m_Initialized(false)
	{
		m_bThreadStarted = false;
		m_bSessionActive = true;
	}

	bool CSession::Initialize(const SessionCreateParams& params)
	{
		m_Log = params.pLog;
		m_extraLog = params.pExtraLog;

		m_InitMsg = CMessage(params.pInitMsg);
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize");

		m_sCCXMLFolder = params.sCCXMLFolder;
		m_sVXMLFolder = params.sVXMLFolder;
		m_sAppLogUrl = params.pVBSLogUrl;

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize StateDispatcher");
		m_StateDispatcher = std::make_unique<CStateDispatcher>(params.dwSurrThread, false, m_Log);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize EventDispatcher");
		m_EventDispatcher = std::make_unique<common::CEventDispatcher<EventObject, LockGuardBase>>();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize SessionStore");
		mSessionStore = std::make_unique<SessionsStore2>();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: check ScriptID");
		m_InitMsg.CheckParam(L"ScriptID", CMessage::Int64, CMessage::Mandatory);
		if (CParam* param = m_InitMsg.ParamByName(L"FileName"))
		{
		}
		else if (CParam* param = m_InitMsg.ParamByName(L"FileText"))
		{
		}
		else
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Cannot find name or text executing ccxml file in initial message");
			return false;
		}

		m_sesId = (ObjectId)m_InitMsg[L"ScriptID"].AsInt64();

		m_ExecContext.pSes = /*this*/params.pSession;
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: new CScopedNameTable");
		m_ExecContext.pVar = params.pVar;
		m_ExecContext.pCEvt = params.pEvtPrc;
		m_ExecContext.pDbg = params.pDebuger;

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: debuger init");
		DebugerPtr2 pDbg = m_ExecContext.pDbg.lock();
		pDbg->Init(m_InitMsg, params.pSession);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: CreateSessionVariables");
		CreateSessionVariables();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: PushScope application");
		m_ExecContext.pVar->PushScope(L"application");

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Create m_ThreadEvt");
		m_ThreadEvt.Create();
		StartThread();
		m_ThreadEvt.Wait();

		m_Initialized = true;
		return true;
	}

	CSession::~CSession()
	{
		//this->m_pSessionVariables.reset();
		//this->m_pCCXML.reset();
		//this->m_pIOProcessors.reset();
		//this->m_pDialogs.reset();
		//this->m_pConferences.reset();
		//this->m_pConferences.reset();

		//m_ExecContext.pDbg.reset();

		m_ExecContext.pVar.reset();
		JoinThread();

		mSessionStore.reset();
		//mResourceCache.reset();

		m_Log.reset();
		m_extraLog.reset();
	}

	class CScriptEngineLock
	{
	public:
		CScriptEngineLock(CExecContext* aExecContext,
			SessionPtr2 aSes,
			EngineLogPtr2 aLog)
			: mExecContext(aExecContext)
		{
			if (mExecContext)
			{
				mExecContext->pEng.reset(new DEBUG_NEW_PLACEMENT CScriptEngine(aSes, aLog));
			}
		}

		~CScriptEngineLock()
		{
			if (mExecContext)
			{
				mExecContext->pEng->Clear();
				mExecContext->pEng.reset();
			}
		}

	private:
		CExecContext* mExecContext = nullptr;
	};

	void CSession::startThread()
	{

		auto appLog = std::make_shared<CAppLog>(m_Log->GetLevel(), m_sAppLogUrl);

		CScriptEngineLock lock(&m_ExecContext, m_ExecContext.pSes.lock(), m_Log);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Worker thread started");

		CMessage errorMsg;
		bool isException = false;
		try
		{
			m_ExecContext.pEng->AddObject(L"session", m_pSessionVariables->AsVariant());

			m_ExecContext.pEng->AddObject2(L"appLoger", appLog->AsVariant());
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"VBEngine started");
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: VBEngine creation failed");
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: VBEngine creation failed");
			errorMsg = ExceptionHandler(L"unknown");
			isException = true;
		}

		if (isException)
		{
			try
			{
				DoError(errorMsg, L"SESSION");
			}
			catch (std::exception& error)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
				m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
			catch (...)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: unhandled");
				m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: unhandled");
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
		}

		//Log(LEVEL_FINEST, __FUNCTIONW__, L"Create CResourceCache...");
		//try
		//{
		//	mResourceCache = std::make_unique<CResourceCache>();
		//}
		//catch (...)
		//{
		//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
		//	m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
		//}

		//if (!mResourceCache)
		//{
		//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");
		//	m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");

		//	DoExit();
		//	SetState(VMS_ENDSCRIPT);
		//}

		//if (!mResourceCache->Ready())
		//{
		//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", mResourceCache->GetHR());
		//	m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", mResourceCache->GetHR());
		//	DoExit();
		//	SetState(VMS_ENDSCRIPT);
		//}

		ChangeState(VMS_BUSY);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Try to load documents");

		if (CParam* param = m_InitMsg.ParamByName(L"FileName"))
		{
			if (!LoadDocument(m_InitMsg[L"FileName"].AsWideStr(), true))
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: Failed to load document");
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"DoExit()");
				DoExit();
			}
		}
		else if (CParam* param = m_InitMsg.ParamByName(L"FileText"))
		{
			if (!LoadDocument(m_InitMsg[L"FileText"].AsWideStr(), false))
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: Failed to load document");
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"DoExit()");
				DoExit();
			}
		}

		m_ThreadEvt.Set();
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Worker thread started");

		ChangeState(VMS_READY);

		for (;;)
		{
			//eThreadMsg& msg = m_queue.front();
			eThreadMsg msg = m_queue.pop_front();
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"THREAD MSG RECEIVED: %i", msg);
			ChangeState(VMS_WAITING);
			//m_queue.pop();

			bool isException = false;
			CCXMLString reason = L"Unknown";
			try
			{
				OnThreadMsg(msg, NULL);
			}
			catch (std::exception& e)
			{
				isException = true;
				reason = stow(e.what());
			}
			catch (_com_error& e)
			{
				isException = true;
				reason = (LPCWSTR)e.Description();
			}
			catch (std::wstring& e)
			{
				isException = true;
				reason = e;
			}
			catch (...)
			{
				isException = true;
			}

			if (isException)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while processing messages: %s", reason.c_str());
				m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while processing messages: %s", reason.c_str());
				try
				{
					auto errorMsg = ExceptionHandler(reason);
					DoError(errorMsg, L"SESSION");
				}
				catch (std::exception& error)
				{
					m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
					m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
					SetState(VMS_ENDSCRIPT);
					break;
				}
				catch (...)
				{
					m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: unhandled");
					m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: unhandled");
					SetState(VMS_ENDSCRIPT);
					break;
				}

			}

			ChangeState(VMS_READY);

			if (msg == tmQuit)
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"On tmQuit: \"%i\" processing messages denied", m_EventDispatcher->GetQueueSize());
				break;
			}
		}

		ChangeState(VMS_ENDSCRIPT);
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Worker thread exited");

		appLog.reset();

	}

	void CSession::ThreadProc()
	{
		::CoInitializeEx(NULL, COINIT_MULTITHREADED);
		try
		{
			m_bThreadStarted = true;

			auto threadSaver = [this]() {m_bThreadStarted = false; };
			FunctionWrapper<decltype(threadSaver)> threadSaverWrapper(threadSaver);

			startThread();

		}
		catch (const std::exception& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"ThreadProc exception: \"%s\"", stow(e.what()).c_str());
		}
		catch (_com_error& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"ThreadProc exception: \"%s\"", (LPCWSTR)e.Description());
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"ThreadProc unhandled exception caught");
		}

		::CoUninitialize();

	}

	ObjectId CSession::GetId() const
	{
		return m_sesId;
	}

	void CSession::SetEventProcessor(CTag_eventprocessor* pEvtPrc)
	{
		m_pTagEvtPrc = pEvtPrc;
		//CMessage msg_loaded(L"CCXMLDocumentLoaded");
		//msg_loaded[L"CallID"] = m_InitMsg[L"CallID"].Value;
		//MakeEvent_CCCXMLLoaded(msg_loaded);
	}

	DWORD CSession::GetState() const
	{
		return m_StateDispatcher->GetState();
	}

	void CSession::ChangeState(DWORD dwState)
	{
		m_StateDispatcher->ChangeState(dwState);
	}

	void CSession::SetState(DWORD dwState)
	{
		m_StateDispatcher->SetState(dwState);
	}

	void CSession::CreateSessionVariables()
	{
		ScopedNameTablePtr pNameTbl = m_ExecContext.pVar;

		pNameTbl->PushScope(L"session");

		CCXMLString sStartupMode;
		if (m_InitMsg.IsParam(L"CallID"))
		{
			sStartupMode = L"newcall";
		}
		else if (m_InitMsg.IsParam(L"ccxmlparent"))
		{
			sStartupMode = L"createccxml";
		}
		else
		{
			sStartupMode = L"external";
		}

		// session.startupmode
		// String that indicates the startup mode that the script was started as:
		//   newcall     Session was started due to a new incoming call.
		//   external    Session was started due to a external session launch request.
		//   createccxml Session was started due to a <createccxml> request.
		pNameTbl->AddVar(L"startupmode", sStartupMode.c_str(), true);

		// session.id
		// A globally unique string that indicates the session identifier of the executing CCXML session.
		CComVariant v;
		v.vt = VT_I8;
		v.llVal = m_sesId;
		pNameTbl->AddVar(L"id", v, true);

		// session.parentid
		// String that indicates the session identifier of the parent of the CCXML session that created this session. In the case the current session has no parent, the value of the variable will be ECMAScript undefined. Once a new CCXML session is created, the new session and its parent are completely independent.
		pNameTbl->AddVar(L"parentid", m_InitMsg.SafeReadParam(L"ccxmlparent", CMessage::String, CComVariant()).Value, true);

		// session.connections
		//pNameTbl->AddVar(L"connections", mSessionStore->GetAllConnections(), true);

		// session.conferences
		// Array which contains a list of the Conference objects that the session is currently using. The array is associative and each key in the array is the conference identifier for the Conference.
		//pNameTbl->AddVar(L"conferences", mSessionStore->GetAllConferences(), true);

		// session.dialogs
		// Array which contains a list of the Dialog objects that the session is currently using. The array is associative and each key in the array is the dialog identifier for the Dialog.
		//pNameTbl->AddVar(L"dialogs", mSessionStore->GetAllDialogs(), true);

		// session.ioprocessors
		// Array which contains a list of external event I/O access URIs which are available to the current session. The array is associative and each key in the array is the type of the event I/O processor.
		m_pIOProcessors.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"IOProcessors"));
		pNameTbl->AddVar(L"ioprocessors", m_pIOProcessors->AsVariant(), true);

		m_pCCXML.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"CCXML"));
		m_pCCXML->Add(L"state",		L"undefined",			true);

		m_pIOProcessors->Add(L"ccxml", m_pCCXML->AsVariant(), true);
		m_pIOProcessors->Add(L"dialog", CComVariant((IDispatch*)NULL), true);
		m_pIOProcessors->Add(L"basichttp", CComVariant((IDispatch*)NULL), true);

		CMessage pSession(L"session");

		pSession[L"startupmode"]	= sStartupMode.c_str();
		pSession[L"id"]	= v;
		pSession[L"parentid"] = m_InitMsg.SafeReadParam(L"ccxmlparent", CMessage::String, CComVariant()).Value;
		//pSession[L"connections"] = mSessionStore->GetAllConnections();
		//pSession[L"conferences"] = mSessionStore->GetAllConnections();
		//pSession[L"dialogs"] = mSessionStore->GetAllDialogs();
		pSession[L"ioprocessors"] = m_pIOProcessors->AsVariant();

		m_pSessionVariables.reset(new DEBUG_NEW_PLACEMENT CEvent(pSession));
	}

	CMessage CSession::ExceptionHandler(const CCXMLString& aReason)
	{
		CMessage evt(L"error.unrecoverable");
		evt[L"tagname"] = L"unrecoverable";//evt.GetName();
		evt[L"description"] = L"Unexpected exception caught";
		evt[L"reason"] = aReason;
		evt[L"ScriptID"] = m_sesId;

		//try
		//{
		//	throw;
		//}
		//catch (CMessage& e)
		//{
		//	evt += e;
		//}
		//catch (std::exception& e)
		//{
		//	//evt[L"reason"] = std::wstring(bstr_t((e.what()))).c_str();
		//	evt[L"reason"] = stow(e.what()).c_str();
		//}
		//catch (std::wstring& e)
		//{
		//	evt[L"reason"] = e;
		//}
		//catch (_com_error& e)
		//{
		//	evt[L"reason"] = (LPCWSTR)e.Description();
		//}
		//catch (...)
		//{
		//	evt[L"reason"] = L"Unknown unhandled exception";
		//}

		return evt;
	}

	void CSession::PostThreadMsg(eThreadMsg msg, void* pParam)
	{
		try
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"PostThreadMsg \"%i\"", msg);
			m_queue.push(msg);
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while m_queue push message");
			throw;
		}
	}



	//CComVariant CSession::FindAttrEvent(const SessionObject& aObjectContainer, const CCXMLString & aAttrName) const
	//{
	//	CComVariant result;

	//	aObjectContainer->FindByPredicate([&result, aAttrName]
	//		(const std::pair<const std::wstring, VAR>& objectPair)
	//	{
	//		NameTableDispatch* bridge = static_cast<NameTableDispatch*>(objectPair.second.vValue.pdispVal);
	//		if (bridge == nullptr)
	//		{
	//			return false;
	//		}

	//		RefLock<NameTableDispatch> lock(bridge);

	//		const VAR pVoxID = bridge->GetSafeValue<VAR>(aAttrName);
	//		if (pVoxID.IsEmpty())
	//		{
	//			return false;
	//		}

	//		result = pVoxID.vValue;
	//		return true;
	//	});

	//	return result;
	//}

	//CComVariant CSession::FindAttrEvent(const CCXMLString& aAttrName) const
	//{
	//	VAR var = m_ExecContext.pVar->GetValue(aAttrName);
	//	if (var.IsEmpty())
	//	{
	//		CCXMLString callId = m_InitMsg[L"CallID"].AsWideStr();
	//		var = mSessionStore->FindConnectionAttrEvent(callId, aAttrName);
	//	}

	//	return var.vValue;
	//}

	void CSession::SendAuxMsg(const CMessage& msg)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"OUT %s", msg.GetName());
		__DumpMessage(msg, m_Log);
		CPackedMsg pm(msg);
		const auto exec = m_ExecContext.pCEvt.lock();
		if (exec)
		{
			exec->PostAuxMessage(pm());
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Message \"%s\" has send", msg.GetName());
		}
		else
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Execution context hasn't initialized");
			throw std::runtime_error("Execution context hasn't initialized");
		}
	}

	void CSession::SendEvent(CEvent& evt)
	{
		SendAuxMsg(evt.ToMsg());
	}

	void CSession::SendEvent(CMessage& evt, int nSendId, ObjectId to, DWORD dwDelay)
	{
		evt[L"DestinationAddress"] = to;
		evt[L"CallbackID"]         = m_InitMsg[L"CallID"].AsInt64();
		evt[L"connectionid"]	   = m_InitMsg[L"CallID"].AsWideStr();

		if (dwDelay > 0)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"SendEvent with delay not implemented");
		}

		SendEventNow(evt, nSendId);
	}

	void CSession::SendEventNow(const CMessage& evt, int nSendId)
	{
		SendAuxMsg(evt);
		CMessage ack(L"send.successfull");
		ack[L"sendid"] = nSendId;
		EmitEvent(ack);
	}

	void CSession::DoError(CMessage &evt, const CCXMLString& _subSystem)
	{
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Dump error: %s", evt.GetName());
		__DumpMessage(evt, m_Log);

		m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Dump error: %s", evt.GetName());
		__DumpMessage(evt, m_extraLog);

		CCXMLString sError, sTag, sDescription;
		ObjectId iScriptID = 0;

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"0_1");


		if (CParam * param = evt.ParamByName(L"reason"))
			sError = param->AsWideStr();
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"0_2");
		if (CParam * param = evt.ParamByName(L"tagname"))
			sTag = param->AsWideStr();
		if (CParam * param = evt.ParamByName(L"description"))
			sDescription = param->AsWideStr();
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"0_3");
		if (CParam * param = evt.ParamByName(L"ScriptID"))
			iScriptID = param->AsInt64();

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"0_4");
		CCXMLString sScriptID = GetScriptIdHexFormat(iScriptID);

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"1");
		evt[L"event_name"] = evt.GetName();//L"error";
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Push error \"%s - %s\" to stack", sTag.c_str(), sError.c_str());
		m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"[%s] Push error \"%s - %s\" to stack", GetCurDoc().c_str(), sTag.c_str(), sError.c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"2");
		if (0 == CAtlString(_subSystem.c_str()).CompareNoCase(L"VBENGINE"))
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"3");
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Script source: %s", m_ExecContext.pEng->ScriptSource().c_str());
		}
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"4");

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"sScriptID: %s", sScriptID.c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"_subSystem: %s", _subSystem.c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"evt.GetName(): %s", evt.GetName());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"sDescription: %s", sDescription.c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"sError: %s", sError.c_str());

		CMessage alarmMsg = AlarmMsg(L"CCXML", sScriptID, _subSystem, evt.GetName(), sDescription, sError, -1111);

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"4_1");

		//SendAuxMsg(alarmMsg);
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"OUT: %s", evt.GetName());
		__DumpMessage(evt, m_Log);

		CPackedMsg pm(alarmMsg);
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"4_2");
		const auto exec = m_ExecContext.pCEvt.lock();
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"4_3");
		if (exec)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"4_4");
			exec->PostAuxMessage(pm());
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Message \"%s\" has send", alarmMsg.GetName());
		}
		else
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Execution context hasn't initialized");
			throw std::runtime_error("Execution context hasn't initialized");
		}


		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"5");

		this->EmitEvent(evt);

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"6");
	}


	void CSession::EmitEvent(const CMessage& evt)
	{
		//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"try to emit event: %s", __DumpMessage(evt, m_Log).c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"try to emit event: %s", evt.GetName());
		__DumpMessage(evt, m_Log);


		//evt[L"eventsource"]		= GetId();
		//evt[L"eventsourcetype"]	= L"ccxml1";

		EventObject pEvt(new DEBUG_NEW_PLACEMENT CEvent(evt));
		if (pEvt->MatchMask(L"error.*"))
		{
			m_EventDispatcher->PushFront(pEvt);
		}
		else
		{
			m_EventDispatcher->PushBack(pEvt);
		}

		//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Emitting event: %s", __DumpMessage(evt, m_Log).c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Emitting event: %s", evt.GetName());
		__DumpMessage(evt, m_Log);


		PostThreadMsg(tmSetEvent);
	}

	void CSession::EmitStep()
	{
		ProcessStep();
	}

	void CSession::EmitEventImmediately (CMessage& evt)
	{
		evt[L"eventsource"]		= GetId();
		evt[L"eventsourcetype"]	= L"ccxml2";

		EventObject pEvt(new DEBUG_NEW_PLACEMENT CEvent(evt));
		m_EventDispatcher->PushFront(pEvt);
		//m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Emitting event: %s", __DumpMessage(evt, m_Log).c_str());
		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Emitting event: %s", evt.GetName());
		__DumpMessage(evt, m_Log);

		PostThreadMsg(tmSetEvent);
	}

	ObjectId CSession::PostMsg(CMessage *aMsg)
	{
		ObjectId nCallID = m_InitMsg[L"CallID"].AsInt64();
		if (aMsg != nullptr)
		{
			if (CParam* param = aMsg->ParamByName(L"CallID"))
			{
				nCallID = param->AsInt64();
			}
			if (CParam* param = aMsg->ParamByName(L"VoxID"))
			{
				nCallID = param->AsInt64();
			}

			if (!aMsg->IsParam(L"CallID"))
			{
				(*aMsg)[L"CallID"] = m_InitMsg[L"CallID"].Value;
			}

			if (!aMsg->IsParam(L"CallbackID"))
			{
				(*aMsg)[L"CallbackID"] = m_InitMsg[L"CallID"].Value;
			}

			(*aMsg)[L"ScriptID"] = m_InitMsg[L"ScriptID"].Value;

			SendAuxMsg(*aMsg);
		}
		return nCallID;
	}

	eWaitEvents CSession::WaitAnyEvent(CMessage* pPostmsg, 
		const WaitingEvents& sEvents, 
		CMessage& waitmsg)
	{
		for (const auto& msgName : sEvents)
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Waiting for event \"%s\"...", msgName.c_str());
		}
		
		ObjectId nCallID = PostMsg(pPostmsg);

		for (;;)
		{
			//eThreadMsg& msg = m_queue.front();
			eThreadMsg msg = m_queue.pop_front();

			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"CSession::WaitAnyEvent GetMessage is \"%d\"...", msg);

			if (msg == tmQuit)
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"eThreadMsg == tmQuit");
				return WE_STOPTHREAD;
			}
			//m_queue.pop();

			bool bEventExist = false;
			for (const auto& msgName : sEvents)
			{
				EventObject evt = m_EventDispatcher->GetEvent(msgName, nCallID);
				if (evt.get())
				{
					waitmsg = evt->ToMsg();
					bEventExist = true;
					break;
				}
			}

			if (bEventExist)
			{
				break;
			}
			ProcessFirstEvent();
		}

		return WE_SUCCESS;
	}

	eWaitEvents CSession::WaitEvent(CMessage* pPostmsg,
		const CCXMLString& aWaitingEventName,
		CMessage& waitmsg)
	{
		WaitingEvents events{ aWaitingEventName };
		return WaitAnyEvent(pPostmsg, events, waitmsg);
	}

	void CSession::OnAuxMsg(const CMessage& msg)
	{
		bool isException = false;
		CCXMLString reason = L"Unknown";
		try
		{
			if (!m_bSessionActive)
			{
				return;
			}

			TranslateMsg(msg);
		}
		catch (std::exception& e)
		{
			isException = true;
			reason = stow(e.what());
		}
		catch (_com_error& e)
		{
			isException = true;
			reason = (LPCWSTR)e.Description();
		}
		catch (std::wstring& e)
		{
			isException = true;
			reason = e;
		}
		catch (...)
		{
			isException = true;
		}

		try
		{
			if (isException)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while translating messages: %s", reason.c_str());
				DoError(ExceptionHandler(reason), L"SESSION");
			}
		}
		catch (std::exception& error)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
			DoExit();
			SetState(VMS_ENDSCRIPT);
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError");
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError");
			DoExit();
			SetState(VMS_ENDSCRIPT);
		}

	}

	void CSession::TranslateMsg(const CMessage& msg)
	{
		// Connection specific messages
		if (const CParam* param = msg.ParamByName(L"AuxEvent"))
		{
			return; // massage has already send
			//DbgEvent(msg);
		}
		else if (msg == L"OFFERED")
		{
			if (const CParam* param = msg.ParamByName(L"SigInfo"))
				m_sQ931xml = param->AsWideStr();

			MakeEvent_ConnectionAlerting(msg/*, L"remote"*/);
		}
		else if (msg == L"TS2D_MakeCallOk")
		{
			EmitEvent(msg); // wait for this event. 
			MakeEvent_ConnectionProgressing(msg/*, L"local"*/);
		}
		else if (msg == L"PROGRESSING")
		{
			MakeEvent_ConnectionProgressing(msg);
		}
		else if (msg == L"ALERTING")
		{
			MakeEvent_ConnectionAlerting(msg/*, L"remote"*/);
		}
		else if (msg == L"CONNECTED")
		{
			MakeEvent_ConnectionConnected(msg);
		}
		else if (msg == L"DISCONNECTED")
		{
			// emit connection.disconnected before dialog.exit
			MakeEvent_ConnectionDisconnected(msg);
			if (msg.IsParam(L"ScriptID"))
			{
				KillDialogs(msg);
			}
		}
		else if (msg == L"TRANSFER_ACK")
		{
			MakeEvent_ConnectionRedirected(msg);
		}
		else if (msg == L"TRANSFER_FAILED")
		{
			MakeEvent_ConnectionRedirectFailed(msg);
		}
		else if (msg == L"TS2D_MakeCallFailed" || msg == L"MAKE_CALL_REJECTED")
		{
			// end current connection
			CMessage failedMsg(msg);
			failedMsg[L"CallID"] = m_InitMsg[L"CallID"].AsInt64();
			EmitEvent(failedMsg); // waiting for this event
			MakeEvent_ConnectionFailed(failedMsg);
		}
		else if (msg == L"CALL_FAIL")
		{
			MakeEvent_ConnectionFailed(msg);
		}

		else if (msg == L"ConferenceCreateAck" || msg == L"ConferenceCreateEmptyAck")
		{
			EmitEvent(msg); // wait for this event. 
			MakeEvent_ConferenceCreated(msg);
		}
		else if (msg == L"ConferenceDestroyAck")
		{
			EmitEvent(msg); // wait for this event. 
			MakeEvent_ConferenceDestroyed(msg);
		}
		else if (msg == L"ConferenceJoinPartyAck")
		{
			MakeEvent_ConferenceJoined(msg);
		}
		else if (msg == L"ConferenceRemovePartyAck")
		{
			MakeEvent_ConferenceUnjoined(msg);
		}
		else if (msg == L"ROUTE_DEV_ACK")
		{
			EmitEvent(msg); // wait for this event. 
			MakeEvent_ConnectionJoined(msg);
		}
		else if (msg == L"UNROUTE_DEV_ACK")
		{
			EmitEvent(msg); // wait for this event. 
			MakeEvent_ConnectionUnjoined(msg);
		}
		//else if (msg == L"RUNSCRIPT_OK")
		//{
		//	if (mSessionStore->FindDialog(msg[L"ScriptID"].AsWideStr()).IsEmpty())
		//	{
		//		ObjectId scriptId = msg[L"ScriptID"].AsInt64();
		//		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Dialog by ScriptID: \"%08X-%08X\"", DWORD(scriptId >> 32), DWORD(scriptId & 0xFFFFFFFF));
		//		mSessionStore->AddDialog(msg);
		//	}
		//}
		else if (msg == L"VXML_RUN_SCRIPT_FAILED")
		{
			ObjectId scriptId = msg[L"ScriptID"].AsInt64();
			if (!mSessionStore->IsDialog(scriptId))
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Dialog by ScriptID: \"%08X-%08X\"", DWORD(scriptId >> 32), DWORD(scriptId & 0xFFFFFFFF));
				mSessionStore->AddDialog(scriptId, msg);
			}

			EmitEvent(msg); // wait for this event. 
			MakeEvent_DialogNotstarted(msg);
		}
		else if (msg == L"VXML_RUN_SCRIPT_OK")
		{
			ObjectId scriptId = msg[L"ScriptID"].AsInt64();
			if (mSessionStore->IsDialog(scriptId))
			{
				return;
			}

			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Dialog by ScriptID: \"%08X-%08X\"", DWORD(scriptId >> 32), DWORD(scriptId & 0xFFFFFFFF));
			mSessionStore->AddDialog(scriptId, msg);

			EmitEvent(msg); // wait for this event. 
			if (const CParam *param = msg.ParamByName(L"PrepareDialog"))
			{
				MakeEvent_DialogPrepared(msg);
			}
			else
			{
				MakeEvent_DialogStart(msg);
			}
		}
		else if (msg == L"END_DIALOG")
		{
			MakeEvent_DialogExit(msg);
		}
		else if (msg == L"TERMINATED_DIALOG")
		{
			MakeEvent_DialogTerminated(msg);
		}
		else if (msg == L"PLAY_WAV_ACK"          || 
				 msg == L"PLAY_WAV_COMPLETED"    ||
				 msg == L"GET_DIGITS_COMPLETED"  ||
				 msg == L"PLAY_VIDEO_ACK"        ||
				 msg == L"PLAY_VIDEO_COMPLETED"  ||
				 msg == L"STOP_VIDEO_COMPLETED"  ||
				 msg == L"REC_WAV_COMPLETED"     ||
				 msg == L"CLEAR_DIGITS_COMPLETED"  )
		{
			//nothing to do
		}
		else if (msg == L"DIGIT_GOT"               ||
				 msg == L"RUNSCRIPT_OK"            ||
				 msg == L"GET_DIGITS_ACK"          ||
				 msg == L"STOP_VIDEO_ACK"          ||
				 msg == L"STOP_CHANNEL_ACK"        ||
				 msg == L"STOP_CHANNEL_COMPLETED"  ||
			     msg == L"CLEAR_DIGITS_ACK"        ||
				 msg == L"REC_WAV_ACK"   		   ||
				 msg == L"EXTENSION"               ||
				 //msg == L"KEEP_ALIVE"			   ||
				 msg == L"DROPED"               )
		{
			// just skip unuseful messages 
		}
		else if (msg == L"KEEP_ALIVE")
		{
			CMessage answer(L"KEEP_ALIVE_ACK");

			ObjectId sourceAddress = msg[L"SourceAddress"].AsInt64();

			SendEvent(answer, GetTickCount() + rand(), sourceAddress, 0);
		}
		else
		{
			if (const CParam *pTargetType = msg.ParamByName(L"targettype"))
			{
				CCXMLString sTargetType = pTargetType->AsWideStr();
				if (sTargetType == L"ccxml")
				{
					CMessage otherMsg(msg);
					AddNamelistToMsg(otherMsg);
					MakeEvent_Other(otherMsg);
				}
			}
			else
			{
				CMessage evt((CCXMLString(L"external.") + msg.Name).c_str());
				EmitEvent(evt += msg);
			}
		}
	}

	bool CSession::LoadDocument(const CCXMLString& sURI,const bool& bFile)
	{
		m_sNextDoc = sURI;
		int image_size = 0;
		ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
		CCXMLString sScriptID = GetScriptIdHexFormat(iScriptID);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Create CResourceCache...");
		std::unique_ptr<CResourceCache> resourceCache;
		try
		{
			resourceCache = std::make_unique<CResourceCache>();
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
		}

		if (!resourceCache)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");

			DoExit();
			SetState(VMS_ENDSCRIPT);
			return false;
		}

		if (!resourceCache->Ready())
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", resourceCache->GetHR());
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", resourceCache->GetHR());
			DoExit();
			SetState(VMS_ENDSCRIPT);
			return false;
		}

		DocumentPtr pDoc;
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Load document: \"%s\"", sURI.c_str());

		std::string in_data;

		try
		{
			SAFEARRAY * pSA = NULL;
			if (SUCCEEDED(resourceCache->GetDocument(sURI, sScriptID, bFile, &pSA)))
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: GetDocument successful");
				// get back archive
				char *c1;
				// Get data pointer
				HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

				int image_size = pSA->rgsabound->cElements;
				in_data.append(c1, image_size);

				::SafeArrayUnaccessData(pSA);
				::SafeArrayDestroy(pSA);


				//std::istringstream is(in_data);
				//boost::archive::binary_iarchive ia(is);
				//// read class state from archive
				//Factory::CCollector parser;
				//Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: deserialization, in_data.size()", in_data.size());
				//ia >> parser;
				//Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: new CDocument");

				//pDoc.reset(new DEBUG_NEW_PLACEMENT CDocument(&parser));

				std::istringstream is(in_data);

				Factory_c::CollectorPtr collector;
				{
					cereal::BinaryInputArchive iarchive(is);
					m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: deserialization, in_data.size(): %i", in_data.size());
					iarchive(collector);
					m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: new CDocument");
				}
				
				if (!collector)
				{
					throw std::runtime_error("Deserialization failed, nullptr document");
				}

				pDoc = std::make_shared<CDocument>(collector, m_Factory);
			}
		}
		//catch (boost::archive::archive_exception& ex)
		//{
		//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), CCXMLString(bstr_t(ex.what())).c_str());
		//	return false;
		//}
		catch (std::exception& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), CCXMLString(bstr_t(e.what())).c_str());
			return false;
		}
		catch (std::wstring& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), e.c_str());
			return false;
		}
		catch (_com_error& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
			return false;
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception loading document [%s]", sURI.c_str());
			if (!in_data.empty())
			{
				m_Log->LogLarge(LEVEL_WARNING, __FUNCTIONW__, L"Document source: %s", in_data.c_str());
			}
			return false;
		}
		
		try
		{
			if (pDoc.get())
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Execute document: \"%s\"", sURI.c_str());
				ExecuteDoc(pDoc, sURI);
				return true;
			}
		}
		catch (std::exception& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing document [%s]: \"%s\"", sURI.c_str(), CCXMLString(bstr_t(e.what())).c_str());
			return false;
		}
		catch (std::wstring& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing document [%s]: \"%s\"", sURI.c_str(), e.c_str());
			return false;
		}
		catch (_com_error& e)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
			return false;
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception executing document [%s]", sURI.c_str());
			return false;
		}

		m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"DoExit");
		DoExit();

		CMessage evt(L"error.badfetch");
		//evt[L"reason"] = format_wstring(L"[%s] Document \"%s\" was requested to load", GetCurDoc().c_str(), sURI.c_str()).c_str();
		evt[L"reason"] = std::wstring(L"[") + GetCurDoc() + L"] Document \"" + sURI + L"\" was requested to load";
		evt[L"tagname"] = L"Load_document";
		evt[L"description"] = L"A fetch of a document has failed";

		DoError(evt, L"SESSION");
		
		return false;
	}

	void CSession::ExecuteDoc(DocumentPtr pDoc, const CCXMLString& sURI)
	{
		if (!m_sCurDoc.empty())
		{
			// It was old document's scope
			m_ExecContext.pVar->PopScope();
		}
		// Create new document's scope
		m_ExecContext.pVar->PushScope(L"document");

		m_sCurDoc = sURI;
		m_sNextDoc = L"";

		if (m_ExecContext.pDoc)
		{
			m_ExecContext.oldDocuments.push_back(m_ExecContext.pDoc);
		}
		m_ExecContext.pDoc = pDoc;

		PostThreadMsg(tmExecuteDoc);
	}

	void CSession::OnThreadMsg(eThreadMsg msg, void* pParam)
	{
		switch (msg)
		{
			case tmExecuteDoc:
				m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Executing document \"%s\"", m_sCurDoc.c_str());
				m_ExecContext.pDoc->Execute(&m_ExecContext);
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Execution of document \"%s\" completed", m_sCurDoc.c_str());
				break;

			case tmSetEvent:
				ProcessEvents();
				break;
		}
	}

	// A CCXML session can end in one of the following ways:
	// * The CCXML application executes an <exit>.
	// * An unhandled "error.*" event.
	// * An unhandled "ccxml.kill" event.
	// * A "ccxml.kill.unconditional" event. 

	void CSession::DoExit()
	{
		m_bSessionActive = false;
		PostThreadMsg(tmQuit);
	}

	void CSession::ProcessUnhandledEvent(EventObject pEvt)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Unhandled event \"%s\"", pEvt->GetName().c_str());

		if (pEvt->MatchMask(L"error.*") || 
			pEvt->GetName() == L"ccxml.kill" ||
			pEvt->GetName() == L"ccxml.kill.unconditional")
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Executing DoExit() due to critical unhandled event \"%s\"", pEvt->GetName().c_str());
			DoExit();
		}
	}

	void CSession::ProcessEvent(EventObject pEvt)
	{
		if (!pEvt)
		{
			return;
		}

		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Processing event \"%s\"", pEvt->GetName().c_str());

		if (m_pTagEvtPrc == NULL)
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"EventProcessor not ready");
			return;
		}

		if (!m_pTagEvtPrc->SetEvent(&m_ExecContext, pEvt))
		{
			ProcessUnhandledEvent(pEvt);
		}
	}

	void CSession::ProcessFirstEvent()
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvents(): m_EvtQ size \"%i\"", m_EventDispatcher->GetQueueSize());
		EventObject evt = m_EventDispatcher->GetFirstEvent();
		if (!evt.get())
		{
			return;
		}

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvents(): pEvt \"%s\"", evt ? L"COOL" : L"NULL");
		ProcessEvent(evt);
	}

	void CSession::ProcessEvents()
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvents(): m_EvtQ size \"%i\"", m_EventDispatcher->GetQueueSize());

		EventObject evt = m_EventDispatcher->GetFirstEvent();
		while (evt.get())
		{
			ProcessEvent(evt);
			evt = m_EventDispatcher->GetFirstEvent();
		}
	}

	CMessage CSession::MakeCommonEvent_Connection(const CMessage& msg, const CCXMLString& aNewState)
	{
		ObjectId connectionId = msg[L"CallID"].AsInt64();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Make common event connection : \"%08X-%08X\", New state: %s", DWORD(connectionId >> 32), DWORD(connectionId & 0xFFFFFFFF), aNewState.c_str());

		return mSessionStore->MakeCommonConnectionMessage(connectionId, aNewState);
	}

	CMessage CSession::MakeCommonEvent_Conference(const CMessage& msg, const CCXMLString& aNewState)
	{
		ObjectId confId = msg[L"ConferenceID"].AsInt64();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Make common event conference: \"%08X-%08X\"", DWORD(confId >> 32), DWORD(confId & 0xFFFFFFFF));

		return mSessionStore->SetConferenceState(confId, aNewState).AsMessage();
	}


	CMessage CSession::MakeCommonEvent_Dialog(const CMessage& msg, const CCXMLString& aNewState)
	{
		ObjectId dialogId = msg[L"ScriptID"].AsInt64();

		//CCXMLString recipientId = msg[L"CallbackID"].AsWideStr();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Make common event dialog: \"%08X-%08X\"", DWORD(dialogId >> 32), DWORD(dialogId & 0xFFFFFFFF));

		CMessage dialogMsg = mSessionStore->SetDialogState(dialogId, /*recipientId, */aNewState).AsMessage();

		bool isError = false;
		std::wstring reason;
		try
		{
			dialogMsg[L"values"] = ExtractNameList(msg)->AsVariant();
		}
		catch (const std::exception& aException)
		{
			isError = true;
			reason = (boost::wformat(L"Exception: \"%s\" - while extract namelist") % stow(aException.what())).str();
		}
		catch (...)
		{
			isError = true;
			reason = L"Unknown exception while extract namelist";
		}

		if (isError)
		{
			CMessage evt(L"error.badfetch");
			evt[L"reason"] = reason;
			evt[L"tagname"] = L"Copy_Namelist";
			evt[L"description"] = L"Cannot copy namelist";

			DoError(evt, L"SESSION");
		}

		return dialogMsg;
	}

	void CSession::MakeEvent_Other(const CMessage& msg)
	{
		EmitEvent(msg);
	}

	void CSession::MakeEvent_CCCXMLLoaded(CMessage& msg)
	{
		m_pCCXML->SetValue(L"state", L"loaded");
		EmitEvent(MakeCommonEvent_Connection(msg, L"loaded"));
	}

	void CSession::MakeEvent_CCCXMLKill(CMessage& msg)
	{
		m_pCCXML->SetValue(L"state", L"kill");
		EmitEvent(MakeCommonEvent_Connection(msg, L"killed"));
	}

	void CSession::MakeEvent_ConnectionProgressing(const CMessage& msg/*, const CCXMLString& aOrigin*/)
	{
		ObjectId callId = msg[L"CallID"].AsInt64();
		if (!mSessionStore->IsConnection(callId))
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Connection by CallID: \"%08X-%08X\"", DWORD(callId >> 32), DWORD(callId & 0xFFFFFFFF));
			mSessionStore->AddConnection(callId, msg/*, aOrigin*/);
		}
		EmitEvent(MakeCommonEvent_Connection(msg, L"progressing"));


		//try
		//{
		//	//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump connections: %s", mSessionStore->ConnectionsToString(m_Log).c_str());
		//	mSessionStore->DumpConnections(m_Log);
		//}
		//catch (std::exception& aError)
		//{
		//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Exception when dumping connection: %s", stow(aError.what()));
		//}
		//catch (...)
		//{
		//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Unknown exception when dumping connection");
		//}
	}

	void CSession::MakeEvent_ConnectionAlerting(const CMessage& msg/*, const CCXMLString& aOrigin*/)
	{
		ObjectId callId = msg[L"CallID"].AsInt64();

		if (!mSessionStore->IsConnection(callId))
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Connection by CallID: \"%08X-%08X\"", DWORD(callId >> 32), DWORD(callId & 0xFFFFFFFF));
			mSessionStore->AddConnection(callId, msg/*, aOrigin*/);
		}

		//CCXMLString& status(m_CallsStatus[msg[L"CallID"].AsWideStr()] = L"alerting");
		EmitEvent(MakeCommonEvent_Connection(msg, L"alerting"));

		//try
		//{
		//	//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump connections: %s", mSessionStore->ConnectionsToString(m_Log).c_str());
		//	mSessionStore->DumpConnections(m_Log);
		//}
		//catch (std::exception& aError)
		//{
		//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Exception when dumping connection: %s", stow(aError.what()));
		//}
		//catch (...)
		//{
		//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Unknown exception when dumping connection");
		//}
	}

	void CSession::MakeEvent_ConnectionConnected(const CMessage& msg)
	{
		EmitEvent(MakeCommonEvent_Connection(msg, L"connected"));

		//try
		//{
		//	//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump connections: %s", mSessionStore->ConnectionsToString(m_Log).c_str());
		//	mSessionStore->DumpConnections(m_Log);
		//}
		//catch (std::exception& aError)
		//{
		//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Exception when dumping connection: %s", stow(aError.what()));
		//}
		//catch (...)
		//{
		//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Unknown exception when dumping connection");
		//}
	}

	void CSession::MakeEvent_ConnectionDisconnected(const CMessage& msg)
	{
		CMessage evt(MakeCommonEvent_Connection(msg, L"disconnected"));
		if (const CParam* param = msg.ParamByName(L"Error_ccValue"))
		{
			evt[L"reason"] = param->AsWideStr();
		}
		if (const CParam* param = msg.ParamByName(L"SigInfo"))
		{
			evt[L"info"] = param->AsWideStr();
		}
		EmitEvent(evt);
	}

	void CSession::MakeEvent_ConnectionFailed(const CMessage& msg)
	{
		CMessage evt(MakeCommonEvent_Connection(msg, L"failed"));
		if (const CParam* param = msg.ParamByName(L"Error_ccValue"))
		{
			evt[L"reason"] = param->AsWideStr();
		}
		else if (const CParam* param = msg.ParamByName(L"ReasonText"))
		{
			evt[L"reason"] = param->AsWideStr();
		}

		if (const CParam* param = msg.ParamByName(L"ReasonDescription"))
		{
			evt[L"ReasonDescription"] = param->AsWideStr();
		}
		else if (const CParam* param = msg.ParamByName(L"Error_gcMsg"))
		{
			evt[L"ReasonDescription"] = param->AsWideStr();
		}
		EmitEvent(evt);
	}

	void CSession::MakeEvent_ConnectionRedirected(const CMessage& msg)
	{
		EmitEvent(MakeCommonEvent_Connection(msg, L"redirected"));
	}

	void CSession::MakeEvent_ConnectionRedirectFailed(const CMessage& msg)
	{
		CMessage evt(MakeCommonEvent_Connection(msg, L"redirect.failed"));
		evt[L"reason"] = msg[L"ErrorCode"].Value;
		evt[L"ReasonDescription"] = msg[L"ErrorDescription"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_ConferenceCreated(const CMessage& msg)
	{
		if (msg.IsParam(L"ErrorCode"))
		{
			CMessage err(L"error.conference.create");
			err[L"conferenceid"] = 0;
			err[L"reason"]	= msg[L"ErrorDescription"].AsWideStr();
			EmitEvent(err);
			return;
		}

		ObjectId confId = msg[L"ConferenceID"].AsInt64();
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Conference by ConferenceID: \"%08X-%08X\"", DWORD(confId >> 32), DWORD(confId & 0xFFFFFFFF));

		mSessionStore->AddConference(confId, msg);
		EmitEvent(MakeCommonEvent_Conference(msg, L"created"));

		//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump conference: %s", mSessionStore->ConferencesToString(m_Log).c_str());
		//mSessionStore->DumpConferences(m_Log);
	}

	void CSession::MakeEvent_ConferenceDestroyed(const CMessage& msg)
	{
		if (msg.IsParam(L"ErrorCode"))
		{
			CMessage err(L"error.conference.destroy");
			err[L"conferenceid"] = msg[L"ConferenceID"].AsWideStr();
			err[L"reason"]	= msg[L"ErrorDescription"].AsWideStr();
			EmitEvent(err);
			return;
		}

		EmitEvent(MakeCommonEvent_Conference(msg, L"destroyed"));
	}

	void CSession::MakeEvent_ConferenceJoined(const CMessage& msg)
	{
		if (msg.IsParam(L"ErrorCode"))
		{
			CMessage err(L"error.conference.joined");
			err[L"conferenceid"] = msg[L"ConferenceID"].AsWideStr();
			err[L"reason"]	= msg[L"ErrorDescription"].AsWideStr();
			EmitEvent(err);
			return;
		}

		EmitEvent(MakeCommonEvent_Conference(msg, L"joined"));
	}

	void CSession::MakeEvent_ConferenceUnjoined(const CMessage& msg)
	{
		if (msg.IsParam(L"ErrorCode"))
		{
			CMessage err(L"error.conference.unjoined");
			err[L"conferenceid"] = msg[L"ConferenceID"].AsWideStr();
			err[L"reason"]	= msg[L"ErrorDescription"].AsWideStr();
			EmitEvent(err);
			return;
		}
		
		EmitEvent(MakeCommonEvent_Conference(msg, L"unjoined"));
	}

	void CSession::MakeEvent_ConnectionJoined(const CMessage& msg)
	{
		if (msg.IsParam(L"ErrorCode"))
		{
			CMessage err(L"error.conference.joined");
			err[L"CallbackID"] = msg[L"CallbackID"].Value;
			err[L"reason"] = msg[L"ErrorDescription"].AsWideStr();
			EmitEvent(err);
			return;
		}

		CMessage evt(L"conference.joined");
		evt[L"connectionid"] = msg[L"CallbackID"].AsWideStr();
		if (const CParam* param = msg.ParamByName(L"ReasonDescription"))
		{
			evt[L"ReasonDescription"] = param->AsWideStr();
		}
		evt[L"CallbackID"] = msg[L"CallbackID"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_ConnectionUnjoined(const CMessage & msg)
	{
		if (msg.IsParam(L"ErrorCode"))
		{
			CMessage err(L"error.conference.unjoined");
			err[L"CallbackID"] = msg[L"CallbackID"].Value;
			err[L"reason"] = msg[L"ErrorDescription"].AsWideStr();
			EmitEvent(err);
			return;
		}

		CMessage evt(L"conference.unjoined");
		evt[L"connectionid"] = msg[L"CallbackID"].AsWideStr();
		if (const CParam* param = msg.ParamByName(L"ReasonDescription"))
		{
			evt[L"ReasonDescription"] = param->AsWideStr();
		}
		
		evt[L"CallbackID"] = msg[L"CallbackID"].Value;
		EmitEvent(evt);
	}

	//void CSession::MakeEvent_Conference(CMessage& msg, const CCXMLString& sSuccessEvt, const CCXMLString& sFailEvt)
	//{
	//	if (msg.IsParam(L"ErrorCode"))
	//	{
	//		CMessage err(sFailEvt.c_str());
	//		err[L"conferenceid"] = 0;
	//		err[L"reason"]	= msg[L"ErrorDescription"].AsWideStr();
	//		EmitEvent(err);
	//	}
	//	else
	//	{
	//		CMessage evt(sSuccessEvt.c_str());
	//		evt[L"connectionid"]	  = msg[/*L"CallID"*/L"CallbackID"].AsWideStr();
	//		evt[L"ReasonDescription"] = msg[L"ReasonDescription"].Value;
	//		evt[L"CallbackID"]		  = msg[L"CallbackID"].Value;
	//		EmitEvent(evt);
	//	}
	//}

	void CSession::MakeEvent_DialogStart(const CMessage& msg)
	{
		//if (mSessionStore->FindDialog(msg[L"ScriptID"].AsWideStr()).IsEmpty())
		//{
		//	ObjectId scriptId = msg[L"ScriptID"].AsInt64();
		//	Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Dialog by ScriptID: \"%08X-%08X\"", DWORD(scriptId >> 32), DWORD(scriptId & 0xFFFFFFFF));
		//	mSessionStore->AddDialog(msg);
		//}
		EmitEvent(MakeCommonEvent_Dialog(msg, L"started"));

		//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump dialogs: %s", mSessionStore->DialogsToString(m_Log).c_str());
		//mSessionStore->DumpDialogs(m_Log);
	}

	void CSession::MakeEvent_DialogPrepared(const CMessage& msg)
	{
		ObjectId scriptId = msg[L"ScriptID"].AsInt64();
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Add Dialog by ScriptID: \"%08X-%08X\"", DWORD(scriptId >> 32), DWORD(scriptId & 0xFFFFFFFF));

		//mSessionStore->AddDialog(msg);
		EmitEvent(MakeCommonEvent_Dialog(msg, L"prepared"));
	}

	void CSession::MakeEvent_DialogNotPrepared(const CMessage& msg)
	{
		EmitEvent(MakeCommonEvent_Dialog(msg, L"notprepared"));
	}

	void CSession::MakeEvent_DialogExit(const CMessage& msg)
	{
		EmitEvent(MakeCommonEvent_Dialog(msg, L"exit"));

		//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Dump dialogs: %s", mSessionStore->DialogsToString(m_Log).c_str());
		//mSessionStore->DumpDialogs(m_Log);
	}

	void CSession::MakeEvent_DialogTerminated(const CMessage& msg)
	{
		EmitEvent(MakeCommonEvent_Dialog(msg, L"terminated"));
	}

	void CSession::MakeEvent_DialogNotstarted(const CMessage& msg)
	{
		CMessage notStarted = MakeCommonEvent_Dialog(msg, L"notstarted");
		notStarted.SetName((CCXMLString(L"error.") + notStarted.GetName()).c_str());

		if (const CParam * param = msg.ParamByName(L"ErrorDescription"))
		{
			notStarted[L"reason"] = param->AsWideStr();
		}

		EmitEvent(notStarted);
	}

	CMessage CSession::DialogPrepareCommon(const CMessage& aPostMsg)
	{
		CCXMLString parentId = aPostMsg[L"CallID"].AsWideStr();
		ObjectId recipientId = Utils::toNum<ObjectId>(parentId);

		CMessage preparedMessage;

		if (mSessionStore->IsConference(recipientId))
		{
			CConference conference = mSessionStore->GetConference(recipientId);
			preparedMessage[L"VoxID"] = conference.GetVoxId();
			preparedMessage[L"ConferenceID"] = recipientId;
		}
		else if (mSessionStore->IsConnection(recipientId))
		{
			CConnection connection = mSessionStore->GetConnection(recipientId);
			preparedMessage[L"Host"] = connection.GetHost().c_str();
			preparedMessage[L"CallID"] = recipientId;
		}
		else
		{
			throw std::runtime_error((boost::format("DialogPrepareCommon: cannot find connection by: \"%08X-%08X\"") % DWORD(recipientId >> 32) % DWORD(recipientId & 0xFFFFFFFF)).str());
		}

		//CMessage preparedMessage = mSessionStore->MakeRecipientMsg(nParentID);

		if (const CParam* pSrc = aPostMsg.ParamByName(L"src"))
		{
			preparedMessage[L"FileName"] = CCXMLString(m_sVXMLFolder + pSrc->AsWideStr()).c_str();
		}

		preparedMessage[L"ParentScriptID"] = GetId();
		preparedMessage[L"SigInfo"] = m_sQ931xml;
		preparedMessage[L"A"] = m_InitMsg.SafeReadParam(L"A", CMessage::String, CComVariant()).Value.bstrVal;
		preparedMessage[L"B"] = m_InitMsg.SafeReadParam(L"B", CMessage::String, CComVariant()).Value.bstrVal;

		if (const CParam* pSrc = aPostMsg.ParamByName(L"src"))
			preparedMessage[L"MonitorDisplayedName"] = pSrc->AsWideStr();
		else
			preparedMessage[L"MonitorDisplayedName"] = L"Unknown";

		AddPredefineToMsg(aPostMsg, preparedMessage);

		if (const CParam* pParam = aPostMsg.ParamByName(L"namelist"))
		{
			preparedMessage[L"namelist"] = pParam->AsWideStr();
		}

		AddNamelistToMsg(preparedMessage);

		return preparedMessage;
	}


	bool CSession::DialogPrepare(const CMessage& postmsg, CCXMLString& outDialogId)
	{
		CMessage msg = DialogPrepareCommon(postmsg);
		msg.SetName(L"ANY2TA_RUN_SCRIPT");
		msg[L"PrepareDialog"] = true;
		msg[L"parenttype"   ] = L"ccxml";
		msg[L"CallID"] = m_InitMsg[L"CallID"].Value;
		msg[L"DestinationAddress"]	= CLIENT_ADDRESS::ANY_CLIENT;

		CMessage waitmsg;
		WaitingEvents waiting_events;
		waiting_events.push_back(L"VXML_RUN_SCRIPT_OK");
		waiting_events.push_back(L"VXML_RUN_SCRIPT_FAILED");
		eWaitEvents retEvent = WaitAnyEvent(&msg, waiting_events, waitmsg);

		if (WE_STOPTHREAD == retEvent)
		{
			DoExit();
			return false;
		}
		if (WE_SUCCESS == retEvent)
		{
			if (!CCXMLString(waitmsg.GetName()).compare(L"VXML_RUN_SCRIPT_OK"))
			{
				if (const CParam* param = waitmsg.ParamByName(L"ScriptID"))
				{
					outDialogId = param->AsWideStr();
					m_nCurDialogID = outDialogId;
					msg[L"ScriptID"] = Utils::toNum<ObjectId>(m_nCurDialogID);
				}
				else
				{
					throw std::runtime_error("Cannot find ScriptID in VXML_RUN_SCRIPT_OK");
				}
			}

			if (!CCXMLString(waitmsg.GetName()).compare(L"VXML_RUN_SCRIPT_FAILED"))
			{
				// nothing to do
			}
		}
		
		return true;
	}

	bool CSession::DialogStart(const CMessage& postmsg, CCXMLString& outDialogID)
	{
		CMessage msg = DialogPrepareCommon(postmsg);
		msg.SetName(L"ANY2TA_RUN_SCRIPT");
		msg[L"parenttype"] = L"ccxml";
		msg[L"CallID"] = m_InitMsg[L"CallID"].Value;
		msg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;

		if (const CParam* pPrepareDialogID = postmsg.ParamByName(L"prepareddialogid"))
		{
			// dialog has already prepared
			//    just start it

			outDialogID = pPrepareDialogID->AsWideStr();
			ObjectId scriptId = pPrepareDialogID->AsInt64();

			msg.SetName(L"LAUNCHER");
			msg[L"ScriptID"] = scriptId;

			SendEvent(msg, GetTickCount() + rand(), scriptId, 0);

			ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), Utils::toNum<ObjectId>(m_nCurDialogID));

			// the dialog has successfully started
			//MakeEvent_DialogStart(msg);
			return true;
		}

		msg[L"DestinationAddress"]	= CLIENT_ADDRESS::ANY_CLIENT;
		CMessage waitmsg;
		WaitingEvents waiting_events;
		waiting_events.push_back(L"VXML_RUN_SCRIPT_OK");
		waiting_events.push_back(L"VXML_RUN_SCRIPT_FAILED");
		eWaitEvents retEvent = WaitAnyEvent(&msg, waiting_events, waitmsg);

		if (WE_STOPTHREAD == retEvent)
		{
			DoExit();
			return false;
		}
		if (WE_SUCCESS == retEvent)
		{
			if (!CCXMLString(waitmsg.GetName()).compare(L"VXML_RUN_SCRIPT_OK"))
			{
				if (const CParam* param = waitmsg.ParamByName(L"ScriptID"))
				{
					outDialogID = param->AsWideStr();
					m_nCurDialogID = outDialogID;
					auto newScriptId = Utils::toNum<ObjectId>(m_nCurDialogID);
					msg[L"ScriptID"] = newScriptId;
					ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), newScriptId);
				}
				else
				{
					throw std::runtime_error("Cannot find ScriptID in VXML_RUN_SCRIPT_OK");
				}
			}

			if (!CCXMLString(waitmsg.GetName()).compare(L"VXML_RUN_SCRIPT_FAILED"))
			{
				// nothing to do
			}
		}
		return true;
	}

	//void CSession::DialogEnd(ObjectId /*nCallID*/, ObjectId nDialogID)
	//{
	//	return DeleteDialog(nDialogID);
	//}

	void CSession::DialogTerminate(const CMessage& aInititalMsg)
	{
		const CParam* pDialogID = aInititalMsg.ParamByName(L"ScriptID");
		if (!pDialogID)
		{
			//m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: cannot find dialog script ID\n", __DumpMessage(aInititalMsg, m_Log).c_str());
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: cannot find dialog script ID: %s", aInititalMsg.GetName());
			__DumpMessage(aInititalMsg, m_Log);

			return;
		}
		auto dialogID = pDialogID->AsInt64();
		CDialog dialog = mSessionStore->GetDialog(dialogID);

		CMessage terminateMsg(aInititalMsg);

		SendEvent(terminateMsg, GetTickCount() + rand(), dialogID,0);
	}

	void CSession::WaitForStep(unsigned int _line)
	{
		DebugerPtr2 pDbg = m_ExecContext.pDbg.lock();
		pDbg->Wait(_line);
	}

	void CSession::ProcessStep()
	{
		++m_nStepCounter;
		DebugerPtr2 pDbg = m_ExecContext.pDbg.lock();
		pDbg->Increase();
	}

	void CSession::DbgEvent(const CMessage& msg)
	{
		DebugerPtr2 pDbg = m_ExecContext.pDbg.lock();
		if (!pDbg.get())
			return;
		pDbg->SetAuxEvent(msg, true);
	}

	//void CSession::KillDialogs(const ObjectId& aCallId, const ObjectId& aScriptId)
	//{
	//	Log(LEVEL_FINEST, __FUNCTIONW__, L"KillDialogs: disconnected call = \"%I64i\"", aCallId);


	//	m_pDialogs->ForEach([aScriptId, aCallId, this](const std::pair<const std::wstring, VAR>& dialogPair)
	//	{
	//		const NameTableDispatch* bridge = static_cast<const NameTableDispatch*>(dialogPair.second.vValue.pdispVal);
	//		if (bridge == nullptr)
	//		{
	//			return;
	//		}

	//		WeakSessionObject dialog = bridge->Get();
	//		if (dialog.expired())
	//		{
	//			return;
	//		}

	//		const auto scriptId = dialog.lock()->GetSafeValue(L"ScriptID");
	//		if (scriptId.IsEmpty())
	//		{
	//			return;
	//		}

	//		ObjectId nScripDialogID = scriptId.vValue.llVal;

	//		if (nScripDialogID == aScriptId)
	//		{
	//			return; // skip dialog, which has send DISCONNECTED message
	//		}

	//		const VAR callId = dialog.lock()->GetSafeValue(L"RecipientID");
	//		if (callId.IsEmpty())
	//		{
	//			return;
	//		}

	//		ObjectId nCallID = callId.vValue.llVal;
	//		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"KillDialogs: dialog nCallID = \"%I64i\"", nCallID);
	//		if (nCallID != aCallId)
	//		{
	//			return;
	//		}

	//		CMessage msg(L"DISCONNECTED");
	//		this->SendEvent(msg, GetTickCount() + rand(), nScripDialogID, 0); // stopping of the main dialog will kill all dialogs					
	//		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Kill dialog script id = \"%I64i\" due to connection callID=\"%I64i\" has disconnected", nScripDialogID, aCallId);
	//	});

	//	//m_pDialogs->Clear(); //should be?
	//}

	void CSession::KillDialogs(const CMessage& msg)
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"KillDialogs: mSessionStore : \"0x%p\"", mSessionStore.get());

		//mSessionStore->DumpDialogs(m_Log);
		ObjectId originCallId = msg.ParamByName(L"CallID")->AsInt64();
		ObjectId originScriptId = msg.ParamByName(L"ScriptID")->AsInt64();

		auto killedDialogs = mSessionStore->KillDialogs(originCallId, originScriptId);
		for (const auto& dialogId : killedDialogs)
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Kill dialog script id = \"%08X-%08X\"", DWORD(dialogId >> 32), DWORD(dialogId & 0xFFFFFFFF));
			CMessage disconnectMsg(msg);
			disconnectMsg[L"ScriptID"] = dialogId;

			this->SendEvent(disconnectMsg, GetTickCount() + rand(), dialogId, 0); // stopping of the main dialog will kill all dialogs					
		}
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"KillDialogs OK");
	}

	void CSession::JoinConferenceElement(ObjectId nCallID, ObjectId nElementID)
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"JoinConferenceElement: nCallID = \"%I64i\", nElementID = \"%I64i\"", nCallID, nElementID);
		m_ConferenceIDs.insert(std::pair<ObjectId, ObjectId>(nCallID, nElementID));
	}

	void CSession::UnJoinConferenceElement(ObjectId nCallID, ObjectId nElementID)
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"UnJoinConferenceElement: nCallID = \"%I64i\", nElementID = \"%I64i\"", nCallID, nElementID);
		ConferenceObject::iterator it = m_ConferenceIDs.find(nCallID);
		while (it!= m_ConferenceIDs.end() && (it->first == nCallID))
		{
			if((it->second == nElementID))
			{
				m_ConferenceIDs.erase(it);
				break;;
			}
			++it;
		}
	}

	bool CSession::IsConference(const ObjectId& aConnectionId) const
	{
		return mSessionStore->IsConference(aConnectionId);
	}

	//CMessage CSession::AddRecipientParams(const ObjectId& aCallId) const
	//{
	//	return mSessionStore->MakeRecipientMsg();
	//}

	//bool CSession::IsConference(const ObjectId& aCallId, ObjectId& outConfId)const
	//{
	//	CCXMLString sID = Utils::toStr(aCallId);
	//	Log(LEVEL_FINEST, __FUNCTIONW__, L"IsConference: nID = \"%I64i\", nConfID = \"%I64i\"", aCallId, outConfId);

	//	// in case conferenceID
	//	if (!m_pConferences->GetSafeValue(sID).IsEmpty()) 
	//	{
	//		outConfId = aCallId;
	//		Log(LEVEL_FINEST, __FUNCTIONW__, L"IsConference: true: \"nConfID = nID\"");
	//		return true;
	//	}

	//	// in case VoxID
	//	WeakSessionObject pConference = m_pConferences->FindByPredicate([aCallId, this]
	//		(const std::pair<const std::wstring, VAR>& confPair)
	//	{
	//		const NameTableDispatch* bridge = static_cast<const NameTableDispatch*>(confPair.second.vValue.pdispVal);
	//		if (bridge == nullptr)
	//		{
	//			return false;
	//		}

	//		WeakSessionObject conference = bridge->Get();
	//		if (conference.expired())
	//		{
	//			return false;
	//		}

	//		const VAR voxId = conference.lock()->GetSafeValue(L"VoxID");
	//		if (voxId.IsEmpty())
	//		{
	//			return false;
	//		}
	//		ObjectId nVoxID = voxId.vValue.llVal;
	//		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"IsConference: nVoxID = \"%I64i\"", nVoxID);

	//		return nVoxID == aCallId;
	//	});

	//	if (pConference.expired())
	//	{
	//		return false;
	//	}

	//	const VAR conferenceId = pConference.lock()->GetSafeValue(L"conferenceid");
	//	if (conferenceId.IsEmpty())
	//	{
	//		return false;
	//	}

	//	outConfId = Utils::wtoi<ObjectId>(conferenceId.vValue.bstrVal);
	//	Log(LEVEL_FINEST, __FUNCTIONW__, L"IsConference: nConfID = \"%I64i\"", outConfId);

	//	return true;
	//}

	ObjectId CSession::GetCallId() const
	{
		return m_InitMsg[L"CallID"].AsInt64();
	}

	CCXMLString CSession::GetScriptIdHexFormat(ObjectId aScriptId) const
	{
		try
		{
			return (boost::wformat(L"%08X-%08X")% HIDWORD(aScriptId) %LODWORD(aScriptId)).str();
		}
		catch (std::exception& aError)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception GetScriptIdHexFormat: \"%s\"", stow(aError.what()));
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception GetScriptIdHexFormat unhandled exception");
		}
		return CCXMLString();
	}

	bool CSession::ConferenceAvailable(
		const ObjectId& nID1,     // [in]
		const ObjectId& nID2,     // [in]
		ObjectId& nConferenceID   // [out]
		)const
	{
		if (IsConference(nID1))
		{
			nConferenceID = nID1;
			return true;
		}
		if (IsConference(nID2)) 
		{
			nConferenceID = nID2;
			return true;
		}
		return false;
	}

	void CSession::DoStep()
	{

		bool isException = false;
		CCXMLString reason = L"Unknown";
		try
		{
			if ((GetState() == VMS_READY))
			{
				EmitStep();
			}
		}
		catch (std::exception& e)
		{
			isException = true;
			reason = stow(e.what());
		}
		catch (_com_error& e)
		{
			isException = true;
			reason = (LPCWSTR)e.Description();
		}
		catch (std::wstring& e)
		{
			isException = true;
			reason = e;
		}
		catch (...)
		{
			isException = true;
		}

		try
		{
			if (isException)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while emit step: %s", reason.c_str());
				DoError(ExceptionHandler(reason), L"SESSION");
			}
		}
		catch (std::exception& error)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError: %s", stow(error.what()).c_str());
			DoExit();
			SetState(VMS_ENDSCRIPT);
		}
		catch (...)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError");
			m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while DoError");
			DoExit();
			SetState(VMS_ENDSCRIPT);
		}
	}

	void CSession::ChangeCallCtrl(const ObjectId& aOldScriptId, const ObjectId& aNewScriptId)
	{
		CDialog dialog = mSessionStore->GetDialog(aNewScriptId);

		if (dialog.IsConference())
		{
			return; //do not necessary change control to conference
		}

		CMessage change_control_msg(L"CHANGE_CALL_CONTROL");
		change_control_msg[L"CallID"] = dialog.GetRecipientId();
		change_control_msg[L"OldScriptID"] = aOldScriptId;
		change_control_msg[L"NewScriptID"] = aNewScriptId;
		change_control_msg[L"DestinationAddress"] = m_InitMsg[L"CallID"].Value;
		change_control_msg[L"SourceAddress"] = m_InitMsg[L"ScriptID"].Value;
		SendAuxMsg(change_control_msg);
	}

	CCXMLString CSession::GetVarValue(const CCXMLString& _name)const
	{
		try
		{
			return m_ExecContext.pEng->ExprEvalToStr(_name);
		}
		catch (CCXMLString &_error)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"GetVarValue error: %s", _error.c_str());
			return SYMBOLNOTFOUND_DBGMSG;
		}
		catch (...)
		{
			return SYMBOLNOTFOUND_DBGMSG;
		}
	}

	CCXMLString CSession::GetConnectionANmberByCallID(const ObjectId& aCallId)
	{
		auto connection = mSessionStore->GetConnection(aCallId);

		return connection.GetRemote();
	}

	void CSession::StartThread()
	{
		m_thread.reset(
			new boost::thread(boost::bind(&CSession::ThreadProc, this))
			);
	}

	void CSession::JoinThread()
	{
		if (m_bThreadStarted)
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Thread still active -> Finish him!");
			DoExit();
			m_thread->join();
		}
		m_thread.reset();
	}

	void CSession::AddPredefineToMsg(const CMessage& aInitialMsg, CMessage& outMsg)
	{
		AddPredefineToMsgInternal(aInitialMsg, m_sVXMLFolder, outMsg);
	}

	void CSession::AddNamelistToMsg(CMessage& msg)
	{
		AddNamelistToMsgInternal(m_ExecContext.pEng, msg);
	}

} // namespace CCXML
