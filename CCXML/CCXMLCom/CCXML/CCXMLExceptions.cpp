/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "StdAfx.h"
#include <exception>
#include <xutility>

namespace CCXML
{
	void throw_exception(const wchar_t* pwszFormat, ...)
	{
		char szBuf[4096] = {0};
		wchar_t wszBuf[sizeof(szBuf)] = {0};
		va_list args;
		va_start(args, pwszFormat);
		_vsnwprintf_s(wszBuf, sizeof(szBuf), sizeof(szBuf) - 1, pwszFormat, args);
		::WideCharToMultiByte(CP_ACP, 0, wszBuf, -1, szBuf, sizeof(szBuf) - 1, NULL, NULL);
		throw std::exception(szBuf);
	}

	void throw_oserror(const wchar_t* pwszMessage, int nErr)
	{
		WCHAR wszBuf[1024] = {0};

		FormatMessageW(
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			nErr = (nErr == -1) ? ::GetLastError() : nErr,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			wszBuf,
			sizeof(wszBuf) / sizeof(WCHAR) - 1,
			NULL);

		throw_exception(
			((DWORD)nErr > 0x10000) ? L"%s (0x%08X, %s)" : L"%s (%d, %s)", 
			pwszMessage, nErr, wszBuf);
	}
}
