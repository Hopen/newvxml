/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "..\StdAfx.h"
#include "boost\lexical_cast.hpp"
#include "CCXMLTags.h"
#include "CCXMLSession.h"
#include "CCXMLEngine.h"
#include "CCXMLNameTable.h"
#include "sv_strutils.h"
#include "Patterns/string_functions.h"
#include <algorithm>
#include <sstream>
#include <fstream>
#include "EngineConfig.h"
#include "..\Engine\EngineLog.h"

using namespace enginelog;

namespace CCXML
{
	static SessionPtr2 GetSessionPtr(CExecContext* pCtx)
	{
		auto pSes = pCtx->pSes.lock();
		if (pSes)
		{
			return pSes;
		}

		throw std::runtime_error("Cannot get session pointer");
		return nullptr;
	}

	class CScopeSaver
	{
	public:
		CScopeSaver(ScopedNameTablePtr pVar, const CCXMLString& sName)
		{
			m_pVar = pVar;
			if (m_pVar)
				m_pVar->PushScope(sName);
		}
		~CScopeSaver()
		{
			if (m_pVar)
				m_pVar->PopScope();
		}	
	private:
		ScopedNameTablePtr m_pVar;
	};


	// CTag implementation

	//static int CtorCount = 0;
	//static int DtorCount = 0;

	ObjectId CTag::GetConnectionId(CExecContext * pCtx)
	{
		ObjectId id = 0;
		CComVariant vExpr = EvalAttrEvent(pCtx, L"connectionid", VT_I8);
		if (vExpr.vt == VT_I8)
		{
			id = vExpr.llVal;
		}
		else
		{
			SessionPtr2 pSes = pCtx->pSes.lock();
			id = pSes->GetCallId();
		}

		return id;
	}

	ObjectId CTag::GetConferenceId(CExecContext * pCtx)
	{
		ObjectId id = 0;
		CComVariant vExpr = EvalAttrEvent(pCtx, L"conferenceid", VT_I8);
		if (vExpr.vt == VT_I8)
		{
			id = vExpr.llVal;
		}
		else
		{
			throw std::logic_error("Could not find ConferenceId");
		}

		return id;
	}

	CTag::CTag(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory): Factory_c::CFTag()
	{
		m_sName   = _parser->GetName();
		//m_sText   = _parser->GetText();
		m_sText = _parser->GetXML();
		m_Attribs = _parser->GetAttr();
		m_iLine   = _parser->GetLine();

		//CEngineLog log;
		//log.Init(CCCXMLEngineConfig::Instance().sLogFileName.c_str());
		//log.Log(__FUNCTIONW__, L"Constructor CTag name: %s", GetName().c_str());

		//CEngineLog log;
		//log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml\\ccxml_tags_%y-%m-%d.log");
		//log.Log(LEVEL_FINEST, __FUNCTIONW__, L"Constructor CTag called: %i", ++CtorCount);
		//m_count = CtorCount;
		//++DtorCount;
	}


	CTag::~CTag()
	{
		//CEngineLog log;
		//log.Init(CCCXMLEngineConfig::Instance().sLogFileName.c_str());
		//log.Log(__FUNCTIONW__, L"Destructor CTag name: %s", GetName().c_str());

		//CEngineLog log;
		//log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml\\ccxml_tags_%y-%m-%d.log");
		//log.Log(LEVEL_FINEST, __FUNCTIONW__, L"Destructor CTag called: %i, at last: %i", m_count/*++DtorCount*/, --DtorCount);

	}

	void CTag::EmitError(CExecContext* pCtx, CMessage& evt, const CCXMLString& description, const CCXMLString& reason)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		evt[L"reason"]	= reason;
		evt[L"tagname"]	= GetName();
		evt[L"description"] = description.c_str();
		pSes->DoError(evt, L"EXECTAGPROC");
	}
	
	CComVariant CTag::EvalAttrEvent(CExecContext* pCtx,
		const CCXMLString& sAttrName,
		VARTYPE evalType)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		CCXMLString sAttrVal = sAttrName;
		if (GetAttrVal(sAttrName, sAttrVal))
		{
			return pCtx->pEng->ExprEvalToType(sAttrVal, evalType);
		}

		return CComVariant();
	}

	

	//CComVariant CTag::EvalAttrEvent(CExecContext* pCtx, 
	//	const CCXMLString& sAttrName, 
	//	VARTYPE evalType)
	//{
	//	SessionPtr2 pSes = pCtx->pSes.lock();

	//	CCXMLString sAttrVal = sAttrName;
	//	if (GetAttrVal(sAttrName, sAttrVal))
	//	{
	//		return pCtx->pEng->ExprEvalToType(sAttrVal, evalType);
	//	}

	//	CComVariant vExpr = pSes->FindAttrEvent(sAttrName);
	//	if (vExpr.vt != evalType)
	//	{
	//		if (FAILED(vExpr.ChangeType(evalType)))
	//		{
	//			CCXMLVariant::ChangeType(vExpr, VT_BSTR);
	//			return pCtx->pEng->ExprEvalToType(vExpr.bstrVal, evalType);
	//		}
	//	}
	//	return vExpr;
	//}

	//ObjectId CTag::EvalAttrEvent(CExecContext* pCtx, const CCXMLString& sAttrName)
	//{
	//	ObjectId id = 0;
	//	CComVariant vExpr = EvalAttrEvent(pCtx, sAttrName, VT_I8);
	//	if (vExpr.vt == VT_I8)
	//	{
	//		id = vExpr.llVal;
	//	}
	//	else
	//	{
	//		GetSessionPtr(pCtx)->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Could not find \"%s\" param value", sAttrName.c_str());
	//	}
	//	return id;
	//}

	// CParentTag implementation

	CParentTag::CParentTag(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory):CTag (_parser, _factory)
	{
		const auto& children = _parser->GetChildren();
		for (const auto& child : children)
		{
			CCXMLString sName = child->GetName();
			if (sName != L"meta" && sName != L"metadata" && sName != L"#comment")	// skip meta tags and comments
			{
				m_Children.emplace_back(_factory.CreateTag(sName, child, _factory));
			}

		}
	}

	bool CParentTag::Execute(CExecContext* pCtx)
	{
		for (const auto pTag : m_Children)
		{
			if (!pTag)
			{
				throw std::runtime_error("void CParentTag::Execute: m_Children has unexpected NULL child");
			}

			GetSessionPtr(pCtx)->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CParentTag::Execute: Executing tag <%s>", pTag->GetName().c_str());

			if (!ExecuteChild(pCtx, pTag))
				return false;
		}
		return true;
	}

	bool CParentTag::ExecuteChild(CExecContext* pCtx, TagPtr pTag)
	{
		auto pSes = GetSessionPtr(pCtx);
		try
		{
			pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Executing tag <%s>, line = \"%i\"", pTag->GetName().c_str(), pTag->GetLine());
			pSes->WaitForStep(pTag->GetLine()); // wait until DoStep execute
			if (!pTag->Execute(pCtx))
			{
				pSes->m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Executing tag <%s> returned FALSE, line = \"%i\"", pTag->GetName().c_str(), pTag->GetLine());
				return false;
			}
			//pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Next child");
		}
		catch (std::wstring& err)
		{
			pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"ExceptionHandle1");
			EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the CallXML document",  err);
		}
		catch (_com_error& err)
		{
			pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"ExceptionHandle2");
			LPCWSTR pwszErr = (LPCWSTR)err.Description();
			EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the CallXML document", pwszErr ? pwszErr : L"<null>");
		}
		return true;
	}

	CParentTag::~CParentTag()
	{
	}

	const CCXMLString& CTextTag::GetText() const
	{
		return m_sText;
	}

	// Tags implementation

	CTag_accept::CTag_accept(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) 
	{
		int test = 0;
	}

	bool CTag_accept::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"ANSWER_CALL");

		ObjectId nCallID = GetConnectionId(pCtx);

		msg[L"CallID"] = nCallID;
		msg[L"DestinationAddress"] = nCallID;

		CCXMLString sHints;
		if (GetAttrVal(L"hints", sHints))
		{
			msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
		}
		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->SendAuxMsg(msg);

		return true;
	}

	bool CTag_cancel::Execute(CExecContext* pCtx)
	{
		//int nSendId = pCtx->pEng->ExprEvalToType(GetAttrVal(L"sendid"), VT_I4).lVal;
		//SessionPtr2 pSes = pCtx->pSes.lock();
		//pSes->CancelEvent(nSendId);

		return true;
	}

	bool CTag_ccxml::Execute(CExecContext* pCtx)
	{
		auto pSes = GetSessionPtr(pCtx);

		pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"PushScope: ccxml");
		pCtx->pVar->PushScope(L"ccxml");
		pSes->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Executing main CCXML tag");
		return __super::Execute(pCtx);
	}

	bool CTag_createcall::Execute(CExecContext* pCtx)
	{
		SessionPtr2 pSes = GetSessionPtr(pCtx);
		// validate input data
		CCXMLString sA, sB, sDest, sContact, /*sSIPContact,*/ sTimeout = L"0s", sHints, sTrunk, sAai, sProfileName;
		if (GetAttrVal(L"dest", sDest))
		{
			sB = pCtx->pEng->ExprEvalToStr(sDest);
		}

		bool bSip = sB.find(L"sip:") != -1;

		// create call
		CMessage msg(L"MAKE_CALL");

		ObjectId id1 = 0, id2 = 0;

		CCXMLString sID1;
		if (GetAttrVal(L"connectionid", sID1))
		{
			sID1 = pCtx->pEng->ExprEvalToStr(sID1);
			id1 = Utils::toNum<ObjectId>(sID1);
		}

		if (!id1) // if param "connectionid" doesn't exist or empty use current callid 
		{
			id1 = GetConnectionId(pCtx);
		}


		if (GetAttrVal(bSip ? L"contact" : L"callerid", sContact))
		{
			sA = pCtx->pEng->ExprEvalToStr(sContact);
		}
		else
		{
			sA = pSes->GetConnectionANmberByCallID(id1);
		}


		if (GetAttrVal(L"hints", sHints))
		{
			sHints = pCtx->pEng->ExprEvalToStr(sHints);
		}

		if (GetAttrVal(L"timeout", sTimeout)) // test it
		{
			sTimeout = pCtx->pEng->ExprEvalToStr(sTimeout);
		}

		if (GetAttrVal(L"aai", sAai)) // test it
		{
			sAai = pCtx->pEng->ExprEvalToStr(sAai);
		}

		if (GetAttrVal(L"ProfileName", sProfileName)) 
		{
			//sProfileName = pCtx->pEng->ExprEvalToStr(sProfileName);
		}

		
		msg[L"A"] = sA;
		msg[L"B"] = sB;

		msg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;
		msg[L"Group"] = L"default";


		if (bSip)//SIP
		{
			msg[L"IPProtocol"] = L"SIP";
			msg[L"StartType"] = 0;
			msg[L"SIPContact"] = sA;//sSIPContact;
		}

		if (!sHints.empty())
		{
			//msg[bSip ? L"SIPUser-To-User" : L"SigInfo"] = sHints;
			msg[L"SigInfo"] = sHints;
		}
		if (!sAai.empty())
		{
			msg[L"aai"] = sAai;
		}

		if (!sProfileName.empty())
		{
			msg[L"ProfileName"] = sProfileName;
		}

		if (GetAttrVal(L"trunk", sTrunk))
		{
			msg[L"Group"] = pCtx->pEng->ExprEvalToStr(sTrunk);
		}
		msg[L"TimeOut"] = Utils::ConvertTimeFromCSS1(sTimeout);


		CMessage waitmsg;
		CCXMLString sId;
		WaitingEvents waiting_events;
		waiting_events.push_back(L"TS2D_MakeCallOk");
		waiting_events.push_back(L"TS2D_MakeCallFailed");
		eWaitEvents retEvent = pSes->WaitAnyEvent(&msg, waiting_events, waitmsg);
		if (WE_SUCCESS != retEvent)
		{
			pSes->DoExit();
			return false;
		}

		if (!CCXMLString(waitmsg.GetName()).compare(L"TS2D_MakeCallFailed"))
		{
			if (CParam* pParam = waitmsg.ParamByName(L"ReasonDescription"))
			{
				pSes->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Make call failed, reason: \"%s\"", pParam->AsString());
			}

			return true;
		}

		if (CCXMLString(waitmsg.GetName()).compare(L"TS2D_MakeCallOk"))
		{
			pSes->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Unknown received message: \"%s\"", waitmsg.GetName());
			return true;
		}

		if (GetAttrVal(L"connectionid", sId))
		{
			CCXMLString statement = sId + L"=\"" + waitmsg[L"CallID"].AsWideStr() + L"\"";
			pSes->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Set new connectionid: \"%s\"", statement.c_str());
			pCtx->pEng->ExecStatement(statement);
		}


		return true;
	}

	//bool CTag_createcall::Execute(CExecContext* pCtx)
	//{
	//	CMessage msg(L"MAKE_CALL");

	//	CCXMLString sA, sB, sDest, sCaller, sTimeout = L"0s";
	//	//CComVariant vVal;
	//		
	//	if (GetAttrVal(L"dest",sDest))
	//	{
	//		//vVal = pCtx->pEng->ExprEval(sDest);
	//		sB = pCtx->pEng->ExprEvalToStr(sDest);

	//		//CCXMLString sipB(sB);
	//		//int pos = -1;
	//		//if ((pos = sipB.find(L"sip:"))>=0)
	//		//{
	//		//	sipB.replace(pos,strlen("sip:"),L"");
	//		//	if ((pos = sipB.find(L"@"))!= -1)
	//		//	{
	//		//		sipB.replace(pos,sipB.length(),L"");
	//		//		if (!sipB.empty())
	//		//			sB = sipB;
	//		//	}
	//		//}
	//	}
	//	if (GetAttrVal(L"callerid",sCaller))
	//	{
	//		sA = pCtx->pEng->ExprEvalToStr(sCaller);
	//	}
	//	//GetAttrVal(L"callerid", sA);
	//	if (GetAttrVal(L"timeout", sTimeout)) // test it
	//	{
	//		sTimeout = pCtx->pEng->ExprEvalToStr(sTimeout);
	//	}

	//	msg[ L"A"                       ] = sA;
	//	msg[ L"B"                       ] = sB;
	//	//try
	//	//{
	//	//	msg[ L"TimeOut"             ] = Utils::ConvertTimeFromCSS2(sTimeout);
	//	//}
	//	//catch (std::wstring& _error)
	//	//{
	//		msg[ L"TimeOut"             ] = Utils::ConvertTimeFromCSS1(sTimeout);
	//	//}
	//	

	//	msg[ L"DestinationAddress"      ] = CLIENT_ADDRESS::ANY_CLIENT;
	//	//msg[ L"bSendMessageToTelServer" ] = 1;
	//	msg[ L"Group"                   ] = L"default";

	//	CCXMLString sHints, sAai;
	//	if (GetAttrVal(L"hints", sHints))
	//	{
	//		msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
	//	}
	//	if (GetAttrVal(L"aai", sAai)) // test it
	//	{
	//		msg[L"aai"] = pCtx->pEng->ExprEvalToStr(sAai);
	//	}

	//	CMessage waitmsg;
	//	CCXMLString sId;
	//	WaitingEvents waiting_events;
	//	waiting_events.push_back(L"TS2D_MakeCallOk");
	//	waiting_events.push_back(L"TS2D_MakeCallFailed");

	//	eWaitEvents retEvent = pCtx->pSes->WaitMultipleEvent(&msg, waiting_events, L"connection.failed", waitmsg);
	//	if (WE_SUCCESS != retEvent)
	//	{
	//		pCtx->pSes->DoExit();
	//		return false;
	//	}

	//	if (!CCXMLString(waitmsg.GetName()).compare(L"TS2D_MakeCallFailed"))
	//	{
	//		if (CParam* pParam = waitmsg.ParamByName(L"ReasonDescription"))
	//		{
	//			GetSessionPtr(pCtx)->m_Log->Log(__FUNCTIONW__, L"Make call failed, reason: \"%s\"", pParam->AsString());
	//		}

	//	}

	//	if (!CCXMLString(waitmsg.GetName()).compare(L"TS2D_MakeCallOk"))
	//	{
	//		if (GetAttrVal(L"connectionid", sId))
	//		{
	//			pCtx->pEng->ExecStatement(sId + L"=\"" + static_cast<CCXMLString>(waitmsg[L"CallID"].AsWideStr()) + L"\"");
	//		}

	//	}
	//	return true;
	//}

	bool CTag_createccxml::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"ANY2TA_RUN_SCRIPT");
		msg[L"parenttype"   ] = L"ccxml";

		ObjectId nRequestID = (__int64(rand()) << 32) | rand();

		CCXMLString sNext = GetAttrVal(L"next");

		msg[L"RunRequestID"] = nRequestID;
		msg[L"FileName"] = sNext;

		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->SendAuxMsg(msg);

		return true;
	}

	bool CTag_createconference::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"ConferenceCreateEmpty");
		ObjectId nCallID = GetConnectionId(pCtx);
		msg[L"DestinationAddress"] = nCallID;
		msg[L"CallID"            ] = nCallID;
		CMessage waitmsg;
		SessionPtr2 pSes = pCtx->pSes.lock();
		eWaitEvents retEvent = pSes->WaitEvent(&msg, L"ConferenceCreateEmptyAck", waitmsg);
		if (WE_SUCCESS != retEvent)
		{
			pSes->DoExit();
			return false;
		}

		CCXMLString sId;
		if (GetAttrVal(L"conferenceid", sId))
		{
			//pCtx->pEng->Assign(sId, waitmsg[L"ConferenceID"].Value);
			pCtx->pEng->ExecStatement(sId + L"=\"" + waitmsg[L"ConferenceID"].AsWideStr() + L"\"");
			sId = pCtx->pEng->ExprEvalToStr(sId);
		}

		return true;
	}

	bool CTag_destroyconference::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"ConferenceDestroy");

		CCXMLString sId;
		if (!GetAttrVal(L"conferenceid", sId))
			return true;

		ObjectId nCallID = GetConferenceId(pCtx);;
		msg[L"ConferenceID"]	= nCallID;
		//msg[L"PartyID"]			= 0x0000000200000000;

		CCXMLString sHints;
		if (GetAttrVal(L"hints", sHints))
		{
			msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
		}

		CMessage waitmsg;
		nCallID = GetConnectionId(pCtx);
		msg[L"DestinationAddress"] = nCallID;
		msg[L"CallID"            ] = nCallID;

		SessionPtr2 pSes = pCtx->pSes.lock();
		eWaitEvents retEvent = pSes->WaitEvent(&msg, L"ConferenceDestroyAck", waitmsg);
		if (WE_SUCCESS != retEvent)
		{
			pSes->DoExit();
			return false;
		}
		return true;
	}

	// Conferences supporting is very expensive, so we have to use only 2 connections by routing.
	// If connection is more then 2, do unroute, than use conference
	bool CTag_join::Execute(CExecContext* pCtx)
	{
		CMessage msg(L""); 

		ObjectId id1, id2, iConfID;
		CCXMLString sID1, sID2;
		if (GetAttrVal(L"id1", sID1))
		{
			sID1 = pCtx->pEng->ExprEvalToStr(sID1);
			id1 = Utils::toNum<ObjectId>(sID1);
		}
		if (GetAttrVal(L"id2", sID2))
		{
			sID2 = pCtx->pEng->ExprEvalToStr(sID2);
			id2 = Utils::toNum<ObjectId>(sID2);
		}
		
		ObjectId nCallID = GetConnectionId(pCtx);

		SessionPtr2 pSes = pCtx->pSes.lock();
		if (pSes->ConferenceAvailable(id1, id2,iConfID))
		{
			msg.SetName(L"ConferenceJoinParty"); 
			CCXMLString sMode;
			if (GetAttrVal(L"duplex", sMode))
			{
				sMode = pCtx->pEng->ExprEvalToStr(sMode);
				if (sMode == L"full")
				{
					msg[L"Mode"] = L"Default";
				}
				else if (sMode == L"half")
				{
					msg[L"Mode"] = L"Receive";
				}
			}

			CCXMLString sHints;
			if (GetAttrVal(L"hints", sHints))
			{
				msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
			}

			msg[L"ConferenceID"]	= (iConfID != id1?id2:id1);// & 0xFFFFFFFF00000000; for what?
			msg[L"PartyID"]			= (iConfID != id1?id1:id2);
			pSes->JoinConferenceElement(iConfID != id1?id2:id1,iConfID != id1?id1:id2);

			msg[L"DestinationAddress"] = nCallID;
			msg[L"CallID"            ] = nCallID;

		}
		else
		{
			msg.SetName(L"ROUTE_DEV");
			msg[ L"SpeakerID"          ]	= id1;
			msg[ L"ListenerID"         ]	= id2;
			msg[ L"DestinationAddress" ]    = id2;
			msg[ L"CallID"             ]    = nCallID;
			pSes->JoinConferenceElement(id1,id2);
		}

		//unsigned __int64 session_id = pCtx->pSes->GetId();
		//pCtx->pSes->JoinConferenceElement(session_id,id1);
		//pCtx->pSes->JoinConferenceElement(session_id,id2);
		

		CMessage waitmsg;
		eWaitEvents retEvent = pSes->WaitEvent(&msg, L"ROUTE_DEV_ACK", waitmsg);
		if (WE_SUCCESS != retEvent)
		{
			pSes->DoExit();
			return false;
		}

		//waitmsg.SetName(L"conference.joined");
		//waitmsg [L"id1"] = id1;
		//waitmsg [L"id2"] = id2;
		//pSes->EmitEventImmediately(waitmsg);

		return true;
	}

	bool CTag_unjoin::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"");

		ObjectId id1, id2, iConfID;
		CCXMLString sID1, sID2;
		if (GetAttrVal(L"id1", sID1))
		{
			sID1 = pCtx->pEng->ExprEvalToStr(sID1);
			id1 = Utils::toNum<ObjectId>(sID1);
		}
		if (GetAttrVal(L"id2", sID2))
		{
			sID2 = pCtx->pEng->ExprEvalToStr(sID2);
			id2 = Utils::toNum<ObjectId>(sID2);
		}
		SessionPtr2 pSes = pCtx->pSes.lock();
		//unsigned __int64 session_id = pCtx->pSes->GetId();
		ObjectId nCallID = GetConnectionId(pCtx);
		if (!nCallID)
			nCallID = pSes->GetCallId();
		//CCXMLString sConferenceID;
		//if (pCtx->pSes->ConferenceAvailable(sID1,sID2,sConferenceID))
		if (pSes->ConferenceAvailable(id1, id2, iConfID))
		{
			//msg.SetName(L"ConferenceUnjoinParty"); 
			msg.SetName(L"ConferenceRemoveParty");

			CCXMLString sHints;
			if (GetAttrVal(L"hints", sHints))
			{
				msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
			}

			msg[L"ConferenceID"]	= (iConfID != id1/*sConferenceID.compare(sID1)*/?id2:id1);// & 0xFFFFFFFF00000000; for what?
			msg[L"PartyID"]			= (iConfID != id1/*sConferenceID.compare(sID1)*/?id1:id2);

			//pCtx->pSes->UnJoinConferenceElement(session_id,sConferenceID.compare(sID1)?id1:id2);
			////pCtx->pSes->UnJoinConference(sConferenceID.compare(sID1)?id1:id2);
			pSes->UnJoinConferenceElement(iConfID != id1/*sConferenceID.compare(sID1)*/?id2:id1,iConfID != id1/*sConferenceID.compare(sID1)*/?id1:id2);

			msg[L"DestinationAddress"] = nCallID;
			msg[L"CallID"            ] = nCallID;
		}
		else
		{
			msg.SetName(L"UNROUTE_DEV");
			//msg[ L"SpeakerID"          ]	= id1;
			msg[ L"ListenerID"         ]	= id2;
			msg[ L"DestinationAddress" ]    = id2;
			msg[ L"CallID"             ]    = nCallID;
			//pCtx->pSes->UnJoinConferenceElement(session_id,id1);
			//pCtx->pSes->UnJoinConferenceElement(session_id,id2);
			pSes->UnJoinConferenceElement(id1,id2);
		}

		

		CMessage waitmsg;
		eWaitEvents retEvent = pSes->WaitEvent(&msg, L"UNROUTE_DEV_ACK", waitmsg);
		if (WE_SUCCESS != retEvent)
		{
			pSes->DoExit();
			return false;
		}
		//waitmsg.SetName(L"conference.unjoined");
		//waitmsg [L"id1"] = id1;
		//waitmsg [L"id2"] = id2;
		//pSes->EmitEventImmediately(waitmsg);

		return true;
	
	}

	bool CTag_dialogprepare::Execute(CExecContext* pCtx)
	{
		CMessage msg;
		CCXMLString sSrc, sPredefine, sConnectionid, sDialogID, sNameList;
		if (GetAttrVal(L"src", sSrc))
		{
			int pos = -1;
			while ((pos = sSrc.find(L"'",++pos))>=0)
			{
				sSrc.replace(pos,1,L"");
			}
			msg[L"src"] = sSrc;
		}

		if (GetAttrVal(L"parameters", sNameList) || GetAttrVal(L"namelist", sNameList))
		{
			msg[L"namelist"] = sNameList;
		}

		if (GetAttrVal(L"predefine", sPredefine))
		{
			msg[L"predefine"] = sPredefine;
		}
		ObjectId nCallID = GetConnectionId(pCtx);
		msg[L"CallID"] = nCallID;

		CCXMLString dialogId;
		SessionPtr2 pSes = pCtx->pSes.lock();
		if (!pSes->DialogPrepare(msg, dialogId))
		{
			return false;
		}

		if (GetAttrVal(L"dialogid", sDialogID))
		{
			pCtx->pEng->ExecStatement(sDialogID + L"=\"" + dialogId + L"\"");
		}
		return true;
	}

	bool CTag_dialogstart::Execute(CExecContext* pCtx)
	{
		auto pSes = GetSessionPtr(pCtx);

		CMessage msg(L"MSG_NAME"); 
		CCXMLString sSrc, sPredefine, sConnectionid, sDialogID, sNameList;
		if (GetAttrVal(L"src", sSrc))
		{
			int pos = -1;
			while ((pos = sSrc.find(L"'",++pos))>=0)
			{
				sSrc.replace(pos,1,L"");
			}
			msg[L"src"] = sSrc;
		}

		if (GetAttrVal(L"parameters", sNameList) || GetAttrVal(L"namelist", sNameList))
		{
			msg[L"namelist"] = sNameList;
		}

		if (GetAttrVal(L"predefine", sPredefine))
		{
			msg[L"predefine"] = pCtx->pEng->ExprEvalToStr(sPredefine);
		}

		ObjectId nCallID = GetConnectionId(pCtx);
		pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"EvalAttrEvent, connectionid: \"%I64i\"", nCallID);

		msg[L"CallID"] = nCallID;
		CCXMLString dialogId;
		ObjectId nDialogID = 0;
		if (GetAttrVal(L"prepareddialogid", dialogId))
		{
			dialogId = pCtx->pEng->ExprEvalToStr(dialogId);
			msg[L"prepareddialogid"] = dialogId;
		}
		ObjectId dialogID = 0;
		if (!pSes->DialogStart(msg, dialogId))
		{
			return false;
		}
		if (GetAttrVal(L"dialogid", sDialogID))
		{
			pCtx->pEng->ExecStatement(sDialogID + L"=\"" + dialogId + L"\"");
		}

		return true;
	}

	bool CTag_dialogterminate::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"TERMINATE");
		CCXMLString sId, sImmediate, sNamelist, sHandler;
		ObjectId nDialogID = 0;
		if (GetAttrVal(L"dialogid", sId))
		{
			sId = pCtx->pEng->ExprEvalToStr(sId);
			nDialogID = Utils::wtoi<__int64>(sId);
			msg[L"ScriptID"] = nDialogID;
		}

		msg[L"immediate"] = false;
		if (GetAttrVal(L"immediate",sImmediate))
		{
			if (!sImmediate.compare(L"true"))
				msg[L"immediate"] = true;
		}

		if (GetAttrVal(L"namelist", sNamelist))
		{
			msg[L"namelist"] = sNamelist;
		}

		if (GetAttrVal(L"handler", sHandler))
		{
			msg[L"handler"] = sHandler;
		}

		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->DialogTerminate(msg);

		return true;
	}

	bool CTag_disconnect::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"DROP_CALL");

		/*CCXMLString sId;
		unsigned __int64 nCallID = 0;
		if (GetAttrVal(L"connectionid", sId))
		{
			nCallID = Utils::toNum<__int64>(sId);
		}
		else
		{
			CComVariant vExpr = pCtx->pSes->CurrentEvent()->Lookup(L"connectionid").vValue;
			if (vExpr.vt)
			{
				nCallID = Utils::wtoi<unsigned __int64>(static_cast<std::wstring>(vExpr.bstrVal));
			}
		}*/
		ObjectId nCallID = GetConnectionId(pCtx);

		msg[L"CallID"] = nCallID;
		msg[L"DestinationAddress"] = nCallID;

		CCXMLString sReason;
		if (GetAttrVal(L"reason", sReason))
		{
			msg[L"Reason"] = pCtx->pEng->ExprEvalToType(sReason, VT_I4);
		}

		CCXMLString sHints;
		if (GetAttrVal(L"hints", sHints))
		{
			msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
		}

		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->SendAuxMsg(msg);

		return true;
	}

	bool CTag_else::Execute(CExecContext* pCtx)
	{
		// Nothing to do - this tag is processed by <if>
		return true;
	}

	bool CTag_elseif::Execute(CExecContext* pCtx)
	{
		// Nothing to do - this tag is processed by <if>
		return true;
	}

	bool CTag_eventprocessor::Execute(CExecContext* pCtx)
	{
		// Let the session know about presence of the event processor
		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->SetEventProcessor(this);
		return true;
	}

	bool CTag_eventprocessor::SetEvent(CExecContext* pCtx, EventObject pEvt)
	{
		auto pSes = GetSessionPtr(pCtx);

		pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Set Event: \"%s\"", pEvt->GetName().c_str());

		CCXMLString sState, sStateVar;
		if (GetAttrVal(L"statevariable", sStateVar))
		{
			sState = pCtx->pEng->ExprEvalToStr(sStateVar);
		}

		for (const TagPtr& pTag : m_Children)
		{
			// Process only <transition> tags
			if (pTag->GetName() != L"transition")
			{
				continue;
			}
			CTag_transition* pTrans = dynamic_cast<CTag_transition*>(pTag.get());
			// If this transition matches event
			if (pTrans->MatchEvent(pCtx, sState, pEvt))
			{
				CScopeSaver saver(pCtx->pVar, L"transition");
				CCXMLString sName;
				if (pTrans->GetAttrVal(L"name", sName))
				{
					pCtx->pEng->AddObject(sName, pEvt->AsVariant());
				}
				// Execute transition tags
				bool bExecute = pTrans->Execute(pCtx);
				//if (!sName.empty())
				//{
				//	pCtx->pEng->DelObject(sName); dont delete the objects
				//}
				if (!bExecute)
				{
					pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"pTrans->Execute() failed");
				}
				return bExecute;
			}
		}

		pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Event \"%s\" not matched, mashine state \"%s\"", pEvt->GetName().c_str(), sState.c_str());
		return false;
	}

	bool CTag_exit::Execute(CExecContext* pCtx)
	{
		__super::Execute(pCtx);
		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->DoExit();
		return true;
	}

	bool CTag_fetch::Execute(CExecContext* pCtx)
	{
		return true;
	}

	bool CTag_goto::Execute(CExecContext* pCtx)
	{
		return true;
	}

	bool CTag_if::Execute(CExecContext* pCtx)
	{
		//Sleep(15000);
		// Check primary <if> condition
		bool bRes = pCtx->pEng->ExprEvalToBool(GetAttrVal(L"cond"));

		for each (TagPtr pTag in m_Children)
		{
			// If we found <else> or <elseif>, break the cycle 
			// if main <if> condition is true, so go to the end of <if>.
			// If the condition is false, evaluate it (<else> is equivalent to <elseif cond="true">)
			if (pTag->GetName() == L"else")
			{
				if (bRes)
				{
					break;
				}
				bRes = true;
			}
			else if	(pTag->GetName() == L"elseif")
			{
				if (bRes)
				{
					break;
				}
				bRes = pCtx->pEng->ExprEvalToBool(pTag->GetAttrVal(L"cond"));
			}
			else if (bRes)
			{
				// Execute child tags if we are in the true condition
				if (!ExecuteChild(pCtx, pTag))
					return false;
			}
		}

		return true;
	}

	bool CTag_log::Execute(CExecContext* pCtx)
	{
		CCXMLString sExpr, sLabel, sText;
		sText = GetAttrVal(L"expr");
		GetAttrVal(L"label", sLabel);
		sExpr = pCtx->pEng->ExprEvalToStr(sText);
		GetSessionPtr(pCtx)->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Expr: \"%s\" (source: \"%s\"), Label: \"%s\"", sExpr.c_str(), sText.c_str(), sLabel.c_str());

		return true;
	}

	bool CTag_merge::Execute(CExecContext* pCtx)
	{
		return true;
	}

	bool CTag_move::Execute(CExecContext* pCtx)
	{
		return true;
	}

	bool CTag_redirect::Execute(CExecContext* pCtx)
	{
		// validate input data
		//CCXMLString sA, sB, sDest, sContact, /*sSIPContact,*/ sTimeout = L"0s", sHints, sTrunk, sProfileName;
		CCXMLString sB, sDest;
		if (GetAttrVal(L"dest",sDest))
		{
			sB = pCtx->pEng->ExprEvalToStr(sDest);
		}

//		int pos = -1;
//		if ((pos = sB.find(L"sip:"))==-1)
//		{
//			CMessage evt(L"connection.redirect.failed");
//			EmitError(pCtx, evt, L"An error occurs when there is an error redirecting a connection using the <redirect> element", L"With blind transfer support SIP connections only");
//			return true;
//		}
//// 		else
//// 		{
//// 			sB.replace(pos,strlen("sip:"),L"");
//// 			if ((pos = sB.find(L"@"))!= -1)
//// 			{
//// 				sB.replace(pos,sB.length(),L"");
//// 			}
//// 		}

		// create call
		//CMessage msg(L"MAKE_CALL");

		ObjectId id1 = 0/*, id2 = 0*/;
		
		CCXMLString sID1;
		if (GetAttrVal(L"connectionid",sID1))
		{
			sID1 = pCtx->pEng->ExprEvalToStr(sID1);
			id1 = Utils::toNum<ObjectId>(sID1);
		}

		if(!id1) // if param "connectionid" doesn't exist or empty use current callid 
		{
			id1 = GetConnectionId(pCtx); 
		}

		SessionPtr2 pSes = pCtx->pSes.lock();
		//if (GetAttrVal(L"contact",sContact))
		//{
		//	sA = pCtx->pEng->ExprEvalToStr(sContact);
		//}
		//else
		//{
		//	sA = pSes->GetConnectionANmberByCallID(Utils::toStr(id1));
		//}


		//if (GetAttrVal(L"hints",sHints))
		//{
		//	sHints = pCtx->pEng->ExprEvalToStr(sHints);
		//}

		//if (GetAttrVal(L"timeout", sTimeout)) // test it
		//{
		//	sTimeout = pCtx->pEng->ExprEvalToStr(sTimeout);
		//}

		//if (GetAttrVal(L"ProfileName", sProfileName))
		//{
		//	//sProfileName = pCtx->pEng->ExprEvalToStr(sProfileName);
		//}


		//msg[ L"A"                       ] = sA;
		//msg[ L"B"                       ] = sB;

		//msg[ L"DestinationAddress"      ] = CLIENT_ADDRESS::ANY_CLIENT;
		//msg[ L"Group"                   ] = L"default";

		////SIP
		//msg[ L"IPProtocol"              ] = L"SIP";
		//msg[ L"StartType"               ] = 0;
		//msg[ L"SIPContact"              ] = sA;//sSIPContact;
		//msg[ L"SIPUser-To-User"         ] = sHints;
		//msg[ L"SigInfo"         ] = sHints;
		//msg[ L"aai"         ] = sHints;
		//if (GetAttrVal(L"trunk",sTrunk))
		//{
		//	msg[L"Group"] = pCtx->pEng->ExprEvalToStr(sTrunk);
		//}
		//msg[ L"TimeOut"             ] = Utils::ConvertTimeFromCSS1(sTimeout);

		//if (!sProfileName.empty())
		//{
		//	msg[L"ProfileName"] = sProfileName;
		//}

		////CCXMLString sHints, sAai;
		////if (GetAttrVal(L"hints", sHints))
		////{
		////	msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
		////}

		//CMessage waitmsg;
		////CCXMLString sId;
		//WaitingEvents waiting_events;
		//waiting_events.push_back(L"TS2D_MakeCallOk");
		//waiting_events.push_back(L"TS2D_MakeCallFailed");
		//eWaitEvents retEvent = pSes->WaitMultipleEvent(&msg, waiting_events, L"connection.failed", waitmsg);
		//if (WE_SUCCESS != retEvent)
		//{
		//	pSes->DoExit();
		//	return false;
		//}

		//if (!CCXMLString(waitmsg.GetName()).compare(L"TS2D_MakeCallFailed"))
		//{
		//	if (CParam* pParam = waitmsg.ParamByName(L"ReasonDescription"))
		//	{
		//		GetSessionPtr(pCtx)->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Make call failed, reason: \"%s\"", pParam->AsString());
		//	}

		//	return true;
		//}

		//if (CCXMLString(waitmsg.GetName()).compare(L"TS2D_MakeCallOk"))
		//{
		//	GetSessionPtr(pCtx)->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Unknown received message: \"%s\"", waitmsg.GetName());
		//	return true;
		//}

		//if (CParam * pParam = waitmsg.ParamByName(L"CallID"))
		//{
		//	id2 = waitmsg[L"CallID"].AsInt64();
		//}

		// transfer
		CMessage transfer(L"ATTENDED_TRANSFER"); 
		transfer[L"CallID"        ]  = id1;
		//transfer[L"TransferCallID"]  = id2;
		transfer[L"SipNumber"] = sB;
		//pSes->SendAuxMsg(transfer);
		pSes->SendEvent(transfer, GetTickCount() + rand(), id1, 0);

		return true;
	}

	bool CTag_reject::Execute(CExecContext* pCtx)
	{
		CMessage msg(L"DROP_CALL");

		//CCXMLString sId;
		//unsigned __int64 nCallID = 0;
		//if (GetAttrVal(L"connectionid", sId))
		//{
		//	nCallID = Utils::toNum<__int64>(sId);
		//}
		//else
		//{
		//	CComVariant vExpr = pCtx->pSes->CurrentEvent()->Lookup(L"connectionid").vValue;
		//	if (vExpr.vt)
		//	{
		//		nCallID = Utils::wtoi<unsigned __int64>(static_cast<std::wstring>(vExpr.bstrVal));
		//	}
		//}
		ObjectId nCallID = GetConnectionId(pCtx);

		msg[L"CallID"] = nCallID;
		msg[L"DestinationAddress"] = nCallID;

		CCXMLString sHints, sReason;
		if (GetAttrVal(L"reason", sReason))
		{
			sReason = pCtx->pEng->ExprEvalToStr(sReason);
			msg[L"Reason"] = _wtoi(sReason.c_str());
		}
		if (GetAttrVal(L"hints", sHints))
		{
			msg[L"SigInfo"] = pCtx->pEng->ExprEvalToStr(sHints);
		}

		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->SendAuxMsg(msg);

		return true;
	}

	bool CTag_script::Execute(CExecContext* pCtx)
	{
		//GetSessionPtr(pCtx)->m_Log->Log(__FUNCTIONW__, L"%s",GetText().c_str());
		CCXMLString sScriptText = GetText();
		CCXMLString sSrc, sFileSrc;
		SessionPtr2 pSes = GetSessionPtr(pCtx);
		if (sScriptText.empty() && GetAttrVal(L"src",sSrc) && !sSrc.empty())
		{

			//pCtx->pSes->GetFileFolder(),pCtx->pSes->GetCurDoc()
			CCXMLString _root_folder = pSes->GetFileFolder();
			CCXMLString _curdoc      = pSes->GetCurDoc();
			bool bRootFolder = false;
			if (sSrc[0] == '/')
			{
				sSrc.erase(0,1); // delete symbol "\"
				bRootFolder = true;
			}

			// for absolute URI
			//WIN32_FIND_DATA fileData;
			int pos = -1;
			//if (!Utils::GetFile(sSrc,&fileData))
			if ((_waccess(sSrc.c_str(),4))!=-1)
			{
				sFileSrc = sSrc;
			}
			// URI is relative 
			if (sFileSrc.empty())
			{
				WCHAR dir   [MAX_PATH]; ZeroMemory(dir   ,sizeof(dir   ));
				WCHAR ExtDir[MAX_PATH]; ZeroMemory(ExtDir,sizeof(ExtDir));
				if ((pos = sSrc.rfind('\\')) != -1)
				{
					sSrc._Copy_s(ExtDir,MAX_PATH,pos+1); // include "\"
					sSrc.erase(0,pos+1);
				}
				if (bRootFolder)
				{
					wcscpy_s(dir,_root_folder.c_str());
				}
				else if ((pos = _curdoc.rfind('\\')) != -1)
				{
					_curdoc._Copy_s(dir,MAX_PATH,pos+1); // include "\"
				}
				CCXMLString mask = dir + CCXMLString(ExtDir) + sSrc;
				if ((_waccess(mask.c_str(),4))!=-1)
				{
					sFileSrc = mask;
				}
				else
				{
					pSes->m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot open file: \"%s\"", sSrc.c_str());
				}

			}
			if (!sFileSrc.empty())
			{
				std::wifstream _in(sFileSrc.c_str(), std::ios::binary);
				// read whole file
				DWORD   dwDataSize=0;                                    
				if( _in.seekg(0, std::ios::end) )
				{
					dwDataSize = static_cast<DWORD>(_in.tellg());
				}
				std::wstring buff(dwDataSize,0);
				if( dwDataSize && _in.seekg(0, std::ios::beg) )
				{
					std::copy( std::istreambuf_iterator< wchar_t >(_in) ,
						std::istreambuf_iterator< wchar_t >() , 
						buff.begin() );
					m_sText = buff;
				}
				else
				{
					pSes->m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"File: \"%s\" is empty", sSrc.c_str());
				}
				_in.close();
			}

		}

		pCtx->pEng->ExecScript(GetText());
		return true;
	}

	bool CTag_send::Execute(CExecContext* pCtx)
	{
		int nSendId = GetTickCount() + rand();
		CCXMLString sTargetType;

		CCXMLString sTarget = GetAttrVal(L"target");
		ObjectId nTarget = 0;
		//ObjectId nParent = 0;

		// Check 'target' attribute
		try
		{
			nTarget = pCtx->pEng->ExprEvalToType(sTarget, VT_I8).llVal;
		}
		catch (std::wstring& err)
		{
			CMessage evt(L"error.send.targetunavailable");
			evt[L"sendid"] = nSendId;
			EmitError(pCtx, evt, L"CCXML could not send the event to the target listed in <send> due to it currently being unavailable", err);
			return true;
		}


		try
		{
			sTargetType = pCtx->pEng->ExprEvalToStr(GetAttrVal(L"targettype"));
			// Check 'targettype' attribute
			if (sTargetType == L"ccxml")
			{
			}
			else if (sTargetType == L"dialog")
			{
				//// telserver nothing known about the dialog, that's why first of all we
				////   send this message to yourself, and after it will be retranslated to the dialog
				//nParent = pCtx->pSes->GetId();
			}
			else if (sTargetType == L"basichttp")
			{
			}
			else
			{
				throw;
			}

		}
		catch (...)
		{
			CMessage evt(L"error.send.targettypeinvalid");
			evt[L"sendid"] = nSendId;
			EmitError(pCtx, evt, L"the targettype attribute specified in a <send> is not valid", L"'targettype' attribute is invalid");
			return true;
		}



		// If there is a delay, obtain it's value
		DWORD dwDelay = 0;
		CCXMLString sDelay;
		if (GetAttrVal(L"delay", sDelay))
		{
			dwDelay = Utils::ConvertTimeFromCSS1(pCtx->pEng->ExprEvalToStr(sDelay));
		}

		// Assign send id to the specified variable if any
		CCXMLString sSendId;
		if (GetAttrVal(L"sendid", sSendId))
		{
			//pCtx->pEng->Assign(sSendId, CComVariant(nSendId));
			pCtx->pEng->ExecStatement(sSendId + L"=\"" + Utils::toStr(nSendId) + L"\"");
		}

		// If this is Data send mode
		CCXMLString sData;
		SessionPtr2 pSes = pCtx->pSes.lock();
		if (GetAttrVal(L"name", sData))
		{
			// Evaluate message name
			CMessage evt(pCtx->pEng->ExprEvalToStr(sData).c_str());
			evt[L"target"]     = nTarget;
			evt[L"targettype"] = sTargetType;
			//evt[L"CallbackID"] = 
			CCXMLString sNameList;
			
			// Parse 'namelist' attribute if any 
			// and fill message fields with variable names values
			if (GetAttrVal(L"namelist", sNameList))
			{
				std::wistringstream in(sNameList.c_str());
				std::istream_iterator<CCXMLString,wchar_t> first(in), last;
				for (; first != last; ++first)
				{
					evt[(*first).c_str()] = pCtx->pEng->ExprEvalToVariant(*first);
				}
			}

			// Send the event
			pSes->SendEvent(evt, nSendId, /*(nParent)?nParent:*/nTarget, dwDelay);
		}
		else
		{
			CMessage evt(L"CCXML_MESSAGE");

			// Include XML content in the event
			evt[L"XML"] = GetText();

			// Send the event
			pSes->SendEvent(evt, nSendId, nTarget, dwDelay);
		}

		return true;
	}

	bool CTag_transition::Execute(CExecContext* pCtx)
	{
		return __super::Execute(pCtx);
	}

	bool CTag_transition::MatchEvent(CExecContext* pCtx, const CCXMLString& sState, EventObject pEvt)
	{
		// 1. Check state variable if there is 'state' attribute
		CCXMLString sStates;
		if (GetAttrVal(L"state", sStates))
		{
			if (!ListContains(sStates, sState))
			{
				return false;
			}
		}
		// 2. Check conditional expression value if there is 'cond' attribute
		CCXMLString sCond;
		if (GetAttrVal(L"cond", sCond))
		{
			if (!pCtx->pEng->ExprEvalToBool(sCond))
			{
				return false;
			}
		}
		// 3. Check event name mask
		CCXMLString sMask;
		if (GetAttrVal(L"event", sMask))
		{
			if (!pEvt->MatchMask(sMask))
			{
				return false;
			}
		}
		return true;
	}

	bool CTag_var::Execute(CExecContext* pCtx)
	{
		CComVariant vVal;
		CCXMLString sVal;
		CCXMLString sName;
		auto pSes = GetSessionPtr(pCtx);
		if (GetAttrVal(L"expr", sVal) && GetAttrVal(L"name", sName))
		{
			pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Exec statement: \"%s = %s\"", sName.c_str(), sVal.c_str());

			try
			{
				if (!SUCCEEDED(pCtx->pEng->ExecStatement(sName + L"=" + sVal)))
				{
					pSes->m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"ExecStatement failed: \"%s = %s\"", sName.c_str(), sVal.c_str());
				}
			}
			catch (...)
			{
				pSes->m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Exception while Exec statement");
				throw;
			}

		}
		return true;
	}

	//bool CTag___timer::Execute(CExecContext* pCtx)
	//{
	//	CCXMLString script_id;
	//	CCXMLString sTimeout;

	//	if (GetAttrVal(L"id", script_id) && GetAttrVal(L"timeout", sTimeout))
	//	{
	//		CComVariant vId = pCtx->pEng->ExprEval(script_id);
	//		auto timeout = Utils::ConvertTimeFromCSS1(sTimeout);


	//	}

	//	return true;
	//}

	// CTagFactory implementation

	TagPtr CTagFactory::CreateTag(
		const CCXMLString& sName, 
		const Factory_c::CollectorPtr& _parser,
		const CTagFactory& _factory) const
	{
		TagsMap::const_iterator it = m_TagsMap.find(sName);

		if (it == m_TagsMap.cend())
		{
			throw std::runtime_error(std::string("Unknown tag type: ") + wtos(sName));
		}
		return (it->second)(_parser, _factory);
	}

	CTagFactory::CTagFactory()
	{
		struct TagFuncs
		{
#define TAG_FUNC(NAME) \
		static TagPtr tag_##NAME(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) { return std::make_shared<CTag_##NAME>(_parser, _factory); }

			TAG_FUNC(accept)
			TAG_FUNC(assign)
			TAG_FUNC(cancel)
			TAG_FUNC(ccxml)
			TAG_FUNC(createcall)
			TAG_FUNC(createccxml)
			TAG_FUNC(createconference)
			TAG_FUNC(destroyconference)
			TAG_FUNC(dialogprepare)
			TAG_FUNC(dialogstart)
			TAG_FUNC(dialogterminate)
			TAG_FUNC(disconnect)
			TAG_FUNC(else)
			TAG_FUNC(elseif)
			TAG_FUNC(eventprocessor)
			TAG_FUNC(exit)
			TAG_FUNC(fetch)
			TAG_FUNC(goto)
			TAG_FUNC(if)
			TAG_FUNC(join)
			TAG_FUNC(log)
			TAG_FUNC(merge)
			TAG_FUNC(move)
			TAG_FUNC(redirect)
			TAG_FUNC(reject)
			TAG_FUNC(script)
			TAG_FUNC(send)
			TAG_FUNC(transition)
			TAG_FUNC(unjoin)
			TAG_FUNC(var)

#undef TAG_FUNC

		};

#define ADD_TAG(NAME) \
	m_TagsMap[L#NAME]			= &TagFuncs::tag_##NAME;

		ADD_TAG(accept           )
		ADD_TAG(assign           )
		ADD_TAG(cancel           )
		ADD_TAG(ccxml            )
		ADD_TAG(createcall       )
		ADD_TAG(createccxml      )
		ADD_TAG(createconference )
		ADD_TAG(destroyconference)
		ADD_TAG(dialogprepare    )
		ADD_TAG(dialogstart      )
		ADD_TAG(dialogterminate  )
		ADD_TAG(disconnect       )
		ADD_TAG(else             )
		ADD_TAG(elseif           )
		ADD_TAG(eventprocessor   )
		ADD_TAG(exit             )
		ADD_TAG(fetch            )
		ADD_TAG(goto             )
		ADD_TAG(if               )
		ADD_TAG(join             )
		ADD_TAG(log              )
		ADD_TAG(merge            )
		ADD_TAG(move             )
		ADD_TAG(redirect         )
		ADD_TAG(reject           )
		ADD_TAG(script           )
		ADD_TAG(send             )
		ADD_TAG(transition       )
		ADD_TAG(unjoin           )
		ADD_TAG(var              )
		//ADD_TAG(__timer          )

#undef ADD_TAG
	}

	bool ListContains(const CCXMLString& sList, const CCXMLString& sItem)
	{
		std::wistringstream in(sList.c_str());
		std::istream_iterator<CCXMLString,wchar_t> first(in), last;
		return std::find(first, last, sItem) != last;
	}
}
