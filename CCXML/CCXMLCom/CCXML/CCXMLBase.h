/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <comdef.h>
#include <list>
#include <memory>
#include <mutex>
#include <atomic>
#include <boost/format.hpp>

#include "..\Common\Utils.h"
#include "..\ComBridgeObject.h"
#include "..\Engine\EngineLog.h"
#include "sid.h"
#include "lockguard.h"
//#include "..\Common\KeepAlive.h"

class IEngineCallback;
class CEngineLog;
//class CSendEvent;
//class CNameTable;
//class CEvent;

class CCCXMLComInterface;

//typedef CComPtr<IResourceCache> ComRes;
//class IResourceCache;

namespace CCXML
{

	namespace CLIENT_ADDRESS{
		const __int64 ANY_CLIENT = ((__int64)0x00000002 << 32);
	}

	typedef std::wstring CCXMLString;

	class CSession;
	class CDocument;
	class CApplication;
	class CScopedNameTable;
	class CScriptEngine;
	class CResourceCache;
	class IDebuger;


	typedef std::shared_ptr<CDocument> DocumentPtr;
	typedef std::shared_ptr< CScriptEngine > EnginePtr;
	typedef std::shared_ptr< CScopedNameTable > ScopedNameTablePtr;
	typedef std::shared_ptr< CResourceCache > ResourceCachePtr;
	typedef std::weak_ptr< IDebuger > DebugerPtr;
	typedef std::shared_ptr< IDebuger > DebugerPtr2;
	typedef std::weak_ptr< CSession > SessionPtr;
	typedef std::shared_ptr< CSession > SessionPtr2;
	typedef std::shared_ptr< CApplication > ApplicationPtr;
	typedef std::shared_ptr< CEngineLog   > EngineLogPtr2;
	typedef std::weak_ptr< CEngineLog   > EngineLogPtr;
	typedef std::weak_ptr< IEngineCallback   > IEngineCallbackPtr;
	typedef std::shared_ptr< IEngineCallback   > IEngineCallbackPtr2;


	struct CExecContext
	{
		SessionPtr			pSes;
		DocumentPtr			pDoc;
		std::list<DocumentPtr> oldDocuments;

		ApplicationPtr		pApp;
		ScopedNameTablePtr	pVar;
		EnginePtr	      	pEng;
		IEngineCallbackPtr	pCEvt;
		//ResourceCachePtr    pRes;
		//EngineLogPtr2		pLog;
		//EngineLogPtr2		pXLog;
		DebugerPtr          pDbg;

	};

	//struct TSystem
	//{
	//	TSystem(): sKeepAliveTimeout(L"1")
	//	{}
	//	std::wstring sKeepAliveTimeout;
	//};


	class CCXMLVariant
	{
	public:
		static CComVariant& ChangeType(CComVariant& v, VARTYPE vtNew)
		{
			HRESULT hr = v.ChangeType(vtNew);
			if (FAILED(hr))
			{
				throw _com_error(hr);
			}
			return v;
		}
	};

	const CCXMLString SYMBOLNOTFOUND_DBGMSG      = L"Symbol not found";
	const CCXMLString PROCESSNOTAVAILABLE_DBGMSG = L"Process not available";

	class CEvent;
	using EventObject = std::shared_ptr<CEvent>;
	using EventDispatch = CComBridge<CEvent>  ;
	using EventDispatchPtr = CComBridge<CEvent>*; //ToDo: check!!!

	class CNameTable;
	using SessionObject = std::shared_ptr<CNameTable>;
	using WeakSessionObject = std::weak_ptr<CNameTable>;
	using NameTableDispatch = CComBridge<CNameTable>;
	using NameTableDispatchPtr = CComBridge<CNameTable>*; //ToDo: check!!!

	extern std::atomic<int> lock_counter;

	class TempLog
	{
	public:
		TempLog()
		{
			mLog.Init(L"finest", L"C:\\IS3\\Logs\\ccxml_temp_%y-%m-%d.log");
			mLog.SetSID(GetGlobalScriptId());
		}

		template <class... TArgs>
		void Log(TArgs&&... aArgs)
		{
			mLog.Log(std::forward<TArgs>(aArgs)...);
		}

	private:
		CEngineLog mLog;
	};

	class LockGuardBase
	{
	protected:
		LockGuardBase(const CCXMLString& aName)
			: mName(aName)
		{
			//static TempLog log;
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"IN: %s - %i", mName.c_str(), ++lock_counter);
		}

		virtual ~LockGuardBase()
		{
			//static TempLog log;
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"OUT: %s - %i", mName.c_str(), --lock_counter);
		}

	private:
		CCXMLString mName;
	};

	template <class T>
	using LockGuard = common::LockGuard<T, LockGuardBase>;

	//template <class TMutex>
	//class LockGuard : public LockGuardBase
	//{
	//public:
	//	LockGuard(const CCXMLString& aName, TMutex& aMutex)
	//		: LockGuardBase(aName)
	//		, mLock(aMutex)
	//	{
	//	}
	//private:
	//	std::lock_guard<TMutex> mLock;
	//};

	inline std::wstring ObjectIdToWideStr(ObjectId aObjectId)
	{
		return (boost::wformat(L"\"%08X-%08X\"") % DWORD(aObjectId >> 32) % DWORD(aObjectId & 0xFFFFFFFF)).str();
	}

	inline std::string ObjectIdToStr(ObjectId aObjectId)
	{
		return (boost::format("\"%08X-%08X\"") % DWORD(aObjectId >> 32) % DWORD(aObjectId & 0xFFFFFFFF)).str();
	}

	void __DumpMessage(
		const CMessage& aMessage,
		const EngineLogPtr2& aLog);
}
