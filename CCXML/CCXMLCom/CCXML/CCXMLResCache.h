/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once
#include <map>
#include "ce_xml.hpp"
#include "sv_utils.h"
#include "CCXMLBase.h"
#include "..\..\CacheInterface\CacheInterface_i.h"
//#import "MsXml6.dll"
//#include "..\Common\factory.h"

extern HINSTANCE g_hInst;

namespace CCXML
{
	class CResourceCache
	{
		CComPtr<IResourceCache> m_Res;

		CComPtr<ITestMTObj> m_Test;

		HRESULT m_hr;
		HRESULT m_test_hr;

		_bstr_t m_SchemaFile;

	public:
		HRESULT GetDocument(const CCXMLString& sSrc, const CCXMLString& sScriptID, const bool& bFile, SAFEARRAY** pSA);
		CResourceCache();
		~CResourceCache();

		bool Ready()const;
		HRESULT GetHR()const{ return m_hr; }
		HRESULT GetTestHR()const{ return m_test_hr; }

		bool TestReady()const;
		HRESULT WriteTestLog(const CCXMLString& sSrc);
	};
}
