/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <list>
#include <map>
#include "CCXMLBase.h"
#include "CCXMLTags.h"
//#include "boost/shared_ptr.hpp"

namespace CCXML
{
	class CDocument
	{
	private:
		TagPtr m_pRootTag;

	public:
		CDocument(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory);
		virtual ~CDocument();
		void Execute(CExecContext* pCtx);
	};
}
