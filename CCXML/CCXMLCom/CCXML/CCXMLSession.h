/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <map>
#include <boost/asio.hpp>
#include "CCXMLEvents.h"
#include "EvtModel.h"
#include "sv_thread.h"
#include "sv_handle.h"
#include "..\Engine\EngineLog.h"
#include "sv_sysobj.h"
#include "Engine.h"
#include "..\CCXMLComInterface.h"
#include "CCXMLBase.h"
//#include "CCXMLDebug.h"
//#include "..\Common\KeepAlive.h"
//#include "..\Engine\Engine.h"
#include "..\Common\locked_queue.h"
#include "CCXMLAppLog.h"
#include "sid.h"
#include "eventdispatcher.h"
#include "CCXMLTags.h"

namespace CCXML
{
	class CTag_eventprocessor;
	class CDocument;
	//class SessionsStore;
	class SessionsStore2;
	//class CCXMLDebugBeholder;

	enum eWaitEvents
	{
		WE_SUCCESS = 0,
		WE_FAILED,
		WE_STOPTHREAD
	};

	struct SessionCreateParams
	{
		LPMESSAGEHEADER		pInitMsg;
		//IEngineCallback*	pEvtPrc;
		IEngineCallbackPtr	pEvtPrc;
		EngineLogPtr2		pLog;
		EngineLogPtr2		pExtraLog;
		CCXMLString			pVBSLogUrl;
		DWORD				dwSurrThread;
		std::wstring		sCCXMLFolder;
		std::wstring		sVXMLFolder;
		//TSystem             CCXMLSystem;
		DebugerPtr          pDebuger;
		SessionPtr          pSession;
		ScopedNameTablePtr  pVar;
		IEngineCallback*	pCallback;
	};

	using WaitingEvents = std::list<CCXMLString>;

	class CStateDispatcher;
	using StateDispatcherPtr = std::unique_ptr<CStateDispatcher>;

	using EventDispatcherPtr = std::unique_ptr<common::CEventDispatcher<EventObject, LockGuardBase>>;

	class CSession
	{
	private:

		friend class CCXMLDebugBeholder;

		enum eThreadMsg
		{
			tmExecuteDoc = WM_USER + 1,
			tmSetEvent,
			tmStep,
			tmQuit = WM_QUIT
		};

	private:
		using ConferenceObject = std::multimap<ObjectId, ObjectId>;

	private:
		CTag_eventprocessor*	m_pTagEvtPrc;
		EventDispatcherPtr		m_EventDispatcher;
		CExecContext			m_ExecContext;
		CMessage				m_InitMsg;
		CCXMLString				m_sCurDoc;
		CCXMLString				m_sNextDoc;
		ObjectId				m_sesId;
		StateDispatcherPtr		m_StateDispatcher;
	public:
		EngineLogPtr2			m_Log;
		EngineLogPtr2			m_extraLog;
	private:
		SVSysObj::CEvent		m_ThreadEvt;

		SessionObject			m_pIOProcessors;
		SessionObject			m_pCCXML;
		CCXMLString				m_sCCXMLFolder;
		CCXMLString				m_sVXMLFolder;
		CCXMLString				m_sAppLogUrl;
		EventObject  			m_pSessionVariables;
		CCXMLString				m_sQ931xml;
		CCXMLString				m_nCurDialogID;
		ConferenceObject        m_ConferenceIDs;
		unsigned int            m_nStepCounter;
		//std::map<CCXMLString, CCXMLString> m_CallsStatus;
		const CTagFactory		m_Factory;

		std::shared_ptr < boost::thread >  m_thread;
		locked_queue      < eThreadMsg    >  m_queue;

		std::atomic_bool m_bThreadStarted;
		bool m_Initialized;

		std::unique_ptr<SessionsStore2> mSessionStore;

		std::mutex m_event_mutex;
		std::atomic_bool m_bSessionActive;


	private:
		void ChangeState(DWORD dwState);
		void SetState(DWORD dwState);
		void CreateSessionVariables();
		CMessage ExceptionHandler(const CCXMLString& aReason);
		void PostThreadMsg(eThreadMsg msg, void* pParam = NULL);
		void SendEvent(CEvent& evt);
		void SendEventNow(const CMessage& evt, int nSendId);
		//void SendQueuedEvent(int nIndex);
		void TranslateMsg(const CMessage& msg);
		bool LoadDocument(const CCXMLString& sURI,const bool& bFile);
		void ProcessUnhandledEvent(/*CEvent**/EventObject pEvt);
		void ProcessEvent(/*CEvent**/EventObject pEvt);
		void ProcessEvents();
		void ProcessStep();
		void ProcessFirstEvent();
		////CMessage FindSessionObject(ObjectId id, const SessionObject& container) const;
		//CMessage FindConnection(const CMessage& msg) const;
		//CMessage FindConnection(const ObjectId aConnId) const;
		//CMessage FindConference(const CMessage& msg) const;
		//CMessage FindConference(const ObjectId aConfId) const;
		//CMessage FindDialog    (const CMessage& msg) const;
		//CMessage FindDialog    (const ObjectId aScriptId) const;
		////DWORD MakeTimerArray(HANDLE hTimers[]);

		//CMessage AddConnection(CMessage& msg, const CCXMLString& sOrig);
		//CMessage AddConference(CMessage& msg/*, const CCXMLString& sOrig*/);
		//CMessage AddDialog    (const CMessage& msg/*, const CCXMLString& sOrig*/);

		//void DeleteConnection(CMessage& msg);
		//void DeleteConference(CMessage& msg);
		//ObjectId DeleteDialog    (CMessage& msg);
		//void DeleteDialog    (/*const ObjectId& _callid, */const ObjectId& _scriptid);
		//void DeleteDialog    (/*const CCXMLString& _callid, */const CCXMLString& _scriptid);
		CMessage MakeCommonEvent_Connection(const CMessage& msg, const CCXMLString& aNewState);
		CMessage MakeCommonEvent_Conference(const CMessage& msg, const CCXMLString& aNewState);
		CMessage MakeCommonEvent_Dialog(const CMessage& msg, const CCXMLString& aNewState);
		//CMessage MakeCommonEvent_ccxml(const CMessage& msg, const CCXMLString& aNewState);
		void MakeEvent_CCCXMLLoaded(CMessage& msg);
		void MakeEvent_CCCXMLKill(CMessage& msg);
		void MakeEvent_ConnectionAlerting(const CMessage& msg/*, const CCXMLString& aOrigin*/);
		void MakeEvent_ConnectionProgressing(const CMessage& msg/*, const CCXMLString& aOrigin*/);
		//void MakeEvent_ConnectionProgressing(const CMessage& msg);
		void MakeEvent_ConnectionConnected(const CMessage& msg);
		void MakeEvent_ConnectionDisconnected(const CMessage& msg);
		void MakeEvent_ConnectionFailed(const CMessage& msg);
		void MakeEvent_ConnectionRedirected(const CMessage& msg);
		void MakeEvent_ConnectionRedirectFailed(const CMessage& msg);
		//void MakeEvent_Conference(CMessage& msg, const CCXMLString& sSuccessEvt, const CCXMLString& sFailEvt);
		void MakeEvent_ConferenceCreated(const CMessage& msg);
		void MakeEvent_ConferenceDestroyed(const CMessage& msg);
		void MakeEvent_ConferenceJoined(const CMessage& msg);
		void MakeEvent_ConferenceUnjoined(const CMessage& msg);
		void MakeEvent_ConnectionJoined(const CMessage& msg);
		void MakeEvent_ConnectionUnjoined(const CMessage& msg);
		void MakeEvent_DialogStart(const CMessage& msg);
		void MakeEvent_DialogPrepared(const CMessage& msg);
		void MakeEvent_DialogNotPrepared(const CMessage& msg);
		void MakeEvent_DialogExit(const CMessage& msg);
		void MakeEvent_DialogTerminated(const CMessage& msg);
		void MakeEvent_DialogNotstarted(const CMessage& msg);
		void MakeEvent_Other(const CMessage& msg);
		//void AddListToMsg(CMessage& msg, const CParam* aNamelist, wchar_t aSplitChar);
		void AddPredefineToMsg(const CMessage& aInitialMsg, CMessage& outMsg);
		void AddNamelistToMsg(CMessage& msg);
		void DbgEvent(const CMessage& msg);
		void KillDialogs(const CMessage& msg);
		//void KillDialogs(const ObjectId& callID, const ObjectId& scriptID);

		//bool IsConference(const ObjectId& nID, ObjectId& confID) const;
		bool IsConference(const ObjectId& aConnectionId) const;
		//CMessage AddRecipientParams(const ObjectId& nID) const;
		void SendToAll(/*const unsigned __int64& CallID,*/ CMessage& msg);
		void ChangeCallCtrl(const ObjectId& aOldScriptId, const ObjectId& aNewScriptId);

	private:
		void ExecuteDoc(DocumentPtr pDoc, const CCXMLString& sURI);
		void OnThreadMsg(eThreadMsg	msg, void* pParam);

	protected:
		void /*virtual DWORD*/ ThreadProc(/*LPVOID pParams*/);
		void startThread();

	public:
		CSession();
		~CSession();
		bool Initialize(const SessionCreateParams& params);
		bool IsInitialized(){ return m_Initialized; }
		void DoStep();
		void WaitForStep(unsigned int _line);
		ObjectId GetId() const;
		DWORD GetState() const;
		void SetEventProcessor(CTag_eventprocessor* pEvtPrc);
		void OnAuxMsg(const CMessage& msg);
		void DoExit();
		//EventObject CurrentEvent();
		//CComVariant FindAttrEvent(const SessionObject& aObjectContainer, const CCXMLString& aAttrName) const;
		//CComVariant FindAttrEvent(const CCXMLString& aConnectionId) const;

		void SendAuxMsg(const  CMessage& msg);
		void SendEvent(CMessage& evt, int nSendId, ObjectId to, DWORD dwDelay);
		//void CancelEvent(int nSendId);
		void EmitEvent(const CMessage& evt);
		void DoError(CMessage &evt, const CCXMLString& _subSystem);
		void EmitStep();
		void EmitEventImmediately (CMessage& evt);
		ObjectId PostMsg(CMessage *aMsg);
		eWaitEvents WaitEvent(CMessage* pPostmsg, const CCXMLString& sEvents, CMessage& waitmsg);
		eWaitEvents WaitAnyEvent(CMessage* pPostmsg, const WaitingEvents& sEvents, CMessage& waitmsg);
		bool InitDialog(ObjectId nCallID, const CMessage& postmsg, CMessage& msg, const ObjectId& nParentDialogID, ObjectId & newDialogId);
		CMessage DialogPrepareCommon(const CMessage& aPostMsg);
		bool DialogStart(const CMessage& postmsg, CCXMLString& outDialogId);
		bool DialogPrepare(const CMessage& postmsg, CCXMLString& outDialogId);
		//void DialogEnd(unsigned __int64 nCallID, /*__int64 nScriptID, */CCXMLString sResName, CCXMLString sResValue);
		//void DialogEnd(ObjectId nCallID, ObjectId nDialogID);
		void DialogTerminate(const CMessage& postmsg);
		//void SubDialogStart(unsigned __int64 nParentScriptID, CCXMLString file_name, CCXMLString subdialog_name, CCXMLString predefine);
		//void SubDialogStart(const CMessage& postmsg);
		//void SubDialogEnd(unsigned __int64 nCallID, __int64 nDialogID, CCXMLString sResName, CCXMLString sResValue);
		void JoinConferenceElement(ObjectId nCallID, ObjectId nElementID);
		void UnJoinConferenceElement(ObjectId nCallID, ObjectId nElementID);
		//void UnJoinConference(ObjectId nCallID, ObjectId nConferenceID);
		CCXMLString GetFileFolder()const{return m_sCCXMLFolder;}
		CCXMLString GetCurDoc()const{return m_sCurDoc;}
		ObjectId GetCallId()const;
		CCXMLString GetScriptIdHexFormat(ObjectId aScriptId)const;

		bool ConferenceAvailable(
			const ObjectId& sID1,     // [in]
			const ObjectId& sID2,     // [in]
			ObjectId& sConferenceID   // [out]
			)const;
		CCXMLString GetVarValue(const CCXMLString& _name)const;
		CCXMLString GetConnectionANmberByCallID(const ObjectId& aCallId);
		
		void StartThread();
		void JoinThread();

	private:
	//	template <class TFunction, class...TArgs>
	//	void _log(TFunction aFunc, const EngineLogPtr2& aLogPtr, TArgs&&...aArgs) const
	//	{
	//		//const auto pLog = aLogPtr.lock();
	//		if (aLogPtr)
	//		{
	//			((*aLogPtr).*aFunc)(std::forward<TArgs>(aArgs)...);
	//		}
	//	}
	//	
	//	template <class...TArgs>
	//	void Log(TArgs&&...aArgs) const
	//	{
	//		_log(&CEngineLog::Log, m_Log, std::forward<TArgs>(aArgs)...);
	//	}

	//	template <class...TArgs>
	//	void LogExtra(TArgs&&...aArgs) const
	//	{
	//		_log(&CEngineLog::Log, m_extraLog, std::forward<TArgs>(aArgs)...);
	//	}

	//	template <class...TArgs>
	//	void LogExtraLarge(TArgs&&...aArgs) const
	//	{
	//		_log(&CEngineLog::LogLarge, m_extraLog, std::forward<TArgs>(aArgs)...);
	//	}

	//public:
	//	template <class...TArgs>
	//	void ThreadLog(TArgs&&...aArgs) const
	//	{
	//		_log(&CEngineLog::Log, m_Log/*m_threadLog*/, std::forward<TArgs>(aArgs)...);
	//	}

	//	template <class...TArgs>
	//	void ThreadLogExtra(TArgs&&...aArgs) const
	//	{
	//		_log(&CEngineLog::Log, m_extraLog/*m_threadExtraLog*/, std::forward<TArgs>(aArgs)...);
	//	}

	//	template <class...TArgs>
	//	void ThreadLogExtraLarge(TArgs&&...aArgs) const
	//	{
	//		_log(&CEngineLog::LogLarge, m_extraLog/*m_threadExtraLog*/, std::forward<TArgs>(aArgs)...);
	//	}
	};
}
