/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "..\StdAfx.h"
#include "CCXMLEngine.h"
#include "sv_strutils.h"
#include "sv_utils.h"
#include <comutil.h>
#include "..\EventBridge.h"
#include "CCXMLResCache.h"
#include "patterns\string_functions.h"
//#include "CCXMLEvents.h"

#define EXPRLIST_STATE		279

using namespace enginelog;

namespace CCXML
{
	class CLocker
	{
	private:
		CRITICAL_SECTION& m_CS;

	public:
		CLocker(CRITICAL_SECTION& cs) : m_CS(cs)
		{
			::EnterCriticalSection(&m_CS);
		}
		~CLocker()
		{
			::LeaveCriticalSection(&m_CS);
		}
	};
	// CScriptControl implementation

	//CScriptControl::CScriptControl(CScopedNameTable* pNameTbl)
	//	: CHackedScriptControl(), m_pNameTbl(pNameTbl)
	//{
	//}

	//HRESULT CScriptControl::OnGetAddr(SYM* pSym, VAR* pVar)
	//{
	//	HRESULT hr = CallOldGetAddr(pSym, pVar);
	//	if (hr != S_OK)
	//	{
	//		CCXML::VAR* v = m_pNameTbl->Find(pSym->bstrSymName);
	//		if (v != NULL)
	//		{
	//			pVar->vt = VT_BYREF | VT_VARIANT;
	//			if (v->vValue.vt & VT_BYREF)
	//			{
	//				pVar->pvarVal = v->vValue.pvarVal;
	//			}
	//			else
	//			{
	//				pVar->pvarVal = &v->vValue;
	//			}
	//			return S_OK;
	//		}
	//	}
	//	return hr;
	//}

	//HRESULT CScriptControl::OnAssignVar(CSession* pSes, VAR* pVarDst, VAR* pVarSrc, DWORD dw)
	//{
	//	CCXML::VAR* pCCXMLVar = (CCXML::VAR*)pVarDst;
	//	CCXML::VAR* pCCXMLVar2 = (CCXML::VAR*)((*(tagVARIANT*)(&*pVarSrc))).pvarVal;
	//	if (::IsBadReadPtr(pCCXMLVar, sizeof(CCXML::VAR)) || 
	//		pCCXMLVar->dwSignature != VAR_SIGNATURE)
	//	{
	//		return CallOldAssignVar(pSes, pVarDst, pVarSrc, dw);
	//	}
	//	pCCXMLVar->SetValue(pVarSrc);
	//	return S_OK;
	//}

	//// CScriptEngine implementation

	//CScriptEngine::CScriptEngine(CScopedNameTable* pNameTbl)
	//	: m_pNameTbl(pNameTbl), m_ScControl(pNameTbl)
	//{
	//	HRESULT hr = m_ScControl.Create();

	//	if (FAILED(hr))
	//	{
	//		throw format_wstring(L"Error: cannot create script control (%s, %08x)", SVUtils::WinErrToStr(hr).c_str(), hr);
	//	}
	//
	//	m_ScControl->Language		= _bstr_t(L"VBScript");
	//	m_ScControl->Timeout		= 10000;
	//	m_ScControl->AllowUI		= VARIANT_TRUE;
	//	m_ScControl->UseSafeSubset	= VARIANT_TRUE;
	//}

	//CScriptEngine::~CScriptEngine()
	//{
	//	m_ScControl.Destroy();
	//}

	//CComVariant CScriptEngine::ExprEval(const std::wstring& sExpr)
	//{
	//	std::wstring sNew(sExpr);
	//	int pos = -1;
	//	while ((pos = sNew.find(L"'",++pos))>=0)
	//	{
	//		sNew.replace(pos,1,L"\"");
	//	}
	//	return m_ScControl->Eval(_bstr_t(/*sExpr*/sNew.c_str()));
	//}

	//bool CScriptEngine::ExprEvalToBool(const std::wstring& sExpr)
	//{
	//	return ExprEvalToType(sExpr, VT_BOOL).boolVal == VARIANT_TRUE;
	//}

	//std::wstring CScriptEngine::ExprEvalToStr(const std::wstring& sExpr)
	//{
	//	return ExprEvalToType(sExpr, VT_BSTR).bstrVal;
	//}

	//CComVariant CScriptEngine::ExprEvalToType(const std::wstring& sExpr, VARTYPE vt)
	//{
	//	CComVariant v = ExprEval(sExpr);
	//	HRESULT hr = v.ChangeType(vt);
	//	if (FAILED(hr))
	//	{
	//		LPCWSTR pwszType = L"Unknown type";
	//		switch (vt)
	//		{
	//			case VT_I1: 
	//			case VT_I2: 
	//			case VT_I4: 
	//			case VT_I8: 
	//			case VT_INT:
	//			case VT_UI1:
	//			case VT_UI2:
	//			case VT_UI4:
	//			case VT_UI8:
	//			case VT_UINT: 
	//				pwszType = L"Integer";
	//				break;
	//			case VT_BOOL: 
	//				pwszType = L"Boolean";
	//				break;
	//			case VT_BSTR: 
	//				pwszType = L"String";
	//				break;
	//		}
	//		throw format_wstring(L"Cannot evaluate expression '%s' to %s", sExpr.c_str(), pwszType);
	//	}
	//	return v;
	//}

	//void CScriptEngine::ExecScript(const std::wstring& sExpr)
	//{
	//	m_ScControl->Reset();
	//	m_ScControl->AddCode(sExpr.c_str());
	//}

	//void CScriptEngine::ExecStatement(const std::wstring& sExpr)
	//{
	//	m_ScControl->ExecuteStatement(sExpr.c_str());
	//}

	//void CScriptEngine::Assign(const std::wstring& sExpr, const CComVariant& value)
	//{
	//	//CComVariant v = ExprEval(L"\"1\"");
	//	// Generate random variable name
	//	CCXMLString sVarName = format_wstring(L"tempvar%u", GetTickCount());
	//	// Create temp variable
	//	VAR& var = m_pNameTbl->AddVar(sVarName, value, true);
	//	// Make assignment
	//	ExecStatement(sExpr + L"=" + sVarName);
	//	//ExprEvalToStr(L"'The number called is' +" + sVarName + L".connection.remote + '.'");
	//	//CCXMLString sExpr2(L"TypeName(" + sVarName + L")");
	//	//sExpr2 = ExprEvalToStr(sExpr2);
	//	// Delete temp variable
	//	m_pNameTbl->DelVar(sVarName);
	//}

	//void CScriptEngine::AddObject(const CCXMLString& sName, IDispatch* object)
	//{
	//	m_ScControl->AddObject(_bstr_t(sName.c_str()),object,_variant_t(true));
	//	//ExprEvalToStr(L"'The number called is' + evt.connection.remote + '.'");
	//}

	/***** NEW SCRIPT ENGINE implementation *****/

	// CLSID VBScript
	#include <initguid.h>
	DEFINE_GUID(CLSID_VBScript, 0xb54f3741, 0x5b07, 0x11cf, 0xa4, 0xb0, 0x0,
		0xaa, 0x0, 0x4a, 0x55, 0xe8); 

	CScriptEngine::CScriptEngine(SessionPtr2 pSesion, EngineLogPtr aLogPtr) 
		: m_pMySite(nullptr)
		, m_Log(aLogPtr)
	{
		// Create script site and COM-objects
		// reference counter's this objects == 0
		Log(LEVEL_FINEST, __FUNCTIONW__, L"Create script site and COM-objects");
		CComObject<CXMLScriptSite>::CreateInstance(&m_pMySite);
		//CComObject<CNameTable>::CreateInstance(&m_pBridge);
		//m_pMyObject->CreateInstance()
		m_pMySite->SetSession(pSesion);
		HRESULT hr = S_OK;
		// init m_spUnkScriptObject
		//hr = m_pBridge->QueryInterface(IID_IUnknown,
		//	(void **)&m_pMySite->m_spUnkScriptObject);
		
		// load and register *.tlb library
		CCXMLString sDllPath = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"CCXMLCom.tlb";
		CComPtr<ITypeLib> sptLib;
		Log(LEVEL_FINEST, __FUNCTIONW__, L"load and register CCXMLCom.tlb library, by path: \"%s\"", sDllPath.c_str());
		hr = LoadTypeLib(sDllPath.c_str(), &sptLib);
//#ifdef _DEBUG
		if (FAILED(hr))
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"failed to load CCXMLCom.tlb library, using default path: \"c:\\is3\\CCXML\\CCXMLCom.tlb\"");
			//m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"failed to load CCXMLCom.tlb library, using default path: \"c:\\is3\\CCXML\\CCXMLCom.tlb\"");
			hr = LoadTypeLib(L"c:\\is3\\CCXML\\CCXMLCom.tlb", &sptLib);

		}
//#endif
		
		// init IActiveScriptSite and ITypeInfo
		hr = sptLib->GetTypeInfoOfGuid(CLSID_NameTable, &m_pMySite->m_spTypeInfo);
		
		// connect to VBScript-machine
		hr = CoCreateInstance(CLSID_VBScript, NULL, CLSCTX_INPROC_SERVER,
			IID_IActiveScript, (void **)&m_spAS);
		
		// get IActiveScriptParse
		hr = m_spAS->QueryInterface(IID_IActiveScriptParse, (void **)&m_spASP);
		
		// set sript site -> increase reference count
		hr = m_spAS->SetScriptSite((IActiveScriptSite *)m_pMySite);
		
		// init machine
		hr = m_spASP->InitNew();
	}

	CScriptEngine::~CScriptEngine()
	{
	}

	void CScriptEngine::Clear()
	{
		if (m_spAS)
		{
			m_spAS->SetScriptState(SCRIPTSTATE_DISCONNECTED);
			m_spAS->Close();
			m_spAS.Release();
		}
		if (m_spASP)
		{
			m_spASP.Release();
		}
		//if (m_pMySite)
		//{
		//	m_pMySite->Clear();
		//	m_pMySite->Release();
		//}
	}

	void CScriptEngine::Normalization(std::wstring& sExpr, std::wstring simbolsIn[],std::wstring simbolsOut[])
	{

		ATLASSERT(sizeof(simbolsOut) == sizeof(simbolsIn));

		CCXMLString *pSim = simbolsIn;
		unsigned int i = 0;
		while (pSim && !pSim->empty())
		{
			int pos = -1;
			while ((pos = sExpr.find(*pSim,++pos))>=0)
			{
				sExpr.replace(pos,pSim->size(),simbolsOut[i]);
			}

			++i;
			++pSim;
		}

		m_scriptSource = sExpr;
	}

	HRESULT CScriptEngine::AddObject(const CCXML::CCXMLString &sName, const CComVariant& object)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CScriptEngine::AddObject", m_mutex);

		std::wstring sNew(sName);
		CCXMLString simbolsIn[]  = {L"'"  , L"$", L"==", L""};
		CCXMLString simbolsOut[] = {L"\"" , L"" , L"=" , L""};
		Normalization(sNew, simbolsIn, simbolsOut);

		//EventObject cur_event = ((EventDispatchPtr)(object.pdispVal))->GetObjectPtr();
		//EventObject pEvent    = ((EventDispatchPtr)(m_pMySite->GetMyObject(sNew).pdispVal))->GetObjectPtr();
		//*(pEvent.get()) = *(cur_event.get());
		//m_pMySite->AddObject(sNew, pEvent->AsVariant());

		m_pMySite->AddObject2(sNew, object);

		return m_spAS->AddNamedItem(sNew.c_str(), SCRIPTITEM_ISVISIBLE |
		SCRIPTITEM_ISSOURCE);


	}

	HRESULT CScriptEngine::AddObject2(const CCXMLString & sName, const CComVariant & object)
	{
		LockGuard<std::mutex> lock(L"CScriptEngine::AddObject2", m_mutex);
		//std::lock_guard<std::mutex> lock(m_mutex);

		m_pMySite->AddObject(sName, object);
		return m_spAS->AddNamedItem(sName.c_str(), SCRIPTITEM_ISVISIBLE |
			SCRIPTITEM_ISSOURCE);
		return S_OK;
	}

	HRESULT CScriptEngine::DelObject(const CCXML::CCXMLString &sName)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CScriptEngine::DelObject", m_mutex);
		////std::wstring sNew(sName);
		////Normalization(sNew);
		//VariableVarsMap::iterator it = m_mVars.find(sName);
		//HRESULT hr = m_pMySite->DelObject(it->second);
		//m_mVars.erase(it);
		//return hr;
		std::wstring sNew(sName);
		CCXMLString simbolsIn[]  = {L"'"  , L"$", L"==", L""};
		CCXMLString simbolsOut[] = {L"\"" , L"" , L"=" , L""};
		Normalization(sNew,simbolsIn,simbolsOut);


		return m_pMySite->DelObject(sNew);

	}

	CComVariant CScriptEngine::ExprEval(const std::wstring& sExpr)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CScriptEngine::ExprEval", m_mutex);

		std::wstring sNew(sExpr);
		CCXMLString simbolsIn[]  = {L"'"  , L"$", L"==", L""};
		CCXMLString simbolsOut[] = {L"\"" , L"" , L"=" , L""};
		Normalization(sNew,simbolsIn,simbolsOut);

		//CLocker lock(m_rCS);
		CComVariant v;
		EXCEPINFO ei;
		m_spASP->ParseScriptText(/*sExpr*/sNew.c_str(), NULL, 
			NULL, NULL, 0, 0, SCRIPTTEXT_ISEXPRESSION, &v, &ei);
		return v;
	}

	HRESULT CScriptEngine::ExecStatement(const std::wstring& sExpr)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CScriptEngine::ExecStatement", m_mutex);

		std::wstring sNew(sExpr);
		CCXMLString simbolsIn[]  = {L"'"  , L"$", L"==", L""};
		CCXMLString simbolsOut[] = {L"\"" , L"" , L"=" , L""};
		Normalization(sNew,simbolsIn,simbolsOut);

		//CLocker lock(m_rCS);
		EXCEPINFO ei;
		HRESULT hr = m_spASP->ParseScriptText(sNew.c_str(), NULL,
			NULL, NULL, 0, 0, 0L, NULL, &ei);
		if (SUCCEEDED(hr))
			hr = m_spAS->SetScriptState(SCRIPTSTATE_CONNECTED);
		return hr;
	}	

	void CScriptEngine::Assign(const std::wstring& sExpr, const CComVariant& value)
	{
		//CCXMLString sExpr2(L"TypeName(" + sExpr + L")");
		////CComVariant v; ExecStatement(L"x = 'sdfsdf'");
		////v = ExprEval(L"TypeName(x)");
		//// Generate random variable name
		//CCXMLString sVarName = format_wstring(L"tempvar%u", GetTickCount());
		//// Create temp variable
		//VAR& var = m_pNameTbl->AddVar(sVarName, value, true);

		//// Make assignment
		//ExecStatement(sExpr + L"=" + sVarName);
		////CComVariant v = ExprEval(sExpr2);
		//// Delete temp variable
		//m_pNameTbl->DelVar(sVarName);
		//ExprEval(L"evt.connection.remote == 'tel:+18315551234'");
	}

	bool CScriptEngine::ExprEvalToBool(const std::wstring& sExpr)
	{
		return ExprEvalToType(sExpr, VT_BOOL).boolVal == VARIANT_TRUE;
	}

	std::wstring CScriptEngine::ExprEvalToStr(const std::wstring& sExpr)
	{
		return ExprEvalToType(sExpr, VT_BSTR).bstrVal;
	}

	CComVariant CScriptEngine::ExprEvalToType(const std::wstring& sExpr, VARTYPE vt)
	{
		CComVariant v = ExprEval(sExpr);
		HRESULT hr = v.ChangeType(vt);
		if (FAILED(hr))
		{
			LPCWSTR pwszType = L"Unknown type";
			switch (vt)
			{
			case VT_I1: 
			case VT_I2: 
			case VT_I4: 
			case VT_I8: 
			case VT_INT:
			case VT_UI1:
			case VT_UI2:
			case VT_UI4:
			case VT_UI8:
			case VT_UINT: 
				pwszType = L"Integer";
				break;
			case VT_BOOL: 
				pwszType = L"Boolean";
				break;
			case VT_BSTR: 
				pwszType = L"String";
				break;
			}
			//throw format_wstring(L"Cannot evaluate expression '%s' to %s", sExpr.c_str(), pwszType);
			throw std::runtime_error(
				std::string("Cannot evaluate expression '") + wtos(sExpr) + "' to" + wtos(pwszType));
		}
		return v;
	}

	CComVariant CScriptEngine::ExprEvalToVariant(const std::wstring & sExpr)
	{
		return ExprEval(sExpr);
	}

	void CScriptEngine::ExecScript(const std::wstring& sExpr)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CScriptEngine::ExecScript", m_mutex);

		std::wstring sNew(sExpr);
		CCXMLString simbolsIn[]  = {L"$", L"==", L""};
		CCXMLString simbolsOut[] = {L"" , L"=" , L""};
		Normalization(sNew,simbolsIn,simbolsOut);

		//CLocker lock(m_rCS);
		EXCEPINFO ei;
		HRESULT hr = m_spASP->ParseScriptText(sNew.c_str(), NULL, 
			NULL, NULL, 0, 0, 0L, NULL, &ei);
		if (SUCCEEDED(hr))
			hr = m_spAS->SetScriptState(SCRIPTSTATE_CONNECTED);

	}

	CCXMLString CScriptEngine::ScriptSource()const 
	{
		return m_scriptSource; 
	}

}
