/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "StdAfx.h"
//#include "HackedScriptControl.h"
//#include <memory>
//#include <boost/scoped_array.hpp>
//
//#pragma comment(lib, "Version.lib")
//
//
//SVHacking::CWin32UserModePatcher CHackedScriptControl::m_GetVarPatcher;
//SVHacking::CWin32UserModePatcher CHackedScriptControl::m_AssignVarPatcher;
//
//DWORD CHackedScriptControl::m_dwTlsIndexThis = ::TlsAlloc();
//DWORD CHackedScriptControl::m_dwTlsIndexRegs = ::TlsAlloc();
//
//CHackedScriptControl::CHackedScriptControl()
//{
//	::TlsSetValue(m_dwTlsIndexThis, this);
//	::TlsSetValue(m_dwTlsIndexRegs, new DEBUG_NEW_PLACEMENT SAVED_REGS);
//	// Ensure this fucking dll loaded before hacking it
//	m_hVBSDll = ::LoadLibraryW(L"VBScript.dll");
//}
//
//CHackedScriptControl::~CHackedScriptControl()
//{
//	::FreeLibrary(m_hVBSDll);
//	delete (SAVED_REGS*)::TlsGetValue(m_dwTlsIndexRegs);
//	::TlsSetValue(m_dwTlsIndexThis, NULL);
//	::TlsSetValue(m_dwTlsIndexRegs, NULL);
//}
//
//HRESULT CHackedScriptControl::Create()
//{
//	if (!IsHackPossible())
//	{
//		// MessageText:
//		//
//		//  Invalid version number in application file
//		//
//		return COMADMIN_E_APP_FILE_VERSION;
//	}
//
//	HRESULT hr = CoCreateInstance(L"MSScriptControl.ScriptControl");
//	
//	if (FAILED(hr))
//	{
//		return hr;
//	}
//
//	if (!HackScriptControl())
//	{
//		return DISP_E_EXCEPTION;
//	}
//
//	return S_OK;
//}
//
//void CHackedScriptControl::Destroy()
//{
//	m_AssignVarPatcher.Restore();
//	m_GetVarPatcher.Restore();
//	Release();
//}
//
//std::wstring CHackedScriptControl::GetScriptControlVer()
//{
//	DWORD dwZero = 0;
//	DWORD dwSize = ::GetFileVersionInfoSizeW(L"VBScript.dll", &dwZero);
//	//std::auto_ptr<BYTE> pBlock(new BYTE[dwSize]);
//	boost::scoped_array<BYTE> pBlock(new DEBUG_NEW_PLACEMENT BYTE[dwSize]);
//
//	if (::GetFileVersionInfoW(
//		L"VBScript.dll", 0, dwSize, (LPVOID)pBlock.get()) == 0)
//	{
//		return std::wstring();
//	}
//
//	LPWORD pCP = NULL;
//	UINT VarSize = 0;
//	if (::VerQueryValueW(
//		pBlock.get(), L"\\VarFileInfo\\Translation", 
//		(LPVOID*)&pCP, &VarSize) == 0)
//	{
//		return std::wstring();
//	}
//
//	WCHAR buf[256] = {0};
//	wsprintf(buf, L"\\StringFileInfo\\%04X%04X\\FileVersion", pCP[0], pCP[1]);
//
//	LPWSTR pText = NULL;
//	if (!VerQueryValueW(pBlock.get(), buf, (LPVOID*)&pText, (PUINT)&dwSize))
//	{
//		return std::wstring();
//	}
//
//	return std::wstring(pText);
//}
//
//bool CHackedScriptControl::IsHackPossible()
//{
//	return GetScriptControlVer() == L"5.6.0.8820";
//}
//
//__declspec(naked)
//void __stdcall NakedGetAddrHelper()
//{
//	__asm
//	{
//		push	edi
//		push	esi
//		push	edx
//		push	ecx
//		push	ebx
//		push	CHackedScriptControl::m_dwTlsIndexRegs
//		mov		eax, TlsGetValue
//		call	eax
//		pop		dword ptr [eax + 0]
//		pop		dword ptr [eax + 4]
//		pop		dword ptr [eax + 8]
//		pop		dword ptr [eax + 12]
//		pop		dword ptr [eax + 16]
//		jmp		CHackedScriptControl::OnGetAddrHelper
//	}
//}
//
//__declspec(naked)
//void __stdcall NakedAssignVarHelper()
//{
//	__asm
//	{
//		push	edi
//		push	esi
//		push	edx
//		push	ecx
//		push	ebx
//		push	CHackedScriptControl::m_dwTlsIndexRegs
//		mov		eax, TlsGetValue
//		call	eax
//		pop		dword ptr [eax + 0]
//		pop		dword ptr [eax + 4]
//		pop		dword ptr [eax + 8]
//		pop		dword ptr [eax + 12]
//		pop		dword ptr [eax + 16]
//		jmp		CHackedScriptControl::OnAssignVarHelper
//	}
//}
//
//HRESULT __stdcall CHackedScriptControl::OnGetAddrHelper(SYM* pSym, VAR* pVar)
//{
//	CHackedScriptControl* pHSC = (CHackedScriptControl*)::TlsGetValue(m_dwTlsIndexThis);
//	if (pHSC != NULL)
//	{
//		try
//		{
//			return pHSC->OnGetAddr(pSym, pVar);
//		}
//		catch (...)
//		{
//		}
//	}
//	return E_FAIL;
//}
//
//HRESULT __stdcall CHackedScriptControl::OnAssignVarHelper(CHackedScriptControl::CSession* pSes,
//														  CHackedScriptControl::VAR* pVarSrc, 
//														  CHackedScriptControl::VAR* pVarDst, 
//														  DWORD dw)
//{
//	CHackedScriptControl* pHSC = (CHackedScriptControl*)::TlsGetValue(m_dwTlsIndexThis);
//	if (pHSC != NULL)
//	{
//		try
//		{
//			return pHSC->OnAssignVar(pSes, pVarSrc, pVarDst, dw);
//		}
//		catch (...)
//		{
//		}
//	}
//	return E_FAIL;
//}
//
//HRESULT CHackedScriptControl::CallOldGetAddr(SYM* pSym, VAR* pVar)
//{
//	HRESULT hr;
//	__asm
//	{
//		push	ebx
//		push	ecx
//		push	edx
//		push	esi
//		push	edi
//		push	CHackedScriptControl::m_dwTlsIndexRegs
//		mov		eax, TlsGetValue
//		call	eax
//		mov		ebx, [eax + 0]
//		mov		ecx, [eax + 4]
//		mov		edx, [eax + 8]
//		mov		esi, [eax + 12]
//		mov		edi, [eax + 16]
//		push	pVar
//		push	pSym
//		mov		eax, this
//		add		eax, m_pGetAddr
//		call	[eax]
//		pop		edi
//		pop		esi
//		pop		edx
//		pop		ecx
//		pop		ebx
//		mov		hr, eax
//	}
//	return hr;
//}
//
//HRESULT CHackedScriptControl::CallOldAssignVar(CSession* pSes, VAR* pVarSrc, VAR* pVarDst, DWORD dw)
//{
//	HRESULT hr;
//	__asm
//	{
//		push	ebx
//		push	ecx
//		push	edx
//		push	esi
//		push	edi
//		push	CHackedScriptControl::m_dwTlsIndexRegs
//		mov		eax, TlsGetValue
//		call	eax
//		mov		ebx, [eax + 0]
//		mov		ecx, [eax + 4]
//		mov		edx, [eax + 8]
//		mov		esi, [eax + 12]
//		mov		edi, [eax + 16]
//		push	dw
//		push	pVarDst
//		push	pVarSrc
//		push	pSes
//		mov		eax, this
//		add		eax, m_pAssignVar
//		call	[eax]
//		pop		edi
//		pop		esi
//		pop		edx
//		pop		ecx
//		pop		ebx
//		mov		hr, eax
//	}
//	return hr;
//}
//
//bool CHackedScriptControl::HackScriptControl()
//{
//	try
//	{
//		DWORD dwModBase = (DWORD)m_hVBSDll;
//		// HRESULT NameTbl::GetAdr(struct SYM *,class VAR *)
//		m_pGetAddr = (PGETADDRFN)m_GetVarPatcher.Patch((LPVOID)(dwModBase + 0x00003B4D), &NakedGetAddrHelper);
//		// HRESULT AssignVar(class CSession *,class VAR *,class VAR *,unsigned long)
//		m_pAssignVar = (PASSIGNVARFN)m_AssignVarPatcher.Patch((LPVOID)(dwModBase + 0x00005FD3), &NakedAssignVarHelper);
//	}
//	catch (...)
//	{
//		return false;
//	}
//
//	return (m_pGetAddr != NULL && m_pAssignVar != NULL);
//}
