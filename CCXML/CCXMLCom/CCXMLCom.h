

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Mon Feb 25 23:21:19 2019
 */
/* Compiler settings for CCXMLCom.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CCXMLCom_h__
#define __CCXMLCom_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICCXMLComInterface_FWD_DEFINED__
#define __ICCXMLComInterface_FWD_DEFINED__
typedef interface ICCXMLComInterface ICCXMLComInterface;
#endif 	/* __ICCXMLComInterface_FWD_DEFINED__ */


#ifndef __INameTable_FWD_DEFINED__
#define __INameTable_FWD_DEFINED__
typedef interface INameTable INameTable;
#endif 	/* __INameTable_FWD_DEFINED__ */


#ifndef __CCXMLComInterface_FWD_DEFINED__
#define __CCXMLComInterface_FWD_DEFINED__

#ifdef __cplusplus
typedef class CCXMLComInterface CCXMLComInterface;
#else
typedef struct CCXMLComInterface CCXMLComInterface;
#endif /* __cplusplus */

#endif 	/* __CCXMLComInterface_FWD_DEFINED__ */


#ifndef __NameTable_FWD_DEFINED__
#define __NameTable_FWD_DEFINED__

#ifdef __cplusplus
typedef class NameTable NameTable;
#else
typedef struct NameTable NameTable;
#endif /* __cplusplus */

#endif 	/* __NameTable_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ICCXMLComInterface_INTERFACE_DEFINED__
#define __ICCXMLComInterface_INTERFACE_DEFINED__

/* interface ICCXMLComInterface */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_ICCXMLComInterface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0AB437C8-4091-4AB4-99AD-5D64A796DF6D")
    ICCXMLComInterface : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Invoke( 
            DISPID dispidMember,
            REFIID riid,
            LCID lcid,
            WORD wFlags,
            DISPPARAMS *pdispparams,
            VARIANT *pvarResult,
            EXCEPINFO *pexcepinfo,
            UINT *puArgErr) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICCXMLComInterfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICCXMLComInterface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICCXMLComInterface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICCXMLComInterface * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICCXMLComInterface * This,
            DISPID dispidMember,
            REFIID riid,
            LCID lcid,
            WORD wFlags,
            DISPPARAMS *pdispparams,
            VARIANT *pvarResult,
            EXCEPINFO *pexcepinfo,
            UINT *puArgErr);
        
        END_INTERFACE
    } ICCXMLComInterfaceVtbl;

    interface ICCXMLComInterface
    {
        CONST_VTBL struct ICCXMLComInterfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICCXMLComInterface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICCXMLComInterface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICCXMLComInterface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICCXMLComInterface_Invoke(This,dispidMember,riid,lcid,wFlags,pdispparams,pvarResult,pexcepinfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispidMember,riid,lcid,wFlags,pdispparams,pvarResult,pexcepinfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICCXMLComInterface_INTERFACE_DEFINED__ */


#ifndef __INameTable_INTERFACE_DEFINED__
#define __INameTable_INTERFACE_DEFINED__

/* interface INameTable */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INameTable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("57393A84-6249-4E26-82A8-0E339A583683")
    INameTable : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetIDsOfNames( 
            REFIID riid,
            LPOLESTR *rgszNames,
            UINT cNames,
            LCID lcid,
            DISPID *rgDispId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Invoke( 
            DISPID dispIdMember,
            REFIID riid,
            LCID lcid,
            WORD wFlags,
            DISPPARAMS *pDispParams,
            VARIANT *pVarResult,
            EXCEPINFO *pExcepInfo,
            UINT *puArgErr) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INameTableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INameTable * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INameTable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INameTable * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INameTable * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INameTable * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INameTable * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INameTable * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INameTable * This,
            REFIID riid,
            LPOLESTR *rgszNames,
            UINT cNames,
            LCID lcid,
            DISPID *rgDispId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INameTable * This,
            DISPID dispIdMember,
            REFIID riid,
            LCID lcid,
            WORD wFlags,
            DISPPARAMS *pDispParams,
            VARIANT *pVarResult,
            EXCEPINFO *pExcepInfo,
            UINT *puArgErr);
        
        END_INTERFACE
    } INameTableVtbl;

    interface INameTable
    {
        CONST_VTBL struct INameTableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INameTable_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INameTable_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INameTable_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INameTable_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INameTable_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INameTable_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INameTable_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INameTable_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INameTable_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INameTable_INTERFACE_DEFINED__ */



#ifndef __CCXMLComLib_LIBRARY_DEFINED__
#define __CCXMLComLib_LIBRARY_DEFINED__

/* library CCXMLComLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_CCXMLComLib;

EXTERN_C const CLSID CLSID_CCXMLComInterface;

#ifdef __cplusplus

class DECLSPEC_UUID("43AD8055-B10A-4E70-B743-E2F3EC588F2A")
CCXMLComInterface;
#endif

EXTERN_C const CLSID CLSID_NameTable;

#ifdef __cplusplus

class DECLSPEC_UUID("D783F64E-AF9C-47D3-9526-577221E10457")
NameTable;
#endif
#endif /* __CCXMLComLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


