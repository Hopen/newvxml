///************************************************************************/
///* Name     : CCXMLCom\CCXMLDebug.cpp                                   */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-IT                                                  */
///* Date     : 21 Nov 2011                                               */
///************************************************************************/
//
#include "stdafx.h"
#include <string>
#include "CCXMLDebug.h"
#include "sv_strutils.h"

namespace CCXML
{

	//implementation
	CCXMLDebugBeholder::CCXMLDebugBeholder(IEngineCallback* _callback, IEngineVM* _engine): 
        IDebuger(_callback,_engine),
		//m_nCounter(0),
		m_ScriptoGenAddress(0),
		//m_pSession(NULL),
		m_debugflag(false),
		m_forcebreak(false),
		m_bWaitForRun(true)
	{
		m_evtReady.Create();
		m_evtReady.Reset();
		m_evtRun.Create();
		m_evtRun.Reset();
	};

	CCXMLDebugBeholder::~CCXMLDebugBeholder()
	{
		CEngineLog log;
		log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml\\ccxml_%y-%m-%d.log");
		log.SetSID(GetGlobalScriptId());

		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"Debuger down");

		//m_pSession.reset();
		m_evtReady.Close();
		m_evtRun.Close();
	}

	void CCXMLDebugBeholder::Add()
	{
		SessionPtr2 pSession = m_pSession.lock();
		pSession->SetState(VMS_WAITING/*VMS_BUSY*/);
		//++m_nCounter;
		m_evtReady.Set();
	}
	void CCXMLDebugBeholder::Release()
	{
		//if (m_nCounter)
			//--m_nCounter;
	}

	void CCXMLDebugBeholder::WaitOffline(unsigned int _line/*, unsigned int _count*/ /* = 1 */)
	{
		//for (unsigned int i=0;i<_count;++i)
		//{
			//if (m_nCounter)
				//Release();
			//else
			//{
		SessionPtr2 pSession = m_pSession.lock();
				pSession->ChangeState/*SetState*/(VMS_READY);
				::WaitForSingleObject((HANDLE)m_evtReady,INFINITE);
				pSession->ChangeState/*SetState*/(VMS_WAITING/*VMS_BUSY*/);
				m_evtReady.Reset();
				//Release();
			//}
		//}
	}

	void CCXMLDebugBeholder::WaitOnline(unsigned int _line/*, unsigned int _count*/ /* = 1 */)
	{
		BreakQueue::const_iterator cit = std::find_if(m_BreakStack.begin(),
														m_BreakStack.end(),
														CBreakPointByLineFinder(_line));
		SessionPtr2 pSession = m_pSession.lock();
		if (( cit!=m_BreakStack.end()) /*&& 
			(cit->condition.empty || m_pSession->m_ExecContext.pEng->ExprEvalToBool(cit->condition))*/)
		{
				CMessage BreakMsg(L"Break");
				BreakMsg[L"File"] = cit->filename;
				BreakMsg[L"Line"] = static_cast<int>(cit->line);

				SendToScriptoGen(BreakMsg);
				pSession->ChangeState/*SetState*/(VMS_WAITING);

				HANDLE hEvents [2] = {(HANDLE)m_evtReady, (HANDLE)m_evtRun};
				::WaitForMultipleObjects(2, hEvents,FALSE, INFINITE);
				//m_evtRun.Reset();
				m_evtReady.Reset();
		}
		else if (m_forcebreak)
		{
			m_forcebreak = false;
			CMessage BreakMsg(L"Break");
			BreakMsg[L"File"] = m_BreakStack.begin()->filename;
			BreakMsg[L"Line"] = static_cast<int>(_line);

			SendToScriptoGen(BreakMsg);
			pSession->ChangeState/*SetState*/(VMS_WAITING);
			HANDLE hEvents [2] = {(HANDLE)m_evtReady, (HANDLE)m_evtRun};
			::WaitForMultipleObjects(2, hEvents,FALSE, INFINITE);
			//m_evtRun.Reset();
			m_evtReady.Reset();
		}
		else
		{
			if (m_bWaitForRun)
			{
				::WaitForSingleObject((HANDLE)m_evtRun,INFINITE);
				m_bWaitForRun = false;
			}
			pSession->ChangeState/*SetState*/(VMS_READY);
			//HANDLE hEvents [2] = {(HANDLE)m_evtReady, (HANDLE)m_evtRun};
			//::WaitForMultipleObjects(2, hEvents,TRUE, INFINITE);
			::WaitForSingleObject((HANDLE)m_evtReady,INFINITE);
			pSession->ChangeState/*SetState*/(VMS_WAITING/*VMS_BUSY*/);
			m_evtReady.Reset();
		}
			
		//for (unsigned int i=0;i<_count;++i)
		//{
			//if (m_nCounter)
				//Release();
			//else
			//{
				//::WaitForSingleObject((HANDLE)m_evt,INFINITE);
				//m_pSession->SetState(VMS_READY);
				//m_evt.Reset();
				//Release();
			//}
		//}
	}

	void CCXMLDebugBeholder::Wait(unsigned int _line/*, unsigned int _count*//* = 1*/)
	{
		m_debugflag?WaitOnline(_line/*, _count*/):WaitOffline(_line/*, _count*/);
	}

	void CCXMLDebugBeholder::Increase(/*unsigned int _count*//* = 1*/)
	{
		//for (unsigned int i=0;i<_count;++i)
		//{
			Add();
		//}
	}

	void CCXMLDebugBeholder::SetAuxEvent(const CMessage& msg, bool _in )
	{
		if (!m_ScriptoGenAddress || !m_debugflag)
			return;

		SessionPtr2 pSession = m_pSession.lock();
		if (msg == L"Run")
		{
			m_evtRun.Set();
			//m_pSession->SetState(VMS_READY);
		}
		else if (msg == L"Break")
		{
			pSession->ChangeState/*SetState*/(VMS_WAITING);
		}
		else if (msg == L"SetBreak")
		{
			TBreakPoint point;
			if (const CParam* param = msg.ParamByName(L"File"))
				point.filename = param->AsString();
			if (const CParam* param = msg.ParamByName(L"Line"))
				point.line = param->AsUInt();
			if (const CParam* param = msg.ParamByName(L"Condition"))
				point.condition = param->AsString();

			if (point.line)
			{
				BreakQueue::const_iterator cit = std::find_if(m_BreakStack.begin(),m_BreakStack.end(),CBreakPointByLineFinder(point.line));
				if(cit == m_BreakStack.end())
					m_BreakStack.push_back(point);
				else
					m_BreakStack.erase(cit);
			}
		}
		else if (msg == L"StepInto")
		{

		}
		else if (msg == L"StepOver")
		{
			m_forcebreak = true;
			pSession->ChangeState/*SetState*/(VMS_READY);
		}
		else if (msg == L"StepOut")
		{

		}
		else if (msg == L"GetSymInfo")
		{
			CMessage SymInfoMsg(L"SymInfo");

			int k = 0;
			while (true)
			{
				std::wstring sParamName = L"SymName" + std::to_wstring(k);
				if (!msg.IsParam(sParamName.c_str()))
				{
					break;
				}
				std::wstring sSymName = msg[sParamName.c_str()]; 
				std::wstring sSymInfo = GetSymInfo(sSymName);
				SymInfoMsg[sParamName.c_str()] = sSymName.c_str();
				SymInfoMsg[(std::wstring(L"Text") + std::to_wstring(k)).c_str()] = sSymInfo.c_str();
				k++;
			}

			//int k = 0;
			//WCHAR wszFmt[MAX_PATH];

			//while (true)
			//{
			//	ZeroMemory(&wszFmt, sizeof(wszFmt));
			//	_snwprintf(wszFmt, MAX_PATH, L"SymName%d", k);
			//	if (!msg.IsParam(wszFmt))
			//	{
			//		break;
			//	}
			//	std::wstring sSymName = msg[wszFmt]; 
			//	std::wstring sSymInfo = GetSymInfo(sSymName);
			//	msg[wszFmt] = sSymName.c_str();
			//	ZeroMemory(&wszFmt, sizeof(wszFmt));
			//	_snwprintf(wszFmt, MAX_PATH, L"Text%d", k);
			//	msg[wszFmt] = sSymInfo.c_str();
			//	k++;
			//}
			SendToScriptoGen(SymInfoMsg);

		}
		//else if (msg == L"EndDebug")
		//{
		//	m_pSession->DoExit();
		//}
		else
		{
			CMessage DbgEventMsg(_in?L"DbgEventIn":L"DbgEventOut");
			CPackedMsg pMsg(msg);
			SYSTEMTIME st;
			FILETIME ft;
			GetLocalTime(&st);
			SystemTimeToFileTime(&st,&ft);
			DbgEventMsg[L"TimeStamp"] = *(__int64*)&ft;
			DbgEventMsg[L"EventData"].SetRawData(pMsg(),pMsg.Size());

			SendToScriptoGen(DbgEventMsg);
		}
	}

	void CCXMLDebugBeholder::Init( CMessage& init_msg, SessionPtr _session )
	{
		if (CParam * param = init_msg.ParamByName(L"ScrGenAddr"))
			m_ScriptoGenAddress = param->AsInt64();

		if (CParam* param = init_msg.ParamByName(L"DebugFlag"))
			m_debugflag = !(param->AsInt() == 0);

		m_pSession = _session;
	}

	void CCXMLDebugBeholder::Log( CCXMLString _log )
	{
		if (!m_ScriptoGenAddress || !m_debugflag)
			return;

		CMessage debug_msg(L"DbgLog");

		debug_msg[ L"Text" ] = _log.c_str();
		SendToScriptoGen(debug_msg);
	}

	void CCXMLDebugBeholder::SendToScriptoGen(CMessage& msg)const
	{
		msg[ L"AuxEvent"           ] = 1;
		msg[ L"DestinationAddress" ] = m_ScriptoGenAddress;

		//m_pSession->SendAuxMsg(msg);
		m_pCallBack->PostAuxMessage(CPackedMsg(msg)());

	}

	//void CCXMLDebugBeholder::SetEvent(PMESSAGEHEADER pMsgHeader)
	//{

	//}

	BOOL CCXMLDebugBeholder::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
	{
		CMessage msg(pMsgHeader);
		if (msg != L"DbgEventIn" && msg != L"DbgEventOut")
			SetAuxEvent(CMessage(pMsgHeader),false);

		if (msg == L"DROP_CALL")
			EndScript(L"DROP_CALL incoming", 0);

		return m_pCallBack->PostAuxMessage(pMsgHeader);
	}

	std::wstring CCXMLDebugBeholder::GetSymInfo(const std::wstring& _name)const
	{
		SessionPtr2 pSession = m_pSession.lock();
		if (pSession->GetState()!=VMS_READY)
			return PROCESSNOTAVAILABLE_DBGMSG;
		return pSession->GetVarValue(_name);
	}

	void CCXMLDebugBeholder::EndScript(const CCXMLString& _reason, const int& _code)const
	{
		if (!m_debugflag) // HotFix 
			return;

		CMessage msg(L"EndDebug");
		msg[L"Code"] = _code;
		msg[L"Reason"] = _reason.c_str();
		SendToScriptoGen(msg);
	}
}
/******************************* eof *************************************/