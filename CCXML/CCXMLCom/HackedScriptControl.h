/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

//#include <string>
//#include <atlbase.h>
//#include "SV_Hacking.h"
//
//#import "C:\Windows\system32\msscript.ocx"
//using namespace MSScriptControl;
//
//class CHackedScriptControl : public CComPtr<IScriptControl>
//{
//protected:
//	// Structure, created by PUSHAD instruction
//	struct SAVED_REGS
//	{
//		DWORD Ebx;
//		DWORD Ecx;
//		DWORD Edx;
//		DWORD Esi;
//		DWORD Edi;
//	};
//
//	// Reverse-engineered structures from VBSCRIPT.DLL
//
//	struct SYM
//	{
//		BSTR	bstrSymName;
//		DWORD	dwNameLen;
//	};
//
//	struct VAR : public VARIANT
//	{
//	};
//
//	class CSession
//	{
//	};
//
//private:
//	// Reverse-engineered functions typedefs
//	typedef HRESULT (__stdcall *PGETADDRFN)(SYM* pSym, VAR* pVar);
//	typedef HRESULT (__stdcall *PASSIGNVARFN)(CSession*, VAR* pVarSrc, VAR* pVarDst, DWORD);
//
//	// Code patchers
//	static SVHacking::CWin32UserModePatcher m_GetVarPatcher;
//	static SVHacking::CWin32UserModePatcher m_AssignVarPatcher;
//
//	// Original functions
//	PGETADDRFN m_pGetAddr;
//	PASSIGNVARFN m_pAssignVar;
//
//	std::wstring GetScriptControlVer();
//	bool IsHackPossible();
//	bool HackScriptControl();
//
//	static HRESULT __stdcall OnGetAddrHelper(SYM* pSym, VAR* pVar);
//	static HRESULT __stdcall OnAssignVarHelper(CSession* pSes, VAR* pVarSrc, VAR* pVarDst, DWORD dw);
//
//	static DWORD m_dwTlsIndexThis;
//	static DWORD m_dwTlsIndexRegs;
//
//	HMODULE m_hVBSDll;
//
//protected:
//	HRESULT CallOldGetAddr(SYM* pSym, VAR* pVar);
//	HRESULT CallOldAssignVar(CSession* pSes, VAR* pVarSrc, VAR* pVarDst, DWORD dw);
//
//public:
//	CHackedScriptControl();
//	~CHackedScriptControl();
//	HRESULT Create();
//	void Destroy();
//	// HRESULT NameTbl::GetAdr(struct SYM *,class VAR *)
//	virtual HRESULT OnGetAddr(SYM* pSym, VAR* pVar)
//	{
//		return CallOldGetAddr(pSym, pVar);
//	}
//	// HRESULT AssignVar(class CSession *,class VAR *,class VAR *,unsigned long)
//	virtual HRESULT OnAssignVar(CSession* pSes, VAR* pVarDst, VAR* pVarSrc, DWORD dw)
//	{
//		return CallOldAssignVar(pSes, pVarDst, pVarSrc, dw);
//	}
//};
