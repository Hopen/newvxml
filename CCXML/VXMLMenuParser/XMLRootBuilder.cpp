/************************************************************************/
/* Name     : VXMLMenuParser\XMLRootBuilder.cpp                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 21 Jul 2014                                               */
/************************************************************************/


#include "stdafx.h"
#include "XMLRootBuilder.h"
#include "EngineLog.h"
#include "FolderMonitor.h"
#include "sv_utils.h"
#include "EngineConfig.h"

const CAtlString DIGITS = L"0123456789";

CRootDocument::CRootDocument()
{

}

//void CRootDocument::Init(const std::wstring& sSchemaFile)
//{
//
//}

std::wstring CRootDocument::getValidateSchema()const
{
	return std::wstring();
}

bool CRootDocument::validateName(const ISATXMLElement& xmlElement)
{
	if (CAtlStringW(xmlElement.GetName().c_str()).CompareNoCase(L"item"))
		return false;

	//CAtlString menuName = xmlElement.GetAttrVal(L"item_name").c_str();
	//m_name = menuName;

	//std::wstring witem_root, wcode;
	//xmlElement.GetAttrVal(L"item_root", witem_root);
	//xmlElement.GetAttrVal(L"code", wcode);

	//m_root = witem_root.c_str();
	//m_code = wcode.c_str();
	//m_info = menuName;

	return true;
}

bool CRootDocument::validateParams(const ISATXMLElement& xmlElement, const std::wstring& mainRoot, const std::wstring& mainCode)
{
	CAtlString menuName = xmlElement.GetAttrVal(L"item_name").c_str();
	m_name = menuName;

	std::wstring witem_root, wcode;
	xmlElement.GetAttrVal(L"item_root", witem_root);
	xmlElement.GetAttrVal(L"code", wcode);

	m_root = witem_root.c_str();
	m_code = wcode.c_str();
	m_info = menuName;

	return true;
}


std::wstring CRootDocument::getParentCode(/*const std::wstring& _top*/)const
{
	//return _top;
	if (m_parentCodes.size())
		return m_parentCodes.top();

	return std::wstring();
}

void CRootDocument::onXMLStart(const ISATXMLElement& xmlElement)
{
	m_xml_elements.push(xmlElement);
	m_parentCodes.push(getCode());
}
void CRootDocument::onXMLEnd(const ISATXMLElement& xmlElement)
{
	if (validateName(xmlElement) && m_xml_elements.size())
	{
		m_parentCodes.pop();
		m_xml_elements.pop();
	}
}

std::wstring CRootDocument::getCode2()const
{
	return IXMLDocumentMy::getCode();
}

//bool CRootDocument::isParentCodeaAvilable(const std::wstring& parentCode)
//{
//	if (parentCode.length() == 1)
//		return false;
//	return true;
//}

///////////////////////////////////////////////////////////////////////////////////

CVXMLDocument::CVXMLDocument()
{

}

//void CVXMLDocument::Init(const std::wstring& sSchemaFile)
//{
//
//}

std::wstring CVXMLDocument::getValidateSchema()const
{
	return SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";
}

bool CVXMLDocument::validateName(const ISATXMLElement& xmlElement)
{
	if (CAtlStringW(xmlElement.GetName().c_str()).CompareNoCase(L"menu"))
		return false;

	//CAtlString menuName = xmlElement.GetAttrVal(L"id").c_str();

	//m_name = menuName; 

	//int pos = menuName.Find(mainRoot.c_str());
	//if (pos < 0)
	//	return false;

	//CAtlString root_tag = menuName.Left(pos + mainRoot.size());
	//CAtlString code = menuName.Right(menuName.GetLength() - (pos + mainRoot.size()));

	//// check code
	//if (0 == code.GetLength())
	//	return false;

	//for (int i = 0; i < code.GetLength(); ++i)
	//{
	//	if (DIGITS.Find(code[i]) < 0)
	//		return false;
	//}
	//
	//m_root = root_tag;
	//m_code = code;

	//xmlElement.GetAttrVal(L"info", m_info);
	//if (!m_info.length())
	//	xmlElement.GetAttrVal(L"comment", m_info);

	return true;
}

bool CVXMLDocument::validateParams(const ISATXMLElement& xmlElement, const std::wstring& mainRoot, const std::wstring& mainCode)
{
	m_name.clear();
	m_root.clear();
	m_code.clear();
	m_info.clear();

	CAtlString menuName = xmlElement.GetAttrVal(L"id").c_str();

	m_name = menuName;

	//int pos = menuName.Find(mainRoot.c_str());
	//if (pos < 0)
	//	return false;

	//CAtlString root_tag = menuName.Left(pos + mainRoot.size());
	//CAtlString code = menuName.Right(menuName.GetLength() - (pos + mainRoot.size()));

	// Compare format <root_tag>_<code>. Example: 0611_EKT_prepaid_1
	int pos = menuName.ReverseFind('_');
	CAtlString root_tag = menuName.Left(pos);
	CAtlString code = menuName.Right(menuName.GetLength() - (pos + 1));

	// check code
	if (0 == code.GetLength())
		return false;

	for (int i = 0; i < code.GetLength(); ++i)
	{
		if (DIGITS.Find(code[i]) < 0)
			return false;
	}

	m_root = root_tag;
	m_code = CAtlString(mainCode.c_str()) + code;

	m_info.clear();
	if (!xmlElement.GetAttrVal(L"info", m_info))
		xmlElement.GetAttrVal(L"comment", m_info);

	return true;
}

std::wstring CVXMLDocument::getParentCode(/*const std::wstring&*/ /*_top*/)const
{
	if (!m_code.length())
		return std::wstring(); // empty string

	std::wstring parent_code(m_code);
	parent_code = parent_code.erase(parent_code.length() - 1);

	return parent_code;

}

//
//bool CVXMLDocument::isParentCodeaAvilable(const std::wstring& parentCode)
//{
//	return true;
//}
//

void CVXMLDocument::onXMLStart(const ISATXMLElement& xmlElement)
{

}
void CVXMLDocument::onXMLEnd(const ISATXMLElement& xmlElement)
{

}

std::wstring CVXMLDocument::getCode2()const
{
	return IXMLDocumentMy::getRoot() + IXMLDocumentMy::getCode();
}





//
//
////const CAtlString DIGITS = L"0123456789";
//
//CXMLRootBuilder::CXMLRootBuilder()
//{
//	Init(std::wstring());
//}
//
//
//CXMLRootBuilder::~CXMLRootBuilder()
//{
//}
//
//
//void CXMLRootBuilder::OnXmlStartElement(const ISATXMLElement& xmlElement)
//{
//	// menu cannot consider other menu into theyself 
//
//	CAtlString menuName, root_tag, code;
//	std::wstring info;
//
//	//// Waiting for <menu> tag
//	//if (!CAtlStringW(xmlElement.GetName().c_str()).CompareNoCase(L"menu")) // vxml menu
//	//{
//	//	// Get name menu
//	//	menuName = xmlElement.GetAttrVal(L"id").c_str();
//	//	m_pLog->Log(__FUNCTIONW__, _T("OnXmlStartElement: menu id=\"%s\""), menuName);
//
//	//	// Compare format <root_tag>_<code>. Example: 0611_EKT_prepaid_1
//	//	int pos = menuName.ReverseFind('_');
//	//	if (pos < 0)
//	//		return;
//
//	//	root_tag = menuName.Left(pos);
//	//	code = menuName.Right(menuName.GetLength() - (pos + 1));
//
//	//	// check root
//	//	std::wstring main_root = m_root_item->root();
//	//	//if (root_tag.CompareNoCase(main_root.c_str())) 
//	//	if (root_tag.Find(main_root.c_str())<0)
//	//		return;
//
//	//	// check code
//	//	if (0 == code.GetLength())
//	//		return;
//
//	//	//DWORD dwCode = 0;
//	//	//swscanf(code, L"%u", &dwCode); // cannot protect from "20help"
//
//	//	//if (0 == dwCode && code.CompareNoCase(L"0"))
//	//	//	return;
//
//	//	for (int i = 0; i < code.GetLength(); ++i)
//	//	{
//	//		if (DIGITS.Find(code[i]) < 0)
//	//			return;
//	//	}
//
//	//	xmlElement.GetAttrVal(L"info", info);
//	//}
//	/*else*/ if (!CAtlStringW(xmlElement.GetName().c_str()).CompareNoCase(L"item")) // root menu
//	{
//		menuName = xmlElement.GetAttrVal(L"item_name").c_str();
//		m_pLog->Log(__FUNCTIONW__, _T("OnXmlStartElement: item name=\"%s\""), menuName);
//
//		std::wstring witem_root, wcode;
//		xmlElement.GetAttrVal(L"item_root", witem_root);
//		xmlElement.GetAttrVal(L"code", wcode);
//
//		root_tag = witem_root.c_str();
//		code = wcode.c_str();
//		info = menuName;
//	}
//	else
//	{
//		return; // skip other
//	}
//
//	// create new item
//	CItem::ItemPtr item(new CItem(std::wstring(menuName), std::wstring(root_tag), std::wstring(code), info));
//
//	//create note
//	if (m_pDoc)
//	{
//		MSXML2::IXMLDOMNodePtr pRecNode = NULL;
//		HRESULT hr = m_pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);
//
//		if (SUCCEEDED(hr) && pRecNode)
//		{
//			MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
//			elem->setAttribute(L"code", _variant_t(code));
//			elem->setAttribute(L"item_name", _variant_t(info.c_str()));
//			/*if (root_tag.GetLength())
//				elem->setAttribute(L"item_root", _variant_t(root_tag));*/
//
//			CComPtr <MSXML2::IXMLDOMNode> pComRecNode(pRecNode);
//			item->setNode(pComRecNode);
//		}
//	}
//
//
//	// check for already exist
//	CItem::ItemMap::iterator it = m_items.find(std::wstring(code));
//	if (it != m_items.end())
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Error: item (name=\"%s\") with code = \"%s\" is dublicated"), menuName, code);
//		return;
//	}
//
//	// insert item
//	m_items[std::wstring(code)] = item;
//
//	// insert to parent item
//	it = m_items.find(item->getParentCode());
//	if (it != m_items.end())
//	{
//		CItem::ItemPtr parentItem = it->second;
//		parentItem->AddChild(item);
//	}
//
//
//}
//
//void CXMLRootBuilder::OnXmlElementData(const std::wstring& elementData, int depth)
//{
//}
//
//void CXMLRootBuilder::OnXmlEndElement(const ISATXMLElement& xmlElement)
//{
//}
//
//void CXMLRootBuilder::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
//{
//	m_pLog->Log(__FUNCTIONW__, L"Error parsing XML document. Line %d, char %d, reason \"%s\"",
//		line, column, errorText.c_str());
//}
//
//bool CXMLRootBuilder::OnXmlAbortParse(const ISATXMLElement& xmlElement)
//{
//	return false;
//}
//
//
//bool CXMLRootBuilder::Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& out_xml)
//{
//	//bool retval = __super::Parse(_filename, _root, _code, out_xml);
//	//if (!retval)
//	return false;
//
//	//singleton_auto_pointer<CXMLFileBuilder> fileBuilder;
//	//singleton_auto_pointer<CFolderMonitor> watcher;
//
//	//CFolderMonitor::URLContainer fileList = watcher->getVXMLFileList();
//
//	//bool bAddNewNodes = false;
//	//for (CItem::ItemMap::iterator it = m_items.begin(); it != m_items.end(); ++it)
//	//{
//	//	// check for root_element
//	//	if (!it->second->root().length())
//	//		continue;
//
//	//	// check for file url with the same root_element
//	//	std::wstring vxml_url, src;
//	//	for (CFolderMonitor::URLContainer::const_iterator cit = fileList.begin(); cit != fileList.end(); ++cit)
//	//	{
//	//		vxml_url = *cit;
//	//		if (!fileBuilder->Parse(vxml_url, it->second->root(), it->second->code(), src))
//	//		{
//	//			m_pLog->Log(__FUNCTIONW__, _T("Error: failed to parse \"%s\""), vxml_url.c_str());
//	//			continue;
//	//		}
//
//
//	//		//if (!watcher->getFileByMask(it->second->root(), vxml_url))
//	//		//	continue;
//
//	//		//// try to parse it
//	//		//if (!fileBuilder->Parse(vxml_url, it->second->root(), it->second->code(), src))
//	//		//{
//	//		//	m_pLog->Log(__FUNCTIONW__, _T("Error: failed to parse \"%s\""), vxml_url.c_str());
//	//		//	continue;
//	//		//}
//
//	//		bAddNewNodes = true;
//	//		//concatinate 2 xml documents
//	//		CItem::ItemPtr parentRootItem = it->second;
//	//		CItem::ItemPtr childRootItem = fileBuilder->getRootItem();
//	//		CComPtr < MSXML2::IXMLDOMNode > child_root_node = childRootItem->getNote();
//	//		if (child_root_node)
//	//		{
//	//			//CComPtr < MSXML2::IXMLDOMNode > child_first_node = NULL;
//	//			CComPtr<MSXML2::IXMLDOMNodeList>	nodeList = NULL;
//	//			HRESULT hr = child_root_node->get_childNodes(&nodeList);
//	//			if (SUCCEEDED(hr) && nodeList)
//	//			{
//	//				CComPtr<MSXML2::IXMLDOMNode> inode = NULL;
//	//				while (SUCCEEDED(nodeList->get_item(0, &inode)) && inode)
//	//				{
//	//					parentRootItem->AddChild(inode);
//	//					inode.Detach();
//	//				}
//	//				//long nodesCount = 0; 
//	//				//nodeList->get_length(&nodesCount);
//
//	//				//CComPtr<MSXML2::IXMLDOMNode> inode = NULL;
//	//				//for (long i = 0; i < nodesCount; /*++i*/)
//	//				//{
//	//				//	nodeList->get_item(i, &inode);
//
//	//				//	_bstr_t xml;
//	//				//	inode->get_xml(&xml.GetBSTR());
//	//				//	std::wstring sXml(xml);
//
//	//				//	parentRootItem->AddChild(inode);
//
//	//				//	inode.Detach();
//	//				//}
//	//			}
//
//	//		}
//
//	//	}
//
//
//	//	//CComPtr < MSXML2::IXMLDOMNode > child_first_node = childRootItem->getFirstChildNode();
//	//	//CComPtr < MSXML2::IXMLDOMNode > parent_node = it->second->getNote();
//	//	//parent_node->appendChild(child_first_node, NULL);
//
//
//	//	//if (child_first_node)
//	//	//{
//	//	//	CItem::ItemPtr parentItem = it->second;
//	//	//	parentItem->AddChild(child_first_node);
//	//	//}
//	//}
//
//	//if (bAddNewNodes)
//	//{
//	//	CComPtr < MSXML2::IXMLDOMNode > child_first_node = m_root_item->getFirstChildNode();
//	//	if (child_first_node)
//	//	{
//	//		_bstr_t bstrXML;
//	//		child_first_node->get_xml(&bstrXML.GetBSTR());
//
//	//		out_xml = bstrXML.GetBSTR();
//	//	}
//	//}
//
//	//return retval;
//	////m_items.clear();
//	////m_root_item.reset(new CItem(L"root_item", L"", L"", L"")); // empty_code -> means that is root item
//	////m_items[L""] = m_root_item;
//
//	////// create new document
//	////CComPtr < MSXML2::IXMLDOMDocument > pDoc;
//	////HRESULT hr = pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
//	////if (FAILED(hr))
//	////{
//	////	m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
//	////	return false;
//	////}
//	////pDoc->put_async(VARIANT_FALSE);
//
//	////CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
//	////hr = pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='WINDOWS-1251'", &pPI);
//	////if (FAILED(hr))
//	////{
//	////	m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
//	////	return false;
//	////}
//	////pDoc->appendChild(pPI, NULL);
//
//	////m_pDoc = pDoc;
//
//	////// create root node
//	////MSXML2::IXMLDOMNodePtr rootPtr = NULL;// pRootNode->GetInterfacePtr();
//	////hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("root_item"), _T(""), &rootPtr);
//	////pDoc->appendChild(rootPtr, NULL);
//
//	////CComPtr <MSXML2::IXMLDOMNode> pRootNode(rootPtr);
//	////m_root_item->setNode(pRootNode);
//
//
//	////if (!m_xmlParser->ParseUrl(_filename))
//	////	return false;
//
//	////m_pDoc.Detach();
//
//	////_bstr_t bstrXML;
//	////pDoc->get_xml(&bstrXML.GetBSTR());
//
//	////std::wstring _dist = bstrXML.GetBSTR();
//
//
//	////return true;
//}
/******************************* eof *************************************/