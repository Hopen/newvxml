/************************************************************************/
/* Name     : VXMLMenuParser\ParserEngineConfig.h                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 10 Jul 2014                                               */
/************************************************************************/
#pragma once
#include "EngineConfig.h"

class CVXMLParserEngineConfig : public CEngineConfig <CVXMLParserEngineConfig>
{
public:
	std::wstring sRootXML;
	std::wstring sVXMLFolder;
	std::wstring sOutputURL;

protected:
	void LoadEngineConfig()
	{
		try
		{
			this->sRootXML = m_config.attr(L"//configuration/Folder/RootXML/Value");
			this->sVXMLFolder = m_config.attr(L"//configuration/Folder/VXMLFolder/Value");
			this->sOutputURL = m_config.attr(L"//configuration/Debug/OutputURL/Value");
		}
		catch (...)
		{
		}
	}

public:
	CVXMLParserEngineConfig()
	{
		try
		{
			LoadEngineConfig();
		}
		catch (...)
		{
		}
	}
	~CVXMLParserEngineConfig()
	{
	}
};