/************************************************************************/
/* Name     : VXMLMenuParser\FolderMonitor.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 14 Jul 2014                                               */
/************************************************************************/


#include "stdafx.h"
#include <vector>
//#include <fstream>
#include "boost\filesystem.hpp"
#include "FolderMonitor.h"
#include "ParserEngineConfig.h"
#include "EngineLog.h"
#include "XMLBuilder.h"
#include "XMLRootBuilder.h"

using namespace boost::filesystem;
const boost::posix_time::seconds CHECK_TIME(30);

//**************************************************//
//              CFolderMonitor::CTimer 
//**************************************************//


CFolderMonitor::CTimer::CTimer(boost::asio::io_service& _io, SimpleCallbackFunc _callback)
: m_strand(_io),
  m_timer(_io, CHECK_TIME),
  m_callBackFunc(_callback)
{
	m_timer.async_wait(m_strand.wrap(boost::bind(&CFolderMonitor::CTimer::Loop, this)));
}

void CFolderMonitor::CTimer::Loop()
{
	//if (CFolderMonitor::m_bNeedToUpdateFileList)
	m_callBackFunc();

	m_timer.expires_at(m_timer.expires_at() + CHECK_TIME);
	m_timer.async_wait(m_strand.wrap(boost::bind(&CFolderMonitor::CTimer::Loop, this)));

}

// init static members

bool CFolderMonitor::m_bNeedToUpdateFileList = false;
boost::mutex CFolderMonitor::m_mut;
CFolderMonitor::URLContainer CFolderMonitor::m_fileList;
CFolderMonitor::SimpleCallbackFunc CFolderMonitor::m_callback;

//**************************************************//
//                   CFolderMonitor 
//**************************************************//

CFolderMonitor::CFolderMonitor() 
{
	//m_pLog.reset(new CEngineLog());
	//m_pLog->Init(CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());

	updateVXMLList();

	m_changeFileConnector = m_monitor.AddModifyHandler(boost::bind(&CFolderMonitor::OnModify, this, _1, _2));
	m_monitor.AddWatchFolder(CVXMLParserEngineConfig::GetInstance()->sVXMLFolder.c_str());
}

CFolderMonitor::~CFolderMonitor()
{
}

//const CFolderMonitor::URLContainer& CFolderMonitor::getFileList()const
//{
//	boost::unique_lock<boost::mutex> lock(m_mut);
//	return fileList;
//}


bool CFolderMonitor::getFileByMask(const std::wstring _mask, std::wstring& out_url)
{
	boost::unique_lock<boost::mutex> lock(m_mut);
	URLContainer::const_iterator cit = std::find_if(m_fileList.begin(), m_fileList.end(), CFileFindHelper(_mask));
	if (cit != m_fileList.end())
	{
		out_url = *cit;
		return !!out_url.length();
	}

	return false;
}

//void CFolderMonitor::ParseIntoFile()
//{
//	std::wstring outputURL = CVXMLParserEngineConfig::GetInstance()->sOutputURL;
//	std::wstring xml;
//	Parse(xml);
//
//	if (!xml.length())
//		return;
//
//	std::ofstream xmlFile(outputURL);
//	xmlFile << xml;
//	xmlFile.close();
//}

/***************  HANDLERS *************/
void CFolderMonitor::OnRename(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName, LPCTSTR _lpszNewName)
{

}

void CFolderMonitor::OnCreate(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName)
{

}


void CFolderMonitor::OnModify(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName)
{
	m_bNeedToUpdateFileList = true;
	//getActualVXMLList();
}

void CFolderMonitor::OnDelete(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName)
{

}


void CFolderMonitor::updateVXMLList()
{
	boost::unique_lock<boost::mutex> lock(m_mut);

	boost::shared_ptr<CEngineLog>  pLog;
	pLog.reset(new CEngineLog());
	pLog->Init(CVXMLParserEngineConfig::GetInstance()->sLevel.c_str(), CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());


	m_fileList.clear();
	std::wstring folder = CVXMLParserEngineConfig::GetInstance()->sVXMLFolder;

	path p(folder);

	try
	{
		if (exists(p))    // does p actually exist?
		{
			if (is_regular_file(p))        // is p a regular file?   
			{
				//cout << p << " size is " << file_size(p) << '\n';
				pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("\"%s\" is regular file, cannot make update"), folder.c_str());
			}

			else if (is_directory(p))      // is p a directory?
			{
				//cout << p << " is a directory containing:\n";

				typedef std::vector<path> vec;             // store paths,
				vec v;                                // so we can sort them later

				std::copy(directory_iterator(p), directory_iterator(), std::back_inserter(v));

				sort(v.begin(), v.end());             // sort, since directory iteration
				// is not ordered on some file systems

				for (vec::const_iterator it(v.begin()); it != v.end(); ++it)
				{
					//cout << "   " << *it << '\n';
					if (is_regular_file(*it) && !CAtlString(L".vxml").CompareNoCase((*it).extension().c_str()))
					{
						m_fileList.push_back((*it).c_str());
						//if (!builder->Parse((*it).c_str(), newXML))
						//{
						//	m_pLog->Log(__FUNCTIONW__, _T("Error: failed to parse \"%s\""), (*it).c_str());
						//	//builder->GetXML(newXML);
						//}

						//postHTML here
						// ...
					}
				}
			}

			else
				pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("\"%s\" exists, but is neither a regular file nor a directory"), folder.c_str());
		}
		else
			pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("\"%s\" does not exist"), folder.c_str());
	}

	catch (const filesystem_error& ex)
	{
		pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception when modifed VXML document: \"%s\" "), ex.what());
	}
}

void CFolderMonitor::InitTimer(/*boost::asio::io_service& _io,*/ SimpleCallbackFunc _callback)
{
	m_callback = _callback;
	m_timer.reset(new CFolderMonitor::CTimer(m_io, OnTimerExpired));
	//m_timer.reset(new CFolderMonitor::CTimer(_io, &CFolderMonitor::OnTimerExpired2));
	boost::thread t(boost::bind(&boost::asio::io_service::run, &m_io));
}


void CFolderMonitor::OnTimerExpired()
{
	if (m_bNeedToUpdateFileList)
	{
		m_bNeedToUpdateFileList = false;
		updateVXMLList();
		m_callback();
	}
}

/******************************* eof *************************************/