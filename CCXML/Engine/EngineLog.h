/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������" �� ��� ������� �������� �.�.        */
/*                  ���� ���" � �� ����" ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "EvtModel.h"

#define BUF_SIZE			0x1000
#define BUF_LARGE_SIZE		0x10000

// ����������� ���������������� ������ ���� ��� ������

namespace enginelog
{
	enum LEVEL
	{
		LEVEL_ALL = 0,
		LEVEL_FINEST = 1,
		LEVEL_FINER = 2,
		LEVEL_FINE = 3,
		LEVEL_CONFIG = 4,
		LEVEL_INFO = 5,
		LEVEL_WARNING = 6,
		LEVEL_SEVERE = 7,
		LEVEL_OFF = 8
	};


}


class CEngineLog
{
private:
	class Locker
	{
	private:
		CRITICAL_SECTION m_CS;

		Locker();
		~Locker();

	public:
		static Locker& Instance();
		static void Lock();
		static void Unlock();
	};

public:
	enginelog::LEVEL GetLevel() const
	{
		return m_level;
	};

	ULONGLONG GetSID()const
	{
		return m_ullSID;
	}

private:
	enginelog::LEVEL translateLogLevel(LPCWSTR pwszLevel);

private:
	HANDLE		m_hFile;
	ULONGLONG	m_ullSID;
	DWORD		m_dwPID;
	DWORD		m_dwTID;
	WCHAR		m_wszBuf[BUF_SIZE];
	enginelog::LEVEL       m_level;
	bool		m_bExtraDate = false;

public:
	CEngineLog();
	~CEngineLog();
	bool Init(LPCWSTR pwszLevel, LPCWSTR pwszFormat, bool extraDate = false);
	bool Init(enginelog::LEVEL level, LPCWSTR pwszFormat, bool extraDate = false);
	void Destroy();
	void SetSID(ULONGLONG ullSID);
	void Log(enginelog::LEVEL level, LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...);
	void LogLarge(enginelog::LEVEL level, LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...);
	LPCWSTR DumpMessage(CMessage& msg);

protected:
	void LogV(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args);
	void LogVLarge(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args);

};


