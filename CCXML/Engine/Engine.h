/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#ifndef __Engine_h__
#define __Engine_h__

#include "rpc.h"
#include "rpcndr.h"

#include "windows.h"
#include "ole2.h"


class CMessage;

typedef class EngineVM EngineVM;

#include "oaidl.h"
#include "ocidl.h"

#ifndef __IEngineVM_INTERFACE_DEFINED__
#define __IEngineVM_INTERFACE_DEFINED__

class IEngineCallback;

EXTERN_C const IID		IID_IEngineVM;
EXTERN_C const IID		IID_IEngineComCallback;
EXTERN_C const CLSID	CLSID_EngineComCallback;

    MIDL_INTERFACE("2A6864ED-3C4B-40B8-9F61-2C79AE71EB48")
    IEngineVM : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Init(PMESSAGEHEADER pMsgHeader, IEngineCallback* pCallback); 
        virtual HRESULT STDMETHODCALLTYPE Destroy(); 
        virtual HRESULT STDMETHODCALLTYPE DoStep(void);     
        virtual HRESULT STDMETHODCALLTYPE GetState(void);  
        virtual HRESULT STDMETHODCALLTYPE SetEvent(PMESSAGEHEADER pMsgHeader);  
		virtual HRESULT STDMETHODCALLTYPE GetNextDoStepInterval(LPDWORD lpdwInterval);
    };
  
#endif

EXTERN_C const IID LIBID_ENGINELib;

EXTERN_C const CLSID CLSID_EngineVM;

class DECLSPEC_UUID("B2C5959B-4F91-4A5E-B899-0F5038DF984D") EngineVM;


enum VMStates
{
	VMS_ERROR = -1,		
	VMS_NULL,
	VMS_READY,
	VMS_WAITING,
	VMS_COM,
	VMS_ENDSCRIPT,
	VMS_BUSY
};

static std::wstring StateString(const VMStates state)
{
	switch (state)
	{
	case VMS_ERROR:
		return L"VMS_ERROR";
	case VMS_NULL:
		return L"VMS_NULL";
	case VMS_READY:
		return L"VMS_READY";
	case VMS_WAITING:
		return L"VMS_WAITING";
	case VMS_COM:
		return L"VMS_COM";
	case VMS_ENDSCRIPT:
		return L"VMS_ENDSCRIPT";
	case VMS_BUSY:
		return L"VMS_BUSY";
	default:
		return L"Unknown state";
	};

	return L"Unknown state";
}




class IEngineCallback
{
public:
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader) = 0;
	virtual BOOL WINAPI GetScriptName(BSTR* pbstrScriptName) = 0;
};

class IEngineComAsync
{
public:
	virtual bool IsWaitCompleted(LPMESSAGEHEADER pMsgHdr) = NULL;
	virtual bool Adjustable() = NULL;
	virtual void AdjustMsg(LPMESSAGEHEADER pMsgHdr, LPMESSAGEHEADER* ppOutMsgHdr) = NULL;
	virtual void Delete() = NULL;
};

MIDL_INTERFACE("584425C2-DB5E-4565-9927-BBF18E199D9E")
IEngineComCallback : public IUnknown
{
public:
	STDMETHOD(SetEngineCallback)(IEngineCallback* pCallback) = 0;
	STDMETHOD(GetComAsync)(IEngineComAsync** ppComAsync) = 0;
	STDMETHOD(SetLastMsg)(PMESSAGEHEADER pMsg) = 0;
};

#endif	// __Engine_h__

