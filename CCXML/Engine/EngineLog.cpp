/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������" �� ��� ������� �������� �.�.        */
/*                  ���� ���" � �� ����" ��� ��� �������.               */
/*                                                                      */
/************************************************************************/
#include "StdAfx.h"
#include <time.h>
#include "EngineLog.h"
#include <boost\shared_array.hpp>

// CEngineLog implementation

using namespace enginelog;

static TCHAR EngineLevelNames[][8] =
{
	_T("ALL"), _T("FT"), _T("FR"), _T("FN"), _T("CFG"), _T("IN"),
	_T("WN"), _T("SV"), _T("OFF")
};


CEngineLog::CEngineLog()
	: m_hFile(NULL), m_ullSID(0),
	m_dwPID(0), m_dwTID(0), m_level(LEVEL_OFF)
{
}

CEngineLog::~CEngineLog()
{
	Destroy();
}

LEVEL CEngineLog::translateLogLevel(LPCWSTR pwszLevel)
{
	if (0 == _wcsicmp(pwszLevel, L"fine"))
	{
		return LEVEL_FINE;
	}
	else if (0 == _wcsicmp(pwszLevel, L"finest"))
	{
		return LEVEL_FINEST;
	}
	else if (0 == _wcsicmp(pwszLevel, L"severe"))
	{
		return LEVEL_SEVERE;
	}
	else if (0 == _wcsicmp(pwszLevel, L"warning"))
	{
		return LEVEL_WARNING;
	}
	else if (0 == _wcsicmp(pwszLevel, L"info"))
	{
		return LEVEL_INFO;
	}
	else if (0 == _wcsicmp(pwszLevel, L"all"))
	{
		return LEVEL_ALL;
	}
	else if (0 == _wcsicmp(pwszLevel, L"none"))
	{
		return LEVEL_OFF;
	}

	return LEVEL_OFF;
}


bool CEngineLog::Init(LEVEL level, LPCWSTR pwszFormat, bool extraDate)
{
	time_t tt;
	struct tm* t;
	WCHAR wszFileName[MAX_PATH + 1] = { 0 };

	m_bExtraDate = extraDate;

	::time(&tt);
	t = ::localtime(&tt);

	if (!::wcsftime(wszFileName, MAX_PATH, pwszFormat, t))
	{
		wcsncpy(wszFileName, pwszFormat, MAX_PATH);
	}

	if ((m_hFile = ::CreateFileW(
		wszFileName,
		GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		0,
		OPEN_ALWAYS,
		0,
		NULL)) == INVALID_HANDLE_VALUE)
	{
		m_hFile = NULL;
		return false;
	}

	m_dwPID = ::GetCurrentProcessId();
	m_dwTID = ::GetCurrentThreadId();

	m_level = level;

	return true;
}

bool CEngineLog::Init(LPCWSTR pwszLevel, LPCWSTR pwszFormat, bool extraDate)
{
	return Init(translateLogLevel(pwszLevel), pwszFormat, extraDate);
}


void CEngineLog::Destroy()
{
	if (m_hFile)
	{
		::CloseHandle(m_hFile);
		m_hFile = NULL;
	}
}

void CEngineLog::SetSID(ULONGLONG ullSID)
{
	m_ullSID = ullSID;
}

void CEngineLog::LogV(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args)
{
	SYSTEMTIME st = { 0 };
	WCHAR wszFmt[256] = { 0 };
	WCHAR wszBuf[BUF_SIZE] = { 0 };
	CHAR szBuf[BUF_SIZE] = { 0 };

	::GetLocalTime(&st);

	int len = 0;
	
	if (m_bExtraDate)
	{
		len = _snwprintf(wszFmt, 255, L"%02d.%02d.%02d %02d:%02d:%02d.%03d S%08X-%08X %s\r\n",
			(int)st.wDay, (int)st.wMonth, (int)st.wYear,
			(int)st.wHour, (int)st.wMinute, (int)st.wSecond, (int)st.wMilliseconds,
			DWORD(m_ullSID >> 32), DWORD(m_ullSID & 0xFFFFFFFF),
			/*pwszFunc, */pwszFmt);
	}
	else
	{
		len = _snwprintf(wszFmt, 255, L"%02d:%02d:%02d.%03d P%04X T%04X S%08X-%08X %s %s\r\n",
			(int)st.wHour, (int)st.wMinute, (int)st.wSecond, (int)st.wMilliseconds,
			m_dwPID, m_dwTID,
			DWORD(m_ullSID >> 32), DWORD(m_ullSID & 0xFFFFFFFF),
			EngineLevelNames[m_level],/*pwszFunc, */pwszFmt);
	}

	int k = _vsnwprintf(wszBuf, BUF_SIZE - 1, wszFmt, args);
	len = (k < 0) ? (BUF_SIZE - 1) : k;

	::WideCharToMultiByte(CP_ACP, 0, wszBuf, len, szBuf, len, NULL, NULL);

	Locker::Lock();
	__try
	{
		DWORD dwWritten = 0;
		::SetFilePointer(m_hFile, 0, 0, FILE_END);
		::WriteFile(m_hFile, szBuf, len, &dwWritten, NULL);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
	Locker::Unlock();
}

void CEngineLog::Log(LEVEL level, LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...)
{
	if (GetLevel() > level)
		return;

	va_list	args;
	va_start(args, pwszFmt);
	LogV(pwszFunc, pwszFmt, args);
	va_end(args);
}

void CEngineLog::LogVLarge(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args)
{
	SYSTEMTIME st = { 0 };
	WCHAR wszFmt[256] = { 0 };
	WCHAR * wszBuf = new WCHAR[BUF_LARGE_SIZE]; // = { 0 };
	ZeroMemory(wszBuf, BUF_LARGE_SIZE);
	CHAR * szBuf = new CHAR[BUF_LARGE_SIZE]; // = { 0 };
	ZeroMemory(szBuf, BUF_LARGE_SIZE);

	::GetLocalTime(&st);

	//	int len = _snwprintf(wszFmt, 255, L"%02d:%02d:%02d.%03d P%04X T%04X S%08X-%08X [%-32s] %s\r\n",
	int len = _snwprintf(wszFmt, 255, L"%02d:%02d:%02d.%03d P%04X T%04X S%08X-%08X %s %s\r\n",
		(int)st.wHour, (int)st.wMinute, (int)st.wSecond, (int)st.wMilliseconds,
		m_dwPID, m_dwTID,
		DWORD(m_ullSID >> 32), DWORD(m_ullSID & 0xFFFFFFFF),
		pwszFunc, pwszFmt);

	int k = _vsnwprintf(wszBuf, BUF_LARGE_SIZE - 1, wszFmt, args);
	len = (k < 0) ? (BUF_LARGE_SIZE - 1) : k;

	::WideCharToMultiByte(CP_ACP, 0, wszBuf, len, szBuf, len, NULL, NULL);

	Locker::Lock();
	__try
	{
		DWORD dwWritten = 0;
		::SetFilePointer(m_hFile, 0, 0, FILE_END);
		::WriteFile(m_hFile, szBuf, len, &dwWritten, NULL);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
	delete[] szBuf;
	delete[] wszBuf;

	Locker::Unlock();
}

void CEngineLog::LogLarge(LEVEL level, LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...)
{
	if (GetLevel() > level)
		return;

	va_list	args;
	va_start(args, pwszFmt);
	LogVLarge(pwszFunc, pwszFmt, args);
	va_end(args);
}


LPCWSTR CEngineLog::DumpMessage(CMessage& msg)
{
	//FIXME
	msg.Dump(m_wszBuf, BUF_SIZE - 1);
	return m_wszBuf;
}

// CEngineLog::Locker implementation

CEngineLog::Locker::Locker()
{
	::InitializeCriticalSection(&m_CS);
}

CEngineLog::Locker::~Locker()
{
	::DeleteCriticalSection(&m_CS);
}

CEngineLog::Locker& CEngineLog::Locker::Instance()
{
	static Locker l;
	return l;
}

void CEngineLog::Locker::Lock()
{
	::EnterCriticalSection(&Locker::Instance().m_CS);
}

void CEngineLog::Locker::Unlock()
{
	::LeaveCriticalSection(&Locker::Instance().m_CS);
}

