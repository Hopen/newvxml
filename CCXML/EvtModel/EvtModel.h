/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <atlcomcli.h>		// For CComVariant
#include <atlstr.h>			// For CString
#include <exception>		// For std::exceptions
#include <string>			// For std::string & std::wstring
#include <list>				// For std::list

#ifndef _EVTMODEL_VER
#define _EVTMODEL_VER		1
#endif

#if _EVTMODEL_VER < 2

// For binary compatibility with old message format

#define	MSG_SIGNATURE		0x34DF7A6B994DACFF
#define	MAX_NAMELEN			63

struct PARAMHEADER
{
	DWORD		dwTotalSize;				// Total param size with header (in bytes)
	char		szName[MAX_NAMELEN + 1];	// Param name
	DWORD		dwRawDataSize;				// Raw data size (if any)
	LPVOID		pRawData;					// Raw data ptr (if any)
	VARIANT		vValue;						// Variant value (if not raw data)
	CHAR		cData[2];					// Actually, param data
};

typedef	PARAMHEADER*			PPARAMHEADER;
typedef	PARAMHEADER*			LPPARAMHEADER;
typedef	const PARAMHEADER*		PCPARAMHEADER;

struct MESSAGEHEADER
{
	ULONGLONG	ullSignature;				// Unique message signature for data validation
	DWORD		dwTotalSize;				// Total message size with header (in bytes)
	DWORD		dwMsgID;					// Message ID = for send mode
	char		szName[MAX_NAMELEN + 1];	// Message Name
	DWORD		dwParamCount;				// Number of params
	CHAR		cData[1];					// Actually, param data
};

typedef	MESSAGEHEADER*			PMESSAGEHEADER;
typedef	MESSAGEHEADER*			LPMESSAGEHEADER;
typedef	const MESSAGEHEADER*	PCMESSAGEHEADER;

#else

// EvtModel Version 2.0 - not implemented yet :)

#endif

#pragma warning(push)
// warning C4290: C++ exception specification ignored except to indicate a function is not __declspec(nothrow)
#pragma warning(disable:4290)

/************************************************************************/
/* CParam class - message parameter                                     */
/************************************************************************/

class CParam
{
	friend class CMessage;
private:
	class CRawData
	{
	private:
		LPVOID	m_pData;
		DWORD	m_dwSize;

	public:
		CRawData();
		CRawData(const CRawData& rd);
		~CRawData();
		void operator =(const CRawData& rd);
		LPCVOID Set(LPCVOID lpData, DWORD dwSize);
		void Clear();
		LPCVOID GetData() const;
		DWORD GetSize() const;
	};

private:
	wchar_t			m_Name[MAX_NAMELEN + 1];
	CComVariant		m_Val;
	CRawData		m_RawData;

	void Init();
	void Unpack(const PARAMHEADER* pph);

	explicit CParam(const PARAMHEADER* pph);

public:
	// Constructors
	CParam();
	CParam(const CParam& p);
	explicit CParam(LPCWSTR pwszName);
	explicit CParam(LPCWSTR pwszName, const CComVariant& vVal);

	// Copy operator =
	CParam& operator =(const CParam& p);

	// Name
	LPCWSTR GetName() const;
	void SetName(LPCWSTR pwszName);

	// Value
	const CComVariant& GetValue() const;
	void SetValue(const CComVariant& vVal);

	// Raw data manipulation
	LPCVOID GetRawData() const;
	DWORD GetRawDataSize() const;
	LPCVOID SetRawData(LPCVOID lpData, DWORD dwSize);
	void ClearRawData();

	// Packing operations
	bool CanPack() const;
	DWORD PackedSize() const;
	DWORD Pack(LPVOID lpBuffer) const;

	// Comparison operators
	bool operator ==(const CParam& p) const;
	bool operator !=(const CParam& p) const;
	bool operator <(const CParam& p) const;

	// Assignment operators
	void operator =(int iVal);
	void operator =(DWORD dwVal);
	void operator =(LONGLONG llVal);
	void operator =(ULONGLONG ullVal);
	void operator =(LPCSTR pszVal);
	void operator =(LPCWSTR pwszVal);
	void operator =(bool bVal);
	void operator =(FLOAT fltVal);
	void operator =(DOUBLE dblVal);
	void operator =(const CComVariant& vVal);
	void operator =(const std::string& s);
	void operator =(const std::wstring& s);
	void operator =(const CString& s);

	// Special method for OLE DATE, cause of typedef double DATE;
	void SetDateTime(DATE dateVal);

	// Type check methods
	bool IsInt() const;
	bool IsFloat() const;
	bool IsString() const;
	bool IsBool() const;
	bool IsDateTime() const;
	bool IsRawData() const;

	// Type cast methods
	LONG		AsInt() const throw(std::exception);
	ULONG		AsUInt() const throw(std::exception);
	LONGLONG	AsInt64() const throw(std::exception);
	ULONGLONG	AsUInt64() const throw(std::exception);
	std::string AsStr() const;
	std::wstring AsWideStr() const;
	CString		AsString() const;
	bool		AsBool() const throw(std::exception);
	FLOAT		AsFloat() const throw(std::exception);
	DOUBLE		AsDouble() const throw(std::exception);
	DATE		AsDateTime() const throw(std::exception);

	// Type operators
	operator int() const throw(std::exception);
	operator __int64() const throw(std::exception);
	operator double() const throw(std::exception);
	operator bool() const throw(std::exception);
	operator LPCWSTR() const throw(std::exception);

	// Conversion operations
	std::wstring Dump() const;
	void Dump(LPWSTR pwszBuf, DWORD dwMaxSize) const;

public:
	// Public properties
	__declspec(property(get=GetName,put=SetName)) LPCWSTR Name;
	__declspec(property(get=GetValue,put=SetValue)) const CComVariant& Value;
	__declspec(property(get=GetRawData)) LPCVOID RawData;
	__declspec(property(get=GetRawDataSize)) DWORD RawDataSize;
};

/************************************************************************/
/* CMessage class - message itself                                      */
/************************************************************************/

class CParamsTable;
typedef std::list<CParam> ParamsList;

class CMessage
{
private:
	wchar_t			m_Name[MAX_NAMELEN + 1];
	CParamsTable*	m_pParams;

	void Init(const CMessage* other = NULL);
	void Unpack(const MESSAGEHEADER* pmh) throw(std::exception);

public:
	enum ParamType
	{
		Mandatory,
		Optional,
		Forbidden
	};

	enum CheckedType
	{
		Int,
		Int64,
		String,
		Float,
		RawData,
		Bool,
		DateTime
	};

public:
	CMessage();
	CMessage(const CMessage& msg);
	explicit CMessage(const MESSAGEHEADER* pmh);
	explicit CMessage(LPCWSTR pwszName);
	~CMessage();

	// Copy operator =
	CMessage& operator =(const CMessage& msg);

	// Name
	LPCWSTR GetName() const;
	void SetName(LPCWSTR pwszName);

	// Message operations
	CMessage& operator +=(const CMessage& msg);
	CMessage Filter(LPCWSTR pwszFilter) const;
	void Clear();

	// Params operations
	CParam& AddParam(const CParam& param);

	CParam& operator [](LPCWSTR pwszName);
	const CParam& operator [](LPCWSTR pwszName) const throw(std::exception);

	CParam* ParamByName(LPCWSTR pwszName);
	const CParam* ParamByName(LPCWSTR pwszName) const;
	CParam Param(LPCWSTR pwszName, const CComVariant& defVal) const;
	CParam Param(LPCWSTR pwszName, LONGLONG defVal) const;
	CParam Param(LPCWSTR pwszName, ULONGLONG defVal) const;

	bool Remove(LPCWSTR pwszName);
	bool IsParam(LPCWSTR pwszName) const;
	DWORD ParamsCount() const;

	ParamsList Params() const;

	// Packing operations
	bool CanPack() const;
	DWORD PackedSize() const;
	DWORD Pack(LPVOID lpBuffer) const throw(std::exception);

	// Comparison operators
	bool operator ==(const CMessage& msg) const;
	bool operator ==(LPCWSTR pwszName) const;
	bool operator !=(const CMessage& msg) const;
	bool operator !=(LPCWSTR pwszName) const;
	bool operator <(const CMessage& msg) const;

	// Conversion operations
	std::wstring Dump() const;
	void Dump(LPWSTR pwszBuf, DWORD dwMaxSize) const;
	void ToVariant(CComVariant& v) const;
	bool FromVariant(const CComVariant& v);
	std::wstring ToXML() const;
	bool FromXML(LPCWSTR pwszXML);

	// Checked parameter operations
	const CParam& SafeReadParam(LPCWSTR pwszName, 
								CMessage::CheckedType type) const throw(std::exception);
	CParam SafeReadParam(LPCWSTR pwszName, 
						 CMessage::CheckedType type, 
						 const CComVariant& defVal) const throw(std::exception);
	void CheckParam(LPCWSTR pwszName, 
					CMessage::CheckedType type, 
					CMessage::ParamType ptype) const throw(std::exception);
	int ParamAsEnum(LPCWSTR pwszName, 
					LPCWSTR pwszEnumValues) const throw(std::exception);

public:
	// Public properties
	__declspec(property(get=GetName,put=SetName)) LPCWSTR Name;
};

/************************************************************************/
/* CMessage packing extension                                           */
/************************************************************************/

class CPackedMsg
{
private:
	PMESSAGEHEADER m_pMH;

public:
	CPackedMsg(const CMessage& msg);
	PMESSAGEHEADER operator ()() const;
	~CPackedMsg();
	DWORD Size() const;
};

// Helper functions

bool IsBadMessage(const MESSAGEHEADER* pmh);
std::wstring DumpMessage(const MESSAGEHEADER* pmh);
void DumpMessage(const MESSAGEHEADER* pmh, LPWSTR pwszBuf, DWORD dwMaxSize);

/************************************************************************/
/* Readonly message extensions                                          */
/************************************************************************/

class CROParam : public PARAMHEADER
{
	friend class CROMessage;
private:
	explicit CROParam() { }

public:
	// Name
	LPCSTR GetName() const;

	// Value
	CComVariant GetValue() const;

	// Raw data manipulation
	LPCVOID GetRawData() const;
	DWORD GetRawDataSize() const;

	// Type check methods
	bool IsInt() const;
	bool IsFloat() const;
	bool IsString() const;
	bool IsBool() const;
	bool IsDateTime() const;
	bool IsRawData() const;

	// Type cast methods
	LONG		AsInt() const throw(std::exception);
	ULONG		AsUInt() const throw(std::exception);
	LONGLONG	AsInt64() const throw(std::exception);
	ULONGLONG	AsUInt64() const throw(std::exception);
	std::string AsStr() const;
	std::wstring AsWideStr() const;
	CString		AsString() const;
	bool		AsBool() const throw(std::exception);
	FLOAT		AsFloat() const throw(std::exception);
	DOUBLE		AsDouble() const throw(std::exception);
	DATE		AsDateTime() const throw(std::exception);

	// Conversion operations
	int Dump(LPWSTR pwszBuf, int cbBuf) const;

public:
	// Public properties
	__declspec(property(get=GetName)) LPCSTR Name;
	__declspec(property(get=GetValue)) const CComVariant& Value;
	__declspec(property(get=GetRawData)) LPCVOID RawData;
	__declspec(property(get=GetRawDataSize)) DWORD RawDataSize;
};

class CROMessage
{
private:
	PMESSAGEHEADER m_pMH;
	bool m_bDetached;

	void Copy(const PMESSAGEHEADER pmh, bool bDetached);
	void Free();

public:
	CROMessage();
	CROMessage(const CROMessage& msg);
	explicit CROMessage(const PMESSAGEHEADER pmh, bool bDetached = true);
	~CROMessage();

	// Copy operator =
	CROMessage& operator =(const CROMessage& msg);

	// Name
	LPCSTR GetName() const;

	const CROParam& operator [](LPCSTR pszName) const throw(std::exception);
	const CROParam* ParamByName(LPCSTR pszName) const;
	bool IsParam(LPCSTR pszName) const;
	DWORD ParamsCount() const;

	// Conversion operations
	void Dump(LPWSTR pwszBuf, int cbBuf) const;
	PMESSAGEHEADER GetMsgHeader() const;
	PMESSAGEHEADER Detach();

public:
	// Public properties
	__declspec(property(get=GetName)) LPCSTR Name;
};

#pragma warning(pop)
