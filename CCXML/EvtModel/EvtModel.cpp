/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include <atlbase.h>		// For CComBSTR
#include "EvtModel.h"

#define HASHTABLE_SIZE			256

#pragma warning(push)
// warning C4996: 'wcsncpy' was declared deprecated
#pragma warning(disable:4996)

// Helper variant copying function, 
// safe for VT_I8/VT_UI8 under pre- 2003/XP OSes
//HRESULT VariantCopy(VARIANTARG* pvargDest, const VARIANTARG* pvargSrc)
//{
//	if (pvargSrc->vt == VT_I8 || pvargSrc->vt == VT_UI8)
//	{
//		::VariantClear(pvargDest);
//		CopyMemory(pvargDest, pvargSrc, sizeof(VARIANT));
//		return S_OK;
//	}
//
//	return ::VariantCopy(pvargDest, (VARIANTARG*)pvargSrc);
//}

/************************************************************************/
/* CParam and CMessage allocators                                       */
/************************************************************************/

//static void* operator new(size_t cb)
//{
// 	return ::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY | HEAP_GENERATE_EXCEPTIONS, cb);
//	return malloc(cb);
//}

//static void* operator new[](size_t cb)
//{
//	return operator new(cb);
//}

//static void operator delete(void *pUserData)
//{
//	::HeapFree(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, pUserData);
//	return free(pUserData);
//}

/************************************************************************/
/* CParam class implementation                                          */
/************************************************************************/

// Constructors

void CParam::Init()
{
	ZeroMemory(m_Name, sizeof(m_Name));
}

CParam::CParam()
{
	Init();
}

CParam::CParam(const CParam& p)
{
	Init();
	operator =(p);
}

CParam::CParam(const PARAMHEADER* pph)
{
	Init();
	Unpack(pph);
}

CParam::CParam(LPCWSTR pwszName)
{
	Init();
	SetName(pwszName);
}

CParam::CParam(LPCWSTR pwszName, const CComVariant& vVal)
{
	Init();
	SetName(pwszName);
	SetValue(vVal);
}

// Copy operator =
CParam& CParam::operator =(const CParam& p)
{
	SetName(p.Name);
	m_Val = p.m_Val;
	m_RawData = p.m_RawData;
	return *this;
}

// Name

LPCWSTR CParam::GetName() const
{
	return m_Name;
}

void CParam::SetName(LPCWSTR pwszName)
{
	wcsncpy(m_Name, pwszName, MAX_NAMELEN);
	m_Name[MAX_NAMELEN] = L'\0';
}

// Value

const CComVariant& CParam::GetValue() const
{
	return m_Val;
}

void CParam::SetValue(const CComVariant& vVal)
{
	m_Val = vVal;
}

// Raw data manipulation

LPCVOID CParam::GetRawData() const
{
	return m_RawData.GetData();
}

DWORD CParam::GetRawDataSize() const
{
	return m_RawData.GetSize();
}

LPCVOID CParam::SetRawData(LPCVOID lpData, DWORD dwSize)
{
	return m_RawData.Set(lpData, dwSize);
}

void CParam::ClearRawData()
{
	m_RawData.Clear();
}

// Packing operations

#if _EVTMODEL_VER < 2

bool CParam::CanPack() const
{
	WORD vt = m_Val.vt;

	if (vt & ~VT_TYPEMASK)
	{
		return false;
	}

	vt = vt & VT_TYPEMASK;
	
	if (vt == VT_UNKNOWN || 
		vt == VT_DISPATCH || 
		vt == VT_VARIANT)
	{
		return false;
	}

	return true;
}

DWORD CParam::PackedSize() const
{
	DWORD dwSize = sizeof(PARAMHEADER);

	// BSTR
	if (m_Val.vt == VT_BSTR)
	{
		dwSize += (::SysStringLen(m_Val.bstrVal) + 1) * sizeof(OLECHAR);
	}
	// Raw data
	else if ((m_Val.vt == VT_EMPTY || m_Val.vt == VT_NULL) && (m_RawData.GetSize() > 0))
	{
		dwSize += m_RawData.GetSize();
	}

	return dwSize;
}

DWORD CParam::Pack(LPVOID lpBuffer) const
{
	PPARAMHEADER pph = (PPARAMHEADER)lpBuffer;
	ZeroMemory(pph, sizeof(PARAMHEADER));

	::WideCharToMultiByte(CP_ACP, 0, m_Name, -1, pph->szName, MAX_NAMELEN, NULL, NULL);

	pph->vValue = m_Val;

	// BSTR
	if (m_Val.vt == VT_BSTR)
	{
		CopyMemory(pph->cData, m_Val.bstrVal, (::SysStringLen(m_Val.bstrVal) + 1) * sizeof(OLECHAR));
	}
	// Raw data
	else if (IsRawData())
	{
		CopyMemory(pph->cData, m_RawData.GetData(), m_RawData.GetSize());
		pph->dwRawDataSize = m_RawData.GetSize();
	}

	return pph->dwTotalSize = PackedSize();
}

void CParam::Unpack(const PARAMHEADER* pph)
{
	::MultiByteToWideChar(CP_ACP, 0, pph->szName, -1, m_Name, MAX_NAMELEN);
	m_Name[MAX_NAMELEN] = L'\0';

	// BSTR
	if (pph->vValue.vt == VT_BSTR)
	{
		m_Val = (OLECHAR*)pph->cData;
	}
	// Raw data
	else if ((pph->vValue.vt == VT_EMPTY || pph->vValue.vt == VT_NULL) && (pph->dwRawDataSize > 0))
	{
		m_RawData.Set(pph->cData, pph->dwRawDataSize);
	}
	// Other
	else
	{
		VariantCopy(&m_Val, &pph->vValue);
	}
}

#else

// EvtModel Version 2.0 - not implemented yet :)

#endif

// Comparison operators

bool CParam::operator ==(const CParam& p) const
{
	if (wcscmp(m_Name, p.m_Name) != 0)
	{
		return false;
	}
	if (::VarCmp((LPVARIANT)&m_Val, (LPVARIANT)&p.m_Val, 0, 0) != VARCMP_EQ)
	{
		return false;
	}
	
	if (IsRawData())
	{
		if (m_RawData.GetSize() != p.m_RawData.GetSize())
		{
			return false;
		}
		return memcmp(m_RawData.GetData(), p.m_RawData.GetData(), m_RawData.GetSize()) != 0;
	}

	return true;
}

bool CParam::operator !=(const CParam& p) const
{
	return !operator ==(p);
}

bool CParam::operator <(const CParam& p) const
{
	int cmp = wcscmp(m_Name, p.m_Name);
	if (cmp < 0)
		return true;
	else if (cmp > 0)
		return false;

	cmp = ::VarCmp((LPVARIANT)&m_Val, (LPVARIANT)&p.m_Val, 0, 0);
	if (cmp == VARCMP_LT)
		return true;
	else if (cmp == VARCMP_GT)
		return false;

	if (IsRawData())
	{
		cmp = (int)m_RawData.GetSize() - (int)p.m_RawData.GetSize();
		if (cmp < 0)
			return true;
		else if (cmp > 0)
			return false;
		return memcmp(m_RawData.GetData(), p.m_RawData.GetData(), m_RawData.GetSize()) < 0;
	}

	return false;
}

// Assignment operators

void CParam::operator =(int iVal)
{
	m_Val = iVal;
}

void CParam::operator =(DWORD dwVal)
{
	m_Val = dwVal;
}

void CParam::operator =(LONGLONG llVal)
{
	m_Val.Clear();
	m_Val.vt = VT_I8;
	m_Val.llVal = llVal;
}

void CParam::operator =(ULONGLONG ullVal)
{
	m_Val.Clear();
	m_Val.vt = VT_UI8;
	m_Val.llVal = ullVal;
}

void CParam::operator =(LPCSTR pszVal)
{
	m_Val = pszVal;
}

void CParam::operator =(LPCWSTR pwszVal)
{
	m_Val = pwszVal;
}

void CParam::operator =(bool bVal)
{
	m_Val.Clear();
	m_Val.vt = VT_BOOL;
	m_Val.boolVal = bVal ? VARIANT_TRUE : VARIANT_FALSE;
}

void CParam::operator =(FLOAT fltVal)
{
	m_Val = fltVal;
}

void CParam::operator =(DOUBLE dblVal)
{
	m_Val = dblVal;
}

void CParam::operator =(const CComVariant& vVal)
{
	m_Val = vVal;
}

void CParam::operator =(const std::string& s)
{
	operator =(s.c_str());
}

void CParam::operator =(const std::wstring& s)
{
	operator =(s.c_str());
}

void CParam::operator =(const CString& s)
{
//	operator =((LPCWSTR)s);
	operator =((CString::PCXSTR)s);
}

// Special method for OLE DATE, cause of typedef double DATE;
void CParam::SetDateTime(DATE dateVal)
{
	m_Val.Clear();
	m_Val.vt = VT_DATE;
	m_Val.date = dateVal;
}

// Type check methods

bool CParam::IsInt() const
{
	switch (m_Val.vt)
	{
		case VT_I1:
		case VT_UI1:
		case VT_I2:
		case VT_UI2:
		case VT_I4:
		case VT_UI4:
		case VT_I8:
		case VT_UI8:
		case VT_INT:
		case VT_UINT:
			return true;
	}

	return false;
}

bool CParam::IsFloat() const
{
	return (m_Val.vt == VT_R4 || m_Val.vt == VT_R8);
}

bool CParam::IsString() const
{
	return (m_Val.vt == VT_BSTR);
}

bool CParam::IsBool() const
{
	return (m_Val.vt == VT_BOOL);
}

bool CParam::IsDateTime() const
{
	return (m_Val.vt == VT_DATE);
}

bool CParam::IsRawData() const
{
	return (m_Val.vt == VT_EMPTY || m_Val.vt == VT_NULL) && (m_RawData.GetSize() > 0);
}

// Type cast methods

LONG CParam::AsInt() const
{
	return (LONG)AsInt64();
}

ULONG CParam::AsUInt() const
{
	return (ULONG)AsInt64();
}

LONGLONG CParam::AsInt64() const
{
	switch (m_Val.vt)
	{
		case VT_I1:		return (LONGLONG)m_Val.cVal;
		case VT_UI1:	return (LONGLONG)m_Val.bVal;
		case VT_I2:		return (LONGLONG)m_Val.iVal;
		case VT_UI2:	return (LONGLONG)m_Val.uiVal;
		case VT_I4:		return (LONGLONG)m_Val.lVal;
		case VT_UI4:	return (LONGLONG)m_Val.ulVal;
		case VT_I8:		return (LONGLONG)m_Val.llVal;
		case VT_UI8:	return (LONGLONG)m_Val.ullVal;
		case VT_INT:	return (LONGLONG)m_Val.intVal;
		case VT_UINT:	return (LONGLONG)m_Val.uintVal;
		case VT_R4:		return (LONGLONG)m_Val.fltVal;
		case VT_R8:		return (LONGLONG)m_Val.dblVal;
		case VT_BSTR:	return _wtoi(m_Val.bstrVal);
		case VT_BOOL:	return (m_Val.boolVal == VARIANT_TRUE) ? 1 : 0;
		case VT_DATE:	return (LONGLONG)m_Val.date;
	}

	throw std::exception("Cannot convert parameter to Int64");
	return 0;
}

ULONGLONG CParam::AsUInt64() const
{
	return (ULONGLONG)AsInt64();
}

std::string CParam::AsStr() const
{
	std::wstring ws = AsWideStr();
	int len = (int)ws.length();
	std::auto_ptr<char> s(new char[len + 1]);
	ZeroMemory(s.get(), len + 1);

	::WideCharToMultiByte(
		CP_ACP, 
		0, ws.c_str(), len, 
		s.get(), len + 1, NULL, NULL);

	return s.get();
}

std::wstring CParam::AsWideStr() const
{
	wchar_t wszBuf[256] = {0};

	if (IsInt())
	{
		switch (m_Val.vt)
		{
			case VT_I1:
			case VT_I2:
			case VT_I4:
			case VT_I8:
			case VT_INT:
				swprintf(wszBuf, L"%I64i", AsInt64());
				break;

			default:
				swprintf(wszBuf, L"%I64u", AsUInt64());
				break;
		}
	}
	else if (IsFloat())
	{
		swprintf(wszBuf, L"%f", AsDouble());
	}
	else if (IsString())
	{
		return (LPCWSTR)m_Val.bstrVal;
	}
	else if (IsBool())
	{
		return (m_Val.boolVal == VARIANT_TRUE) ? L"true" : L"false";
	}
	else if (IsDateTime())
	{
		CComBSTR bstr;
		if (FAILED(::VarBstrFromDate(
			m_Val.date, LANG_USER_DEFAULT, LOCALE_NOUSEROVERRIDE, &bstr)))
		{
			return L"";
		}
		else
		{
			return (LPCWSTR)bstr;
		}
	}
	else if (m_Val.vt == VT_EMPTY || m_Val.vt == VT_NULL)
	{
		return L"";
	}
	else
	{
		throw std::exception("Cannot convert parameter to String");
	}

	return std::wstring(wszBuf);	
}

CString CParam::AsString() const
{
	return CString(AsWideStr().c_str());
}

bool CParam::AsBool() const
{
	if (IsBool())
	{
		return (m_Val.boolVal == VARIANT_TRUE);
	}
	else if (IsInt())
	{
		return AsInt64() != 0;
	}
	else if (IsFloat())
	{
		return AsDouble() != 0.0;
	}
	else if (IsString())
	{
		if (!wcsicmp(m_Val.bstrVal, L"true"))
		{
			return true;
		}
		if (!wcsicmp(m_Val.bstrVal, L"false"))
		{
			return false;
		}
	}

	throw std::exception("Cannot convert parameter to Bool");
	return 0;
}

FLOAT CParam::AsFloat() const
{
	return (FLOAT)AsDouble();
}

DOUBLE CParam::AsDouble() const
{
	if (IsString())
	{
		return (DOUBLE)_wtof(m_Val.bstrVal);
	}
	else if (IsInt())
	{
		return (DOUBLE)AsInt64();
	}

	switch (m_Val.vt)
	{
		case VT_R4: return (DOUBLE)m_Val.fltVal;
		case VT_R8: return m_Val.dblVal;
	}

	throw std::exception("Cannot convert parameter to Double");
	return 0.0;
}

DATE CParam::AsDateTime() const
{
	if (IsFloat())
	{
		return (DATE)AsDouble();
	}
	else if (IsString())
	{
		DATE date = 0.0;
		if (SUCCEEDED(::VarDateFromStr(
			m_Val.bstrVal, 
			LANG_USER_DEFAULT, 
			0, 
			&date)))
		{
			return date;
		}
	}
	else if (IsDateTime())
	{
		return m_Val.date;
	}

	throw std::exception("Cannot convert parameter to DateTime");
	return 0.0;
}

// Type operators

CParam::operator int() const
{
	if (!IsInt())
	{
		throw std::exception("Parameter is not Int");
	}
	return (int)AsInt();
}

CParam::operator __int64() const
{
	if (!IsInt())
	{
		throw std::exception("Parameter is not Int");
	}
	return AsInt();
}

CParam::operator double() const
{
	if (!IsInt())
	{
		throw std::exception("Parameter is not Float");
	}
	return AsFloat();
}

CParam::operator bool() const
{
	if (!IsBool())
	{
		throw std::exception("Parameter is not Bool");
	}
	return AsBool();
}

CParam::operator LPCWSTR() const
{
	if (!IsString())
	{
		throw std::exception("Parameter is not String");
	}
	return m_Val.bstrVal;
}

// Conversion operations
std::wstring CParam::Dump() const
{
	wchar_t wszBuf[256] = {0};
	Dump(wszBuf, sizeof(wszBuf) / sizeof(wszBuf[0]));
	return std::wstring(wszBuf);
}

void CParam::Dump(LPWSTR pwszBuf, DWORD dwMaxSize) const
{
	ZeroMemory(pwszBuf, dwMaxSize);

	int k = _snwprintf(pwszBuf, dwMaxSize, L"%s = ", GetName());
	if (k > 0)
	{
		dwMaxSize -= k;
		pwszBuf += k;
	}
	else
	{
		return;
	}

	if (IsRawData())
	{
		_snwprintf(pwszBuf, dwMaxSize, L"Raw data, size=%u", GetRawDataSize());
		return;
	}

	switch (m_Val.vt)
	{
		case VT_I8:
		case VT_UI8:
			_snwprintf(
				pwszBuf, dwMaxSize, L"0x%08X-%08X", 
				DWORD(m_Val.llVal >> 32), DWORD(m_Val.llVal & 0xFFFFFFFF));
			break;

		case VT_I1:
		case VT_UI1:
		case VT_I2:
		case VT_UI2:
		case VT_I4:
		case VT_UI4:
		case VT_INT:
		case VT_UINT:
		case VT_R4:
		case VT_R8:
		case VT_BOOL:
		case VT_DATE:
			_snwprintf(pwszBuf, dwMaxSize, L"%s", AsWideStr().c_str());
			break;

		case VT_BSTR:
			if (::SysStringLen(m_Val.bstrVal) > 128)
			{
				_snwprintf(pwszBuf, dwMaxSize, L"\"%128s\"...", m_Val.bstrVal);
			}
			else
			{
				_snwprintf(pwszBuf, dwMaxSize, L"\"%s\"", m_Val.bstrVal);
			}
			break;

		default:
			_snwprintf(pwszBuf, dwMaxSize, L"Unknown Type %hu", m_Val.vt);
			break;
	}
}

/************************************************************************/
/* CParam::CRawData class implementation                                */
/************************************************************************/

CParam::CRawData::CRawData() : m_pData(NULL), m_dwSize(0)
{
}

CParam::CRawData::CRawData(const CRawData& rd) : m_pData(NULL), m_dwSize(0)
{
	operator =(rd);
}

CParam::CRawData::~CRawData()
{
	Clear();
}

void CParam::CRawData::operator =(const CRawData& rd)
{
	if (&rd != this)
	{
		Set(rd.m_pData, rd.m_dwSize);
	}
}

LPCVOID CParam::CRawData::Set(LPCVOID lpData, DWORD dwSize)
{
	Clear();
	
	if (lpData)
	{
		m_pData = new BYTE[dwSize];
		m_dwSize = dwSize;
		CopyMemory(m_pData, lpData, dwSize);
	}

	return m_pData;
}

void CParam::CRawData::Clear()
{
	if (m_pData != NULL)
	{
		delete m_pData;
		m_pData = NULL;
	}
	m_dwSize = 0;
}

LPCVOID CParam::CRawData::GetData() const
{
	return m_pData;
}

DWORD CParam::CRawData::GetSize() const
{
	return m_dwSize;
}

/************************************************************************/
/* CMessage params hash table                                           */
/************************************************************************/

class CParamsTable
{
private:
	struct ListNode : public CParam
	{
		int			nTableEntry;
		ListNode*	pPrev;
		ListNode*	pNext;
		ListNode(const CParam& p, int entry) :
			CParam(p), nTableEntry(entry), pPrev(NULL), pNext(NULL)
		{
		}
		~ListNode()
		{
		}
	};

	ListNode*	m_Table[HASHTABLE_SIZE];
	ListNode*	m_pBegin;
	ListNode*	m_pEnd;
	int			m_nSize;

	void Init()
	{
		ZeroMemory(m_Table, sizeof(m_Table));
		m_pBegin = m_pEnd = NULL;
		m_nSize = 0;
	}

	static DWORD Hash(LPCWSTR pwszName);
	ListNode* Find(int index, LPCWSTR pwszName);
	const ListNode* Find(int index, LPCWSTR pwszName) const;

public:
	class iterator
	{
	private:
		ListNode* pNode;

	public:
		iterator(ListNode* node) : pNode(node)
		{
		}
		iterator& operator ++()
		{
			if (pNode != NULL)
			{
				pNode = pNode->pNext;
			}
			return *this;
		}
		CParam& operator *()
		{
			return *pNode;
		}
		bool operator !=(const iterator& it) const
		{
			return pNode != it.pNode;
		}
	};

public:
	// Constructors
	CParamsTable()
	{
		Init();
	}
	CParamsTable(const CParamsTable& table)
	{
		Init();
		Copy(table);
	}
	~CParamsTable()
	{
		Clear();
	}

	// Params operations
	CParam& Add(const CParam& p);
	bool Remove(LPCWSTR pwszName);
	CParam* Find(LPCWSTR pwszName);
	const CParam* Find(LPCWSTR pwszName) const;

	// Table operations
	int GetSize() const
	{
		return m_nSize;
	}
	void Copy(const CParamsTable& table);
	void Clear();

	// Iterators
	iterator begin() const
	{
		return iterator(m_pBegin);
	}
	iterator end() const
	{
		return iterator(NULL);
	}

	// Comparison operators
	bool operator ==(const CParamsTable& table) const;
	bool operator <(const CParamsTable& table) const;
};

/************************************************************************/
/* CParamsTable class implementation                                    */
/************************************************************************/

DWORD CParamsTable::Hash(LPCWSTR pwszName)
{
	DWORD hash = 0;

	while (*pwszName)
	{
		hash = (hash << 5) + hash + (*pwszName++);
	}

	return hash % HASHTABLE_SIZE;
}

const CParamsTable::ListNode* CParamsTable::Find(int index, LPCWSTR pwszName) const
{
	ListNode* p = m_Table[index];
	for (ListNode* q = p; p != NULL && (p->nTableEntry == q->nTableEntry); q = p, p = p->pNext)
	{
		if (!wcscmp(p->Name, pwszName))
		{
			return p;
		}
	}
	return NULL;
}

CParamsTable::ListNode* CParamsTable::Find(int index, LPCWSTR pwszName)
{
	return (ListNode*)((const CParamsTable*)this)->Find(index, pwszName);
}

CParam& CParamsTable::Add(const CParam& p)
{
	DWORD index = Hash(p.Name);

	if (CParam* pParam = Find(index, p.Name))
	{
		*pParam = p;
		return *pParam;
	}

	ListNode* pOld = m_Table[index];
	ListNode* pNode = new ListNode(p, index);

	if (pOld == NULL)
	{
		// insert into the end of the list, list may be still empty
		pNode->pNext = NULL;
		if (m_pBegin == NULL)
		{
			// list is empty
			m_pBegin = pNode;
			pNode->pPrev = NULL;
		}
		else
		{
			m_pEnd->pNext = pNode;
			pNode->pPrev = m_pEnd;
		}
		m_pEnd = pNode;
	}
	else
	{
		// insert into the middle of the list
		pNode->pNext = pOld;
		pNode->pPrev = pOld->pPrev;
		pOld->pPrev = pNode;
		if (pNode->pPrev == NULL)
		{
			// so we are inserting before the beginning of the list
			m_pBegin = pNode;
		}
		else
		{
			pNode->pPrev->pNext = pNode;
		}
	}

	m_Table[index] = pNode;
	m_nSize++;

	return *pNode;
}

bool CParamsTable::Remove(LPCWSTR pwszName)
{
	DWORD index = Hash(pwszName);
	ListNode*& pEntry = m_Table[index];
	ListNode* pNode = Find(index, pwszName);
	
	if (pNode == NULL)
	{
		return false;
	}

	// modify table entry
	if (pNode == pEntry)
	{
		if (pNode->pNext != NULL && 
			pNode->pNext->nTableEntry == pNode->nTableEntry)
		{
			pEntry = pNode->pNext;
		}
		else
		{
			pEntry = NULL;
		}
	}

	// if this is the beginning of the list
	if (pNode == m_pBegin)
	{
		m_pBegin = pNode->pNext;
	}
	// if this is the end of the list
	if (pNode == m_pEnd)
	{
		m_pEnd = pNode->pPrev;
	}

	// adjust neighbor pointers
	if (pNode->pPrev)
	{
		pNode->pPrev->pNext = pNode->pNext;
	}
	if (pNode->pNext)
	{
		pNode->pNext->pPrev = pNode->pPrev;
	}

	m_nSize--;
	delete pNode;

	return true;
}

CParam* CParamsTable::Find(LPCWSTR pwszName)
{
	return Find(Hash(pwszName), pwszName);
}

const CParam* CParamsTable::Find(LPCWSTR pwszName) const
{
	return Find(Hash(pwszName), pwszName);
}

void CParamsTable::Copy(const CParamsTable& table)
{
	Clear();
	for (iterator it = table.begin(); it != table.end(); ++it)
	{
		Add(*it);
	}
}

void CParamsTable::Clear()
{
	for (ListNode* p = m_pBegin; p != NULL;)
	{
		ListNode* q = p->pNext;
		delete p;
		p = q;
	}
	Init();
}

// Comparison operators

bool CParamsTable::operator ==(const CParamsTable& table) const
{
	if (m_nSize != table.m_nSize)
	{
		return false;
	}

	for (iterator it = begin(); it != end(); ++it)
	{
		if (const CParam* p = table.Find((*it).Name))
		{
			if (*it != *p)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	return true;
}

bool CParamsTable::operator <(const CParamsTable& table) const
{
	if (m_nSize < table.m_nSize)
	{
		return true;
	}
	else if (table.m_nSize < m_nSize)
	{
		return false;
	}

	iterator it1 = begin(), it2 = table.begin();
	for (; it1 != end(); ++it1, ++it2)
	{
		if (*it1 < *it2)
		{
			return true;
		}
		else if (*it2 < *it1)
		{
			return false;
		}
	}

	return false;
}

/************************************************************************/
/* CMessage class implementation                                        */
/************************************************************************/

// Initialization

void CMessage::Init(const CMessage* other)
{
	m_pParams = (other == NULL) ? new CParamsTable() : new CParamsTable(*other->m_pParams);
	ZeroMemory(m_Name, sizeof(m_Name));
}

// Constructors

CMessage::CMessage()
{
	Init();
}

CMessage::CMessage(const CMessage& msg)
{
	Init(&msg);
	SetName(msg.Name);
}

CMessage::CMessage(const MESSAGEHEADER* pmh)
{
	Init();
	Unpack(pmh);
}

CMessage::CMessage(LPCWSTR pwszName)
{
	Init();
	SetName(pwszName);
}

CMessage::~CMessage()
{
	delete m_pParams;
}

// Copy operator =

CMessage& CMessage::operator =(const CMessage& msg)
{
	Clear();
	SetName(msg.m_Name);
	m_pParams->Copy(*msg.m_pParams);
	return *this;
}

// Name

LPCWSTR CMessage::GetName() const
{
	return m_Name;
}

void CMessage::SetName(LPCWSTR pwszName)
{
	wcsncpy(m_Name, pwszName, MAX_NAMELEN);
	m_Name[MAX_NAMELEN] = L'\0';
}

// Message operations

CMessage& CMessage::operator +=(const CMessage& msg)
{
	CParamsTable::iterator it = msg.m_pParams->begin();
	CParamsTable::iterator end = msg.m_pParams->end();
	for (; it != end; ++it)
	{
		if (!IsParam((*it).Name))
		{
			AddParam((CParam&)(*it));
		}
	}
	return *this;
}

CMessage CMessage::Filter(LPCWSTR pwszFilter) const
{
	CMessage msg(this->Name);

	for (LPCWSTR p = pwszFilter, q = NULL; *p != L'\0'; p = q + 1)
	{
		q = wcschr(p, ';');
		wchar_t wszBuf[MAX_NAMELEN + 1] = {0};
		wcsncpy(wszBuf, p, q ? min(MAX_NAMELEN, q - p) : MAX_NAMELEN);
		if (wszBuf[0] != L'\0')
		{
			if (const CParam* p = this->ParamByName(wszBuf))
			{
				msg.AddParam(*p);
			}
		}
		if (q == NULL)
		{
			break;
		}
	}

	return msg;
}

void CMessage::Clear()
{
	m_pParams->Clear();
	ZeroMemory(m_Name, sizeof(m_Name));
}

// Params operations

CParam& CMessage::AddParam(const CParam& param)
{
	return m_pParams->Add(param);
}

CParam& CMessage::operator [](LPCWSTR pwszName)
{
	if (CParam* p = ParamByName(pwszName))
	{
		return *p;
	}
	return AddParam(CParam(pwszName));
}

const CParam& CMessage::operator [](LPCWSTR pwszName) const
{
	if (const CParam* p = ParamByName(pwszName))
	{
		return *p;
	}
	throw std::exception("Parameter not found");
}

CParam* CMessage::ParamByName(LPCWSTR pwszName)
{
	return m_pParams->Find(pwszName);
}

const CParam* CMessage::ParamByName(LPCWSTR pwszName) const
{
	return m_pParams->Find(pwszName);
}

CParam CMessage::Param(LPCWSTR pwszName, const CComVariant& defVal) const
{
	if (const CParam* p = ParamByName(pwszName))
	{
		return *p;
	}
	return CParam(pwszName, defVal);
}

CParam CMessage::Param(LPCWSTR pwszName, LONGLONG defVal) const
{
	CComVariant v;
	v.vt = VT_I8;
	v.llVal = defVal;
	return Param(pwszName, v);
}

CParam CMessage::Param(LPCWSTR pwszName, ULONGLONG defVal) const
{
	CComVariant v;
	v.vt = VT_UI8;
	v.ullVal = defVal;
	return Param(pwszName, v);
}

bool CMessage::Remove(LPCWSTR pwszName)
{
	return m_pParams->Remove(pwszName);
}

bool CMessage::IsParam(LPCWSTR pwszName) const
{
	return m_pParams->Find(pwszName) != NULL;
}

DWORD CMessage::ParamsCount() const
{
	return m_pParams->GetSize();
}

ParamsList CMessage::Params() const
{
	ParamsList list;
	CParamsTable::iterator it = m_pParams->begin();
	CParamsTable::iterator end = m_pParams->end();
	
	for (; it != end; ++it)
	{
		list.push_back(*it);
	}
	
	return list;
}

// Packing operations

#if _EVTMODEL_VER < 2

bool CMessage::CanPack() const
{
	CParamsTable::iterator it = m_pParams->begin();
	CParamsTable::iterator end = m_pParams->end();

	for (; it != end; ++it)
	{
		if (!(*it).CanPack())
		{
			return false;
		}
	}

	return true;
}

DWORD CMessage::PackedSize() const
{
	DWORD dwSize = sizeof(MESSAGEHEADER);
	CParamsTable::iterator it = m_pParams->begin();
	CParamsTable::iterator end = m_pParams->end();

	for (; it != end; ++it)
	{
		dwSize += (*it).PackedSize();
	}

	return dwSize;
}

DWORD CMessage::Pack(LPVOID lpBuffer) const
{
	if (!CanPack())
	{
		throw std::exception("Cannot pack message - not packable");
	}

	PMESSAGEHEADER	pMH = (PMESSAGEHEADER)lpBuffer;
	char*			pParam = pMH->cData;
	DWORD			dwSize = PackedSize();

	if (::IsBadWritePtr(lpBuffer, dwSize) != 0)
	{
		throw std::exception("Cannot write message buffer - IsBadWritePtr() failed");
	}

	ZeroMemory(lpBuffer, dwSize);
	pMH->dwTotalSize = dwSize;

	::WideCharToMultiByte(CP_ACP, 0, m_Name, -1, pMH->szName, MAX_NAMELEN, NULL, NULL);

	CParamsTable::iterator it = m_pParams->begin();
	CParamsTable::iterator end = m_pParams->end();
	for (; it != end; ++it)
	{
		(*it).Pack(pParam);
		pParam += (*it).PackedSize();
	}

	pMH->dwParamCount = ParamsCount();
	pMH->ullSignature = MSG_SIGNATURE;
	
	return dwSize;
}

void CMessage::Unpack(const MESSAGEHEADER* pmh)
{
	Clear();

	PPARAMHEADER pParam = (PPARAMHEADER)pmh->cData;

	if (IsBadMessage(pmh))
	{
		throw std::exception("Cannot unpack message - bad binary format");
	}

	::MultiByteToWideChar(CP_ACP, 0, pmh->szName, -1, m_Name, MAX_NAMELEN);
	m_Name[MAX_NAMELEN] = L'\0';

	for (DWORD i = 0; i < pmh->dwParamCount; i++)
	{
		AddParam(CParam(pParam));
		pParam = (PPARAMHEADER)((LPBYTE)pParam + pParam->dwTotalSize);
	}	
}

#else

// EvtModel Version 2.0 - not implemented yet :)

#endif

// Comparison operators

bool CMessage::operator ==(const CMessage& msg) const
{
	if (!operator ==(msg.Name))
	{
		return false;
	}

	return *m_pParams == *msg.m_pParams;
}

bool CMessage::operator ==(LPCWSTR pwszName) const
{
	return wcscmp(m_Name, pwszName) == 0;
}

bool CMessage::operator !=(const CMessage& msg) const
{
	return !operator ==(msg);
}

bool CMessage::operator !=(LPCWSTR pwszName) const
{
	return wcscmp(m_Name, pwszName) != 0;
}

bool CMessage::operator <(const CMessage& msg) const
{
	return *m_pParams < *msg.m_pParams;
}

// Conversion operations

std::wstring CMessage::Dump() const
{
	std::wstring s;
	s.reserve(4096);

	s = std::wstring(L"Name: ") + GetName();

	CParamsTable::iterator it = m_pParams->begin();
	CParamsTable::iterator end = m_pParams->end();
	for (; it != end; ++it)
	{
		wchar_t wszBuf[256] = {0};
		(*it).Dump(wszBuf, sizeof(wszBuf) / sizeof(wszBuf[0]));
		s += std::wstring(L"; ") + wszBuf;
	}

	return s;
}

void CMessage::Dump(LPWSTR pwszBuf, DWORD dwMaxSize) const
{
	ZeroMemory(pwszBuf, dwMaxSize);

	size_t len = 0;
	_snwprintf(pwszBuf, dwMaxSize - 1, L"Name: %s", this->Name);

	CParamsTable::iterator it = m_pParams->begin();
	CParamsTable::iterator end = m_pParams->end();
	for (; (it != end) && (len < dwMaxSize - 1); ++it)
	{
		len += wcslen(pwszBuf + len);
		wcsncpy(pwszBuf + len, L"; ", dwMaxSize - 1 - len);
		len += wcslen(pwszBuf + len);
		if (len < dwMaxSize - 1)
		{
			(*it).Dump(pwszBuf + len, dwMaxSize - 1 - len);
		}
	}
}

void CMessage::ToVariant(CComVariant& v) const
{
	CPackedMsg pm(*this);
	SAFEARRAY* psa = NULL;
	SAFEARRAYBOUND bound[1] = {pm.Size(), 0};

	psa = ::SafeArrayCreate(VT_UI1, 1, bound);

	for (DWORD i = 0; i < pm.Size(); i++)
	{
		::SafeArrayPutElement(psa, (LONG*)&i, ((LPBYTE)pm()) + i);
	}

	v.vt = VT_ARRAY | VT_UI1;
	v.parray = psa;
}

bool CMessage::FromVariant(const CComVariant& v)
{
	if (v.vt != (VT_ARRAY | VT_UI1))
	{
		return false;
	}

	SAFEARRAY* psa = v.parray;
	LONG lPackedSize = 0;
	::SafeArrayGetUBound(psa, 1, &lPackedSize);
	BYTE* pPackedMsg = new BYTE[lPackedSize];

	for (LONG i = 0; i < lPackedSize; i++)
	{
		::SafeArrayGetElement(psa, &i, pPackedMsg + i);
	}

	if (IsBadMessage((PMESSAGEHEADER)pPackedMsg))
	{
		return false;
	}

	Clear();
	Unpack((PMESSAGEHEADER)pPackedMsg);

	return true;
}

std::wstring CMessage::ToXML() const
{
	return std::wstring();
}

bool CMessage::FromXML(LPCWSTR pwszXML)
{
	return false;
}

// Checked parameter operations

const CParam& CMessage::SafeReadParam(LPCWSTR pwszName, 
									  CMessage::CheckedType type) const
{
	CheckParam(pwszName, type, CMessage::Mandatory);
	return *ParamByName(pwszName);
}

CParam CMessage::SafeReadParam(LPCWSTR pwszName, 
							   CMessage::CheckedType type, 
							   const CComVariant& defVal) const
{
	CheckParam(pwszName, type, CMessage::Optional);
	const CParam* pParam = ParamByName(pwszName);
	return (pParam) ? *pParam : CParam(pwszName, defVal);
}

void CMessage::CheckParam(LPCWSTR pwszName, 
						  CMessage::CheckedType type, 
						  CMessage::ParamType ptype) const
{
	const CParam* pParam = ParamByName(pwszName);

	if (pParam == NULL && ptype == CMessage::Mandatory)
	{
		throw std::exception("Missing mandatory parameter");
	}

	if (pParam == NULL && ptype == CMessage::Optional)
	{
		return;
	}

	if (ptype == Forbidden)
	{
		if (pParam == NULL)
		{
			return;
		}
		throw std::exception("Existing forbidden parameter");
	}

	switch (type)
	{
		case Int:
		case Int64:
			if (!pParam->IsInt())
			{
				throw std::exception("Param must be Int type");
			}
			break;

		case String:
			if (!pParam->IsString())
			{
				throw std::exception("Param must be String type");
			}
			break;

		case RawData:
			if (!pParam->IsRawData())
			{
				throw std::exception("Param must be RawData type");
			}
			break;

		case Float:
			if (!pParam->IsFloat())
			{
				throw std::exception("Param must be Float type");
			}
			break;

		case Bool:
			if (!pParam->IsBool())
			{
				throw std::exception("Param must be Bool type");
			}
			break;

		case DateTime:
			if (!pParam->IsDateTime())
			{
				throw std::exception("Param must be DateTime type");
			}
			break;
	}
}

int CMessage::ParamAsEnum(LPCWSTR pwszName, 
						  LPCWSTR pwszEnumValues) const
{
	std::wstring sParam = SafeReadParam(pwszName, CMessage::String).AsWideStr();
	std::wstring sValues = pwszEnumValues;

	int iEnum = 0;
	for (LPWSTR p = (LPWSTR)sValues.c_str(), q = NULL; *p != L'\0'; iEnum++)
	{
		q = wcstok(p, L";,");
		
		CAtlStringW sCmp(p);
		sCmp.Trim();
		
		if (sCmp.CompareNoCase(sParam.c_str()) == 0)
		{
			return iEnum;
		}
		
		if (q != NULL)
		{
			p += wcslen(p) + 1;
		}
		else
		{
			break;
		}
	}

	throw std::exception("Parameter value must be one of the specified values");
	return -1;
}

/************************************************************************/
/* CPackedMsg class implementation                                      */
/************************************************************************/

CPackedMsg::CPackedMsg(const CMessage& msg)
{
	m_pMH = (PMESSAGEHEADER)new BYTE[msg.PackedSize()];
	msg.Pack(m_pMH);
}

PMESSAGEHEADER CPackedMsg::operator ()() const
{
	return m_pMH;
}

CPackedMsg::~CPackedMsg()
{
	delete m_pMH;
}

DWORD CPackedMsg::Size() const
{
	return m_pMH->dwTotalSize;
}

// Helper functions

bool IsBadMessage(const MESSAGEHEADER* pmh)
{
	if (pmh->ullSignature != MSG_SIGNATURE)
	{
		return true;
	}

	if (IsBadReadPtr(pmh, sizeof(MESSAGEHEADER)))
	{
		return true;
	}

	if (IsBadReadPtr(pmh, pmh->dwTotalSize))
	{
		return true;
	}

	return false;
}

std::wstring DumpMessage(const MESSAGEHEADER* pmh)
{
	return CMessage(pmh).Dump();
}

void DumpMessage(const MESSAGEHEADER* pmh, LPWSTR pwszBuf, DWORD dwMaxSize)
{
	CMessage(pmh).Dump(pwszBuf, dwMaxSize);
}

/************************************************************************/
/* Readonly message extensions                                          */
/************************************************************************/

// CROParam implementation

LPCSTR CROParam::GetName() const
{
	return this->szName;
}

CComVariant CROParam::GetValue() const
{
	// BSTR
	if (this->vValue.vt == VT_BSTR)
	{
		return CComVariant((OLECHAR*)this->cData);
	}
	// Other
	else
	{
		return CComVariant(this->vValue);
	}

	return CComVariant();
}

LPCVOID CROParam::GetRawData() const
{
	if (IsRawData())
	{
		return this->cData;
	}
	return NULL;
}

DWORD CROParam::GetRawDataSize() const
{
	return this->dwRawDataSize;
}

bool CROParam::IsInt() const
{
	switch (this->vValue.vt)
	{
		case VT_I1:
		case VT_UI1:
		case VT_I2:
		case VT_UI2:
		case VT_I4:
		case VT_UI4:
		case VT_I8:
		case VT_UI8:
		case VT_INT:
		case VT_UINT:
			return true;
	}

	return false;
}

bool CROParam::IsFloat() const
{
	VARTYPE vt = this->vValue.vt;
	return (vt == VT_R4 || vt == VT_R8);
}

bool CROParam::IsString() const
{
	return (this->vValue.vt == VT_BSTR);
}

bool CROParam::IsBool() const
{
	return (this->vValue.vt == VT_BOOL);
}

bool CROParam::IsDateTime() const
{
	return (this->vValue.vt == VT_DATE);
}

bool CROParam::IsRawData() const
{
	VARTYPE vt = this->vValue.vt;
	return (vt == VT_EMPTY || vt == VT_NULL) && (this->dwRawDataSize > 0);
}

LONG CROParam::AsInt() const
{
	return (LONG)AsInt64();
}

ULONG CROParam::AsUInt() const
{
	return (ULONG)AsInt64();
}

LONGLONG CROParam::AsInt64() const
{
	switch (this->vValue.vt)
	{
		case VT_I1:		return (LONGLONG)this->vValue.cVal;
		case VT_UI1:	return (LONGLONG)this->vValue.bVal;
		case VT_I2:		return (LONGLONG)this->vValue.iVal;
		case VT_UI2:	return (LONGLONG)this->vValue.uiVal;
		case VT_I4:		return (LONGLONG)this->vValue.lVal;
		case VT_UI4:	return (LONGLONG)this->vValue.ulVal;
		case VT_I8:		return (LONGLONG)this->vValue.llVal;
		case VT_UI8:	return (LONGLONG)this->vValue.ullVal;
		case VT_INT:	return (LONGLONG)this->vValue.intVal;
		case VT_UINT:	return (LONGLONG)this->vValue.uintVal;
		case VT_R4:		return (LONGLONG)this->vValue.fltVal;
		case VT_R8:		return (LONGLONG)this->vValue.dblVal;
		case VT_BSTR:	return _wtoi((LPCWSTR)this->cData);
		case VT_BOOL:	return (this->vValue.boolVal == VARIANT_TRUE) ? 1 : 0;
		case VT_DATE:	return (LONGLONG)this->vValue.date;
	}

	throw std::exception("Cannot convert parameter to Int64");
	return 0;
}

ULONGLONG CROParam::AsUInt64() const
{
	return (ULONGLONG)AsInt64();
}

std::string CROParam::AsStr() const
{
	std::wstring ws = AsWideStr();
	int len = ws.length();
	std::auto_ptr<char> s(new char[len + 1]);
	ZeroMemory(s.get(), len + 1);

	::WideCharToMultiByte(
		CP_ACP, 
		0, ws.c_str(), len, 
		s.get(), len + 1, NULL, NULL);

	return s.get();
}

std::wstring CROParam::AsWideStr() const
{
	wchar_t wszBuf[256] = {0};

	if (IsInt())
	{
		switch (this->vValue.vt)
		{
			case VT_I1:
			case VT_I2:
			case VT_I4:
			case VT_I8:
			case VT_INT:
				swprintf(wszBuf, L"%I64i", AsInt64());
				break;

			default:
				swprintf(wszBuf, L"%I64u", AsUInt64());
				break;
		}
	}
	else if (IsFloat())
	{
		swprintf(wszBuf, L"%f", AsDouble());
	}
	else if (IsString())
	{
		return (LPCWSTR)this->cData;
	}
	else if (IsBool())
	{
		return (this->vValue.boolVal == VARIANT_TRUE) ? L"true" : L"false";
	}
	else if (IsDateTime())
	{
		CComBSTR bstr;
		if (FAILED(::VarBstrFromDate(
			this->vValue.date, LANG_USER_DEFAULT, LOCALE_NOUSEROVERRIDE, &bstr)))
		{
			return L"";
		}
		else
		{
			return (LPCWSTR)bstr;
		}
	}
	else if (this->vValue.vt == VT_EMPTY || this->vValue.vt == VT_NULL)
	{
		return L"";
	}
	else
	{
		throw std::exception("Cannot convert parameter to String");
	}

	return std::wstring(wszBuf);	
}

CString CROParam::AsString() const
{
	return CString(AsWideStr().c_str());
}

bool CROParam::AsBool() const
{
	if (IsBool())
	{
		return (this->vValue.boolVal == VARIANT_TRUE);
	}
	else if (IsInt())
	{
		return AsInt64() != 0;
	}
	else if (IsFloat())
	{
		return AsDouble() != 0.0;
	}
	else if (IsString())
	{
		if (!wcsicmp(this->vValue.bstrVal, L"true"))
		{
			return true;
		}
		if (!wcsicmp(this->vValue.bstrVal, L"false"))
		{
			return false;
		}
	}

	throw std::exception("Cannot convert parameter to Bool");
	return 0;
}

FLOAT CROParam::AsFloat() const
{
	return (FLOAT)AsDouble();
}

DOUBLE CROParam::AsDouble() const
{
	if (IsString())
	{
		return (DOUBLE)_wtof(this->vValue.bstrVal);
	}
	else if (IsInt())
	{
		return (DOUBLE)AsInt64();
	}

	switch (this->vValue.vt)
	{
		case VT_R4: return (DOUBLE)this->vValue.fltVal;
		case VT_R8: return this->vValue.dblVal;
	}

	throw std::exception("Cannot convert parameter to Double");
	return 0.0;
}

DATE CROParam::AsDateTime() const
{
	if (IsFloat())
	{
		return (DATE)AsDouble();
	}
	else if (IsString())
	{
		DATE date = 0.0;
		if (SUCCEEDED(::VarDateFromStr(
			(OLECHAR*)this->cData, 
			LANG_USER_DEFAULT, 
			0, 
			&date)))
		{
			return date;
		}
	}
	else if (IsDateTime())
	{
		return this->vValue.date;
	}

	throw std::exception("Cannot convert parameter to DateTime");
	return 0.0;
}

int CROParam::Dump(LPWSTR pwszBuf, int cbBuf) const
{
	ZeroMemory(pwszBuf, cbBuf);

	int k = _snwprintf(pwszBuf, cbBuf, L"%S = ", GetName());
	if (k > 0)
	{
		cbBuf -= k;
		pwszBuf += k;
	}
	else
	{
		return k;
	}

	if (IsRawData())
	{
		return _snwprintf(pwszBuf, cbBuf, L"Raw data, size=%u", GetRawDataSize());
	}

	switch (this->vValue.vt)
	{
		case VT_I8:
		case VT_UI8:
			return k + _snwprintf(
				pwszBuf, cbBuf, L"0x%08X-%08X", 
				DWORD(this->vValue.llVal >> 32), DWORD(this->vValue.llVal & 0xFFFFFFFF));
			break;

		case VT_I1:
		case VT_UI1:
		case VT_I2:
		case VT_UI2:
		case VT_I4:
		case VT_UI4:
		case VT_INT:
		case VT_UINT:
		case VT_R4:
		case VT_R8:
		case VT_BOOL:
		case VT_DATE:
			return k + _snwprintf(pwszBuf, cbBuf, L"%s", AsWideStr().c_str());

		case VT_BSTR:
			if (::wcslen((LPCWSTR)this->cData) > 128)
			{
				return k + _snwprintf(pwszBuf, cbBuf, L"\"%128s\"...", this->cData);
			}
			return k + _snwprintf(pwszBuf, cbBuf, L"\"%s\"", this->cData);

		default:
			return k + _snwprintf(pwszBuf, cbBuf, L"Unknown Type %hu", this->vValue.vt);
	}

	return k;
}

// CROMessage implementation

CROMessage::CROMessage() 
	: m_pMH(NULL), m_bDetached(true)
{

}

CROMessage::CROMessage(const CROMessage& msg)
{
	Copy(msg.m_pMH, false);
}

CROMessage::CROMessage(const PMESSAGEHEADER pmh, bool bDetached)
{
	Copy(pmh, bDetached);
}

CROMessage::~CROMessage()
{
	Free();
}

void CROMessage::Copy(const PMESSAGEHEADER pmh, bool bDetached)
{
	m_bDetached = bDetached;
	if (bDetached)
	{
		m_pMH = pmh;
	}
	else
	{
		DWORD dwSize = pmh->dwTotalSize;
		m_pMH = (PMESSAGEHEADER)malloc(dwSize);
		memcpy(m_pMH, pmh, dwSize);
	}
}

void CROMessage::Free()
{
	if (!m_bDetached && m_pMH != NULL)
	{
		free(m_pMH);
		m_pMH = NULL;
	}
}

CROMessage& CROMessage::operator =(const CROMessage& msg)
{
	Free();
	Copy(msg.m_pMH, false);
	return *this;
}

LPCSTR CROMessage::GetName() const
{
	return m_pMH->szName;
}

const CROParam& CROMessage::operator [](LPCSTR pszName) const
{
	const CROParam* p = ParamByName(pszName);
	if (p == NULL)
	{
		throw std::exception("Parameter not found");
	}
	return *p;
}

const CROParam* CROMessage::ParamByName(LPCSTR pszName) const
{
	PPARAMHEADER pParam = (PPARAMHEADER)m_pMH->cData;

	for (DWORD i = 0; i < m_pMH->dwParamCount; i++)
	{
		if (!strcmp(pParam->szName, pszName))
		{
			return (CROParam*)pParam;
		}
		pParam = (PPARAMHEADER)((LPBYTE)pParam + pParam->dwTotalSize);
	}

	return NULL;
}

bool CROMessage::IsParam(LPCSTR pszName) const
{
	return ParamByName(pszName) != NULL;
}

DWORD CROMessage::ParamsCount() const
{
	return m_pMH->dwParamCount;
}

void CROMessage::Dump(LPWSTR pwszBuf, int cbBuf) const
{
	if (cbBuf < 2)
	{
		return;
	}

	PPARAMHEADER pParam = (PPARAMHEADER)m_pMH->cData;

	int nLen = _snwprintf(pwszBuf, cbBuf - 1, L"Name: %S", m_pMH->szName);
	if (nLen < 0)
	{
		return;
	}
	else
	{
		cbBuf -= nLen;
		pwszBuf += nLen;
	}

	for (DWORD i = 0; i < m_pMH->dwParamCount; i++)
	{
		if (nLen < 3)
		{
			break;
		}
		else
		{
			*pwszBuf++ = L';';
			*pwszBuf++ = L' ';
			cbBuf -= 2;
		}

		nLen = ((CROParam*)pParam)->Dump(pwszBuf, cbBuf - 1);
		if (nLen < 0)
		{
			break;
		}
		else
		{
			cbBuf -= nLen;
			pwszBuf += nLen;
			pParam = (PPARAMHEADER)((LPBYTE)pParam + pParam->dwTotalSize);
		}
	}
}

PMESSAGEHEADER CROMessage::GetMsgHeader() const
{
	return m_pMH;
}

PMESSAGEHEADER CROMessage::Detach()
{
	m_bDetached = true;
	return m_pMH;
}

#pragma warning(pop)
