// dllmain.cpp : Implementation of DllMain.

#include "stdafx.h"
#include "resource.h"
#include "VXMLCom_i.h"
#include <DbgHelp.h>
#include <atlstr.h>
#include <ATLComTime.h>
#include <comdef.h>
#include "..\Engine\EngineLog.h"
#include "sid.h"

#include "dllmain.h"

#include "mdump.h"
//#include "singleton.h"
#include <eh.h>
#include <Psapi.h>
#include <sstream>

typedef unsigned int exception_code_t;

namespace Global
{
	ObjectId g_scriptID = 0;
}

static const wchar_t* opDescription(const ULONG opcode)
{
	switch (opcode) {
	case 0: return L"read";
	case 1: return L"write";
	case 8: return L"user-mode data execution prevention (DEP) violation";
	default: return L"unknown";
	}
}

static const wchar_t* seDescription(const exception_code_t& code)
{
	switch (code) {
	case EXCEPTION_ACCESS_VIOLATION:         return L"EXCEPTION_ACCESS_VIOLATION";
	case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:    return L"EXCEPTION_ARRAY_BOUNDS_EXCEEDED";
	case EXCEPTION_BREAKPOINT:               return L"EXCEPTION_BREAKPOINT";
	case EXCEPTION_DATATYPE_MISALIGNMENT:    return L"EXCEPTION_DATATYPE_MISALIGNMENT";
	case EXCEPTION_FLT_DENORMAL_OPERAND:     return L"EXCEPTION_FLT_DENORMAL_OPERAND";
	case EXCEPTION_FLT_DIVIDE_BY_ZERO:       return L"EXCEPTION_FLT_DIVIDE_BY_ZERO";
	case EXCEPTION_FLT_INEXACT_RESULT:       return L"EXCEPTION_FLT_INEXACT_RESULT";
	case EXCEPTION_FLT_INVALID_OPERATION:    return L"EXCEPTION_FLT_INVALID_OPERATION";
	case EXCEPTION_FLT_OVERFLOW:             return L"EXCEPTION_FLT_OVERFLOW";
	case EXCEPTION_FLT_STACK_CHECK:          return L"EXCEPTION_FLT_STACK_CHECK";
	case EXCEPTION_FLT_UNDERFLOW:            return L"EXCEPTION_FLT_UNDERFLOW";
	case EXCEPTION_ILLEGAL_INSTRUCTION:      return L"EXCEPTION_ILLEGAL_INSTRUCTION";
	case EXCEPTION_IN_PAGE_ERROR:            return L"EXCEPTION_IN_PAGE_ERROR";
	case EXCEPTION_INT_DIVIDE_BY_ZERO:       return L"EXCEPTION_INT_DIVIDE_BY_ZERO";
	case EXCEPTION_INT_OVERFLOW:             return L"EXCEPTION_INT_OVERFLOW";
	case EXCEPTION_INVALID_DISPOSITION:      return L"EXCEPTION_INVALID_DISPOSITION";
	case EXCEPTION_NONCONTINUABLE_EXCEPTION: return L"EXCEPTION_NONCONTINUABLE_EXCEPTION";
	case EXCEPTION_PRIV_INSTRUCTION:         return L"EXCEPTION_PRIV_INSTRUCTION";
	case EXCEPTION_SINGLE_STEP:              return L"EXCEPTION_SINGLE_STEP";
	case EXCEPTION_STACK_OVERFLOW:           return L"EXCEPTION_STACK_OVERFLOW";
	default: return L"UNKNOWN EXCEPTION";
	}
}

static std::wstring information(struct _EXCEPTION_POINTERS* ep, bool has_exception_code = false, exception_code_t code = 0)
{
	HMODULE hm;
	::GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, static_cast<LPCTSTR>(ep->ExceptionRecord->ExceptionAddress), &hm);
	MODULEINFO mi;
	::GetModuleInformation(::GetCurrentProcess(), hm, &mi, sizeof(mi));
	char fn[MAX_PATH];
	::GetModuleFileNameExA(::GetCurrentProcess(), hm, fn, MAX_PATH);

	std::wostringstream oss;
	oss << L"SE " << (has_exception_code ? seDescription(code) : L"") << L" at address 0x" << std::hex << ep->ExceptionRecord->ExceptionAddress << std::dec
		<< L" inside " << fn << L" loaded at base address 0x" << std::hex << mi.lpBaseOfDll << L"\n";

	if (has_exception_code && (
		code == EXCEPTION_ACCESS_VIOLATION ||
		code == EXCEPTION_IN_PAGE_ERROR)) {
		oss << L"Invalid operation: " << opDescription(ep->ExceptionRecord->ExceptionInformation[0]) << L" at address 0x" << std::hex << ep->ExceptionRecord->ExceptionInformation[1] << std::dec << L"\n";
	}

	if (has_exception_code && code == EXCEPTION_IN_PAGE_ERROR) {
		oss << L"Underlying NTSTATUS code that resulted in the exception " << ep->ExceptionRecord->ExceptionInformation[2] << L"\n";
	}

	return oss.str();
}

int __cdecl CrtDbgHook(int nReportType, char* szMsg, int* pnRet)
{
	CEngineLog log;
	log.Init(L"finest", L"C:\\IS3\\Logs\\vxml_dump_%y-%m-%d.log");
	log.SetSID(Global::g_scriptID);
	log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _bstr_t(szMsg));

	__debugbreak();

    return TRUE;//Return true - Abort,Retry,Ignore dialog will *not* be displayed
}

LONG WINAPI TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
{
	CEngineLog log;
	log.Init(L"finest", L"C:\\IS3\\Logs\\vxml_dump_%y-%m-%d.log");
	log.SetSID(Global::g_scriptID);

	CString threadID; 
	threadID.Format(_T("_p%d_t%d_"), GetCurrentProcessId(),::GetCurrentThreadId());

	log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"Minidump creation started");
	CString strDumpName = _T("c:\\is3\\VXML") + threadID + COleDateTime::GetCurrentTime().Format(_T("_%Y-%m-%d_%H-%M-%S")) + _T(".dmp");
	LONG retval = EXCEPTION_CONTINUE_SEARCH;

	HANDLE hFile = ::CreateFile( strDumpName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );

	if (hFile!=INVALID_HANDLE_VALUE)
	{
		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"Minidump file opened");
		_MINIDUMP_EXCEPTION_INFORMATION ExInfo;

		ExInfo.ThreadId = ::GetCurrentThreadId();
		ExInfo.ExceptionPointers = pExceptionInfo;
		ExInfo.ClientPointers = NULL;

		// write the dump
		BOOL bOK = ::MiniDumpWriteDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );
		if (bOK)
		{
			exception_code_t code = 0;
			if (pExceptionInfo)
			{
				code = pExceptionInfo->ExceptionRecord->ExceptionCode;
			}
			log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"Minidump write done");
			log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"%s", information(pExceptionInfo, true, code).c_str());
			retval = EXCEPTION_EXECUTE_HANDLER;
		}
		else
			log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"Minidump write failed");

		::CloseHandle(hFile);
	}
	else
		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"INVALID_HANDLE_VALUE");
	return retval;
}
#include "VXML/VXMLTags.h"
CVXMLComModule _AtlModule;
HINSTANCE g_hInst;

ObjectId GetGlobalScriptId()
{
	return _AtlModule.Get();
}

void SetGlobalScriptId(ObjectId aObjectId)
{
	_AtlModule.Set(aObjectId);
}

LPCTSTR c_lpszModuleName = _T("VXMLCom.dll");

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	g_hInst = hInstance;
	 //_CrtSetReportHook2(_CRT_RPTHOOK_INSTALL, CrtDbgHook);
	 //::SetUnhandledExceptionFilter( &TopLevelFilter );
////	singleton_auto_pointer<MiniDumper> g_MiniDump;
	//VXML::EventScope scope, prompt;
	//VXML::TagPtr pTag(VXML::CTagFactory_c::CreateTag(L"factory_init", NULL, 0/*, scope, prompt, L"DllMain"*/));

	BOOL blResult = _AtlModule.DllMain(dwReason, lpReserved); 
	//_CrtDumpMemoryLeaks();
	return blResult;
}
