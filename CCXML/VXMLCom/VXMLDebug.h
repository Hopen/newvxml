///************************************************************************/
///* Name     : VXMLCom\VXMLDebug.h                                       */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-IT                                                  */
///* Date     : 13 Dec 2011                                               */
///************************************************************************/
//#include "sv_helpclasses.h"
//#include "sv_sysobj.h"
//#include "VXMLSession.h"
//
//namespace VXML
//{
//	class VXMLDebugBeholder: public IDebuger
//	{
//		struct TBreakPoint
//		{
//			VXMLString  filename;
//			unsigned int line;
//			VXMLString  condition;
//		};
//
//		class CBreakPointByLineFinder
//		{
//		public:
//			CBreakPointByLineFinder(unsigned int _line):m_line(_line)
//			{
//			}
//
//			bool operator()(const TBreakPoint _point)
//			{
//				return m_line == _point.line;
//			}
//
//		private:
//			unsigned int m_line;
//		};
//
//	public:
//		VXMLDebugBeholder(IEngineCallback* _callback, IEngineVM* _engine);
//		virtual ~VXMLDebugBeholder();
//
//		// definition interface
//		void Wait        ( unsigned int _line/*, unsigned int _count = 1*/ );
//		void Increase    ( /*unsigned int _count = 1*/ );
//		void SetAuxEvent ( CMessage& msg, bool _in );
//		void Init        ( CMessage& init_msg, SessionPtr _session);
//		void Log         ( VXMLString _log        );
//		void EndScript   ( const VXMLString& _reason, const int& _code)const;
//		// engine interface
//		//void SetEvent(PMESSAGEHEADER pMsgHeader);
//		BOOL WINAPI GetScriptName(BSTR* pbstrScriptName){return TRUE;}
//		BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
//
//	private:
//		void Add();
//		void Release();
//		void WaitOffline (unsigned int _line/*, unsigned int _count = 1*/);
//		void WaitOnline  (unsigned int _line/*, unsigned int _count = 1*/);
//		void SendToScriptoGen (CMessage& msg)const;
//		std::wstring GetSymInfo(const std::wstring& _name)const;
//
//		typedef std::list<TBreakPoint> BreakQueue;
//	private:
//		//unsigned int m_nCounter;
//		SVSysObj::CEvent m_evtReady;
//		SVSysObj::CEvent m_evtRun;
//		SessionPtr       m_pSession;
//		bool             m_debugflag;
//		bool             m_forcebreak;
//		ObjectId         m_ScriptoGenAddress;
//		BreakQueue       m_BreakStack;
//		bool             m_bWaitForRun;
//	};
//}
///******************************* eof *************************************/