/************************************************************************/
/* Name     : VXMLCom\KeepAliveProvider.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 22 Apr 2015                                               */
/************************************************************************/
#pragma once

#include <list>
#include <mutex>
#include <atomic>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include "sv_sysobj.h"
#include "VXMLBase.h"

class CKeepAliveProvider
{
public:
	void SubscribeTimer(const __int64&);
	void UnsubscribeTimer(const __int64&);

	void MakeKeepAliveMsg(const __int64& scriptID);
	void Init(const unsigned int& interval, VXML::IEngineCallbackPtr pExec, VXML::SystemLogPtr pLog);
	void Stop();

	size_t IncreaseDeadCounter() { return ++m_DeadCounter; }
	void ResetDeadCounter() { m_DeadCounter = 0; }

private:

	void StartThread();
	void JoinThread();
public:
	CKeepAliveProvider();
	~CKeepAliveProvider();
private:

	void ThreadProc();

private:
	typedef std::list <__int64> SubscribersCollector;
	SubscribersCollector m_subscribers;

	std::mutex m_mutex;

	std::shared_ptr < boost::thread >  m_thread;
	std::atomic_bool m_bThreadStarted;
	VXML::IEngineCallbackPtr m_engine;
	VXML::SystemLogPtr       m_log;

	SVSysObj::CEvent		m_ThreadStarted;
	unsigned int			m_iLongPoolTimer;
	unsigned int			m_curSec;

	std::atomic<size_t> m_DeadCounter;
};

/******************************* eof *************************************/