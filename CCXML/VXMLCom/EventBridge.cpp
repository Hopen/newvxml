// EventBridge.cpp : Implementation of CNameTable

#include "stdafx.h"
#include "sv_strutils.h"
#include "EventBridge.h"

#include "EngineConfig.h"
#include "..\Engine\EngineLog.h"
//using namespace enginelog;

namespace VXML
{
	CNameTable::CNameTable(const std::wstring& aName)
		: InstanceController(L"CNameTable", L"c:\\is3\\logs\\ccxml\\vxml_temp_%y-%m-%d.log")
		, m_name(aName)
	{
		LogDefaultCtor();
		LogPlace(L"Name: %s", m_name.c_str());
	}

	CNameTable::CNameTable(const CMessage& rhs)
		: InstanceController(L"CNameTable", L"c:\\is3\\logs\\ccxml\\vxml_temp_%y-%m-%d.log")
		, m_name(rhs.GetName())
	{
		LogDefaultCtor();
		LogPlace(L"Name: %s", m_name.c_str());

		ParamsList params = rhs.Params();

		for (const CParam& param : params)
		{
			this->Add(param.GetName(), param.GetValue(), false);
		}
	}
	void CNameTable::Clone(const CNameTable& rhs)
	{
		this->Clear();
		this->m_bActAsAssocArray = rhs.m_bActAsAssocArray;

		NameContainer::const_iterator cit = rhs.m_Names.begin();
		while (cit != rhs.m_Names.end())
		{
			this->m_Names[cit->first] = cit->second;
			++cit;
		}
	}

	CNameTable::~CNameTable()
	{
		LogDtor();
		LogPlace(L"Name: %s", m_name.c_str());
	}

	void CNameTable::SetValue(const VXMLString & sName, const CComVariant & val)
	{
		NameContainer::iterator it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			//throw format_wstring(L"Name '%s' not found", sName.c_str());
			throw std::runtime_error(std::string("CNameTable::SetValue: '") + wtos(sName) + "' not found");
		}
		it->second.SetValue(val);
	}

	const VAR CNameTable::GetValue(const VXMLString & sName) const
	{
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			//throw format_wstring(L"Name '%s' not found", sName.c_str());
			throw std::runtime_error(std::string("CNameTable::GetValue: '") + wtos(sName) + "' not found");
		}

		return it->second;
	}

	const VAR CNameTable::GetSafeValue(const VXMLString & sName) const
	{
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			static VAR empty;
			return empty;
		}

		return it->second;
	}

	VAR& CNameTable::GetUnsafeValue(const VXMLString & sName)
	{
		auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			//throw format_wstring(L"Name '%s' not found", sName.c_str());
			throw std::runtime_error(std::string("CNameTable::GetUnsafeValue: '") + wtos(sName) + "' not found");
		}

		return it->second;
	}

	const VAR* CNameTable::GetAddressOf(const VXMLString & sName) const
	{
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			return nullptr;
		}
		return &it->second;
	}

	VAR* CNameTable::Find(const VXMLString & sName)
	{
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			return nullptr;
		}
		return &it->second;
	}

	VAR& CNameTable::Add(const VXMLString& sName, const CComVariant& val, bool bReadOnly)
	{
		if (sName.empty())
		{
			throw std::runtime_error("Try add CNameTable variable with empty name");
		}

		VAR& var(m_Names[sName] = VAR(sName, val, bReadOnly));
		return var;
	}

	const CComVariant CNameTable::AddRef2(const VXMLString& sName, const CComVariant& val, bool bReadOnly)
	{
		CComVariant refval;
		refval.vt = VT_VARIANT | VT_BYREF;

		auto it = m_Names.find(sName);
		if (it != m_Names.end())
		{
			//it->second.vValue = val;
			it->second = VAR(sName, val, bReadOnly);
			refval.pvarVal = &it->second.vValue;
		}
		else
		{
			VAR& var(m_Names[sName] = VAR(sName, val, bReadOnly));
			refval.pvarVal = &var.vValue;
		}
		return refval;
	}


	bool CNameTable::Del(const VXMLString& sName)
	{
		NameContainer::iterator it = m_Names.find(sName);
		if (it != m_Names.end())
		{
			m_Names.erase(it);
			return true;
		}
		return false;
	}

	//CMessage CNameTable::ToMsg() const
	//{
	//	std::lock_guard<std::mutex> lock(m_mutex);
	//	CMessage msg;
	//	for (auto it = m_Names.begin(); it != m_Names.end(); ++it)
	//	{
	//		if (it->second.vValue.vt == VT_DISPATCH)
	//		{
	//			NameTableDispatch* bridge = static_cast<NameTableDispatch*>(it->second.vValue.pdispVal);
	//			if (bridge)
	//			{
	//				CMessage bridgeMsg = bridge->ToMsg<CMessage>();
	//				msg[it->first.c_str()] = bridgeMsg.Dump();
	//			}
	//			continue;
	//		}
	//		msg[it->first.c_str()] = it->second.vValue;
	//	}
	//	return msg;
	//}

	CMessage CNameTable::ToMsg() const
	{
		CMessage msg(m_name.c_str());
		for (auto it = m_Names.begin(); it != m_Names.end(); ++it)
		{
			if (it->second.vValue.vt == VT_BSTR)
			{
				msg[it->first.c_str()] = it->second.vValue.bstrVal;
			}

			else if (it->second.vValue.vt == VT_UI1
				|| it->second.vValue.vt == VT_UI2
				|| it->second.vValue.vt == VT_UI4
				|| it->second.vValue.vt == VT_UI8
				|| it->second.vValue.vt == VT_UINT)
			{
				msg[it->first.c_str()] = it->second.vValue.ullVal;
			}
			else if (it->second.vValue.vt == VT_I2
				|| it->second.vValue.vt == VT_I4
				|| it->second.vValue.vt == VT_I1
				|| it->second.vValue.vt == VT_I8
				|| it->second.vValue.vt == VT_INT)
			{
				msg[it->first.c_str()] = it->second.vValue.llVal;
			}
			else if (it->second.vValue.vt == VT_DISPATCH)
			{
				NameTableDispatch* bridge = static_cast<NameTableDispatch*>(it->second.vValue.pdispVal);
				if (bridge)
				{
					CMessage bridgeMsg = bridge->ToMsg<CMessage>();
					msg[it->first.c_str()] = bridgeMsg.Dump();
				}
				continue;
			}
			else
			{
				msg[it->first.c_str()] = L"Unknown type";//it->second.vValue;
			}

			//msg[it->first.c_str()] = it->second.vValue;
		}
		return msg;
	}

	void CNameTable::Clear()
	{
		m_Names.clear();
	}

	// CNameTable
	STDMETHODIMP CNameTable::GetTypeInfoCount(
		/* [out] */ UINT *pctinfo)
	{
		if (::IsBadWritePtr(pctinfo, sizeof(*pctinfo)))
		{
			return E_INVALIDARG;
		}

		*pctinfo = 0;
		return S_OK;
	}

	STDMETHODIMP CNameTable::GetTypeInfo(
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ ITypeInfo **ppTInfo)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP CNameTable::GetIDsOfNames(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId)
	{
		HRESULT hRes = S_OK;
		//if(this->m_bActAsAssocArray)
		//{
		try
		{
			for (UINT i = 0; i < cNames; i++)
			{
				std::wstring name(static_cast<wchar_t*>(rgszNames[i]));
				//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames \"%s\"", name.c_str());

				const VAR* pVar = GetAddressOf(rgszNames[i]);
				if (pVar != nullptr)
				{
					rgDispId[i] = reinterpret_cast<DISPID>(pVar);
				}
				else
				{
					//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames DISPID_UNKNOWN");
					rgDispId[i] = DISPID_UNKNOWN;
					hRes = DISP_E_UNKNOWNNAME;
					break;
				}
			}
		}
		catch (std::exception& exception)
		{
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames Exception: \"%s\"", stow(exception.what()).c_str());
			hRes = E_OUTOFMEMORY;
		}
		catch (...)
		{
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames Unexpacted Exception");
			hRes = E_OUTOFMEMORY;
		}

		//}
		return hRes;//bUnkName?hRes :__super::GetIDsOfNames(riid, rgszNames, cNames, lcid, rgDispId);
	}

	STDMETHODIMP CNameTable::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr)
	{
		// TODO: Add your implementation code here
		VAR* pVar = NULL;

		struct Error
		{
			HRESULT hr;
			VXMLString description;
			Error(HRESULT _hr, LPCWSTR pwszDescr)
				: hr(_hr), description(pwszDescr)
			{
			}
		};

		try
		{

			if (dispIdMember)
			{
				pVar = reinterpret_cast<VAR*>(dispIdMember);
				if (::IsBadReadPtr(pVar, sizeof(VAR)))
				{
					throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
				}
			}
			else
			{

			}
			if (pVar && pVar->vValue.vt == VT_DISPATCH)
			{
				VARIANT& val = pVar->vValue;

				::VariantClear(pVarResult);

				if (val.vt & VT_BYREF)
				{
					::VariantCopy(pVarResult, val.pvarVal);
				}
				else
				{
					::VariantCopy(pVarResult, &val);
				}
				return S_OK;
			}

			// Get
			if (pVar && (wFlags & DISPATCH_PROPERTYGET))
			{
				// Check pVarResult pointer
				if (::IsBadReadPtr(pVarResult, sizeof(VARIANT)))
				{
					throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid pVarResult parameter");
				}

				// Copy VAR to pVarResult

				// Note!!! If this value is VT_BYREF, than it is actually stored
				// in another name table (scope), and here we have only reference to it.
				// So, modify it only in referenced table.
				VARIANT& val = pVar->vValue;
				if (val.vt & VT_BYREF)
				{
					::VariantCopy(pVarResult, val.pvarVal);
				}
				else
				{
					::VariantCopy(pVarResult, &val);
				}
			}
			// Set
			else if (pVar && (wFlags & DISPATCH_PROPERTYPUT))
			{
				// Need second arg as assignment value
				if (pDispParams->cArgs < 1)
				{
					throw Error(E_INVALIDARG, L"Invalid number of parameters");
				}

				// Check supplied value pointer
				if (::IsBadReadPtr(&pDispParams->rgvarg[0], sizeof(VARIANT)))
				{
					throw Error(E_INVALIDARG, L"Invalid assignment r-value");
				}

				try
				{
					// Set VAR value
					pVar->SetValue(&pDispParams->rgvarg[0]);
				}
				catch (std::exception& e)
				{
					// Cannot set it
					throw Error(DISP_E_EXCEPTION, CComVariant(e.what()).bstrVal);
				}
			}
			else
			{
				::VariantCopy(pVarResult, &AsVariant());
			}
			return S_OK;
		}
		catch (Error& err)
		{
			// Check pExcepInfo structure pointer
			if (::IsBadReadPtr(pExcepInfo, sizeof(EXCEPINFO)))
			{
				return DISP_E_EXCEPTION;
			}

			// Fill pExcepInfo structure

			ZeroMemory(pExcepInfo, sizeof(EXCEPINFO));

			pExcepInfo->bstrDescription = ::SysAllocString(err.description.c_str());
			pExcepInfo->bstrSource = ::SysAllocString(L"");
			pExcepInfo->bstrHelpFile = ::SysAllocString(L"");
			pExcepInfo->scode = err.hr;

			return err.hr;
		}
		catch (...)
		{
			return DISP_E_EXCEPTION;
		}

	}

}