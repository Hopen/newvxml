/************************************************************************/
/* Name     : VXMLCom\KeepAliveProvider.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 22 Apr 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "KeepAliveProvider.h"
#include "EvtModel.h"
//#include "..\Engine\EngineLog.h"
#include "sv_strutils.h"
#include "common.h"

#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "..\Common\SystemLogEx.h"

CKeepAliveProvider::CKeepAliveProvider()
	: m_DeadCounter(0)
{
	m_bThreadStarted = false;
}

void CKeepAliveProvider::Init(const unsigned int& interval, VXML::IEngineCallbackPtr pExec, VXML::SystemLogPtr pLog)
{
	m_iLongPoolTimer = interval;
	m_engine      = pExec;
	m_log         = pLog;

	m_log->LogInfo( L"CKeepAliveProvider initializing...");
	m_log->LogInfo( L"CKeepAliveProvider expired every %i seconds...", m_iLongPoolTimer);

	m_ThreadStarted.Create();
	StartThread();
	m_ThreadStarted.Wait();
}

CKeepAliveProvider::~CKeepAliveProvider()
{
	m_log->LogInfo( L"CKeepAliveProvider clearing...");
}

void CKeepAliveProvider::Stop()
{
	JoinThread();
}

void CKeepAliveProvider::SubscribeTimer(const __int64& scriptID)
{
	if (m_iLongPoolTimer == 0)
	{
		m_log->LogWarning( L"Error: KeepAliveTimeout == 0");
		return;
	}
	std::wstring  sScriptID = format_wstring(L"%08X-%08X", HIDWORD(scriptID), LODWORD(scriptID));
	m_log->LogInfo( L"SubscribeTimer for %s", sScriptID.c_str());
	{
		std::lock_guard <std::mutex> lock(m_mutex);
		m_subscribers.push_back(scriptID);
	}
}

void CKeepAliveProvider::UnsubscribeTimer(const __int64& scriptID)
{
	std::wstring  sScriptID = format_wstring(L"%08X-%08X", HIDWORD(scriptID), LODWORD(scriptID));
	m_log->LogInfo( L"UnsubscribeTimer for %s", sScriptID.c_str());

	{
		std::lock_guard <std::mutex> lock(m_mutex);
		m_subscribers.remove(scriptID);
	}
}

void CKeepAliveProvider::MakeKeepAliveMsg(const __int64& scriptID)
{
	CMessage msg(L"KEEP_ALIVE");
	msg[L"DestinationAddress"] = scriptID;
	CPackedMsg pm(msg);
	if (m_engine && m_bThreadStarted)
	{
		m_engine->PostAuxMessage(pm());
	}
}

void CKeepAliveProvider::StartThread()
{
	m_thread.reset(
		new boost::thread(boost::bind(&CKeepAliveProvider::ThreadProc, this))
		);

	m_bThreadStarted = true;
}
//
void CKeepAliveProvider::JoinThread()
{
	m_log->LogFinest( L"CKeepAliveProvider JoinThread");

	if (m_bThreadStarted)
	{
		m_bThreadStarted = false;
		//::Sleep(1000); //waiting for ThreadProc done
		m_thread->join();
	}
	m_thread.reset();
	m_log->LogFinest( L"CKeepAliveProvider Thread has joined");
}

void CKeepAliveProvider::ThreadProc()
{
	m_ThreadStarted.Set();

	m_log->LogInfo( L"CKeepAliveProvider thread started");
	m_bThreadStarted = true;

	while (m_bThreadStarted)
	{
		if (1)
		{
			std::lock_guard <std::mutex> lock(m_mutex);
			if (m_subscribers.size())
			{
				++m_curSec;
			}
			if (m_curSec >= m_iLongPoolTimer)
			{
				m_log->LogFinest( L"========================keep alive tick===================");
				for(auto& scriptID : m_subscribers)
				{
					MakeKeepAliveMsg(scriptID);
				}
				m_curSec = 0;
			}
		}
		::Sleep(1000);
	}
	m_log->LogFinest( L"CKeepAliveProvider ThreadProc exit");
}

/******************************* eof *************************************/