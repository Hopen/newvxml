/************************************************************************/
/* Name     : Common\VXMLEngineConfig.cpp                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solution                                           */
/* Date     : 20 Apr 2019                                               */
/************************************************************************/

#include "stdafx.h"

#include <boost/property_tree/xml_parser.hpp>

#include "sv_utils.h"
#include "sid.h"
#include "VXMLResCache.h"
#include "ExtraLog.h"

#include "VXMLEngineConfig.h"

static void LogExtra(const wchar_t* aError)
{
	ExtraLog log;
	log.Init(GetGlobalScriptId(), L"finest", L"C:\\IS3\\Logs\\vxml_extra_%y-%m-%d.log");
	log.LogWarning(aError);
}

CVXMLEngineConfig::CVXMLEngineConfig()
{
	try
	{
		m_sFileName = SVUtils::GetModuleFileName(g_hInst) + L".config";
		std::wifstream _in(m_sFileName.c_str(), std::ios::binary);

		boost::property_tree::wiptree propertyTree;
		boost::property_tree::read_xml(_in, propertyTree);
		const boost::property_tree::wiptree& log = propertyTree.get_child(L"configuration").get_child(L"Log");
		const boost::property_tree::wiptree& folder = propertyTree.get_child(L"configuration").get_child(L"Folder");
		const boost::property_tree::wiptree& system = propertyTree.get_child(L"configuration").get_child(L"System");
		const boost::property_tree::wiptree& settings = propertyTree.get_child(L"configuration").get_child(L"DefaultVXMLSettings");
		const boost::property_tree::wiptree& text2speech = propertyTree.get_child(L"configuration").get_child(L"Text-to-speech");
		this->sLogFileName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Format");
		this->m_sExtraLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Extra");
		this->m_sVBSLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.VBS");
		this->sLevel = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Level");

		this->XMLFolders.sVXMLFolder = folder.get<std::wstring>(L"VXMLFolder.<xmlattr>.Value");
		this->XMLFolders.sWavFolder = folder.get<std::wstring>(L"WavFolder.<xmlattr>.Value");
		this->XMLFolders.sGrammarFolder = folder.get<std::wstring>(L"GrammarFolder.<xmlattr>.Value");
		this->XMLFolders.sVideoFolder = folder.get<std::wstring>(L"VideoFolder.<xmlattr>.Value");
		this->XMLFolders.sRecognitionsFolder = folder.get<std::wstring>(L"RecognitionsFolder.<xmlattr>.Value");

		this->VXMLDefSet.sTimeout = settings.get<std::wstring>(L"Timeout.<xmlattr>.Value");
		this->VXMLDefSet.sInterdigittimeout = settings.get<std::wstring>(L"Interdigittimeout.<xmlattr>.Value");
		this->VXMLDefSet.sWaitTime = settings.get<std::wstring>(L"WaitTime.<xmlattr>.Value");
		this->VXMLDefSet.sTermchar = settings.get<std::wstring>(L"Termchar.<xmlattr>.Value");

		this->Systems.sKeepAliveTimeout = system.get<std::wstring>(L"KeepAliveTimeout.<xmlattr>.Value");
		this->Systems.bRPCore = system.get<std::wstring>(L"RPCore.<xmlattr>.Value") == L"1";

		this->Text2Speech.sVoice = text2speech.get<std::wstring>(L"Voice.<xmlattr>.Value");
		this->Text2Speech.sRTSPHost = text2speech.get<std::wstring>(L"RTSPHost.<xmlattr>.Value");
		this->Text2Speech.sRTSPPort = Utils::toNum<int>(text2speech.get<std::wstring>(L"RTSPPort.<xmlattr>.Value"));
		this->Text2Speech.sPlayTrigg = Utils::toNum<int>(text2speech.get<std::wstring>(L"PlayTrigg.<xmlattr>.Value"));
		this->Text2Speech.sNonThreshold = Utils::toNum<int>(text2speech.get<std::wstring>(L"NonThreshold.<xmlattr>.Value"));
		this->Text2Speech.sNonPlayTrigg = Utils::toNum<int>(text2speech.get<std::wstring>(L"NonPlayTrigg.<xmlattr>.Value"));
		this->Text2Speech.sMagicWord = Utils::toNum<int>(text2speech.get<std::wstring>(L"MagicWord.<xmlattr>.Value"));

		this->Text2Speech.szVnTimeout = Utils::toNum<int>(text2speech.get<std::wstring>(L"VnTimeout.<xmlattr>.Value"));

		auto vnHosts = text2speech.get_child(L"VnHosts");
		for (const auto& host : vnHosts)
		{
			if (host.first == L"<xmlcomment>")
			{
				continue;
			}

			std::string hostName = wtos(host.second.get<std::wstring>(L"<xmlattr>.Host"));
			std::string port = wtos(host.second.get<std::wstring>(L"<xmlattr>.Port"));

			this->Text2Speech.sVnHosts.emplace_back(hostName, port);
		}

		this->Text2Speech.sVnUrl = text2speech.get<std::wstring>(L"VnUrl.<xmlattr>.Value");
		this->Text2Speech.iConfidenceLevel = Utils::toNum<int>(text2speech.get<std::wstring>(L"ConfidenceLevel.<xmlattr>.Value"));

		_in.close();
	}
	catch (boost::property_tree::xml_parser_error error)
	{
		LogExtra((std::wstring(L"Config XML parser error:") + stow(error.message())).c_str());

		throw std::wstring(L"XML parser error!");
	}
}

std::wstring CVXMLEngineConfig::GetLevel() const
{
	return sLevel;
}

std::wstring CVXMLEngineConfig::GetFileName() const
{
	return sLogFileName;
}

/******************************* eof *************************************/