// dllmain.h : Declaration of module class.

//#include <boost/thread/mutex.hpp>
#include "sid.h"

class CVXMLComModule : public CAtlDllModuleT< CVXMLComModule >
{
public :
	DECLARE_LIBID(LIBID_VXMLComLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_VXMLCOM, "{DC65F2EF-9F34-43DC-9A1D-28729476AB8B}")

	ObjectId Get() const { return mScriptId; }
	void Set(ObjectId aObjectId) { mScriptId = aObjectId; }

private:
	ObjectId mScriptId = 0;
};

extern class CVXMLComModule _AtlModule;
