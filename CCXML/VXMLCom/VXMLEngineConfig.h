/************************************************************************/
/* Name     : Common\VXMLEngineConfig.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solution                                           */
/* Date     : 20 Apr 2019                                               */
/************************************************************************/

#pragma once
#include "VXMLBase.h"

class CVXMLEngineConfig
{
public:
	CVXMLEngineConfig();

	std::wstring GetLevel() const;
	std::wstring GetFileName() const;

public:
	VXML::TDefaultVXMLSettings VXMLDefSet;
	VXML::TFolders XMLFolders;
	VXML::TSystem Systems;
	VXML::TTextToSpeech Text2Speech;

	std::wstring sLogFileName;
	std::wstring m_sExtraLogName;
	std::wstring m_sVBSLogName;
	std::wstring sLevel;

private:
	std::wstring m_sFileName;
};

/******************************* eof *************************************/