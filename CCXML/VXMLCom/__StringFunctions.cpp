#include "stdafx.h"
#include "StringFunctions.h"

std::wstring stow( const std::string& in, std::locale loc)
{
	std::wstring out( in.length(), 0 );
	std::string::const_iterator i = in.begin(), ie = in.end();
	std::wstring::iterator j = out.begin();

	for( ; i!=ie; ++i, ++j )
		*j = std::use_facet< std::ctype< wchar_t > > ( loc ).widen( *i );

	return out;
}

std::string wtos( const std::wstring& in, std::locale loc)
{
	std::string out( in.length(), 0 );
	std::wstring::const_iterator i = in.begin(), ie = in.end();
	std::string::iterator j = out.begin();

	for( ; i!=ie; ++i, ++j)
		*j = std::use_facet< std::ctype< char > > ( loc ).narrow( *i );

	return out;
}