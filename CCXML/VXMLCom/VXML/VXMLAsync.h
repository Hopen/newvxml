/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLAcync.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 17 Feb 2018                                               */
/************************************************************************/
#pragma once
#include "VXMLBase.h"

namespace VXML
{
	enum class E_ASYNC_EVENT
	{
		AE_UNKNOWN = 0,
		AE_REQUEST,
		AE_GET
	};

	enum class JobStatus
	{
		Unknown = 0,
		Waiting,
		InProgress,
		Finished,
		Aborted,
		Error
	};

	struct AsyncInfo
	{
		bool IsAsync = false;
		ObjectId ScriptId;
		VXMLString RequestId;
		VXMLString RequestName;
		VXMLString Body;
		VXMLString Contenttype;
		E_ASYNC_EVENT Event;
		VXMLString Namelist;
		VXMLString Target;
		VXMLString Port;
		VXMLString Timeout;
		bool IsCancel = false;
		VXMLString FetchAudio;
		bool IsPlayAudio = false;
		bool IsAudioFinished = false;
		bool IsSyncAckCome = false;

		AsyncInfo() = default;

		AsyncInfo(
			bool aIsAsync,
			ObjectId aScriptId,
			const VXMLString& aRequestId,
			const VXMLString& aRequestName,
			const VXMLString& aBody,
			const VXMLString& aContenttype,
			E_ASYNC_EVENT aEvent,
			const VXMLString& aNamelist,
			const VXMLString& aTarget,
			const VXMLString& aPort,
			const VXMLString& aTimeout,
			bool aIsCancel,
			const VXMLString& aFetchAudio)
			: IsAsync(aIsAsync)
			, ScriptId(aScriptId)
			, RequestId(aRequestId)
			, RequestName(aRequestName)
			, Body(aBody)
			, Contenttype(aContenttype)
			, Event(aEvent)
			, Namelist(aNamelist)
			, Target(aTarget)
			, Port(aPort)
			, Timeout(aTimeout)
			, IsCancel(aIsCancel)
			, FetchAudio(aFetchAudio)
			, IsPlayAudio(!aFetchAudio.empty())
		{}

		//CComVariant AsVariant() const
		//{
		//	SessionObject info = std::make_shared<CNameTable>();
		//	info->Add(L"IsAsync", IsAsync, true);
		//	info->Add(L"ScriptId", ScriptId, true);
		//	info->Add(L"RequestId", RequestId.c_str(), true);
		//	info->Add(L"Body", Body.c_str(), true);
		//	info->Add(L"Contenttype", Contenttype.c_str(), true);
		//	info->Add(L"Event", Event.c_str(), true);
		//	info->Add(L"Namelist", Namelist.c_str(), true);
		//	info->Add(L"Target", Target.c_str(), true);
		//	info->Add(L"Port", Port.c_str(), true);
		//	info->Add(L"Timeout", Timeout.c_str(), true);
		//	info->Add(L"IsCancel", IsCancel, true);

		//	return info->AsVariant();
		//}
	};

	using AsyncRequestContainer = std::map<VXMLString, AsyncInfo>;

	class EventConverter
	{
	public:
		EventConverter()
		{
			mEventsName[E_ASYNC_EVENT::AE_REQUEST] = L"ANY2AC_asyncHttpRequest";
			mEventsName[E_ASYNC_EVENT::AE_GET] = L"ANY2AC_getResult";

			mEvents[L"request"] = E_ASYNC_EVENT::AE_REQUEST;
			mEvents[L"get"] = E_ASYNC_EVENT::AE_GET;
		}

		VXMLString AsyncEventToString(E_ASYNC_EVENT aEvent) const
		{
			VXMLString name;
			const auto cit = mEventsName.find(aEvent);
			if (cit != mEventsName.cend())
			{
				name = cit->second;
			}
			return name;
		}

		E_ASYNC_EVENT GetEventByShortString(const VXMLString& aName)
		{
			E_ASYNC_EVENT event = E_ASYNC_EVENT::AE_UNKNOWN;
			const auto cit = mEvents.find(aName);
			if (cit != mEvents.cend())
			{
				event = cit->second;
			}
			return event;
		}

	private:
		std::map<E_ASYNC_EVENT, VXMLString> mEventsName;
		std::map<VXMLString, E_ASYNC_EVENT> mEvents;
	};

	class AsyncEventClient
	{
	public:
		AsyncEventClient(const SystemLogPtr& aLog)
			: mLog(aLog)
		{
		}

		//E_ASYNC_EVENT GetEventByShortString(const AsyncInfo& aInfo)
		//{
		//	return mConverter.GetEventByShortString(aInfo.Event);
		//}

	public:
		CMessage Create_ANY2AC_asyncHttpRequest(
			const ObjectId& aScriptId,
			const VXMLString& aRequestId,
			bool aIsAsync,
			const VXMLString& aUri,
			const VXMLString& aPort,
			const VXMLString& aRequest,
			bool aIsFetchAudio);

		CMessage Create_ANY2AC_getResult(
			const VXMLString& aRequestId,
			bool aIsCancel);

		CMessage CreateCommon(
			const ObjectId& aScriptId,
			const VXMLString& aEvent,
			const VXMLString& aRequest);

	private:
		EventConverter mConverter;
		SystemLogPtr mLog;
	};
}
/******************************* eof *************************************/