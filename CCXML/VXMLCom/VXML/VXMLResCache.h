/************************************************************************/
/* Name     : VXMLCom\VXMLResCache.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 1 Dec 2009                                                */
/************************************************************************/
#pragma once

#include "ce_xml.hpp"
#include "sv_utils.h"
#include "VXMLBase.h"
#include "..\..\CacheInterface\CacheInterface_i.h"
#import "MsXml6.dll"

extern HINSTANCE g_hInst;

namespace VXML
{
	class CResourceCache
	{
		//CComPtr<IStream>  pStream;
		//CComPtr<IResourceCache> m_basedRes;
		CComPtr<IResourceCache> m_Res;
		HRESULT m_hr;

		CComPtr<ITestMTObj> m_Test;

	public:
		//MSXML2::IXMLDOMNodePtr GetDocument(const VXMLString& sSrc, const bool& bFile);
		//VXMLString GetDocument(const VXMLString& sSrc, const bool& bFile);
		HRESULT GetDocument(const VXMLString& sSrc, const VXMLString& sScriptID, const bool& bFile, SAFEARRAY** pSA);
		HRESULT ParseT2SMessage(const VXMLString& sSrc, SAFEARRAY** pSA);
		//CDocument* GetDocument(const VXMLString& sURI);
		//void PutDocument(CDocument* pDoc);
		CResourceCache();
		~CResourceCache();

		bool Ready()const;

		bool TestReady()const;
		HRESULT GetHR()const{ return m_hr; }
		HRESULT WriteTestLog(const VXMLString& sSrc);
		//bool BaseReady()const;

		//bool AttachResourceInterface();

	};
}

/******************************* eof *************************************/