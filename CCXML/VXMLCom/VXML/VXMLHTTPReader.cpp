/************************************************************************/
/* Name     : VXMLCom\VXMLHTTPReader.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 11 Jun 2010                                               */
/************************************************************************/
#include "..\StdAfx.h"
#include "VXMLHTTPReader.h"

#include <tchar.h>
#include <stdio.h>
#include <vector>

namespace VXML
{
	extern wchar_t*  clientModulesPath;    

	CHTTPReader::CHTTPReader(LPCTSTR lpszServerName,bool bUseSSL)
	: m_hInternet(NULL),
	  m_hConnection(NULL),
	  m_hRequest(NULL),
	  m_lpszServerName(NULL),
	  m_lpszDefaultHeader(NULL),
	  m_lpszDataBuffer(NULL),
	  m_dwBufferSize(0),
	  m_bUseSSL(bUseSSL),
	  m_dwLastError(0)
	{
	  SetDefaultHeader(TEXT( 
		  //"Accept: */*"
		"Content-Type: application/x-www-form-urlencoded\r\n"
		  L"Accept-Language:ru\r\n"
		  L"Accept-Encoding:gzip, deflate"
		  ));

	  StrDup(m_lpszServerName,lpszServerName);
	}

	CHTTPReader::~CHTTPReader()
	{
	  delete m_lpszDataBuffer;
	  delete m_lpszServerName;

	  InternetCloseHandle(m_hInternet);
	  m_hInternet = NULL;

	  SetDefaultHeader(NULL);
	}
	/*
	bool  CHTTPReader::OpenInternet
	(
	 LPCTSTR lpszAgent=TEXT("CASINO CLIENT v1.1")
	);
	*/

	bool CHTTPReader::CheckError(bool bTest)
	{
	  if (bTest == false) {
		m_dwLastError = ::GetLastError();
	  }
	  return bTest;
	}

	bool CHTTPReader::OpenInternet(LPCTSTR lpszAgent)
	{
	  if (m_hInternet == NULL)
		m_hInternet = ::InternetOpen(
		  lpszAgent,
		  INTERNET_OPEN_TYPE_PRECONFIG,
		  NULL,
		  NULL,
		  0);

	  return CheckError(m_hInternet != NULL);
	}

	void CHTTPReader::CloseInternet ()
	{
	  CloseConnection();
	  
	  if (m_hInternet) 
		::InternetCloseHandle(m_hInternet);
	  m_hInternet = NULL;
	}

	bool CHTTPReader::OpenConnection (LPCTSTR lpszServerName)
	{    
	  if (OpenInternet(L"IE") && m_hConnection == NULL)
		m_hConnection = ::InternetConnect(
		  m_hInternet,
		  lpszServerName? 
			lpszServerName:
			m_lpszServerName? 
			  m_lpszServerName: 
			  TEXT("localhost"),
		  m_bUseSSL? INTERNET_DEFAULT_HTTPS_PORT: INTERNET_DEFAULT_HTTP_PORT,
		  NULL,
		  NULL,
		  INTERNET_SERVICE_HTTP,
		  0,
		  1u);
	  
	  return CheckError(m_hConnection != NULL);
	}

	void CHTTPReader::CloseConnection ()
	{
	  CloseRequest();

	  if (m_hConnection)
		::InternetCloseHandle(m_hConnection);
	  m_hConnection = NULL;
	}

	bool CHTTPReader::SendRequest (LPCTSTR lpszVerb,
					 LPCTSTR lpszAction,
					 LPCTSTR lpszData,
					 LPCTSTR lpszReferer)
	{
	  if (OpenConnection()) {
		CloseRequest();

		LPCTSTR AcceptTypes[] = { TEXT("*/*"), NULL}; /**/
		m_hRequest = ::HttpOpenRequest(
		  m_hConnection,
		  lpszVerb,
		  lpszAction,
		  NULL,
		  lpszReferer,
		  AcceptTypes,
		  (m_bUseSSL? INTERNET_FLAG_SECURE|INTERNET_FLAG_IGNORE_CERT_CN_INVALID: 0) 
			| INTERNET_FLAG_KEEP_CONNECTION,
		  1);

		if (m_hRequest != NULL) {
		  if (::HttpSendRequest(
			  m_hRequest, 
			  m_lpszDefaultHeader, 
			  -1,
			  (LPVOID)lpszData,
			  lpszData? _tcslen(lpszData): 0) == FALSE)
		  {
			int err = GetLastError();
			CheckError(false);
			CloseRequest();
			return false;
		  }
		}
	  }

	  return CheckError(m_hRequest != NULL);
	}

	bool CHTTPReader::Get (LPCTSTR lpszAction,LPCTSTR lpszReferer)
	{
	  return SendRequest(TEXT("GET"),lpszAction,NULL,lpszReferer);
	}

	bool CHTTPReader::Post (LPCTSTR lpszAction,LPCTSTR lpszData,LPCTSTR lpszReferer)
	{
	  return SendRequest(TEXT("POST"),lpszAction,lpszData,lpszReferer);
	}

	void CHTTPReader::CloseRequest ()
	{
	  if (m_hRequest)
		::InternetCloseHandle(m_hRequest);
	  
	  m_hRequest = NULL;
	}

	void CHTTPReader::StrDup (LPTSTR& lpszDest,LPCTSTR lpszSource)
	{
	  delete lpszDest;

	  if (lpszSource == NULL) {
		lpszDest = NULL;
	  } else {
		lpszDest = new DEBUG_NEW_PLACEMENT TCHAR[_tcslen(lpszSource)+1];
		_tcscpy(lpszDest,lpszSource);
	  }
	}
	                                          
	void CHTTPReader::SetDefaultHeader (LPCTSTR lpszDefaultHeader)
	{
	  StrDup(m_lpszDefaultHeader,lpszDefaultHeader);
	}

	wchar_t *CHTTPReader::GetData (wchar_t *lpszBuffer,DWORD dwSize,DWORD *lpdwBytesRead)
	{
	  DWORD dwBytesRead;
	  if (lpdwBytesRead == NULL)
		lpdwBytesRead = &dwBytesRead;
	  *lpdwBytesRead = 0;

	  if (m_hRequest) {
		bool bRead = ::InternetReadFile(
		  m_hRequest,
		  lpszBuffer,
		  dwSize,
		  lpdwBytesRead) != FALSE;
		lpszBuffer[*lpdwBytesRead] = 0;

		return CheckError(bRead) && *lpdwBytesRead? lpszBuffer: NULL;
	  }

	  return NULL;
	}

	DWORD CHTTPReader::GetDataSize ()
	{
	  if (m_hRequest) {
		DWORD dwDataSize = 0;
		DWORD dwLengthDataSize = sizeof(dwDataSize);

		BOOL bQuery = ::HttpQueryInfo(
		  m_hRequest,
		  HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER,
		  &dwDataSize, 
		  &dwLengthDataSize,
		  NULL);

		return bQuery? dwDataSize: 0;
	  }

	  return 0;
	}

	wchar_t *CHTTPReader::GetData (DWORD *lpdwBytesRead)
	{
	  DWORD dwDataSize = 200000; //GetDataSize();
	  SetDataBuffer(dwDataSize);  
	  return GetData(
		m_lpszDataBuffer, 
		dwDataSize? dwDataSize: m_dwBufferSize, 
		lpdwBytesRead);
	}

	void CHTTPReader::SetDataBuffer (DWORD dwBufferSize)
	{
	  if (dwBufferSize > m_dwBufferSize) {
		delete m_lpszDataBuffer;
		m_lpszDataBuffer = new DEBUG_NEW_PLACEMENT wchar_t[(m_dwBufferSize = dwBufferSize) + 1];
	  }
	}


	CHTTPConnector::CHTTPConnector()
{
}

CHTTPConnector::~CHTTPConnector()
{
}

int CHTTPConnector::PostData(std::wstring szServerName,
							 UINT uFileSize,
							 std::wstring ObjectName, 
							 LPCWSTR lpOptional, 
							 std::wstring &Data)  // [out]
{
	int stat = 0;
	//try
	{
		// default server name
		
		HINTERNET session = InternetOpen(TEXT("VXML"), 
										  INTERNET_OPEN_TYPE_PRECONFIG, 
										  0, 
										  0, 
										  0);

		HINTERNET hConnect= InternetConnect(session,
											szServerName.c_str(),
											INTERNET_DEFAULT_HTTP_PORT, 
											L"", 
											L"", 
											INTERNET_SERVICE_HTTP, 
											0, 
											1);

		std::vector<std::wstring>m_strMIMEs;
		m_strMIMEs.push_back(L"text/html");
		m_strMIMEs.push_back(L"*/*");

		LPTSTR *szAcceptTypes = NULL;
		const int iCountMimes = m_strMIMEs.size();
	   { // convert CStringArray to array TCHAR strings
		  szAcceptTypes = new DEBUG_NEW_PLACEMENT LPTSTR[iCountMimes+1];
		  ZeroMemory(szAcceptTypes, (iCountMimes+1)*sizeof(LPTSTR));
		  for (int i=0; i<iCountMimes; i++) {
			 szAcceptTypes[i] = new DEBUG_NEW_PLACEMENT TCHAR[m_strMIMEs[i].length()+1];
			 if (!szAcceptTypes[i]) {
				break;
			 }
			 _tcscpy(szAcceptTypes[i], m_strMIMEs[i].c_str());
		  }
	   }
		

		//LPCTSTR AcceptTypes[] = { TEXT("html/text"), TEXT("*/*"), NULL}; 
		wchar_t hdrs[] =TEXT(
			"Content-Type: application/x-www-form-urlencoded;"
			L"Accept-Language:ru;"
			L"Accept-Encoding:gzip, deflate");

		DWORD dwFlags = INTERNET_FLAG_RELOAD | INTERNET_FLAG_NEED_FILE | INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_NO_UI;
		HINTERNET hRequest = HttpOpenRequest(
			hConnect, 
			TEXT("POST"),
			ObjectName.c_str(),
			TEXT("HTTP/1.1"),
			NULL,
			(LPCTSTR*)szAcceptTypes,//AcceptTypes, 
			dwFlags/*INTERNET_FLAG_KEEP_CONNECTION*/, 
			1);

		DWORD dwErrCode = ::GetLastError();

		for (int i=0; i<iCountMimes; i++) {
			if (szAcceptTypes[i]) {
				delete [] szAcceptTypes[i];
			}
		}
		delete [] szAcceptTypes;
		m_strMIMEs.clear();

		DWORD dwReceiveTimeout = 90000;
		BOOL bRes = InternetSetOption(
						hRequest,
						INTERNET_OPTION_RECEIVE_TIMEOUT,
						(LPVOID)&dwReceiveTimeout,
						sizeof(dwReceiveTimeout)
						);


		if (hRequest)
			HttpSendRequest(
				hRequest, 
				(LPCTSTR)hdrs, 
				wcslen(hdrs), 
				(void*)lpOptional, 
				wcslen(lpOptional));

		dwErrCode = ::GetLastError();
		CMemBuffer mbuf(0);
		UINT AllByteRead = 0;
		DWORD dwSize = 0;

		do
		{
			BOOL bResult = InternetQueryDataAvailable(
				hRequest,
				&dwSize,
				0,0
			);
			if (dwSize == 0)  // read complete
				break;
			// Allocate space for the buffer.
			LPVOID lpBuffer = new DEBUG_NEW_PLACEMENT BYTE[dwSize];
			ZeroMemory(lpBuffer, dwSize);
			DWORD dwNumberOfBytesRead = 0;
			bResult = InternetReadFile(hRequest, lpBuffer, dwSize, &dwNumberOfBytesRead);
			mbuf.Add(lpBuffer,dwNumberOfBytesRead);
			delete [] lpBuffer;
			AllByteRead+=dwNumberOfBytesRead;

		}
		while(dwSize);


		InternetCloseHandle(hRequest);
		InternetCloseHandle(hConnect);
		InternetCloseHandle(session);

		if (!AllByteRead)
		{

			//CFile file;
			//int st = 0;
			//if (!file.Open("c:\\Projects\\data2.gz", CFile::modeCreate | CFile::modeWrite))
			//					{
			//						st = e5006;
			//						//ErrorExit(pParentWnd->GetSafeHwnd(),"Only for test");
			//					}
			//				if (!st)
			//				{
			//					file.Write(mbuf.GetData(),mbuf.GetSize());
			//					file.Close();
			//				}


			//Data.append(mbuf.GetData(),mbuf.GetSize());

			int testlen = Data.length();
		}


	}
	//catch (CInternetException *error)
	//{
	//	
	//	//TCHAR   szCause[255];
	//	//CString strFormatted;

	//	//error->GetErrorMessage(szCause, 255);
	//	//strFormatted = szCause;
	//	//ErrorExit(hWnd,strFormatted);

	//	//error->Delete();
	//	//stat = e5005;
	//}

	return stat;

}


const  int FILE_BUFFER_SIZE = 5000;
int CHTTPConnector::GetData(std::wstring szServerName,
							UINT uFileSize,
							std::wstring ObjectName, 
						 //LPCSTR lpOptional, 
						 std::wstring &Data)  // [out]
{
	int stat = 0;
	//try
	{

		HINTERNET session = InternetOpen(TEXT("VXML"), 
										  INTERNET_OPEN_TYPE_PRECONFIG, 
										  0, 
										  0, 
										  0);
		DWORD dwErrCode = ::GetLastError();

		HINTERNET hConnect= InternetConnect(session,
											szServerName.c_str(),
											INTERNET_DEFAULT_HTTP_PORT, 
											0, 
											0, 
											INTERNET_SERVICE_HTTP, 
											0, 
											1u);
		dwErrCode = ::GetLastError();

		LPCTSTR AcceptTypes[] = { TEXT("*/*"), NULL}; 
		wchar_t hdrs[] =TEXT(
			"Content-Type: application/x-www-form-urlencoded\r\n"
			L"Accept-Language:ru\r\n"
			L"Accept-Encoding:gzip, deflate");

		HINTERNET hRequest = HttpOpenRequest(hConnect, 
			NULL,//L"GET", - default
			ObjectName.c_str(),
			NULL,
			L"",
			AcceptTypes, 
			/*INTERNET_FLAG_KEEP_CONNECTION | */INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_RESYNCHRONIZE, 
			1);

		if (hRequest)
			HttpSendRequest(hRequest, hdrs, wcslen(hdrs), NULL, 0);

		dwErrCode = ::GetLastError();
		DWORD dwDataSize = 0;

		if (hRequest) 
		{
			DWORD dwLengthDataSize = sizeof(dwDataSize);
			BOOL bQuery = ::HttpQueryInfo(
				hRequest,
				HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER,
				&dwDataSize, 
				&dwLengthDataSize,
				NULL);
		}

		DWORD dReadFileCount = 0;
		DWORD dwBytesRead    = 0;
		char buffer[FILE_BUFFER_SIZE];
		ZeroMemory(buffer,FILE_BUFFER_SIZE);
		uFileSize = dwDataSize;

		if (dwDataSize)
		{
			int max_range = int(dwDataSize/FILE_BUFFER_SIZE) + 1;

			char * CompressedData = new DEBUG_NEW_PLACEMENT char [dwDataSize];
			ZeroMemory(CompressedData,dwDataSize);
			bool Running = true;
			UINT rest = 0;
			do 
			{
				InternetReadFile (hRequest, (LPVOID)buffer, FILE_BUFFER_SIZE, &dReadFileCount);
				//CompressedData+=buffer;
				memcpy(CompressedData+dwBytesRead,buffer,dReadFileCount);
				dwBytesRead+=dReadFileCount;

			}
			while (dReadFileCount && Running); 

			InternetCloseHandle(hRequest);
			InternetCloseHandle(hConnect);
			InternetCloseHandle(session);

			if (dwDataSize  == dwBytesRead)
			{

				//Data.append(mbuf.GetData(),mbuf.GetSize());
				int testlen = Data.length();
			}
			delete [] CompressedData;
		}
		else
		{
			InternetCloseHandle(hRequest);
			InternetCloseHandle(hConnect);
			InternetCloseHandle(session);
		}



	}
	//catch (CInternetException *error)
	//{
		
	//}

	return stat;

}

}



// ------------------------- eof ---------------------------------