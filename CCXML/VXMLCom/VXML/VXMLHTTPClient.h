/************************************************************************/
/* Name     : VXMLCom\VXMLHTTPClient.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 12 Jun 2017                                               */
/************************************************************************/
#pragma once
//#include <boost/property_tree/json_parser.hpp>
#include <Wininet.h>
#include "VXMLBase.h"
#include "Socket\SpawnClient.h"
#include "tgbot\net\HttpParser.h"
#include "Patterns\ptree_parser.h"
#include "..\Common\SystemLogEx.h"
//using namespace enginelog;

namespace VXML
{
	static std::string urlencode(const std::wstring &input)
	{
		std::string output;
		int cbNeeded = WideCharToMultiByte(CP_UTF8, 0, input.c_str(), -1, NULL, 0, NULL, NULL);
		if (cbNeeded > 0) {
			char *utf8 = new char[cbNeeded];
			if (WideCharToMultiByte(CP_UTF8, 0, input.c_str(), -1, utf8, cbNeeded, NULL, NULL) != 0) {
				for (char *p = utf8; *p; *p++) {
					char onehex[5];
					_snprintf(onehex, sizeof(onehex), "%%%02.2X", (unsigned char)*p);
					output.append(onehex);
				}
			}
			delete[] utf8;
		}
		return output;
	}

	class CARSClient
	{
	public:
		CARSClient(
			const SystemLogPtr& aLog,
			const VnHostCollector& aVnHosts,
			const std::wstring& aVnUrl,
			size_t aTimeout)
			: mLog(aLog)
			, mHosts(aVnHosts)
			, mUrl(aVnUrl)
			, mTimeout(aTimeout)
		{

		}

		bool Connect(
			const std::string& aHost,
			const std::string& aPort,
			const std::string& aUrl,
			std::string& outResponse)
		{
			spawn_client::TConnectionClientParams params;
			params._uri = aHost;
			params._port = aPort;
			params._timeout = mTimeout;

			std::vector<TgBot::HttpReqArg> args;
			std::string url = aUrl;
			params._request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

			spawn_client::makeClient(params, mLog.get());

			outResponse = params._response;

			return !params._error;
		}

		bool Classify(
			const std::wstring& aUtterance,
			std::wstring& outClassifyId)
		{
			VnHostCollector doubleHosts;

			srand(static_cast<size_t>(time(nullptr)));

			// copy twise
			std::copy(mHosts.cbegin(), mHosts.cend(), std::back_inserter(doubleHosts));
			std::copy(mHosts.cbegin(), mHosts.cend(), std::back_inserter(doubleHosts));

			size_t randomSelect = 0;

			if (mHosts.size() > 1)
			{
				randomSelect = rand() % mHosts.size();
			}

			bool success = false;
			std::string httpResponse;

			for (size_t i = randomSelect; i < randomSelect + mHosts.size(); ++i)
			{
				TVnHosts host = doubleHosts[i];
				std::string url = "http://" + host.Host + ":" + host.Port + wtos(mUrl) + urlencode(aUtterance);

				success = Connect(
					host.Host,
					host.Port,
					url,
					httpResponse);

				if (success)
				{
					break;
				}
			}

			if (!success)
			{
				return false;
			}

			TgBot::HttpParser::HeadersMap headers;

			std::string response = TgBot::HttpParser::getInstance().parseRequest(httpResponse, headers);

			mLog->LogInfo(L"CARSClient: Get response: %s", stow(response).c_str());

			int pos = response.find("{");
			if (pos > 0)
			{
				response = response.substr(pos, response.length());
			}

			auto tree = core::support::ptree_parser::tree_from_string_json(stow(response));
			outClassifyId = tree.get<std::wstring>(L"classId");

			return true;

			//spawn_client::TConnectionClientParams params;
			//params._uri = wtos(m_vnHost);
			//params._port = std::to_string(m_vnPort);

			//std::vector<TgBot::HttpReqArg> args;
			//params._request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

			//spawn_client::makeClient(params, m_log.get());

			//TgBot::HttpParser::HeadersMap headers;

			//std::string response = TgBot::HttpParser::getInstance().parseRequest(params._response, headers);

			//m_log->LogInfo( L"CARSClient", L"Get response: %s", stow(response).c_str());

			//int pos = response.find("{");
			//if (pos > 0)
			//{
			//	response = response.substr(pos, response.length());
			//}

			//auto tree = core::support::ptree_parser::tree_from_string_json(stow(response));
			//const auto classId = tree.get<std::wstring>(L"classId");

			//return classId;
		}
	private:
		SystemLogPtr mLog;
		VnHostCollector mHosts;
		std::wstring mUrl;
		size_t mTimeout;
	};


}// end namespace
/******************************** eof ***********************************/