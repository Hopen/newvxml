/************************************************************************/
/* Name     : VXMLCom\VXMLSession.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Nov 2009                                               */
/************************************************************************/

#pragma once
#include "VXMLBase.h"
#include "sv_thread.h"
#include "sv_handle.h"
#include "sv_sysobj.h"
#include "..\CVXMLComInterface.h"
#include "VXMLEngineEvent.h"
#include "VXMLTags.h"
#include "..\Common\locked_queue.h"
//#include "Patterns/singleton.h"
#include "VXMLAppLog.h"
#include "VXMLAsync.h"


class CKeepAliveProvider;
using KeepAliveProviderPtr = std::unique_ptr<CKeepAliveProvider>;

namespace VXML
{
	enum eWaitEvents
	{
		WE_SUCCESS = 0,
		WE_FAILED,
		WE_STOPTHREAD
	};

	struct SessionCreateParams
	{
		LPMESSAGEHEADER		pInitMsg;
		IEngineCallback*	pEvtPrc;
		SystemLogPtr		pLog;
		ExtraLogPtr 	 	pExtraLog;
		ExtraLogPtr 		pVBSLog;
		DWORD				dwSurrThread;
		TFolders			VXMLFolders;
		TDefaultVXMLSettings VXMLDefSet;
		TSystem             VXMLSystem;
		Q931Ptr				pQ931Info;
		VXMLString			sQ931XML;
		IVOXPtr             pVOX;
		SessionPtr			pSession;
		TTextToSpeech       Text2Speech;
	};

	using EventMessages = std::list<CMessage>;

	class IDebuger: public IEngineCallback
	{
	public:
		IDebuger(IEngineCallback* _callback, IEngineVM* _engine)
		{
			m_pCallBack = _callback ;
			m_pEngine   = _engine   ;
		}
	public: // interface
		virtual void Wait        (unsigned int _line/*, unsigned int _count = 1*/) = 0;
		virtual void Increase    (/*unsigned int _count = 1*/) = 0;
		virtual void SetAuxEvent (CMessage& msg, bool _in) = 0;
		virtual void Init        (CMessage& init_msg, SessionPtr _session) = 0;
		virtual void Log         (VXMLString _log       ) = 0;

	protected:
		IEngineCallback* m_pCallBack;
		IEngineVM*       m_pEngine;
	};

	class CEventDispatcher;
	using EventDispatcherPtr = std::unique_ptr<CEventDispatcher>;

	class CStateDispatcher;
	using StateDispatcherPtr = std::unique_ptr<CStateDispatcher>;

	class CSession
	{
		enum eThreadMsg
		{
			tmExecuteDoc = WM_USER + 1,
			tmSetEvent,
			tmExecutePredefinition,
			tmDocumentPrepared,
			tmQuit = WM_QUIT
		};

	private:
		class CTellib
		{
		public:
			IVOXPtr GetInstance()
			{
				if (m_pVox == NULL)
					m_pVox = LoadTelLib();
				return m_pVox;
			}
		private:
			IVOXPtr LoadTelLib();
			IVOXPtr m_pVox;
		};

		//typedef std::shared_ptr <CTellib> TelLibPtr;
		using TelLibPtr = std::unique_ptr<CTellib>;
	public:
		CSession();
		virtual ~CSession(void);
		bool Initialize(const SessionCreateParams& params);
		bool IsInitialized(){ return m_Initialized; }

	private:
		CMessage				m_InitMsg;
		VXMLString				m_sCurDoc;
		CExecContext			m_ExecContext;
		SystemLogPtr			m_Log;
		ExtraLogPtr			    m_extraLog;
		DWORD					m_dwState;
		TFolders			    m_VXMLFolders;
		TDefaultVXMLSettings	m_VXMLDefSet;
		TSystem				    m_VXMLSystem;
		StateDispatcherPtr		m_StateDispatcher;
		TTextToSpeech			m_Text2SpeechSettings;

		SessionObject		    m_pLocal;
		SessionObject		    m_pRemote;
		SessionObject		    m_pProtocol;
		SessionObject		    m_pConnection;
		SessionObject		    m_pDialog;
		SessionObject		    m_pLastMessage;
		AsyncRequestContainer	m_pAsyncRequests;


		//SessionObject			m_pMenus;
		SessionGrammar			m_pGrammars;
		EventPtr				m_pSessionVariables;
		EventPtr				m_pApplicationVariables;
		EventPtr				m_pDialogVariables;
		bool                    m_DialogActive;
		//unsigned int            m_nStepCounter;

		//int						m_CSCounter;
		TelLibPtr				m_pVox;
		std::shared_ptr<CAppLog> m_appLog;
		locked_queue      < eThreadMsg    >  m_queue;

		bool m_Initialized;

		SessionObject        m_pGlobalVars;
		SessionObject        m_pPredefineVars;

		CEvtScopeCollector   m_ScopeCollector;
		ObjectId		     m_ScriptId;

	private:
		void CreateSessionVariables();
		void MakeKeepAlive(int aKeepAliveTimeout);
		VXMLString ThreadMsgToString(const eThreadMsg& _msg)const;
		void SendQueuedEvent(int nIndex);
	public:
		template <class... TArgs>
		void Log(LPCWSTR aText, TArgs&&... aArgs)
		{
			m_Log->LogInfo( aText, std::forward<TArgs>(aArgs)...);
		}
		CMessage ExceptionHandler();
	private:
		void DoDocumentPrepared();
		void TranslateMsg(const CMessage& msg);
		VXMLString GetText2SpeechVoice        () const{ return m_Text2SpeechSettings.sVoice; }
		VXMLString GetText2SpeechRTSPHost     () const{ return m_Text2SpeechSettings.sRTSPHost; }
		int GetText2SpeechRTSPPort            () const{ return m_Text2SpeechSettings.sRTSPPort; }
		//int GetText2SpeechThreshold           () const{ return m_Text2SpeechSettings.sThreshold; }
		int GetText2SpeechPlayTrigg           () const{ return m_Text2SpeechSettings.sPlayTrigg; }
		int GetText2SpeechNonThreshold        () const{ return m_Text2SpeechSettings.sNonThreshold; }
		int GetText2SpeechNonPlayTrigg        () const{ return m_Text2SpeechSettings.sNonPlayTrigg; }
		int GetText2SpeechMagicWord           () const{ return m_Text2SpeechSettings.sMagicWord; }

		void MakeEvent_ClearDigits                  (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_VXMLRunScriptOK              (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_VXMLRunScriptFAILED          (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_EndDialog                    (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_DigitGot                     (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_PlayWavCompleted             (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_TransferPlayWavCompleted     (TransferInfo& outTransferInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_PlayWavAck                   (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_PlayWavBreak                 (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_TextToSpeechAck              (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_TextToSpeechCompleted        (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_RecognizeAck                 (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_RecognizeCompleted		    (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_StreamCompleted              (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_GetDigitsCompleted           (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_DialogTransferComplete       (TransferInfo& outTransferInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_ConnectionDisconnectHangup   (TransferInfo& outTransferInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_ConnectionDisconnectTransfer (TransferInfo& outTransferInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_DialogStopChannel            (TransferInfo& outTransferInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_SC2VRunFunctionAck           (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_SC2VRunFunctionCompleted     (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_Disconnected					(const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_Terminated                   (const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_RecWav(CMessage&);
		//void MakeEvent_AsyncGetResult               (const CMessage&, const EventWPtr& curEvent);
		
		void MakeEvent_AsyncHttpRequestAck			(AsyncInfo aAsyncInfo, const CMessage& aMsg, const ItemEventPtr & aCurEvent);
		void MakeEvent_AsyncHttpResponse            (AsyncInfo aAsyncInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_SyncHttpRequestAck			(AsyncInfo aAsyncInfo, const CMessage& aMsg, const ItemEventPtr & aCurEvent);
		void MakeEvent_SyncHttpResponse				(AsyncInfo aAsyncInfo, const CMessage&, const ItemEventPtr& curEvent);

		void MakeEvent_AsyncCommonHttpResponse		(const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_FetchingPlayWavCompleted     (AsyncInfo aAsyncInfo, const CMessage&, const ItemEventPtr& curEvent);
		void MakeEvent_FetchingPlayWavAck           (AsyncInfo aAsyncInfo, const CMessage&, const ItemEventPtr& curEvent);

		
		bool DigitsEmit(const CMessage& evt, const ItemEventPtr& curEvent);
		bool VoiceEmit(const CMessage& aMrcpMessage, const ItemEventPtr& aCurEvent);
		bool LoadPredefinitions(const VXMLString& sPredefine);
		void DoExit(const VXMLString& sConnectedWithMessageName, const VXMLString& sNamelist, const VXMLString& _error_description);
		void ChangeCallCtrl(const ObjectId& _oldScriptID, const ObjectId& _newScriptID);
		void MakeTransferDialogStopShannel(TransferInfo& outTransferInfo, const ItemEventPtr& curEvent);

		Factory_c::CMrcpFactory LoadT2SAnswer(const VXMLString& sSrc) const;
		CMessage MakeMrcpEvent(const CMessage& msg) const;

		CMessage CreatePlayWavMessage(const VXMLString& _uri)const;
		CMessage CreateTextToSpeechMessage(const VXMLString& aText, bool aIsDtmfInput)const;
		CMessage CreateMRCPRecognizeMessage(bool aIsBargeIn = false)const;
		std::pair<VXMLString, VXMLString> CreateLocalUrl(const VXMLString& aUrl, const VXMLString& aANumber) const;
		CMessage CreateMASRecieveFilesMessage(const VXMLString& aUrl, const VXMLString& aLocalUrl, const VXMLString& aLocalFolder)const;
		VXMLString SendMASRecieveFilesMessage(const VXMLString& aRecognitionUrl, const VXMLString& aConfidence, bool aCheckConfidence)const;

		void MakeErrorASRAckMsg(const CMessage &aMsg);
		void MakeErrorTTSAckMsg(const CMessage &aMsg);
		void MakeErrorAsyncAckMsg(const CMessage &aMsg);

		VXMLString GetTTSSpeakName()const;
		VXMLString GetTTSSpeakAckName()const;
		VXMLString GetTTSSpeakCompletedName()const;
		VXMLString GetGrammarPathParamName()const;

		void DoError(
			const wchar_t* aErrorName,
			const VXMLString& aSubSystem,
			const VXMLString& aReason,
			const VXMLString& aTag,
			const VXMLString& aDescription);

		void DoExit();

		void InitEngine();
		void DestroyEngine();

		void MakeVXMLRunScriptFailed(const VXMLString& aSource);

	public:
		void DoStep();
		
		void EmitSubEvent(
			const VXMLString& aCurrMenuName, 
			const MenuPtr& aMenu,
			const TagWPtr aEnterPoint,
			const ScopeServicePtr& aScope);

		void EmitRecursiveMenuEvent(
			const VXMLString& aCurrMenuName,
			const MenuPtr& aMenu,
			const ScopeServicePtr& aScope);

		void EmitRecursiveDocEvent(
			//const VXMLString& aCurrMenuName,
			const MenuPtr& aMenu,
			const VXMLString& aUri,
			const TagWPtr& aEnterPoint,
			const ScopeServicePtr& aScope);

		void EmitPredefineEvent(
			//const VXMLString& aCurrMenuName,
			const MenuPtr& aMenu,
			const VXMLString& aUri,
			const TagWPtr& aEnterPoint,
			const ScopeServicePtr& aScope);

		void EmitSubEvent(const CMessage& evt);
		//TagWPtr LoadDocument(const VXMLString& sURI,const bool& bFile);
		DocumentPtr LoadDocument(const VXMLString& sURI, const bool& bFile);
		void SendAuxMsg(CMessage& msg) const;
		void DoError(CMessage &evt, const VXMLString& _subSystem);
		void DoExit(VXMLString sNamelist);
public:
		void DoDialogError(const VXMLString& sErrorDescription);
		void DoTerminated(const VXMLString& sNamelist, const VXMLString& sHandler);
		ObjectId GetParentCallID()const;
		VXMLString GetWavFolder()const {return m_VXMLFolders.sWavFolder;}
		VXMLString GetGrammaPath()const {return m_VXMLFolders.sGrammarFolder;}
		VXMLString GetFileFolder()const{return m_VXMLFolders.sVXMLFolder;}
		VXMLString GetVideoFolder()const{return m_VXMLFolders.sVideoFolder;}
		VXMLString GetRicognitionsFolder()const { return m_VXMLFolders.sRecognitionsFolder; }
		VXMLString GetCurDocUri()const{return m_sCurDoc;}

		void AssignEvents(
			const ItemEventPtr& aCurrEvt,
			WaitingEventsContainer&& aEvents);

		void WaitEvents(const EventMessages& aMessages);

		void AssignAndWaitEvents(
			const ItemEventPtr& aCurrEvt, 
			const EventMessages& aMessages, 
			WaitingEventsContainer&& aEvents);

		bool IsVox()const;

		void DoAsyncEvent(const VXMLString& aEvent, const AsyncInfo& aInfo);
		void DoSyncEvent(const VXMLString& aEvent, const AsyncInfo& aInfo);

		void SetEvent(const CMessage& msg);
		//SessionObject AddMenu(CMessage& msg);
		//SessionGrammar AddGrammar(VXMLString name, VXMLString path);
		SessionGrammar AddGrammar(const VXMLString& name, const VXMLString& source, const VXMLString& mode, const VXMLString& _type, bool bText);
		SessionGrammar FindGrammar(const VXMLString& grammar_name) const;
		//SessionObject  FindMenu   (const VXMLString& menu_name) const;
		
		//VXMLString GetCurMenu()const;
		//VXMLString GetCurMenu(const EventPtr& aCurrEvent)const;
		CMessage CSession::CreateStatisticMsg(const VXMLString& formID) const;
		void ProccedFormComment(const VXMLString& formID , const VXMLString& comment) const;
		void ProccedBufferComment(const VXMLString& formID, const VXMLString& buffer) const;
		void SendEvent(CMessage& evt, /*int nSendId,*/ ObjectId to/*, DWORD dwDelay*/);
		void SendEventToYourself(CMessage& evt);
		ObjectId GetScriptID()const;
		ObjectId GetParentScriptID()const;
		VXMLString GetPredefineFiles()const;
		void ChangeState(DWORD dwState);
		void SetState(DWORD dwState);
		DWORD GetState() const;
		VXMLString GetVarValue(const VXMLString& _name)const;
		VXMLString GetSpeakNumberFiles(const VXMLString& expr, int sex )const;
		VXMLString GetSpeakTimeFiles(DWORD hh, DWORD mm, DWORD ss)const;
		VXMLString GetSpeakDateFiles(DWORD dd, DWORD mm, DWORD yy)const;
		VXMLString GetSpeakMoneyFiles(const VXMLString& expr, const VXMLString& format)const;
		VXMLString GetSpeakTrafficFiles(const VXMLString& expr)const;
		VXMLString GetSpeakMinutesFiles(const VXMLString& expr)const;
		VXMLString GetSpeakSMSFiles(const VXMLString& expr)const;

		void startKeepAliveTimers();
		void stopKeepAliveTimers();
		bool IsSessionActive()const;
		//bool IsMenuStillActive()const;
		bool IsDtmfInputMode()const;
		bool IsVoiceInputMode()const;
		bool IsAudioOutputMode()const;
		bool IsT2SOutputMode()const;

		void DoClearDigits();
		void DoGetDigits(int aMaxDtmf);
		void DoStartDialog(const VXMLString& filename, const VXMLString& menuid, const VXMLString& parenttype, const VXMLString& namelist);
		void DoStartFunction(const VXMLString& function_name, const VXMLString& namelist);
		void DoPlayWav(const VXMLString& uri, const WaitingEvents& _extraEvents, bool aIsDtmfInput, bool aIsVoiceInput);
		void DoSpeakText(const VXMLString& aText, bool aIsDtmfInput, bool aIsVoiceInput);
		void DoDialogTransfer(const VXMLString& _dest, 
			bool bBridge, 
			const VXMLString& _max_time, 
			const VXMLString& _connection_timeout,
			const VXMLString& _aai,
			const VXMLString& _SIPContact,
			const VXMLString& _SIPUserToUser,
			const VXMLString& _Trunk,
			const VXMLString& _TransferAudio,
			const VXMLString& _Namelist,
			TransferInfo& outTransferInfo);

		bool PrepareDoc(DocumentPtr pDoc, const VXMLString& sURI);
		void DoDocumentFailed(const VXMLString& _source, const VXMLString& _error_description);

		//TagWPtr DoLoadDocument(const VXMLString& sURI, const bool& bFile);

		//TagWPtr GetMenuTag(const VXMLString& menu_name, const VXMLString& fieldName) const;
		MenuPtr GetMenu(const VXMLString& menu_name, const VXMLString& fieldName) const;
		MenuPtr GetFirstMenu() const;
		
		MenuPtr GetCurMenu();
		MenuPtr GetCurMenu() const;

		//std::pair<TagWPtr, VXMLString> GetEventTag(const CMessage& aEvent);
		TagPtr GetDocumentEnterPoint()const;
		inline ScopeServicePtr CreateSessionScope() const ;

		template <class... TArgs>
		void DoErrorBadfetch(TArgs&& ... aArgs)
		{
			DoError(L"error.badfetch", std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void DoErrorSemantic(TArgs&& ... aArgs)
		{
			DoError(L"error.semantic", std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void DoErrorUnrecoverable(TArgs&& ... aArgs)
		{
			DoError(L"error.unrecoverable", std::forward<TArgs>(aArgs)...);
		}

		bool IsRecursiveEvent() const;

	private:

		std::unique_ptr<AsyncEventClient> m_asyncClient;
		EventDispatcherPtr m_dispatcher;
		KeepAliveProviderPtr m_keepAlive;
	};
}

/******************************* eof *************************************/