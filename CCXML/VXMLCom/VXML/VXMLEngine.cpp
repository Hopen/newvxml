/************************************************************************/
/* Name     : VXMLCom\VXMLNEngine.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 7 Dec 2009                                                */
/************************************************************************/

#include "..\StdAfx.h"
#include "VXMLEngine.h"
#include "sv_strutils.h"
#include "VXMLResCache.h"
#include "..\Common\SystemLogEx.h"
#include "..\Common\ExtraLog.h"

//using namespace enginelog;

namespace VXML
{
	class CLocker
	{
	private:
		CRITICAL_SECTION& m_CS;

	public:
		CLocker(CRITICAL_SECTION& cs) : m_CS(cs)
		{
			::EnterCriticalSection(&m_CS);
		}
		~CLocker()
		{
			::LeaveCriticalSection(&m_CS);
		}
	};

	// CLSID VBScript
	#include <initguid.h>
	DEFINE_GUID(CLSID_VBScript, 0xb54f3741, 0x5b07, 0x11cf, 0xa4, 0xb0, 0x0,
		0xaa, 0x0, 0x4a, 0x55, 0xe8); 

	CScriptEngine::CScriptEngine(SessionPtr2 pSesion, SystemLogPtr pLog) : m_pMySite(NULL), m_Log(pLog)//, m_rCS(pSesion->getPtrToCS())
	{
		// Create script site and COM-objects
		// reference counter's this objects == 0
		//::InitializeCriticalSection(&m_CS);

		pSesion->Log(L"Create script site and COM-objects");
		CComObject<CXMLScriptSite>::CreateInstance(&m_pMySite);
		//CComObject<CNameTable>::CreateInstance(&m_pBridge);
		//m_pBridge->CreateInstance()
		m_pMySite->SetSession(pSesion);


		HRESULT hr = S_OK;
		// init m_spUnkScriptObject
		//hr = m_pBridge->QueryInterface(IID_IUnknown,
		//	(void **)&m_pMySite->m_spUnkScriptObject);

		// load and register *.tlb library
		VXMLString sDllPath = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"VXMLCom.tlb";
		pSesion->Log(L"load and register VXMLCom.tlb library, by path: \"%s\"", sDllPath.c_str());
		CComPtr<ITypeLib> sptLib;
		hr = LoadTypeLib(sDllPath.c_str(), &sptLib);
		//#ifdef _DEBUG
		if (FAILED(hr))
		{
			pSesion->Log(L"failed to load VXMLCom.tlb library, using default path: \"c:\\is3\\CCXML\\VXMLCom.tlb\"");
			hr = LoadTypeLib(L"c:\\is3\\CCXML\\VXMLCom.tlb", &sptLib);
		}
		//#endif

		if (FAILED(hr))
		{
			pSesion->Log(L"failed to load VXMLCom.tlb library");
			throw VXMLString(L"failed to load VXMLCom.tlb library");
		}
		// init IActiveScriptSite and ITypeInfo
		hr = sptLib->GetTypeInfoOfGuid(CLSID_NameTable, &m_pMySite->m_spTypeInfo);
		if (FAILED(hr))
		{
			pSesion->Log( L"failed to GetTypeInfoOfGuid");
			throw VXMLString(L"failed to GetTypeInfoOfGuid");;
		}


		// connect to VBScript-machine
		hr = CoCreateInstance(CLSID_VBScript, NULL, CLSCTX_INPROC_SERVER,
			IID_IActiveScript, (void **)&m_spAS);

		if (FAILED(hr))
		{
			pSesion->Log( L"failed to CoCreateInstance CLSID_VBScript");
			throw VXMLString(L"failed to CoCreateInstance CLSID_VBScript");;
		}


		// get IActiveScriptParse
		hr = m_spAS->QueryInterface(IID_IActiveScriptParse, (void **)&m_spASP);

		if (FAILED(hr))
		{
			pSesion->Log( L"failed to QueryInterface IID_IActiveScriptParse");
			throw VXMLString(L"failed to QueryInterface IID_IActiveScriptParse");;
		}


		// set sript site -> increase reference count
		hr = m_spAS->SetScriptSite((IActiveScriptSite *)m_pMySite);

		if (FAILED(hr))
		{
			pSesion->Log( L"failed to SetScriptSite IActiveScriptSite");
			throw VXMLString(L"failed to SetScriptSite IActiveScriptSite");;
		}


		// init machine
		hr = m_spASP->InitNew();

		if (FAILED(hr))
		{
			pSesion->Log( L"failed to m_spASP InitNew");
			throw VXMLString(L"failed to m_spASP InitNew");;
		}


		//ExecNormalStatement(L"localroot=\'/Report/Page/Group/Item[Title = 'Computer Name']/Value\'");
	}

	CScriptEngine::~CScriptEngine()
	{
		//
		//m_pMySite->Release();
		//::DeleteCriticalSection(&m_CS);
	}

	void CScriptEngine::Clear()
	{
		if (m_spAS)
		{
			m_spAS->SetScriptState(SCRIPTSTATE_DISCONNECTED);
			m_spAS->Close();
			m_spAS.Release();
		}
		if (m_spASP)
		{
			m_spASP.Release();
		}
		//if (m_pMySite)
		//{
		//	m_pMySite->Clear();
		//	m_pMySite->Release();
		//}
	}

	void CScriptEngine::Normalization(std::wstring& sExpr, std::wstring simbolsIn[], std::wstring simbolsOut[])
	{

		ATLASSERT(sizeof(simbolsOut) == sizeof(simbolsIn));

		VXMLString *pSim = simbolsIn;
		unsigned int i = 0;
		while (pSim && !pSim->empty())
		{
			int pos = -1;
			while ((pos = sExpr.find(*pSim, ++pos)) >= 0)
			{
				sExpr.replace(pos, pSim->size(), simbolsOut[i]);
			}

			++i;
			++pSim;
		}
		//va_list vl;
		//va_start(vl,sExpr);
		//VXMLString sim = va_arg(vl,VXMLString);
		//while (!sim.empty())
		//{
		//	int pos = -1;
		//	while ((pos = sExpr.find(sim,++pos))>=0)
		//	{
		//		sExpr.replace(pos,1,L"");
		//	}

		//	sim = va_arg(vl,VXMLString);
		//}
		//va_end(vl);


		//int pos = -1;
		////while ((pos = sExpr.find(L"'",++pos))>=0)
		////{
		////	sExpr.replace(pos,1,L"\"");
		////}
		////pos = -1;
		//while ((pos = sExpr.find(L"$",++pos))>=0)
		//{
		//	sExpr.replace(pos,1,L"");
		//}
		//pos = -1;
		//while ((pos = sExpr.find(L"==",++pos))>=0)
		//{
		//	sExpr.replace(pos,2,L"=");
		//}
		////pos = -1;
		////while ((pos = sExpr.find(L"#",++pos))>=0)
		////{
		////	sExpr.replace(pos,2,L"");
		////}
		m_scriptSource = sExpr;
	}

	HRESULT CScriptEngine::AddObject(const VXML::VXMLString &sName, /*IDispatch **/const CComVariant &object)
	{
		std::wstring sNew(sName);
		VXMLString simbolsIn[] = { L"'"  , L"$", L"==", L"" };
		VXMLString simbolsOut[] = { L"\"" , L"" , L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		//CLocker lock(m_rCS);
		m_pMySite->AddObject(sNew, object);
		return m_spAS->AddNamedItem(sNew.c_str(), SCRIPTITEM_ISVISIBLE |
			SCRIPTITEM_ISSOURCE);
	}

	HRESULT CScriptEngine::AddObject2(const VXML::VXMLString & sName, const CComVariant & object)
	{
		m_pMySite->AddObject(sName, object);
		return m_spAS->AddNamedItem(sName.c_str(), SCRIPTITEM_ISVISIBLE |
			SCRIPTITEM_ISSOURCE);
		return S_OK;
	}


	HRESULT CScriptEngine::DelObject(const std::wstring &sName)
	{
		std::wstring sNew(sName);
		VXMLString simbolsIn[] = { L"'"  , L"$", L"==", L"" };
		VXMLString simbolsOut[] = { L"\"" , L"" , L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		//CLocker lock(m_rCS);
		return m_pMySite->DelObject(sNew);
	}


	CComVariant CScriptEngine::ExprEval(const std::wstring& sExpr)
	{
		std::wstring sNew(sExpr);
		VXMLString simbolsIn[] = { L"'"  , L"$", L"==", L"" };
		VXMLString simbolsOut[] = { L"\"" , L"" , L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		//CLocker lock(m_rCS);
		CComVariant v;
		EXCEPINFO ei;
		m_spASP->ParseScriptText(/*sExpr*/sNew.c_str(), NULL,
			NULL, NULL, 0, 0, SCRIPTTEXT_ISEXPRESSION, &v, &ei);
		return v;
	}

	HRESULT CScriptEngine::ExecStatement(const std::wstring& sExpr)
	{
		std::wstring sNew(sExpr);
		VXMLString simbolsIn[] = { L"'"  , L"$", L"==", L"" };
		VXMLString simbolsOut[] = { L"\"" , L"" , L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		m_Log->LogInfo( L"ExecStatement: %s", sNew.c_str());

		//CLocker lock(m_rCS);
		EXCEPINFO ei;
		HRESULT hr = m_spASP->ParseScriptText(sNew.c_str(), NULL,
			NULL, NULL, 0, 0, 0L, NULL, &ei);
		if (SUCCEEDED(hr))
			hr = m_spAS->SetScriptState(SCRIPTSTATE_CONNECTED);
		else
			m_spAS->SetScriptState(SCRIPTSTATE_DISCONNECTED);
		return hr;
	}

	HRESULT CScriptEngine::ExecStatement2(const std::wstring& sExpr)
	{
		EXCEPINFO ei;
		HRESULT hr = m_spASP->ParseScriptText(sExpr.c_str(), NULL,
			NULL, NULL, 0, 0, 0L, NULL, &ei);
		if (SUCCEEDED(hr))
			hr = m_spAS->SetScriptState(SCRIPTSTATE_CONNECTED);
		else
			m_spAS->SetScriptState(SCRIPTSTATE_DISCONNECTED);

		return hr;
	}

	HRESULT CScriptEngine::ExecAssignNormalStatement(const std::wstring& sExpr)
	{
		std::wstring sNew(sExpr);
		VXMLString simbolsIn[] = { L"==", L"" };
		VXMLString simbolsOut[] = { L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		return ExecStatement2(sNew);
	}

	HRESULT CScriptEngine::ExecNormalStatement(const std::wstring& sExpr)
	{
		std::wstring sNew(sExpr);
		VXMLString simbolsIn[] = { L"'"  , L"$", L"==", L"" };
		VXMLString simbolsOut[] = { L"\"" , L"" , L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		return ExecStatement(sNew);
	}

	void CScriptEngine::Assign(const std::wstring& sExpr, const CComVariant& value)
	{
		//CCXMLString sExpr2(L"TypeName(" + sExpr + L")");
		////CComVariant v; ExecNormalStatement(L"x = 'sdfsdf'");
		////v = ExprEval(L"TypeName(x)");
		//// Generate random variable name
		//CCXMLString sVarName = format_wstring(L"tempvar%u", GetTickCount());
		//// Create temp variable
		//VAR& var = m_pNameTbl->AddVar(sVarName, value, true);

		//// Make assignment
		//ExecNormalStatement(sExpr + L"=" + sVarName);
		////CComVariant v = ExprEval(sExpr2);
		//// Delete temp variable
		//m_pNameTbl->DelVar(sVarName);
		//ExprEval(L"evt.connection.remote == 'tel:+18315551234'");
	}

	bool CScriptEngine::ExprEvalToBool(const std::wstring& sExpr)
	{
		return ExprEvalToType(sExpr, VT_BOOL).boolVal == VARIANT_TRUE;
	}

	std::wstring CScriptEngine::ExprEvalToStr(const std::wstring& sExpr)
	{
		return ExprEvalToType(sExpr, VT_BSTR).bstrVal;
	}

	int CScriptEngine::ExprEvalToInt(const std::wstring& sExpr)
	{
		return ExprEvalToType(sExpr, VT_INT).intVal;
	}

	CComVariant CScriptEngine::ExprEvalToType(const std::wstring& sExpr, VARTYPE vt)
	{
		CComVariant v = ExprEval(sExpr);
		HRESULT hr = v.ChangeType(vt);
		if (FAILED(hr))
		{
			LPCWSTR pwszType = L"Unknown type";
			switch (vt)
			{
			case VT_I1:
			case VT_I2:
			case VT_I4:
			case VT_I8:
			case VT_INT:
			case VT_UI1:
			case VT_UI2:
			case VT_UI4:
			case VT_UI8:
			case VT_UINT:
				pwszType = L"Integer";
				break;
			case VT_BOOL:
				pwszType = L"Boolean";
				break;
			case VT_BSTR:
				pwszType = L"String";
				break;
			}
			throw format_wstring(L"Cannot evaluate expression '%s' to %s", sExpr.c_str(), pwszType);
		}
		return v;
	}

	HRESULT CScriptEngine::ExecScript(const std::wstring& sExpr)
	{
		//ExecNormalStatement(sExpr);
		std::wstring sNew(sExpr);
		VXMLString simbolsIn[] = { L"$", L"==", L"" };
		VXMLString simbolsOut[] = { L"" , L"=" , L"" };
		Normalization(sNew, simbolsIn, simbolsOut);

		//CLocker lock(m_rCS);
		EXCEPINFO ei;
		HRESULT hr = m_spASP->ParseScriptText(sNew.c_str(), NULL,
			NULL, NULL, 0, 0, 0L, NULL, &ei);
		if (SUCCEEDED(hr))
			hr = m_spAS->SetScriptState(SCRIPTSTATE_CONNECTED);
		else m_spAS->SetScriptState(SCRIPTSTATE_DISCONNECTED);

		return hr;
	}

}