/************************************************************************/
/* Name     : VXMLCom\VXMLNameTable.cpp                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 7 Dec 2009                                                */
/************************************************************************/

#include "..\StdAfx.h"
#include "VXMLNameTable.h"
#include "sv_strutils.h"
#include "..\EventBridge.h"
//#include "EngineConfig.h"
//#include "..\Engine\EngineLog.h"
#include "VXMLEngineEvent.h"
//using namespace enginelog;


namespace VXML
{
	// VAR implementation

	VAR::VAR()
		: InstanceController(L"VAR", L"c:\\is3\\logs\\ccxml\\vxml_temp_%y-%m-%d.log")
		, bIsEmpty(true)
		, sName(L"technical empty var")
	{
		LogDefaultCtor();
	}


	VAR::VAR(const VXMLString& name, const CComVariant& value, bool readonly)
		: InstanceController(L"VAR", L"c:\\is3\\logs\\ccxml\\vxml_temp_%y-%m-%d.log")
		, sName(name)
		, vValue(value)
		, bReadOnly(readonly)
		, bIsEmpty(false)
	{
		LogDefaultCtor();
		LogPlace(L"Name: %s", name.c_str());

		//SetInitialValue(value);
	}

	VAR::VAR(const VAR& rhs)
		: InstanceController(L"VAR", L"c:\\is3\\logs\\ccxml\\vxml_temp_%y-%m-%d.log")
	{
		//SetInitialValue(rhs.vValue);
		this->sName = rhs.sName;
		this->bReadOnly = rhs.bReadOnly;
		this->bIsEmpty = rhs.bIsEmpty;
		this->vValue = rhs.vValue;

		LogCopyCtor();
		LogPlace(L"Name: %s", sName.c_str());
	}

	VAR& VAR::operator=(VAR const & rhs)
	{
		if (this == &rhs) {
			return *this;
		}
		this->vValue = rhs.vValue;
		//SetInitialValue(rhs.vValue);
		this->sName = rhs.sName;
		this->bReadOnly = rhs.bReadOnly;
		this->bIsEmpty = rhs.bIsEmpty;

		return *this;
	}

	VAR::~VAR()
	{
		vValue.Clear();

		LogDtor();
		LogPlace(L"Name: %s", sName.c_str());
	}

	void VAR::SetValue(const CComVariant& newVal)
	{
		if (vValue.vt & VT_BYREF)
		{
			::VariantCopy(vValue.pvarVal, &newVal);
		}
		else
		{
			vValue = newVal;
		}
	}

	//void VAR::SetValue(VARIANT* pNewVal)
	//{
	//	if (bReadOnly)
	//	{
	//		throw format_wstring(L"Cannot set value of read-only variable '%s'", sName.c_str());
	//	}
	//	if (vValue.vt & VT_BYREF)
	//	{
	//		::VariantCopy(vValue.pvarVal, pNewVal);
	//	}
	//	else
	//	{
	//		::VariantCopy(&vValue, pNewVal);
	//	}
	//}

	//void VAR::SetValue(const CComVariant& newVal)
	//{
	//	if (vValue.vt & VT_BYREF)
	//	{
	//		::VariantCopy(vValue.pvarVal, (VARIANT*)&newVal);
	//	}
	//	else
	//	{
	//		::VariantCopy(&vValue, (VARIANT*)&newVal);
	//	}
	//}

	////void VAR::Clear()
	////{
	////	switch (vValue.vt)
	////	{
	////	case VT_I1:
	////	case VT_I2:
	////	case VT_I4:
	////	case VT_I8:
	////	case VT_INT:
	////	case VT_UI1:
	////	case VT_UI2:
	////	case VT_UI4:
	////	case VT_UI8:
	////	case VT_UINT:
	////		vValue.intVal = 0;
	////		break;
	////	case VT_BOOL:
	////		vValue.boolVal = FALSE;
	////		break;
	////	case VT_BSTR:
	////		vValue.bstrVal = L"";
	////		break;
	//////	case VT_DISPATCH:
	//////{
	//////		EventObject objVar = ((EventDispatchPtr)(vValue.pdispVal))->GetObjectPtr();
	//////		if (objVar.get())
	//////			objVar->ClearValues();
	//////		break;
	//////	}
	////	}
	////}

//	// CScopedNameTable implementation
//
//	CScopedNameTable::CScopedNameTable()
		//{
//
//		//							CEngineLog log;
//		//log.Init(CVXMLEngineConfig::Instance().sLogFileName.c_str());
//		//log.Log(__FUNCTIONW__, L"Constructor CScopedNameTable called: %i", ++CtorCount);
//
//		m_pGlobalNames.reset(new DEBUG_NEW_PLACEMENT CNameTable());
//		//m_pGlobalNames = SessionObject(new CNameTable());
//
//		//m_count = CtorCount;
		//	}
//
//	CScopedNameTable::~CScopedNameTable()
		//	{
//		//m_pGlobalNames->Release();
//		//if (m_gStack.size())
//		//{
//		//	for(unsigned int i=0;i<m_gStack.size();++i)
//		//	{
//		//		delete m_gStack[i];
//		//	}
//		//}
//		m_gStack.clear();
//
//		//									CEngineLog log;
//		//log.Init(CVXMLEngineConfig::Instance().sLogFileName.c_str());
//		//log.Log(__FUNCTIONW__, L"Destructor CScopedNameTable called: %i", m_count/*++DtorCount*/);
//		//++DtorCount;
		//	}
//
//	void CScopedNameTable::PushScope(const VXMLString& sName)
//	{
//		//// push globals to stack
//		//PushStack();
//
//		//// Create new name table for the scope
//		//CNameTable* pNameTbl = new CNameTable();
//
//		//VARIANT val;
//		//// Make it read-only variable in the global name table as IDispatch
//		//val.vt = VT_DISPATCH;
//		//val.pdispVal = pNameTbl;
//		//m_pGlobalNames->Add(sName, val, true);
//		//// Add the name to the scope list
//		//m_Scopes.push_back(ScopePair(sName, pNameTbl));
//
		//// push globals to stack
//		//PushStack();
//
		//// Create new name table for the scope
//		//CNameTable* pNameTbl = NULL;
//		//SessionObject pNameTbl = SessionObject(new CNameTable());
//		//CNameTable* tn = new CNameTable();
//
////		CComVariant pNameTbl;// = new CNameTable();
////
////		if (m_Scopes.empty())
////		{
////			pNameTbl = new DEBUG_NEW_PLACEMENT CNameTable();
////		}
////		else
////		//if (!m_Scopes.empty())
////		{
////			ScopePair& scope = *m_Scopes.rbegin();
////			//pNameTbl = new CNameTable(*(CNameTable*)(scope.second));
////			//scope.second.CopyTo(&tn);
////			//SessionObject obj1 = scope.second;
////			//CNameTable* sc1 = obj1.get();
////			//SessionObject obj2 = SessionObject(new CNameTable(*sc1));
////			//pNameTbl = obj2;
////
////			////pNameTbl = SessionObject(new CNameTable(*(CNameTable*)(scope.second.get())));
////
////			//CComVariant obj1 = scope.second;
////			//CComVariant obj2;
////			//obj1.Copy(&obj2);
////
////			//pNameTbl = obj2;
////
////			//pNameTbl.Copy(&scope.second);
////
////			CComVariant obj1 = scope.second;
////
////// 			CNameTable* nt1 = (CNameTable*)(obj1.pdispVal);
////// 			CNameTable* nt2 = new CNameTable(*nt1);
////// 
////// 			CNameTable* nt3 = new CNameTable(*((CNameTable*)(obj1.pdispVal)));
////
////
////			pNameTbl = new DEBUG_NEW_PLACEMENT CNameTable(*((CNameTable*)(obj1.pdispVal))); //copy
////		}
////		//CComTable pNameTbl(tn);
////
////		//VARIANT val;
////		//// Make it read-only variable in the global name table as IDispatch
////		//val.vt = VT_DISPATCH;
////		//val.pdispVal = pNameTbl/*.get()*/;
////		//m_pGlobalNames->Add(sName, val, true);
////		if (CNameTable* pGlobalNames = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
////			pGlobalNames->Add(sName, pNameTbl, true);
////		// Add the name to the scope list
////		m_Scopes.push_back(ScopePair(sName, pNameTbl));
//
//		SessionObject pNameTbl;
//
//		if (m_Scopes.empty())
//		{
//			pNameTbl.reset (new DEBUG_NEW_PLACEMENT CNameTable());
//		}
//		else
//		{
//			ScopePair& scope = *m_Scopes.rbegin();
//			pNameTbl = SessionObject(new DEBUG_NEW_PLACEMENT CNameTable(*(CNameTable*)(scope.second.get())));
//		}
//		//m_pGlobalNames->Add(sName, pNameTbl->AsVariant(), true);
//		// Add the name to the scope list
//		m_Scopes.push_back(ScopePair(sName, pNameTbl));
//
//	}
//
//	void CScopedNameTable::PopScope()
//	{
//		if (m_Scopes.empty())
//		{
//			return;
//		}
//
//		//ScopePair& scope = *m_Scopes.rbegin();
//		////// Delete the scope from the global name table
//		//// pop back from globals stack
//		////PopStack();
//
//		//// Delete the scope itself
//		//scope.second->Release();
//		// Delete the scope from the scope list
//		m_Scopes.pop_back();
//	}
//
//	//void CScopedNameTable::PushStack()
//	//{
//	//	////CNameTable* copy_globals = new CNameTable(*m_pGlobalNames);
//	//	////m_gStack.push_back(copy_globals);
//
//	//	////CNameTable* copy_globals = new CNameTable(*m_pGlobalNames);
//	//	//CComVariant copy_globals;// = new CNameTable(*m_pGlobalNames);
//	//	////SessionObject copy_globals = SessionObject(new CNameTable(*m_pGlobalNames.get()));
//	//	//m_pGlobalNames.Copy(&copy_globals);
//	//	//m_gStack.push_back(m_pGlobalNames);
//	//	//m_pGlobalNames = copy_globals;
//
//	//	SessionObject copy_globals = SessionObject(new CNameTable(*m_pGlobalNames.get()));
//	//	m_gStack.push_back(m_pGlobalNames);
//	//	m_pGlobalNames = copy_globals;
//	//}
//
//	//void CScopedNameTable::PopStack()
//	//{
//	//	if (!m_gStack.size())
//	//		return;
//	//	//m_pGlobalNames->Release(); Dtor
//	//	m_pGlobalNames = m_gStack[m_gStack.size()-1];
//	//	m_gStack.pop_back();
//	//}
//
//	VAR& CScopedNameTable::AddVar(const VXMLString& sName, 
//		const CComVariant& val, 
//		bool bReadOnly)
//	{
//		////if (m_Scopes.empty())
//		////{
//		////	// Add the variable to the global name table
//		////	return m_pGlobalNames->Add(sName, val, bReadOnly);
//		////}
//		////// Add the variable to the current scope
//		////VAR& var = m_Scopes.rbegin()->second->Add(sName, val, bReadOnly);
//		////// Add the reference to the global name table
//		////CComVariant refval;
//		////refval.vt = VT_VARIANT | VT_BYREF;
//		////refval.pvarVal = &var.vValue;
//		////// If the reference already exists, it is updated, so if there is the variable
//		////// with the same name in several different scopes, it is always visible in
//		////// the global name table as that in the current scope.
//		////m_pGlobalNames->Add(sName, refval, bReadOnly);
//		////return var;
//
//
		////if (m_Scopes.empty())
		////{
		////	// Add the variable to the global name table
//		//	if (CNameTable* pGlobalNames = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//		//		return pGlobalNames->Add(sName, val, bReadOnly);
//		//	
		////}
//		//CNameTable* pScopes = dynamic_cast<CNameTable*>(m_Scopes.rbegin()->second.pdispVal);
		////// Add the variable to the current scope
//		//VAR& var = pScopes->Add(sName, val, bReadOnly);
		////// Add the reference to the global name table
		////CComVariant refval;
		////refval.vt = VT_VARIANT | VT_BYREF;
		////refval.pvarVal = &var.vValue;
		////// If the reference already exists, it is updated, so if there is the variable
		////// with the same name in several different scopes, it is always visible in
		////// the global name table as that in the current scope.
//		//if (CNameTable* pGlobalNames = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//		//	pGlobalNames->Add(sName, refval, bReadOnly);
		////return var;
//
		//if (m_Scopes.empty())
		//{
		//	// Add the variable to the global name table
//			return m_pGlobalNames->Add(sName, val, bReadOnly);
		//	
		//}
//
		//// Add the variable to the current scope
//		VAR& var = m_Scopes.rbegin()->second->Add(sName, val, bReadOnly);
		//// Add the reference to the global name table
		//CComVariant refval;
		//refval.vt = VT_VARIANT | VT_BYREF;
		//refval.pvarVal = &var.vValue;
		//// If the reference already exists, it is updated, so if there is the variable
		//// with the same name in several different scopes, it is always visible in
		//// the global name table as that in the current scope.
//
//		////////////SessionObject pNameTbl;
//
//		////////////if (m_Scopes.empty())
//		////////////{
//		////////////	pNameTbl.reset(new DEBUG_NEW_PLACEMENT CNameTable());
//		////////////}
//		////////////else
//		////////////{
//		////////////	ScopePair& scope = *m_Scopes.rbegin();
//		////////////	pNameTbl = SessionObject(new CNameTable(*(CNameTable*)(scope.second.get())));
//		////////////}
//
//		////////////m_pGlobalNames->Add(sName, pNameTbl->AsVariant(), bReadOnly);
//
//		CComVariant refval2(val);
//
//		m_pGlobalNames->Add(sName, refval2, bReadOnly);
		//return var;
//	}
//
//	void CScopedNameTable::DelVar(const VXMLString& sName)
//	{
		//if (m_Scopes.empty())
		//{
		//	// Delete the variable from the global name table
//			m_pGlobalNames->Del(sName);
		//}
		//else
		//{
		//	// Delete the variable from the current scope
//			m_Scopes.rbegin()->second->Del(sName);
		//}
//
//
//		//if (m_Scopes.empty())
//		//{
//		//	// Delete the variable from the global name table
//		//	if (CNameTable* pGlobalNames = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//		//		pGlobalNames->Del(sName);
//		//}
//		//else
//		//{
//		//	// Delete the variable from the current scope
//		//	if( CNameTable* pScopes = dynamic_cast<CNameTable*>(m_Scopes.rbegin()->second.pdispVal))
//		//		pScopes->Del(sName);
//		//}
//	}
//
//	VAR* CScopedNameTable::Find(const VXMLString& sName)
//	{
//		//////////// Find the variable in the global name table
//		//////////return m_pGlobalNames->Find(sName);
//
//		//////if (m_Scopes.empty())
//		//////{
//		//////	// Lookup the variable in the global name table
//		//////	return m_pGlobalNames->Find(sName);
//		//////}
//		//////// Lookup the variable in the current scope
//		//////return m_Scopes.rbegin()->second->Find(sName);
//
//		//////		// Find the variable in the global name table
//		////////return m_pGlobalNames->Find(sName);
//		////////if (m_Scopes.empty())
//		////////{
//		////////	// Lookup the variable in the global name table
//		////////	if (CNameTable* pGlobalNames = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//		////////		return pGlobalNames->Find(sName);
//		////////}
//		////////// Lookup the variable in the current scope
//		////////CNameTable* pScopes = dynamic_cast<CNameTable*>(m_Scopes.rbegin()->second.pdispVal);
//		////////if (pScopes)
//		////////	return pScopes->Find(sName);
//		////////else
//		////////{
//		////////	return NULL;
//		////////}
//
//		// Find the variable in the global name table
//
//		if (m_Scopes.empty())
//		{
//			// Lookup the variable in the global name table
		//return m_pGlobalNames->Find(sName);
//		}
//
//		//// Lookup the variable in the current scope
//		//if (VAR* var = m_Scopes.rbegin()->second->Find(sName))
//		//	return var;
//
		////return m_pGlobalNames->Find(sName);
//
//		// Lookup the variable in the current scope
//		return m_Scopes.rbegin()->second->Find(sName);
//	}
//
//	SessionObject CScopedNameTable::FindScope(const VXMLString& sName)
//	{
//		if (m_Scopes.empty())
//		{
//			return SessionObject();
//		}
//
//		return m_Scopes.rbegin()->second;
//	}
//
//	VAR& CScopedNameTable::Lookup(const VXMLString& sName)
//	{
		//if (m_Scopes.empty())
		//{
		//	// Lookup the variable in the global name table
//			return m_pGlobalNames->Lookup(sName);
		//}
		//// Lookup the variable in the current scope
//		return m_Scopes.rbegin()->second->Lookup(sName);
//
//		//if (m_Scopes.empty())
//		//{
//		//	// Lookup the variable in the global name table
//		//	if (CNameTable* pGlobalNames = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//		//		return pGlobalNames->Lookup(sName);
//		//}
//		//// Lookup the variable in the current scope
//		//CNameTable* pScopes = dynamic_cast<CNameTable*>(m_Scopes.rbegin()->second.pdispVal);
//		//if (pScopes)
//		//	return pScopes->Lookup(sName);
//
//		//throw(L"CScopedNameTable::Lookup: We got no RTTI when looking for \"sName\"");
//
//	}
//
//	//CNameTable* CScopedNameTable::GetGlobal()const
//	//{
//	//	if (CNameTable * nt = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//	//		return nt;
//	//	return NULL;
//	//}
//
//	CComVariant CScopedNameTable::GetGlobal()const
		//{
//		//if (CNameTable * nt = dynamic_cast<CNameTable*>(m_pGlobalNames.pdispVal))
//		//	return nt;
//		//return NULL;
//		return m_pGlobalNames->AsVariant();
		//}


	// CScopeService implementation

	CScopeService::CScopeService()
	{
		//m_GlobalScope = std::make_shared<CNameTable>();
		//PushScope(m_GlobalScope);
	}

	//void CScopeService::PushScope()
	//{
	//	PushScope(std::make_shared<CNameTable>());
	//}

	//void CScopeService::_PushScope(SessionObject& scope)
	//{
	//	if (!scope.get())
	//	{
	//		SessionObject parentScope = m_scopes.back();
	//		scope = SessionObject(new DEBUG_NEW_PLACEMENT CNameTable(*(CNameTable*)(parentScope.get())));
	//		//scope->SetParent(parentScope);
	//	}
	//	m_scopes.push_back(scope);
	//}

	//void CScopeService::_PopScope()
	//	{
	//	m_scopes.pop_back();
	//	}


	VAR& CScopeService::AddVar(const VXMLString& sName, const CComVariant& val, bool bReadOnly)
	{
		VAR& value = m_scopes.back()->Add(sName, val, bReadOnly);

		//m_GlobalScope->Add(sName, val, bReadOnly);

		// looking for global var
		if (m_scopes.size() > 1)
		{
			SessionObject global_vars = *m_scopes.begin(); // must be global vars

			VAR * global_var = global_vars->Find(sName);
			if (global_var)
			{
				global_var->SetValue(val);
			}
		}

		//if (m_scopes.size() > 1)
		//{
		//	ScopeContainer backScope(m_scopes);
		//	std::reverse(backScope.begin(), backScope.end());
		//	for each(SessionObject scope in backScope)
		//	{
		//		if (!scope->Find(sName))
		//		{
		//			break;
		//		}
		//		scope->Add(sName, val, bReadOnly);
		//	}
		//}

		return value;
		
		//return m_GlobalScope->Add(sName, val, bReadOnly);
	}
	VAR* CScopeService::Find(const VXMLString& sName)
	{
		//return m_scopes.back()->Find(sName);
		VAR* pVar{ nullptr };

		if (m_scopes.size())
		{
			std::find_if(m_scopes.rbegin(), m_scopes.rend(), [sName, &pVar](const SessionObject& _scope)
			{
				pVar = _scope->Find(sName);
				if (pVar)
				{
					return true;
				}

				return false;
			});
		}

		return pVar;
	}
	VAR& CScopeService::Lookup(const VXMLString& sName)
	{
		//return m_scopes.back()->Lookup(sName);
		VAR* pVar = Find(sName);
		if (!pVar)
		{
			throw format_wstring(L"Name '%s' not found", sName.c_str());
		}
		return *pVar;
	}

	void CScopeService::DelVar(const VXMLString& sName)
	{
		if (m_scopes.size())
		{
			SessionObject& _scope = *m_scopes.rbegin();
			_scope->Del(sName);
		}
	}

	//CComVariant CScopeService::GetGlobal()const
	//{
	//	return m_GlobalScope->AsVariant();
	//}



}