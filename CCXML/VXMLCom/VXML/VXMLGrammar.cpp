/************************************************************************/
/* Name     : VXMLCom\VXMLDocument.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 15 Dec 2009                                               */
/************************************************************************/
#include "..\StdAfx.h"
#include "VXMLGrammar.h"
#include <boost/scoped_array.hpp>
#include "sv_strutils.h"
//#include "StringFunctions.h"
//#include "..\Common\EngineScriptSite.h"

namespace VXML
{
	int VXMLGrammar::Load(const VXML::VXMLString &_in_file)
	{
		//m_events.clear();
		HANDLE hFile = CreateFile ( _in_file.c_str(), 
			GENERIC_READ/*|GENERIC_WRITE*/, 
			FILE_SHARE_READ, 
			NULL, 
			OPEN_EXISTING, 
			FILE_FLAG_SEQUENTIAL_SCAN, 
			NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			std::string error = "Cannot load grammar by name: " + wtos(_in_file);
			throw std::runtime_error(error.c_str());
		}
		unsigned int size = 	GetFileSize(hFile, NULL)*sizeof(WCHAR);
		auto pReturnedString = std::make_unique<WCHAR[]>(size);
		if (!GetPrivateProfileString(NULL,NULL,NULL,pReturnedString.get(),size,_in_file.c_str()))
			return 1;
		
		std::vector<VXMLString>eventsName;
		VXMLString Name;
		for(unsigned int i=0;i<size;++i)
		{
			if ((pReturnedString.get())[i] == L'\0') //ToDO: check
			{
				if (Name.length()==0)
					break;
				eventsName.push_back(Name);
				Name.clear();
			}
			else
				Name+=(pReturnedString.get())[i];
		}

		for (unsigned int i=0;i<eventsName.size();++i)
		{
			unsigned int j=0;
			WCHAR str[256];
			ZeroMemory(str,256);
			while (GetPrivateProfileString(eventsName[i].c_str(),Utils::toStr<unsigned int>(j).c_str(),NULL,str,255,_in_file.c_str()))
			{
				VAR & var = Add(eventsName[i],str);
				++j;
			}
		}

		return 0;
	}

	void VXMLGrammar::SetExternalSource(const VXMLString& aSrc)
	{
		if (aSrc.find(L"http://") >= 0)
		{
			m_Source = aSrc;
		}
		else
		{
			// ToDo: load from file
		}
	}

	VAR& VXMLGrammar::Add(const VXMLString& sName, const CComVariant& val)
	{
		NameContainer::iterator it = m_Names.insert(std::pair<std::wstring,VAR>(sName,VAR(sName, val, false)));
		return it->second;
	}

	int VXMLGrammar::LookUp(const VXMLString& event, const VXMLString& dtmf)
	{
		NameContainer::iterator it = m_Names.find(event);
		while (it!= m_Names.end() && (it->first == event))
		{
			if(static_cast<CGrammarStr>(it->second.vValue.bstrVal) == dtmf)
				return 1;
			++it;
		}
		return 0;
	}

	VXMLString VXMLGrammar::LookUp(const VXMLString& dtmf)
	{
		NameContainer::iterator it = m_Names.begin();
		while (it!=m_Names.end())
		{
			if(static_cast<CGrammarStr>(it->second.vValue.bstrVal) == dtmf)
				return it->first;
			++it;
		}
		return L"";
	}

	VAR* VXMLGrammar::Find(const VXMLString& sName)
	{
		NameContainer::iterator it = m_Names.find(sName);
		return (it == m_Names.end()) ? NULL : &it->second;
	}

	// IDispatch methods

	STDMETHODIMP VXMLGrammar::GetTypeInfoCount( 
		/* [out] */ UINT *pctinfo)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP VXMLGrammar::GetTypeInfo( 
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ ITypeInfo **ppTInfo)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP VXMLGrammar::GetIDsOfNames( 
		/* [in] */ REFIID riid,
		/* [size_is][in] */ LPOLESTR *rgszNames,
		/* [in] */ UINT cNames,
		/* [in] */ LCID lcid,
		/* [size_is][out] */ DISPID *rgDispId)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP VXMLGrammar::Invoke( 
		/* [in] */ DISPID dispIdMember,
		/* [in] */ REFIID riid,
		/* [in] */ LCID lcid,
		/* [in] */ WORD wFlags,
		/* [out][in] */ DISPPARAMS *pDispParams,
		/* [out] */ VARIANT *pVarResult,
		/* [out] */ EXCEPINFO *pExcepInfo,
		/* [out] */ UINT *puArgErr)
	{
		return E_NOTIMPL;
	}

	//// IUnknown methods

	//STDMETHODIMP VXMLGrammar::QueryInterface( 
	//	/* [in] */ REFIID riid,
	//	/* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject)
	//{
	//	return E_NOTIMPL;
	//}

	//ULONG VXMLGrammar::AddRef(void)
	//{
	//	return ++m_ulRefCount;
	//}

	//ULONG VXMLGrammar::Release(void)
	//{
	//	ULONG result = --m_ulRefCount;
	//	if (!result)
	//	{

	//		delete this;
	//	}
	//	return result;
	//}
}
/******************************* eof *************************************/