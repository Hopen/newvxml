/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLEventScope.cpp                           */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 22 May 2019                                               */
/************************************************************************/

#include "VXMLEventScope.h"
#include "VXMLTags.h"

namespace VXML
{
	std::weak_ptr<CTag> CEvtScope::GetExecTag() const
	{
		return m_pExecTag->shared_from_this();
	}

	CEvtScopeCollector::EventScope CEvtScopeCollector::ExtractCurrentScopes()
	{
		EventScope resultScope;

		// set own scope for each menu
		if (m_scopes.size()>1)
		{
			EventScope::reverse_iterator cit = m_scopes.rbegin();
			EventScope::reverse_iterator dit = m_scopes.rend();
			while (cit != m_scopes.rend())
			{
				if (cit->GetEventName().empty())
				{
					// remember position of flag
					if (dit == m_scopes.rend())
						dit = cit;
					++cit;
					continue;
				}

				resultScope.push_back(*cit);
				++cit;
			}
			if (dit != m_scopes.rend())
			{
				EventScope copy(++dit, m_scopes.rend());
				copy.reverse();
				m_scopes.swap(copy);
			}
		}
		else
		{
			if (m_scopes.size() == 1)
			{
				auto first = m_scopes.front();
				if (!first.GetEventName().empty())
				{
					//resultScope.swap(m_scopes);
					resultScope = m_scopes;
				}
				else
				{
					m_scopes.clear();
				}
			}
		}

		return resultScope;
	}
}

/******************************* eof *************************************/