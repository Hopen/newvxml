/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLAcync.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 17 Feb 2018                                               */
/************************************************************************/
#include "stdafx.h"
#include "VXMLAsync.h"
#include "IdGenerator.h"

namespace VXML
{
	CMessage AsyncEventClient::Create_ANY2AC_asyncHttpRequest(
		const ObjectId& aScriptId,
		const VXMLString& aRequestId,
		bool aIsAsync,
		const VXMLString& aUri,
		const VXMLString& aPort,
		const VXMLString& aRequest,
		bool aIsFetchAudio)
	{
		CMessage msg(L"ANY2AC_asyncHttpRequest");

		msg[L"ScriptId"] = aScriptId;
		msg[L"Host"] = aUri;
		msg[L"Port"] = aPort;
		msg[L"Request"] = aRequest;
		msg[L"IsAsync"] = aIsAsync;
		msg[L"IsFetchAudio"] = aIsFetchAudio;
		msg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;
		msg[L"RequestId"] = aRequestId;

		return msg;
	}

	CMessage AsyncEventClient::Create_ANY2AC_getResult(
		const VXMLString& aRequestId,
		bool aIsCancel)
	{
		CMessage msg(L"ANY2AC_getResult");

		msg[L"RequestId"] = aRequestId;
		msg[L"CancelRequest"] = aIsCancel;

		return msg;
	}

	CMessage AsyncEventClient::CreateCommon(
		const ObjectId& aScriptId,
		const VXMLString& aEvent,
		const VXMLString& aRequest)
	{
		CMessage msg(aEvent.c_str());
		msg[L"Request"] = aRequest;
		msg[L"ScriptID"] = aScriptId;

		return msg;
	}

	//CMessage AsyncEventClient::CreateMessage(const AsyncInfo& aInfo)
	//{
	//	CMessage msg;
	//	
	//	auto event = mConverter.GetEventByShortString(aInfo.Event);
	//	if (event == E_ASYNC_EVENT::AE_REQUEST)
	//	{
	//		msg = Create_ANY2AC_asyncHttpRequest(
	//			aInfo.ScriptId,
	//			aInfo.IsAsync,
	//			aInfo.Target,
	//			aInfo.Port,
	//			aInfo.Body);
	//	}
	//	else if (event == E_ASYNC_EVENT::AE_REQUEST)
	//	{
	//		msg = Create_ANY2AC_getResult(
	//			aInfo.RequestId,
	//			aInfo.IsCancel);
	//	}
	//	else
	//	{
	//		msg = CreateCommon(
	//			aInfo.ScriptId,
	//			aInfo.Event,
	//			aInfo.Body);
	//	}

	//	return msg;
	//}
}
/******************************* eof *************************************/