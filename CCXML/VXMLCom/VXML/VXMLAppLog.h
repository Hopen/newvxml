#include <memory>
#include "VXMLBase.h"

namespace VXML
{
	class CAppLog : public std::enable_shared_from_this<CAppLog>
	{
	public:
		CAppLog(const ExtraLogPtr& _log);

		virtual CComVariant AsVariant()
		{
			return CComVariant(new CComBridge<CAppLog>(shared_from_this()));
		}

	private:
		HRESULT log(const VXMLString&);

		HRESULT init(const VXMLString&);

	public:
		// IDispatch methods
		virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(
			/* [out] */ UINT *pctinfo);

		virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(
			/* [in] */ UINT iTInfo,
			/* [in] */ LCID lcid,
			/* [out] */ ITypeInfo **ppTInfo);

		STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId);
		STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr);

	private:
		ExtraLogPtr m_log;
	};
}