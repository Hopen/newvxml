/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLEngineEvent.h                            */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/

#pragma once

#include "EvtModel.h"
#include "VXMLBase.h"
#include "VXMLNameTable.h"
#include "VXMLMenu.h"
#include "..//EventBridge.h"

namespace VXML
{
	class CEventDispatcher;
	struct CEventFactory;

	enum class E_EVENT_TYPE
	{
		ET_UNKNOWN = 0,
		ET_SUB = 1,
		ET_RECURSIVE,
		ET_PREDEFINE
	};

	class CEngineEvent : public CNameTable
	{
		//friend class CEventDispatcher;
		//friend class CEventDispatcher;
		//friend struct CEventFactory;

	private:
		VXMLString	mName;
		//CMenu mMenu;
		//TagWPtr mCurTag;

		//VMStates    mState;
		//WaitingEventsContainer mEventsList;

		//ScopeServicePtr mScopeStack;

		//VXMLString mAttrId;

		//E_EVENT_TYPE mType;

	public:
		//CMessage Dump()const;
		CEngineEvent(const CMessage& msg);
		//CMessage ToMsg() const;
		bool MatchMask(const VXMLString& sMask);
	//	const VXMLString& GetName() const;
	//	CMenu& GetMenu() { return mMenu; }
	//
	//	bool IsDone()const { return mState == VMStates::VMS_NULL; }
	//	VMStates GetState()const { return mState; }
	//	bool isRecursiveEvent()const;
	//	bool isPredefineEvent()const;
	//	SavedScopeServicePtr GetScopeStack()const { return mScopeStack; }

	//	MessageHandler GetMessageHandlerByName(const VXMLString& _name)
	//	{
	//		MessageHandler handler;
	//		const auto cit = mEventsList.find(_name);
	//		if (cit != mEventsList.cend())
	//		{
	//			handler = cit->second;
	//		}
	//		return handler;
	//	}

	//	VXMLString GetAttrId() const { return mAttrId; }
	//	TagWPtr getBeginPoint()const { return mMenu.BeginTagPtr; }

	//private:

	//	TagWPtr getCurPoint()const { return mCurTag; }
	//	void setCurPoint(TagWPtr _tag)
	//	{
	//		mCurTag = _tag;
	//	}

	//	void SetState(const VMStates& _state) { mState = _state; }

	//	template <class TWaitingEventsContainer>
	//	void SetWaitingEvents(TWaitingEventsContainer&& _events){ mEventsList = std::forward<TWaitingEventsContainer>(_events); }
	//	bool IsListConsistEvent(const VXMLString& _event) const 
	//	{ 
	//		return !!mEventsList.count(_event);
	//	}

	//	template <class T>
	//	void SetScopeStack(T&& _stack)  { mScopeStack = std::forward<T>(_stack); }
	};

	class CItemEvent
	{
		friend class CEventDispatcher;
		friend class CEventDispatcher;

	public:
		CItemEvent() = default;
		virtual ~CItemEvent() = default;

		CItemEvent(const CItemEvent&) = default;
		CItemEvent& operator=(const CItemEvent&) = default;

	public:
		virtual VXMLString Dump() const;
		const VXMLString& GetName() const { return mName; }
		VMStates GetState()const { return mState; }
		SavedScopeServicePtr GetScopeStack()const { return mScopeStack; }
		VXMLString GetAttrId() const { return mAttrId; }

		bool MatchMask(const VXMLString& sMask);

		bool isRecursiveEvent()const { return mType == E_EVENT_TYPE::ET_RECURSIVE; }
		bool isPredefineEvent()const { return mType == E_EVENT_TYPE::ET_PREDEFINE; }

		MessageHandler GetMessageHandlerByName(const VXMLString& _name)
		{
			MessageHandler handler;
			const auto cit = mEventsList.find(_name);
			if (cit != mEventsList.cend())
			{
				handler = cit->second;
			}
			return handler;
		}

		// interface
		virtual TagWPtr getBeginPoint()const = 0;
		//virtual CMenu& GetMenu() = 0;
		MenuPtr GetMenu() { return mMenu; }

		template <class TWaitingEventsContainer>
		void SetWaitingEvents(TWaitingEventsContainer&& _events) { mEventsList = std::forward<TWaitingEventsContainer>(_events); }
		bool IsListConsistEvent(const VXMLString& _event) const
		{
			return !!mEventsList.count(_event);
		}

		TagWPtr getCurPoint()const { return mCurTag; }
		void setCurPoint(TagWPtr _tag)
		{
			mCurTag = _tag;
		}


	protected:
		VXMLString	mName;
		TagWPtr mCurTag;
		VMStates    mState;
		WaitingEventsContainer mEventsList;
		ScopeServicePtr mScopeStack;
		VXMLString mAttrId;
		E_EVENT_TYPE mType;

		MenuPtr mMenu;
	};

	class CMenuEvent : public CItemEvent
	{
		friend struct CEventFactory;

	public:
		TagWPtr getBeginPoint()const override { return mMenu->BeginTagPtr; }

		CMenuEvent() = default;
		virtual VXMLString Dump() const override;
	private:
		//CMenu mMenu;
	};

	class CDocEvent : public CItemEvent
	{
		friend struct CEventFactory;

	public:
		CDocEvent() = default;

		TagWPtr getBeginPoint()const override { return mRootTag; }

		virtual VXMLString Dump() const override;

	private:
		TagWPtr mRootTag;
		VXMLString mUri;
		//CMenu mMenu;
	};

	class CTagEvent : public CItemEvent
	{
		friend struct CEventFactory;

	public:
		CTagEvent() = default;
		//CTagEvent(CMenu& aMenu)
		//	: mMenu(aMenu)
		//{}

		TagWPtr getBeginPoint()const override { return mEnterPoint; }

		virtual VXMLString Dump() const override;

	private:
		TagWPtr mEnterPoint;
		//CMenu& mMenu;
	};

	struct CEventFactory
	{
		static ItemEventPtr CreateMenuEvent(
			const VXMLString& aAttrId,
			const MenuPtr& aMenu,
			const ScopeServicePtr& aScope,
			E_EVENT_TYPE aType)
		{
			if (!aMenu || !aMenu->IsEnabled())
			{
				throw std::string("Cannot find menu with id: ") + wtos(aAttrId);
			}

			auto newEvent = std::make_shared<CMenuEvent>();
			newEvent->mAttrId = aAttrId;
			newEvent->mMenu = aMenu;
			newEvent->mCurTag = aMenu->BeginTagPtr;
			newEvent->mScopeStack = aScope;
			newEvent->mType = aType;
			newEvent->mName = aMenu->InputEvent;

			return newEvent;
		}

		static ItemEventPtr CreateDocEvent(
			//const VXMLString& aAttrId,
			const MenuPtr& aMenu,
			const VXMLString& aUri,
			const TagWPtr& aEnterPoint,
			const ScopeServicePtr& aScope,
			E_EVENT_TYPE aType)
		{
			auto newEvent = std::make_shared<CDocEvent>();
			newEvent->mAttrId = aMenu ? aMenu->MenuName: L"";
			//newEvent->mEnterPoint = aEnterPoint;
			newEvent->mRootTag = aEnterPoint;
			newEvent->mCurTag = aEnterPoint;
			newEvent->mScopeStack = aScope;
			newEvent->mType = aType;
			newEvent->mUri = aUri;
			newEvent->mMenu = aMenu;

			return newEvent;
		}

		static ItemEventPtr CreateTagEvent(
			const VXMLString& aAttrId,
			const MenuPtr& aMenu,
			const TagWPtr& aEnterPoint,
			const ScopeServicePtr& aScope,
			E_EVENT_TYPE aType)
		{
			if (!aMenu || !aMenu->IsEnabled())
			{
				throw std::string("Cannot find menu with id: ") + wtos(aAttrId);
			}

			auto newEvent = std::make_shared<CTagEvent>();
			newEvent->mAttrId = aAttrId;
			newEvent->mEnterPoint = aEnterPoint;
			newEvent->mCurTag = aEnterPoint;
			newEvent->mScopeStack = aScope;
			newEvent->mType = aType;
			newEvent->mMenu = aMenu;
			newEvent->mName = aMenu->InputEvent;

			return newEvent;
		}


	};
}
