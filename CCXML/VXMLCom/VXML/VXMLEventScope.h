/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLEventScope.h                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 22 May 2019                                               */
/************************************************************************/
#pragma once

//#include "VXMLBase.h"
#include <string>
#include <memory>
#include <list>
#include <algorithm>
#include <atlstr.h>

namespace VXML
{
	class CTag;
	struct CExecContext;

	class CEvtScope
	{
	public:
		CEvtScope() :m_iCount(0), m_pExecTag(nullptr) {}
		CEvtScope(const std::wstring& _name,
			const size_t& _count,
			const std::wstring& _cond,
			CTag* const _tag,
			const std::wstring& _url,
			const std::wstring& _eventVariableName)
			: m_sName(_name)
			, m_iCount(_count)
			, m_sCond(_cond)
			, m_pExecTag(_tag)
			, m_sUrl(_url)
			, m_sEventVairiableName(_eventVariableName)
		{}
	public:
		std::wstring GetEventName()const { return m_sName; }
		size_t GetCount()const { return m_iCount; }
		const std::wstring& GetCond() const { return m_sCond; }
		bool ExecCond(const CExecContext* pCtx) const;
		std::wstring GetDocUri()const { return m_sUrl; }
		std::weak_ptr<CTag> GetExecTag()const;
		std::wstring GetEventVairiableName() const { return m_sEventVairiableName; }

		CTag* GetTag()const { return m_pExecTag; }//Test123 delete this
	private:
		std::wstring m_sName;  // event name
		size_t     m_iCount; // existing counter
		std::wstring m_sCond;  // executing condition
		std::wstring m_sUrl;   // document name
		std::wstring m_sEventVairiableName; // variable name
		CTag*      m_pExecTag;
	};

	class CEvtScopeCollector
	{
	public:
		using EventScope = std::list <CEvtScope>;
		using ScopeStack = std::list < EventScope >;

		//ToDo: using EventScope = std::map <VXMLString, CEvtScope>;
		//  VXMLString - uri document
		//  CEvtScope - scope

	private:
		EventScope m_scopes;
		EventScope m_prompts;

		ScopeStack m_stack;
	public:
		void AddScope(const CEvtScope& _scope)
		{
			m_scopes.emplace_back(_scope);
		}

		bool CheckPromptExist(size_t count)
		{
			if (m_prompts.empty())
			{
				return false;
			}

			auto cit = std::find_if(m_prompts.cbegin(), m_prompts.cend(), [count](const CEvtScope& aEvtScope)
			{
				return count == aEvtScope.GetCount() && aEvtScope.GetCond().empty();
			});

			return cit != m_prompts.cend();
		}

		void AddPrompt(const CEvtScope& _prompt)
		{
			m_prompts.emplace_back(_prompt);
		}

		EventScope ExtractCurrentScopes();

		EventScope ExtractCurrentPrompts()
		{
			EventScope resultPrompts;

			resultPrompts.swap(m_prompts); //clear m_prompts
			return resultPrompts;
		}

		void RemoveUnnecessaryScopes(const std::wstring& _documentURI)
		{
			m_scopes.erase(std::remove_if(m_scopes.begin(), m_scopes.end(), [_documentURI](const CEvtScope& _evt)
			{
				return !CAtlString(_evt.GetDocUri().c_str()).CompareNoCase(_documentURI.c_str());
			}
			), m_scopes.end());
		}

		void ClearAll()
		{
			m_scopes.clear();
			m_prompts.clear();
		}

		void PushScope()
		{
			m_stack.push_back(m_prompts);
		}

		void PopScope()
		{
			if (m_stack.size())
			{
				m_scopes = m_stack.back();
				m_stack.pop_back();
			}
		}
	};
}

/******************************* eof *************************************/