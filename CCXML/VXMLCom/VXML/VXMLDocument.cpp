
#include "..\StdAfx.h"
#include "VXMLDocument.h"
#include "sv_strutils.h"
#include "sv_utils.h"

//extern HINSTANCE g_hInst;

namespace VXML
{
	// CDocument implementation

	CDocument::CDocument(const Factory_c::CollectorPtr& _parser, CEvtScopeCollector& _scope)
		: m_pRootTag(std::make_shared<CTag_vxml>(*this, _parser, nullptr, _scope))
	{
	}

	CDocument::~CDocument()
	{
		int test = 0;
	}

	void CDocument::Execute(CExecContext* pCtx)
	{
		std::shared_ptr<CTag> pDoc(m_pRootTag);
		TagWPtr newTag;
		m_pRootTag->ExecuteWithLog(pCtx, m_pRootTag, newTag);
	}

	MenuPtr CDocument::CreateNextMenu(const VXMLString& aMenuName, const VXMLString& aFieldName) const
	{
		MenuPtr menu = _getMenu(aMenuName, aFieldName);
		if (!menu)
		{
			return nullptr;
		}
		return menu->Clone();
	}

	MenuPtr CDocument::CreateNextMenu() const
	{
		if (!mFirstMenu)
		{
			return nullptr;
		}
		return mFirstMenu->Clone();
	}

	void CDocument::EmplaceMenuTag(const DoubleStringKey& aDoubleStringKey, TagWPtr aTagWPtr)
	{
		if (!mAllTags.emplace(aDoubleStringKey, std::make_shared<CMenu>( aDoubleStringKey.MenuId, aDoubleStringKey.FormId, aTagWPtr )).second)
		{
			throw std::logic_error(std::string("Menu with the same name (")
				+ wtos(aDoubleStringKey.MenuId) + ", "
				+ wtos(aDoubleStringKey.FormId) + ") has alredy exist");
		}
		else
		{
			if (!mFirstMenu && aDoubleStringKey.FormId.empty()) // field cannot be first menu
			{
				mFirstMenu = std::make_shared<CMenu>( aDoubleStringKey.MenuId, aDoubleStringKey.FormId, aTagWPtr );
			}
		}
	}

	MenuPtr CDocument::_getMenu(const VXMLString& aMenuName, const VXMLString& aFieldName) const
	{
		auto cit = mAllTags.find({ aMenuName, aFieldName });
		if (cit == mAllTags.cend())
		{
			return nullptr;
		}
		return cit->second;
	}

	//TagWPtr CDocument::GetMenuTag(const VXMLString& aMenuName, const VXMLString& aFieldName)
	//{
	//	TagWPtr result;

	//	MenuPtr menu = _getMenu(aMenuName, aFieldName);
	//	if (menu && menu->IsEnabled())
	//	{
	//		result = menu->BeginTagPtr;
	//	}

	//	return result;
	//}

	//MenuPtr CDocument::GetMenu(const VXMLString& aMenuName, const VXMLString& aFieldName)
	//{
	//	return _getMenu(aMenuName, aFieldName);
	//}

	//MenuPtr CDocument::GetFirstMenu() const
	//{
	//	return mFirstMenu;
	//}
}