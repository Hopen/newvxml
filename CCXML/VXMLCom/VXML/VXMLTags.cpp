/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLTags.cpp                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/
#include "..\StdAfx.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/format.hpp>
#include  <io.h>
#include "IdGenerator.h"
#include "sv_strutils.h"
#include "HashMap.h"
//#include "..\Engine\EngineLog.h"
#include "VXMLTags.h"
#include "VXMLSession.h"
#include "VXMLEngine.h"
#include "VXMLBSClient.h"
#include "VXMLDocument.h"
#include "..\Common\ExtraLog.h"
#include "..\Common\SystemLogEx.h"

#include <boost/property_tree/xml_parser.hpp>

#import "..\..\Bin/VXMLPassStorage.dll" no_namespace named_guids

//using namespace enginelog;

#define CLEARDIGITS CMessage msg_cleardigits(L"CLEAR_DIGITS"); \
CMessage wait_cleardigits; \
msg_cleardigits[L"DestinationAddress"] = pSes->GetParentCallID(); \
if (pSes->WaitEvent(&msg_cleardigits, L"clear_digits.completed", L"error", wait_cleardigits)!= WE_SUCCESS) { pSes->DoExit(L""); return false; }

#define STOPCHANNEL CMessage msg_stopchannel(L"STOP_CHANNEL"); \
CMessage wait_stopchannel; \
msg_stopchannel[L"DestinationAddress"] = pSes->GetParentCallID(); \
pSes->SendEvent(msg_stopchannel, pSes->GetParentScriptID());

#define INCNOINPUT 	int count = pCtx->pVbs->ExprEvalToInt(L"noinput_count")+1; \
pCtx->pVbs->ExecStatement(L"noinput_count = \"" + Utils::toStr(count) + L"\"");

#define CLEARCOUNTERS pCtx->pVbs->ExecStatement(L"noinput_count = \"0\""); \
	pCtx->pLog->LogFinest(L"CLEARCOUNTERS"); \
	pCtx->pVbs->ExecStatement(L"nomatch_count = \"0\""); 

#include "EngineConfig.h"

namespace VXML
{
	enum class E_URITYPE
	{
		UNKNOWN = 0,
		MENU,
		FILE,
		TEXT,
		URL
	};

	const int TRANSFERAUDIOREPEATE = 99;



	//CEvtScopeCollector::EventScope CEvtScopeCollector::ExtractCurrentScopes()
	//{
	//	EventScope resultScope;
	//	// set own scope for each menu
	//	if (m_scopes.size()>1)
	//	{
	//		EventScope::reverse_iterator cit = m_scopes.rbegin();
	//		EventScope::reverse_iterator dit = m_scopes.rend();
	//		while (cit != m_scopes.rend())
	//		{
	//			if (cit->GetEventName().empty())
	//			{
	//				// remember position of flag
	//				if (dit == m_scopes.rend())
	//					dit = cit;
	//				++cit;
	//				continue;
	//			}

	//			resultScope.push_back(*cit);
	//			++cit;
	//		}
	//		if (dit != m_scopes.rend())
	//		{
	//			EventScope copy(++dit, m_scopes.rend());
	//			copy.reverse();
	//			m_scopes.swap(copy);
	//		}
	//	}
	//	else
	//		m_scopes.clear();

	//	return resultScope;
	//}

	static E_URITYPE TranslateURI(
		const VXMLString& _root_folder, 
		const VXMLString& _curdoc, 
		const VXMLString& _uri, 
		VXMLString& _file, 
		VXMLString& _menu)
	{
		if (_uri.empty())
		{
			return E_URITYPE::UNKNOWN;
		}

		E_URITYPE uri_type = E_URITYPE::MENU;

		bool bFile = true;
		bool bRootFolder = false;
		if (_uri[0] == '/')
			bRootFolder = true;

		for (unsigned int i = (bRootFolder || _uri[0] == '\\') ? 1 : 0; i<_uri.length(); ++i)
		{
			if (_uri[i] == '#')
			{
				bFile = false;
				continue;
			}
			if (bFile)
			{
				_file += _uri[i];
			}
			else
			{
				_menu += _uri[i];
			}
		}
		// for URL
		if (_file.find(L"http://") == 0)
		{
			return uri_type;
		}

		WCHAR fileExt[MAX_PATH]; 
		ZeroMemory(fileExt, sizeof(fileExt));
		int pos = -1;

		WIN32_FIND_DATA fileData;

		if (!Utils::GetFile(_file, &fileData))
		{
			return E_URITYPE::FILE;
		}
		else if (!Utils::GetFile(_file + L".*", &fileData))
		{
			std::wstring filename = fileData.cFileName;

			if ((pos = filename.rfind('.')) != -1)
			{
				filename._Copy_s(fileExt, MAX_PATH, filename.length() - pos, pos);

				if (!wcscmp(fileExt, L".vxml") ||
					!wcscmp(fileExt, L".xml"))
				{
					_file += fileExt;
					return E_URITYPE::FILE;
				}
			}
		}

		// URI is relative 
		if (!_file.empty())
		{
			uri_type = E_URITYPE::FILE;
			WCHAR dir[MAX_PATH]; ZeroMemory(dir, sizeof(dir));
			WCHAR ExtDir[MAX_PATH]; ZeroMemory(ExtDir, sizeof(ExtDir));
			if ((pos = _file.rfind('\\')) != -1)
			{
				_file._Copy_s(ExtDir, MAX_PATH, pos + 1); // include "\"
				_file.erase(0, pos + 1);
			}
			if (bRootFolder)
			{
				wcscpy_s(dir, _root_folder.c_str());
			}
			else if ((pos = _curdoc.rfind('\\')) != -1)
			{
				_curdoc._Copy_s(dir, MAX_PATH, pos + 1); // include "\"
			}
			VXMLString mask = dir + VXMLString(ExtDir) + _file + L".*";
			if (!Utils::GetFile(mask, &fileData))
			{
				WCHAR fileExt[MAX_PATH]; ZeroMemory(fileExt, sizeof(fileExt));
				WCHAR fileName[MAX_PATH]; ZeroMemory(fileName, sizeof(fileName));
				std::wstring filename = fileData.cFileName;
				if ((pos = filename.rfind('.')) != -1)
				{
					filename._Copy_s(fileExt, MAX_PATH, filename.length() - pos, pos);
					filename._Copy_s(fileName, MAX_PATH, pos);
				}
				if (!_file.compare(fileName) &&
					(!wcscmp(fileExt, L".vxml") ||
					!wcscmp(fileExt, L".xml")))
					_file = dir + VXMLString(ExtDir) + _file + fileExt;
			}

		}

		return uri_type;
	}

	static TagWPtr LoadNewMenu(
		CExecContext* pCtx, 
		const VXMLString& aMenuName,
		const VXMLString& sFormName)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		pCtx->pLog->LogInfo(L"Going to Next Menu - <%s>",
			aMenuName.c_str()
			);

		//initialize new scope
		ScopeServicePtr scopeStack = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());
		if (!pSes->IsRecursiveEvent())
		{
			scopeStack->PopScope(); // delete catch, filled, etc scope
		}

		MenuPtr newMenu;
		if (!sFormName.empty()) // keep form scope for next field, subdialog, transfer or record
		{
			// initialize new form field 
			newMenu = pSes->GetMenu(aMenuName, sFormName);
		}
		else
		{
			//initialize new menu scope
			scopeStack->PopScope();
			scopeStack->PushScope(std::make_shared<CNameTable>(L"new_menu_scope"));
			newMenu = pSes->GetMenu(aMenuName, L"");
		}

		if (!newMenu || !newMenu->BeginTagPtr.lock().get())
		{
			VXMLString error = (boost::wformat(L"There are no menu with id = \"%s\" in current document: \"%s\"") % aMenuName % pSes->GetCurDocUri()).str();

			pCtx->pLog->LogWarning(error.c_str());
			pCtx->pXLog->LogWarning(error.c_str());

			CTag::EmitErrorBadfetch(
				pCtx,
				BadfetchErrorsToString(BadfetchErrors::BE_MENU_NOT_FOUND),
				L"",
				error);

			return NULLTAG;
		}
		else
		{
			auto pParentTag = dynamic_cast<CParentTag*>(newMenu->BeginTagPtr.lock().get());
			if (!pParentTag)
			{
				throw std::runtime_error("Cannot make dynamic_cast from menu or field");
			}
			pParentTag->Init();
		}

		//msg_next_menu[L"id"] = aMenuName.c_str();

		pSes->EmitRecursiveMenuEvent(
			aMenuName,
			newMenu,
			scopeStack);

		return newMenu->BeginTagPtr;
	}

	static TagWPtr LoadNewDocument(CExecContext* pCtx, const VXMLString& aFileName, const VXMLString& aMenuName)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		TagWPtr nextMenuTag;

		pCtx->pLog->LogInfo( L"Going to File [%s] menu <%s>",
			aFileName.c_str(),
			aMenuName.c_str()
			);

		if (aFileName.empty())
		{
			throw std::runtime_error("Empty document name");;
		}

		ScopeServicePtr scopeStack = pSes->CreateSessionScope();
		scopeStack->PushScope(std::make_shared<CNameTable>(L"document_scope"));

		DocumentPtr newDocument = pSes->LoadDocument(aFileName, true);

		if (!newDocument)
		{
			VXMLString error = format_wstring(
				L"Failed to load document: %s", 
				std::wstring(CAtlString(aFileName.c_str()).Left(256)).c_str());

			CTag::EmitErrorBadfetch(
				pCtx,
				BadfetchErrorsToString(BadfetchErrors::BE_LOAD_DOCUMENT),
				L"",
				error);

			return NULLTAG;
		}

		MenuPtr newMenu;
		if (!aMenuName.empty())
		{
			newMenu = newDocument->CreateNextMenu(aMenuName, L"");
		}
		else
		{
			newMenu = newDocument->CreateNextMenu();
		}

		if (!newMenu || !newMenu->IsEnabled())
		{
			VXMLString error = (boost::wformat(
				L"There are no menu with id = \"%s\" in current document: \"%s\"") % aMenuName % pSes->GetCurDocUri()).str();

			CTag::EmitErrorBadfetch(
				pCtx,
				BadfetchErrorsToString(BadfetchErrors::BE_MENU_NOT_FOUND),
				L"",
				error);

			return NULLTAG;
		}

		TagWPtr pEventEnterPoint = NULLTAG;
		if (pSes->PrepareDoc(newDocument, aFileName))
		{
			pEventEnterPoint = pSes->GetDocumentEnterPoint();
		}
		else
		{
			VXMLString sError = format_wstring(L"Failed to load document: %s", std::wstring(CAtlString(aFileName.c_str()).Left(256)).c_str());
			pCtx->pLog->LogWarning(L"Error: %s", sError.c_str());

			//pSes->DoDocumentFailed(aFileName, sError);
			return NULLTAG;
		}

		pSes->EmitRecursiveDocEvent(
			//aMenuName,
			newMenu,
			aFileName,
			pEventEnterPoint,
			scopeStack);

		return nextMenuTag;
	}

	static TagWPtr LoadNewUri(CExecContext* pCtx, const VXMLString aUri)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		TagWPtr nextMenuTag;

		pCtx->pLog->LogInfo( L"Going to URL [%s]",
			aUri.c_str()
			);
		//msg_next_menu.SetName(L"LoadNewDocument");
		//msg_next_menu[L"url"] = sNext.c_str();
		//msg_next_menu[L"FileText"] = file.c_str();

		// ToDo - load text here and get menu id
		throw std::logic_error("Unsupported <goto> format");

		return nextMenuTag;
	}

	static bool MakeNextMenu(
		CExecContext* pCtx, 
		const VXMLString& aNext, 
		const VXMLString& aFormName, 
		TagWPtr& aNextTag)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		VXMLString file,
			menu;

		pCtx->pLog->LogInfo( L"Translate URI: RootFolder - <%s>, CurDoc - <%s>, URI - <%s>",
			pSes->GetFileFolder().c_str(),
			pSes->GetCurDocUri().c_str(),
			aNext.c_str()
			);
		//CMessage msg_next_menu;

		E_URITYPE url_type = E_URITYPE::UNKNOWN;
		try
		{
			url_type = TranslateURI(pSes->GetFileFolder(), pSes->GetCurDocUri(), aNext, file, menu);
		}
		catch (std::exception& e)
		{
			pCtx->pLog->LogWarning(L"Exception while translating URI [%s]: \"\"",
				aNext.c_str(),
				_bstr_t(e.what())
				);
			url_type = E_URITYPE::UNKNOWN;
		}
		catch (...)
		{
			pCtx->pLog->LogWarning(L"Unknown exception while translating URI [%s]",
				aNext.c_str()
				);

			url_type = E_URITYPE::UNKNOWN;
		}

		ScopeServicePtr scopeStack;
		switch (url_type)
		{
		case E_URITYPE::MENU:
		{
			aNextTag = LoadNewMenu(pCtx, menu, aFormName);
			break;
		}
		case E_URITYPE::FILE:
		{
			aNextTag = LoadNewDocument(pCtx, file, menu);
			break;
		}
		case E_URITYPE::URL:
		{
			aNextTag = LoadNewUri(pCtx, aNext);
			break;
		}
		case E_URITYPE::UNKNOWN:
		default:
		{
			VXMLString error = format_wstring(
				L"Failed to load: %s",
				std::wstring(CAtlString(aNext.c_str()).Left(256)).c_str());

			CTag::EmitErrorBadfetch(
				pCtx,
				BadfetchErrorsToString(BadfetchErrors::BE_LOAD_DOCUMENT),
				L"",
				error);

			return false;
		}
		}

		return !aNextTag.expired();
	}


	static bool IsCatchEvent(const VXMLString& _name)
	{
		if (_name == L"nomatch" || 
			_name == L"filled" ||
			_name == L"noinput" || 
			_name == L"error"   || 
			_name == L"help"    ||
			_name == L"catch"  	  ) 
			return true;
		return false;
	}

	static bool IsCatchEvent(const TagWPtr& _tag)
	{
		if (_tag.lock() && IsCatchEvent(_tag.lock()->GetName()))
			return true;
		return false;
	}

	bool CEvtScope::ExecCond(const CExecContext* pCtx) const
	{
		if(m_sCond.empty())
			return true;
		return pCtx->pVbs->ExprEvalToBool(m_sCond);
	}

	// CTag implementation
	CComVariant CTag::EvalAttrEvent(CExecContext* pCtx, 
		const VXMLString& sAttrName, 
		VARTYPE evalType)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		VXMLString sAttrVal = sAttrName;
		if (GetAttrVal(sAttrName, sAttrVal))
		{
			return pCtx->pVbs->ExprEvalToType(sAttrVal, evalType);
		}
		CComVariant vExpr;
		return vExpr;
	}

	static int total_tags = 0;

	/****************************************************************************/
	/*                                                                          */
	/*                              CTAG                                        */
	/*                                                                          */
	/****************************************************************************/
	CTag::CTag(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope)
	{
			m_sName   = _parser->GetName();
			m_sText   = _parser->GetText();
			m_Attribs = _parser->GetAttr();
			m_iLine   = _parser->GetLine();
			m_sXML    = _parser->GetXML();
			m_sParams = _parser->GetParams();
			m_pParent = pParent;
			m_sUri    = _parser->GetUri();
	}

	CTag::~CTag()
	{
	}

	bool CTag::ExecuteWithLog(CExecContext * pCtx, const TagWPtr & pCurtag, TagWPtr & pNextTag)
	{
		_logTag(pCtx);

		bool res = Execute(pCtx, pCurtag, pNextTag);

		if (pNextTag.expired())
		{
			pNextTag = m_pParent->getNextTag(pCtx, this->shared_from_this());
		}

		return res;
	}

	void CTag::EmitErrorBadfetch(
		CExecContext* aCtx, 
		const VXMLString& aReason, 
		const VXMLString& aTagName,
		const VXMLString& aDescription)
	{
		SessionPtr2 pSes = aCtx->pSes.lock();

		pSes->DoErrorBadfetch(
			L"EXECTAGPROC",
			aReason,
			aTagName,
			aDescription);
	}

	void CTag::EmitErrorSemantic(CExecContext* pCtx, const VXMLString& reason, const VXMLString& description)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		pSes->DoErrorSemantic(
			L"EXECTAGPROC",
			reason,
			GetName(),
			description);
	}

	void CTag::EmitErrorUnrecoverable(
		CExecContext* pCtx, 
		const VXMLString& 
		reason, const VXMLString& description) const
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		pSes->DoErrorUnrecoverable(
			L"EXECTAGPROC",
			reason,
			GetName(),
			description);
	}

	bool CTag::CheckDigits(CExecContext* pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		//SessionObject pMenu = pSes->FindMenu(pSes->GetCurMenu());
		MenuPtr menu = pSes->GetCurMenu();

		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		bool makeGetDigits = false;
		bool bGetDigitsSent = menu->IsGetDigitsSent;
		//if (VAR * pGetDigitsSent = pMenu->Find(L"GetDigitsSent"))
		//{
		//	bGetDigitsSent = static_cast<bool>(!!pGetDigitsSent->vValue.boolVal);
		//}

		int iMaxDtmf = 0;
		if (!bGetDigitsSent)
		{
			if (Utils::toLower(menu->InputEvent) == L"noinput")
			{
				makeGetDigits = true;
			}
			else
			{
				VAR* pMaxDTMF = pVar->Find(L"maxdtmfcount");
				if (pMaxDTMF && pMaxDTMF->vValue.intVal)
				{
					iMaxDtmf = Utils::toNum<int>(_bstr_t(pMaxDTMF->vValue).GetBSTR());

					if (!menu->Utterance.empty())
					{
						menu->PreviousUtterance = menu->Utterance;
						iMaxDtmf -= menu->Utterance.length();
					}

					makeGetDigits = true;
				}

			}

		}

		//int iMaxDtmf = 0;
		//VAR * pCurEvent = pMenu->Find(L"event");
		//if (pCurEvent // eventExist (has got event)
		//	&& !bGetDigitsSent) // first check
		//{
		//	if (!CAtlString(_bstr_t(pCurEvent->vValue.bstrVal).GetBSTR()).CollateNoCase(L"noinput")) // got noinput event
		//	{
		//		makeGetDigits = true;
		//	}
		//	else
		//	{
		//		VAR* pMaxDTMF = pVar->Find(L"maxdtmfcount");
		//		if (pMaxDTMF && pMaxDTMF->vValue.intVal)
		//		{
		//			iMaxDtmf = Utils::toNum<int>(_bstr_t(pMaxDTMF->vValue).GetBSTR());

		//			if (VAR * pCurEvent = pMenu->Find(L"utterance"))
		//			{
		//				pMenu->Add(L"PreviousUtterance", pCurEvent->vValue.bstrVal, false);
		//				//iMaxDtmf -= pMaxDTMF->vValue.intVal;
		//				iMaxDtmf -= _bstr_t(pCurEvent->vValue.bstrVal).length();
		//			}
		//			
		//			makeGetDigits = true;
		//		}
		//	}
		//}

		if (makeGetDigits)
		{
			pSes->DoGetDigits(iMaxDtmf);
			menu->IsGetDigitsSent = true;
			//pMenu->Add(L"GetDigitsSent", true, false);

			return false;

		}

		return true;
	}


	//CMessage CTag::CreatePlayWavMessage(CExecContext* pCtx, const VXMLString& _uri)
	//{
	//	VXMLString sSpeaker, sLanguage;

	//	ScopeServicePtr pVar = pCtx->pVar.lock();
	//	if (!pVar)
	//	{
	//		throw std::range_error("Empty event scope");
	//	}

	//	VAR* pSpeaker = pVar->Find(L"Speaker");
	//	if (pSpeaker)
	//		sSpeaker = _bstr_t(pSpeaker->vValue);
	//	VAR* pLanguage = pVar->Find(L"Language");
	//	if (pLanguage)
	//		sLanguage = _bstr_t(pLanguage->vValue);

	//	CMessage msg(L"PLAY_WAV");
	//	VXMLString sTermChar = _bstr_t(pVar->Find(L"termchar")->vValue);

	//	LPCWSTR p = _uri.c_str(), q;
	//	VXMLString sParamName, sParamVal;
	//	int k = 0;

	//	SessionPtr2 pSes = pCtx->pSes.lock();
	//	while (*p)
	//	{
	//		q = wcschr(p, L';');
	//		if (!q)
	//		{
	//			sParamVal = trim_wstring(p);
	//		}
	//		else
	//		{
	//			sParamVal = trim_wstring(VXMLString(p, q - p));
	//		}
	//		if (!sParamVal.empty())
	//		{
	//			if (sParamVal[0] == '/' || sParamVal[0] == '\\')
	//				sParamVal.erase(0,1);
	//			sParamName = format_wstring(L"File%d", ++k);
	//			VXMLString sFolder = pSes->GetWavFolder();
	//			WCHAR szBuf[1024];
	//			PathCombineW(szBuf, szBuf, sFolder.c_str());
	//			if (!sSpeaker.empty())
	//				PathCombineW(szBuf, szBuf, sSpeaker.c_str());
	//			if (!sLanguage.empty())
	//				PathCombineW(szBuf, szBuf, sLanguage.c_str());
	//			PathCombineW(szBuf, szBuf, sParamVal.c_str());
	//			msg[sParamName.c_str()] = szBuf;
	//		}
	//		if (!q)
	//		{
	//			break;
	//		}
	//		p = q + 1;
	//	}

	//	msg[L"FilesCount"] = k;
	//	msg[L"DigitMask"] = sTermChar;

	//	return msg;
	//}

	//CMessage CTag::CreateMRCPRecognizeMessage(CExecContext * pCtx, bool bBargin, bool bIsModal)
	//{
	//	return CMessage();
	//}

	VXMLString CTag::onXMLStart()const
	{
		VXMLString out = L"<" + GetName() + GetParams() + L"/>";
		return out;
	}

	VXMLString CTag::onXMLEnd()const
	{
		return L"";
	}

	void CTag::serialize2(CExecContext* pCtx, VXMLString& _out)const
	{
		std::wstring out;
		out = this->onXMLStart();
		out += this->onXMLEnd();
		_out += out;
	}

	void CTag::_logTag(CExecContext* pCtx) const 
	{
		pCtx->pLog->LogInfo( L"Executing tag <%s>, line = \"%i\"", this->GetName().c_str(), this->GetLine());
	}

	/****************************************************************************/
	/*                                                                          */
	/*                              CPARENTTAG                                  */
	/*                                                                          */
	/****************************************************************************/

	CParentTag::CParentTag(
		CDocument& _doc, 
		const Factory_c::CollectorPtr& _parser, 
		CParentTag* pParent, 
		CEvtScopeCollector& _scope) 
		: CTag(_doc, _parser, pParent, _scope)
	{
		//default filter
		const auto& children = _parser->GetChildren();
		for (const auto& child : children)
		{
			VXMLString sName = child->GetName();
			if (sName == L"text" || sName == L"#comment")
			{
				continue;
			}

			VXMLString attrId;
			if (pParent)
			{
				if (sName == L"form" || sName == L"menu")
				{
					//attrId = pParent->GetAttrValSafe(L"id").get();
				}
				else if (sName == L"field"
					|| sName == L"transfer"
					|| sName == L"subdialog"
					|| sName == L"record")
				{
					attrId = pParent->GetAttrValSafe(L"id").get();
				}
			}

			if (sName == L"menu" ||
				sName == L"form" ||
				sName == L"field" ||
				sName == L"transfer" ||
				sName == L"record" ||
				sName == L"subdialog")
			{
				CEvtScope newScope;
				//_scope.push_back(newScope); // insert flag
				_scope.AddScope(newScope);
			}

			auto newChild = CTagFactory::CreateTag(sName, _doc, child, this, _scope);
			//newChild->AddScope(_scope);
			newChild->AddToDocument(attrId, _doc);

			m_Children.emplace_back(std::move(newChild));

			//if (sName != L"text" && sName != L"#comment")	// skip meta tags and comments
			//{
			//	if (sName == L"menu" ||
			//		sName == L"form" ||
			//		sName == L"field" ||
			//		sName == L"transfer" ||
			//		sName == L"record" ||
			//		sName == L"subdialog")
			//	{
			//		CEvtScope newScope;
			//		//_scope.push_back(newScope); // insert flag
			//		_scope.AddScope(newScope);
			//	}
			//	m_Children.emplace_back(CTagFactory::CreateTag(sName, _doc, child, this, _scope));
			//}

		}

		SetFilter([](TagPtr pTag) {return true; });

	}

	//TagWPtr CParentTag::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	//{
	//	pCtx->pLog->LogFinest( L"getNextTag");

	//	TagWPtr nextTag(NULLTAG);

	//	const CParentTag::TagContainer& _children_without_catches = GetChildren();
	//	if (!tag.lock().get()) //looking for first tag
	//	{
	//		if (_children_without_catches.size()) //looking for first execute tag
	//		{
	//			nextTag = *_children_without_catches.begin();
	//		}
	//	}
	//	else //looking for next tag
	//	{
	//		//const CTag* cthis = tag;
	//		const TagPtr cthis = tag.lock();

	//		auto cit = std::find_if(_children_without_catches.begin(), _children_without_catches.end(), [cthis](TagPtr child)
	//		{
	//			if (cthis.get() == child.get())
	//				return true;
	//			return false;
	//		}
	//		);

	//		// try to find next item
	//		if (cit != _children_without_catches.end())
	//		{
	//			auto nextItem = ++cit;
	//			if (nextItem != _children_without_catches.end())
	//			{
	//				nextTag = *nextItem;
	//			}
	//		}
	//	}

	//	SessionPtr2 pSes = pCtx->pSes.lock();
	//	if (nextTag.lock().get())
	//	{
	//		if (this->ifGotBreakingEvent(nextTag))
	//		{
	//			this->MakeBreakingEvent(pCtx, nextTag);
	//		}

	//		pCtx->pLog->LogFinest( L"Got Breaking Event");
	//		return nextTag;
	//	}

	//	m_endTag = true;
	//	nextTag = shared_from_this();

	//	return nextTag;
	//}

	TagWPtr CParentTag::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		//pCtx->pLog->LogFinest( L"getNextTag");

		TagWPtr nextTag(NULLTAG);

		if (!tag.expired())
		{
			nextTag = tag.lock()->_getNextTag();
		}
		else
		{
			const CParentTag::TagContainer& _children_without_catches = GetChildren();
			if (_children_without_catches.size()) //looking for first execute tag
			{
				nextTag = *_children_without_catches.begin();
			}
		}

		SessionPtr2 pSes = pCtx->pSes.lock();
		if (!nextTag.expired())
		{
			if (this->ifGotBreakingEvent(nextTag))
			{
				this->MakeBreakingEvent(pCtx, nextTag);
				pCtx->pLog->LogFinest( L"Got Breaking Event");
			}

			return nextTag;
		}

		m_endTag = true;
		nextTag = shared_from_this();

		return nextTag;
	}

	CParentTag::~CParentTag()
	{
	}


	bool CParentTag::onBeginTag(CExecContext* pCtx/*, TagWPtr& pNextTag*/)
	{
		return true;
	}

	bool CParentTag::onEndTag(CExecContext* pCtx/*, TagWPtr& pNextTag*/)
	{
		//SessionPtr2 pSes = pCtx->pSes.lock();
		//SessionObject pMenu = pSes->FindMenu(pSes->GetCurMenu());

		m_endTag = false;

		//VAR* pEndTag = pMenu->Find(L"EndMenuTag");
		//if (pEndTag)
		//{
		//	CComVariant v;
		//	v.vt = VT_BOOL;
		//	v.boolVal = VARIANT_FALSE;
		//	pEndTag->SetValue(v);
		//}
		//else
		//{
		//	pMenu->Add(L"EndMenuTag", FALSE, false);
		//}

		return true;
	}

	void CParentTag::serialize2(CExecContext * pCtx, VXMLString & aOut) const
	{
		std::wstring out;
		out = this->onXMLStart();
		std::wstring xml = this->GetXML();

		bool bTag = false;
		std::wstring sName, sText;

		if (m_Children.size())
		{
			TagContainer::const_iterator cit = m_Children.begin();

			for (unsigned int i = 0; i < xml.size(); ++i)
			{
				if (xml[i] == '<')
				{
					bTag = true;
					out += sText;
					sText.clear();
					continue;
				}
				if (xml[i] == '>')
				{
					(*cit)->serialize2(pCtx, out);
					++cit;
					bTag = false;
					continue;
				}

				if (!bTag)
					sText += xml[i];
			}

			if (cit != m_Children.end())
			{
				out += sText;
			}
		}
		else
		{
			out += xml;
		}

		out += this->onXMLEnd();
		aOut += out;
	}

	VXMLString CParentTag::onXMLStart()const
	{
		VXMLString out = L"<" + GetName() + GetParams() + L">";
		return out;
	}

	VXMLString CParentTag::onXMLEnd()const
	{
		VXMLString out = L"</" + GetName() + L">";
		return out;
	}

	bool CParentTag::Execute(CExecContext* pCtx, const TagWPtr& /*pCurTag*/, TagWPtr& pNextTag)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		if (!m_endTag)
		{
			if (!isInitialized(pCtx))
			{
				pNextTag = shared_from_this();//stay here
				return true;
			}

			if (onBeginTag(pCtx))
			{
				_logTag(pCtx);
				pNextTag = getNextTag(pCtx, NULLTAG);
			}
			else
			{
				pNextTag = m_pParent->getNextTag(pCtx, this->shared_from_this());
			}
		}
		else
		{
			if (!isFinitialized(pCtx))
			{
				pNextTag = shared_from_this();//stay here
				return true;
			}
			pCtx->pLog->LogInfo( L"Close </%s>", this->GetName().c_str());
			onEndTag(pCtx);
			
			if (!m_pParent) // close vxml tag
			{
				return false;
			}
			pNextTag = m_pParent->getNextTag(pCtx, this->shared_from_this());
		}

		return true;
	}

	bool CParentTag::ExecuteWithLog(CExecContext * pCtx, const TagWPtr & pCurtag, TagWPtr & pNextTag)
	{
		return Execute(pCtx, pCurtag, pNextTag);
	}

	// CTextTag implementation
	//const VXMLString CTextTag::GetText() const
	//{
	//	return m_sText;
	//}

	void CTextTag::serialize2(CExecContext* pCtx, VXMLString& _out)const
	{
		std::wstring out;
		out = this->onXMLStart();
		std::wstring text = this->GetText();
		out += text;
		out += this->onXMLEnd();

		_out += out;
	}

	VXMLString CTextTag::onXMLStart()const
	{
		VXMLString out = L"<" + GetName() + GetParams() + L">";
		return out;
	}

	VXMLString CTextTag::onXMLEnd()const
	{
		VXMLString out = L"</" + GetName() + L">";
		return out;
	}

	/****************************************************************************/
	/*                                                                          */
	/*                              CEXECUTABLETAG                              */
	/*                                                                          */
	/****************************************************************************/

	// CExecutableTag implementation
	CExecutableTag::CExecutableTag(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CParentTag(_doc, _parser, pParent, _scope) 
	{
		SetFilter([](TagPtr pTag)
		{
			return !IsCatchEvent(pTag->GetName());
		});

		m_EvtScope = _scope.ExtractCurrentScopes();
		m_Prompts = _scope.ExtractCurrentPrompts();

	}

	TagWPtr CExecutableTag::ExecEvents(
		const CExecContext* aCtx, 
		const CMessage& aEvent)
	{
		//test123
		aCtx->pLog->LogFinest( L"Dump all EventScopes");
		for (const auto& scope : m_EvtScope)
		{
			CTag* tag = scope.GetTag();
			if (!tag)
			{
				aCtx->pLog->LogFinest( L"NULL Tag EventScope");
			}
			else
			{
				aCtx->pLog->LogFinest( L"EventScope: name=\"%s\", uri=\"%s\", line=%i, xml=\"%s\"",
					scope.GetEventName().c_str(),
					scope.GetDocUri().c_str(),
					tag->GetLine(),
					tag->GetXML().c_str());

				//aCtx->pLog->LogFinest( L"EventScope:tag=\"0x%p\"",
				//	scope.GetExecTag().lock().get());
			}

			if (!scope.GetExecTag().lock().get())
			{
				aCtx->pLog->LogFinest( L"Cannot make shared_from_this()");
			}
		}

		VXMLString eventName(Utils::toLower<wchar_t>(aEvent.GetName()));
		if (eventName.empty())
		{
			throw std::logic_error("Empty event name");
		}

		size_t eventCount = 1;
		if (eventName == L"noinput" || eventName == L"nomatch")
		{
			eventCount = aCtx->pVbs->ExprEvalToInt(eventName + L"_count") + 1;
			aCtx->pLog->LogInfo( L"Increase event count: \"%s\", count: %d", eventName.c_str(), eventCount);
			if (!SUCCEEDED(aCtx->pVbs->ExecStatement(eventName + L"_count = \"" + Utils::toStr(eventCount) + L"\"")))
			{
				aCtx->pLog->LogFinest( L"ExecStatement increase event count failed: \"%s\", count: %d", eventName.c_str(), eventCount);
				return NULLTAG;
			}

			///**************** DO NOT DELETE - FUTURE VERSION *******************/
			//SessionPtr2 pSes = pCtx->pSes.lock();
			//SessionObject pMenu = pSes->FindMenu(GetAttrIdName());

			//VAR* pEventCount = pMenu->Find(sEventName + L"_count");
			//if (pEventCount)
			//{
			//	CComVariant v;
			//	v.vt = VT_INT;
			//	v.intVal = pEventCount->vValue.intVal + 1;
			//	pEventCount->SetValue(v);
			//}
			///**************** DO NOT DELETE *******************/

		}

		CEvtScopeCollector::EventScope resEvents;
		std::copy_if(m_EvtScope.cbegin(), m_EvtScope.cend(), std::back_inserter(resEvents), [&](const CEvtScope& aScope)
		{
			bool result = false;
			if (aScope.GetEventName() != eventName)
			{
				return result;	
			}

			auto eventVariableName = aScope.GetEventVairiableName();
			if (!eventVariableName.empty())
			{
				aCtx->pVbs->AddObject(eventVariableName, std::make_shared<CNameTable>(aEvent)->AsVariant());
			}

			result = aScope.ExecCond(aCtx);

			if (!eventVariableName.empty())
			{
				aCtx->pVbs->DelObject(eventVariableName);
			}

			return result;
		});


		// 2. Find the "correct count": the highest count among the catch elements still 
		//     on the list less than or equal to the current count value. 

		CEvtScopeCollector::EventScope::iterator rit = resEvents.end();

		for (CEvtScopeCollector::EventScope::iterator cit = resEvents.begin(); cit != resEvents.end(); ++cit)
		{
			size_t cur_count = cit->GetCount();
			if (cur_count == eventCount)
			{
				rit = cit;
				break;
			}
			if (cur_count < eventCount && (rit == resEvents.end() || cur_count > rit->GetCount()))
			{
				rit = cit;
			}
		}

		// 3. Select the first element in the list with the "correct count". 
		if (rit!=resEvents.end())
		{
			aCtx->pLog->LogInfo( L"Processing event: \"%s\", count: %d", eventName.c_str(), eventCount);
			return rit->GetExecTag();
		}
		else
		{
			aCtx->pLog->LogWarning(L"unexpected execute event: \"%s\"", eventName.c_str());
			return NULLTAG;
		}
		return NULLTAG;
	}

	bool CExecutableTag::MatchPrompt(VXML::TagPtr promptTag, VXML::CExecContext *pCtx) const
	{
		VXMLString sTagPromptCount;
		promptTag->GetAttrVal(L"count", sTagPromptCount);

		int tagCount = sTagPromptCount.empty() ? 1 : Utils::toNum<int>(sTagPromptCount);
		SessionPtr2 pSes = pCtx->pSes.lock();
		MenuPtr menu = pSes->GetCurMenu();

		if (/*pSes->IsRecursiveEvent()
			&& */menu->PromptCount == tagCount 
			&& !menu->NeedReprompt 
			&& menu->PromptLine != promptTag->GetLine())
		{
			throw std::runtime_error((boost::format("prompt [line == %i] got the same counter [%i] as previous ones") % promptTag->GetLine() % tagCount).str());
		}

		if (!menu->NeedReprompt)
		{
			return false;
		}

		VXMLString menuID = GetAttrIdName();
		

		//SessionObject pMenu = pSes->FindMenu(menuID);
		//{
		//	VAR* pReprompt = pMenu->Find(L"reprompt");
		//	if (pReprompt && pReprompt->vValue.boolVal == VARIANT_FALSE)
		//		return false;

		//	VAR* pPrompCount = pMenu->Find(L"prompt_count");
		//	if (pPrompCount)
		//		prompt_count = pPrompCount->vValue.intVal + 1;
		//}


		auto promptCount = menu->PromptCount + 1;
		auto curpromptIt = std::find_if(m_Prompts.cbegin(), m_Prompts.cend(), [tagCount](const CEvtScope& aScope)
		{
			return aScope.GetCount() == tagCount;
		});

		auto rit = m_Prompts.cend();
		for (auto cit = m_Prompts.cbegin(); cit != m_Prompts.cend(); ++cit)
		{
			size_t cur_count = cit->GetCount();
			if (cur_count == promptCount)
			{
				rit = cit;
				break;
			}
			if (cur_count < promptCount && (rit == m_Prompts.end() || cur_count > rit->GetCount()))
			{
				rit = cit;
			}
		}
		
		return (rit!=m_Prompts.end() && curpromptIt == rit);
	}

	// Tags implementation

	CTag_audio::~CTag_audio()
	{
		int test = 0;
	}

	bool CTag_audio::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		VXMLString sSrc;
		VXMLString sExpr;
		VXMLString sURI;
		if (GetAttrVal(L"src",sSrc))
		{
			sURI = sSrc;
		}
		else if (GetAttrVal(L"expr",sExpr))
		{
			sURI = pCtx->pVbs->ExprEvalToStr(sExpr);
		}

		if (sURI.empty())
			return false;

		/******** GET BARGEIN ***********/
		SessionPtr2 pSes = pCtx->pSes.lock();

		bool bFetch = false;
		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		MenuPtr menu = pSes->GetCurMenu();
		menu->Audiomode = L"playwav";

		//pVar->AddVar(L"audiomode", L"playwav", false);

		VAR* pVarField = pVar->Find(L"bargein");
		if (pVarField)
		{
			if (pVarField->vValue.vt == VT_BOOL)
				bFetch = static_cast<bool>(!!pVarField->vValue.boolVal);// 
			if (pVarField->vValue.vt == VT_BSTR)
				bFetch = !CAtlStringW(pVarField->vValue.bstrVal).CollateNoCase(L"true");
		}

		pVar->AddVar(L"bargein", bFetch, false);

		WaitingEvents extraEvents;
		VXMLString sExtra;
		if (GetAttrVal(L"extra", sExtra))
		{
			std::wistringstream in(sExtra);
			std::istream_iterator<VXMLString, wchar_t> first(in), last;
			std::copy(first, last, std::back_inserter(extraEvents));;
		}

		/************* CHECKING INPUTMODE *************/
		// only for prompts, menus and fields. No voice in catch events 
		//bool bVoiceInput = (pVar->Find(L"voicegrammar") != nullptr) ? true : false;
		//bool bDtmfInput = (pVar->Find(L"dtmfgrammar") != nullptr) ? true : false;

		bool bVoiceInput = false;
		bool bDtmfInput = false;

		bVoiceInput = menu->InputMode == L"voice";
		bDtmfInput = menu->InputMode == L"dtmf";

		//if (VAR* pInputMode = pVar->Find(L"inputmode"))
		//{
		//	bVoiceInput |= pInputMode->AsString() == L"voice";
		//	bDtmfInput |= pInputMode->AsString() == L"dtmf";
		//}

		pSes->DoPlayWav(sURI, extraEvents, bDtmfInput, bVoiceInput);

		return true;
	}

	bool CTag_block::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
	}

	CTag_catch::CTag_catch(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope): CParentTag(_doc, _parser, pParent, _scope)
	{ 
		try
		{
			VXMLString sAllEvents, sEvent, sCount, sCond, sEventVariableName;
			GetAttrVal(L"event", sAllEvents);
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);
			GetAttrVal(L"eventname", sEventVariableName);

			LPCWSTR p = sAllEvents.c_str(), q;

			while (*p)
			{
				q = wcschr(p, L' ');
				if (!q)
				{
					sEvent = trim_wstring(p);
				}
				else
				{
					sEvent = trim_wstring(VXMLString(p, q - p));
				}
				if (!sEvent.empty())
				{
					sEvent = Utils::toLower<wchar_t>(sEvent);

					CEvtScope pNew(
						sEvent, 
						sCount.empty() ? 1 : Utils::toNum<int>(sCount), 
						sCond, 
						this, 
						_parser->GetUri(),
						sEventVariableName);

					_scope.AddScope(pNew);
					//_scope.push_back(pNew);
				}
				if (!q)
				{
					break;
				}
				p = q + 1;
			}
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"catch\" tag");
		}
	}

	bool CTag_catch::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_choice::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	VXMLString CTag_choice::MatchDTMF(CExecContext* pCtx/*, VAR* pGrammar*/)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		VXMLString event(L"nomatch"); 

		VXMLString sExpr;
		VXMLString sDTMF;
		VXMLString sEvent;
		if (GetAttrVal(L"dtmf",sDTMF))
		{

		}
		else if (GetAttrVal(L"expr",sExpr))
		{
			sDTMF = pCtx->pVbs->ExprEvalToStr(sExpr);
		}

		if (GetAttrVal(L"event", sEvent))
		{
		}

		if (sDTMF.empty() && sEvent.empty())
		{
			throw std::range_error("Menu choice \"dtmf\" or \"event\" must be declared");
		}

		if (!sDTMF.empty() && !sEvent.empty())
		{
			throw std::range_error("Exactly one of \"dtmf\" or \"event\" must be declared");
		}

		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		//VAR * pCurDTMF = nullptr;
		MenuPtr menu = pSes->GetCurMenu();

		if (!menu->Utterance.empty())
		{
			VAR * pTermChar = pVar->Find(L"termchar");
			if ((sDTMF + VXMLString(_bstr_t(pTermChar->vValue))) == menu->Utterance)
				event = L"filled";

			if (sDTMF == menu->Utterance)
				event = L"filled";
		}
		else if (!sEvent.empty())
		{
			if (sEvent == menu->Utterance)
			{
				event = L"filled";
			}
		}
		return event;
	}

	bool CTag_choice::onBeginTag(CExecContext* pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		if (MatchDTMF(pCtx) == L"filled")
		{
			MenuPtr menu = pSes->GetCurMenu();
			menu->InputEvent = L"filled";
			menu->PromptCount = 0;
			menu->NeedReprompt = true;
			menu->PromptLine = 0;

			pCtx->pLog->LogInfo( L"Erasing prompt_count. Set reprompt...");


			//SessionObject pMenu = pSes->FindMenu(pSes->GetCurMenu());
			//pMenu->Add(L"event", L"filled", true);

			//if (VAR* pParam = pMenu->Find(L"prompt_count"))
			//{
			//	CComVariant v;
			//	v.vt = VT_INT;
			//	v.intVal = 0;
			//	pParam->SetValue(v);
			//	pCtx->pLog->LogFinest( L"Erasing prompt_count...");
			//}

			//VAR* pReprompt = pMenu->Find(L"reprompt");
			//if (pReprompt)
			//{
			//	CComVariant v;
			//	v.vt = VT_BOOL;
			//	v.boolVal = true;
			//	pReprompt->SetValue(v);
			//	pCtx->pLog->LogFinest( L"Set reprompt...");
			//}


			///***************** DO NOT DELETE - FUTURE VERSION ************************/
			//if (VAR* pParam = pMenu->Find(L"noinput_count"))
			//{
			//	CComVariant v;
			//	v.vt = VT_INT;
			//	v.intVal = 0;
			//	pParam->SetValue(v);
			//}

			//if (VAR* pParam = pMenu->Find(L"nomatch_count"))
			//{
			//	CComVariant v;
			//	v.vt = VT_INT;
			//	v.intVal = 0;
			//	pParam->SetValue(v);
			//}
			///***************** DO NOT DELETE ************************/
			CLEARCOUNTERS; // deprecated - delete this 


			return true;
		}

		return false;
	}

	bool CTag_choice::isInitialized(CExecContext* pCtx)
	{
		return CheckDigits(pCtx);

	}

	bool CTag_choice::onEndTag(CExecContext* pCtx/*, TagWPtr& pNextTag*/)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		__super::onEndTag(pCtx);

		// decide where to go next
		VXMLString sVal;
		VXMLString sNext;
		if (GetAttrVal(L"next", sVal))
		{
			sNext = sVal;
		}
		else if (GetAttrVal(L"expr", sVal))
		{
			sNext = pCtx->pVbs->ExprEvalToStr(sVal);
		}

		MenuPtr menu = pSes->GetCurMenu();
		if (!menu->Utterance.empty())
		{
			pSes->ProccedBufferComment(menu->MenuName, menu->Utterance);
		}

		TagWPtr nextMenu; //unused
		return MakeNextMenu(pCtx, sNext, L"", nextMenu);
	}

	bool CTag_else::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
		// Nothing to do - this tag is processed by <if>
		//return true;
	}

	bool CTag_elseif::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
		//// Nothing to do - this tag is processed by <if>
		//return true;
	}

	CTag_error::CTag_error(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope): CParentTag(_doc, _parser, pParent, _scope) 
	{ 
		try
		{
			VXMLString sName(L"error");
			VXMLString sCount, sCond, sEventVariableName;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);
			GetAttrVal(L"eventname", sEventVariableName);

			CEvtScope pNew(
				sName, 
				sCount.empty() ? 1 : Utils::toNum<int>(sCount), 
				sCond, 
				this, 
				_parser->GetUri(),
				sEventVariableName);

			_scope.AddScope(pNew);
			//_scope.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"error\" tag");
		}
	}

	bool CTag_error::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;

		//CScopeSaver saver(pCtx->pVar,L"error");
		//return __super::Execute(pCtx); // - action
	}

	bool CTag_exit::Execute(VXML::CExecContext* pCtx, const TagWPtr& /*pCurTag*/, TagWPtr& /*pNextTag*/)
	{
		VXMLString sReturnName;
		if (GetAttrVal(L"namelist",sReturnName))
		{
		//	sReturnValue = pCtx->pVbs->ExprEvalToStr(sReturnName);
		}
		SessionPtr2 pSes = pCtx->pSes.lock();
		pSes->DoExit(/*nCallID,*/sReturnName);

		return true;
	}

	CTag_field::CTag_field(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope)
		: CExecutableTag(_doc, _parser, pParent, _scope)
	{
	}

	bool CTag_field::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	VXMLString CTag_field::GetAttrIdName()const
	{
		return GetAttrVal(L"name");
	}

	bool CTag_field::isFinitialized(CExecContext * pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr menu = pSes->GetCurMenu();
		VXMLString sInputmode = menu->InputMode;
		if (sInputmode.empty())
		{
			sInputmode = L"dtmf"; // use default inputmode
		}

		//SessionObject pMenu = pSes->FindMenu(pSes->GetCurMenu());

		//// CHECKING INPUTMODE
		//VXMLString sUtterance, sInputmode;
		//if (VAR *value = pMenu->Find(L"inputmode"))
		//{
		//	sInputmode = _bstr_t(value->vValue.bstrVal);
		//}

		//if (sInputmode.empty()) // still nothing -> use default inputmode
		//{
		//	sInputmode = L"dtmf";
		//}

		//CHECK INPUT
		if (!sInputmode.compare(L"voice"))
		{
			//if (!pCurField->CheckVoiceInput(pCtx)) // ToDo: recognize!
			return true;
		}

		return this->CheckDigits(pCtx);
	}

	bool CTag_field::onBeginTag(CExecContext* pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		// initializing menu
		VXMLString field_name = GetAttrVal(L"name");

		if (field_name.empty())
		{
			//error.badfetch
			pCtx->pLog->LogWarning(L"CTag_field::onBeginTag: EmitError");
			EmitErrorBadfetch(
				pCtx, 
				BadfetchErrorsToString(BadfetchErrors::BE_SYNTAX),
				GetName(),
				L"The <field>-tag must have a name");
			return false;
		}

		if (!SUCCEEDED(pCtx->pVbs->ExecStatement(field_name + L"= \"\"")))
		{
			pCtx->pLog->LogWarning(L"ExecStatement: cannot clear field_name \"%s\"", field_name.c_str());
		}

		MenuPtr menu = pSes->GetCurMenu();
		menu->Utterance.clear();
		menu->InputEvent = L"noinput";
		menu->IsGetDigitsSent = false;
		//menu.PromptCount = 0;

		//SessionObject pMenu = pSes->FindMenu(field_name);
		//if (!pMenu)
		//{
		//	throw std::range_error("There are no current field");
		//}


		//pMenu->Add(L"utterance", L"", true);
		//pMenu->Add(L"event", L"noinput", true);
		//pMenu->Add(L"GetDigitsSent", false, true);

		//if (VAR * prompt_count = pMenu->Find(L"prompt_count"))
		//{

		//}
		//else
		//{
		//	pMenu->Add(L"prompt_count", 0, true);
		//}

		//pMenu->Add(L"prompt_count", 0, true);
		///***************** DO NOT DELETE - FUTURE VERSION ************************/
		////pMenu->Add(L"noinput_count", 0, true); for future version
		////pMenu->Add(L"nomatch_count", 0, true);
		///***************** DO NOT DELETE ************************/
		//CLEARCOUNTERS; // deprecated - delete this 

		return true;
	}

	bool CTag_field::onEndTag(CExecContext* pCtx)
	{
		__super::onEndTag(pCtx);

		SessionPtr2 pSes = pCtx->pSes.lock();
		MenuPtr menu = pSes->GetCurMenu();
		VXMLString sInputmode = menu->InputMode;
		if (sInputmode.empty())
		{
			sInputmode = L"dtmf";
		}

		VXMLString exist_event(L"filled");
		if (!menu->InputEvent.empty())
		{
			exist_event = menu->InputEvent;
		}

		if (0 == sInputmode.compare(L"voice"))
		{
			//the event has already defined in mrcp answer
			if (!menu->MrcpResult.empty())
			{
				exist_event = menu->MrcpResult;
				menu->MrcpResult.clear(); // need this ???
			}
		}
		if (0 == sInputmode.compare(L"dtmf"))
		{
			SessionGrammar pLocalGrammar;

			ScopeServicePtr pVar = pCtx->pVar.lock();
			if (!pVar)
			{
				throw std::range_error("Empty event scope");
			}

			if (VAR* pDtmfGrammar = pVar->Find(L"dtmfgrammar")) //looking for local grammar
			{
				VXMLString grammar_name = _bstr_t(pDtmfGrammar->vValue);
				pLocalGrammar = pSes->FindGrammar(grammar_name);
			}

			if (pLocalGrammar)
			{
				VXMLString grammar_event = pLocalGrammar->LookUp(menu->Utterance);
				if (!grammar_event.empty())
				{
					exist_event = grammar_event;
				}
			}

			pSes->ProccedBufferComment(menu->MenuName, menu->Utterance);
		}

		menu->InputEvent = exist_event;


		//VXMLString curMenuName = pSes->GetCurMenu();

		//SessionObject pMenu = pSes->FindMenu(curMenuName);

		//// CHECKING INPUTMODE
		//VXMLString sUtterance, sInputmode;
		//if (VAR *value = pMenu->Find(L"inputmode"))
		//{
		//	sInputmode = _bstr_t(value->vValue.bstrVal);
		//}

		//if (sInputmode.empty()) // still nothing -> use default inputmode
		//{
		//	sInputmode = L"dtmf";
		//}

		////CHECK INPUT
		//if (!sInputmode.compare(L"voice"))
		//{
		//}
		//if (!sInputmode.compare(L"dtmf"))
		//{
		//	//if (!this->CheckDigits(pCtx))
		//	//{
		//	//	return false;
		//	//}
		//}

		//// GET UTTERANCE
		//if (VAR *value = pMenu->Find(L"utterance"))
		//{
		//	sUtterance = _bstr_t(value->vValue.bstrVal);
		//}

		//// MATCH UTTERANCE
		//VXMLString exist_event(L"filled");
		//if (VAR *value = pMenu->Find(L"event"))
		//{
		//	exist_event = _bstr_t(value->vValue.bstrVal);
		//}

		//if (0 == sInputmode.compare(L"voice"))
		//{
		//	//the event has already defined in mrcp answer
		//	if (VAR *value = pMenu->Find(L"result"))
		//	{
		//		exist_event = _bstr_t(value->vValue.bstrVal);
		//		pMenu->Del(L"result");
		//	}
		//}
		//if (0 == sInputmode.compare(L"dtmf"))
		//{
		//	SessionGrammar pLocalGrammar;

		//	ScopeServicePtr pVar = pCtx->pVar.lock();
		//	if (!pVar)
		//	{
		//		throw std::range_error("Empty event scope");
		//	}

		//	if (VAR* pDtmfGrammar = pVar->Find(L"dtmfgrammar")) //looking for local grammar
		//	{
		//		VXMLString grammar_name = _bstr_t(pDtmfGrammar->vValue);
		//		pLocalGrammar = pSes->FindGrammar(grammar_name);
		//	}

		//	if (pLocalGrammar)
		//	{
		//		VXMLString grammar_event = pLocalGrammar->LookUp(sUtterance);
		//		if (!grammar_event.empty())
		//		{
		//			exist_event = grammar_event;
		//		}
		//	}

		//	pSes->ProccedBufferComment(curMenuName, sUtterance);
		//}

		//pMenu->Add(L"event", exist_event.c_str(), true);

		///***************** DO NOT DELETE - FUTURE VERSION ************************/
		//if (VAR* pParam = pMenu->Find(L"noinput_count"))
		//{
		//	CComVariant v;
		//	v.vt = VT_INT;
		//	v.intVal = 0;
		//	pParam->SetValue(v);
		//}

		//if (VAR* pParam = pMenu->Find(L"nomatch_count"))
		//{
		//	CComVariant v;
		//	v.vt = VT_INT;
		//	v.intVal = 0;
		//	pParam->SetValue(v);
		//}
		///***************** DO NOT DELETE ************************/
		//CLEARCOUNTERS; // deprecated - delete this 

		// send event
		CMessage msg(exist_event.c_str());

		ScopeServicePtr newScope = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());
		newScope->PushScope(std::make_shared<CNameTable>(L"field_scope"));
		
		TagWPtr nextMenuTag = ExecEvents(pCtx, msg);
		if (nextMenuTag.lock().get())
		{
			pSes->EmitSubEvent(menu->MenuName, menu, nextMenuTag, newScope);
		}

		return true;
	}

	TagWPtr CTag_field::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		if (IsCatchEvent(tag))
		{
			return NULLTAG;
		}

		return __super::getNextTag(pCtx, tag);
	}

	void CTag_field::AddToDocument(const VXMLString& aParentAttrId, CDocument& _doc)
	{
		_doc.EmplaceMenuTag(DoubleStringKey{ this->GetAttrIdName(), aParentAttrId }, shared_from_this());
	}

	CTag_filled::CTag_filled(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CParentTag(_doc, _parser, pParent, _scope)
	{
		try
		{
			VXMLString sName(L"filled");

			CEvtScope pNew(
				sName, 
				/*sCount.empty() ? 1 : Utils::toNum<int>(sCount)*/1, 
				/*sCond*/L"", 
				this, _parser->GetUri(),
				L"");
			_scope.AddScope(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"noinput\" tag");
		}
	}


	bool CTag_filled::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	//VXMLString CTag_filled::Match(CExecContext* pCtx, VAR* pGrammar)
	//{
	//	VXMLString event(L"filled"); //event.clear();
	//	if (!pGrammar)
	//		return event;
	//	// find active grammar
	//	VXMLString grammar_name = _bstr_t(pGrammar->vValue);
	//	SessionPtr2 pSes = pCtx->pSes.lock();
	//	SessionGrammar pG = pSes->FindGrammar(grammar_name);
	//	if (!pG)
	//		return event;

	//	// check input mode
	//	ScopeServicePtr pVar = pCtx->pVar.lock();
	//	if (!pVar)
	//	{
	//		throw std::range_error("Empty event scope");
	//	}

	//	VAR* pInputMode = pVar->Find(L"inputmode");

	//	if (pInputMode)
	//	{
	//		if (_bstr_t(pInputMode->vValue) == _bstr_t(L"voice"))
	//		{
	//			// ToDo: !!!

	//			return event;
	//		}

	//		if (_bstr_t(pInputMode->vValue) == _bstr_t(L"dtmf"))
	//		{
	//			// check input value
	//			event.clear();
	//			VAR* pCurDTMF = pVar->Find(L"utterance");
	//			if (!pCurDTMF)
	//				return event;

	//			// matching...
	//			event = pG->LookUp(VXMLString(_bstr_t(pCurDTMF->vValue)).c_str());
	//			return event;
	//		}
	//	}

	//	return event;
	//}

	CTag_form::CTag_form(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope)
		: CExecutableTag(_doc, _parser, this, _scope)
	{
	}

	bool CTag_form::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_form::onBeginTag(CExecContext* pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		VXMLString formID = GetAttrVal(L"id");
		VXMLString sComment;
		if (GetAttrVal(L"comment", sComment))
		{
			pSes->ProccedFormComment(formID, sComment);
		}

		return true;
	}

	TagWPtr CTag_form::getNextTag(CExecContext * pCtx, const TagWPtr & tag) const
	{
		if (ifGotBreakingEvent(tag))
		{
			return NULLTAG;
		}

		return __super::getNextTag(pCtx, tag);
	}

	bool CTag_form::ifGotBreakingEvent(const TagWPtr & currentTag) const 
	{
		if (currentTag.lock() &&
			   (currentTag.lock()->GetName() == L"field"     ||
				currentTag.lock()->GetName() == L"subdialog" ||
				currentTag.lock()->GetName() == L"transfer"  ||
				currentTag.lock()->GetName() == L"record"
				))
			return true;

		return false;
	}

	bool CTag_form::MakeBreakingEvent(CExecContext* pCtx, const TagWPtr& pTag)const
	{
		if (pTag.lock())
		{
			SessionPtr2 pSes = pCtx->pSes.lock();

			VXMLString formName = this->GetAttrIdName();
			VXMLString fieldName = pTag.lock()->GetAttrIdName();

			ScopeServicePtr newScope = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());

			MenuPtr menu = pSes->GetMenu(fieldName, formName);
			//menu.FormId = formName;
			pSes->EmitRecursiveMenuEvent(fieldName, menu, newScope);

			return true;
		}
		return false;
	}

	VXMLString CTag_form::GetAttrIdName()const
	{
		return GetAttrVal(L"id");
	}

	//void CTag_form::AddScope(CEvtScopeCollector& _scope)
	//{
	//	CEvtScope newScope;
	//	_scope.AddScope(newScope);
	//}
	void CTag_form::AddToDocument(const VXMLString& aParentAttrId, CDocument& _doc)
	{
		_doc.EmplaceMenuTag(DoubleStringKey{ this->GetAttrIdName(), L"" }, shared_from_this());
	}

	bool CTag_goto::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		pCtx->pLog->LogFinest( L"CTag_goto::Execute: erase counters");

		MenuPtr menu = pSes->GetCurMenu();
		menu->PromptCount = 0; //need this ???
		menu->NeedReprompt = true; // need this ???
		menu->PromptLine = 0;

		//SessionObject pMenu = pSes->FindMenu(pSes->GetCurMenu());
		//if (VAR* pParam = pMenu->Find(L"prompt_count"))
		//{
		//	CComVariant v;
		//	v.vt = VT_INT;
		//	v.intVal = 0;
		//	pParam->SetValue(v);
		//	pCtx->pLog->LogFinest( L"Erasing prompt_count...");
		//}

		//if (VAR* pReprompt = pMenu->Find(L"reprompt"))
		//{
		//	CComVariant v;
		//	v.vt = VT_BOOL;
		//	v.boolVal = VARIANT_TRUE;
		//	pReprompt->SetValue(v);
		//	pCtx->pLog->LogFinest( L"Set reprompt...");
		//}


		///// ??? Do this????
		/////***************** DO NOT DELETE - FUTURE VERSION ************************/
		////if (VAR* pParam = pMenu->Find(L"noinput_count"))
		////{
		////	CComVariant v;
		////	v.vt = VT_INT;
		////	v.intVal = 0;
		////	pParam->SetValue(v);
		////}

		////if (VAR* pParam = pMenu->Find(L"nomatch_count"))
		////{
		////	CComVariant v;
		////	v.vt = VT_INT;
		////	v.intVal = 0;
		////	pParam->SetValue(v);
		////}
		/////***************** DO NOT DELETE ************************/


		VXMLString sVal;
		VXMLString sNext;
		pCtx->pLog->LogFinest( L"CTag_goto::Execute: GetAttrVal next");
		if (GetAttrVal(L"next", sVal))
		{
			sNext = sVal;
		}
		else
		{
			pCtx->pLog->LogFinest( L"CTag_goto::Execute: GetAttrVal expr");
			if (GetAttrVal(L"expr", sVal))
			{
				sNext = pCtx->pVbs->ExprEvalToStr(sVal);
			}
		}

		VXMLString sNextItem, sFormName;
		if (sNext.empty())
		{
			if (GetAttrVal(L"nextitem", sVal))
			{
				sNextItem = sVal;
			}
		}

		if (!sNextItem.empty())
		{
			sFormName = menu->FormId;
			sNext = format_wstring(L"#%s", sNextItem.c_str());

			//if (VAR* pParam = pMenu->Find(L"formid"))
			//{
			//	sFormName = pParam->vValue.bstrVal;
			//	sNext = format_wstring(L"#%s", sNextItem.c_str());
			//}
		}

		return MakeNextMenu(pCtx, sNext, sFormName, pNextTag);

		//pNextTag = ? -> end of execution event
	}

	VXMLString GetGrammarFileName(
		const VXMLString& aGrammarSrc,
		const VXMLString& aModeText, 
		const VXMLString& aGrammarPath)
	{
		VXMLString src = aGrammarSrc;
		int pos = -1;
		if ((pos = src.find(L"builtin:")) >= 0)
		{
			src = src.replace(pos, 8, L"");
			if ((pos = src.find(aModeText + L"/")) != -1)
			{
				src = src.replace(pos, 5, aModeText + L"-");
			}
			if (src.find(L".gr") == -1)
			{
				src = src + L".gr";
			}
			//sGrammarName = sSrc;
			src = aGrammarPath + src;
		}
		else if ((pos = src.find(L"http://")) >= 0)
		{
			//nothing to do
		}

		return src;
	}

	bool CTag_grammar::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_grammar::onBeginTag(CExecContext* pCtx)
	{
		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::runtime_error("Empty event scope");
		}

		VXMLString sMode = Utils::toLower<wchar_t>(GetAttrVal(L"mode"));
		if (sMode != L"dtmf" && sMode != L"voice")
		{
			throw std::runtime_error(std::string("Unsupported input mode: ") + wtos(sMode));
		}

		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr menu = pSes->GetCurMenu();
		menu->InputMode = sMode;

		pVar->AddVar(L"inputmode",sMode.c_str(),false);

		VXMLString sSrc, sGrammarName;
		if (GetAttrVal(L"src", sSrc))
		{
		}
		else if (GetAttrVal(L"srcexpr", sSrc))
		{
			sSrc = pCtx->pVbs->ExprEvalToStr(sSrc);
		}

		bool bText = false;

		// external grammar
		if (!sSrc.empty())
		{
			sSrc = GetGrammarFileName(sSrc, sMode, pSes->GetGrammaPath());
			sGrammarName = format_wstring(L"%s%u", sMode.c_str(), GetTickCount());
		}
		else //internal grammar
		{
			if (!sMode.compare(L"dtmf"))
			{
				throw std::runtime_error("no internal dtmf grammar available");
			}
			else if (!sMode.compare(L"voice"))
			{
				sSrc = L"<?xml version=\"1.0\" encoding=\"utf-8\"?>";
				this->serialize2(pCtx, sSrc);
				sGrammarName = format_wstring(L"voice%u", GetTickCount());
				bText = true;
			}

		}

		pCtx->pLog->LogInfo( L"Add grammar: name = \"%s\", mode = \"%s\", src = \"%s\""
			, sGrammarName.c_str()
			, sMode.c_str()
			, sSrc.c_str());

		pSes->AddGrammar(sGrammarName, sSrc, sMode, L"application/srgs+xml", bText);

		if (!sMode.compare(L"dtmf"))
		{
			pVar->AddVar(L"dtmfgrammar", sGrammarName.c_str(), false);
		}
		if (!sMode.compare(L"voice"))
		{
			pVar->AddVar(L"voicegrammar", sGrammarName.c_str(), false);
		}

		VXMLString term;
		if (GetAttrVal(L"term",term))
		{
			pVar->AddVar(L"termchar", term.c_str(), false);
		}

		return false; //never get in
	}

	CTag_if::CTag_if(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag * pParent, CEvtScopeCollector& _scope) : CParentTag(_doc, _parser, pParent, _scope)
	{
	}

	bool CTag_if::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		//bool bRes = pCtx->pVbs->ExprEvalToBool(GetAttrVal(L"cond"));
		//bool bDone = false;

		//auto ifCondition =
		//	[&bRes, &bDone, pCtx](TagPtr pTag)
		//{
		//	// If we found <else> or <elseif>, break the cycle 
		//	// if main <if> condition is true, so go to the end of <if>.
		//	// If the condition is false, evaluate it (<else> is equivalent to <elseif cond="true">)
		//	if (!bDone)
		//	{
		//		if (pTag->GetName() == L"else")
		//		{
		//			if (bRes)
		//			{
		//				bDone = true;
		//				return true;
		//			}
		//			bRes = true;
		//		}
		//		else if (pTag->GetName() == L"elseif")
		//		{
		//			if (bRes)
		//			{
		//				bDone = true;
		//				return true;
		//			}
		//			bRes = pCtx->pVbs->ExprEvalToBool(pTag->GetAttrVal(L"cond"));
		//		}
		//		else if (bRes)
		//		{
		//			// Execute child tags if we are in the true condition
		//			return false;
		//		}
		//	}

		//	return true;
		//};

		//SetFilter(ifCondition);

		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_if::isInitialized(CExecContext * pCtx)
	{
		bool bRes = pCtx->pVbs->ExprEvalToBool(GetAttrVal(L"cond"));
		bool bDone = false;

		auto ifCondition =
			[&bRes, &bDone, pCtx](TagPtr pTag)
		{
			// If we found <else> or <elseif>, break the cycle 
			// if main <if> condition is true, so go to the end of <if>.
			// If the condition is false, evaluate it (<else> is equivalent to <elseif cond="true">)
			if (!bDone)
			{
				if (pTag->GetName() == L"else")
				{
					if (bRes)
					{
						bDone = true;
						return false;
					}
					bRes = true;
				}
				else if (pTag->GetName() == L"elseif")
				{
					if (bRes)
					{
						bDone = true;
						return false;
					}
					bRes = pCtx->pVbs->ExprEvalToBool(pTag->GetAttrVal(L"cond"));
				}
				else if (bRes)
				{
					// Execute child tags if we are in the true condition
					return true;
				}
			}

			return false;
		};

		SetFilter2(ifCondition);

		return true;
	}

	CTag_item::CTag_item(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag * pParent, CEvtScopeCollector& _scope)
		: CParentTag(_doc, _parser, pParent, _scope)
	{
	}
	bool CTag_item::Execute(CExecContext * pCtx, const TagWPtr & pCurtag, TagWPtr & pNextTag)
	{
		return false;
	}

	bool CTag_log::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		//return false;

		VXMLString sExpr, sLabel, sText;
		sText = GetAttrVal(L"expr");
		GetAttrVal(L"label", sLabel);
		sExpr = pCtx->pVbs->ExprEvalToStr(sText);
		pCtx->pLog->LogFinest( L"Expr: \"%s\" (source: \"%s\"), Label: \"%s\"", sExpr.c_str(), sText.c_str(), sLabel.c_str());
		return true;
	}

	//this tag will appear in version vxml 3.0 
	bool CTag_media::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
	}

	CTag_menu::CTag_menu(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CExecutableTag(_doc, _parser, pParent, _scope)
	{
	}

	bool CTag_menu::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_menu::onBeginTag(CExecContext* pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		// initializing menu
		VXMLString menuID = GetAttrVal(L"id");

		if (menuID.empty())
		{
			//error.badfetch
			pCtx->pLog->LogWarning(L"CTag_menu::Execute: EmitError");
			EmitErrorBadfetch(
				pCtx, 
				BadfetchErrorsToString(BadfetchErrors::BE_SYNTAX), 
				GetName(),
				L"The <menu>-tag must have a name");
			return false;
		}

		MenuPtr menu = pSes->GetCurMenu();
		menu->Utterance.clear();
		menu->InputEvent = L"noinput";
		menu->IsGetDigitsSent = false;
		menu->InputMode = L"dtmf";
		//menu.PromptCount = 0;

		ScopeServicePtr pVar = pCtx->pVar.lock();
		pVar->AddVar(L"inputmode", L"dtmf", false);

		//SessionObject pMenu = pSes->FindMenu(menuID);
		//if (!pMenu)
		//{
		//	throw std::range_error("There are no current menu");
		//}

		VXMLString sComment;
		if (GetAttrVal(L"comment", sComment))
		{
			pSes->ProccedFormComment(menuID, sComment);
		}

		if (!SUCCEEDED(pCtx->pVbs->ExecStatement(menuID + L"= \"\"")))
		{
			pCtx->pLog->LogWarning(L"ExecStatement failed: %s = \"\"", menuID.c_str());
		}

		//pMenu->Add(L"utterance", L"", true);
		//pMenu->Add(L"event", L"noinput", true);
		//pMenu->Add(L"GetDigitsSent", false, true);
		//pMenu->Add(L"inputmode", "dtmf", false);
		//
		//if (VAR * prompt_count = pMenu->Find(L"prompt_count"))
		//{

		//}
		//else
		//{
		//	pMenu->Add(L"prompt_count", 0, true);
		//}

		////pMenu->Add(L"prompt_count", 0, true);
		/////***************** DO NOT DELETE - FUTURE VERSION ************************/
		//////pMenu->Add(L"noinput_count", 0, true); for future version
		//////pMenu->Add(L"nomatch_count", 0, true);
		/////***************** DO NOT DELETE ************************/
		////CLEARCOUNTERS; // deprecated - delete this 

		return true;
	}

	bool CTag_menu::onEndTag(CExecContext* pCtx)
	{
		__super::onEndTag(pCtx);

		SessionPtr2 pSes = pCtx->pSes.lock();
		if (/*!pSes->IsMenuStillActive() || */!pSes->IsSessionActive())
			return false;

		MenuPtr menu = pSes->GetCurMenu();

		if (!menu->IsGetDigitsSent && menu->Utterance.empty())
		{
			return true;
		}

		VXMLString sEvent = L"noinput"; // default event
		if (!menu->InputEvent.empty())
		{
			sEvent = menu->InputEvent;
			pSes->ProccedBufferComment(menu->MenuName, menu->Utterance);
		}

		//VXMLString curMenuName = GetAttrVal(L"id");
		//SessionObject pMenu = pSes->FindMenu(curMenuName);

		//bool bGetDigitsSent = false;
		//if (const VAR * pGetDigitsSent = pMenu->Find(L"GetDigitsSent"))
		//{
		//	bGetDigitsSent = static_cast<bool>(!!pGetDigitsSent->vValue.boolVal);
		//}

		//VXMLString sUtterance;
		//if (VAR *value = pMenu->Find(L"utterance"))
		//{
		//	sUtterance = _bstr_t(value->vValue.bstrVal);
		//}

		//if (!bGetDigitsSent && sUtterance.empty())
		//{
		//	return true;
		//}

		//VXMLString sEvent = L"noinput"; // default event

		//if (VAR * eventType = pMenu->Find(L"event"))
		//{
		//	sEvent = _bstr_t(eventType->vValue.bstrVal);
		//	pSes->ProccedBufferComment(curMenuName, sUtterance);
		//}

		ScopeServicePtr newScope = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());
		newScope->PushScope(std::make_shared<CNameTable>(L"menu_scope"));
		// send event
		CMessage msg(sEvent.c_str());

		TagWPtr nextMenuTag = ExecEvents(pCtx, msg);
		if (nextMenuTag.lock().get())
		{
			pSes->EmitSubEvent(menu->MenuName, menu, nextMenuTag, newScope);
		}

		return true;
	}

	VXMLString CTag_menu::GetAttrIdName()const
	{
		return GetAttrVal(L"id");
	}

	TagWPtr CTag_menu::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		if (IsCatchEvent(tag))
		{
			return NULLTAG;
		}

		return __super::getNextTag(pCtx, tag);
	}

	//void CTag_menu::AddScope(CEvtScopeCollector& _scope)
	//{
	//		CEvtScope newScope;
	//		_scope.AddScope(newScope);
	//}
	void CTag_menu::AddToDocument(const VXMLString& aParentAttrId, CDocument& _doc)
	{
		_doc.EmplaceMenuTag(DoubleStringKey{ this->GetAttrIdName(), L"" }, shared_from_this());
	}

	bool CTag_meta::Execute(CExecContext * pCtx, const TagWPtr & pCurtag, TagWPtr & pNextTag)
	{
		return false;
	}
	bool CTag_metatext::Execute(CExecContext * pCtx, const TagWPtr & pCurtag, TagWPtr & pNextTag)
	{
		bool       bFetch = false;

		SessionPtr2 pSes = pCtx->pSes.lock();

		/************* CHECKING INPUTMODE *************/
		// only for prompts, menus and fields. No voice in catch events 
		ScopeServicePtr pVar = pCtx->pVar.lock();

		MenuPtr menu = pSes->GetCurMenu();
		menu->Audiomode = L"t2s";

		//pVar->AddVar(L"audiomode", L"t2s", false);

		bool bVoiceInput = (pVar && (pVar->Find(L"voicegrammar") != NULL)) ? true : false;
		bool bDtmfInput = (pVar && (pVar->Find(L"dtmfgrammar") != NULL)) ? true : false;

		pSes->DoSpeakText(this->GetText(), bDtmfInput, bVoiceInput);

		return true;
	}

	CTag_noinput::CTag_noinput(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope): CParentTag(_doc, _parser, pParent, _scope) 
	{ 
		try
		{
			VXMLString sName(L"noinput");
			VXMLString sCount, sCond, sEventVariableName;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);
			GetAttrVal(L"eventname", sEventVariableName);

			CEvtScope pNew(
				sName, 
				sCount.empty() ? 1 : Utils::toNum<int>(sCount), 
				sCond, 
				this, 
				_parser->GetUri(),
				sEventVariableName);

			_scope.AddScope(pNew);
			//_scope.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"noinput\" tag");
		}
	}

	bool CTag_noinput::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}


	CTag_nomatch::CTag_nomatch(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope): CParentTag(_doc, _parser, pParent, _scope) 
	{ 
		try
		{
			VXMLString sName(L"nomatch");
			VXMLString sCount, sCond, sEventVariableName;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);
			GetAttrVal(L"eventname", sEventVariableName);

			CEvtScope pNew(
				sName, 
				sCount.empty() ? 1 : Utils::toNum<int>(sCount), 
				sCond, 
				this, 
				_parser->GetUri(),
				sEventVariableName);

			_scope.AddScope(pNew);
			//_scope.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"nomatch\" tag");
		}
	}

	bool CTag_nomatch::Execute(VXML::CExecContext *pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);

		//CScopeSaver saver(pCtx->pVar,L"nomatch");
		//return __super::Execute(pCtx); // - action
	}

	bool CTag_object::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
	}

	CTag_one_of::CTag_one_of(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag * pParent, CEvtScopeCollector& _scope)
		: CParentTag(_doc, _parser, pParent, _scope)
	{
	}
	bool CTag_one_of::Execute(CExecContext * pCtx, const TagWPtr & pCurtag, TagWPtr & pNextTag)
	{
		return false;
	}

	CTag_prompt::CTag_prompt(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope): CParentTag(_doc, _parser, pParent, _scope)//, m_pOwner(NULL)
	{
		//try
		//{
			VXMLString sName(L"prompt");
			VXMLString sCount, sCond, sEventVariableName;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);
			GetAttrVal(L"eventname", sEventVariableName);

			size_t count = sCount.empty() ? 1 : Utils::toNum<int>(sCount);

			//if (_scope.CheckPromptExist(count))
			//{
			//	throw std::runtime_error((boost::format("prompt [line == %i] got the same counter [%i] as previous ones") % this->GetLine() % count).str());
			//}

			CEvtScope pNew(
				sName, 
				count,
				sCond, 
				this, 
				_parser->GetUri(),
				sEventVariableName);

			_scope.AddPrompt(pNew);
		//}
		//catch (...)
		//{
		//	//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"prompt\" tag");
		//}
	}
	bool CTag_prompt::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_prompt::onBeginTag(CExecContext* pCtx)
	{
		auto pSes = pCtx->pSes.lock();

		bool       bFetch = true;
		VXMLString sFetch;
		if (GetAttrVal(L"bargein", sFetch))
		{
			bFetch = pCtx->pVbs->ExprEvalToBool(sFetch);
		}
		
		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		pVar->AddVar(L"bargein", bFetch, false);

		const CExecutableTag* exec = GetExecParent();
		if (!exec)
		{
			throw std::runtime_error("Get exe parent");
		}

		if (!exec->MatchPrompt(shared_from_this(), pCtx))
		{
			return false;
		}

		MenuPtr menu = pSes->GetCurMenu();

		//increase prompt counter
		menu->NeedReprompt = false;
		++menu->PromptCount;
		menu->PromptLine = this->GetLine();

		pCtx->pLog->LogFinest( L"Set prompt_count = %i", menu->PromptCount);

		//VAR* pInputMode = pVar->Find(L"inputmode");
		//if (pInputMode && !pInputMode->AsString().empty())
		//{
		//	menu.InputMode = pInputMode->AsString(); // need this ???
		//}

		//SessionObject pMenu = pCtx->pSes.lock()->FindMenu(exec->GetAttrIdName());
		//{
		//	VAR* pReprompt = pMenu->Find(L"reprompt");
		//	if (pReprompt)
		//	{
		//		CComVariant v;
		//		v.vt = VT_BOOL;
		//		v.boolVal = VARIANT_FALSE;
		//		pReprompt->SetValue(v);
		//	}


		//	VAR* pPrompCount = pMenu->Find(L"prompt_count");
		//	if (pPrompCount)
		//	{
		//		CComVariant v;
		//		v.vt = VT_INT;
		//		v.intVal = pPrompCount->vValue.intVal + 1;
		//		pPrompCount->SetValue(v);
		//		pCtx->pLog->LogFinest( L"Set prompt_count = %i", v.intVal);
		//	}
		//}

		//VAR* pInputMode = pVar->Find(L"inputmode");
		//if (pInputMode && !pInputMode->AsString().empty())
		//{
		//	pMenu->Add(L"inputmode", pInputMode->AsString().c_str(), false);
		//}

		return true;
	}

	bool CTag_property::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		VXMLString sPropertyName,
				   sPropertyValue;

		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}


		if(GetAttrVal(L"name",sPropertyName) && GetAttrVal(L"value",sPropertyValue))
		{
			int pos = -1;
			while ((pos = sPropertyValue.find(L"\"",++pos))>=0)
			{
				sPropertyValue.replace(pos,1,L"");
			}
			pVar->AddVar(sPropertyName.c_str(),sPropertyValue.c_str(),false);
		}

		//pNextTag = getNextTag(pCtx);
		//pNextTag = m_pParent->getNextTag(pCtx, this);

		return true;
	}


	bool CTag_record::GetRecord(CExecContext* pCtx)
	{
		//CMessage msg(L"REC_WAV");

		//VXMLString sRecordInputName,
		//	sMaxTime,
		//	//sFinalSilence,
		//	sType;
		//bool       //bBeep     = false, ignore it
		//	bDtmfterm = false;

		//// get input file name than will hold the recoding
		//if(GetAttrVal(L"name",sRecordInputName))
		//{
		//	sRecordInputName = pCtx->pVbs->ExprEvalToStr(sRecordInputName);
		//}
		//else if (GetAttrVal(L"expr",sRecordInputName))
		//{
		//	sRecordInputName = pCtx->pVbs->ExprEvalToStr(sRecordInputName);
		//}
		//msg[L"FileName"] = sRecordInputName.c_str();

		//ScopeServicePtr pVar = pCtx->pVar.lock();
		//if (!pVar)
		//{
		//	throw std::range_error("Empty event scope");
		//}

		//// get time params
		//if(GetAttrVal(L"maxtime",sMaxTime))
		//{
		//	/*int pos = -1;
		//	if ((pos = sMaxTime.find(L"ms",0))>=0)
		//	{
		//		sMaxTime.replace(pos,2,L"");
		//	}
		//	if ((pos = sMaxTime.find(L"s",0))>=0)
		//	{
		//		sMaxTime.replace(pos,1,L"");
		//	}

		//	msg[L"MaxTime"] = Utils::toNum<int>(sMaxTime);*/
		//	msg[L"MaxTime"] = Utils::ConvertTimeFromCSS1(sMaxTime);
		//}
		//else
		//{
		//	VAR * pWaitTime = pVar->Find(L"WaitTime");
		//	msg[L"MaxTime"]	   = Utils::toNum<int>(_bstr_t(pWaitTime->vValue).GetBSTR());
		//}

		////if(GetAttrVal(L"finalsilence",sFinalSilence))
		////{
		////	int pos = -1;
		////	if ((pos = sFinalSilence.find(L"ms",0))>=0)
		////	{
		////		sFinalSilence.replace(pos,2,L"");
		////	}
		////	if ((pos = sFinalSilence.find(L"s",0))>=0)
		////	{
		////		sFinalSilence.replace(pos,1,L"");
		////	}

		////	//msg[L"MaxSilence"] = Utils::toNum<int>(sFinalSilence);
		////}
		////else
		////{
		//	VAR * pTimeOut  = pVar->Find(L"timeout");
		//	msg[L"MaxSilence"] = Utils::toNum<int>(_bstr_t(pTimeOut->vValue).GetBSTR());
		////}

		//VXMLString sTermChar = _bstr_t(pVar->Find(L"termchar")->vValue);
		//msg[L"DigitMask"]  = sTermChar;

		////m_RecWavCompletedMsg.Clear();
		//SessionPtr2 pSes = pCtx->pSes.lock();

		//CMessage waitmsg;
		//VXMLString sId;
		//msg[L"DestinationAddress"] = pSes->GetParentCallID();
		//eWaitEvents retEvent = pSes->WaitEvent(&msg, L"rec_wav.completed", L"error", waitmsg);
		//if (retEvent == WE_SUCCESS)
		//{
		//	//m_RecWavCompletedMsg = waitmsg;
		//	// SUCCESSED
		//	CParam* pParam = waitmsg.ParamByName(L"TerminationReason");
		//	if (!pParam)
		//		return true;
		//	if (pParam->AsString() == L"TM_MAXSIL") // 1. max silence
		//	{
		//		// NOINPUT

		//	}
		//	else if (pParam->AsString() == L"TM_DIGIT") // 2. dtmf input
		//	{
		//		// we're got digits
		//		if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
		//		{
		//			VXMLString digits(pParam->AsString());
		//			pVar->AddVar(L"utterance",VXMLString(pParam->AsString()).c_str(),false);

		//			//test123
		//			//pCtx->pVbs->ExecStatement(L"tmp_utterance = \"" + VXMLString(pParam->AsWideStr()) + L"\"");

		//			CLEARDIGITS;
		//		}
		//	}
		//	else if (pParam->AsString() == L"TM_MAXTIME") // 3. max time
		//	{
		//		pVar->AddVar(L"inputmode",L"voice",false);
		//	}
		//	else if (pParam->AsString() == L"TM_USRSTOP") // 4. hand up
		//	{
		//		pVar->AddVar(L"inputmode",L"voice",false);
		//	}
		//}
		//else //if (retEvent == WE_STOPTHREAD)
		//{
		//	// stop tone playing
		//	CMessage waitmsg, stop_chanel_msg(L"STOP_CHANNEL");
		//	stop_chanel_msg[L"DestinationAddress"] = pSes->GetParentCallID();
		//	//pCtx->pSes->WaitEvent(&stop_chanel_msg, L"stop_chanel.completed", L"error", waitmsg);
		//	pSes->SendEvent(stop_chanel_msg, pSes->GetParentScriptID()); // why wait? why not send?
		//	//::ExitThread(0);
		//	pSes->DoExit(L"");
		//	return false;
		//}
		return true;
	}

	//bool  CTag_record::CheckDigits(CExecContext* pCtx)
	//{
	//	return true; // nothing to do, we already have got digits
	//}

	bool CTag_record::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
	}

	VXMLString CTag_record::GetAttrIdName()const
	{
		return GetAttrVal(L"name");
	}

	TagWPtr CTag_record::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		if (IsCatchEvent(tag))
		{
			return NULLTAG;
		}

		return __super::getNextTag(pCtx, tag);
	}

	//void CTag_record::AddScope(CEvtScopeCollector& _scope)
	//{
	//	CEvtScope newScope;
	//	_scope.AddScope(newScope);
	//}
	void CTag_record::AddToDocument(const VXMLString& aParentAttrId, CDocument& _doc)
	{
		_doc.EmplaceMenuTag(DoubleStringKey{ this->GetAttrIdName(), aParentAttrId }, shared_from_this());
	}

	bool CTag_reprompt::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr menu = pSes->GetCurMenu();
		menu->NeedReprompt = true;
		pCtx->pLog->LogInfo( L"Set reprompt...");
		return true;
	}

	CTag_rule::CTag_rule(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag * pParent, CEvtScopeCollector& _scope)
		:CParentTag(_doc, _parser, pParent, _scope)
	{
	}
	bool CTag_rule::Execute(CExecContext * pCtx, const TagWPtr & pCurTag, TagWPtr & pNextTag)
	{
		return false;//__super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_say_as::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}
	bool CTag_say_as::onBeginTag(CExecContext * pCtx)
	{
		VXMLString sInterpretAs;
		if (GetAttrVal(L"interpret-as", sInterpretAs))
		{
		}
		else if (GetAttrVal(L"interpret-as-expr", sInterpretAs))
		{
			sInterpretAs = pCtx->pVbs->ExprEvalToStr(sInterpretAs);
		}

		if (sInterpretAs.empty())
		{
			pCtx->pLog->LogWarning(L"say_as:execute: \"interpret-as\" or \"interpret-as-expr\" value required");
			return true;
		}

		ScopeServicePtr pVar = pCtx->pVar.lock();
		pVar->AddVar(L"interpret_as",sInterpretAs.c_str(),false);

		VXMLString sFormat,sDetail;
		if (GetAttrVal(L"format",sFormat))
			pVar->AddVar(L"format",sFormat.c_str(),false);
		if (GetAttrVal(L"detail",sDetail))
			pVar->AddVar(L"detail",sDetail.c_str(),false);
		return true;
	}
	bool CTag_script::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		//pCtx->pLog->Log(__FUNCTIONW__, L"%s",GetText().c_str());
		VXMLString sScriptText = GetXML();
		VXMLString sSrc, sFileSrc;
		SessionPtr2 pSes = pCtx->pSes.lock();
		if (sScriptText.empty() && GetAttrVal(L"src",sSrc) && !sSrc.empty())
		{

				//pCtx->pSes->GetFileFolder(),pCtx->pSes->GetCurDocUri()
				VXMLString _root_folder = pSes->GetFileFolder();
				VXMLString _curdoc      = pSes->GetCurDocUri();
				bool bRootFolder = false;
				if (sSrc[0] == '/')
				{
					sSrc.erase(0,1); // delete symbol "\"
					bRootFolder = true;
				}

				// for absolute URI
				//WIN32_FIND_DATA fileData;
				int pos = -1;
				//if (!Utils::GetFile(sSrc,&fileData))
				if ((_waccess(sSrc.c_str(),4))!=-1)
				{
					sFileSrc = sSrc;
				}
				// URI is relative 
				if (sFileSrc.empty())
				{
					WCHAR dir   [MAX_PATH]; ZeroMemory(dir   ,sizeof(dir   ));
					WCHAR ExtDir[MAX_PATH]; ZeroMemory(ExtDir,sizeof(ExtDir));
					if ((pos = sSrc.rfind('\\')) != -1)
					{
						sSrc._Copy_s(ExtDir,MAX_PATH,pos+1); // include "\"
						sSrc.erase(0,pos+1);
					}
					if (bRootFolder)
					{
						wcscpy_s(dir,_root_folder.c_str());
					}
					else if ((pos = _curdoc.rfind('\\')) != -1)
					{
						_curdoc._Copy_s(dir,MAX_PATH,pos+1); // include "\"
					}
					VXMLString mask = dir + VXMLString(ExtDir) + sSrc;
					if ((_waccess(mask.c_str(),4))!=-1)
					{
						sFileSrc = mask;
					}
					else
					{
						pCtx->pLog->LogWarning(L"Cannot open file: \"%s\"", sSrc.c_str());
					}

				}
				if (!sFileSrc.empty())
				{
					std::wifstream _in(sFileSrc.c_str(),std::ios::binary);
					// read whole file
					DWORD   dwDataSize=0;                                    
					if( _in.seekg(0, std::ios::end) )
					{
						dwDataSize = static_cast<DWORD>(_in.tellg());
					}
					std::wstring buff(dwDataSize,0);
					if( dwDataSize && _in.seekg(0, std::ios::beg) )
					{
						std::copy( std::istreambuf_iterator< wchar_t >(_in) ,
							std::istreambuf_iterator< wchar_t >() , 
							buff.begin() );
						m_sText = buff;
					}
					else
					{
						pCtx->pLog->LogWarning(L"File: \"%s\" is empty", sSrc.c_str());
					}
					_in.close();
				}

		}

		pCtx->pVbs->ExecScript(sScriptText);

		return true;
	}

	CTag_subdialog::CTag_subdialog(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope)
		: CExecutableTag(_doc, _parser, pParent, _scope)
	{
	}

	bool CTag_subdialog::StartDialog(CExecContext* pCtx)
	{
		VXMLString sSubDialogURI, sFunctionName;
		bool bStartSubDialog = true;
		if(GetAttrVal(L"src",sSubDialogURI))
		{

		}
		else if (GetAttrVal(L"expr",sSubDialogURI))
		{
			sSubDialogURI = pCtx->pVbs->ExprEvalToStr(sSubDialogURI);
		}
		else if (GetAttrVal(L"function", sFunctionName))
		{
			bStartSubDialog = false;
		}

		VXMLString sNamelist;
		if (GetAttrVal(L"namelist", sNamelist))
		{
		}

		SessionPtr2 pSes = pCtx->pSes.lock();
		// ToDo: supporting file text !!!
		VXMLString sCurDoc       = pSes->GetCurDocUri();
		VXMLString file, 
			menu;

		if (bStartSubDialog)
		{
			TranslateURI(pSes->GetFileFolder(), sCurDoc, sSubDialogURI, file, menu);
			if (file.empty())
				file = sCurDoc;

			pSes->DoStartDialog(file, menu, L"dialog", sNamelist);
		}
		else
		{
			pSes->DoStartFunction(sFunctionName, sNamelist);
		}
		return true;
	}

	bool CTag_subdialog::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	VXMLString CTag_subdialog::GetAttrIdName()const
	{
		return GetAttrVal(L"name");
	}

	bool CTag_subdialog::isFinitialized(CExecContext * pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr menu = pSes->GetCurMenu();
		if (menu->IsDialogStarted)
		{
			return true;
		}

		this->StartDialog(pCtx);
		menu->IsDialogStarted = true;

		return false;//return here
	}

	bool CTag_subdialog::onBeginTag(CExecContext* pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();
		// initializing menu
		VXMLString subdialog_name = GetAttrVal(L"name");

		if (subdialog_name.empty())
		{
			//error.badfetch
			pCtx->pLog->LogWarning(L"CTag_subdialog::onBeginTag: EmitError");
			EmitErrorBadfetch(
				pCtx, 
				BadfetchErrorsToString(BadfetchErrors::BE_SYNTAX),
				GetName(),
				L"The <subdialog>-tag must have a name");
			return false;
		}

		// Check conditional expression value if there is 'cond' attribute
		VXMLString sCond;
		if (GetAttrVal(L"cond", sCond))
		{
			if (!pCtx->pVbs->ExprEvalToBool(sCond))
			{
				return false;
			}
		}

		if (!SUCCEEDED(pCtx->pVbs->ExecStatement(subdialog_name + L"= \"\"")))
		{
			pCtx->pLog->LogWarning(L"ExecStatement failed: subdialog_name = \"\"");
		}

		MenuPtr menu = pSes->GetCurMenu();
		menu->Utterance.clear(); // need this ???
		menu->InputEvent = L"noinput";
		menu->IsDialogStarted = false;


		//pMenu->Add(L"prompt_count", 0, true);
		///***************** DO NOT DELETE - FUTURE VERSION ************************/
		////pMenu->Add(L"noinput_count", 0, true); for future version
		////pMenu->Add(L"nomatch_count", 0, true);
		///***************** DO NOT DELETE ************************/
		//CLEARCOUNTERS; // deprecated - delete this 

		return true;
	}

	bool CTag_subdialog::onEndTag(CExecContext* pCtx)
	{
		__super::onEndTag(pCtx);

		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr menu = pSes->GetCurMenu();
		VXMLString sInputmode = menu->InputEvent;

		if (sInputmode.empty())
		{
			sInputmode = L"dtmf";
		}

		//CHECK INPUT
		if (!sInputmode.compare(L"voice"))
		{
			//if (!pCurField->CheckVoiceInput(pCtx)) // ToDo: recognize!
			return false;
		}
		if (!sInputmode.compare(L"dtmf"))
		{
			//if (!this->CheckDigits(pCtx)) has already checked
			//	return false;
		}

		ScopeServicePtr pVar = pCtx->pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		VXMLString exist_event(L"filled");

		if (0 == sInputmode.compare(L"voice"))
		{
			//the event has already defined in mrcp answer
		}
		if (0 == sInputmode.compare(L"dtmf"))
		{
			SessionGrammar pLocalGrammar;
			if (VAR* pDtmfGrammar = pVar->Find(L"dtmfgrammar")) //looking for local grammar
			{
				VXMLString grammar_name = _bstr_t(pDtmfGrammar->vValue);
				pLocalGrammar = pSes->FindGrammar(grammar_name);
			}

			if (pLocalGrammar)
			{
				exist_event = pLocalGrammar->LookUp(menu->Utterance);
			}
		}

		menu->InputEvent = exist_event;



		//VXMLString curMenuName = pSes->GetCurMenu();
		//SessionObject pMenu = pSes->FindMenu(curMenuName);

		//// CHECKING INPUTMODE
		//VXMLString sUtterance, sInputmode;
		//if (VAR *value = pMenu->Find(L"inputmode"))
		//{
		//	sInputmode = _bstr_t(value->vValue.bstrVal);
		//}

		//if (sInputmode.empty()) // still nothing -> use default inputmode
		//{
		//	sInputmode = L"dtmf";
		//}

		////CHECK INPUT
		//if (!sInputmode.compare(L"voice"))
		//{
		//	//if (!pCurField->CheckVoiceInput(pCtx)) // ToDo: recognize!
		//	return false;
		//}
		//if (!sInputmode.compare(L"dtmf"))
		//{
		//	//if (!this->CheckDigits(pCtx)) has already checked
		//	//	return false;
		//}

		//// GET UTTERANCE
		//if (VAR *value = pMenu->Find(L"utterance"))
		//{
		//	sUtterance = _bstr_t(value->vValue.bstrVal);
		//}

		//ScopeServicePtr pVar = pCtx->pVar.lock();
		//if (!pVar)
		//{
		//	throw std::range_error("Empty event scope");
		//}

		//// MATCH UTTERANCE
		//VXMLString exist_event(L"filled");

		//if (0 == sInputmode.compare(L"voice"))
		//{
		//	//the event has already defined in mrcp answer
		//}
		//if (0 == sInputmode.compare(L"dtmf"))
		//{
		//	SessionGrammar pLocalGrammar;
		//	if (VAR* pDtmfGrammar = pVar->Find(L"dtmfgrammar")) //looking for local grammar
		//	{
		//		VXMLString grammar_name = _bstr_t(pDtmfGrammar->vValue);
		//		pLocalGrammar = pSes->FindGrammar(grammar_name);
		//	}

		//	if (pLocalGrammar)
		//	{
		//		exist_event = pLocalGrammar->LookUp(sUtterance);
		//	}
		//}

		//pMenu->Add(L"event", exist_event.c_str(), true);

		/////***************** DO NOT DELETE - FUTURE VERSION ************************/
		////if (VAR* pParam = pMenu->Find(L"noinput_count"))
		////{
		////	CComVariant v;
		////	v.vt = VT_INT;
		////	v.intVal = 0;
		////	pParam->SetValue(v);
		////}

		////if (VAR* pParam = pMenu->Find(L"nomatch_count"))
		////{
		////	CComVariant v;
		////	v.vt = VT_INT;
		////	v.intVal = 0;
		////	pParam->SetValue(v);
		////}
		/////***************** DO NOT DELETE ************************/
		////CLEARCOUNTERS; // deprecated - delete this 

		ScopeServicePtr newScope = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());
		newScope->PushScope(std::make_shared<CNameTable>(L"subdialog_scope"));

		// send event
		CMessage msg(exist_event.c_str());

		TagWPtr nextMenuTag = ExecEvents(pCtx, msg);
		if (nextMenuTag.lock().get())
		{
			pSes->EmitSubEvent(menu->MenuName, menu, nextMenuTag, newScope);
		}

		return true;
	}

	TagWPtr CTag_subdialog::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		if (IsCatchEvent(tag))
		{
			return NULLTAG;
		}

		return __super::getNextTag(pCtx, tag);
	}

	//void CTag_subdialog::AddScope(CEvtScopeCollector& _scope)
	//{
	//	CEvtScope newScope;
	//	_scope.AddScope(newScope);
	//}
	void CTag_subdialog::AddToDocument(const VXMLString& aParentAttrId, CDocument& _doc)
	{
		_doc.EmplaceMenuTag(DoubleStringKey{ this->GetAttrIdName(), aParentAttrId }, shared_from_this());
	}

	bool CTag_send::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		VXMLString requestId, requestIdName;
		if (!GetAttrVal(L"requestid", requestIdName))
		{
			EmitErrorBadfetch(
				pCtx, 
				BadfetchErrorsToString(BadfetchErrors::BE_SYNTAX),
				GetName(),
				L"<send> \"requestId\" attribute required");
			return true;
		}

		auto getValue = [&](const VXMLString& aName, bool isForce)
		{
			VXMLString value;
			if (GetAttrVal(aName + L"expr", value))
			{
				value = pCtx->pVbs->ExprEvalToStr(value);
			}
			else if (GetAttrVal(aName, value))
			{
				// Nothing
			}

			if (isForce && value.empty())
			{
				throw std::runtime_error(std::string(wtos(aName) + " attribute required"));
			}

			return value;
		};

		EventConverter converter;
		auto eventString = getValue(L"event", true);
		auto eventType = converter.GetEventByShortString(eventString);

		if (eventType == E_ASYNC_EVENT::AE_GET)
		{
			requestId = pCtx->pVbs->ExprEvalToStr(requestIdName);
		}
		else
		{
			requestId = CIdGenerator::GetInstance()->makeSId();
		}

		bool isAsync = false;
		VXMLString asyncExpr;
		if (GetAttrVal(L"asyncexpr", asyncExpr))
		{
			isAsync = pCtx->pVbs->ExprEvalToBool(asyncExpr);
		}
		else if (GetAttrVal(L"async", asyncExpr))
		{
			isAsync = pCtx->pVbs->ExprEvalToBool(asyncExpr);
		}

		bool isCancel = false;
		VXMLString cancel;
		if (GetAttrVal(L"cancel", cancel))
		{
			isCancel = pCtx->pVbs->ExprEvalToBool(cancel);
		}

		VXMLString namelist, timeout;
		GetAttrVal(L"namelist", namelist);

		if (GetAttrVal(L"timeout", timeout))
		{
			timeout = pCtx->pVbs->ExprEvalToStr(timeout);
		}

		try
		{
			auto pSes = pCtx->pSes.lock();

			auto info = AsyncInfo(
				isAsync,
				pSes->GetScriptID(),
				requestId,
				requestIdName,
				getValue(L"body", false),
				getValue(L"contenttype", false),
				eventType,
				namelist,
				getValue(L"target", eventType != E_ASYNC_EVENT::AE_GET),
				getValue(L"port", eventType != E_ASYNC_EVENT::AE_GET),
				timeout,
				isCancel,
				getValue(L"fetchaudio", false));

			if (isAsync)
			{
				pSes->DoAsyncEvent(eventString, info);
			}
			else
			{
				pSes->DoSyncEvent(eventString, info);
			}
		}
		catch (const std::exception& aError)
		{
			EmitErrorBadfetch(
				pCtx, 
				BadfetchErrorsToString(BadfetchErrors::BE_SYNTAX),
				GetName(),
				stow(aError.what()));
			return true;
		}

		return true;
	}

	bool CTag_submit::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
	}

	CTag_transfer::CTag_transfer(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope)
		: CExecutableTag(_doc, _parser, pParent, _scope)
	{
	}

	bool CTag_transfer::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	VXMLString CTag_transfer::GetAttrIdName()const
	{
		return GetAttrVal(L"name");
	}

	bool CTag_transfer::isFinitialized(CExecContext * pCtx)
	{
		SessionPtr2 pSes = pCtx->pSes.lock();

		if (!mTransferInfo.IsDialogTransferSent)
		{

		}

		//SessionObject pMenu = pSes->FindMenu(pSes->GetCurMenu());

		//bool bDialogTransferSent = false;
		//if (VAR * pDialogTransferSent = pMenu->Find(L"DialogTransferSent"))
		//	bDialogTransferSent = static_cast<bool>(!!pDialogTransferSent->vValue.boolVal);
		if (!mTransferInfo.IsDialogTransferSent/*bDialogTransferSent*/)
		{
			VXMLString sExpr, sURI, sBridge, sAai, 
				sSIPContact,
				sSIPUserToUser,
				sTrunk,
				sTransferAudio;

			if (GetAttrVal(L"dest", sExpr))
			{
				sURI = sExpr;
			}
			else if (GetAttrVal(L"destexpr", sExpr))
			{
				sURI = pCtx->pVbs->ExprEvalToStr(sExpr);
			}

			if (sURI.empty())
				return false;
			
			bool bBridge = false;
			if (GetAttrVal(L"bridge", sBridge))
			{
				bBridge = pCtx->pVbs->ExprEvalToBool(sBridge);
			}

			ScopeServicePtr pVar = pCtx->pVar.lock();
			if (!pVar)
			{
				throw std::range_error("Empty event scope");
			}

			VXMLString sMaxTime;
			if (GetAttrVal(L"maxtime", sMaxTime))
			{
			}
			else
			{
				VAR * pWaitTime = pVar->Find(L"WaitTime");
				sMaxTime = _bstr_t(pWaitTime->vValue).GetBSTR();
			}

			VXMLString sConnectTimeout;
			if (GetAttrVal(L"connecttimeout", sConnectTimeout))
			{
			}

			if (GetAttrVal(L"aai", sAai))
			{
			}
			else if (GetAttrVal(L"aaiexpr", sAai))
			{
				sAai = pCtx->pVbs->ExprEvalToStr(sAai);
			}

			if (GetAttrVal(L"SIPContact", sSIPContact))
			{
				sSIPContact = pCtx->pVbs->ExprEvalToStr(sSIPContact);
			}
			if (GetAttrVal(L"SIPUserToUser", sSIPUserToUser))
			{
				sSIPUserToUser = pCtx->pVbs->ExprEvalToStr(sSIPUserToUser);
			}

			if (GetAttrVal(L"trunk", sTrunk))
			{
			}

			if (GetAttrVal(L"transferaudio", sTransferAudio))
			{
			}
			else if (GetAttrVal(L"transferaudioexpr", sTransferAudio))
			{
				sTransferAudio = pCtx->pVbs->ExprEvalToStr(sTransferAudio);
			}

			if (!sTransferAudio.empty())
			{
				mTransferInfo.IsDialogTransferAudioRepeat = true;
				mTransferInfo.DialogTransferAudioUri = sTransferAudio;

				//pMenu->Add(L"DialogTransferAudioRepeat", true, false);
				//pMenu->Add(L"DialogTransferAudioUri"   , sTransferAudio.c_str(), false);
			}

			VXMLString sNamelist;
			if (GetAttrVal(L"namelist", sNamelist))
			{
			}

			pSes->DoDialogTransfer(
				sURI, 
				bBridge, 
				sMaxTime, 
				sConnectTimeout, 
				sAai,
				sSIPContact,
				sSIPUserToUser,
				sTrunk,
				sTransferAudio,
				sNamelist,
				mTransferInfo);
			//pMenu->Add(L"DialogTransferSent", true, false);
			mTransferInfo.IsDialogTransferSent = true;

			return false;

		}

		return true;
	}

	bool CTag_transfer::onBeginTag(CExecContext * pCtx)
	{
		mTransferInfo = TransferInfo();

		VXMLString sCond;
		if (GetAttrVal(L"cond", sCond))
		{
			if (!pCtx->pVbs->ExprEvalToBool(sCond))
			{
				return false;
			}
		}

		VXMLString transfer_name = GetAttrIdName();
		if (!SUCCEEDED(pCtx->pVbs->ExecStatement(transfer_name + L"= \"\"")))
		{
			pCtx->pLog->LogWarning(L"ExecStatement: cannot clear transfer_name \"%s\"", transfer_name.c_str());
		}

		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr& menu = pSes->GetCurMenu();
		menu->Utterance.clear(); // need this ???
		menu->InputEvent = L"noinput";
		//menu.PromptCount = 0;

		/*SessionObject pMenu = pSes->FindMenu(transfer_name);
		if (!pMenu)
		{
			throw std::range_error("There are no current transfer");
		}


		pMenu->Add(L"utterance", L"", true);
		pMenu->Add(L"event", L"noinput", true);

		if (VAR * prompt_count = pMenu->Find(L"prompt_count"))
		{

		}
		else
		{
			pMenu->Add(L"prompt_count", 0, true);
		}
*/
		return true;
	}

	bool CTag_transfer::onEndTag(CExecContext * pCtx)
	{
		__super::onEndTag(pCtx);

		SessionPtr2 pSes = pCtx->pSes.lock();

		MenuPtr& menu = pSes->GetCurMenu();
		menu->InputEvent = L"filled";

		//VXMLString curMenuName = pSes->GetCurMenu();
		//SessionObject pMenu = pSes->FindMenu(curMenuName);

		//// ALWAYS FILLED
		//VXMLString exist_event(L"filled");
		//pMenu->Add(L"event", exist_event.c_str(), true);

		ScopeServicePtr newScope = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());
		newScope->PushScope(std::make_shared<CNameTable>(L"transfer_scope"));

		// send event
		CMessage msg(L"filled");

		TagWPtr nextMenuTag = ExecEvents(pCtx, msg);
		if (nextMenuTag.lock().get())
		{
			pSes->EmitSubEvent(menu->MenuName, menu, nextMenuTag, newScope);
		}
		return true;
	}

	TagWPtr CTag_transfer::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		if (IsCatchEvent(tag))
		{
			return NULLTAG;
		}

		return __super::getNextTag(pCtx, tag);
	}

	//void CTag_transfer::AddScope(CEvtScopeCollector& _scope)
	//{
	//	CEvtScope newScope;
	//	_scope.AddScope(newScope);
	//}
	void CTag_transfer::AddToDocument(const VXMLString& aParentAttrId, CDocument& _doc)
	{
		_doc.EmplaceMenuTag(DoubleStringKey{ this->GetAttrIdName(), aParentAttrId }, shared_from_this());
	}

	bool CTag_throw::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return false;
	}

	CTag_vxml::CTag_vxml(
		CDocument& _doc, 
		const Factory_c::CollectorPtr& _parser, 
		CParentTag * pParent, 
		CEvtScopeCollector& _scope) : CExecutableTag(_doc, _parser, pParent, _scope)
	{
		SetFilter([](TagPtr pTag)
		{
			return !IsCatchEvent(pTag->GetName());
		});

		//_doc.SetGlobalEvtScope(m_EvtScope);
	}

	bool CTag_vxml::Execute(VXML::CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		return __super::Execute(pCtx, pCurTag, pNextTag);
	}

	bool CTag_vxml::MakeBreakingEvent(VXML::CExecContext* pCtx, const TagWPtr& tag) const
	{
		if (tag.lock())
		{
			SessionPtr2 pSes = pCtx->pSes.lock();

			//VXMLString menuName = pCtx->pEvt->GetAttrId();  
			//if (menuName.empty())
			//{
			//	menuName = tag.lock()->GetAttrIdName();
			//}
			//

			////if (1) //check previously definded menu id 
			////{
			////	VAR* pMenuName = pCtx->pEvt->Find(L"NextExecuteMenuID");
			////	if (pMenuName && !pMenuName->AsString().empty()) //just change menu name
			////		menuName = pMenuName->vValue.bstrVal;
			////}

			//CMenu menu = pSes->GetMenu(menuName, L"");

			//if (!menu.IsEnabled())
			//{
			//	EmitErrorBadfetch(
			//		pCtx,
			//		BadfetchErrorsToString(BadfetchErrors::BE_MENU_NOT_FOUND),
			//		(boost::wformat(L"There are no menu with id = \"%s\" in current document: \"%s\"") % menuName % pSes->GetCurDocUri()).str());

			//	return false;
			//}

			MenuPtr menu = pSes->GetCurMenu();

			ScopeServicePtr newScope = std::make_shared<CScopeService>(*pCtx->pVar.lock().get());
			newScope->PushScope(std::make_shared<CNameTable>(L"vxml_scope"));

			pSes->EmitRecursiveMenuEvent(menu->MenuName, menu, newScope);

			return true;
		}
		return false;
	}

	//bool CTag_vxml::SetMenu(VXML::CExecContext *pCtx, EventPtr pEvt)
	//{
	//	return true;
	//}

	//bool CTag_vxml::SetEvent(CExecContext* pCtx, EventPtr pEvt)
	//{
	//	return true;
	//}

	TagWPtr CTag_vxml::getNextTag(CExecContext* pCtx, const TagWPtr& tag)const
	{
		//if (tag.lock() && tag.lock().get())
		//{
			//TagPtr shrTag = const_cast<CTag*>(tag.lock().get())->shared_from_this();
			if (ifGotBreakingEvent(tag))
			{
				return NULLTAG;
			}
		//}

			if (IsCatchEvent(tag))
			{
				return NULLTAG;
			}

		return __super::getNextTag(pCtx, tag);
	}

	bool CTag_vxml::ifGotBreakingEvent(const TagWPtr & currentTag) const 
	{
		return currentTag.lock() && currentTag.lock()->IsBreakingEvent();
		//if (currentTag.lock() &&
		//	((currentTag.lock()->GetName() == L"menu") ||
		//		(currentTag.lock()->GetName() == L"form")
		//		))
		//	return true;

		//return false;
	}

	bool CTag_value::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		ScopeServicePtr pVar = pCtx->pVar.lock();

		VAR * pInterpretAs = pVar->Find(L"interpret_as");
		VAR * pFormat      = pVar->Find(L"format");
		VAR * pDetail      = pVar->Find(L"detail");

		if (!pInterpretAs)
		{
			pCtx->pLog->LogWarning( L"value:execute: \"interpret_as\" value required");
			return false;
		}

		VXMLString sValue, sExpr;

		// Get URL
		if (GetAttrVal(L"expr",sExpr))
		{
			sValue = pCtx->pVbs->ExprEvalToStr(sExpr);
		}

		if (sValue.empty())
		{
			pCtx->pLog->LogWarning(L"value:execute: \"expr\" param required");
			return false;
		}

		VXMLString sFiles;
		CAtlStringW sInterpretAs = _bstr_t(pInterpretAs->vValue);
		SessionPtr2 pSes = pCtx->pSes.lock();

		bool isTellib = true;

		if (0 == sInterpretAs.CollateNoCase(L"currency"))
		{
			VXMLString sFormat; sFormat.clear();
			if (pFormat)
			{
				sFormat = _bstr_t(pFormat->vValue);
			}
			sFiles = pSes->GetSpeakMoneyFiles(sValue,sFormat);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"date"))
		{
			DWORD dwDate  = 0,
				  dwMonth = 0,
				  dwYear  = 0;
			if (swscanf(sValue.c_str(), L"%u.%u.%u", &dwDate, &dwMonth, &dwYear) != 3)
			{
				EmitErrorSemantic(pCtx, L"A run-time error was found in the VoiceXML document",
					format_wstring(L"CTag_value::Execute: unexpected date format \"%s\", use \"DD:MM:YYYY\" only", sValue.c_str()));
				return false;
			}
			sFiles = pSes->GetSpeakDateFiles(dwDate, dwMonth, dwYear);

		}
		else if (0 == sInterpretAs.CollateNoCase(L"time"))
		{
			DWORD dwHours   = 0,
				  dwMinutes = 0,
				  dwSeconds = 0;
			if (swscanf(sValue.c_str(), L"%u:%u:%u", &dwHours, &dwMinutes, &dwSeconds) != 3)
			{
				EmitErrorSemantic(pCtx, L"A run-time error was found in the VoiceXML document",
					format_wstring(L"CTag_value::Execute: unexpected time format \"%s\", use \"HH:MM:SS\" only", sValue.c_str()));
				return false;

			}
			sFiles = pSes->GetSpeakTimeFiles(dwHours,dwMinutes,dwSeconds);

		}
		else if (0 == sInterpretAs.CollateNoCase(L"numbers"))
		{
			int sex = ssMale;
			if (pDetail && pDetail->vValue.vt & VT_INT)
			{
				sex = Utils::toNum<int>(VXMLString(_bstr_t(pDetail->vValue).GetBSTR()));
			}
			sFiles = pSes->GetSpeakNumberFiles(sValue,sex);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"traffic"))
		{
			sFiles = pSes->GetSpeakTrafficFiles(sValue);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"minutes"))
		{
			sFiles = pSes->GetSpeakMinutesFiles(sValue);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"sms"))
		{
			sFiles = pSes->GetSpeakSMSFiles(sValue);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"acronym"))
		{
			isTellib = false;
		}
		else
		{
			VXMLString sErr = L"The \"interpret-as\" attribute of \"say-as\" element allows \"currency\", \"date\", "
				                 L"\"time\" or \"numbers\" value.";
			EmitErrorSemantic(pCtx, L"A run-time error was found in the VoiceXML document", sErr);

			return false;
		}

		if (isTellib)
		{
			if (sFiles.empty())
			{
				EmitErrorSemantic(pCtx,  L"value:execute: ", L"voice filelist is empty");
				return false;
			}
			pSes->DoPlayWav(sFiles, WaitingEvents(), false, false);
		}
		else
		{
			MenuPtr menu = pSes->GetCurMenu();
			menu->Audiomode = L"t2s";

			//pVar->AddVar(L"audiomode", L"t2s", false);
			pSes->DoSpeakText(sValue, false, false);
		}

		return true;
	}

	void CTag_value::serialize2(CExecContext* pCtx, VXMLString& out)const
	{
		VXMLString sVal;
		if (GetAttrVal(L"expr", sVal))
		{
			sVal = pCtx->pVbs->ExprEvalToStr(sVal);
		}

		out += sVal;
	}

	bool CTag_var::Execute(CExecContext* pCtx, const TagWPtr& pCurTag, TagWPtr& pNextTag)
	{
		CComVariant vVal;
		VXMLString sVal;
		VXMLString sName;

		if (GetAttrVal(L"expr", sVal) && GetAttrVal(L"name", sName))
		{
			//vVal = pCtx->pVbs->ExprEval(sVal);
			pCtx->pLog->LogInfo( L"Exec statement: \"%s = %s\"", sName.c_str(), sVal.c_str());
			if(!SUCCEEDED(pCtx->pVbs->ExecStatement(sName+ L"=" + sVal)))
			{
				pCtx->pLog->LogWarning(L"ExecStatement failed: \"%s = %s\"", sName.c_str(), sVal.c_str());
			}
		}
		return true;
	}
	// CTagFactory implementation

	TagPtr CTagFactory::CreateTag(const VXMLString& sName, CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope)
	{
		const CTagFactory& tagFactory = _doc.GetFactory();

		auto it = tagFactory.m_TagsMap.find(sName);
		if (it == tagFactory.m_TagsMap.cend())
		{
			throw std::runtime_error(std::string("Unknown tag type: ") + wtos(sName));
		}
		return (it->second)(_doc, _parser, pParent, _scope);
	}

	CTagFactory::CTagFactory()
	{
		struct TagFuncs
		{
#define TAG_FUNC(NAME) \
 	static TagPtr tag_##NAME(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) { return std::make_shared<CTag_##NAME>(_doc, _parser, pParent, _scope); }

			TAG_FUNC(assign           )
				TAG_FUNC(audio            )
				TAG_FUNC(block            )
				TAG_FUNC(catch			  )
				TAG_FUNC(choice			  )
				TAG_FUNC(goto			  )
				TAG_FUNC(else             )
				TAG_FUNC(elseif           )
				TAG_FUNC(error            )
				TAG_FUNC(exit             )
				TAG_FUNC(field            )
				TAG_FUNC(filled           )
				TAG_FUNC(form             )
				TAG_FUNC(grammar          )
				TAG_FUNC(if               )
				TAG_FUNC(item             )
				TAG_FUNC(log              )
				TAG_FUNC(media            )
				TAG_FUNC(menu             )
				TAG_FUNC(meta			  )
				TAG_FUNC(metatext		  )
				TAG_FUNC(noinput          )
				TAG_FUNC(nomatch          )
				TAG_FUNC(object           )
				TAG_FUNC(one_of           )
				TAG_FUNC(prompt           )
				TAG_FUNC(property		  )
				TAG_FUNC(record			  )
				TAG_FUNC(reprompt         )
				TAG_FUNC(return			  )
				TAG_FUNC(rule             )
				TAG_FUNC(say_as			  )
				TAG_FUNC(script           )
				TAG_FUNC(send             )
				TAG_FUNC(subdialog        )
				TAG_FUNC(submit   		  )
				TAG_FUNC(transfer         )
				TAG_FUNC(throw   		  )
				TAG_FUNC(value            )
				TAG_FUNC(var              )
				TAG_FUNC(vxml             )
#undef TAG_FUNC

		};

#define ADD_TAG(NAME) \
	m_TagsMap[L#NAME]			= &TagFuncs::tag_##NAME;

		ADD_TAG(assign           )
			ADD_TAG(audio            )
			ADD_TAG(block            )
			ADD_TAG(catch            )
			ADD_TAG(choice           )
			ADD_TAG(goto			 )
			ADD_TAG(else             )
			ADD_TAG(elseif           )
			ADD_TAG(error            )
			ADD_TAG(exit             )
			ADD_TAG(field            )
			ADD_TAG(filled           )
			ADD_TAG(form             )
			ADD_TAG(grammar          )
			ADD_TAG(if               )
			ADD_TAG(item             )
			ADD_TAG(log              )
			ADD_TAG(media            )
			ADD_TAG(menu             )
			ADD_TAG(meta			 )
			ADD_TAG(metatext		 )
			ADD_TAG(noinput          )
			ADD_TAG(nomatch          )
			ADD_TAG(object           )
			//ADD_TAG(one_of			 )
			ADD_TAG(prompt           )
			ADD_TAG(property		 )
			ADD_TAG(record			 )
			ADD_TAG(reprompt         )
			ADD_TAG(return			 )
			ADD_TAG(rule             )
			//ADD_TAG(say_as			 )
			ADD_TAG(script           )
			ADD_TAG(send             )
			ADD_TAG(subdialog		 )
			ADD_TAG(submit           )
			ADD_TAG(transfer         )
			ADD_TAG(throw            )
			ADD_TAG(value            )
			ADD_TAG(var              )
			ADD_TAG(vxml             )
#undef ADD_TAG

			m_TagsMap[L"say-as"] = &TagFuncs::tag_say_as;
			m_TagsMap[L"one-of"] = &TagFuncs::tag_one_of;

	}
}