/************************************************************************/
/* Name     : VXMLCom\VXMLDocument.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 15 Dec 2009                                               */
/************************************************************************/
#pragma once

#include "VXMLBase.h"
#include "VXMLTags.h"
#include <list>
#include <map>
#include "boost/shared_ptr.hpp"

#include "HashMap.h"
#include "VXMLMenu.h"

namespace VXML
{
	class CDocument
	{
	private:
		const CTagFactory m_TagFactory;
		MenuPtr mFirstMenu;
		std::unordered_map<DoubleStringKey, MenuPtr> mAllTags;
		TagPtr m_pRootTag;

		CEvtScopeCollector::EventScope m_GlobalEvtScope;

	public:
		// throws std::exception, _com_error
		CDocument(const Factory_c::CollectorPtr& _parser, CEvtScopeCollector& _scope);
		~CDocument();
		void Execute(CExecContext* pCtx);
		TagPtr GetEnterPoint() const { return m_pRootTag; }
		//TagWPtr GetMenuTag(const VXMLString& menu_name, const VXMLString& fieldName);

		//MenuPtr GetMenu(const VXMLString& aMenuName, const VXMLString& aFieldName);
		//MenuPtr GetFirstMenu() const;
		MenuPtr CreateNextMenu(const VXMLString& aMenuName, const VXMLString& aFieldName) const;
		MenuPtr CreateNextMenu() const;

		void EmplaceMenuTag(const DoubleStringKey& aDoubleStringKey, TagWPtr aTagWPtr);

		const CTagFactory& GetFactory() const { return m_TagFactory; }

		//void SetGlobalEvtScope(const CEvtScopeCollector::EventScope& aEventScope)
		//{
		//	m_GlobalEvtScope = aEventScope;
		//}
		//CEvtScopeCollector::EventScope GetGlobalEvtScope() const { return m_GlobalEvtScope; }

	private:
		MenuPtr _getMenu(const VXMLString& aMenuName, const VXMLString& aFieldName) const;
	};
}

/******************************* eof *************************************/