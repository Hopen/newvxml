/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLEngineEvent.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/
#include "..\StdAfx.h"
#include "VXMLEngineEvent.h"
#include "VXMLTags.h"

// For PathMatchSpecW
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

namespace VXML
{
	CEngineEvent::CEngineEvent(const CMessage& msg)
		: CNameTable(msg)
		, mName(msg.Name)
	{
		//m_typeNameTable = L"CEngineEvent";
		// Convert message to event
		this->Add(L"name", CComVariant(msg.Name), true);
		//const ParamsList& parlist = msg.Params();
		//ParamsList::const_iterator it = parlist.begin();
		//for (; it != parlist.end(); ++it)
		//{
		//	const CParam& p = *it;
		//	this->Add(p.Name, p.Value, true);
		//}
	}

	//CMessage CEngineEvent::ToMsg() const
	//{
	//	// Convert event to message
	//	CMessage msg(__super::ToMsg());
	//	msg.Name = mName.c_str();
	//	return msg;
	//}

	bool CEngineEvent::MatchMask(const VXMLString& sMask)
	{
		// �� ������ ��� ���� �����-�� ���� ��������� ����� ������.
		// ��� ����� ��������, �� ������� ���� �� �������� ��������
		return ::PathMatchSpecW(mName.c_str(), sMask.c_str()) != 0;
	}

	//const VXMLString& CEngineEvent::GetName() const
	//{
	//	return mName;
	//}

	//bool CEngineEvent::isRecursiveEvent()const
	//{
	//	return mType == E_EVENT_TYPE::ET_RECURSIVE;
	//}

	//CMessage CEngineEvent::Dump()const
	//{
	//	auto msg = ToMsg();
	//	msg[L"_state"] = StateString(GetState());

	//	if (!mCurTag.expired())
	//	{
	//		auto curtag = mCurTag.lock();
	//		msg[L"_tag"] = curtag->ToString();
	//	}

	//	return msg;
	//}

	//bool CEngineEvent::isPredefineEvent() const
	//{
	//	return mType == E_EVENT_TYPE::ET_PREDEFINE;
	//}

	//void CEngineEvent::Invalidate()
	//{
	//	mName = L"Invalid";
	//}

	//VXMLString CEngineEvent::FindWaitingEvent(const VXMLString& _event)const
	//{
	//	auto NameIt = std::find(m_eventsList.begin(), m_eventsList.end(), _event);
	//	if (NameIt != m_eventsList.end())
	//		return *NameIt;
	//	return L"";
	//}

	bool CItemEvent::MatchMask(const VXMLString& sMask)
	{
		// �� ������ ��� ���� �����-�� ���� ��������� ����� ������.
		// ��� ����� ��������, �� ������� ���� �� �������� ��������
		return ::PathMatchSpecW(mName.c_str(), sMask.c_str()) != 0;
	}

	std::wostream& operator<< (std::wostream& os, E_EVENT_TYPE aType)
	{
		switch (aType)
		{
		case E_EVENT_TYPE::ET_UNKNOWN:
		{
			os << L"Unknown";
			break;
		}
		case E_EVENT_TYPE::ET_SUB:
		{
			os << L"Sub";
			break;
		}
		case E_EVENT_TYPE::ET_RECURSIVE:
		{
			os << L"Recursive";
			break;
		}
		case E_EVENT_TYPE::ET_PREDEFINE:
		{
			os << L"Predefine";
			break;
		}
		default:
		{
			os << L"???";
			break;
		}
		}

		return os;
	}

	VXMLString CItemEvent::Dump() const
	{
		std::wostringstream stream;
		stream << L"CItemEvent {"
			<< L"Name: " << mName << L", "
			<< L"Type: " << mType << L", "
			<< L"AttrId: " << mAttrId << L"}";

		return stream.str();
	}

	VXMLString CMenuEvent::Dump() const
	{
		std::wostringstream stream;
		stream << L"MenuEvent {"
			<< L"Name: " << mName << L", "
			<< L"Type: " << mType << L", "
			<< L"AttrId: " << mAttrId << L"}";

		return stream.str();
	}

	VXMLString CDocEvent::Dump() const
	{
		std::wostringstream stream;
		stream << L"DocEvent {"
			<< L"Uri: " << mUri << L", "
			<< L"Type: " << mType << L", "
			<< L"AttrId: " << mAttrId << L"}";

		return stream.str();
	}

	VXMLString CTagEvent::Dump() const
	{
		std::wostringstream stream;
		stream << L"TagEvent {"
			<< L"Name: " << mName << L", "
			<< L"Type: " << mType << L", "
			<< L"AttrId: " << mAttrId << L"}";

		return stream.str();
	}

}
