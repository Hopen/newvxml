/************************************************************************/
/* Name     : VXMLCom\VXMLResCache.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 1 Dec 2009                                                */
/************************************************************************/

#pragma once

#include "..\StdAfx.h"
#include "EvtModel.h"
#include "VXMLResCache.h"
#include "VXMLDocument.h"
#include "VXMLSession.h"
#include "..\CacheInterface\CacheInterface_i.c"

namespace VXML
{
	CResourceCache::CResourceCache()
	{
		m_hr = m_Res.CoCreateInstance(/*L"CacheInterface.ResourceCache.1"*/CLSID_ResourceCache);

		//m_Test.CoCreateInstance(CLSID_TestMTObj);
		//HRESULT hr = m_basedRes.CoCreateInstance(/*L"CacheInterface.ResourceCache.1"*/CLSID_ResourceCache);
		//if (SUCCEEDED(hr))
		//{
		//	hr = CoMarshalInterThreadInterfaceInStream(IID_IResourceCache, m_basedRes, &pStream);
		//}
		//m_Res = NULL;
		////if (FAILED(hr))
		////{
		////	throw format_wstring(L"Error: Cannot marshaling an interface pointer to CLSID_ResourceCache (%s, %08x)", SVUtils::WinErrToStr(hr).c_str(), hr);
		////}

		////m_Res.CoCreateInstance(/*L"CacheInterface.ResourceCache.1"*/CLSID_ResourceCache);

	}

	CResourceCache::~CResourceCache()
	{
	}


	//MSXML2::IXMLDOMNodePtr CResourceCache::GetDocument(const VXMLString& sSrc, const bool& bFile)
	//{
	//	if (!m_Res)
	//		return NULL;

	//	/*HRESULT hr = S_OK;
	//	VARIANT val;
	//	val.vt = VT_DISPATCH;
	//	val.pdispVal = NULL;*/

	//	//CComPtr<IResourceCache> iRes; 
	//	//int test = 0;
	//	//if (test)
	//	//{
	//	//	CoGetInterfaceAndReleaseStream(pStream, IID_IResourceCache, (void**)&iRes);
	//	//	pStream.Detach();

	//	//	std::wstring sSchemaFile; sSchemaFile.clear();// = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"ccxml.xsd";
	//	//	if (SUCCEEDED(hr = iRes->GetDocument(_bstr_t(sSrc.c_str()),_bstr_t(sSchemaFile.c_str()),bFile,&val)))
	//	//	{
	//	//		return val.pdispVal;
	//	//	}
	//	//}

	//	HRESULT hr = S_OK;
	//	//BSTR bstrSerializeXMLDocument = L"";
	//	CComBSTR sbsOut;

		//	std::wstring sSchemaFile; sSchemaFile.clear();// = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"ccxml.xsd";
	//	if (SUCCEEDED(hr = m_Res->GetDocument(_bstr_t(sSrc.c_str()),_bstr_t(sSchemaFile.c_str()),bFile,&sbsOut)))
		//	{
	//		return NULL;
	//		//return val.pdispVal;
		//	}
	//	DWORD dwErr = ::GetLastError();
	//	hr = HRESULT_FROM_WIN32(dwErr);
	//	return NULL;
		//}

	//VXMLString CResourceCache::GetDocument(const VXMLString& sSRC, const bool& bFile)
	//{
	//	if (!m_Res)
	//		return NULL;

	//	HRESULT hr = S_OK;
	//	CComBSTR sbsOut;
	//	std::wstring sSchemaFile; sSchemaFile.clear();// = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"ccxml.xsd";
	//	/*if (SUCCEEDED(hr = m_Res->GetDocument(_bstr_t(sSRC.c_str()),_bstr_t(sSchemaFile.c_str()),bFile,&sbsOut)))
	//	{
	//		return sbsOut.Detach();
	//	}*/
	//	DWORD dwErr = ::GetLastError();
	//	hr = HRESULT_FROM_WIN32(dwErr);
	//	return L"";
	//}

	HRESULT CResourceCache::GetDocument(const VXMLString& sSrc, const VXMLString& sScriptID, const bool& bFile, SAFEARRAY** pSA)
	{
		std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";
		if (!m_Res)
			return E_FAIL;
		return m_Res->GetDocument(_bstr_t(sSrc.c_str()), _bstr_t(sSchemaFile.c_str()), _bstr_t(sScriptID.c_str()), bFile, pSA);
	}


	//bool CResourceCache::AttachResourceInterface()
	//{
	//	HRESULT hr = CoGetInterfaceAndReleaseStream(pStream, IID_IResourceCache, (void**)&m_Res);
	//	if (FAILED(hr))
	//	{
	//		//throw format_wstring(L"Error: Cannot unmarshal stream from object CLSID_ResourceCache (%s, %08x)", SVUtils::WinErrToStr(hr).c_str(), hr);
	//		pStream.Detach();
	//		return false;
	//	}
	//	pStream.Detach();
	//	return true;
	//}

	bool CResourceCache::Ready()const
	{
		return !!m_Res.p;
	}

	//bool CResourceCache::BaseReady()const
	//{
	//	return !!m_basedRes.p;
	//}
	//CDocument* CResourceCache::GetDocument(const VXMLString& sURI)
	//{
	//	return NULL;
	//}

	//void CResourceCache::PutDocument(CDocument* pDoc)
	//{

	//}

	bool CResourceCache::TestReady()const
	{
		return !!m_Test.p;
	}

	HRESULT CResourceCache::WriteTestLog(const VXMLString& sSrc)
	{
		if (!m_Test)
			return E_FAIL;
		m_Test->WriteLog(_bstr_t(sSrc.c_str()));
		return S_OK;
	}

	HRESULT CResourceCache::ParseT2SMessage(const VXMLString& sSrc, SAFEARRAY** pSA)
	{
		if (!m_Res)
			return E_FAIL;
		return m_Res->ParseT2SMessage(_bstr_t(sSrc.c_str()), pSA);
	}
}
