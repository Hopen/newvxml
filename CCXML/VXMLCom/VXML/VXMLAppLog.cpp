#include "stdafx.h"
#include "VXMLAppLog.h"
//#include "..\Engine\EngineLog.h"
#include "..\Common\ExtraLog.h"

//using namespace enginelog;

namespace VXML
{
	enum class ID_FUNCTIONS
	{
		INIT = 1,
		LOG
	};

	CAppLog::CAppLog(const ExtraLogPtr& _log): m_log(_log)
	{

	}

	HRESULT CAppLog::init(const VXMLString& uri)
	{
		if (m_log)
		{
			m_log->InitDefault(uri);
		}

		return S_OK;
	}

	HRESULT CAppLog::log(const VXMLString &log_str)
	{
		if (m_log)
		{
			m_log->LogInfo(L"%s", log_str.c_str());
		}
		else
		{
			return E_NOT_SET;
		}
		return S_OK;
	}


	STDMETHODIMP CAppLog::GetTypeInfoCount(
		/* [out] */ UINT *pctinfo)
	{
		if (::IsBadWritePtr(pctinfo, sizeof(*pctinfo)))
		{
			return E_INVALIDARG;
		}

		*pctinfo = 0;
		return S_OK;
	}

	STDMETHODIMP CAppLog::GetTypeInfo(
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ ITypeInfo **ppTInfo)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP CAppLog::GetIDsOfNames(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId)
	{
		// TODO: Add your implementation code here

		HRESULT hRes = S_OK;
		
		for (UINT i = 0; i < cNames; i++)
		{
			if ( 0 == CAtlStringW(*rgszNames).CompareNoCase(L"log"))
			{
				rgDispId[i] = static_cast<int>(ID_FUNCTIONS::LOG);
			}
			else if (0 == CAtlStringW(*rgszNames).CompareNoCase(L"init"))
			{
				rgDispId[i] = static_cast<int>(ID_FUNCTIONS::INIT);
			}
			else
				rgDispId[i] = DISPID_UNKNOWN;
		}
		return hRes;
	}

	STDMETHODIMP CAppLog::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr)
	{
		struct Error
		{
			HRESULT hr;
			VXMLString description;
			Error(HRESULT _hr, LPCWSTR pwszDescr)
				: hr(_hr), description(pwszDescr)
			{
			}
		};

		HRESULT hr = E_FAIL;

		try
		{
			// Trying to get first arg - arguments are stored in reverse order
			VARIANT* pName = &pDispParams->rgvarg[pDispParams->cArgs - 1];
			if (::IsBadReadPtr(pName, sizeof(VARIANT)))
			{
				*puArgErr = 0;
				throw Error(E_INVALIDARG, L"Invalid parameter value for Name");
			}

			CComVariant vName(*pName);
			// Trying to cast item name to BSTR
			if (vName.ChangeType(VT_BSTR) != S_OK)
			{
				throw Error(E_INVALIDARG, L"Invalid parameter type for Name");
			}

			switch (static_cast<ID_FUNCTIONS>(dispIdMember))
			{
			case ID_FUNCTIONS::INIT:
			{
				init(vName.bstrVal);
				break;
			}
			case ID_FUNCTIONS::LOG:
			{
				log(vName.bstrVal);
				break;
			}
			}

			hr = S_OK;
		}
		catch (Error& err)
		{
			// Check pExcepInfo structure pointer
			if (::IsBadReadPtr(pExcepInfo, sizeof(EXCEPINFO)))
			{
				hr = DISP_E_EXCEPTION;
			}

			// Fill pExcepInfo structure

			ZeroMemory(pExcepInfo, sizeof(EXCEPINFO));

			pExcepInfo->bstrDescription = ::SysAllocString(err.description.c_str());
			pExcepInfo->bstrSource = ::SysAllocString(L"");
			pExcepInfo->bstrHelpFile = ::SysAllocString(L"");
			pExcepInfo->scode = err.hr;

			hr = err.hr;
		}
		catch (...)
		{
			hr = DISP_E_EXCEPTION;
		}

		//VARIANT var;
		//var.vt = VT_INT;
		//var.intVal = 0;

		//::VariantCopy(pVarResult, &var);

		return hr;
	}
}