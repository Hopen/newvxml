/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLTags.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/

#pragma once

#include <map>
#include <list>
#include <set>

#include "VXMLBase.h"
#include "EvtModel.h"
#include "VXMLEngineEvent.h"
#include "VXMLGrammar.h"
#include "..\Common\factory_c.h"
#include "VXMLEventScope.h"
//#include "Patterns/singleton.h"

//#include "..\Engine\EngineLog.h" // Delete this Test123

namespace VXML
{
	class CTag_assign           ;
	class CTag_audio			;
	class CTag_block			;
	class CTag_catch            ;
	class CTag_choice           ;
	class CTag_else             ;
	class CTag_elseif           ;
	class CTag_error            ;
	class CTag_exit				;
	class CTag_goto				;
	class CTag_field			;
	class CTag_filled			;
	class CTag_form				;
	class CTag_grammar			;
	class CTag_if               ;
	class CTag_item				;
	class CTag_log              ;
	class CTag_media			;
	class CTag_menu				;
	class CTag_meta				;
	class CTag_metatext			;
	class CTag_noinput          ;
	class CTag_nomatch			;
	class CTag_object			;
	class CTag_one_of			;
	class CTag_prompt			;
	class CTag_property			;
	class CTag_record			;
	class CTag_reprompt         ;
	class CTag_return			;
	class CTag_rule				;
	class CTag_say_as			;
	class CTag_script           ;
	class CTag_send             ;
	class CTag_subdialog		;
	class CTag_submit			;
	class CTag_transfer			;
	class CTag_throw			;
	class CTag_value    		;
	class CTag_var              ;
	class CTag_vxml				;

	//class CEvtScopeCollector;

	class CParentTag;
	class CExecutableTag;

	class CTag : public Factory_c::CFTag, public std::enable_shared_from_this <CTag>
	{
	protected:
		CComVariant EvalAttr(
			CExecContext* pCtx, 
			const VXMLString& sAttrName, 
			const CComVariant& defVal, 
			VARTYPE evalType);

		CComVariant EvalAttrEvent(
			CExecContext* pCtx, 
			const VXMLString& sAttrName, 
			VARTYPE evalType);

	public:
		CTag(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual ~CTag();//{}
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag) = 0;
		virtual bool ExecuteWithLog(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		//void EmitError(CExecContext* pCtx, CMessage& evt, const VXMLString& description, const VXMLString& reason);
		static void EmitErrorBadfetch(
			CExecContext* aCtx, 
			const VXMLString& aReason, 
			const VXMLString& aTagName,
			const VXMLString& aDescription);
		void EmitErrorSemantic(CExecContext* pCtx, const VXMLString& reason, const VXMLString& description);
		void EmitErrorUnrecoverable(CExecContext* pCtx, const VXMLString& reason, const VXMLString& description) const;
		virtual bool IsBreakingEvent() const { return false; };
		virtual VXMLString GetAttrIdName()const 
		{
			//throw std::logic_error("Wrong GetAttrIdName call");
			return L"";
		}

		virtual TagWPtr ExecEvents(const CExecContext* aCtx, const CMessage& aEvent)
		{
			throw std::logic_error("Wrong ExecEvents call");
		}

		//virtual void AddScope(CEvtScopeCollector& /*_scope*/) {};
		virtual void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) {};

		bool CheckDigits(CExecContext* pCtx);

		//virtual CMessage CreatePlayWavMessage(CExecContext* pCtx, const VXMLString& _uri);

		virtual void serialize2(CExecContext* pCtx, VXMLString& out)const;
		virtual VXMLString onXMLStart()const;
		virtual VXMLString onXMLEnd()const;

		void SetNextTag(const TagWPtr& aPtr) { m_pNextTag = aPtr; }
		const TagWPtr& _getNextTag() const { return m_pNextTag; }
	protected:
		virtual void _logTag(CExecContext* pCtx)const ;
		virtual const CExecutableTag* GetExecParent()const
		{
			return nullptr;
		}

	protected:
		CParentTag* m_pParent = nullptr;
		TagWPtr m_pNextTag;

	private:
		virtual VXMLString GetAttrIdNameSafe() const = 0;
	};



	class CParentTag : public CTag
	{
	public:
		using TagContainer = std::list<TagPtr>;

	protected:
		TagContainer				m_Children;
		TagContainer				m_filteredChildren;

		mutable bool				m_endTag = false;

		//TagContainer				m_CatchEvents; ToDo!!!

	public:
		void Init() { m_endTag = false; }

		CParentTag(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		~CParentTag();
	public:

		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		virtual bool ExecuteWithLog(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		virtual TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const;

		virtual bool onBeginTag(CExecContext* pCtx/*, TagWPtr& pNextTag*/);
		virtual bool onEndTag  (CExecContext* pCtx/*, TagWPtr& pNextTag*/);

	protected:
		virtual bool isInitialized(CExecContext* pCtx) { return true; }
		virtual bool isFinitialized(CExecContext* pCtx) { return true; }

		virtual bool ifGotBreakingEvent(const TagWPtr& currentTag)const  { return false; }
		virtual bool MakeBreakingEvent(CExecContext* pCtx, const TagWPtr& pTag)const { return false; }

		virtual void serialize2(CExecContext* pCtx, VXMLString& out)const;
		virtual VXMLString onXMLStart()const;
		virtual VXMLString onXMLEnd()const;

		template <class T> 
		void SetFilter(T&& _filter)
		{
			m_filteredChildren.clear();

			TagWPtr prevTag;
			for (TagContainer::reverse_iterator rit = m_Children.rbegin(); rit != m_Children.rend(); ++rit)
			{
				TagPtr& tagPtr = *rit;
				if (_filter(tagPtr))
				{
					tagPtr->SetNextTag(prevTag);
					prevTag = tagPtr;
					m_filteredChildren.emplace_front(tagPtr);
				}
				else
				{
					tagPtr->SetNextTag(NULLTAG);
				}
			}
		}

		template <class T>
		void SetFilter2(T&& _filter)
		{
			m_filteredChildren.clear();

			for (auto& aTagPtr : m_Children)
			{
				if (_filter(aTagPtr))
				{
					m_filteredChildren.emplace_back(aTagPtr);
				}

				aTagPtr->SetNextTag(NULLTAG);
			}

			TagWPtr prevTag;
			for (TagContainer::reverse_iterator rit = m_filteredChildren.rbegin(); rit != m_filteredChildren.rend(); ++rit)
			{
				TagPtr& tagPtr = *rit;
				tagPtr->SetNextTag(prevTag);
				prevTag = tagPtr;
			}
		}

		virtual const CExecutableTag* GetExecParent()const override
		{
			if (m_pParent)
			{
				return m_pParent->GetExecParent();
			}
			return nullptr;
		}

	protected:
		const TagContainer& GetChildren() const  { return m_filteredChildren; };

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTextTag : public CTag
	{
	public:
		CTextTag() = default;
		CTextTag(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope){};

		virtual void serialize2(CExecContext* pCtx, VXMLString& out)const;
		virtual VXMLString onXMLStart()const;
		virtual VXMLString onXMLEnd()const;

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	//class CEvtScope
	//{
	//public:
	//	CEvtScope():m_iCount(0),m_pExecTag(NULL){}
	//	CEvtScope(const VXMLString& _name,
	//		const size_t& _count,
	//		const VXMLString& _cond,
	//		CTag* const _tag, 
	//		const VXMLString& _url,
	//		const VXMLString& _eventVariableName) 
	//		: m_sName(_name)
	//		, m_iCount(_count)
	//		, m_sCond(_cond)
	//		, m_pExecTag(_tag)
	//		, m_sUrl(_url)
	//		, m_sEventVairiableName(_eventVariableName)
	//	{}
	//public:
	//	VXMLString GetEventName()const {return m_sName;}
	//	size_t GetCount()const{return m_iCount;}
	//	const VXMLString& GetCond() const { return m_sCond; }
	//	bool ExecCond(const CExecContext* pCtx) const;
	//	VXMLString GetDocUri()const { return m_sUrl; }
	//	TagWPtr GetExecTag()const{ return m_pExecTag->shared_from_this(); }
	//	VXMLString GetEventVairiableName() const { return m_sEventVairiableName; }

	//	CTag* GetTag()const {return m_pExecTag;}//Test123 delete this
	//private:
	//	VXMLString m_sName;  // event name
	//	size_t     m_iCount; // existing counter
	//	VXMLString m_sCond;  // executing condition
	//	VXMLString m_sUrl;   // document name
	//	VXMLString m_sEventVairiableName; // variable name
	//	CTag*      m_pExecTag;
	//};



	class CExecutableTag: public CParentTag 
	{
	public:
		CExecutableTag(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
	protected:
		CEvtScopeCollector::EventScope m_EvtScope;
		CEvtScopeCollector::EventScope m_Prompts;

		virtual const CExecutableTag* GetExecParent()const override
		{
			return this;
		}

		//class CFindCurPrompt
		//{
		//public:
		//	CFindCurPrompt(const int& cur_count):m_iCurCount(cur_count)
		//	{}
		//	bool operator()(const CEvtScope& scope)
		//	{
		//		return (scope.GetCount()==m_iCurCount);
		//	}

		//private:
		//	int m_iCurCount;
		//};

	public:
		bool MatchPrompt(TagPtr promptTag, CExecContext* pCtx) const;
		//bool Match(CExecContext* pCtx);

		TagWPtr ExecEvents(
			const CExecContext* aCtx, 
			const CMessage& aEvent) override;
	};


	// TAGS
	class CTag_audio: public CTag
	{
	public:
		CTag_audio(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		~CTag_audio();
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};

		RecognizeInfo mEmptyRecognizeInfo;
	};

	class CTag_block: public CParentTag
	{
	public:

		CTag_block(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CParentTag(_doc, _parser, pParent, _scope) { }

		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_catch: public CParentTag
	{
	public:
		CTag_catch(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_choice: public CParentTag
	{
	public:
		CTag_choice(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : /*CTag*/CParentTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

		VXMLString MatchDTMF(CExecContext* pCtx);
		bool onBeginTag(CExecContext* pCtx);
		bool onEndTag(CExecContext* pCtx/*, TagWPtr& pNextTag*/) override;
	protected:
		bool isInitialized(CExecContext* pCtx) override;

	};

	class CTag_else : public CTag
	{
	public:
		CTag_else(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_elseif : public CTag
	{
	public:
		CTag_elseif(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_error: public CParentTag
	{
	public:
		CTag_error(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};


	class CTag_exit: public CTag
	{
	public:
		CTag_exit(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_field: public CExecutableTag
	{
	public:
		CTag_field(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		virtual VXMLString GetAttrIdName()const override;

		bool isFinitialized(CExecContext* pCtx) override;
		bool onBeginTag(CExecContext * pCtx);
		bool onEndTag(CExecContext* pCtx) override;

		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

		//void AddScope(CEvtScopeCollector& /*_scope*/) override;
		void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) override;

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return GetAttrVal(L"name");
		};
	};

	class CTag_filled: public CParentTag
	{
	public:
		CTag_filled(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		//VXMLString Match(CExecContext* pCtx, VAR* pGrammar);
		//bool MatchDialogEnd();
	};

	class CTag_form: public CExecutableTag
	{
	public:
		CTag_form(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		void SetCurFieldName(const VXMLString& _field){m_sCurField = _field;}
		VXMLString GetCurFieldName()const {return m_sCurField;}
		//TagPtr GetField(const VXMLString& _name);

		virtual VXMLString GetAttrIdName()const override;

		bool onBeginTag(CExecContext* pCtx);
		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

		bool IsBreakingEvent() const override { return true; };

		//void AddScope(CEvtScopeCollector& /*_scope*/) override;
		void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) override;

	protected:
		bool ifGotBreakingEvent(const TagWPtr& currentTag)const  override;
		bool MakeBreakingEvent(CExecContext* pCtx, const TagWPtr& pTag)const override;
	private:
		VXMLString m_sCurField;

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return GetAttrVal(L"id");
		};
	};

	class CTag_goto:public CTag
	{
	public:
		CTag_goto(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	private:

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_grammar:public CParentTag
	{
	public:
		CTag_grammar(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CParentTag(_doc, _parser, pParent, _scope)/*,m_pGrammar(NULL)*/ { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		bool onBeginTag(CExecContext* pCtx);
	};

	class CTag_if : public CParentTag
	{
	public:
		CTag_if(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);// : CParentTag(_parser, pParent) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	protected:
		bool isInitialized(CExecContext* pCtx) override;
	};
	
	class CTag_item : public CParentTag
	{
	public:
		CTag_item(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_log : public CTag
	{
	public:
		CTag_log(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_media: public CTag
	{
	public:
		CTag_media(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};


	class CTag_menu: public CExecutableTag
	{
		// definitions
	public:
		CTag_menu(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		bool onBeginTag(CExecContext* pCtx);
		bool onEndTag(CExecContext* pCtx) override;
		VXMLString GetAttrIdName()const override;

		bool IsBreakingEvent() const override { return true; };

		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

		//void AddScope(CEvtScopeCollector& /*_scope*/) override;
		void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) override;

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return GetAttrVal(L"id");;
		};
	};

	class CTag_meta : public CTextTag
	{
	public:
		CTag_meta(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTextTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_metatext : public virtual CTag
	{
	public:
		CTag_metatext(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope)
		{}
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_noinput: public CParentTag
	{
	public:
		CTag_noinput(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_nomatch: public CParentTag
	{
	public:
		CTag_nomatch(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_object: public CTag
	{
	public:
		CTag_object(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_one_of : public CParentTag
	{
	public:
		CTag_one_of(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_prompt: public CParentTag
	{
	public:
		CTag_prompt(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		bool onBeginTag(CExecContext* pCtx);
	};

	class CTag_property: public CTag
	{
	public:
		CTag_property(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_record: public CExecutableTag
	{
	public:
		CTag_record(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CExecutableTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		bool GetRecord(CExecContext* pCtx);

		VXMLString GetAttrIdName()const override;

		//void AddScope(CEvtScopeCollector& /*_scope*/) override;
		void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) override;

		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return GetAttrVal(L"name");
		};
	};

	class CTag_reprompt:public CTag
	{
	public:
		CTag_reprompt(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_return: public CTag_exit
	{
	public:
		CTag_return(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : /*CTag*/CTag_exit(_doc, _parser, pParent, _scope) { }
	};

	class CTag_rule : public CParentTag
	{
	public:
		CTag_rule(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_say_as: public CParentTag
	{
	public:
		CTag_say_as(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope): CParentTag(_doc, _parser, pParent, _scope) {}
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

		bool onBeginTag(CExecContext * pCtx)override;
	};

	class CTag_script : public CTextTag
	{
	public:
		CTag_script(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTextTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_send : public CTag
	{
	public:
		CTag_send(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_subdialog: public CExecutableTag
	{
	public:
		CTag_subdialog(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

		VXMLString GetAttrIdName()const override;

		bool isFinitialized(CExecContext* pCtx) override;
		bool onBeginTag(CExecContext * pCtx)override;
		bool onEndTag(CExecContext* pCtx) override;

		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

		//void AddScope(CEvtScopeCollector& /*_scope*/) override;
		void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) override;

	private:
		bool StartDialog(CExecContext* pCtx);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return GetAttrVal(L"name");
		};
	};

	class CTag_submit: public CTag
	{
	public:
		CTag_submit(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_transfer: public CExecutableTag
	{

	public:
		CTag_transfer(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		VXMLString GetAttrIdName()const override;

		bool isFinitialized(CExecContext* pCtx) override;
		bool onBeginTag(CExecContext * pCtx) override;
		bool onEndTag(CExecContext* pCtx) override;

		//void AddScope(CEvtScopeCollector& /*_scope*/) override;
		void AddToDocument(const VXMLString& aParentAttrId, CDocument& /*_doc*/) override;

		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return GetAttrVal(L"name");
		};

		TransferInfo mTransferInfo;
	};

	class CTag_throw:public CParentTag
	{
	public:
		CTag_throw(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CParentTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
	};

	class CTag_value: public CTag_audio
	{
	public:
		CTag_value(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) 
			: CTag_audio(_doc, _parser, pParent, _scope)
		{}
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	protected:
		void serialize2(CExecContext* pCtx, VXMLString& out)const;
	private:
		RecognizeInfo mEmptyRecognizeInfo;
	};

	class CTag_var : public CTag
	{
	public:
		CTag_var(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag(_doc, _parser, pParent, _scope) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);

	private:
		VXMLString GetAttrIdNameSafe() const override
		{
			return L"";
		};
	};

	class CTag_vxml: public CExecutableTag/*CParentTag*/
	{
	public:
		CTag_vxml(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);// : CParentTag(_parser, pParent) { }
		virtual bool Execute(CExecContext* pCtx, const TagWPtr& pCurtag, TagWPtr& pNextTag);
		//bool SetMenu(CExecContext* pCtx, EventPtr pEvt);
		//bool SetEvent(CExecContext* pCtx, EventPtr pEvt);

		TagWPtr getNextTag(CExecContext* pCtx, const TagWPtr& tag)const override;

		virtual VXMLString GetAttrIdName() const override
		{
			//throw std::runtime_error("Tag \"VXML\" runtime GetAttrIdName call");
			return L"";
		}

	protected:
		bool MakeBreakingEvent(CExecContext* pCtx, const TagWPtr& pTag)const override;
		bool ifGotBreakingEvent(const TagWPtr& currentTag)const  override;

	};


	class CTag_assign : public CTag_var
	{
	public:
		CTag_assign(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope) : CTag_var(_doc, _parser, pParent, _scope) { }
	};

	// CLASS FACTORY
	class CTagFactory
	{
	private:
		using TagFunc = TagPtr(*)(CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
		using TagsMap = std::map<VXMLString, TagFunc>;

		TagsMap m_TagsMap;

	public:
		CTagFactory::CTagFactory();

		static TagPtr CreateTag(const VXMLString& sName, CDocument& _doc, const Factory_c::CollectorPtr& _parser, CParentTag* pParent, CEvtScopeCollector& _scope);
	};
}

/******************************* eof *************************************/