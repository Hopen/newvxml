/************************************************************************/
/* Name     : VXMLCom\VXMLSession.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Nov 2009                                               */
/************************************************************************/
#include "..\StdAfx.h"
#include <algorithm>
#include <time.h>
//#include <boost\archive\binary_iarchive.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "sv_strutils.h"
#include "common.h"
#include "BinaryFile.h"
//#include "..\Engine\EngineLog.h"
#include "..\Common\alarmmsg.h"
//#include "Log\SystemLogEx.h"
#include "KeepAliveProvider.h"
#include "VXMLBase.h"
#include "VXMLSession.h"
#include "VXMLResCache.h"
#include "VXMLDocument.h"
#include "VXMLEngine.h"
#include "VXMLDebug.h"
#include "VXMLHTTPClient.h"
//#include "StringFunctions.h"
#include "Patterns\\string_functions.h"
#include "..\Common\SystemLogEx.h"
#include "..\Common\ExtraLog.h"


//using namespace enginelog;

const size_t DEADTIMER_LIMIT = 2;

enum ECompletionCause
{
	CC_Success = 0,
	CC_Nomatch,
	CC_Noinput,
	CC_RecognitionTimeout
};

namespace VXML
{
	static std::vector<VXMLString> GetSplitedFiles(const VXMLString& aNamelist)
	{
		std::vector <VXMLString> _files;

		boost::split(_files, aNamelist, std::bind2nd(std::equal_to<wchar_t>(), ';'));
		_files.erase(std::remove_if(_files.begin(), _files.end(),
			[](const VXMLString& _uri)
		{
			if (_uri.empty())
				return true;
			return false;
		}
		), _files.end());
		return _files;
	}

	static void AddNamelistToMsg(
		CExecContext* aCtx,
		const VXMLString& aNamelist,
		const NamelistContainer& aNameListParams,
		CMessage& outMsg)
	{
		outMsg[L"namelist"] = aNamelist.c_str();

		for (const auto& param : aNameListParams)
		{
			if (param.empty())
			{
				continue;
			}

			outMsg[param.c_str()] = aCtx->pVbs->ExprEval(param);
		}
	}

	static void AddNamelistToMsg(
		CExecContext* aCtx,
		const VXMLString& aNamelist,
		CMessage& outMsg)
	{
		if (aNamelist.empty())
		{
			return;
		}

		NamelistContainer namelist;
		std::wistringstream in(aNamelist);
		std::istream_iterator<VXMLString, wchar_t> first(in), last;
		std::copy(first, last, std::back_inserter(namelist));

		AddNamelistToMsg(aCtx, aNamelist, namelist, outMsg);
	}

	/****************************************************************************************************/
	/*                                          CStateDispatcher                                        */
	/****************************************************************************************************/

	class CStateDispatcher
	{
	public:
		CStateDispatcher(bool aLogGetStep,
			SystemLogPtr aLog)
			: m_logGetStep(aLogGetStep)
			, m_log(aLog)
		{
		}

		void ChangeState(DWORD dwState)
		{
			SetState(dwState);
		}

		DWORD GetState() const
		{
			return m_state;
		}
		void SetState(DWORD dwState)
		{
			if (GetState() == dwState) // has already set
				return;

			m_state = dwState;

			if (m_logGetStep)
			{
				LogState();
			}
		}
	private:
		void LogState() const
		{
			if (!m_log)
			{
				return;
			}

			switch (m_state)
			{
			case VMS_BUSY:
				m_log->LogFinest( L"SetState - VMS_BUSY");
				break;
			case VMS_READY:
				m_log->LogFinest( L"SetState - VMS_READY");
				break;
			case VMS_WAITING:
				m_log->LogFinest( L"SetState - VMS_WAITING");
				break;
			case VMS_ENDSCRIPT:
				m_log->LogFinest( L"SetState - VMS_ENDSCRIPT");
				break;
			}
		}
	private:
		DWORD m_state = VMS_WAITING;
		bool m_logGetStep = false;
		SystemLogPtr m_log;
	};

	IVOXPtr CSession::CTellib::LoadTelLib()
	{
		IVOXPtr pVox;
		HRESULT hr = pVox.CreateInstance(__uuidof(VOX));
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}
		return pVox;
	}

	/****************************************************************************************************/
	/*                                          CEventDispatcher                                        */
	/****************************************************************************************************/

	class CEventDispatcher
	{
	public:
		CEventDispatcher(SystemLogPtr aLog)
			: mLog(aLog)
		{}

		~CEventDispatcher()
		{
			mLog->LogInfo( L"CEventDispatcher destructor");
		}

		ItemEventPtr EmitNewSubEvent(
			const VXMLString& aCurMenuName,
			const MenuPtr& aMenu,
			const TagWPtr aEnterPoint,
			const ScopeServicePtr& aScope)
		{
			mLog->LogInfo( L"EmitNewSubEvent - New menu: %s", aCurMenuName.c_str());
			_dumpEvents();

			auto pEvt = CEventFactory::CreateTagEvent(
				aCurMenuName,
				aMenu,
				aEnterPoint,
				aScope,
				E_EVENT_TYPE::ET_SUB);

			if (pEvt->MatchMask(L"error.*"))
			{
				mEvents.emplace_front(pEvt);
			}
			else
			{
				mEvents.emplace_back(pEvt);
			}
			//mCurEventCit = mEvents.begin();

			return pEvt;
		}

		ItemEventPtr EmitNewRecursiveMenuEvent(
			const VXMLString& aCurMenuName,
			const MenuPtr& aMenu,
			const ScopeServicePtr& aScope)
		{
			mLog->LogInfo( L"EmitNewRecursiveMenuEvent - New menu: %s", aCurMenuName.c_str());

			mRecursiveEvent = CEventFactory::CreateMenuEvent(
				aCurMenuName,
				aMenu,
				aScope,
				E_EVENT_TYPE::ET_RECURSIVE);

			mEvents.clear();

			return mRecursiveEvent;
		}

		ItemEventPtr EmitNewRecursiveDocEvent(
			//const VXMLString& aCurMenuName,
			const MenuPtr& aMenu,
			const VXMLString& aUri,
			const TagWPtr aEnterPoint,
			const ScopeServicePtr& aScope)
		{
			mLog->LogInfo( L"EmitNewRecursiveDocEvent - New menu: %s", aMenu->MenuName.c_str());

			mRecursiveEvent = CEventFactory::CreateDocEvent(
				aMenu,
				aUri,
				aEnterPoint,
				aScope,
				E_EVENT_TYPE::ET_RECURSIVE);

			mEvents.clear();

			return mRecursiveEvent;
		}

		ItemEventPtr EmitNewPredefineEvent(
			//const VXMLString& aCurMenuName,
			const MenuPtr& aMenu,
			const VXMLString& aUri,
			const TagWPtr aEnterPoint,
			const ScopeServicePtr& aScope)
		{
			if (aMenu)
			{
				mLog->LogInfo( L"EmitNewPredefineEvent - New menu: %s", aMenu->MenuName.c_str());
			}

			mRecursiveEvent = CEventFactory::CreateDocEvent(
				aMenu,
				aUri,
				aEnterPoint,
				aScope,
				E_EVENT_TYPE::ET_PREDEFINE);

			mEvents.clear();

			return mRecursiveEvent;
		}

		template <class TWaitingEventsContainer>
		void SetWaitingEvents(
			const ItemEventPtr& currEvent,
			TWaitingEventsContainer&& _events) const
		{
			currEvent->SetWaitingEvents(std::forward<TWaitingEventsContainer>(_events));
			//currEvent->SetState(VMStates::VMS_WAITING);
		}

		static void SetState(const ItemEventPtr& aEvent, VMStates aState) { /*aEvent->SetState(aState);*/ }

		template <class T>
		void SetScopeStack(CExecContext* pCtx, T&& _scope) const
		{
			//if (mLog->IfLogAvailable())
			//{
			//	ScopeServicePtr scope = _scope.lock();
			//	auto currStack = scope->GetStack();

			//	mLog->LogInfo(L"Scope stack containing:");
			//	for (const SessionObject& sc : currStack)
			//	{
			//		CMessage msg = sc->ToMsg();
			//		mLog->LogInfo(msg.Dump().c_str());
			//	}
			//}

			pCtx->pVar = _scope;
		}

		bool DoEventStep(CExecContext* pCtx)
		{
			ItemEventPtr currEvent = this->_getCurrentEvent();
			if (!currEvent.get())
			{
				return false;
			}

			pCtx->pEvt = currEvent;

			TagWPtr curtag = currEvent->getCurPoint();
			TagWPtr beginPoint = currEvent->getBeginPoint();
			TagWPtr newPoint;

			SetScopeStack(pCtx, currEvent->GetScopeStack());

			if (curtag.lock().get())
			{
				curtag.lock()->ExecuteWithLog(pCtx, curtag, newPoint);
			}
			else
			{
				return false;
			}

			if (!newPoint.lock().get()) // event has finished
			{
				if (currEvent->isPredefineEvent())
				{
					_eraceCurrentEvent();
					return false;
				}
				if (currEvent->isRecursiveEvent())
				{
					currEvent->setCurPoint(beginPoint);
				}
				else
				{
					_eraceCurrentEvent(currEvent);
				}
			}
			else
			{
				currEvent->setCurPoint(newPoint);
			}

			//_eraseUnusefulEvents();

			return true;
		}

		ItemEventPtr GetEventByMsg(const CMessage& msg) const
		{
			VXMLString msgName = msg.GetName();

			ItemEventPtr _activeEvent(nullptr);

			// 1. if waiting for this event
			if (/*mRecursiveEvent->GetState() == VMStates::VMS_WAITING
				&& */(mRecursiveEvent->IsListConsistEvent(msgName)))
			{
				_activeEvent = mRecursiveEvent;
			}

			// 2. 
			if (!_activeEvent)
			{
				auto activeEvents = std::find_if(mEvents.cbegin(), mEvents.cend(), [msgName](const ItemEventPtr& _evt)
				{
					return (/*(_evt->GetState() == VMStates::VMS_WAITING) && */(_evt->IsListConsistEvent(msgName)));
				});

				if (activeEvents != mEvents.cend())
				{
					_activeEvent = *activeEvents;
				}
			}

			return _activeEvent;
		}

		const ItemEventPtr& GetRecursiveEvent() const
		{
			return mRecursiveEvent;
		}

		void DeleteAllSubEvents()
		{
			mEvents.clear();
		}

	private:

		const ItemEventPtr& _getCurrentEvent() const
		{
			static ItemEventPtr emptyEvent;

			if (!mEvents.empty())
			{
				return mEvents.front();
			}

			if (mRecursiveEvent)
			{
				return mRecursiveEvent;
			}

			assert(mRecursiveEvent && "GetCurrentEvent - Empty recurcive event");
			return emptyEvent;
		};

		void _eraceCurrentEvent()
		{
			mRecursiveEvent.reset();
		}

		void _eraceCurrentEvent(const ItemEventPtr& aCurrentEvent)
		{
			if (aCurrentEvent->isRecursiveEvent() || aCurrentEvent->isPredefineEvent())
			{
				return;
			}

			if (mEvents.empty())
			{
				assert(false && "Unknown current event");
				return;
			}

			ItemEventPtr& frontEvent = mEvents.front();
			if (aCurrentEvent != frontEvent)
			{
				assert(false && "Unknown front event");
				return;
			}

			mEvents.erase(mEvents.cbegin());
		}
		void _dumpEvents() const
		{
			if (!mLog->IfLogAvailable())
			{
				return;
			}

			assert(mRecursiveEvent && "_dumpEvents - Empty recurcive event");
			if (mRecursiveEvent)
			{
				mLog->LogInfo(L"Recursive event: %s", mRecursiveEvent->Dump().c_str());
			}

			for (const auto& subEvt : mEvents)
			{
				if (!subEvt)
				{
					assert(false && "Empty subevent");
					continue;
				}
				mLog->LogInfo(L"\tSub event: %s", subEvt->Dump().c_str());
			}
		}

	private:
		ItemEventPtr mRecursiveEvent;
		EventQueue mEvents;
		SystemLogPtr mLog;
	};

	/****************************************************************************************************/
	/*                                            CSession                                              */
	/****************************************************************************************************/

	CSession::CSession() 
		: m_dwState(VMS_WAITING)
		//, m_nStepCounter(0)
		, m_DialogActive(true)
		, m_Initialized(false)
		//, m_CSCounter(0)
	{
		m_pVox = std::make_unique<CTellib>();
	}

	bool CSession::Initialize(const SessionCreateParams& params)
	{
		m_InitMsg = CMessage(params.pInitMsg);
		m_Log = params.pLog;
		m_extraLog = params.pExtraLog;
		m_VXMLFolders = params.VXMLFolders;
		m_VXMLDefSet = params.VXMLDefSet;
		m_VXMLSystem = params.VXMLSystem;
		m_Text2SpeechSettings = params.Text2Speech;

		m_asyncClient = std::make_unique<AsyncEventClient>(m_Log);

		if (CParam* param = m_InitMsg.ParamByName(L"PrepareDialog"))
			m_DialogActive = false;

		if (CParam* param = m_InitMsg.ParamByName(L"FileName"))
		{
		}
		else if (CParam* param = m_InitMsg.ParamByName(L"FileText"))
		{
		}
		else
		{
			throw std::exception("Cannot find name or text executing vxml file in initial message");
		}
		m_InitMsg.CheckParam(L"ScriptID", CMessage::Int64, CMessage::Mandatory);
		m_InitMsg.CheckParam(L"ParentScriptID", CMessage::Int64, CMessage::Mandatory);
		m_InitMsg.CheckParam(L"parenttype", CMessage::String, CMessage::Mandatory);

		m_Log->LogFinest( L"CSession: Initialize StateDispatcher");
		m_StateDispatcher = std::make_unique<CStateDispatcher>(true, m_Log);

		m_ExecContext.pSes = params.pSession;
		m_ExecContext.pEng = params.pEvtPrc;
		m_ExecContext.pLog = params.pLog;
		m_ExecContext.pXLog = params.pExtraLog;
		m_ExecContext.p931 = params.pQ931Info;
		m_ExecContext.sXML = params.sQ931XML;

		CreateSessionVariables();

		int iKeepAliveTimeout = 1;
		if (!m_VXMLSystem.sKeepAliveTimeout.empty())
		{
			iKeepAliveTimeout = Utils::toNum<int>(m_VXMLSystem.sKeepAliveTimeout);
		}

		MakeKeepAlive(iKeepAliveTimeout);

		m_Log->LogInfo( L"InitMsg Name: \"%s\"; Dialog ScriptID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"ScriptID"].AsWideStr().c_str());
		// A message received
		try
		{
			InitEngine();

			m_appLog.reset(new DEBUG_NEW_PLACEMENT CAppLog(params.pVBSLog));
			m_ExecContext.pVbs->AddObject2(L"appLoger", m_appLog->AsVariant());
			m_Log->LogInfo( L"VBEngine started");

			m_ExecContext.pVbs->AddObject(L"session", m_pSessionVariables->AsVariant());
			m_ExecContext.pVbs->AddObject(L"application", m_pApplicationVariables->AsVariant());
			m_ExecContext.pVbs->AddObject(L"dialog", m_pDialogVariables->AsVariant());
			m_ScriptId = m_InitMsg[L"ScriptID"].AsInt64();
			VXMLString statement = L"ScriptID=\"" + m_InitMsg[L"ScriptID"].AsWideStr() + L"\"";
			if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement)))
				throw (statement.c_str());

			if (CParam * param = m_InitMsg.ParamByName(L"Host"))
			{
				statement = L"Host=\"" + param->AsWideStr() + L"\"";
				if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement)))
					throw (statement.c_str());
			}
		}
		catch (...)
		{
			m_Log->LogWarning(L"Failed to load VBScript engine");
			CMessage errorMsg = ExceptionHandler();
			try
			{
				DoError(errorMsg, L"SESSION");
			}
			catch (std::exception& error)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: %s", stow(error.what()).c_str());
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
			catch (...)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: unhandled");
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}

			return false;
		}

		try
		{
			m_dispatcher = std::make_unique<CEventDispatcher>(m_Log);
		}
		catch (...)
		{
			m_Log->LogWarning(L"Failed to make EventDispatcher");
			CMessage errorMsg = ExceptionHandler();
			try
			{
				DoError(errorMsg, L"SESSION");
			}
			catch (std::exception& error)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: %s", stow(error.what()).c_str());
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
			catch (...)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: unhandled");
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}

			return false;
		}

		m_Log->LogInfo( L"LoadPredefinitions");
		VXMLString sPredefine;
		if (CParam *pPredefine = m_InitMsg.ParamByName(L"predefine"))
			sPredefine = pPredefine->AsWideStr();
		else if (CParam *pPredefine = m_InitMsg.ParamByName(L"Predefine"))
		{
			m_Log->LogWarning(L"WARNING: Deprecated param name \"Predefine\"");
			sPredefine = pPredefine->AsWideStr();
		}

		bool bLoadDocumentFailed = false;
		//TagWPtr pEventEnterPoint = NULLTAG;
		DocumentPtr newDocument;
		VXMLString sErrorDescription;
		if (!sPredefine.empty())
		{
			if (!LoadPredefinitions(sPredefine))
			{
				sErrorDescription = format_wstring(L"Failed to load predefines: %s", sPredefine.c_str());
				bLoadDocumentFailed = true;
			}
		}
		
		VXMLString fileName;
		if (!bLoadDocumentFailed)
		{
			if (CParam* param = m_InitMsg.ParamByName(L"FileName"))
			{
				fileName = m_InitMsg[L"FileName"].AsWideStr();
				newDocument = LoadDocument(fileName, true);
			}
			else if (CParam* param = m_InitMsg.ParamByName(L"FileText"))
			{
				fileName = L"FileText";
				newDocument = LoadDocument(m_InitMsg[L"FileText"].AsWideStr(), false);
			}
		}

		m_Log->LogInfo( L"ChangeState");

		if (bLoadDocumentFailed || !newDocument)
		{
			VXMLString sError = format_wstring(L"Failed to load document: %s", std::wstring(CAtlString(fileName.c_str()).Left(256)).c_str());
			m_Log->LogWarning(L"Error: %s", sError.c_str());

			DoDocumentFailed(fileName, sError);
			ChangeState(VMS_ENDSCRIPT);
			return true;
		}

		VXMLString nextExecuteMenuId;

		MenuPtr menu;
		if (CParam *pMenu = m_InitMsg.ParamByName(L"NextExecuteMenuID"))
		{
			nextExecuteMenuId = pMenu->AsWideStr();
			menu = newDocument->CreateNextMenu(nextExecuteMenuId, L"");
		}
		else
		{
			menu = newDocument->CreateNextMenu();
		}

		if (!menu || !menu->IsEnabled())
		{
			VXMLString error;
			if (nextExecuteMenuId.empty())
			{
				error = format_wstring(L"There are no menu in document: \"%s\"", std::wstring(CAtlString(fileName.c_str()).Left(256)).c_str());
			}
			else
			{
				error = format_wstring(L"Failed execute menu: \"%s\", cause menu doesn't exist", nextExecuteMenuId.c_str());
			}

			m_Log->LogWarning(L"Error: %s", error.c_str());

			DoDocumentFailed(fileName, error);
			ChangeState(VMS_ENDSCRIPT);

			return true;
		}

		TagWPtr pEventEnterPoint = NULLTAG;
		if (PrepareDoc(newDocument, fileName))
		{
			pEventEnterPoint = GetDocumentEnterPoint();
		}

		//create new scope stack
		ScopeServicePtr scopes = this->CreateSessionScope();
		scopes->PushScope(std::make_shared<CNameTable>(L"session_scope"));

		EmitRecursiveDocEvent(menu, fileName, pEventEnterPoint, scopes);

		ChangeState(VMS_READY);

		m_Initialized = true;
		return true;
	}

	CSession::~CSession(void)
	{
		m_Log->LogInfo( L"CSession destructor");
		DestroyEngine();

		m_keepAlive->Stop();
		m_pVox.reset();
	}

	//TagWPtr CSession::LoadDocument(const VXMLString& sURI, const bool& bFile)
	//{
	//	ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
	//	VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));

	//	//EventScope saved_globals_scope = m_globals_scope;
	//	m_ScopeCollector.PushScope();
	//	m_ScopeCollector.RemoveUnnecessaryScopes(m_sCurDoc, m_Log);
	//	//m_globals_scope.remove_if(CFindDocumentScopes(m_sCurDoc));
	//	ResourceCachePtr	pRes;
	//	try
	//	{
	//		pRes.reset(new DEBUG_NEW_PLACEMENT CResourceCache());
	//	}
	//	catch (...)
	//	{
	//		m_Log->LogWarning(L"Failed to load CResourceCache");
	//		//ExceptionHandler();
	//	}
	//	if (!pRes->Ready())
	//	{
	//		m_Log->LogWarning(L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
	//		//m_extraLog->LogWarning(L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
	//		return NULLTAG;
	//	}

	//	DocumentPtr pDoc;
	//	m_Log->LogFinest( L"Load document: \"%s\"", sURI.c_str());
	//	try
	//	{
	//		SAFEARRAY * pSA = NULL;
	//		if (SUCCEEDED(/*m_ExecContext.*/pRes->GetDocument(sURI, sScriptID, bFile, &pSA)))
	//		{
	//			m_Log->LogFinest( L"LoadDocument: GetDocument successful");
	//			// get back archive
	//			char *c1;
	//			// Get data pointer
	//			HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

	//			int image_size = pSA->rgsabound->cElements;


	//			/*for (i = 0; i < image_size; i++)
	//			retVal += BYTE (c1[i]);*/
	//			std::string in_data;
	//			in_data.append(c1, image_size);

	//			::SafeArrayUnaccessData(pSA);
	//			::SafeArrayDestroy(pSA);

	//			std::istringstream is(in_data);

	//			Factory_c::CollectorPtr collector;
	//			{
	//				cereal::BinaryInputArchive iarchive(is);
	//				m_Log->LogFinest( L"LoadDocument: deserialization, in_data.size()", in_data.size());
	//				iarchive(collector);
	//				m_Log->LogFinest( L"LoadDocument: new CDocument");
	//			}

	//			pDoc = std::make_shared<CDocument>(collector, m_ScopeCollector);

	//		}
	//	}
	//	catch (boost::archive::archive_exception& ex)
	//	{
	//		m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(ex.what())).c_str());
	//		return NULLTAG;
	//	}
	//	catch (std::exception& e)
	//	{
	//		m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
	//		return NULLTAG;
	//	}
	//	catch (std::wstring& e)
	//	{
	//		m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), e.c_str());
	//		return NULLTAG;
	//	}
	//	catch (_com_error& e)
	//	{
	//		m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
	//		return NULLTAG;
	//	}
	//	catch (...)
	//	{
	//		m_Log->LogWarning(L"Unknown exception loading document [%s]", sURI.c_str());
	//		return NULLTAG;
	//	}

	//	if (PrepareDoc(pDoc, sURI))
	//	{
	//		return GetDocumentEnterPoint();//pDoc->GetEnterPoint();
	//	}

	//	m_ScopeCollector.PopScope();

	//	return NULLTAG;
	//}

	DocumentPtr CSession::LoadDocument(const VXMLString& sURI, const bool& bFile)
	{
		DocumentPtr pDoc;

		ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
		VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));

		//EventScope saved_globals_scope = m_globals_scope;
		m_ScopeCollector.PushScope();
		m_ScopeCollector.RemoveUnnecessaryScopes(m_sCurDoc);
		//m_globals_scope.remove_if(CFindDocumentScopes(m_sCurDoc));
		ResourceCachePtr	pRes;
		try
		{
			pRes.reset(new DEBUG_NEW_PLACEMENT CResourceCache());
		}
		catch (...)
		{
			m_Log->LogWarning(L"Failed to load CResourceCache");
			//ExceptionHandler();
		}
		if (!pRes->Ready())
		{
			m_Log->LogWarning(L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
			return pDoc;
		}

		m_Log->LogInfo( L"Load document: \"%s\"", sURI.c_str());
		try
		{
			SAFEARRAY * pSA = NULL;
			if (SUCCEEDED(pRes->GetDocument(sURI, sScriptID, bFile, &pSA)))
			{
				m_Log->LogInfo( L"LoadDocument: GetDocument successful");
				// get back archive
				char *c1;
				// Get data pointer
				HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

				int image_size = pSA->rgsabound->cElements;


				/*for (i = 0; i < image_size; i++)
				retVal += BYTE (c1[i]);*/
				std::string in_data;
				in_data.append(c1, image_size);

				::SafeArrayUnaccessData(pSA);
				::SafeArrayDestroy(pSA);

				std::istringstream is(in_data);

				Factory_c::CollectorPtr collector;
				{
					cereal::BinaryInputArchive iarchive(is);
					m_Log->LogFinest( L"LoadDocument: deserialization, in_data.size() = %i", in_data.size());
					iarchive(collector);
					m_Log->LogFinest( L"LoadDocument: new CDocument");
				}

				pDoc = std::make_shared<CDocument>(collector, m_ScopeCollector);

			}
		}
		catch (boost::archive::archive_exception& ex)
		{
			m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(ex.what())).c_str());
		}
		catch (std::exception& e)
		{
			m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
		}
		catch (std::wstring& e)
		{
			m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), e.c_str());
		}
		catch (_com_error& e)
		{
			m_Log->LogWarning(L"Exception loading document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
		}
		catch (...)
		{
			m_Log->LogWarning(L"Unknown exception loading document [%s]", sURI.c_str());
		}

		if (!pDoc)
		{
			m_ScopeCollector.PopScope();
		}

		return pDoc;
	}

	//TagWPtr CSession::DoLoadDocument(const VXMLString& sSource, const bool& bFile)
	//{
	//	VXMLString sErrorDescription;

	//	m_Log->LogFinest( L"DoLoadDocument");

	//	TagWPtr reloadedDoc = LoadDocument(sSource, bFile);

	//	if (!reloadedDoc.lock().get())
	//	{

	//		VXMLString sError = format_wstring(L"Failed to load document: %s", std::wstring(CAtlString(sSource.c_str()).Left(256)).c_str());
	//		m_Log->LogWarning(L"Error: %s", sError.c_str());

	//		DoDocumentFailed(sSource, sErrorDescription);

	//		MakeVXMLRunScriptFailed(sSource);
	//	}
	//	else
	//	{
	//		m_Log->LogFinest( L"Document ready to execute");

	//		DoDocumentPrepared();
	//	}

	//	return reloadedDoc;
	//}

	//void CSession::

	bool CSession::LoadPredefinitions(const VXMLString& sPredefine)
	{
		ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
		VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));

		ResourceCachePtr	pRes;
		try
		{
			pRes.reset(new DEBUG_NEW_PLACEMENT CResourceCache());
		}
		catch (...)
		{
			m_Log->LogWarning(L"Failed to load CResourceCache");
		}
		if (!pRes->Ready())
		{
			m_Log->LogWarning(L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
			return false;
		}

		std::vector <VXMLString> _files;

		boost::split(_files, sPredefine, std::bind2nd(std::equal_to<wchar_t>(), ';'));

		_files.erase(std::remove_if(_files.begin(), _files.end(),
			[](const VXMLString& _uri)
		{
			if (_uri.empty())
				return true;
			return false;
		}
		), _files.end());


		for(const VXMLString& _sURI: _files)
		{
			DocumentPtr pDoc;

			m_Log->LogInfo( L"Load predefine: \"%s\"", _sURI.c_str());
			// LOADING
			try
			{
				SAFEARRAY * pSA = NULL;
				if (SUCCEEDED(pRes->GetDocument(_sURI, sScriptID, true, &pSA)))
				{
					// get back arhive
					char *c1;
					// Get data pointer
					HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

					int image_size = pSA->rgsabound->cElements;

					std::string in_data;
					in_data.append(c1, image_size);

					::SafeArrayUnaccessData(pSA);
					::SafeArrayDestroy(pSA);

					std::istringstream is(in_data);

					Factory_c::CollectorPtr collector;
					{
						cereal::BinaryInputArchive iarchive(is);
						m_Log->LogFinest( L"LoadDocument: deserialization, in_data.size() = %i", in_data.size());
						iarchive(collector);
						m_Log->LogFinest( L"LoadDocument: new CDocument");
					}

					pDoc = std::make_shared<CDocument>(collector, m_ScopeCollector);

				}
			}
			catch (boost::archive::archive_exception& ex)
			{
				m_Log->LogWarning(L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(ex.what())).c_str());
				return false;
			}
			catch (std::exception& e)
			{
				m_Log->LogWarning(L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
				return false;
			}
			catch (std::wstring& e)
			{
				m_Log->LogWarning(L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), e.c_str());
				return false;
			}
			catch (_com_error& e)
			{
				m_Log->LogWarning(L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), (LPCWSTR)e.Description());
				return false;
			}
			catch (...)
			{
				m_Log->LogWarning(L"Unknown exception loading predefine document [%s]", _sURI.c_str());
				return false;
			}


			//EXECUTE
			try
			{
				if (/*pSA &&*/ pDoc.get())
				{
					m_Log->LogInfo( L"Execute predefine: \"%s\"", _sURI.c_str());

					//m_bPredefinitionInProcess = true;
					//pDoc->Execute(&m_ExecContext);
					//m_bPredefinitionInProcess = false;

					//prepare document
					m_ExecContext.pDoc = pDoc;

					TagWPtr pEventEnterPoint = this->GetDocumentEnterPoint();
					if (pEventEnterPoint.lock().get())
					{
						//CMessage msg(L"LoadPredefineDocument");

						ScopeServicePtr scopes = this->CreateSessionScope();

						MenuPtr menu = GetFirstMenu();
						if (menu && menu->IsEnabled())
						{
							m_Log->LogWarning(L"Unexpected menu has found: \"%s\" [%s]", menu->MenuName.c_str(), _sURI.c_str());
							return false;
						}

						EmitPredefineEvent(menu, _sURI, pEventEnterPoint, scopes);

						//execute here
						while (m_dispatcher->DoEventStep(&m_ExecContext))
						{
						}
					}
					m_ExecContext.predefineDocuments.push_back(pDoc);
				}
				else
				{
					DoErrorBadfetch(
						L"SESSION",
						BadfetchErrorsToString(BadfetchErrors::BE_LOAD_DOCUMENT),
						L"Load_predefine",
						format_wstring(L"Predefine document \"%s\" was requested to load", _sURI.c_str()));

					return false;

				}
			}
			catch (std::exception& e)
			{
				m_Log->LogWarning(L"Exception executing predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
				return false;
			}
			catch (std::wstring& e)
			{
				m_Log->LogWarning(L"Exception executing predefine document [%s]: \"%s\"", _sURI.c_str(), e.c_str());
				return false;
			}
			catch (_com_error& e)
			{
				m_Log->LogWarning(L"Exception executing predefine document [%s]: \"%s\"", _sURI.c_str(), (LPCWSTR)e.Description());
				return false;
			}
			catch (...)
			{
				m_Log->LogWarning(L"Unknown exception executing predefine document [%s]", _sURI.c_str());
				return false;
			}


		}

		return true;
	}

	bool CSession::PrepareDoc(DocumentPtr pDoc, const VXMLString& sURI)
	{
		try
		{
			if (pDoc.get())
			{
				m_Log->LogInfo( L"Execute document: \"%s\"", sURI.c_str());
				
				if (!m_sCurDoc.empty())
				{
					// It was old document's scope
					//m_ExecContext.pVar->PopScope();
				}
				// Create new document's scope
				//m_ExecContext.pVar->PushScope(/*L"document"*/);

				m_sCurDoc = sURI;
				//m_sNextDoc = L"";
				while (m_ExecContext.previousDoc.size() > 3)
					m_ExecContext.previousDoc.pop_front();

				if (m_ExecContext.pDoc)
					m_ExecContext.previousDoc.push_back(m_ExecContext.pDoc);

				m_ExecContext.pDoc = pDoc;

				m_Log->LogFinest( L"Document ready to execute");

				DoDocumentPrepared();

				return true;
			}

		}
		catch (std::exception& e)
		{
			m_Log->LogWarning(L"Exception executing document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
			return false;
		}
		catch (std::wstring& e)
		{
			m_Log->LogWarning(L"Exception executing document [%s]: \"%s\"", sURI.c_str(), e.c_str());
			return false;
		}
		catch (_com_error& e)
		{
			m_Log->LogWarning(L"Exception executing document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
			return false;
		}
		catch (...)
		{
			m_Log->LogWarning(L"Unknown exception executing document [%s]", sURI.c_str());
			return false;
		}

		return false;
	}

	void CSession::CreateSessionVariables()
	{
		m_pGlobalVars    = std::make_shared<CNameTable>(L"global_vars_scope");
		m_pPredefineVars = std::make_shared<CNameTable>(L"predefine_vars_scope");
		m_pLastMessage   = std::make_shared<CNameTable>(L"last_message_scope");

		ScopeServicePtr service = std::make_shared<CScopeService>();
		CScopeService::CScopeSaver saver(service, m_pGlobalVars);

		// Application GLOBAL variables
		service->AddVar(L"utterance",L"", false);
		service->AddVar(L"inputmode",L"", false);
		service->AddVar(L"confidence", L"0", false);
		service->AddVar(L"classid", L"", false);
		service->AddVar(L"recognition", L"", false);
		service->AddVar(L"recognitionlocal", L"", false);

		service->AddVar(L"termchar",m_VXMLDefSet.sTermchar.c_str(),false);
		service->AddVar(L"timeout",m_VXMLDefSet.sTimeout.c_str(),false);
		service->AddVar(L"WaitTime",m_VXMLDefSet.sWaitTime.c_str(),false);
		service->AddVar(L"interdigittimeout", m_VXMLDefSet.sInterdigittimeout.c_str(), false);
		service->AddVar(L"maxdtmfcount", 0, false);
		service->AddVar(L"Speaker", L"", false);
		service->AddVar(L"Language", L"", false);
		
		m_pGrammars.reset(new DEBUG_NEW_PLACEMENT VXMLGrammar(L""));
		service->AddVar(L"grammars", m_pGrammars->AsVariant(), false);

		// Session variables
		m_pLocal.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"local"));
		m_pLocal->Add(L"uri",	m_InitMsg.SafeReadParam(L"B", CMessage::String, CComVariant()).Value,	true);

		m_pRemote.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"remote"));
		m_pRemote->Add(L"uri",	m_InitMsg.SafeReadParam(L"A", CMessage::String, CComVariant()).Value,	true);

		m_pProtocol.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"protocol"));
		m_pProtocol->Add(L"name", L"ISUP", true);
		m_pProtocol->Add(L"xml", m_ExecContext.sXML.c_str(), true);
		m_pProtocol->Add(L"q931", CComVariant(m_ExecContext.p931),false);

		m_pConnection.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"connection"));
		m_pConnection->Add(L"local"   ,m_pLocal   ->AsVariant(),false);
		m_pConnection->Add(L"remote"  ,m_pRemote  ->AsVariant(),false);
		m_pConnection->Add(L"protocol",m_pProtocol->AsVariant(),false);

		CMessage pSession(L"session");
		pSession[L"connection" ] = m_pConnection->AsVariant();
		m_pSessionVariables = EventPtr(new DEBUG_NEW_PLACEMENT CEngineEvent(pSession));

		CMessage pApplication(L"application");
		pApplication[L"lastresult"] = m_pGlobalVars->AsVariant();
		pApplication[L"lastmessage"] = m_pLastMessage->AsVariant();
		m_pApplicationVariables = EventPtr(new DEBUG_NEW_PLACEMENT CEngineEvent(pApplication));

		m_pDialog.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"dialog"));

		CParam* paramName = m_InitMsg.ParamByName(L"parameters");
		if (!paramName)
		{
			paramName = m_InitMsg.ParamByName(L"namelist");
		}

		if (paramName)
		{
			std::wistringstream in(paramName->AsWideStr());
			std::istream_iterator<VXMLString, wchar_t> first(in), last;
			for (; first != last; ++first)
			{
				m_pDialog->Add((*first), m_InitMsg[(*first).c_str()].AsWideStr().c_str(), false);
			}
		}


		CMessage pDialog(L"dialog");
		pDialog[L"namelist"] = m_pDialog->AsVariant();
		pDialog[L"parameters"] = m_pDialog->AsVariant();
		m_pDialogVariables = EventPtr(new DEBUG_NEW_PLACEMENT CEngineEvent(pDialog));

		ObjectId nScriptID = (ObjectId)m_InitMsg[L"ScriptID"].AsUInt64();
		m_InitMsg.Remove(L"ScriptID");
		m_InitMsg[L"ScriptID"] = nScriptID;
		m_InitMsg[L"ScriptID"] = (ObjectId)m_InitMsg[L"ScriptID"].AsInt64();

		if (CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			m_Log->LogFinest( L"\n InitMsg Name: \"%s\"; CallID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"CallID"].AsWideStr().c_str());
		}
		if (CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			m_Log->LogFinest( L"\n InitMsg Name: \"%s\"; VoxID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"VoxID"].AsWideStr().c_str());
		}
	}

	void CSession::MakeKeepAlive(int aKeepAliveTimeout)
	{
		m_keepAlive = std::make_unique<CKeepAliveProvider>();
		m_keepAlive->Init(aKeepAliveTimeout * 60, m_ExecContext.pEng, m_ExecContext.pLog);
	}

	ScopeServicePtr CSession::CreateSessionScope() const 
	{
		ScopeServicePtr scopes = std::make_shared<CScopeService>();
		scopes->PushScope(m_pGlobalVars);
		scopes->PushScope(m_pPredefineVars);

		return scopes;
	}

	bool CSession::IsRecursiveEvent() const
	{
		return m_ExecContext.pEvt->isRecursiveEvent();
	}

	void CSession::DoError(
		const wchar_t* aErrorName,
		const VXMLString& aSubSystem,
		const VXMLString& aReason,
		const VXMLString& aTag,
		const VXMLString& aDescription)
	{
		CMessage evt(aErrorName);
		evt[L"reason"] = aReason;
		evt[L"tagname"] = aTag;
		evt[L"description"] = aDescription;

		DoError(evt, aSubSystem);
	}


	//void CSession::PostThreadMsg(eThreadMsg msg, void* pParam)
	//{
	//	m_Log->LogFinest( L"THREAD MSG: %i", msg);
	//	m_queue.push(msg);
	//	//__super::PostMessage(msg, 0, (LPARAM)pParam);
	//}

	void CSession::SendAuxMsg(CMessage& msg) const
	{
		CPackedMsg pm(msg);
		m_ExecContext.pEng->PostAuxMessage(pm());
		m_Log->LogInfo( L"OUT %s", msg.Dump().c_str());
	}

	//void CSession::OnThreadMsg(eThreadMsg msg, void* pParam)
	//{
	//	//m_throwEvt = false;
	//	m_Log->LogFinest( L"Incoming \"%s\" (%i) thread message", ThreadMsgToString(msg).c_str(), int(msg));
	//	
	//	if (!this->IsActive())
	//	{
	//		m_Log->LogFinest( L"Cannot execute message, current session does not active");
	//		return;
	//	}

	//	switch (msg)
	//	{
	//	//case tmExecuteDoc:
	//	//case tmDocumentPrepared:
	//	//	{
	//	//		if (!this->IsActive())
	//	//		{
	//	//			m_Log->Log(__FUNCTIONW__, L"Cannot execute message, current session does not active");
	//	//			return;
	//	//		}

	//	//		if(msg == tmExecuteDoc)
	//	//		{
	//	//			m_Log->Log(__FUNCTIONW__, L"Executing document \"%s\"", m_sCurDoc.c_str());
	//	//			if (m_ExecContext.pDoc.get())
	//	//				m_ExecContext.pDoc->Execute(&m_ExecContext);
	//	//			m_DialogActive = true;
	//	//			m_Log->Log(__FUNCTIONW__, L"Execution of document \"%s\" completed", m_sCurDoc.c_str());
	//	//		}
	//	//		if( msg == tmDocumentPrepared)
	//	//		{
	//	//			DoDocumentPrepared();
	//	//		}
	//	//		break;
	//	//	}
	//	case tmExecuteDoc:
	//		{

	//			m_Log->Log(LEVEL_FINEST,__FUNCTIONW__, L"Executing document \"%s\"", m_sCurDoc.c_str());
	//			bool bDocRet = true;
	//			if (m_ExecContext.pDoc.get())
	//				bDocRet = m_ExecContext.pDoc->Execute(&m_ExecContext);
	//			m_DialogActive = true;
	//			if (bDocRet)
	//				m_Log->LogFinest( L"Execution of document \"%s\" completed", m_sCurDoc.c_str());
	//			else
	//				m_Log->LogFinest( L"Execution of document \"%s\" failed", m_sCurDoc.c_str());

	//			break;
	//		}
	//	case tmDocumentPrepared:
	//		{
	//			DoDocumentPrepared();
	//			break;
	//		}
	//	case tmSetEvent:
	//		{
	//			ProcessEvents();
	//			break;
	//		}
	//	}
	//}

	DWORD CSession::GetState() const
	{
		return m_StateDispatcher->GetState();
	}

	void CSession::ChangeState(DWORD dwState)
	{
		m_StateDispatcher->ChangeState(dwState);
	}

	void CSession::SetState(DWORD dwState)
	{
		m_StateDispatcher->SetState(dwState);
	}

	void CSession::SendQueuedEvent(int nIndex)
	{
	}

	//void CSession::ProcessUnhandledEvent(EventPtr pEvt)
	//{
	//	m_Log->LogInfo( L"Unhandled event \"%s\"", pEvt->GetName().c_str());
	//	//m_extraLog->Log(__FUNCTIONW__, L"[%s] Unhandled event \"%s\"", GetCurDocUri().c_str(), pEvt->GetName().c_str());

	//	if (pEvt->MatchMask(L"Invalid") 
	//		)
	//	{
	//		m_Log->LogFinest( L"Executing DoDialogError()");
	//		//DoExit(/*GetParentID(),L"",*/L"");
	//		DoDialogError(format_wstring(L"Unhandled event \"%s\"", pEvt->GetName().c_str()));
	//	}

	//}
	CMessage CSession::ExceptionHandler()
	{
		CMessage evt(L"error.unrecoverable");
		evt[L"tagname"]	= L"unrecoverable";//evt.GetName();
		evt[L"description"] = L"Unexpected exception caught";

		try
		{
			throw;
		}
		catch (CMessage& e)
		{
			evt += e;
		}
		catch (std::bad_function_call & e)
		{
			evt[L"reason"] = std::wstring(_bstr_t(e.what())).c_str();
		}
		catch (std::exception& e)
		{
			evt[L"reason"] = std::wstring(_bstr_t(e.what())).c_str();
		}
		catch (std::wstring& e)
		{
			evt[L"reason"] = e;
		}
		catch (_com_error& e)
		{
			evt[L"reason"] = (LPCWSTR)e.Description();
		}
		catch (...)
		{
			evt[L"reason"] = L"Unknown unhandled exception";
		}

		return evt;
	}

	void CSession::DoExit(VXMLString sNamelist)
	{
		DoExit(L"END_DIALOG", sNamelist, L"no error");
	}
	void CSession::DoTerminated(const VXMLString& sNamelist, const VXMLString& sHandler)
	{
		if (!sHandler.empty())
		{
			m_Log->LogInfo( L"Execute terminated handle: %s", sHandler.c_str());
			m_ExecContext.pVbs->ExecScript(sHandler);
		}
		else
		{
			m_Log->LogFinest( L"no terminated handle");
		}

		DoExit(L"TERMINATED_DIALOG", sNamelist, L"no error");
	}
	void CSession::DoDialogError(const VXMLString& sErrorDescription)
	{
		DoExit(L"error.dialog", L"", sErrorDescription);
	}
	void CSession::DoExit()
	{
		//m_bSessionEnd = true;
		//m_Log->LogInfo( L"Post message \"tmQuit\"");
		//PostThreadMsg(tmQuit);
		SetState(VMStates::VMS_ENDSCRIPT);
	}

	void CSession::InitEngine()
	{
		//m_ExecContext.pVbs.reset(new DEBUG_NEW_PLACEMENT CScriptEngine(m_ExecContext.pSes.lock(), m_Log));
		m_ExecContext.pVbs = std::make_unique<CScriptEngine>(m_ExecContext.pSes.lock(), m_Log);
	}

	void CSession::DestroyEngine()
	{
		m_ExecContext.pVbs->Clear();
		m_ExecContext.pVbs.reset();
		m_Log->LogInfo( L"Engine has destruct");
	}

	void CSession::MakeVXMLRunScriptFailed(const VXMLString& aSource)
	{
		ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();

		DoErrorBadfetch(
			L"SESSION",
			BadfetchErrorsToString(BadfetchErrors::BE_LOAD_DOCUMENT),
			L"Run script",
			format_wstring(L"Cannot run script [scriptID = %08X-%08X] \"%s\"", HIDWORD(iScriptID), LODWORD(iScriptID), aSource.c_str()));
	}

	void CSession::DoExit(const VXMLString& sConnectedWithMessageName, const VXMLString& sNamelist, const VXMLString& sErrorDescription)
	{
		//m_bSessionEnd = true;

		CMessage msg(sConnectedWithMessageName.c_str());
		msg[L"targettype" ] = m_InitMsg[L"parenttype"].Value;
		msg[L"reason"] = sErrorDescription.c_str();

		if (!sNamelist.empty())
		{
			msg[L"namelist"] = sNamelist;

			std::wistringstream in(sNamelist.c_str());
			std::istream_iterator<VXMLString, wchar_t> first(in), last;

			//LPCWSTR p = sNamelist.c_str(), q;
			VXMLString sParamName;
			for (; first != last; ++first)
			{
				msg[(*first).c_str()] = m_ExecContext.pVbs->ExprEval(*first);
			}
		}

		ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), GetParentScriptID());

		SendEvent(msg, GetParentScriptID());
		//::Sleep(3000);
		DoExit();
	}

	void CSession::DoError(CMessage &evt, const VXMLString& _subSystem)
	{
		//m_throwEvt = true;
		VXMLString sError, sTag, sDescription;
		ObjectId iScriptID = 0;
		if (CParam *pParam = evt.ParamByName(L"reason"))
			sError = pParam->AsWideStr();
		if (CParam *pParam = evt.ParamByName(L"tagname"))
			sTag = pParam->AsWideStr();
		if (CParam *pParam = evt.ParamByName(L"description"))
			sDescription = pParam->AsWideStr();
		if (CParam *pParam = m_InitMsg.ParamByName(L"ScriptID"))
			iScriptID = pParam->AsInt64();

		VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));

		evt[L"event_name"] = evt.GetName();//L"error";

		m_Log->LogWarning( 
			L"Push error \"%s - %s\" to stack", 
			sTag.c_str(), 
			sError.c_str());

		m_extraLog->LogWarning(
			L"[%s] Push error \"%s - %s [%s]\" to stack", 
			GetCurDocUri().c_str(), 
			sTag.c_str(), 
			sError.c_str(),
			sDescription.c_str());

		if (0 == CAtlString(_subSystem.c_str()).CompareNoCase(L"VBENGINE"))
			m_extraLog->LogExtra(L"Script source: %s", m_ExecContext.pVbs->ScriptSource().c_str());

		SendAuxMsg(AlarmMsg(L"VXML", sScriptID, _subSystem, evt.GetName(), sDescription, sError, -1111));

		this->EmitSubEvent(evt);
	}

	void CSession::EmitSubEvent(
		const VXMLString& aCurrMenuName,
		const MenuPtr& aMenu,
		const TagWPtr aEnterPoint,
		const ScopeServicePtr& aScope)
	{
		//m_Log->LogInfo( L"Push \"%s\" to stack", aCurrMenuName.c_str());
		const auto emitedEvent = m_dispatcher->EmitNewSubEvent(aCurrMenuName, aMenu, aEnterPoint, aScope);
		m_Log->LogFinest( L"Emitting event: %s", emitedEvent->Dump().c_str());
	}

	void CSession::EmitRecursiveMenuEvent(
		const VXMLString& aCurrMenuName,
		const MenuPtr& aMenu,
		const ScopeServicePtr& aScope)
	{
		//m_Log->LogInfo( L"Push \"%s\" to stack", aCurrMenuName.c_str());
		const auto emitedEvent = m_dispatcher->EmitNewRecursiveMenuEvent(aCurrMenuName, aMenu, aScope);
		m_Log->LogFinest( L"Emitting event: %s", emitedEvent->Dump().c_str());
	}

	void CSession::EmitRecursiveDocEvent(
		//const VXMLString& aCurrMenuName,
		const MenuPtr& aMenu,
		const VXMLString& aUri,
		const TagWPtr& aEnterPoint,
		const ScopeServicePtr& aScope)
	{
		//m_Log->LogInfo( L"Push \"%s\" to stack", aUri.c_str());
		const auto emitedEvent = m_dispatcher->EmitNewRecursiveDocEvent(aMenu, aUri, aEnterPoint, aScope);
		m_Log->LogInfo( L"Emitting event: %s", emitedEvent->Dump().c_str());
	}

	void CSession::EmitPredefineEvent(
		//const VXMLString& aCurrMenuName,
		const MenuPtr& aMenu,
		const VXMLString& aUri,
		const TagWPtr& aEnterPoint,
		const ScopeServicePtr& aScope)
	{
		//m_Log->LogInfo( L"Push \"%s\" to stack", aUri.c_str());
		const auto emitedEvent = m_dispatcher->EmitNewPredefineEvent(aMenu, aUri, aEnterPoint, aScope);
		m_Log->LogFinest( L"Emitting event: %s", emitedEvent->Dump().c_str());
	}

	void CSession::EmitSubEvent(const CMessage& evt)
	{
		bool bReqursiveMenu = IsRecursiveEvent();

		//initialize new scope
		ScopeServicePtr scopeStack = std::make_shared<CScopeService>(*m_ExecContext.pVar.lock().get());
		if (!bReqursiveMenu)
		{
			scopeStack->PopScope(); // delete catch, filled and etc. scopes
		}

		MenuPtr menu = GetCurMenu();

		TagWPtr nextMenuTag = menu->BeginTagPtr.lock()->ExecEvents(&m_ExecContext, evt);
		if (nextMenuTag.lock().get())
		{
			EmitSubEvent(menu->MenuName, menu, nextMenuTag, scopeStack);
		}
		else
		{
			m_Log->LogWarning(L"Unhandled event \"%s\"", evt.GetName());
			if (CEngineEvent(evt).MatchMask(L"error.*")
				|| CEngineEvent(evt).MatchMask(L"*.TS2D_INVALID_CALL"))
			{
				DoExit(L"");
			}
		}


		//TagWPtr nextMenuTag;
		//VXMLString currentMenuId;

		//std::tie(nextMenuTag, currentMenuId) = GetEventTag(evt);

		//if (nextMenuTag.lock().get())
		//{
		//	EmitSubEvent(currentMenuId, evt, nextMenuTag, scopeStack);
		//}
		//else
		//{
			//m_Log->LogInfo( L"Unhandled event \"%s\"", evt.GetName());
			//if (CEngineEvent(evt).MatchMask(L"error.*")
			//	|| CEngineEvent(evt).MatchMask(L"*.TS2D_INVALID_CALL"))
			//{
			//	DoExit(L"");
			//}
		//}

	}

	void CSession::DoDocumentPrepared()
	{
		//Sleep(20000);
		CMessage msg(L"VXML_RUN_SCRIPT_OK");
		msg[L"ScriptID"   ]    = m_InitMsg[L"ScriptID"].AsWideStr();
		msg[L"targettype" ]    = m_InitMsg[L"parenttype"].Value;
		msg[L"CallbackID"]     = m_InitMsg[L"CallbackID"].Value;
		msg[L"ParentScriptID"] = m_InitMsg[L"ParentScriptID"].Value;
		if(m_InitMsg.ParamByName(L"PrepareDialog"))
			msg[L"PrepareDialog"] = m_InitMsg[L"PrepareDialog"].Value;
		SendEvent(msg,GetParentScriptID());

		//CMessage msg(L"DialogPrepared_Ack");
		//msg[L"targettype"] = L"ccxml";
		//SendEvent(msg,GetParentScriptID());
	}

	void CSession::DoDocumentFailed(const VXMLString& _source, const VXMLString& _error_description)
	{
		//Sleep(20000);
		CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		msg[L"ScriptID"] = m_InitMsg[L"ScriptID"].AsWideStr();
		msg[L"targettype"] = m_InitMsg[L"parenttype"].Value;
		msg[L"CallbackID"] = m_InitMsg[L"CallbackID"].Value;
		msg[L"ParentScriptID"] = m_InitMsg[L"ParentScriptID"].Value;
		if (m_InitMsg.ParamByName(L"PrepareDialog"))
			msg[L"PrepareDialog"] = m_InitMsg[L"PrepareDialog"].Value;
		msg[L"ErrorDescription"] = _error_description.c_str();
		msg[L"FileName"] = _source.c_str();
		
		SendEvent(msg, GetParentScriptID());
	}

	//EventPtr CSession::CurrentEvent()
	//{
	//	////CLocker lock(m_CS1, m_Log, &m_CSCounter);
	//	//std::unique_lock<std::mutex> lock(m_CS1);
	//	//if (m_EvtQ.empty())
	//	//{
	//	//	throw std::range_error("Empty massage list");
	//	//}
	//	//return *m_EvtQ.rbegin();
	//	try
	//	{
	//		return m_dispatcher.CurrentEvent();
	//	}
	//	catch (std::exception const & ex)
	//	{
	//		m_Log->LogFinest( L"CATCH \"%s\"...set session state = WAITING", VXMLString(bstr_t(ex.what())).c_str());
	//		SetState(VMS_WAITING);
	//	}

	//	return EventPtr(NULL);
	//	

	//}

	//EventPtr CSession::GetFirstWaitingEvent(const VXMLString& _massage_name)
	//{
	//	try
	//	{
	//		return m_dispatcher.GetEventByName(_massage_name);
	//	}
	//	catch (std::exception const & ex)
	//	{
	//		m_Log->LogFinest( L"CATCH \"%s\"...", VXMLString(bstr_t(ex.what())).c_str());
	//		SetState(VMS_WAITING);
	//	}

	//	return EventPtr(NULL);
	//}

	//void CSession::DeleteEvent(CSession::EventPtr& evt)
	//{
	//	//CLocker lock(m_CS1, m_Log, &m_CSCounter);
	//	std::unique_lock<std::mutex> lock(m_CS1);
	//	if (m_EvtQ.empty())
	//	{
	//		throw std::range_error("Empty massage list");
	//	}
	//	
	//	m_EvtQ.erase(std::remove_if(m_EvtQ.begin(), m_EvtQ.end(), 
	//		[evt](const EventPtr& _evt)
	//	{
	//		if (evt == _evt)
	//			return true;
	//		return false;
	//	}
	//	), m_EvtQ.end());
	//}

	//void CSession::CancelEvent(const VXMLString& event_name)
	//{
	//	ObjectId nParentID = GetParentCallID();
	//	//CLocker lock(m_CS1, m_Log, &m_CSCounter);
	//	std::unique_lock<std::mutex> lock(m_CS1);
	//	EventQueue::iterator it = m_EvtQ.begin();
	//	for (; it != m_EvtQ.end(); ++it)
	//	{
	//		if ((*it)->GetName() == event_name)
	//		{
	//			CMessage findmsg = (*it)->ToMsg();
	//			if (CParam* pParam = findmsg.ParamByName(L"CallbackID"))
	//			{
	//				if (pParam->AsInt64() == nParentID)
	//				{
	//					m_EvtQ.erase(it);
	//					break;
	//				}
	//			}
	//		}
	//	}
	//}

	//void CSession::AssignAndWaitEvents(const EventWPtr& aCurrEvt, CMessage* pPostmsg, WaitingEventsContainer& aEvents)
	//{
	//	EventMessages messages;
	//	if (pPostmsg)
	//	{
	//		messages.emplace_back(*pPostmsg);
	//	}

	//	AssignAndWaitEvents(aCurrEvt, messages, aEvents);
	//}

	void CSession::AssignEvents(
		const ItemEventPtr& aCurrEvt,
		WaitingEventsContainer&& aEvents)
	{
		for (const auto& curEvent : aEvents)
		{
			m_Log->LogInfo( L"Waiting for event \"%s\"...", curEvent.first.c_str());
		}

		m_dispatcher->SetWaitingEvents(aCurrEvt, std::move(aEvents));
	}

	void CSession::WaitEvents(
		const EventMessages& aMessages)
	{
		SetState(VMS_WAITING);

		ObjectId nParentID = GetParentCallID();
		VXMLString callIdName = IsVox() ? L"VoxID" : L"CallID";

		for (auto msg : aMessages)
		{
			msg[callIdName.c_str()] = nParentID;
			msg[L"CallbackID"] = nParentID;
			msg[L"ScriptID"] = m_InitMsg[L"ScriptID"].Value;

			if (!msg.IsParam(L"DestinationAddress"))
			{
				msg[L"DestinationAddress"] = nParentID;
			}

			SendAuxMsg(msg);
		}
	}

	void CSession::AssignAndWaitEvents(
		const ItemEventPtr& aCurrEvt, 
		const EventMessages& aMessages, 
		WaitingEventsContainer&& aEvents)
	{
		AssignEvents(aCurrEvt, std::move(aEvents));

		WaitEvents(aMessages);
	}

	bool CSession::IsVox() const
	{
		return m_InitMsg.ParamByName(L"VoxID") != nullptr;
	}

	void CSession::DoAsyncEvent(const VXMLString& aEvent, const AsyncInfo& aInfo)
	{
		EventMessages messages;
		
		WaitingEventsContainer waiting_events_with_handle;
		
		m_pAsyncRequests[aInfo.RequestId] = aInfo;

		if (aInfo.Event == E_ASYNC_EVENT::AE_REQUEST)
		{
			waiting_events_with_handle.emplace(L"ANY2AC_asyncHttpRequestAck", boost::bind(&CSession::MakeEvent_AsyncHttpRequestAck, this, aInfo, _1, _2));

			if (!aInfo.FetchAudio.empty())
			{
				messages.emplace_back(CreatePlayWavMessage(aInfo.FetchAudio));

				waiting_events_with_handle.emplace(L"PLAY_WAV_ACK", boost::bind(&CSession::MakeEvent_FetchingPlayWavAck, this, aInfo, _1, _2));
				waiting_events_with_handle.emplace(L"PLAY_WAV_COMPLETED", boost::bind(&CSession::MakeEvent_FetchingPlayWavCompleted, this, aInfo, _1, _2));
			}

			messages.emplace_back(
				m_asyncClient->Create_ANY2AC_asyncHttpRequest(
					aInfo.ScriptId,
					aInfo.RequestId,
					true,
					aInfo.Target,
					aInfo.Port,
					aInfo.Body,
					!aInfo.FetchAudio.empty()));

		}
		else if (aInfo.Event == E_ASYNC_EVENT::AE_GET)
		{
			waiting_events_with_handle.emplace(L"AC2R_asyncHttpResponse", boost::bind(&CSession::MakeEvent_AsyncHttpResponse, this, aInfo, _1, _2));

			messages.emplace_back(
				m_asyncClient->Create_ANY2AC_getResult(
				aInfo.RequestId,
				aInfo.IsCancel));
		}
		else
		{
			waiting_events_with_handle.emplace(aEvent, boost::bind(&CSession::MakeEvent_AsyncCommonHttpResponse, this, _1, _2));

			messages.emplace_back(
				m_asyncClient->CreateCommon(
				aInfo.ScriptId,
				aEvent,
				aInfo.Body));
		}

		if (messages.empty())
		{
			DoErrorBadfetch(L"SESSION",
				BadfetchErrorsToString(BadfetchErrors::BE_ASYNC), 
				L"send", 
				L"Create AsyncMessage failed");
			return;
		}

		AssignAndWaitEvents(m_ExecContext.pEvt, messages, std::move(waiting_events_with_handle));
	}

	void CSession::DoSyncEvent(const VXMLString& aEvent, const AsyncInfo& aInfo)
	{
		EventMessages messages;

		WaitingEventsContainer waiting_events_with_handle;

		m_pAsyncRequests[aInfo.RequestId] = aInfo;

		if (aInfo.Event == E_ASYNC_EVENT::AE_REQUEST)
		{
			if (aInfo.IsPlayAudio)
			{
				messages.emplace_back(CreatePlayWavMessage(aInfo.FetchAudio));

				waiting_events_with_handle.emplace(L"PLAY_WAV_ACK", boost::bind(&CSession::MakeEvent_FetchingPlayWavAck, this, aInfo, _1, _2));
				waiting_events_with_handle.emplace(L"PLAY_WAV_COMPLETED", boost::bind(&CSession::MakeEvent_FetchingPlayWavCompleted, this, aInfo, _1, _2));
			}

			waiting_events_with_handle.emplace(L"ANY2AC_asyncHttpRequestAck", boost::bind(&CSession::MakeEvent_SyncHttpRequestAck, this, aInfo, _1, _2));
			waiting_events_with_handle.emplace(L"AC2R_asyncHttpResponse", boost::bind(&CSession::MakeEvent_SyncHttpResponse, this, aInfo, _1, _2));

			messages.emplace_back(
				m_asyncClient->Create_ANY2AC_asyncHttpRequest(
					aInfo.ScriptId,
					aInfo.RequestId,
					false,
					aInfo.Target,
					aInfo.Port,
					aInfo.Body,
					!aInfo.FetchAudio.empty()));
		}
		else if (aInfo.Event == E_ASYNC_EVENT::AE_GET)
		{
			DoErrorBadfetch(L"SESSION",
				BadfetchErrorsToString(BadfetchErrors::BE_ASYNC),
				L"send", 
				L"Get Sync Request not implemented");
			return;
		}
		else
		{
			waiting_events_with_handle.emplace(aEvent, boost::bind(&CSession::MakeEvent_AsyncCommonHttpResponse, this, _1, _2));

			messages.emplace_back(
				m_asyncClient->CreateCommon(
					aInfo.ScriptId,
					aEvent,
					aInfo.Body));
		}

		if (messages.empty())
		{
			DoErrorBadfetch(L"SESSION",
				BadfetchErrorsToString(BadfetchErrors::BE_ASYNC),
				L"send", 
				L"Create SyncMessage failed");
			return;
		}

		AssignAndWaitEvents(m_ExecContext.pEvt, messages, std::move(waiting_events_with_handle));
	}

	void CSession::DoStep()
	{
		CMessage errorMsg;
		bool isException = false;
		try
		{
			if ((GetState() == VMS_READY))
			{
				int test123 = 0;

				if (test123)
				{
					CMessage msg(L"SomeMsg");
					this->SetEvent(msg);
				}

				if (!m_dispatcher->DoEventStep(&m_ExecContext))
				{
					SetState(VMStates::VMS_WAITING);
				}
			}
		}
		catch (...)
		{
			m_Log->LogWarning(L"Exception: while do next step");
			errorMsg = ExceptionHandler();
			isException = true;
		}

		if (isException)
		{
			try
			{
				DoError(errorMsg, L"SESSION");
			}
			catch (std::exception& error)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: %s", stow(error.what()).c_str());
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
			catch (...)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: unhandled");
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
		}
	}

	void CSession::SetEvent(const CMessage& msg)
	{
		m_Log->LogFinest( L"Dump CMessage: \"%s\"", msg.Dump().c_str());

		CMessage errorMsg;
		bool isException = false;
		try
		{
			//EventPtr curEvent = m_dispatcher->GetEventByMsg(msg); //if waiting this event
			//if (!curEvent)
			//{
			//	curEvent = m_ExecContext.pEvt;
			//}

			m_dispatcher->SetScopeStack(&m_ExecContext, m_ExecContext.pEvt->GetScopeStack());
			TranslateMsg(msg);
		}
		catch (...)
		{
			m_Log->LogWarning(L"Exception: while do next step");
			errorMsg = ExceptionHandler();
			isException = true;
		}

		if (isException)
		{
			try
			{
				DoError(errorMsg, L"SESSION");
			}
			catch (std::exception& error)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: %s", stow(error.what()).c_str());
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
			catch (...)
			{
				m_extraLog->LogWarning(L"Exception: while DoError: unhandled");
				DoExit();
				SetState(VMS_ENDSCRIPT);
			}
		}
	}

	void CSession::TranslateMsg(const CMessage& msg)
	{
		// Connection specific messages
		if (const CParam* param = msg.ParamByName(L"AuxEvent"))
		{
			return; // massage has already send
			//DbgEvent(msg);
		}

		// Connection specific messages
		else if (//msg == L"PLAY_WAV_ACK"           ||
		    //msg == L"DIGIT_GOT"              ||
			msg == L"MAIN"					 ||
			msg == L"DROPED"				 ||
			msg == L"GET_DIGITS_ACK"         ||
			msg == L"STOP_VIDEO_ACK"         ||
			msg == L"STOP_CHANNEL_ACK"       ||
			//msg == L"STOP_CHANNEL_COMPLETED" ||
			msg == L"CLEAR_DIGITS_ACK"       ||
			msg == L"EXTENSION"              ||
			msg == L"REC_WAV_ACK"   		   )
		{
			// just skip unuseful messages 
		}
		else if (msg == L"KEEP_ALIVE")
		{
			auto counter = m_keepAlive->IncreaseDeadCounter();
			m_Log->LogFinest( L"Increase dead counter: %i", counter);

			if (counter > DEADTIMER_LIMIT)
			{
				m_Log->LogWarning(L"Dead counter has reached the limit! Terminate dialog");
				CMessage terminate(L"TERMINATE");
				terminate[L"immediate"] = true;
				SendEvent(terminate, m_InitMsg[L"ScriptID"].AsInt64());
			}
		}
		else if (msg == L"KEEP_ALIVE_ACK")
		{
			m_keepAlive->ResetDeadCounter();
		}

		//else if (msg == L"PLAY_WAV_ACK")
		//{
		//	//if(CParam* param = msg.ParamByName(L"ErrorCode"))
		//	//{
		//	//	msg.SetName(L"PLAY_WAV_COMPLETED");
		//	//	MakeEvent_MenuWav(msg);
		//	//}

		//}
		//else if (msg == L"PLAY_WAV_COMPLETED")
		//{
		//	MakeEvent_PlayWavCompleted(msg);
		//}
		//else if (msg == L"GET_DIGITS_COMPLETED")
		//{
		//	MakeEvent_GetDigitsCompleted(msg);
		//}
		//else if (msg == L"CLEAR_DIGITS_COMPLETED")
		//{
		//	MakeEvent_ClearDigits(msg);
		//	//SetState(VMS_READY);
		//}
		//else if (msg == L"VXML_RUN_SCRIPT_OK")
		//{
		//	MakeEvent_VXMLRunScriptOK(msg);
		//}
		//else if (msg == L"VXML_RUN_SCRIPT_FAILED")
		//{
		//	MakeEvent_VXMLRunScriptFAILED(msg);
		//}
		//else if (msg == L"END_DIALOG")
		//{
		//	MakeEvent_EndDialog(msg);
		//}
		//else if (msg == L"DIGIT_GOT")
		//{
		//	//VAR* pCurDTMF = m_ExecContext.pVar->Find(L"bargein");
		//	//if (pCurDTMF && pCurDTMF->vValue.boolVal == FALSE)
		//	//{
		//	//	CMessage stop_channel_msg(L"STOP_CHANNEL");
		//	//	SendEvent(stop_channel_msg, GetParentCallID());

		//	//	m_ExecContext.pVar->DelVar(L"bargein");
		//	//}

		//	ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		//	if (!pVar)
		//	{
		//		throw std::range_error("Empty event scope");
		//	}

		//	VAR* pCurDTMF = pVar->Find(L"bargein");
		//	if (pCurDTMF && pCurDTMF->vValue.boolVal == TRUE)
		//	{
		//		CMessage stop_channel_msg(L"STOP_CHANNEL");
		//		SendEvent(stop_channel_msg, GetParentCallID());

		//		//m_ExecContext.pVar->DelVar(L"bargein");
		//		pCurDTMF->vValue.boolVal = FALSE;
		//	}

		//}
		//else if (msg == L"END_DIALOG"/* && m_bSubDialogInProcess*/ || msg == L"TERMINATED_DIALOG")
		//{
		//	//m_bSubDialogInProcess = false;
		//	MakeEvent_SubDialogEnd(msg);
		//}
		else if (msg == L"DISCONNECTED")
		{
			MakeEvent_Disconnected(msg, m_dispatcher->GetRecursiveEvent());
		}
		else if (msg == L"PLAY_VIDEO_ACK")
		{
			//MakeEvent_VideoAcknowledge(msg);
		}
		else if (msg == L"PLAY_VIDEO_COMPLETED")
		{
			//MakeEvent_VideoCompleted(msg);
		}
		else if (msg == L"STOP_VIDEO_COMPLETED")
		{
			//MakeEvent_StopVideoCompleted(msg);
		}
		else if (msg == L"STOP_CHANNEL_COMPLETED") // ??
		{
			//MakeEvent_StopChannelCompleted(msg);
		}
		else if (msg == L"REC_WAV_COMPLETED")
		{
			//MakeEvent_RecWav(msg);
		}
		else if (msg == L"LAUNCHER")
		{
			//if (!m_DialogActive)
			//{
			//	//m_DialogActive = true;
			//	//ExecuteDoc();
			//	InitializeCurTag();
			//}
		}
		else if (msg == L"TERMINATE")
		{
			bool immediate = msg.SafeReadParam(L"immediate", CMessage::Bool, true).AsBool();
			if (immediate)
			{
				DoExit();
			}
			else
			{
				DoTerminated(
					msg.SafeReadParam(L"namelist", CMessage::String, L"").AsWideStr(),
					msg.SafeReadParam(L"handler", CMessage::String, L"").AsWideStr());
			}
		}
		else
		{
			MessageHandler eventHandler;
			auto curEvent = m_dispatcher->GetEventByMsg(msg);
			if (curEvent)
			{
				eventHandler = curEvent->GetMessageHandlerByName(msg.GetName());
			}
			if (eventHandler)
			{
				//invoke handler
				eventHandler(msg, curEvent);
			}
			else
			{
				if (const CParam *pTargetType = msg.ParamByName(L"targettype"))
				{
					VXMLString sTargetType = pTargetType->AsWideStr();
					if (sTargetType == L"dialog")
					{
						EmitSubEvent(msg);
						return;
					}
				}
				CMessage evt((VXMLString(L"external.") + msg.Name).c_str());
				evt[L"event_name"] = msg.Name;
				EmitSubEvent(evt += msg);
			}
		}


	}

	//CMessage CSession::MakeCommonEvent(CMessage& msg, VXMLString message)
	//{
	//	CMessage evt((message).c_str());
	//	return evt;
	//}

	//void CSession::MakeEvent_MenuWav(CMessage& msg)
	//{
	//	CMessage evt(MakeCommonEvent(msg,L"wav.completed"));
	//	evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
	//	evt[L"TerminationReason"]	= msg[L"TerminationReason"].Value;
	//	evt[L"DigitsBuffer"]		= msg[L"DigitsBuffer"].Value;
	//	EmitEvent(evt);
	//}

	//void CSession::MakeEvent_MenuDigits(CMessage& msg)
	//{
	//	CMessage evt(MakeCommonEvent(msg,L"get_digits.completed"));
	//	evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
	//	evt[L"DigitsBuffer"]				= msg[L"DigitsBuffer"].Value;
	//	evt[L"TerminationReason"]	= msg[L"TerminationReason"].Value;
	//	EmitEvent(evt);
	//}

	void CSession::MakeEvent_ClearDigits(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		m_dispatcher->SetState(curEvent, VMStates::VMS_READY);

		SetState(VMS_READY);
	}

	void CSession::MakeEvent_Disconnected(const CMessage& msg, const ItemEventPtr& aCurEvent)
	{
		CMessage disconnect_msg(msg);
		SendEvent(disconnect_msg, m_InitMsg[L"ParentScriptID"].AsInt64());

		TagWPtr nextMenuTag;
		ScopeServicePtr newScope;
		VXMLString curMenuName;

		MenuPtr menu = aCurEvent->GetMenu();

		if (aCurEvent)
		{
			TagPtr beginTag = aCurEvent->getBeginPoint().lock();
			if (beginTag)
			{
				disconnect_msg.SetName(L"connection.disconnect.hangup");
				menu->InputEvent = L"connection.disconnect.hangup";

				newScope = std::make_shared<CScopeService>(*m_ExecContext.pVar.lock());
				newScope->PushScope(std::make_shared<CNameTable>(L"disconnected_scope"));

				nextMenuTag = beginTag->ExecEvents(&m_ExecContext, disconnect_msg);
				curMenuName = beginTag->GetAttrIdName();
			}
		}

		if (nextMenuTag.lock().get())
		{
			m_dispatcher->DeleteAllSubEvents();

			WaitingEventsContainer waiting_events_with_handle;
			AssignEvents(aCurEvent, std::move(waiting_events_with_handle));

			EmitSubEvent(curMenuName, menu, nextMenuTag, newScope);
			SetState(VMS_READY);
		}
		else
		{
			DoExit(L"");
		}

		m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
	}
	

	//void CSession::MakeEvent_SubDialogEnd(CMessage& msg)
	//{
	//	CMessage evt(MakeCommonEvent(msg,L"SUB_DIALOG_END"));
	//	evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
	//	evt[L"ScriptID"]			= msg[L"ScriptID"].Value;
	//	if (CParam* paramName  = msg.ParamByName(L"namelist"))
	//	{
	//		evt[L"namelist"] = msg[L"namelist"].Value;
	//		VXMLString sNameList = paramName->AsWideStr();
	//		LPCWSTR p = sNameList.c_str(), q;
	//		VXMLString sParamName;

	//		while (*p)
	//		{
	//			q = wcschr(p, L' ');
	//			if (!q)
	//			{
	//				sParamName = trim_wstring(p);
	//			}
	//			else
	//			{
	//				sParamName = trim_wstring(VXMLString(p, q - p));
	//			}
	//			if (!sParamName.empty())
	//			{
	//				evt[sParamName.c_str()]=m_ExecContext.pVbs->ExprEvalToStr(sParamName).c_str();
	//			}
	//			if (!q)
	//			{
	//				break;
	//			}
	//			p = q + 1;
	//		}
	//	}
	//	//evt[L"res_name"]			= msg[L"res_name"].Value;
	//	//evt[L"res_value"]			= msg[L"res_value"].Value;
	//	EmitEvent(evt);
	//}

	void CSession::MakeEvent_VXMLRunScriptOK(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), msg.ParamByName(L"ScriptID")->AsInt64());

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"END_DIALOG", boost::bind(&CSession::MakeEvent_EndDialog, this, _1, _2));

		AssignEvents(curEvent, std::move(waiting_events_with_handle));
	}

	void CSession::MakeEvent_VXMLRunScriptFAILED(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		VXMLString sSource;
		if (const CParam *param = msg.ParamByName(L"FileName"))
			sSource = param->AsWideStr();

		MakeVXMLRunScriptFailed(sSource);

		m_dispatcher->SetState(curEvent, VMStates::VMS_READY);

		SetState(VMS_READY);
	}

	void CSession::MakeEvent_EndDialog(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		//EventWPtr curEvent = m_dispatcher->GetEventByMsg(msg);
		//if (!curEvent.lock().get())
		//	return;

		m_dispatcher->SetState(curEvent, VMStates::VMS_READY);

		//Set namelist
		if (const CParam* paramName = msg.ParamByName(L"namelist"))
		{
			std::wistringstream in(paramName->AsWideStr());
			std::istream_iterator<VXMLString, wchar_t> first(in), last;
			for (; first != last; ++first)
			{
				m_pDialog->Add((*first), m_InitMsg[(*first).c_str()].AsWideStr().c_str(), false);

				VXMLString statement = (*first) + L"=\"" + msg[(*first).c_str()].AsWideStr() + L"\"";
				if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement.c_str())))
					throw(statement.c_str());

			}
		}

		SetState(VMS_READY);
	}

	void CSession::MakeEvent_DigitGot(const CMessage &, const ItemEventPtr& curEvent)
	{
		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		VAR* pCurDTMF = pVar->Find(L"bargein");
		if (pCurDTMF && pCurDTMF->vValue.boolVal == VARIANT_TRUE)
		{
			CMessage stop_channel_msg(L"STOP_CHANNEL");
			SendEvent(stop_channel_msg, GetParentCallID());

			pVar->DelVar(L"bargein");
			pCurDTMF->vValue.boolVal = VARIANT_FALSE;
		}
	}

	void CSession::MakeEvent_Terminated(const CMessage &, const ItemEventPtr & curEvent)
	{
	}

	void CSession::MakeEvent_RecWav(CMessage& msg)
	{
		//CMessage evt(MakeCommonEvent(msg,L"rec_wav.completed"));
		//evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		//evt[L"TerminationReason"]	= msg[L"TerminationReason"].Value;
		//evt[L"DigitsBuffer"]		= msg[L"DigitsBuffer"].Value;
		//EmitEvent(evt);
	}

	//void CSession::MakeEvent_AsyncGetResult(const CMessage &, const EventWPtr & curEvent)
	//{
	//	//if (!SUCCEEDED(pCtx->pVbs->ExecStatement(requestId + L"=\"" + requestIdValue + L"\"")))
	//	//{
	//	//	pCtx->pLog->LogWarning(L"ExecStatement failed: requestId = \"%s\"", requestIdValue.c_str());
	//	//}

	//}

	void CSession::MakeEvent_AsyncHttpRequestAck(
		AsyncInfo aAsyncInfo, 
		const CMessage &aMsg, 
		const ItemEventPtr & aCurEvent)
	{
		VXMLString requestIdValue;
		if (const auto param = aMsg.ParamByName(L"RequestId"))
		{
			requestIdValue = param->AsWideStr();
		}
		else
		{
			throw std::runtime_error("No RequestId in ANY2AC_asyncHttpRequestAck event");
		}

		m_pLastMessage->Add(L"requestid", requestIdValue.c_str(), false);
		m_pLastMessage->Add(L"status", 0, false);
		m_pLastMessage->Add(L"statusstring", L"Unknown", false);

		JobStatus status = JobStatus::Unknown;

		if (const CParam* param = aMsg.ParamByName(L"Status"))
		{
			status = static_cast<JobStatus>(param->AsInt());
		}

		bool isAudioFinished = false;
		if (status == JobStatus::Error)
		{
			MakeErrorAsyncAckMsg(aMsg);
		}
		else
		{
			auto it = m_pAsyncRequests.find(requestIdValue);
			if (it == m_pAsyncRequests.end())
			{
				throw std::runtime_error(std::string("Unknown request id: ") + wtos(requestIdValue));
			}
			auto& request = it->second;
			request.IsSyncAckCome = true;
			isAudioFinished = request.IsAudioFinished;
		}

		if (aAsyncInfo.IsPlayAudio && !isAudioFinished)
		{
			CMessage msg_stopchannel(L"STOP_CHANNEL");
			SendEvent(msg_stopchannel, GetParentCallID());
		}

		m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
		SetState(VMS_READY);
	}

	void CSession::MakeEvent_SyncHttpRequestAck(
		AsyncInfo aAsyncInfo, 
		const CMessage & aMsg, 
		const ItemEventPtr & aCurEvent)
	{
		VXMLString requestIdValue;
		if (const auto param = aMsg.ParamByName(L"RequestId"))
		{
			requestIdValue = param->AsWideStr();
		}
		else
		{
			throw std::runtime_error("No RequestId in ANY2AC_asyncHttpRequestAck event");
		}

		m_pLastMessage->Add(L"requestid", requestIdValue.c_str(), false);
		m_pLastMessage->Add(L"status", 0, false);
		m_pLastMessage->Add(L"statusstring", L"Unknown", false);

		JobStatus status = JobStatus::Unknown;

		if (const CParam* param = aMsg.ParamByName(L"Status"))
		{
			status = static_cast<JobStatus>(param->AsInt());
		}

		bool isError = false;
		bool isAudioFinished = false;
		if (status == JobStatus::Error)
		{
			MakeErrorAsyncAckMsg(aMsg);
			isError = true;
		}
		else
		{
			auto it = m_pAsyncRequests.find(requestIdValue);
			if (it == m_pAsyncRequests.end())
			{
				throw std::runtime_error(std::string("Unknown request id: ") + wtos(requestIdValue));
			}
			auto& request = it->second;
			request.IsSyncAckCome = true;
		}

		if (aAsyncInfo.IsPlayAudio && isError)
		{
			CMessage msg_stopchannel(L"STOP_CHANNEL");
			SendEvent(msg_stopchannel, GetParentCallID());
		}

		if (isError)
		{
			m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
		}
	}

	void CSession::MakeEvent_AsyncHttpResponse(
		AsyncInfo aAsyncInfo, 
		const CMessage& aMsg, 
		const ItemEventPtr& aCurEvent)
	{
		m_pLastMessage->Clear();

		if (const CParam* param = aMsg.ParamByName(L"Response"))
		{
			m_pLastMessage->Add(L"response", param->AsWideStr().c_str(), false);
		}

		if (const CParam* param = aMsg.ParamByName(L"RequestId"))
		{
			m_pLastMessage->Add(L"requestid", param->AsWideStr().c_str(), false);
		}

		if (const CParam* param = aMsg.ParamByName(L"Status"))
		{
			m_pLastMessage->Add(L"status", param->AsInt(), false);
		}

		if (const CParam* param = aMsg.ParamByName(L"StatusString"))
		{
			m_pLastMessage->Add(L"statusstring", param->AsWideStr().c_str(), false);
		}

		m_Log->LogInfo( L"DUMP Async message: %s", m_pLastMessage->ToMsg().Dump().c_str());

		auto it = m_pAsyncRequests.find(aAsyncInfo.RequestId);
		if (it == m_pAsyncRequests.end())
		{
			throw std::runtime_error(std::string("Unknown request id: ") + wtos(aAsyncInfo.RequestId));
		}
		auto& request = it->second;
		request.IsSyncAckCome = true;

		if (request.IsPlayAudio && !request.IsAudioFinished)
		{
			CMessage msg_stopchannel(L"STOP_CHANNEL");
			SendEvent(msg_stopchannel, GetParentCallID());
		}

		m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
		SetState(VMS_READY);
	}


	void CSession::MakeEvent_SyncHttpResponse(
		AsyncInfo aAsyncInfo, 
		const CMessage& aMsg, 
		const ItemEventPtr& aCurEvent)
	{
		m_pLastMessage->Clear();

		if (const CParam* param = aMsg.ParamByName(L"Response"))
		{
			m_pLastMessage->Add(L"response", param->AsWideStr().c_str(), false);
		}

		if (const CParam* param = aMsg.ParamByName(L"RequestId"))
		{
			m_pLastMessage->Add(L"requestid", param->AsWideStr().c_str(), false);
		}

		if (const CParam* param = aMsg.ParamByName(L"Status"))
		{
			m_pLastMessage->Add(L"status", param->AsInt(), false);
		}

		if (const CParam* param = aMsg.ParamByName(L"StatusString"))
		{
			m_pLastMessage->Add(L"statusstring", param->AsWideStr().c_str(), false);
		}

		m_Log->LogInfo( L"DUMP Async message: %s", m_pLastMessage->ToMsg().Dump().c_str());

		auto it = m_pAsyncRequests.find(aAsyncInfo.RequestId);
		if (it == m_pAsyncRequests.end())
		{
			throw std::runtime_error(std::string("Unknown request id: ") + wtos(aAsyncInfo.RequestId));
		}
		auto& request = it->second;
		request.IsSyncAckCome = true;

		if (request.IsPlayAudio)
		{
			CMessage msg_stopchannel(L"STOP_CHANNEL");
			SendEvent(msg_stopchannel, GetParentCallID());
		}

		m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
		SetState(VMS_READY);
	}

	void CSession::MakeEvent_AsyncCommonHttpResponse(const CMessage &, const ItemEventPtr & curEvent)
	{
	}

	void CSession::MakeEvent_FetchingPlayWavCompleted(
		AsyncInfo aAsyncInfo, 
		const CMessage& aMsg, 
		const ItemEventPtr & curEvent)
	{
		auto it = m_pAsyncRequests.find(aAsyncInfo.RequestId);
		if (it == m_pAsyncRequests.end())
		{
			throw std::runtime_error(std::string("Unknown request id: ") + wtos(aAsyncInfo.RequestId));
		}
		auto& request = it->second;

		if (aAsyncInfo.IsAsync)
		{
			request.IsAudioFinished = true;
			if (request.IsSyncAckCome)
			{
				DoClearDigits();
			}
		}
		else
		{
			if (!request.IsSyncAckCome && aAsyncInfo.IsPlayAudio && !aAsyncInfo.FetchAudio.empty())
			{
				SendEvent(CreatePlayWavMessage(aAsyncInfo.FetchAudio), GetParentCallID());
			}
		}
	}

	void CSession::MakeEvent_FetchingPlayWavAck(
		AsyncInfo aAsyncInfo, 
		const CMessage & aMsg, 
		const ItemEventPtr & curEvent)
	{
		if (const CParam* param = aMsg.ParamByName(L"ErrorCode"))
		{
			CMessage newMsg(L"PLAY_WAV_COMPLETED");
			newMsg += aMsg;
			newMsg[L"TerminationReason"] = L"TM_ERROR";
			MakeEvent_FetchingPlayWavCompleted(aAsyncInfo, newMsg, curEvent);
		}
	}


	bool CSession::DigitsEmit(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		//m_dispatcher->SetState(curEvent, VMStates::VMS_READY);

		VXMLString _digits;
		if (const CParam* pParam = msg.ParamByName(L"DigitsBuffer"))
			_digits = pParam->AsString();

		if (_digits.empty())
			return false;

		MenuPtr menu = curEvent->GetMenu();

		if (!menu->PreviousUtterance.empty())
		{
			_digits = menu->PreviousUtterance + _digits;
			menu->PreviousUtterance.clear();
		}

		menu->Utterance = _digits;
		menu->InputEvent = L"nomatch";

		VXMLString statement = menu->MenuName + L"=\"" + _digits + L"\"";
		if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement.c_str())))
		{
			m_ExecContext.pLog->LogWarning(L"ExecStatement failed: %s", statement.c_str());
		}


		////set digits
		//TagPtr beginTag = curEvent->getBeginPoint().lock();
		//if (!beginTag.get())
		//	return false;

		//VXMLString attrId = beginTag->GetAttrIdName();
		//if (!attrId.size())
		//	return false;

		//SessionObject pMenu = FindMenu(attrId);
		//if (pMenu.get())
		//{
		//	VXMLString previousUtterance;
		//	if (VAR* pPreviousUtterance = pMenu->Find(L"PreviousUtterance"))
		//	{
		//		previousUtterance = _bstr_t(pPreviousUtterance->vValue.bstrVal);
		//		pMenu->Del(L"PreviousUtterance");
		//	}

		//	if (!previousUtterance.empty())
		//	{
		//		_digits = previousUtterance + _digits;
		//	}

		//	pMenu->Add(L"utterance", _digits.c_str(), false);
		//	pMenu->Add(L"event", L"nomatch", true);

		//	VXMLString statement = attrId + L"=\"" + _digits + L"\"";
		//	if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement.c_str())))
		//	{
		//		m_ExecContext.pLog->LogWarning(L"ExecStatement failed: %s", statement.c_str());
		//	}
		//}

		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		pVar->AddVar(L"utterance", _digits.c_str(), false);

		return true;
	}

	bool CSession::VoiceEmit(const CMessage & aMrcpMessage, const ItemEventPtr & aCurEvent)
	{
		bool bRecognizeInput = false;

		VXMLString utterance, confidence, result(L"filled"), meaning, recognitionOrigin, recognitionLocal;
		if (const CParam* pParam = aMrcpMessage.ParamByName(L"input"))
		{
			utterance = pParam->AsWideStr();
		}

		if (const CParam *param = aMrcpMessage.ParamByName(L"confidence"))
		{
			confidence = param->AsWideStr();
		}
		if (const CParam *param = aMrcpMessage.ParamByName(L"result"))
		{
			result = param->AsWideStr();
		}
		if (const CParam *param = aMrcpMessage.ParamByName(L"swi_meaning"))
		{
			meaning = param->AsWideStr();
		}

		if (const CParam *param = aMrcpMessage.ParamByName(L"WaveformUri"))
		{
			recognitionOrigin = param->AsWideStr();
		}

		//if (result == L"nomatch" || result == L"filled") // USRL-541
		//{
		//	recognitionLocal = SendMASRecieveFilesMessage(recognitionOrigin, confidence, result == L"filled");
		//}

		bRecognizeInput = !utterance.empty();

		std::wstring classId;

		if (bRecognizeInput)
		{
			CARSClient client(
				m_Log, 
				m_Text2SpeechSettings.sVnHosts, 
				m_Text2SpeechSettings.sVnUrl,
				m_Text2SpeechSettings.szVnTimeout);
			
			if (!client.Classify(utterance, classId))
			{
				throw std::runtime_error("APP-servers are not avaiable");
			}
		}

		MenuPtr menu = GetCurMenu();
		menu->Utterance = utterance;
		menu->ClassId = classId;
		menu->Confidence = confidence;
		menu->MrcpResult = result;
		menu->Meaning = meaning;
		menu->Recognition = recognitionOrigin;
		menu->InputEvent = L"nomatch";

		VXMLString statement;
		if (!classId.empty())
		{
			statement = menu->MenuName + L"=\"" + classId + L"\"";
		}

		if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement.c_str())))
		{
			m_ExecContext.pLog->LogWarning(L"ExecStatement failed: %s", statement.c_str());
		}



		//VXMLString attrId = GetCurMenu(aCurEvent);

		//if (attrId.empty())
		//{
		//	return false;
		//}

		//SessionObject pMenu = FindMenu(attrId);
		//if (pMenu.get())
		//{
		//	pMenu->Add(L"utterance", utterance.c_str(), false);
		//	pMenu->Add(L"classid", classId.c_str(), false);
		//	pMenu->Add(L"confidence", confidence.c_str(), false);
		//	pMenu->Add(L"result", result.c_str(), false);
		//	pMenu->Add(L"meaning", meaning.c_str(), false);
		//	pMenu->Add(L"recognition", recognitionOrigin.c_str(), false);
		//	//pMenu->Add(L"recognitionlocal", recognitionLocal.c_str(), false);
		//	pMenu->Add(L"event", L"nomatch", true);

		//	VXMLString statement;
		//	if (!classId.empty())
		//	{
		//		statement = attrId + L"=\"" + classId + L"\"";
		//	}
		//	
		//	if (!SUCCEEDED(m_ExecContext.pVbs->ExecStatement(statement.c_str())))
		//	{
		//		m_ExecContext.pLog->LogWarning(L"ExecStatement failed: %s", statement.c_str());
		//	}
		//}

		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		pVar->AddVar(L"utterance", utterance.c_str(), false);
		pVar->AddVar(L"classid", classId.c_str(), false);
		pVar->AddVar(L"confidence", confidence.c_str(), false);
		pVar->AddVar(L"result", result.c_str(), false);
		pVar->AddVar(L"meaning", meaning.c_str(), false);
		pVar->AddVar(L"recognition", recognitionOrigin.c_str(), false);
		pVar->AddVar(L"recognitionlocal", recognitionLocal.c_str(), false);

		return bRecognizeInput;
	}

	void CSession::MakeEvent_PlayWavAck(const CMessage& aMsg, const ItemEventPtr& curEvent)
	{
		if (const CParam* param = aMsg.ParamByName(L"ErrorCode"))
		{
			CMessage newMsg(L"PLAY_WAV_COMPLETED");
			newMsg += aMsg;
			newMsg[L"TerminationReason"] = L"TM_ERROR";
			MakeEvent_PlayWavCompleted(newMsg, curEvent);

			// USRL-190
			//CMessage stop_channel_msg(L"STOP_CHANNEL");
			//SendEvent(stop_channel_msg, GetParentCallID());

			//m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			//SetState(VMS_READY);

			//DoError(MakeErrorTTSAckMsg(aMsg), L"SESSION");
		}
	}

	void CSession::MakeEvent_PlayWavBreak(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		//set incoming message like digits
		CMessage fake_digits_msg(msg);
		fake_digits_msg[L"DigitsBuffer"] = msg.GetName();

		CMessage stop_channel_msg(L"STOP_CHANNEL");
		SendEvent(stop_channel_msg, GetParentCallID());

		if (DigitsEmit(fake_digits_msg, curEvent))
		{
			DoClearDigits();
		}
		else
		{
			SetState(VMS_READY);
		}
	}

	void CSession::MakeEvent_PlayWavCompleted(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		const CParam* pParam = msg.ParamByName(L"TerminationReason");
		if (!pParam)
		{
			throw std::logic_error("There are no TerminationReason in PLAY_WAV_COMPLETED");
		}

		// There are 4 ways which discribe how PLAY_WAV_COMPLETED comes:
		// 1. Abonent has pressed "digit" and "term digit" ->
		//     -> we got PLAY_WAV_COMPLETED with TerminationReason == TM_DIGIT && not empty DigitsBuffer
		// 2. Abonent has pressed only "digit", PLAY_WAV has finished ->
		//     -> we got PLAY_WAV_COMPLETED with TerminationReason == TM_EOD && not empty DigitsBuffer
		// 3. Abonent has pressed nothing, PLAY_WAV has finished ->
		//     -> we got PLAY_WAV_COMPLETED with TerminationReason == TM_EOD && empty DigitsBuffer
		// 4. In case BargeIn = true, abonent pressed "digit", STOP_CHANNEL has sent ->
		//     -> we got PLAY_WAV_COMPLETED with TerminationReason == TM_USRSTOP && not empty DigitsBuffer

		bool bDigitsSuccessfulEmit = false;
		if (pParam->AsString() == L"TM_DIGIT" 
			|| pParam->AsString() == L"TM_EOD"
			|| pParam->AsString() == L"TM_USRSTOP")
		{
			bDigitsSuccessfulEmit = DigitsEmit(msg, curEvent);
		}

		if (!curEvent->isRecursiveEvent())
		{
			// any catch event -> just return 
			m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
			return;
		}

		// 1. Complete audio event
		MenuPtr menu = GetCurMenu();
		menu->RecInfo.PlayWavCompleted = true;

		bool bGetInput = false;

		// ToDo: optimization here
		if (menu->InputMode == L"voice")
		{
			bGetInput = menu->RecInfo.RecognizeCompleted;
		}
		else
		{
			bGetInput = true;
		}

		//VXMLString currMenuName = GetCurMenu(curEvent);
		//if (currMenuName.empty())
		//{
		//	// any catch event -> just return 
		//	m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
		//	SetState(VMS_READY);
		//	return;
		//}
		//SessionObject pMenu = FindMenu(currMenuName);
		//if (!pMenu)
		//{
		//	throw std::runtime_error(format_string("MakeEvent_PlayWavCompleted: Cannot find menu \"%s\"", wtos(currMenuName)));
		//}

		//pMenu->Add(L"PlayWavCompleted", true, false);

		//// 2. Check completed input
		//bool bGetInput = false;
		//if (IsVoiceInputMode())
		//{
		//	VAR *pRecognizeCompleted = pMenu->Find(L"RecognizeCompleted");
		//	if (pRecognizeCompleted
		//		&& pRecognizeCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
		//	{
		//		bGetInput = true;
		//	}
		//}
		//else
		//{
		//	bGetInput = true; // if dtmf input
		//}

		// 3. finished event
		if (bGetInput)
		{
			if (bDigitsSuccessfulEmit)
			{
				DoClearDigits();
			}
			else
			{
				m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
				SetState(VMS_READY);
			}
		}
	}

	void CSession::MakeEvent_TransferPlayWavCompleted(TransferInfo& outTransferInfo, const CMessage &, const ItemEventPtr& curEvent)
	{
		if (outTransferInfo.DialogTransferAudioRepeat)
		{
			SendEvent(CreatePlayWavMessage(outTransferInfo.DialogTransferAudioUri), GetParentCallID());
		}
	}

	static VXMLString MakeErrorDescription(const CMessage &aMsg)
	{
		int iErrorCode = 0;
		if (const CParam* param = aMsg.ParamByName(L"ErrorCode"))
		{
			iErrorCode = param->AsInt();
		}

		VXMLString reason(L"Unknown error");
		if (const CParam *param = aMsg.ParamByName(L"ErrorDescription"))
		{
			reason = param->AsWideStr();
		}

		return format_wstring(L"%s error:\"%s\", ErrorCode = \"%i\"", aMsg.GetName(), reason.c_str(), iErrorCode);
	}

	void CSession::MakeErrorASRAckMsg(const CMessage &aMsg)
	{
		auto description = MakeErrorDescription(aMsg);

		Log(description.c_str());

		DoErrorBadfetch(
			L"SESSION",
			BadfetchErrorsToString(BadfetchErrors::BE_ASR),
			L"Async-response",
			description);
	}

	void CSession::MakeErrorTTSAckMsg(const CMessage &aMsg)
	{
		auto description = MakeErrorDescription(aMsg);

		Log(description.c_str());

		DoErrorBadfetch(
			L"SESSION",
			BadfetchErrorsToString(BadfetchErrors::BE_TTS),
			L"Text-to-speech",
			description);
	}

	void CSession::MakeErrorAsyncAckMsg(const CMessage & aMsg)
	{
		VXMLString reason(L"Unknown error");
		if (const CParam *param = aMsg.ParamByName(L"Reason"))
		{
			reason = param->AsWideStr();
		}

		VXMLString description = format_wstring(L"%s error:\"%s\"", aMsg.GetName(), reason.c_str());

		Log(description.c_str());

		DoErrorBadfetch(
			L"SESSION",
			BadfetchErrorsToString(BadfetchErrors::BE_ASYNC),
			L"Async send",
			description);
	}

	VXMLString CSession::GetTTSSpeakName() const
	{
		if (m_VXMLSystem.bRPCore)
		{
			return L"MRCP_SPEAK";
		}
		return L"MRCP_TEXT_TO_SPEECH";
	}

	VXMLString CSession::GetTTSSpeakAckName() const
	{
		if (m_VXMLSystem.bRPCore)
		{
			return L"MRCP_SPEAK_ACK";
		}
		return L"MRCP_TEXT_TO_SPEECH_ACK";
	}

	VXMLString CSession::GetTTSSpeakCompletedName() const
	{
		if (m_VXMLSystem.bRPCore)
		{
			return L"MRCP_SPEAK_COMPLETED";
		}
		return L"MRCP_TEXT_TO_SPEECH_COMPLETED";
	}

	VXMLString CSession::GetGrammarPathParamName() const
	{
		if (m_VXMLSystem.bRPCore)
		{
			return L"GrammarPath";
		}
		return L"Dictionary";
	}

	void CSession::MakeEvent_TextToSpeechAck(const CMessage &aMsg, const ItemEventPtr & curEvent)
	{
		if (const CParam* param = aMsg.ParamByName(L"ErrorCode"))
		{
			CMessage stop_channel_msg(L"STOP_CHANNEL");
			SendEvent(stop_channel_msg, GetParentCallID());

			MakeErrorTTSAckMsg(aMsg);

			m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
		}
	}

	void CSession::MakeEvent_TextToSpeechCompleted(const CMessage &, const ItemEventPtr & curEvent)
	{
		if (!curEvent->isRecursiveEvent())
		{
			// any catch event -> just return 
			m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
			return;
		}

		MenuPtr menu = GetCurMenu();
		menu->RecInfo.TextCompleted = true;

		// 2. Check completed input
		bool isRecognizeCompleted = false;
		if (menu->InputMode == L"voice")
		{
			isRecognizeCompleted = menu->RecInfo.RecognizeCompleted;
		}
		else
		{
			isRecognizeCompleted = true;
		}

		// 4. finished event
		if (isRecognizeCompleted)
		{
			m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
		}

		/****************************************/
		// ToDo: dtmf input
		//VAR *pGetInput = pMenu->Find(L"GetInput");
		//if (pGetInput && pGetInput->vValue.boolVal == ATL_VARIANT_TRUE)
		//{
		//	m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
		//	SetState(VMS_READY);
		//}
		//pMenu->Add(L"TextCompleted", true, false);

		//VAR *pGetInput = pMenu->Find(L"GetInput");
		//if (pGetInput && pGetInput->vValue.boolVal == ATL_VARIANT_TRUE)
		//{
		//	m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
		//	SetState(VMS_READY);
		//}
		/****************************************/
	}

	void CSession::MakeEvent_RecognizeAck(const CMessage& aMsg, const ItemEventPtr& curEvent)
	{
		if (const CParam* param = aMsg.ParamByName(L"ErrorCode"))
		{
			CMessage stop_channel_msg(L"STOP_CHANNEL");
			SendEvent(stop_channel_msg, GetParentCallID());

			MakeErrorASRAckMsg(aMsg);

			m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
		}
	}

	void CSession::MakeEvent_RecognizeCompleted(const CMessage &aMsg, const ItemEventPtr& aCurEvent)
	{
		std::pair<bool, VXMLString> error = std::make_pair(false, L"");

		try
		{
			MenuPtr menu = GetCurMenu();
			menu->RecInfo.RecognizeCompleted = true;

			//VXMLString currMenuName = GetCurMenu(aCurEvent);

			//// 1. get current menu object 
			//SessionObject pMenu = FindMenu(currMenuName);
			//if (!pMenu)
			//{
			//	throw std::runtime_error(format_string("MakeEvent_RecognizeCompleted: Cannot find menu \"%s\"", wtos(currMenuName)));
			//}

			//pMenu->Add(L"RecognizeCompleted", true, false);

			// 2. Check voice input
			CMessage mrspMsg = MakeMrcpEvent(aMsg);
			m_Log->LogInfo( L"DUMP MRCP message: %s", mrspMsg.Dump().c_str());
			if (VoiceEmit(mrspMsg, aCurEvent))
			{
				//pMenu->Add(L"RecognizeInput", true, false);
				menu->RecInfo.RecognizeInput = true;
			}

			//3. Check output completed
			bool isAudioMode = IsAudioOutputMode();
			bool isGetOutputCompleted = isAudioMode 
				? menu->RecInfo.PlayWavCompleted 
				: menu->RecInfo.TextCompleted;


			//bool isGetOutputCompleted = false;
			//bool isAudioMode = IsAudioOutputMode();
			//const wchar_t* outputMode = isAudioMode ? L"PlayWavCompleted" : L"TextCompleted";

			//VAR *outputCommpleted = pMenu->Find(outputMode);
			//if (outputCommpleted && outputCommpleted->vValue.boolVal == ATL_VARIANT_TRUE)
			//{
			//	isGetOutputCompleted = true;
			//}

			// 4. Check Stream Completed
			bool isStreamCompleted = true;
			//bool isStreamCompleted = false;
			//if (VAR *streamCompleted = pMenu->Find(L"StreamCompleted"))
			//{
			//	isStreamCompleted = streamCompleted->vValue.boolVal == ATL_VARIANT_TRUE;
			//}

			// 5. finish input event
			if (isAudioMode)
			{
				if (isGetOutputCompleted)
				{
					m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
					SetState(VMS_READY);
				}
			}
			else // t2s mode
			{
				if (isStreamCompleted)
				{
					m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
					SetState(VMS_READY);
				}
			}
		}
		catch(std::runtime_error& aError)
		{
			error = std::make_pair(true, stow(aError.what()));
		}
		catch (...)
		{
			error = std::make_pair(true, L"Unknown exception error");
		}

		if (error.first)
		{
			DoErrorBadfetch(
				L"SESSION",
				BadfetchErrorsToString(BadfetchErrors::BE_ASR),
				L"Voice-recognition",
				error.second);

			m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
			SetState(VMS_READY);
		}



		/*****************************************/
		// ToDo: dtmf input + recognize
		//VAR *pGetInput = pMenu->Find(L"GetInput");
		//if (pGetInput && pGetInput->vValue.boolVal == ATL_VARIANT_TRUE)
		//{
		//	return; // wait for next event
		//}

		//CMessage mrspMsg = MakeMrcpEvent(aMsg);

		//if (VoiceEmit(mrspMsg, aCurEvent))
		//{
		//	pMenu->Add(L"RecognizeInput", true, false);
		//}

		//bool bGetInput = false;
		//if (IsVoiceInputMode())
		//{
		//	pMenu->Add(L"GetInput", true, false);
		//	bGetInput = true;
		//}


		//VAR *pStreamCompleted = pMenu->Find(L"StreamCompleted");
		//if (pStreamCompleted
		//	&& pStreamCompleted->vValue.boolVal == ATL_VARIANT_TRUE
		//	&& IsDtmfInputMode)
		//{
		//	pMenu->Add(L"GetInput", true, false);
		//	bGetInput = true;
		//}

		//VAR *pTextCompleted = pMenu->Find(L"TextCompleted");
		//if (pTextCompleted
		//	&& pTextCompleted->vValue.boolVal == ATL_VARIANT_TRUE
		//	&& bGetInput)
		//{
		//	m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
		//	SetState(VMS_READY);
		//}
		/*****************************************/
	}

	void CSession::MakeEvent_StreamCompleted(const CMessage &aMsg, const ItemEventPtr & aCurEvent)
	{
		MenuPtr menu = GetCurMenu();
		menu->RecInfo.StreamCompleted = true;


		//VXMLString currMenuName = GetCurMenu(aCurEvent);

		//// 1. complete event
		//SessionObject pMenu = FindMenu(currMenuName);
		//if (!pMenu)
		//{
		//	throw std::runtime_error(format_string("MakeEvent_StreamCompleted: Cannot find menu \"%s\"", wtos(currMenuName)));
		//}
		//pMenu->Add(L"StreamCompleted", true, false);

		// 2. check TerminationReason
		const CParam* pParam = aMsg.ParamByName(L"TerminationReason");
		if (!pParam)
		{
			throw std::logic_error("There are no TerminationReason in STREAM_COMPLETED");
		}

		bool isGetDigitCompleted = false;
		if (pParam->AsString() == L"TM_DIGIT" || pParam->AsString() == L"TM_EOD")
		{
			isGetDigitCompleted = DigitsEmit(aMsg, aCurEvent);
		}

		//bool isVoiceInput = IsVoiceInputMode();
		bool isVoiceInput = menu->InputMode == L"voice";

		bool isRecognizeCompleted = false;
		if (isVoiceInput)
		{
			isRecognizeCompleted = menu->RecInfo.RecognizeCompleted;
		}

		//bool isRecognizeCompleted = false;
		//if (isVoiceInput)
		//{
		//	VAR *pRecognizeCompleted = pMenu->Find(L"RecognizeCompleted");
		//	if (pRecognizeCompleted
		//		&& pRecognizeCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
		//	{
		//		isRecognizeCompleted = true;
		//	}
		//}

		bool isTextCompleted = menu->RecInfo.TextCompleted;
		//bool isTextCompleted = false;
		//VAR *pTextCompleted = pMenu->Find(L"TextCompleted");
		//if (pTextCompleted
		//	&& pTextCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
		//{
		//	isTextCompleted = true;
		//}

		if (isVoiceInput)
		{
			if (isTextCompleted && isRecognizeCompleted)
			{
				SetState(VMS_READY);
				m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
			}
		}
		else
		{
			if (isGetDigitCompleted)
			{
				DoClearDigits();
			}
			if (isTextCompleted)
			{
				SetState(VMS_READY);
				m_dispatcher->SetState(aCurEvent, VMStates::VMS_READY);
			}
		}
	}



	void CSession::MakeEvent_GetDigitsCompleted(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		//if (const CParam* pParam = msg.ParamByName(L"DigitsBuffer"))
		//{
			DigitsEmit(msg, curEvent);
		//}

		DoClearDigits();
	}

	void CSession::MakeEvent_DialogTransferComplete(TransferInfo& outTransferInfo, const CMessage& msg, const ItemEventPtr& curEvent)
	{
		/*******/
		//MakeEvent_DialogStopChannel(msg, curEvent);
		/*** make this instead ****/
		ChangeCallCtrl(GetParentScriptID(), GetScriptID());
		MakeTransferDialogStopShannel(outTransferInfo, curEvent);
		/*******/
		
		stopKeepAliveTimers();

		DigitsEmit(msg, curEvent);
		DoClearDigits();
	}

	void CSession::MakeEvent_ConnectionDisconnectHangup(TransferInfo& outTransferInfo, const CMessage& msg, const ItemEventPtr& curEvent)
	{
		//MakeEvent_DialogStopChannel(msg, curEvent);
		ChangeCallCtrl(GetParentScriptID(), GetScriptID());
		MakeTransferDialogStopShannel(outTransferInfo, curEvent);


		//ToDo here
		//...

		//m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
		//this->SetState(VMStates::VMS_READY);
	}

	void CSession::MakeEvent_ConnectionDisconnectTransfer(TransferInfo& outTransferInfo, const CMessage& msg, const ItemEventPtr& curEvent)
	{
		//MakeEvent_DialogStopChannel(msg, curEvent);
		ChangeCallCtrl(GetParentScriptID(), GetScriptID());
		MakeTransferDialogStopShannel(outTransferInfo, curEvent);

		stopKeepAliveTimers();

		m_Log->LogInfo( L"Connection: \"%I64i\" has redirected", msg.ParamByName(L"CallID")->AsInt64());
		DoExit(L"");
	}

	void CSession::MakeEvent_DialogStopChannel(TransferInfo& outTransferInfo, const CMessage &, const ItemEventPtr & curEvent)
	{
		MakeTransferDialogStopShannel(outTransferInfo, curEvent);
		ChangeCallCtrl(GetScriptID(), GetParentScriptID());
	}

	void CSession::MakeEvent_SC2VRunFunctionAck(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		if (const CParam *param = msg.ParamByName(L"ErrorCode"))
		{
			ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();

			VXMLString description = L"A fetch of a document has failed";
			if (const CParam *param = msg.ParamByName(L"ErrorDescription"))
			{
				description = param->AsString();
			}

			m_dispatcher->SetState(curEvent, VMStates::VMS_READY);
			SetState(VMS_READY);

			DoErrorBadfetch(
				L"SESSION",
				BadfetchErrorsToString(BadfetchErrors::BE_RUN),
				L"Run st3 function",
				description);
		}
		else
		{
			ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), msg.ParamByName(L"ScriptID")->AsInt64());
		}

//		WaitingEventsContainer waiting_events_with_handle;
//		waiting_events_with_handle.emplace(L"SC2V_RUN_FUNCTION_COMPLETED", boost::bind(&CSession::MakeEvent_SC2VRunFunctionCompleted, this, _1, _2));

//		curEvent.lock()->SetWaitingEvents(waiting_events_with_handle);

	}

	void CSession::MakeEvent_SC2VRunFunctionCompleted(const CMessage& msg, const ItemEventPtr& curEvent)
	{
		MakeEvent_EndDialog(msg, curEvent);
	}

	//SessionObject CSession::AddMenu(CMessage& msg)
	//{
	//	SessionObject pMenu(new CNameTable(msg));
	//	VXMLString sMenuName = msg.GetName();
	//	pMenu->Add(L"reprompt", true, false); // once
	//	m_pMenus->Add(sMenuName, pMenu->AsVariant(), true);

	//	return pMenu;
	//}

	SessionGrammar CSession::AddGrammar(const VXMLString& name, const VXMLString& source, const VXMLString& mode, const VXMLString& _type, bool bText)
	{
		SessionGrammar pGrammar(new DEBUG_NEW_PLACEMENT VXMLGrammar(mode));
		pGrammar->SetContentType(_type);

		if (!mode.compare(L"dtmf"))
		{
			pGrammar->Load(source);
		}
		if (!mode.compare(L"voice"))
		{
			if (bText)
			{
				pGrammar->SetVoiceSource(source);
			}
			else
			{
				pGrammar->SetExternalSource(source);
			}
		}

		m_pGrammars->Add(name, pGrammar->AsVariant());
		return pGrammar;
	}

	template <class T, class Type>
	ShareObjectPtr<T> GetComplexObject(const VXMLString& _name, const ShareObjectPtr<T>& collector)
	{
		if (_name.empty())
		{
			//return ShareObjectPtr<T>();
			throw std::logic_error("Empty complex object name");
		}

		VAR* objectPair = collector->Find(_name);

		if (!objectPair)
		{
			return ShareObjectPtr<T>();
		}

		const Type* bridge = static_cast<const Type*>(objectPair->vValue.pdispVal);
		if (bridge == nullptr)
		{
			return ShareObjectPtr<T>();
		}

		WeakObjectPtr<T> object = bridge->Get();
		if (object.expired())
		{
			return ShareObjectPtr<T>();
		}

		return object.lock();
	}

	SessionGrammar CSession::FindGrammar(const VXMLString& grammar_name) const
	{
		return GetComplexObject <VXMLGrammar, GrammarDispatch>(grammar_name, m_pGrammars);
	}

	CMessage CSession::CreateStatisticMsg(const VXMLString& formID)const
	{
		CMessage postmsg;
		// format date
		wchar_t date[256];
		time_t  tt;
		time(&tt);
		wcsftime(date, 256, L"%Y-%m-%d %H:%M:%S", localtime(&tt));
		DWORD dBufSize = 1024;
		wchar_t cname[1024];
		ZeroMemory(cname, sizeof(cname));
		GetComputerName(cname, &dBufSize);

		postmsg[L"datetime"] = date;
		postmsg[L"servername"] = cname;
		postmsg[L"ScriptID"] = m_InitMsg[L"ScriptID"].AsInt64();//.Value;
		postmsg[L"anumber"] = m_InitMsg[L"A"].Value;
		postmsg[L"bnumber"] = m_InitMsg[L"B"].Value;
		postmsg[L"menuid"] = formID;
		postmsg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;

		return postmsg;
	}

	void CSession::ProccedFormComment(const VXMLString& formID, const VXMLString& comment) const
	{
		CMessage postmsg = CreateStatisticMsg(formID);
		postmsg.SetName(L"VXML2SA_Statistics");
		postmsg[L"comment"] = m_ExecContext.pVbs->ExprEvalToStr(comment);//comment;

		SendAuxMsg(postmsg);
	}

	void CSession::ProccedBufferComment(const VXMLString& formID, const VXMLString& buffer) const
	{
		CMessage postmsg = CreateStatisticMsg(formID + L"_DigitsBuffer");
		postmsg.SetName(L"VXML2SA_Statistics");
		postmsg[L"comment"] = (VXMLString(L"DigitsBuffer: ") + buffer).c_str();

		SendAuxMsg(postmsg);
	}

	ObjectId CSession::GetParentCallID()const
	{
		if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
			return param->AsInt64();
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
			return param->AsInt64();
		return 0;
	}

	//void CSession::GetParentCallID(CMessage& _msg)const
	//{
	//	if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
	//		_msg[L"CallID"] = param->AsUInt64();
	//	if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
	//		_msg[L"VoxID"] = param->AsUInt64();

	//}

	void CSession::SendEvent(CMessage& evt, /*int nSendId,*/ ObjectId to/*, DWORD dwDelay*/)
	{
		ObjectId nParentID = 0;
		if (CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			nParentID = m_InitMsg[L"CallID"].Value.llVal;
			evt[L"CallID"] = nParentID;
		}
		if (CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			nParentID = m_InitMsg[L"VoxID"].Value.llVal;
			evt[L"VoxID"] = nParentID;
		}

		evt[L"CallbackID"]			= nParentID;
		evt[L"ScriptID"]			= m_InitMsg[L"ScriptID"].Value;
		evt[L"SaveScriptID"]		= m_InitMsg[L"ScriptID"].Value;

		evt[L"DestinationAddress"] = to;
		SendAuxMsg(evt);
		//if (dwDelay == 0)
		//{
		//	SendEventNow(evt, nSendId);
		//}
		//else
		//{
		//	m_SendEvtQ.push_back(CSendEvent(evt, nSendId, dwDelay));
		//}
	}

	void CSession::SendEventToYourself(CMessage& evt)
	{
		SendEvent(evt, m_InitMsg[L"ScriptID"].AsInt64());
	}

	ObjectId CSession::GetScriptID() const
	{
		//return m_InitMsg.ParamByName(L"ScriptID")->AsInt64();
		return m_ScriptId;
	}

	ObjectId CSession::GetParentScriptID()const
	{
		return m_InitMsg[L"ParentScriptID"].AsInt64();
	}

	VXMLString CSession::GetPredefineFiles()const
	{
		if (const CParam *pPredefine = m_InitMsg.ParamByName(L"predefine"))
			return pPredefine->AsWideStr();
		if (const CParam *pPredefine = m_InitMsg.ParamByName(L"Predefine"))
			return pPredefine->AsWideStr();

		return L"";
	}

	CMessage CSession::CreatePlayWavMessage(const VXMLString & _uri) const
	{
		CMessage msg(L"PLAY_WAV");

		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		VXMLString sSpeaker, sLanguage;
		VAR* pSpeaker = pVar->Find(L"Speaker");
		if (pSpeaker)
			sSpeaker = _bstr_t(pSpeaker->vValue);
		VAR* pLanguage = pVar->Find(L"Language");
		if (pLanguage)
			sLanguage = _bstr_t(pLanguage->vValue);

		VXMLString sTermChar = _bstr_t(pVar->Find(L"termchar")->vValue);

		const auto splitedWavs = GetSplitedFiles(_uri);
		int k = 0;

		for (auto wavFile : splitedWavs)
		{
			boost::trim(wavFile);
			if (wavFile[0] == '/' || wavFile[0] == '\\')
			{
				wavFile.erase(0, 1);
			}
			VXMLString sParamName = format_wstring(L"File%d", ++k);
			VXMLString sFolder = GetWavFolder();
			WCHAR szBuf[1024];
			PathCombineW(szBuf, szBuf, sFolder.c_str());
			if (!sSpeaker.empty())
				PathCombineW(szBuf, szBuf, sSpeaker.c_str());
			if (!sLanguage.empty())
				PathCombineW(szBuf, szBuf, sLanguage.c_str());
			PathCombineW(szBuf, szBuf, wavFile.c_str());
			msg[sParamName.c_str()] = szBuf;
		}

		msg[L"FilesCount"] = k;
		msg[L"DigitMask"] = sTermChar;
		msg[L"DestinationAddress"] = GetParentCallID();

		return msg;
	}

	CMessage CSession::CreateTextToSpeechMessage(const VXMLString & aText, bool aIsDtmfInput) const
	{
		VXMLString AsrUri = L"rtsp://" + m_Text2SpeechSettings.sRTSPHost + L":" + Utils::toStr(m_Text2SpeechSettings.sRTSPPort) + L"/tts";

		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		CMessage msg(GetTTSSpeakName().c_str()/*L"MRCP_TEXT_TO_SPEECH"*/);
		msg[L"Version"] = 1;
		msg[L"Encoding"] = L"UTF-8";
		msg[L"Language"] = L"ru-RU";
		msg[L"ContentType"] = L"application/ssml+xml";
		msg[L"Voice"] = GetText2SpeechVoice().c_str();
		msg[L"Text"] = aText;
		msg[L"CallID"] = GetParentCallID();
		msg[L"RTSPHost"] = GetText2SpeechRTSPHost().c_str();
		msg[L"RTSPPort"] = GetText2SpeechRTSPPort();
		msg[L"AsrUri"] = AsrUri.c_str();

		if (aIsDtmfInput)
		{
			VXMLString sTermChar = _bstr_t(pVar->Find(L"termchar")->vValue);
			msg[L"DigitMask"] = sTermChar;
		}

		return msg;
	}

	template <class TString1, class TString2>
	static void SetParam(const ScopeServicePtr& aVar, CMessage& aMsg, TString1&& aParamVarName, TString2&& aParamMsgName)
	{
		if (const VAR *pParam = aVar->Find(std::forward<TString1>(aParamVarName)))
		{
			aMsg[std::forward<TString2>(aParamMsgName)] = pParam->vValue.vt == VT_BSTR ? pParam->vValue.bstrVal : 0;
		}
	}

	CMessage CSession::CreateMRCPRecognizeMessage(bool aIsBargeIn) const
	{
		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		VAR* pCurGrammar = pVar->Find(L"voicegrammar");
		if (!pCurGrammar)
		{
			throw std::runtime_error("Create MRCP_RECOGNIZE message: Voicegrammar must be specified");
		}

		VXMLString grammar_name = _bstr_t(pCurGrammar->vValue.bstrVal).GetBSTR();
		SessionGrammar pG = FindGrammar(grammar_name);

		if (!pG)
		{
			throw std::runtime_error("Create MRCP_RECOGNIZE message: Cannot find voice grammar");
		}

		VXMLString RTSPHost = GetText2SpeechRTSPHost();
		int RTSPPort = GetText2SpeechRTSPPort();
		VXMLString AsrUri = L"rtsp://" + RTSPHost + L":" + Utils::toStr(RTSPPort) + L"/asr";

		//NoInputTimeout = 7000
		//	RecognitionTimeout = 15000
		//	ConfidenceThreshhold = 5
		//	SensitivityLevel = 35
		//	SpeedVsAccuracy = 67
		//	SpeechCompleteTimeout = 1000
		//	SpeechIncompleteTimeout = 2000

		CMessage msg(L"MRCP_RECOGNIZE");
		msg[L"Version"] = 1;
		msg[L"VAD_ON"] = 1;
		msg[L"BargeIn"] = aIsBargeIn ? 1 : 0;
		msg[L"PlayTrigg"] = GetText2SpeechPlayTrigg();
		msg[L"NonThreshold"] = GetText2SpeechNonThreshold();
		msg[L"NonPlayTrigg"] = GetText2SpeechNonPlayTrigg();
		msg[L"CallID"] = GetParentCallID();
		msg[/*L"Dictionary"*/GetGrammarPathParamName().c_str()] = pG->GetVoiceSource().c_str();
		msg[L"MagicWord"] = GetText2SpeechMagicWord();
		msg[L"Async"] = 1;
		msg[L"UseDenoiser"] = false;
		msg[L"ContentType"] = pG->GetContentType().c_str()/*L"application/srgs+xml"*/;
		msg[L"RTSPHost"] = RTSPHost.c_str();
		msg[L"RTSPPort"] = RTSPPort;
		msg[L"AsrUri"] = AsrUri.c_str();

		VAR * pTimeOut = pVar->Find(L"timeout");
		VAR * pWaitTime = pVar->Find(L"WaitTime");
		VAR * pInterdigittimeout = pVar->Find(L"Interdigittimeout");
		msg[L"MaxSilence"] = Utils::toNum<int>(_bstr_t(pTimeOut->vValue).GetBSTR());
		msg[L"MaxTime"] = Utils::toNum<int>(_bstr_t(pWaitTime->vValue).GetBSTR());
		msg[L"InterDigitDelay"] = Utils::toNum<int>(_bstr_t(pInterdigittimeout->vValue).GetBSTR());

		if (VAR *pCompletetimeout = pVar->Find(L"completetimeout"))
			msg[L"completetimeout"] = pCompletetimeout->vValue.vt == VT_BSTR ? pCompletetimeout->vValue.bstrVal : L"";

		if (VAR *pConfidencelevel = pVar->Find(L"confidencelevel"))
			msg[L"confidencelevel"] = pConfidencelevel->vValue.vt == VT_BSTR ? pConfidencelevel->vValue.bstrVal : L"";

		if (VAR *pIncompletetimeout = pVar->Find(L"incompletetimeout"))
			msg[L"incompletetimeout"] = pIncompletetimeout->vValue.vt == VT_BSTR ? pIncompletetimeout->vValue.bstrVal : L"";

		if (VAR *pMaxspeechtimeout = pVar->Find(L"maxspeechtimeout"))
			msg[L"maxspeechtimeout"] = pMaxspeechtimeout->vValue.vt == VT_BSTR ? pMaxspeechtimeout->vValue.bstrVal : L"";

		if (VAR *pSensitivity = pVar->Find(L"sensitivity"))
			msg[L"sensitivity"] = pSensitivity->vValue.vt == VT_BSTR ? pSensitivity->vValue.bstrVal : L"";

		//if (VAR *pSpeedvsaccuracy = pVar->Find(L"speedvsaccuracy"))
		//	msg[L"speedvsaccuracy"] = pSpeedvsaccuracy->vValue.vt == VT_BSTR ? pSpeedvsaccuracy->vValue.bstrVal : L"";

		if (VAR * pInterdigittimeout = pVar->Find(L"Interdigittimeout"))
			msg[L"interdigittimeout"] = pInterdigittimeout->vValue.vt == VT_BSTR ? pInterdigittimeout->vValue.bstrVal : L"";

		//[USRL-209]
		SetParam(pVar, msg, L"noinputtimeout", L"no-input-timeout");
		SetParam(pVar, msg, L"recognitiontimeout", L"recognition-timeout");
		SetParam(pVar, msg, L"confidencethreshhold", L"confidence-threshold");
		SetParam(pVar, msg, L"sensitivitylevel", L"sensitivity-level");
		SetParam(pVar, msg, L"speedvsaccuracy", L"speed-vs-accuracy");
		SetParam(pVar, msg, L"speechcompletetimeout", L"speech-complete-timeout");
		SetParam(pVar, msg, L"speechincompletetimeout", L"speech-incomplete-timeout");

		return msg;
	}

	static VXMLString GetFileName(const VXMLString & _uri)
	{
		VXMLString fileName = _uri;
		int pos = fileName.rfind(L"/");
		if (pos > 0)
		{
			fileName.erase(0, pos + 1); //include "/"
		}
		return fileName;
	}

	std::pair<VXMLString, VXMLString> CSession::CreateLocalUrl(
		const VXMLString& aUrl, 
		const VXMLString& aANumber) const
	{
		auto ScriptId = m_InitMsg.ParamByName(L"ScriptID")->AsInt64();

		// d:\wav\2017\08\17\id_scriptid_17082017_9606800725.wav

		wchar_t date[256];
		time_t  tt;
		time(&tt);
		wcsftime(date, 256, L"%Y\\%m\\%d", localtime(&tt));

		VXMLString localFolder = format_wstring(L"%s\\%s",
			GetRicognitionsFolder().c_str(),
			date);

		VXMLString localUrl = format_wstring(L"%s\\%08X-%08X_%s_%s",
			localFolder.c_str(),
			DWORD(ScriptId >> 32),
			DWORD(ScriptId & 0xFFFFFFFF),
			aANumber.c_str(),
			GetFileName(aUrl).c_str());
		
		return std::make_pair(localUrl, localFolder);
	}

	CMessage CSession::CreateMASRecieveFilesMessage(
		const VXMLString& aUrl, 
		const VXMLString& aLocalUrl,
		const VXMLString& aLocalFolder) const
	{
		if (!boost::filesystem::exists(aLocalFolder) && !boost::filesystem::create_directories(aLocalFolder))
		{
			throw std::runtime_error(format_string("Cannot create \"%s\" folder", wtos(aLocalFolder).c_str()));
		}

		CMessage msg(L"S2MAS_RECEIVE_FILES");
		msg[L"Url0"] = aUrl.c_str();
		msg[L"CommitUrl0"] = L"";
		msg[L"FilePath0"] = aLocalUrl.c_str();
		msg[L"Version0"] = 0;
		msg[L"FileCount"] = 1;
		msg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;

		return msg;
	}

	VXMLString CSession::SendMASRecieveFilesMessage(
		const VXMLString& aRecognitionUrl, 
		const VXMLString& aConfidence,
		bool aCheckConfidence) const
	{
		VXMLString recognitionLocalUrl;

		if (aRecognitionUrl.empty())
		{
			return recognitionLocalUrl;
		}

		bool needToSend = true;
		if (aCheckConfidence)
		{
			int confidenceValue = 0;
			if (!aConfidence.empty())
			{
				confidenceValue = Utils::toNum<int>(aConfidence);
			}

			int confidenceLevel = m_Text2SpeechSettings.iConfidenceLevel;
			needToSend = confidenceLevel > 0 && confidenceValue < confidenceLevel;
		}

		if (needToSend)
		{
			VXMLString anumber = m_InitMsg[L"A"].AsWideStr();
			VXMLString localFolder;


			std::tie(recognitionLocalUrl, localFolder) = CreateLocalUrl(aRecognitionUrl, anumber);

			CMessage masMsg = CreateMASRecieveFilesMessage(aRecognitionUrl, recognitionLocalUrl, localFolder);
			SendAuxMsg(masMsg);
		}

		return recognitionLocalUrl;
	}

	void CSession::ChangeCallCtrl(const ObjectId& _oldScriptID, const ObjectId& _newScriptID)
	{
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
			return; // do not necessary change control in conference
		CMessage change_control_msg(L"CHANGE_CALL_CONTROL");
		change_control_msg [ L"CallID"             ] = m_InitMsg[ L"CallID"   ].Value ;
		change_control_msg [ L"OldScriptID"        ] = _oldScriptID                   ;
		change_control_msg [ L"NewScriptID"        ] = _newScriptID                   ;
		change_control_msg [ L"DestinationAddress" ] = m_InitMsg[ L"CallID"   ].Value ;
		change_control_msg [ L"SourceAddress"      ] = m_InitMsg[ L"ScriptID" ].Value ;
		SendAuxMsg(change_control_msg);
	}

	void CSession::MakeTransferDialogStopShannel(TransferInfo& outTransferInfo, const ItemEventPtr& curEvent)
	{
		if (outTransferInfo.DialogTransferAudioRepeat)
		{
			outTransferInfo.DialogTransferAudioRepeat = false;
			CMessage msg_stopchannel(L"STOP_CHANNEL");
			SendEvent(msg_stopchannel, GetParentCallID());
		}
	}

	Factory_c::CMrcpFactory CSession::LoadT2SAnswer(const VXMLString & sSrc) const
	{
		Factory_c::CMrcpFactory parser;

		ResourceCachePtr	pRes;
		try
		{
			pRes.reset(new DEBUG_NEW_PLACEMENT CResourceCache());
		}
		catch (...)
		{
			m_Log->LogWarning(L"Failed to load CResourceCache");
		}
		if (!pRes->Ready())
		{
			m_Log->LogWarning(L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
			return parser;
		}
		try
		{
			SAFEARRAY * pSA = NULL;
			HRESULT hr = pRes->ParseT2SMessage(sSrc, &pSA);
			if (SUCCEEDED(hr))
			{
				m_Log->LogInfo( L"LoadDocument: ParseT2SMessage successful");
				// get back archive
				char *c1;
				// Get data pointer
				HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

				int image_size = pSA->rgsabound->cElements;
				std::string in_data;
				in_data.append(c1, image_size);

				::SafeArrayUnaccessData(pSA);
				::SafeArrayDestroy(pSA);

				std::istringstream is(in_data);
				{
					cereal::BinaryInputArchive iarchive(is);
					m_Log->LogFinest( L"LoadT2SAnswer: deserialization");
					iarchive(parser);
				}

				//std::istringstream is(in_data);
				//boost::archive::binary_iarchive ia(is);
				//// read class state from archive
				//m_Log->LogFinest( L"LoadT2SAnswer: deserialization");
				//ia >> parser;
			}
			else
			{
				m_Log->LogWarning(L"Error: LoadT2SAnswer::ParseT2SMessage cannot load \n %s \nError code: %d", sSrc.c_str(), hr);
			}
		}
		catch (std::exception& e)
		{
			m_Log->LogWarning(L"Exception loading LoadT2SAnswer [%s]: \"%s\"", sSrc.c_str(), VXMLString(bstr_t(e.what())).c_str());
		}
		catch (std::wstring& e)
		{
			m_Log->LogWarning(L"Exception loading LoadT2SAnswer [%s]: \"%s\"", sSrc.c_str(), e.c_str());
		}
		catch (_com_error& e)
		{
			m_Log->LogWarning(L"Exception loading LoadT2SAnswer [%s]: \"%s\"", sSrc.c_str(), (LPCWSTR)e.Description());
		}
		catch (...)
		{
			m_Log->LogWarning(L"Unknown exception loading LoadT2SAnswer [%s]", sSrc.c_str());
		}

		return parser;
	}

	CMessage CSession::MakeMrcpEvent(const CMessage& aMsg) const
	{
		// Get params
		VXMLString data, decription, waveformUri;
		if (const CParam* param = aMsg.ParamByName(L"RecognitionData"))
		{
			data = param->AsWideStr();
		}

		if (const CParam* param = aMsg.ParamByName(L"CompletionDescription"))
		{
			decription = param->AsWideStr();
		}

		int completionCause = 0;
		if (const CParam* param = aMsg.ParamByName(L"CompletionCause"))
		{
			completionCause = Utils::toNum<int>(param->AsWideStr());
		}

		if (const CParam* param = aMsg.ParamByName(L"WaveformUri"))
		{
			waveformUri = param->AsString();
		}

		// make message
		CMessage mrcpResultMsg(L"MRCP_RECOGNIZE_COMPLETED");
		switch (completionCause)
		{
		case CC_Success:
		{
			if (!data.empty())
			{
				Factory_c::CMrcpFactory answer = LoadT2SAnswer(data);
				mrcpResultMsg[L"input"] = answer.GetInput().c_str();
				mrcpResultMsg[L"confidence"] = answer.GetConfidence().c_str();
				mrcpResultMsg[L"swi_meaning"] = answer.GetSWImeaning().c_str();
			}
			mrcpResultMsg[L"result"] = L"filled";
			break;
		}
		case CC_Nomatch:
		{
			if (!data.empty())
			{
				Factory_c::CMrcpFactory answer = LoadT2SAnswer(data);
				mrcpResultMsg[L"input"] = answer.GetInput().c_str();
				mrcpResultMsg[L"confidence"] = answer.GetConfidence().c_str();
				mrcpResultMsg[L"swi_meaning"] = answer.GetSWImeaning().c_str();
			}
			mrcpResultMsg[L"result"] = L"nomatch";
			break;
		}
		case CC_Noinput:
		{
			mrcpResultMsg[L"result"] = L"noinput";
			break;
		}
		default:
		{
			mrcpResultMsg[L"result"] = decription;
		}
		}

		if (!waveformUri.empty())
		{
			mrcpResultMsg[L"WaveformUri"] = waveformUri;
		}

		m_Log->LogInfo( L"MRCP result: %s", mrcpResultMsg.Dump().c_str());
		return mrcpResultMsg;
	}

	VXMLString CSession::GetVarValue(const VXMLString& _name)const
	{
		try
		{
			return m_ExecContext.pVbs->ExprEvalToStr(_name);
		}
		catch (VXMLString &_error)
		{
			m_Log->LogWarning(L"GetVarValue error: %s", _error.c_str());
			return SYMBOLNOTFOUND_DBGMSG;
		}
		catch (...)
		{
			return SYMBOLNOTFOUND_DBGMSG;
		}
	}

	VXMLString CSession::GetSpeakNumberFiles(const VXMLString& expr, int sex)const 
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakNumberFiles(
			CComVariant(expr.c_str()), tagSpeakSex(sex), VARIANT_FALSE);

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakTimeFiles(DWORD hh, DWORD mm, DWORD ss)const 
	{
		//m_Log->Log(__FUNCTIONW__, L"function CSession::GetSpeakTimeFiles unimplemented");



		SYSTEMTIME st = {0};
		st.wHour   = static_cast<WORD>(hh);
		st.wMinute = static_cast<WORD>(mm);
		st.wSecond = static_cast<WORD>(ss);

		DOUBLE dblTime = 0.0;
		::SystemTimeToVariantTime(&st, &dblTime);
		CComVariant Time(dblTime);

		_bstr_t Files = m_pVox->GetInstance()->GetSpeakTimeFiles(
			Time, tfHours | tfMinutes | tfSeconds);

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakDateFiles(DWORD dd, DWORD mm, DWORD yy)const 
	{
		//m_Log->Log(__FUNCTIONW__, L"function CSession::GetSpeakDateFiles unimplemented");

		SYSTEMTIME st = {0};
		//::GetLocalTime(&st);
		st.wDay   = static_cast<WORD>(dd);
		st.wMonth = static_cast<WORD>(mm);
		st.wYear  = static_cast<WORD>(yy);

		DOUBLE dblTime = 0.0;
		::SystemTimeToVariantTime(&st, &dblTime);
		CComVariant Time(dblTime);

		_bstr_t Files = m_pVox->GetInstance()->GetSpeakDateFiles(
			Time, dfDays | dfMonths | dfYears);

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakMoneyFiles(const VXMLString& expr, const VXMLString& format)const 
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakMoneyFiles(CComVariant(expr.c_str()),
			                           _bstr_t(format.c_str()));

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakTrafficFiles(const VXMLString & expr) const
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakTrafficFiles(CComVariant(expr.c_str()));

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakMinutesFiles(const VXMLString & expr) const
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakMinutesFiles(CComVariant(expr.c_str()));

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakSMSFiles(const VXMLString & expr) const
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakSMSFiles(CComVariant(expr.c_str()));

		return VXMLString(Files);
	}

	void CSession::startKeepAliveTimers(/*const ObjectId& scriptID*/)
	{
		//int iKeepAliveTimeout = 1;
		//if (!m_VXMLSystem.sKeepAliveTimeout.empty())
		//	iKeepAliveTimeout = Utils::toNum<int>(m_VXMLSystem.sKeepAliveTimeout);
		//m_Log->LogInfo( L"Starting Keep Alive Timeout, expired every \"%i\" minutes", iKeepAliveTimeout);


		//m_keep_alive.reset(new DEBUG_NEW_PLACEMENT CSharedKeepAlive(iKeepAliveTimeout,
		//	m_InitMsg.ParamByName(L"ScriptID")->AsInt64(),
		//	m_InitMsg.ParamByName(L"ParentScriptID")->AsInt64(),
		//	m_ExecContext.pEng));

		m_keepAlive->SubscribeTimer(m_InitMsg.ParamByName(L"ScriptID")->AsInt64());
		m_keepAlive->SubscribeTimer(m_InitMsg.ParamByName(L"ParentScriptID")->AsInt64());
	}


	void CSession::stopKeepAliveTimers()
	{
		m_Log->LogInfo( L"Stop Keep Alive timers");
		m_keepAlive->UnsubscribeTimer(m_InitMsg.ParamByName(L"ScriptID")->AsInt64());
		m_keepAlive->UnsubscribeTimer(m_InitMsg.ParamByName(L"ParentScriptID")->AsInt64());
	}

	VXMLString CSession::ThreadMsgToString(const eThreadMsg& _msg)const
	{
		switch (_msg)
		{
		case tmExecuteDoc:
			return L"Execute Document";
		case tmSetEvent:
			return L"Set Event";
		case tmExecutePredefinition:
			return L"Execute predefine Document";
		case tmDocumentPrepared:
			return L"Prepare Document";
		case tmQuit:
			return L"Quit";
		}

		return L"Unknown thread message";
	}

	void CSession::DoPlayWav(
		const VXMLString& uri, 
		const WaitingEvents& _extraEvents, 
		bool aIsDtmfInput, 
		bool aIsVoiceInput)
	{
		EventMessages messages;
		if (aIsVoiceInput)
		{
			messages.emplace_back(CreateMRCPRecognizeMessage());

			ScopeServicePtr pVar = m_ExecContext.pVar.lock();
			pVar->AddVar(L"utterance", L"", false);
			pVar->AddVar(L"classid", L"", false);
			pVar->AddVar(L"confidence", L"", false);
			pVar->AddVar(L"meaning", L"", false);
			pVar->AddVar(L"recognition", L"", false);
			pVar->AddVar(L"recognitionlocal", L"", false);

			MenuPtr menu = GetCurMenu();
			menu->RecInfo.RecognizeCompleted = false; //need this ???
			menu->RecInfo.PlayWavCompleted = false;
			menu->RecInfo.RecognizeInput = false;


			//VXMLString attrId = GetCurMenu(m_ExecContext.pEvt);
			//SessionObject pMenu = FindMenu(attrId);
			//if (pMenu.get())
			//{
			//	VAR *pRecognizeCompleted = pMenu->Find(L"RecognizeCompleted");
			//	if (pRecognizeCompleted && pRecognizeCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
			//	{
			//		pMenu->Add(L"RecognizeCompleted", false, false);
			//	}
			//	VAR *pPlayWavCompleted = pMenu->Find(L"PlayWavCompleted");
			//	if (pPlayWavCompleted && pPlayWavCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
			//	{
			//		pMenu->Add(L"PlayWavCompleted", false, false);
			//	}
			//	VAR *pRecognizeInput = pMenu->Find(L"RecognizeInput");
			//	if (pRecognizeInput && pRecognizeInput->vValue.boolVal == ATL_VARIANT_TRUE)
			//	{
			//		pMenu->Add(L"RecognizeInput", false, false);
			//	}
			//}
		}

		messages.emplace_back(CreatePlayWavMessage(uri));

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"PLAY_WAV_ACK", boost::bind(&CSession::MakeEvent_PlayWavAck, this, _1, _2));
		waiting_events_with_handle.emplace(L"PLAY_WAV_COMPLETED", boost::bind(&CSession::MakeEvent_PlayWavCompleted, this, _1, _2));

		if (aIsDtmfInput)
		{
			waiting_events_with_handle.emplace(L"DIGIT_GOT", boost::bind(&CSession::MakeEvent_DigitGot, this, _1, _2));
		}

		if (aIsVoiceInput)
		{
			waiting_events_with_handle.emplace(L"MRCP_RECOGNIZE_ACK", boost::bind(&CSession::MakeEvent_RecognizeAck, this, _1, _2));
			waiting_events_with_handle.emplace(L"MRCP_RECOGNIZE_COMPLETED", boost::bind(&CSession::MakeEvent_RecognizeCompleted, this, _1, _2));
		}

		for(const auto& _event: _extraEvents)
		{
			waiting_events_with_handle.emplace(_event, boost::bind(&CSession::MakeEvent_PlayWavBreak, this, _1, _2));
		}

		AssignAndWaitEvents(m_ExecContext.pEvt, messages, std::move(waiting_events_with_handle));
	}

	void CSession::DoSpeakText(const VXMLString& aText, bool aIsDtmfInput, bool aIsVoiceInput)
	{
		EventMessages messages;
		if (aIsVoiceInput)
		{
			messages.emplace_back(CreateMRCPRecognizeMessage());

			ScopeServicePtr pVar = m_ExecContext.pVar.lock();
			pVar->AddVar(L"utterance", L"", false);
			pVar->AddVar(L"classid", L"", false);
			pVar->AddVar(L"confidence", L"", false);
			pVar->AddVar(L"meaning", L"", false);
			pVar->AddVar(L"recognition", L"", false);
			pVar->AddVar(L"recognitionlocal", L"", false);
			
			MenuPtr menu = GetCurMenu();
			menu->RecInfo.RecognizeCompleted = false; //need this ???
			menu->RecInfo.PlayWavCompleted = false;
			menu->RecInfo.RecognizeInput = false;

			//VXMLString attrId = GetCurMenu(m_ExecContext.pEvt);
			//SessionObject pMenu = FindMenu(attrId);
			//if (pMenu.get())
			//{
			//	VAR *pRecognizeCompleted = pMenu->Find(L"RecognizeCompleted");
			//	if (pRecognizeCompleted && pRecognizeCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
			//	{
			//		pMenu->Add(L"RecognizeCompleted", false, false);
			//	}
			//	VAR *pTextCompleted = pMenu->Find(L"TextCompleted");
			//	if (pTextCompleted && pTextCompleted->vValue.boolVal == ATL_VARIANT_TRUE)
			//	{
			//		pMenu->Add(L"TextCompleted", false, false);
			//	}
			//	VAR *pRecognizeInput = pMenu->Find(L"RecognizeInput");
			//	if (pRecognizeInput && pRecognizeInput->vValue.boolVal == ATL_VARIANT_TRUE)
			//	{
			//		pMenu->Add(L"RecognizeInput", false, false);
			//	}
			//}
		}

		messages.emplace_back(CreateTextToSpeechMessage(aText, aIsDtmfInput));

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(GetTTSSpeakAckName()/*L"MRCP_TEXT_TO_SPEECH_ACK"*/, boost::bind(&CSession::MakeEvent_TextToSpeechAck, this, _1, _2));
		waiting_events_with_handle.emplace(GetTTSSpeakCompletedName()/*L"MRCP_TEXT_TO_SPEECH_COMPLETED"*/, boost::bind(&CSession::MakeEvent_TextToSpeechCompleted, this, _1, _2));
		//waiting_events_with_handle.emplace(L"STREAM_COMPLETED", boost::bind(&CSession::MakeEvent_StreamCompleted, this, _1, _2));

		if (aIsVoiceInput)
		{
			waiting_events_with_handle.emplace(L"MRCP_RECOGNIZE_ACK", boost::bind(&CSession::MakeEvent_RecognizeAck, this, _1, _2));
			waiting_events_with_handle.emplace(L"MRCP_RECOGNIZE_COMPLETED", boost::bind(&CSession::MakeEvent_RecognizeCompleted, this, _1, _2));
		}

		AssignAndWaitEvents(m_ExecContext.pEvt, messages, std::move(waiting_events_with_handle));
	}

	void CSession::DoDialogTransfer(const VXMLString& _dest,
		bool bBridge,
		const VXMLString& _max_time,
		const VXMLString& _connection_timeout,
		const VXMLString& _aai,
		const VXMLString& _SIPContact,
		const VXMLString& _SIPUserToUser,
		const VXMLString& _Trunk,
		const VXMLString& _AudioUri,
		const VXMLString& _Namelist,
		TransferInfo& outTransferInfo)
	{
		CMessage msg(L"dialog.transfer");

		msg[L"uri"] = _dest.c_str();
		msg[L"bridge"] = bBridge;
		msg[L"maxtime"] = _max_time.c_str();
		msg[L"connecttimeout"] = _connection_timeout.c_str();
		msg[L"aai"] = _aai.c_str();
		msg[L"targettype"] = L"ccxml";
		msg[L"type"] = bBridge ? L"bridge" : L"blind";

		if (!_SIPContact.empty())
			msg[L"SIPContact"] = _SIPContact.c_str();
		
		if (!_SIPUserToUser.empty())
			msg[L"SIPUserToUser"] = _SIPUserToUser.c_str();
		
		if (!_Trunk.empty())
			msg[L"trunk"] = _Trunk.c_str();

		AddNamelistToMsg(&m_ExecContext, _Namelist, msg);

		// transferaudio is the URI of audio source to play while the transfer attempt is in progress (before far-end answer).
		//   repeat it 99 times to make audio continued
		bool bMakePlayWav = false;
		if (!_AudioUri.empty())
		{
			bMakePlayWav = true;
		}

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"dialog.transfer.complete", boost::bind(&CSession::MakeEvent_DialogTransferComplete, this, outTransferInfo, _1, _2));
		waiting_events_with_handle.emplace(L"connection.disconnect.hangup", boost::bind(&CSession::MakeEvent_ConnectionDisconnectHangup, this, outTransferInfo, _1, _2));
		waiting_events_with_handle.emplace(L"connection.disconnect.transfer", boost::bind(&CSession::MakeEvent_ConnectionDisconnectTransfer, this, outTransferInfo, _1, _2));

		if (bMakePlayWav)
		{
			waiting_events_with_handle.emplace(L"dialog.stop_channel", boost::bind(&CSession::MakeEvent_DialogStopChannel, this, outTransferInfo, _1, _2));
			waiting_events_with_handle.emplace(L"PLAY_WAV_COMPLETED", boost::bind(&CSession::MakeEvent_TransferPlayWavCompleted, this, outTransferInfo, _1, _2));
		}

		AssignEvents(m_ExecContext.pEvt, std::move(waiting_events_with_handle));

		if (bMakePlayWav)
		{
			SendEvent(CreatePlayWavMessage(_AudioUri), GetParentCallID());
		}
		else
		{
			ChangeCallCtrl(GetScriptID(), GetParentScriptID());
		}

		startKeepAliveTimers();

		WaitEvents({});

		SendEvent(msg, GetParentScriptID());
	}

	void CSession::DoClearDigits()
	{
		CMessage msg_cleardigits(L"CLEAR_DIGITS");
		CMessage wait_cleardigits;
		msg_cleardigits[L"DestinationAddress"] = GetParentCallID();

		//WaitingEvents waiting_events;
		//waiting_events.push_back(L"CLEAR_DIGITS_COMPLETED");

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"CLEAR_DIGITS_COMPLETED", boost::bind(&CSession::MakeEvent_ClearDigits, this, _1, _2));


		AssignAndWaitEvents(m_ExecContext.pEvt, { msg_cleardigits }, std::move(waiting_events_with_handle));
	}

	void CSession::DoGetDigits(int aMaxDtmf)
	{
		ScopeServicePtr pVar = m_ExecContext.pVar.lock();
		if (!pVar)
		{
			throw std::range_error("Empty event scope");
		}

		VXMLString sTermChar = _bstr_t(pVar->Find(L"termchar")->vValue);

		CMessage msg(L"GET_DIGITS");
		msg[L"DigitMask"] = sTermChar;
		VAR * pTimeOut = pVar->Find(L"timeout");
		VAR * pWaitTime = pVar->Find(L"WaitTime");
		VAR * pInterDigitTimeOut = pVar->Find(L"interdigittimeout");
		//VAR * pMaxDTMF = pVar->Find(L"maxdtmfcount");
		msg[L"MaxSilence"] = Utils::toNum<int>(_bstr_t(pTimeOut->vValue).GetBSTR());
		msg[L"MaxTime"] = Utils::toNum<int>(_bstr_t(pWaitTime->vValue).GetBSTR());
		msg[L"InterDigitDelay"] = Utils::toNum<int>(_bstr_t(pInterDigitTimeOut->vValue).GetBSTR());
		if (aMaxDtmf > 0)
		{
			msg[L"MaxDTMF"] = aMaxDtmf;
		}

		CMessage waitmsg;
		VXMLString sId;
		msg[L"DestinationAddress"] = this->GetParentCallID();

		//WaitingEvents waiting_events;
		//waiting_events.push_back(L"GET_DIGITS_COMPLETED");

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"GET_DIGITS_COMPLETED", boost::bind(&CSession::MakeEvent_GetDigitsCompleted, this, _1, _2));


		AssignAndWaitEvents(m_ExecContext.pEvt, { msg }, std::move(waiting_events_with_handle));
	}

	void CSession::DoStartDialog(
		const VXMLString& filename, 
		const VXMLString& menuid, 
		const VXMLString& parenttype, 
		const VXMLString& namelist)
	{
		CMessage postmsg(L"ANY2TA_RUN_SCRIPT");
		postmsg[L"parenttype"] = parenttype.c_str();
		postmsg[L"FileName"] = filename.c_str();
		if (!menuid.empty())
		{
			postmsg[L"NextExecuteMenuID"] = menuid.c_str();
		}

		ObjectId nParentId = 0;
		if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			nParentId = param->AsInt64();
			postmsg[L"CallID"] = nParentId;
		}
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			nParentId = param->AsInt64();
			postmsg[L"VoxID"] = nParentId;
		}


		postmsg[L"ParentScriptID"] = m_InitMsg[L"ScriptID"].Value;
		VXMLString sPredefine = GetPredefineFiles();
		if (!sPredefine.empty())
		{
			postmsg[L"predefine"] = sPredefine.c_str();
		}

		//msg[L"DestinationAddress"] = pCtx->pSes->GetParentCallID();
		postmsg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;

		AddNamelistToMsg(&m_ExecContext, namelist, postmsg);

		CMessage waitmsg;
		VXMLString sId;

		//WaitingEvents waiting_events;
		//waiting_events.push_back(L"VXML_RUN_SCRIPT_OK");
		//waiting_events.push_back(L"VXML_RUN_SCRIPT_FAILED");

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"VXML_RUN_SCRIPT_OK", boost::bind(&CSession::MakeEvent_VXMLRunScriptOK, this, _1, _2));
		waiting_events_with_handle.emplace(L"VXML_RUN_SCRIPT_FAILED", boost::bind(&CSession::MakeEvent_VXMLRunScriptFAILED, this, _1, _2));

		AssignAndWaitEvents(m_ExecContext.pEvt, {}, std::move(waiting_events_with_handle));

		SendEvent(postmsg, CLIENT_ADDRESS::ANY_CLIENT);
	}

	void CSession::DoStartFunction(
		const VXMLString& function_name, 
		const VXMLString& namelist)
	{
		CMessage postmsg(L"V2SC_RUN_FUNCTION");
		postmsg[L"parenttype"] = L"dialog";
		postmsg[L"function"] = function_name.c_str();

		ObjectId nParentId = 0;
		if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			nParentId = param->AsInt64();
			postmsg[L"CallID"] = nParentId;
		}
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			nParentId = param->AsInt64();
			postmsg[L"VoxID"] = nParentId;
		}

		if (!namelist.empty())
		{
			//postmsg[L"namelist"] = namelist.c_str;
		}
		//postmsg[L"DestinationAddress"] = GetParentScriptID();

		AddNamelistToMsg(&m_ExecContext, namelist, postmsg);

		CMessage waitmsg;
		VXMLString sId;

		WaitingEventsContainer waiting_events_with_handle;
		waiting_events_with_handle.emplace(L"SC2V_RUN_FUNCTION_ACK", boost::bind(&CSession::MakeEvent_SC2VRunFunctionAck, this, _1, _2));
		waiting_events_with_handle.emplace(L"SC2V_RUN_FUNCTION_COMPLETED", boost::bind(&CSession::MakeEvent_SC2VRunFunctionCompleted, this, _1, _2));

		AssignAndWaitEvents(m_ExecContext.pEvt, {}, std::move(waiting_events_with_handle));

		SendEvent(postmsg, GetParentScriptID());

	}

	//TagWPtr CSession::GetMenuTag(const VXMLString& menu_name, const VXMLString& fieldName) const
	//{
	//	return m_ExecContext.pDoc->GetMenuTag(menu_name, fieldName);
	//}

	MenuPtr CSession::GetMenu(const VXMLString& menu_name, const VXMLString& fieldName) const
	{
		return m_ExecContext.pDoc->CreateNextMenu(menu_name, fieldName);
	}

	MenuPtr CSession::GetFirstMenu() const
	{
		return m_ExecContext.pDoc->CreateNextMenu();
	}

	MenuPtr CSession::GetCurMenu()
	{
		return m_ExecContext.pEvt->GetMenu();
	}

	MenuPtr CSession::GetCurMenu() const
	{
		return m_ExecContext.pEvt->GetMenu();
	}


	//std::pair<TagWPtr, VXMLString> CSession::GetEventTag(const CMessage& aEvent)
	//{
	//	//EventPtr curEvent = m_ExecContext.pEvt;
	//	//if (!curEvent.get())
	//	//{
	//	//	curEvent = m_dispatcher->GetCurrentEvent();
	//	//}

	//	ItemEventPtr curEvent = m_dispatcher->GetRecursiveEvent();

	//	if (!curEvent.get())
	//	{
	//		throw std::logic_error("Cannot find current event");
	//	}

	//	TagPtr beginTag = curEvent->getBeginPoint().lock();
	//	if (!beginTag.get())
	//	{
	//		throw std::logic_error("Cannot find event begin point");
	//	}

	//	return std::make_pair(beginTag->ExecEvents(&m_ExecContext, aEvent), beginTag->GetAttrIdName());
	//}

	TagPtr CSession::GetDocumentEnterPoint()const
	{
		return m_ExecContext.pDoc->GetEnterPoint();
	}
	
	//bool CSession::IsMenuStillActive()const
	//{
	//	EventWPtr currMenu = m_dispatcher->GetCurrentEvent();
	//	if (currMenu.lock().get())
	//	{
	//		if (!currMenu.lock()->GetState() == VMStates::VMS_NULL)
	//			return true;
	//	}

	//	return false;
	//}

	static bool GetMode(const ScopeServicePtr& aScope, 
		const VXMLString& aModeType,
		const VXMLString& aModeValue)
	{
		if (!aScope)
		{
			throw std::range_error("Empty event scope");
		}

		VAR* inputmode = aScope->Find(aModeType);
		if (!inputmode)
		{
			throw std::range_error(format_string("\"%s\" is out of current scope", wtos(aModeType)));
		}

		return inputmode->AsString() == aModeValue;
	}

	bool CSession::IsDtmfInputMode() const
	{
		//return GetMode(m_ExecContext.pVar.lock(), L"inputmode", L"dtmf");
		return GetCurMenu()->InputMode == L"dtmf";
	}

	bool CSession::IsVoiceInputMode() const
	{
		//return GetMode(m_ExecContext.pVar.lock(), L"inputmode", L"voice");
		return GetCurMenu()->InputMode == L"voice";
	}

	bool CSession::IsAudioOutputMode() const
	{
		//return GetMode(m_ExecContext.pVar.lock(), L"audiomode", L"playwav");
		return GetCurMenu()->Audiomode == L"playwav";
	}

	bool CSession::IsT2SOutputMode() const
	{
		//return GetMode(m_ExecContext.pVar.lock(), L"audiomode", L"t2s");
		return GetCurMenu()->Audiomode == L"t2s";
	}

	bool CSession::IsSessionActive()const
	{
		if (this->GetState() != VMStates::VMS_ENDSCRIPT)
			return true;
		return false;
	}
}

/******************************* eof *************************************/