/************************************************************************/
/* Name     : VXMLCom\VXMLDocument.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 15 Dec 2009                                               */
/************************************************************************/

#pragma once
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>


#include "VXMLBase.h"
#include "VXMLNameTable.h"
#include "EventBridge.h"


namespace VXML
{
	const WCHAR numbers[] = L"0123456789";

	class CGrammarStr:public VXMLString
	{
	public:
		CGrammarStr(){};
		CGrammarStr(VXMLString _str):VXMLString(_str){}
		CGrammarStr(WCHAR *pWChar):VXMLString(pWChar){}
		//CGrammarStr(std::wstring _str):VXMLString(_str){}
		~CGrammarStr(){}
		bool operator==(const VXMLString& rhs)
		{
			VXMLString temp = *this;
			if (temp.length() != rhs.length())
				return false;
			for (unsigned int i=0;i<temp.length();++i)
			{
				if (temp[i]=='?' && wcschr(numbers,rhs[i]))
					continue;
				if (temp[i]!=rhs[i])
					return false;
			}
			return true;
		}
	};

	class VXMLGrammar: public CNameTable
	{
	public:
		//VXMLGrammar(){};
		VXMLGrammar(const VXMLString& _mode)
			: CNameTable(L"grammar")
			, m_Mode(_mode) {}
		
		virtual ~VXMLGrammar()
		{

		};

		virtual CMessage ToMsg() const override
		{
			return CMessage(L"grammar");
		}

	private:
		using NameContainer = std::multimap<std::wstring, VAR>;
		NameContainer	m_Names;
		VXMLString m_Source;
		VXMLString m_Mode;
		VXMLString m_linkEvent;
		VXMLString m_ContentType;

	public:		
		int Load(const VXMLString& _in_file);
		void SetVoiceSource(const VXMLString& src) { m_Source = src; }
		void SetExternalSource(const VXMLString& src);

		VAR& Add(const VXMLString& sName, const CComVariant& val);
		VAR* Find(const VXMLString& sName);
		int LookUp(const VXMLString& event, const VXMLString& dtmf);
		VXMLString LookUp(const VXMLString& dtmf);

		VXMLString GetVoiceSource()const { return m_Source; }
		VXMLString GetMode()const { return m_Mode; }

		bool isVoice()const { return !m_Mode.compare(L"voice"); }
		bool isDtmf()const { return !m_Mode.compare(L"dtmf"); }

		VXMLString GetLinkEvent()const { return m_linkEvent; }
		VXMLString GetContentType()const { return m_ContentType; }

		void SetLinkEvent(const VXMLString& _event) { m_linkEvent = _event; }
		void SetContentType(const VXMLString& _type) { m_ContentType = _type; }

	private:

		//ULONG	  m_ulRefCount;

	public:
		// IDispatch methods
		virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount( 
			/* [out] */ UINT *pctinfo);

		virtual HRESULT STDMETHODCALLTYPE GetTypeInfo( 
			/* [in] */ UINT iTInfo,
			/* [in] */ LCID lcid,
			/* [out] */ ITypeInfo **ppTInfo);

		virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames( 
			/* [in] */ REFIID riid,
			/* [size_is][in] */ LPOLESTR *rgszNames,
			/* [in] */ UINT cNames,
			/* [in] */ LCID lcid,
			/* [size_is][out] */ DISPID *rgDispId);

		virtual /* [local] */ HRESULT STDMETHODCALLTYPE Invoke( 
			/* [in] */ DISPID dispIdMember,
			/* [in] */ REFIID riid,
			/* [in] */ LCID lcid,
			/* [in] */ WORD wFlags,
			/* [out][in] */ DISPPARAMS *pDispParams,
			/* [out] */ VARIANT *pVarResult,
			/* [out] */ EXCEPINFO *pExcepInfo,
			/* [out] */ UINT *puArgErr);

		//// IUnknown methods
		//virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
		//	/* [in] */ REFIID riid,
		//	/* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);

		//virtual ULONG STDMETHODCALLTYPE AddRef(void);

		//virtual ULONG STDMETHODCALLTYPE Release(void);
	};

	//class CGrammarConteiner
	//{
	//public:
	//	CGrammarConteiner():cur_pCtx(NULL)
	//	{}
	//	CGrammarConteiner(VXML::CExecContext *pCtx):m_sGrammars(L""),m_termchar(L""),cur_pCtx(NULL){Lock(pCtx);}
	//	virtual ~CGrammarConteiner() {Unlock();};
	//	void Lock(VXML::CExecContext *pCtx)
	//	{
	//		if (!(cur_pCtx=pCtx))
	//			return;
	//		VAR* pVal = pCtx->pVar->Find(L"curgrammar");
	//		m_sGrammars = pVal?_bstr_t(pVal->vValue):L"";
	//		pVal = pCtx->pVar->Find(L"termchar");
	//		m_termchar = pVal?_bstr_t(pVal->vValue):L"";
	//	}
	//	void Unlock()
	//	{
	//		if (cur_pCtx /*&& m_sGrammars.length()*/)
	//		{
	//			cur_pCtx->pVar->AddVar(L"curgrammar",m_sGrammars.c_str(),false);
	//			cur_pCtx->pVar->AddVar(L"termchar",m_termchar.c_str(),false);
	//		}
	//	}
	//private:
	//	VXMLString m_sGrammars;
	//	VXMLString m_termchar;
	//	VXML::CExecContext *cur_pCtx;
	//};

}

/******************************* eof *************************************/