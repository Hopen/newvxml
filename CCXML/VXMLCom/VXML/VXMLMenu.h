/************************************************************************/
/* Name     : VXMLCom\VXMLDocument.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Dec 2018                                               */
/************************************************************************/
#pragma once
#include "VXMLBase.h"

namespace VXML
{
	struct IMenu
	{
		IMenu() = default;
	protected:
		IMenu(const IMenu&) = default;
		//IMenu(IMenu&&) = default;

		IMenu& operator=(const IMenu&) = default;
		//IMenu& operator=(IMenu&&) = default;
	};

	struct CMenu;
	using MenuPtr = std::shared_ptr<CMenu>;

	struct CMenu : public IMenu
	{
		CMenu() = default;

		explicit CMenu(
			const VXMLString& aMenuName,
			const VXMLString& aFormId,
			const TagWPtr& aTagPtr)
			: IMenu()
			, MenuName(aMenuName)
			, FormId(aFormId)
			, BeginTagPtr(aTagPtr)
			, mIsEnabled(true)
		{}

		VXMLString MenuName;// Field name also
		VXMLString FormId;
		TagWPtr BeginTagPtr;
		bool IsGetDigitsSent = false;
		VXMLString InputEvent;
		VXMLString Utterance;
		VXMLString PreviousUtterance;
		VXMLString InputMode;
		VXMLString MrcpResult;
		VXMLString ClassId;
		VXMLString Confidence;
		VXMLString Meaning;
		VXMLString Recognition;
		//VXMLString DialogTransferAudioUri; // optimaize this -> add to structure

		bool NeedReprompt = true;
		size_t PromptCount = 0;
		size_t PromptLine = 0;
		bool IsDialogStarted = false;
		//bool IsDialogTransferSent = false;
		//bool IsDialogTransferAudioRepeat = true;
		//bool PlayWavCompleted = false; // optimize
		//bool RecognizeCompleted = false; //optimize
		//bool DialogTransferAudioRepeat = false; //optimize
		//bool TextCompleted = false; //optimaize
		//bool RecognizeInput = false; //optimize
		//bool StreamCompleted = false;
		RecognizeInfo RecInfo;
		VXMLString Audiomode;

		bool IsEnabled() const { return mIsEnabled; } // ToDo : delete this method
		MenuPtr Clone()
		{
			return std::make_shared<CMenu>(*this);
		}
	private:
		bool mIsEnabled = false;
	};

}

/******************************* eof *************************************/