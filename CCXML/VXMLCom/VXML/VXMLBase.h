/************************************************************************/
/* Name     : VXMLCom\VXMLCom.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Nov 2009                                               */
/************************************************************************/

#pragma once

#include <string>
#include <comdef.h>
#include <mutex>
#include <unordered_map>
#include <boost/asio.hpp>
//#include <boost/shared_ptr.hpp>
//#include <boost/weak_ptr.hpp>
#include <list>

#include "..\Common\Utils.h"
#include "..\Common\ComBridgeObject.h"
//#include "..\Common\KeepAlive.h"

#include "EvtModel.h"
#include "..\Engine\Engine.h"
//#include "stringfunctions.h"
#include "Patterns\\string_functions.h"

#import "..\..\..\TelLib\TelLib.dll" no_namespace

class IEngineCallback;
class CVXMLComInterface;
//class CEngineLog;
//class IVOX;
struct Iq931Info;
class CTelServerEmulator;
//class CSystemLog;

class SystemLogEx;
class ExtraLog;

namespace VXML
{
	namespace CLIENT_ADDRESS{
		const __int64 ANY_CLIENT = ((__int64)0x00000002 << 32);
	}

	typedef std::wstring VXMLString;
	typedef /*unsigned*/ __int64 ObjectId;

	class CSession;
	class CDocument;
	class CApplication;
	//class CScopedNameTable;
	class CScopeService;
	class CScriptEngine;
	class CResourceCache;
	class IDebuger;
	class CEngineEvent;
	class CItemEvent;

	using EventPtr   = std::shared_ptr < CEngineEvent > ;
	//using EventWPtr  = std::weak_ptr < CEngineEvent >;

	using ItemEventPtr = std::shared_ptr < CItemEvent >;
	using ItemEventWPtr = std::weak_ptr < CItemEvent >;

	using EventQueue = std::list<ItemEventPtr>;

	typedef std::shared_ptr<CDocument> DocumentPtr;
	typedef std::unique_ptr< CScriptEngine > EnginePtr;
	//typedef std::shared_ptr< CScopedNameTable > ScopedNameTablePtr;
	typedef std::shared_ptr< CResourceCache > ResourceCachePtr;
	//typedef std::weak_ptr< IDebuger > DebugerPtr;
	//typedef std::shared_ptr< IDebuger > DebugerPtr2;
	typedef std::weak_ptr< CSession > SessionPtr;
	typedef std::shared_ptr< CSession > SessionPtr2;
	typedef std::shared_ptr< CApplication > ApplicationPtr;
	
	using ExtraLogPtr = std::shared_ptr<ExtraLog>;
	using SystemLogPtr = std::shared_ptr<SystemLogEx>;

	//typedef std::shared_ptr< IEngineCallback   > IEngineCallbackPtr;
	//typedef std::shared_ptr< Iq931Info   > Q931Ptr;
	typedef CComPtr<Iq931Info> Q931Ptr;
	//typedef CComBridge<Iq931Info>  Q931Dispatch; //???

	//typedef std::weak_ptr< IEngineCallback   > IEngineCallbackPtr;
	using IEngineCallbackPtr = IEngineCallback*;
	typedef std::shared_ptr< IEngineCallback   > IEngineCallbackPtr2;


	typedef std::shared_ptr< CTelServerEmulator  > TelServPtr;
	using ScopeServicePtr      = std::shared_ptr<CScopeService>;
	using SavedScopeServicePtr = std::weak_ptr  <CScopeService>;

	class CNameTable;
	using SessionObject = std::shared_ptr <CNameTable>;
	//using WeakObject    = std::weak_ptr   <CNameTable>;
	using WeakSessionObject = std::weak_ptr<CNameTable>;
	typedef CComBridge<CNameTable>  NameTableDispatch;
	typedef CComBridge<CNameTable>* NameTableDispatchPtr;

	class VXMLGrammar;
	typedef std::shared_ptr<VXMLGrammar>SessionGrammar;
	typedef CComBridge<VXMLGrammar>  GrammarDispatch;
	typedef CComBridge<VXMLGrammar>* GrammarDispatchPtr;

	template <class T>
	using ShareObjectPtr = std::shared_ptr<T>;

	template <class T>
	using WeakObjectPtr = std::weak_ptr<T>;

	class CTag;
	using TagPtr = std::shared_ptr <CTag>;
	using TagWPtr = std::weak_ptr   <CTag>;

	const auto NULLTAG = TagPtr(nullptr);

	using WaitingEvents		= std::list<VXMLString>;
	using NamelistContainer = std::list<VXMLString>;

	using MessageHandler = std::function <void(const CMessage&, const ItemEventPtr&)>;

	//struct TWaitingEvents
	//{
	//	VXMLString name;
	//	MessageHandler handle;

	//	TWaitingEvents(VXMLString _name, MessageHandler&& _handler)
	//		: name(std::move(_name)), handle(std::move(_handler))
	//	{

	//	}
	//};
	//using WaitingEventsContainer = std::list<TWaitingEvents>;
	using WaitingEventsContainer = std::unordered_map<VXMLString, MessageHandler>;

	struct CExecContext
	{
		SessionPtr			 pSes;
		DocumentPtr			 pDoc;
		std::list<DocumentPtr>	predefineDocuments;
		std::list<DocumentPtr>  previousDoc;

		ApplicationPtr		 pApp;
		//ScopedNameTablePtr	pVar;
		SavedScopeServicePtr pVar;
		EnginePtr		     pVbs;
		IEngineCallbackPtr	 pEng;
		//ResourceCachePtr	 pRes;
		SystemLogPtr	     pLog;
		ExtraLogPtr		     pXLog;
		Q931Ptr 			 p931;
		VXMLString			 sXML;
		//DebugerPtr          pDbg;
		//IVOXPtr             pVox;
		ItemEventPtr            pEvt;
		//SessionObject        pGlobalsVar;
	};

	class VXMLVariant
	{
	public:
		static CComVariant& ChangeType(CComVariant& v, VARTYPE vtNew)
		{
			HRESULT hr = v.ChangeType(vtNew);
			if (FAILED(hr))
			{
				throw _com_error(hr);
			}
			return v;
		}
	};

	struct TDefaultVXMLSettings
	{
		TDefaultVXMLSettings(): sTimeout(L"3000"),
			sInterdigittimeout(L"2000"),
			sWaitTime(L"10000"),
			sTermchar(L"#")//,
		    //sKeepAliveTimeout(L"1")
		{}
		std::wstring sTimeout;
		std::wstring sInterdigittimeout;
		std::wstring sWaitTime;
		std::wstring sTermchar;
		//std::wstring sKeepAliveTimeout;
	};

	struct TFolders
	{
		std::wstring sVXMLFolder;
		std::wstring sWavFolder;
		std::wstring sGrammarFolder;
		std::wstring sVideoFolder;
		std::wstring sRecognitionsFolder;
	};
	struct TSystem
	{
		TSystem(): sKeepAliveTimeout(L"1")
		{}
		std::wstring sKeepAliveTimeout;
		bool bRPCore = false;
	};

	struct TVnHosts
	{
		TVnHosts() = default;
		TVnHosts(
			const std::string& aHost,
			const std::string& aPort)
			: Host(aHost)
			, Port(aPort)
		{}

		std::string Host;
		std::string Port;
	};

	using VnHostCollector = std::vector<TVnHosts>;

	const size_t DEFAULT_TIMEOUT_SEC = 3;

	struct TTextToSpeech
	{
		std::wstring sVoice;
		std::wstring sRTSPHost;
		int sRTSPPort;
		//int sThreshold;
		int sPlayTrigg;
		int sNonThreshold;
		int sNonPlayTrigg;
		int sMagicWord;
		VnHostCollector sVnHosts;
		std::wstring sVnUrl;
		size_t szVnTimeout = VXML::DEFAULT_TIMEOUT_SEC;
		int iConfidenceLevel = 0;
	};

	const VXMLString SYMBOLNOTFOUND_DBGMSG      = L"Symbol not found";
	const VXMLString PROCESSNOTAVAILABLE_DBGMSG = L"Process not available";

	enum class BadfetchErrors
	{
		BE_UNKNOWN = 0,
		BE_MENU_NOT_FOUND = 1,
		BE_SYNTAX,
		BE_ASYNC,
		BE_LOAD_DOCUMENT,
		BE_TTS,
		BE_ASR,
		BE_RUN
	};

	inline VXMLString BadfetchErrorsToString(BadfetchErrors aError)
	{
		VXMLString result;

		switch (aError)
		{
		case BadfetchErrors::BE_UNKNOWN:
		{
			result = L"BE_UNKNOWN";
			break;
		}
		case BadfetchErrors::BE_MENU_NOT_FOUND:
		{
			result = L"BE_MENUNOTFOUND";
			break;
		}
		case BadfetchErrors::BE_SYNTAX:
		{
			result = L"BE_SYNTAX";
			break;
		}
		case BadfetchErrors::BE_ASYNC:
		{
			result = L"BE_ASYNC";
			break;
		}
		case BadfetchErrors::BE_LOAD_DOCUMENT:
		{
			result = L"BE_LOADDOCUMENT";
			break;
		}
		case BadfetchErrors::BE_TTS:
		{
			result = L"BE_TTS";
			break;
		}
		case BadfetchErrors::BE_ASR:
		{
			result = L"BE_ASR";
			break;
		}
		case BadfetchErrors::BE_RUN:
		{
			result = L"BE_RUN";
			break;
		}
		default:
		{
			result = L"BE_UNKNOWN";
		}
		};

		return result;
	}

	struct TransferInfo
	{
		VXMLString DialogTransferAudioUri;
		bool IsDialogTransferSent = false;
		bool IsDialogTransferAudioRepeat = true;
		bool PlayWavCompleted = false;
		//bool RecognizeCompleted = false;
		bool DialogTransferAudioRepeat = false;
		//bool TextCompleted = false;
		//bool RecognizeInput = false;
		//bool StreamCompleted = false;
	};

	struct RecognizeInfo
	{
		bool PlayWavCompleted = false;
		bool RecognizeCompleted = false;
		bool RecognizeInput = false;
		bool TextCompleted = false;
		bool StreamCompleted = false;
	};
}