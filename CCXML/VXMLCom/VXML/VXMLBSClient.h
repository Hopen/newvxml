///************************************************************************/
///* Name     : VXMLCom\VXMLBSClient.h                                    */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-IT                                                  */
///* Date     : 14 Jul 2010                                               */
///************************************************************************/
#pragma once
//

//#include "bsclient.h"
//#include <iostream>
//#include <istream>
//#include <ostream>
//#include <string>
//#include <boost/asio.hpp>
//#include <boost/lexical_cast.hpp>
//#include "VXMLBase.h"
//#include "..\StringFunctions.h"
//
//using boost::asio::ip::tcp;
//
//namespace VXML
//{
//
//	//enum E_CLIENT
//	//{
//	//	e1001 = 1001, // Invalid response
//	//	e1002, // Exception
//	//	e1003
//	//};
//
//	class CSyncClient
//	{
//	public:
//		CSyncClient(const VXMLString& _method):m_method(_method.c_str())
//		{
//		}
//		virtual ~CSyncClient(){}
//
//		int Send(const VXMLString& _servername, const VXMLString& _params, VXMLString& sAnswer)
//		{
//			try
//			{
//				boost::asio::io_service io_service;
//
//				std::string szServerName = boost::lexical_cast<std::string>(_servername);
//				// Get a list of endpoints corresponding to the server name.
//				tcp::resolver resolver(io_service);
//				tcp::resolver::query query(szServerName, "http");
//				tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
//				tcp::resolver::iterator end;
//
//				// Try each endpoint until we successfully establish a connection.
//				tcp::socket socket(io_service);
//				boost::system::error_code error = boost::asio::error::host_not_found;
//				while (error && endpoint_iterator != end)
//				{
//				  socket.close();
//				  socket.connect(*endpoint_iterator++, error);
//				}
//				if (error)
//				  throw boost::system::system_error(error);
//
//				// Form the request. We specify the "Connection: close" header so that the
//				// server will close the socket after transmitting the response. This will
//				// allow us to treat all data up until the EOF as the content.
//				boost::asio::streambuf request;
//				std::ostream request_stream(&request);
//				request_stream << m_method << " " << _bstr_t(_params.c_str()) << " HTTP/1.0\r\n";
//				request_stream << "Host: " << szServerName << "\r\n";
//				request_stream << "Accept: */*\r\n";
//				request_stream << "Connection: close\r\n\r\n";
//
//				// Send the request.
//				boost::asio::write(socket, request);
//
//				// Read the response status line.
//				boost::asio::streambuf response;
//				boost::asio::read_until(socket, response, "\r\n");
//
//				// Check that response is OK.
//				std::istream response_stream(&response);
//				std::string http_version;
//				response_stream >> http_version;
//				unsigned int status_code;
//				response_stream >> status_code;
//				std::string status_message;
//				std::getline(response_stream, status_message);
//				if (!response_stream || http_version.substr(0, 5) != "HTTP/")
//				{
//				  //printf("Invalid response");
//				  return 1;
//				}
//				if (status_code != 200)
//				{
//				  //printf("Response returned with status code: %i\n", status_code);
//				  return status_code;
//				}
//
//				// Read the response headers, which are terminated by a blank line.
//				boost::asio::read_until(socket, response, "\r\n\r\n");
//
//				// Process the response headers.
//				
//				std::string header, parthead;
//				while (std::getline(response_stream, parthead) && parthead != "\r")
//				{
//					header+=parthead + "\n";
//				}
//
//				// Write whatever content we already have to output.
//				std::string szReceive;
//				if (response.size() > 0)
//				{
//					std::ostringstream out;
//					out << &response;;
//					szReceive = out.str();
//				}
//
//				// Read until EOF, writing data to output as we go.
//				while (boost::asio::read(socket, response,
//					  boost::asio::transfer_at_least(1), error))
//				{
//					std::ostringstream out;
//					out << &response;;
//					szReceive += out.str();
//				}
//				
//				sAnswer = boost::lexical_cast<std::wstring>(szReceive);
//
//				if (error != boost::asio::error::eof)
//				  throw boost::system::system_error(error);
//				}
//				catch (std::exception& e)
//				{
//					//printf("Exception: %s\n", e.what());
//					//return e1002;
//					throw e;
//				}
//
//			return 0;
//		}
//	private:
//		_bstr_t m_method;
//		
//	};
//
//}
