/************************************************************************/
/* Name     : VXMLCom\VXMLNEngine.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 7 Dec 2009                                                */
/************************************************************************/

#pragma once

#include <activscp.h>
#include "..\EventBridge.h"
#include "VXMLSession.h"
//#include "..\Common\EngineScriptSite.h"

namespace VXML
{
	extern const GUID CLSID_VBScript; 


	class CXMLScriptSite :
		public CComObjectRootEx<CComSingleThreadModel>,
		public IActiveScriptSite
	{
	public:

		DECLARE_PROTECT_FINAL_CONSTRUCT()

		BEGIN_COM_MAP(CXMLScriptSite)
			COM_INTERFACE_ENTRY(IActiveScriptSite)
		END_COM_MAP()

		CXMLScriptSite()
		{
		}
		~CXMLScriptSite()
		{
		}

		// ������ IActiveScriptSite...
		virtual HRESULT __stdcall GetLCID(LCID *plcid)
		{
			return S_OK;
		}

		virtual HRESULT __stdcall GetItemInfo(LPCOLESTR pstrName,
			DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppti)
		{
			if (ppti)
			{
				*ppti = NULL;

				// ���� ������ ITypeInfo... 
				if (dwReturnMask & SCRIPTINFO_ITYPEINFO) {
					*ppti = m_spTypeInfo;
					(*ppti)->AddRef();
				}
			}

			// ���� Windows Scripting ��������� ppunkItem...
			if (ppunkItem)
			{
				*ppunkItem = NULL;

				// ���� Windows Scripting ������� IUnknown ������ �������...
				if (dwReturnMask & SCRIPTINFO_IUNKNOWN)
				{
					// ...� ��������� ������ � ������ pstrName...
					USES_CONVERSION;
					//if (!lstrcmpi(_T("event1"), pstrName)) 
					ScriptObjects::iterator it = m_objs.find(pstrName);
					if (it != m_objs.end())
					{
						// ...�� ����������� ��� ������.
						*ppunkItem = it->second.pdispVal;
						// ...� AddRef'�� ��� ������...
						(*ppunkItem)->AddRef();
					}
				}
			}
			return S_OK;
		}

		virtual HRESULT __stdcall GetDocVersionString(BSTR *pbstrVersion)
		{
			return S_OK;
		}

		virtual HRESULT __stdcall OnScriptTerminate(
			const VARIANT *pvarResult, const EXCEPINFO *pexcepInfo)
		{
			return S_OK;
		}

		virtual HRESULT __stdcall OnStateChange(SCRIPTSTATE ssScriptState)
		{
			return S_OK;
		}

		virtual HRESULT __stdcall OnScriptError(
			IActiveScriptError *pscriptError)
		{
			// ��� ��������� �������� � ������ ������ � �������.
			// ����� ��������� ���������� � pscriptError.

			//ATLASSERT(m_pSes);

			EXCEPINFO ei;
			HRESULT hr = pscriptError->GetExceptionInfo(&ei);

			//USES_CONVERSION;
			//::MessageBox(::GetActiveWindow(), ei.bstrDescription, L"Script Error", MB_SETFOREGROUND);

			SessionPtr2 pSes = m_pSes.lock();

			CMessage evt(L"error.semantic");

			if (SUCCEEDED(hr) && bstr_t(ei.bstrDescription).length())
			{
				VXMLString reason = bstr_t(ei.bstrDescription) + L" : " + bstr_t(ei.bstrSource);
				evt[L"reason"] = reason.c_str();
			}
			else
			{
				evt[L"reason"] = L"Unknown exception";
			}
			evt[L"tagname"] = L"Script_Error";
			evt[L"description"] = L"Runtime vbscript error";
			pSes->DoError(evt, L"VBENGINE");


			return S_OK;
		}

		virtual HRESULT __stdcall OnEnterScript(void)
		{
			return S_OK;
		}

		virtual HRESULT __stdcall OnLeaveScript(void)
		{
			return S_OK;
		}

		bool AddObject(const std::wstring _name, const CComVariant& object/*IDispatch *object*/)
		{
			//NameTableDispatchPtr name = dynamic_cast<NameTableDispatchPtr>(object);
			//CComPtr<IUnknown> spUnkScriptObject;

			//HRESULT hr = name->QueryInterface(IID_IUnknown,
			//	(void **)&spUnkScriptObject);

			//m_objs[_name] = spUnkScriptObject;
			//return S_OK;

			m_objs[_name] = object;
			return S_OK;

		}

		HRESULT DelObject(const std::wstring _name)
		{
			ScriptObjects::iterator it = m_objs.find(_name);
			if (it != m_objs.end())
			{
				m_objs.erase(it);
			}

			return S_OK;
		}

		HRESULT Clear()
		{
			//for (ScriptObjects::iterator it = m_objs.begin(); it!=m_objs.end(); ++it)
			//{
			//}
			m_objs.clear();
			return S_OK;
		}


		void SetSession(SessionPtr  pSes)
		{
			m_pSes = pSes;
		}

	public:
		//CComPtr<IUnknown> m_spUnkScriptObject;
		CComPtr<ITypeInfo> m_spTypeInfo;

	private:
		//typedef std::map<std::wstring,CComPtr<IUnknown>> ScriptObjects;
		typedef std::map<std::wstring, CComVariant> ScriptObjects;
		ScriptObjects  m_objs;

		SessionPtr m_pSes;

	};

	class CScriptEngine
	{
	private:
		CComObject<CXMLScriptSite> * m_pMySite;
		CComPtr<IActiveScriptParse>    m_spASP;
		CComPtr<IActiveScript>         m_spAS;
		VXMLString                  m_scriptSource;
		SystemLogPtr			m_Log;

		void Normalization(std::wstring& sExpr, std::wstring simbolsIn[], std::wstring simbolsOut[]);
	public:
		CScriptEngine(SessionPtr2 pSes, SystemLogPtr pLog);
		~CScriptEngine();

		void Clear();

		HRESULT AddObject(const std::wstring& sName, /*IDispatch**/ const CComVariant &object);
		HRESULT AddObject2(const std::wstring& sName, /*IDispatch**/ const CComVariant &object);
		HRESULT DelObject(const std::wstring &sName);

		HRESULT ExecAssignNormalStatement(const std::wstring& sExpr);
		HRESULT ExecNormalStatement(const std::wstring& sExpr);
		HRESULT ExecStatement(const std::wstring& sExpr);
		HRESULT ExecStatement2(const std::wstring& sExpr);

		CComVariant ExprEval(const std::wstring& sExpr);
		void Assign(const std::wstring& sExpr, const CComVariant& value);
		bool ExprEvalToBool(const std::wstring& sExpr);
		int ExprEvalToInt(const std::wstring& sExpr);
		std::wstring ExprEvalToStr(const std::wstring& sExpr);
		CComVariant ExprEvalToType(const std::wstring& sExpr, VARTYPE vt);
		HRESULT ExecScript(const std::wstring& sExpr);
		VXMLString ScriptSource()const { return m_scriptSource; }
	};

}