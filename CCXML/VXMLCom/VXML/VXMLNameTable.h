/************************************************************************/
/* Name     : VXMLCom\VXMLNameTable.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 7 Dec 2009                                                */
/************************************************************************/

#pragma once

#include <map>
#include <queue>
#include <vector>
#include <list>
#include "VXMLBase.h"
#include "EvtModel.h"
#include "InstanceController.h"


#define VAR_SIGNATURE			0x12345678

namespace VXML
{
	class CNameTable;

	//struct VAR
	//{
	//	CComVariant	vValue;
	//	VXMLString	sName;
	//	bool		bReadOnly;
	//	DWORD		dwSignature;
	//	VAR(const VXMLString& name, const CComVariant& value, bool readonly)
	//		: sName(name), vValue(value), bReadOnly(readonly),
	//		dwSignature(VAR_SIGNATURE)
	//	{
	//	}
	//	VAR(const VAR& rhs)
	//	{
	//		this->vValue      = rhs.vValue;
	//		this->sName       = rhs.sName;
	//		this->bReadOnly   = rhs.bReadOnly;
	//		this->dwSignature = rhs.dwSignature;
	//	}
	//	VAR()
	//	{
	//		::VariantInit(&vValue);
	//	}
	//	void SetValue(VARIANT* pNewVal);
	//	void SetValue(const CComVariant& newVal);
	//	//void Clear();
	//	bool IsReadOnly()const { return bReadOnly; }

	//	VXMLString AsString()const { return static_cast<wchar_t*>(vValue.bstrVal); }
	//};

	struct VAR : public InstanceController<VAR>
	{
		CComVariant	vValue;
		VXMLString	sName;
		bool		bReadOnly;
	private:
		bool		bIsEmpty;
	public:

		VAR();
		VAR(const VXMLString& name, const CComVariant& value, bool readonly);
		VAR(const VAR& rhs);

		VAR& operator=(VAR const& rhs);

		virtual ~VAR();

		//void SetValue(VARIANT* pNewVal);
		void SetValue(const CComVariant& newVal);
		//void SetInitialValue(const CComVariant& newVal);

		bool IsEmpty()const { return bIsEmpty; }

		VXMLString AsString()const { return static_cast<wchar_t*>(vValue.bstrVal); }
	};




	//class CScopedNameTable
	//{
	//private:
	//	//typedef CComPtr<CNameTable> CComTable;
	//	//typedef std::pair<VXMLString,CNameTable* > ScopePair;
	//	//typedef std::pair<VXMLString,CComVariant > ScopePair;
	//	//typedef std::pair<VXMLString,CComTable > ScopePair;
	//	typedef std::pair<VXMLString,SessionObject> ScopePair;
	//	typedef std::list<ScopePair> Scopes;
	//	//typedef std::vector<CNameTable* /*SessionObject*/> StackGlobalNames;
	//	//typedef std::vector<CComVariant /*SessionObject*/> StackGlobalNames;
	//	typedef std::vector<SessionObject> StackGlobalNames;

	//	//CNameTable* m_pGlobalNames;
	//	//CComVariant m_pGlobalNames;
	//	SessionObject m_pGlobalNames;
	//	//CComTable	m_pGlobalNames;
	//	Scopes		     m_Scopes;
	//	StackGlobalNames m_gStack;

	//	//int m_count;

	//public:
	//	CScopedNameTable();
	//	~CScopedNameTable();
	//	void PushScope(const VXMLString& sName);
	//	void PopScope();
	//	//void PushStack();
	//	//void PopStack();
	//	VAR& AddVar(const VXMLString& sName, const CComVariant& val, bool bReadOnly);
	//	void DelVar(const VXMLString& sName);
	//	VAR* Find(const VXMLString& sName);
	//	SessionObject FindScope(const VXMLString& sName);
	//	VAR& Lookup(const VXMLString& sName);
	//	//CNameTable*	GetGlobal()const{return m_pGlobalNames/*.get()*/;}
	//	CComVariant	GetGlobal()const;
	//	//{
	//	//	if (CNameTable * nt = dynamic_cast<CNameTable>(m_pGlobalNames.pdispVal))
	//	//		return nt;
	//	//	return NULL;
	//	//}
	//};

	class CScopeService //: /*public singleton <CScopeService>,*/ public std::enable_shared_from_this <CScopeService>
	{
		/*friend class singleton<CScopeService>;*/
		//friend class CScopeSaver;

	public:
		using ScopeContainer = std::vector <SessionObject>;

		VAR& AddVar(const VXMLString& sName, const CComVariant& val, bool bReadOnly);
		VAR* Find(const VXMLString& sName);
		VAR& Lookup(const VXMLString& sName);
		void DelVar(const VXMLString& sName);

		//CComVariant	GetGlobal()const;
		CScopeService();
		ScopeContainer GetStack()const  { return m_scopes; }

		CScopeService(const CScopeService& rhs)
		{
			std::copy(rhs.m_scopes.begin(), rhs.m_scopes.end(), std::back_inserter(m_scopes));
		}
	private:
		//template <class T>
		//void _PushScopeImpl(T&& scope, std::false_type)
		//{
		//	m_scopes.emplace_back(std::forward<T>(scope));
		//}

		//void _PushScopeImpl(VXMLString scope, std::true_type)
		//{
		//	PushScope(std::make_shared<CNameTable>());
		//}
	//public:
	//	template <class T>
	//	void PushScope(T&& scope)
	//	{
	//		_PushScopeImpl(std::forward<T>(scope), std::is_array<std::remove_reference<T>>);
	//	}
	public:
		//void PushScope();

		template <class T>
		void PushScope(T&& scope)
		{
			m_scopes.emplace_back(std::forward<T>(scope));
		}
		void PopScope()
		{
			m_scopes.pop_back();
		}

	private:
		//SessionObject  m_GlobalScope;
		//WeakObject     m_currEvent;


		ScopeContainer m_scopes;

	public:

		//template <class T>
		//void PushScope(T&& scope)
		//{

		//}


		class CScopeSaver
		{
			using CScopeServicePtr = std::shared_ptr<CScopeService>;
		public:
			template <class T>
			CScopeSaver(const CScopeServicePtr& scopeObject, T&& pNewScope)
				: m_pSavedScope(scopeObject)
			{
				//if (pNewScope.get())
				//{
					scopeObject->PushScope(std::forward<T>(pNewScope));
					//m_bHasPushed = true;
				//}
			}
			~CScopeSaver()
			{
				if (m_pSavedScope /*&& m_bHasPushed*/)
					m_pSavedScope->PopScope();
			}
		private:
			CScopeServicePtr m_pSavedScope;
			//bool m_bHasPushed{ false };
		};
	};


}