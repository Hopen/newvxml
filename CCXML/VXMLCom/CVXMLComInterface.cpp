// CVXMLComInterface.cpp : Implementation of CVXMLComInterface

#include "stdafx.h"
#include "CVXMLComInterface.h"
#include "VXMLResCache.h"
#include "sv_helpclasses.h"
#include "ce_xml.hpp"
#include "VXMLBase.h"
#include "VXMLDebug.h"
#include "EngineConfig.h"
#include "assert.h"
#include "sid.h"
//#include "Log/SystemLogEx.h"
#include "..\Common\ExtraLog.h"
#include "..\Common\SystemLogEx.h"
#include "VXMLEngineConfig.h"

#import "..\Bin\ISUPMessageProcessor.dll" rename_namespace("ISUP_namespace") named_guids

static void LogExtra(const wchar_t* aError)
{
	ExtraLog log;
	log.Init(GetGlobalScriptId(), L"finest", L"C:\\IS3\\Logs\\vxml_extra_%y-%m-%d.log");
	log.LogWarning(aError);
}

// CVXMLComInterface
HRESULT CVXMLComInterface::FinalConstruct()
{
	try
	{
		//m_systemLog = std::make_shared<SystemLogEx>();
		m_extraLog = std::make_shared<ExtraLog>();
		m_vbsLog = std::make_shared<ExtraLog>();

		m_pSes.reset(new DEBUG_NEW_PLACEMENT VXML::CSession(/*m_CS1, m_CS2*/));

	}
	catch (...)
	{
		LogExtra(L"CCXML final construct exception");

		Error(L"VXML final construct exception", GUID_NULL, E_FAIL);
		return E_FAIL;
	}

	return S_OK;
}

void CVXMLComInterface::FinalRelease()
{
	try
	{
		m_systemLog->LogInfo( L"VXML FinalRelease...");
		//m_pDebuger.reset();
		//m_pResCache.reset();
		//m_pVar.reset();
		m_pSes.reset();
#ifdef _EMULATE_TELSERVER
		m_pTelserv.reset();
#endif
		m_systemLog.reset();
		m_extraLog.reset();

		//::DeleteCriticalSection(&m_CS2);
		//::DeleteCriticalSection(&m_CS1);
	}
	catch (...)
	{
		LogExtra(L"CCXML final release exception");

		Error(L"VXML final release exception", GUID_NULL, E_FAIL);
	}

	//	_CrtDumpMemoryLeaks();
}



STDMETHODIMP CVXMLComInterface::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IVXMLComInterface
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}



//STDMETHODIMP CVXMLComInterface::Init(PMESSAGEHEADER pMsgHeader)
//{
//	// TODO: Add your implementation code here
//
//	return S_OK;
//}

STDMETHODIMP CVXMLComInterface::Destroy(void)
{
	// TODO: Add your implementation code here
	if (m_pSes.get() && m_pSes->IsInitialized())
	{
		//delete m_pSes;
		//m_pSes = NULL;

		//if (m_pQ931Info)
		//	m_pQ931Info->Release();

		m_systemLog->LogFinest(L"VXML session destroyed");
	}
	else
	{
		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
		return E_FAIL;
	}

	//if (m_pQ931Info)
	//	m_pQ931Info->Release();


	return S_OK;
}

STDMETHODIMP CVXMLComInterface::DoStep(void)
{
	if (m_pSes.get() != NULL && m_pSes->IsInitialized())
	{
		if (m_pSes->GetState() != VMS_ENDSCRIPT)
			m_pSes->DoStep();		
	}
	else
	{
		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
		return E_FAIL;
	}

	return S_OK;
}

STDMETHODIMP CVXMLComInterface::GetState(void)
{
	// TODO: Add your implementation code here
	if (m_pSes.get() != NULL && m_pSes->IsInitialized())
	{
		return (HRESULT)m_pSes->GetState();
	}
	return E_FAIL;
}

STDMETHODIMP CVXMLComInterface::SetEvent(PMESSAGEHEADER pMsgHeader)
{
#ifdef _EMULATE_TELSERVER
	if (m_pTelserv)
		m_pTelserv->SetEvent(pMsgHeader);
#endif

	if (m_pSes.get() != NULL && m_pSes->IsInitialized())
	{
		if (m_pSes->GetState() != VMS_ENDSCRIPT)
		{
			m_systemLog->LogInfo( L"IN %s", DumpMessage(pMsgHeader).c_str());
			m_pSes->SetEvent(CMessage(pMsgHeader));
		}
	}
	else
	{
		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
		return E_FAIL;
	}
	return S_OK;
}

STDMETHODIMP CVXMLComInterface::GetNextDoStepInterval(LPDWORD lpdwInterval)
{
	// TODO: Add your implementation code here
	*lpdwInterval = (DWORD)-1;
	//*lpdwInterval = 100;
	return S_OK;
}

class CLocker
{
private:
	CRITICAL_SECTION& m_CS;

public:
	CLocker(CRITICAL_SECTION& cs) : m_CS(cs)
	{
		::EnterCriticalSection(&m_CS);
	}
	~CLocker()
	{
		::LeaveCriticalSection(&m_CS);
	}
};

STDMETHODIMP CVXMLComInterface::Init(PMESSAGEHEADER pMsgHeader, IEngineCallback* pCallback)
{
	try
	{
		mEngineConfig = std::make_unique<CVXMLEngineConfig>();

		CMessage msg(pMsgHeader);
		msg.CheckParam(L"ScriptID", CMessage::Int64, CMessage::Mandatory);

		CParam* pSID = msg.ParamByName(L"ScriptID");
		//if (true)
		//{
		//	mEngineConfig;
		//}
		// Log initialization

		//m_systemLog->Init(mEngineConfig->sLevel.c_str(), mEngineConfig->sLogFileName.c_str());
		//m_systemLog->SetSID(pSID->AsInt64());

		// ExtraLog initialization
		try
		{
			m_extraLog->Init(pSID->AsInt64(), mEngineConfig->sLevel.c_str(), mEngineConfig->m_sExtraLogName.c_str());
			m_vbsLog->Init(pSID->AsInt64(), mEngineConfig->sLevel.c_str(), mEngineConfig->m_sVBSLogName.c_str(), true);
		}
		catch (const std::exception& aError)
		{
			LogExtra((std::wstring(L"Exception while create extra log: ") + stow(aError.what())).c_str());
		}
		catch (...)
		{
			LogExtra(L"Unknown exception while create extra log");
		}

		try
		{
			m_systemLog = std::make_shared<SystemLogEx>(
				pSID->AsInt64(),
				mEngineConfig->GetFileName(), 
				mEngineConfig->GetLevel());
		}
		catch (const std::exception& aError)
		{
			LogExtra((std::wstring(L"Exception while create new log: ") + stow(aError.what())).c_str());
			m_extraLog->LogWarning(L"Exception while create new log", stow(aError.what()).c_str());
		}
		catch (...)
		{
			LogExtra(L"Unknown exception while create new log");
			m_extraLog->LogWarning(L"Unknown exception while create new log");
		}

		VXML::SessionCreateParams params;

		m_systemLog->LogInfo( L"Initializing VXML 2 session...");

		m_sQ931XML  = L"";
		CParam* pQ931 = msg.ParamByName(L"SigInfo");
		if (pQ931 && !pQ931->AsWideStr().empty())
		{
			if (!CreateQ931ProtocolInfo(pQ931->AsWideStr()))
				m_systemLog->LogInfo( L"CreateQ931ProtocolInfo failed");
		}

		m_systemLog->LogInfo( msg.Dump().c_str());

		params.pEvtPrc		  = /*m_pDebuger;*/pCallback;
		params.pInitMsg		  = pMsgHeader;
		params.pLog			  = m_systemLog;
		params.pExtraLog	  = m_extraLog;
		params.pVBSLog		  = m_vbsLog;
		params.dwSurrThread	  = ::GetCurrentThreadId();
		params.VXMLFolders    = mEngineConfig->XMLFolders;
		params.VXMLDefSet     = mEngineConfig->VXMLDefSet;
		params.Text2Speech    = mEngineConfig->Text2Speech;
		params.VXMLSystem     = mEngineConfig->Systems;
		params.pQ931Info	  = m_pQ931Info;
		params.sQ931XML       = m_sQ931XML;
		params.pSession = m_pSes;

		try
		{
			if (!m_pSes->Initialize(params))
			{
				m_systemLog->LogFinest(L"CVXMLComInterface: VXML session initializing failed");
				return E_FAIL;
			}
		}
		catch (const std::exception& aError)
		{
			LogExtra((std::wstring(L"Exception while Initialize session: ") + stow(aError.what())).c_str());
		}
		catch (...)
		{
			LogExtra(L"Unknown exception while Initialize session");
		}
		
		m_systemLog->LogFinest( L"VXML session initialized");
	}
	catch (std::exception& aError)
	{
		//if (m_systemLog)
		//	m_systemLog->LogWarning(L"Error: %s", stow(aError.what()).c_str());

		LogExtra(stow(aError.what()).c_str());
		return DISP_E_EXCEPTION;
	}
	catch (std::wstring& e)
	{
		if (m_systemLog)
			m_systemLog->LogWarning(L"Error: %s", e.c_str());

		LogExtra(e.c_str());
		return DISP_E_EXCEPTION;
	}
	catch (_com_error& e)
	{
		if (m_systemLog)
			m_systemLog->LogWarning(L"Error: %s", (LPCWSTR)e.Description());
		LogExtra((LPCWSTR)e.Description());
		return DISP_E_EXCEPTION;
	}
	catch (...)
	{
		if (m_systemLog)
			m_systemLog->LogWarning(L"Error: unknown exception occurred");
		LogExtra(L"Error: unknown exception occurred");
		return DISP_E_EXCEPTION;
	}

	return S_OK;

}

//static _bstr_t SelectSingleNode(MSXML2::IXMLDOMDocument2Ptr pDoc, const std::wstring& sQuery)
//{
//	_bstr_t  retVal(L"");
//	MSXML2::IXMLDOMNodePtr	Node;
//	if (pDoc)
//	{
//		Node = pDoc->selectSingleNode(sQuery.c_str());
//		if (Node)
//			retVal = Node->text;
//	}
//	if (Node)
//		Node.Release();
//	return retVal;
//}

static _bstr_t SelectValue(MSXML2::IXMLDOMDocument2Ptr pDoc, const std::wstring& sQuery, const std::wstring& sValue)
{
	MSXML2::IXMLDOMNodePtr	       Node;
	MSXML2::IXMLDOMElementPtr      NodeElem;
	MSXML2::IXMLDOMNamedNodeMapPtr NodeMap;
	_bstr_t  retVal(L"");
	if (pDoc)
	{
		Node = pDoc->selectSingleNode(sQuery.c_str());
		if (Node)
		{
			NodeElem =  Node;
			NodeMap  =  NodeElem->Getattributes();
			Node     =  NodeMap->getNamedItem(sValue.c_str());
			if (Node)
			{
				retVal =  Node->text;
			}
		}
	}
	if (Node)    	Node     .Release();
	if (NodeElem)	NodeElem .Release();
	if (NodeMap)	NodeMap  .Release();

	return retVal;
}
bool CVXMLComInterface::CreateQ931ProtocolInfo(const std::wstring &SigInfo)
{
	m_systemLog->LogFinest( L"Initializing Q931 COM protocol...");

	CComPtr<ISUP_namespace::IISUPMessage > p;
	m_systemLog->LogFinest( L"CoCreateInstance ISUP...");
	HRESULT hr = p.CoCreateInstance(L"ISUPMessageProcessor.ISUPMessage");
	if (FAILED(hr))
		return false;
	m_systemLog->LogFinest( L"ISUP created");

	MSXML2::IXMLDOMDocument2Ptr pDoc = p->ToXML(_bstr_t(SigInfo.c_str()));

	if (!pDoc)
		return false;

	pDoc->get_xml(&m_sQ931XML.GetBSTR());

	m_systemLog->LogFinest( L"CoCreateInstance q931Info...");
	//Iq931Info* pQ931Info = NULL;
	hr = m_pQ931Info.CoCreateInstance(CLSID_q931Info);
	m_systemLog->LogFinest( L"q931Info created");

	//hr = CoCreateInstance(
	//	CLSID_q931Info,
	//	0,
	//	CLSCTX_INPROC_SERVER,
	//	IID_Iq931Info,
	//	(LPVOID*)&(pQ931Info));

	if (FAILED(hr) || ! m_pQ931Info.p)
		return false;

	ICallingPartyCategoryInfo         *pCallingPartyCategoryInfo         = NULL;
	IRedirectionNumberRestrictionInfo *pRedirectionNumberRestrictionInfo = NULL;
	ICorrelationIDInfo                *pCorrelationIDInfo                = NULL;
	ISCFIDInfo                        *pSCFIDInfo                        = NULL;
	IRedirectionInformationInfo       *pRedirectionInformationInfo       = NULL;
	ICauseIndicatorsInfo              *pCauseIndicatorsInfo              = NULL;
	ICalledPartyInfo                  *pCalledPartyInfo                  = NULL;
	ICallingPartyInfo                 *pCallingPartyInfo                 = NULL;
	ILocationNumberInfo               *pLocationNumberInfo               = NULL;
	IRedirectionNumberInfo            *pRedirectionNumberInfo            = NULL;
	IUserToUserInfo                   *pUserToUserInfo                   = NULL;

	//SelectSingleNode(pDoc, L"//Parameters/CallingPartyCategory");

	hr = m_pQ931Info->get_CallingPartyCategory(&pCallingPartyCategoryInfo);
	if (SUCCEEDED(hr) && pCallingPartyCategoryInfo)
	{
		pCallingPartyCategoryInfo->put_CallingPartyCategory(
			Utils::toNumX<LONG>(SelectValue(pDoc,L"//Parameters/CallingPartyCategory",L"value").GetBSTR())
			);
	}

	hr = m_pQ931Info->get_RedirectionNumberRestriction(&pRedirectionNumberRestrictionInfo);
	if (SUCCEEDED(hr) && pRedirectionNumberRestrictionInfo)
	{
		pRedirectionNumberRestrictionInfo->put_PresentationRestricted(
			Utils::toNumX<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumberRestriction",L"value").GetBSTR())
			);
	}

	hr = m_pQ931Info->get_CorrelationID(&pCorrelationIDInfo);
	if (SUCCEEDED(hr) && pCorrelationIDInfo)
	{
		pCorrelationIDInfo->put_CorrelationID(SelectValue(pDoc,L"//Parameters/CorrelationID",L"value"));
	}

	hr = m_pQ931Info->get_SCFID(&pSCFIDInfo);
	if (SUCCEEDED(hr) && pSCFIDInfo)
	{
		pSCFIDInfo->put_SCFID(SelectValue(pDoc,L"//Parameters/SCFID",L"value"));
	}

	hr = m_pQ931Info->get_RedirectionInformation(&pRedirectionInformationInfo);
	if (SUCCEEDED(hr) && pRedirectionInformationInfo)
	{
		pRedirectionInformationInfo->put_RedirectingIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"RedirectingIndicator").GetBSTR())
			);

		pRedirectionInformationInfo->put_OriginalRedirectionReason(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"OriginalRedirectionReason").GetBSTR())
			);

		pRedirectionInformationInfo->put_RedirectionCounter(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"RedirectionCounter").GetBSTR())
			);

		pRedirectionInformationInfo->put_RedirectingReason(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"RedirectingReason").GetBSTR())
			);
	}

	hr = m_pQ931Info->get_CauseIndicators(&pCauseIndicatorsInfo);
	if (SUCCEEDED(hr) && pCauseIndicatorsInfo)
	{
		pCauseIndicatorsInfo->put_Location(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CauseIndicators",L"Location").GetBSTR())
			);

		pCauseIndicatorsInfo->put_CodingStandart(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CauseIndicators",L"CodingStandart").GetBSTR())
			);

		pCauseIndicatorsInfo->put_CauseValue(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CauseIndicators",L"CauseValue").GetBSTR())
			);

	}

	hr = m_pQ931Info->get_CalledPartyNumber(&pCalledPartyInfo);
	if (SUCCEEDED(hr) && pCalledPartyInfo)
	{
		pCalledPartyInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NatureOfAddress").GetBSTR())
			);

		pCalledPartyInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"OddEvenIndicator").GetBSTR())
			);

		pCalledPartyInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pCalledPartyInfo->put_InternalNetworkNumberIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"InternalNetworkNumberIndicator").GetBSTR())
			);

		pCalledPartyInfo->put_CalledAddressSignals(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"CalledAddressSignals").GetBSTR());

	}

	hr = m_pQ931Info->get_CallingPartyNumber(&pCallingPartyInfo);
	if (SUCCEEDED(hr) && pCallingPartyInfo)
	{
		pCallingPartyInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NatureOfAddress").GetBSTR())
			);

		pCallingPartyInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"OddEvenIndicator").GetBSTR())
			);

		pCallingPartyInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pCallingPartyInfo->put_ScreeningIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"ScreeningIndicator").GetBSTR())
			);

		pCallingPartyInfo->put_PresentationRestrictionIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"PresentationRestrInd").GetBSTR())
			);

		pCallingPartyInfo->put_CallingAddressSignals(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"CallingAddressSignals").GetBSTR());

	}

	hr = m_pQ931Info->get_LocationNumber(&pLocationNumberInfo);
	if (SUCCEEDED(hr) && pLocationNumberInfo)
	{
		pLocationNumberInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"NatureOfAddress").GetBSTR())
			);

		pLocationNumberInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"OddEvenIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_ScreeningIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"ScreeningIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_PresentationRestrictionIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"PresentationRestrictionIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_InternationalNNI(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"InternationalNNI").GetBSTR())
			);

		pLocationNumberInfo->put_LocationNumber(SelectValue(pDoc,L"//Parameters/LocationNumber",L"LocationNumber").GetBSTR());

	}

	hr = m_pQ931Info->get_RedirectionNumber(&pRedirectionNumberInfo);
	if (SUCCEEDED(hr) && pRedirectionNumberInfo)
	{
		pRedirectionNumberInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"NatureOfAddress").GetBSTR())
			);

		pRedirectionNumberInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"OddEvenIndicator").GetBSTR())
			);

		pRedirectionNumberInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pRedirectionNumberInfo->put_InternalNetworkNumberIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"InternalNetworkNumberIndicator").GetBSTR())
			);

		pRedirectionNumberInfo->put_RedirectionAddressSignals(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"RedirectionAddressSignals").GetBSTR());

	}

	hr = m_pQ931Info->get_UserToUser(&pUserToUserInfo);
 	if (SUCCEEDED(hr) && pUserToUserInfo)
 	{
 		pUserToUserInfo->put_UserToUser(SelectValue(pDoc,L"//Parameters/UserToUserInformation",L"value"));
 	}

	m_systemLog->LogFinest( L"Initializing Q931 COM protocol completed");
	return true;
}

//IVOXPtr CVXMLComInterface::LoadTelLib()
//{
//	m_systemLog->LogFinest( L"CreateInstance TELLIB...");
//	IVOXPtr pVox;
//	HRESULT hr = pVox.CreateInstance(__uuidof(VOX));
//	if (FAILED(hr))
//	{
//		throw _com_error(hr);
//	}
//	m_systemLog->LogFinest( L"TELLIB created");
//
//	return pVox;
//}

CVXMLComInterface::CVXMLComInterface()
{
	//::InitializeCriticalSection(&m_CS);
}

CVXMLComInterface::~CVXMLComInterface()
{
	//::DeleteCriticalSection(&m_CS);
}

//Iq931Info* CVXMLComInterface::GetQ931ProtocolInfo(const std::wstring&SigInfo)
//{
//	if (!m_pQ931Info)
//		m_pQ931Info = CreateQ931ProtocolInfo(SigInfo);
//	return m_pQ931Info;
//}
