// CVXMLComInterface.h : Declaration of the CCVXMLComInterface

#pragma once

#include <mutex>
#include "resource.h"       // main symbols
#include "VXMLCom_i.h"
#include "_IVXMLComInterfaceEvents_CP.h"
#include "EvtModel.h"
#include "Engine.h"
#include "../q931/q931.h"
#include "VXMLSession.h"
#include "_IVXMLComInterfaceEvents_CP.H"
#include "stdafx.h"
//#ifndef _WIN32_WINNT            // Specifies that the minimum required platform is Windows Vista.
//#define _WIN32_WINNT 0x0600     // Change this to the appropriate value to target other versions of Windows.
//#endif
#include "..\Engine\EngineLog.h"
#ifdef _EMULATE_TELSERVER
#include "TelServerEmulation.h"
#endif

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

class CVXMLEngineConfig;

// CVXMLComInterface

class ATL_NO_VTABLE CVXMLComInterface :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CVXMLComInterface, &CLSID_VXMLComInterface>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CVXMLComInterface>,
	public CProxy_IVXMLComInterfaceEvents<CVXMLComInterface>,
	public IDispatchImpl<IVXMLComInterface, &IID_IVXMLComInterface, &LIBID_VXMLComLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IEngineVM
{
private:
	VXML::SystemLogPtr m_systemLog;
	VXML::ExtraLogPtr m_extraLog;
	VXML::ExtraLogPtr m_vbsLog;
	VXML::SessionPtr2   m_pSes;
	VXML::Q931Ptr		m_pQ931Info;
	_bstr_t				m_sQ931XML;

	std::unique_ptr<CVXMLEngineConfig> mEngineConfig;


#ifdef _EMULATE_TELSERVER
	VXML::TelServPtr m_pTelserv;
#endif

private:
	class CCSLocker
	{
	private:
		int &m_CS;
	public:
		CCSLocker(int &_CS):m_CS(_CS)
		{
			++m_CS;
		}
		~CCSLocker()
		{
			--m_CS;
		}
	};
public:
	CVXMLComInterface();

	virtual ~CVXMLComInterface();

DECLARE_REGISTRY_RESOURCEID(IDR_CVXMLCOMINTERFACE)


BEGIN_COM_MAP(CVXMLComInterface)
	COM_INTERFACE_ENTRY(IVXMLComInterface)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(IEngineVM)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CVXMLComInterface)
	CONNECTION_POINT_ENTRY(__uuidof(_IVXMLComInterfaceEvents))
END_CONNECTION_POINT_MAP()
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();

	void FinalRelease();

	bool CreateQ931ProtocolInfo(const std::wstring&);

	//IVOXPtr LoadTelLib();

public:

	STDMETHOD(Destroy)(void);
	STDMETHOD(DoStep)(void);
	STDMETHOD(GetState)(void);
	STDMETHOD(SetEvent)(PMESSAGEHEADER pMsgHeader);
	STDMETHOD(GetNextDoStepInterval)(LPDWORD lpdwInterval);
	STDMETHOD(Init)(PMESSAGEHEADER pMsgHeader, IEngineCallback* pCallback);

};

OBJECT_ENTRY_AUTO(__uuidof(VXMLComInterface), CVXMLComInterface)
