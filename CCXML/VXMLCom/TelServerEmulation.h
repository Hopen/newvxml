///************************************************************************/
///* Name     : CCXMLCom\TelServerEmulation.h                             */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-IT                                                  */
///* Date     : 12 Nov 2010                                               */
///************************************************************************/
//#pragma once
//#include <algorithm>
//#include <map>
//#include <list>
//#include <boost/shared_ptr.hpp>
//#include <boost/asio.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>
//#include <boost/bind.hpp>
//#include <boost/thread.hpp>
//
//#include "sv_thread.h"
//#include "sv_handle.h"
//#include "sv_sysobj.h"
//
//#include "EvtModel.h"
//#include "..\Engine\Engine.h"
//
//class CTelServerEmulator: public IEngineCallback//, private SVThread::CThread
//{
//	typedef /*unsigned*/ __int64 ObjectId;
//
//	class CContentParam
//	{
//	public:
//		CContentParam():m_bCompleted(false){}
//		CContentParam(const CContentParam& rhs)
//		{
//			m_TermChar   = rhs.m_TermChar  ;
//			m_DigitsGot  = rhs.m_DigitsGot ;
//			m_MaxSilence = rhs.m_MaxSilence;
//			m_MaxTime    = rhs.m_MaxTime   ;
//
//			m_tSilence   = rhs.m_tSilence  ;
//			m_tWait      = rhs.m_tWait     ;
//			m_pStrand    = rhs.m_pStrand   ;
//			m_CallID     = rhs.m_CallID    ;
//			m_bCompleted = rhs.m_bCompleted;
//		}
//		CContentParam(
//			const ObjectId& _callid,
//			const std::wstring& _digit, 
//			const std::wstring& _term,
//			const int& _max_silence,
//			const int& _max_wait
//			): m_DigitsGot(_digit),m_TermChar(_term), m_MaxSilence(_max_silence), m_MaxTime(_max_wait),m_CallID(_callid),m_bCompleted(false)
//		{
//			m_pStrand = std::shared_ptr<boost::asio::strand>(new DEBUG_NEW_PLACEMENT boost::asio::strand(m_io));
//			if (m_MaxSilence)
//			{
//				m_tSilence = std::shared_ptr<boost::asio::deadline_timer>(new DEBUG_NEW_PLACEMENT boost::asio::deadline_timer(m_io, boost::posix_time::milliseconds(m_MaxSilence)));
//				m_tSilence->async_wait(m_pStrand->wrap(boost::bind(&CContentParam::OnSilentsEnd, this)));
//
//			}
//			m_thread = boost::thread(boost::bind(&boost::asio::io_service::run, &m_io));
//
//			//if (m_MaxTime)
//			//	m_tWait    = std::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(io, boost::posix_time::seconds(m_MaxTime   )));
//
//		}
//		~CContentParam()
//		{
//			m_thread.join();
//		}
//
//		void OnSilentsEnd()
//		{
//			if (m_bCompleted)
//				return;
//			ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(CTelServerEmulator::m_CS);
//			if (m_DigitsGot.empty()) // NOINPUT
//			{
//				OnEvent(L"TM_MAXSIL1");
//				return;
//			}
//			if (m_MaxTime)
//			{
//				m_tWait = std::shared_ptr<boost::asio::deadline_timer>(new DEBUG_NEW_PLACEMENT boost::asio::deadline_timer(m_io, boost::posix_time::milliseconds(m_MaxTime - m_MaxSilence)));
//				m_tWait->async_wait(m_pStrand->wrap(boost::bind(&CContentParam::OnTimeEnd, this)));
//			}
//
//		}
//
//		void OnTimeEnd()
//		{
//			if (m_bCompleted)
//				return;
//			ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(CTelServerEmulator::m_CS);
//			OnEvent(L"TM_DIGIT");
//		}
//
//		void OnEvent(WCHAR * sTerminationReason)
//		{
//			CMessage msg(L"GET_DIGITS_COMPLETED");
//			msg[L"CallbackID"]        = m_CallID;
//			msg[L"TermDigit"]         = m_TermChar;
//			msg[L"DigitsBuffer"]      = m_DigitsGot.c_str();
//			msg[L"TerminationReason"] = sTerminationReason;
//			//CTelServerEmulator::pTelServ->OnAuxMsg(msg);
//			CTelServerEmulator::PostMessage(msg);
//		}
//
//		void operator=(const CContentParam& rhs)
//		{
//			m_TermChar   = rhs.m_TermChar  ;
//			m_DigitsGot  = rhs.m_DigitsGot ;
//			m_MaxSilence = rhs.m_MaxSilence;
//			m_MaxTime    = rhs.m_MaxTime   ;
//
//			m_tSilence   = rhs.m_tSilence  ;
//			m_tWait      = rhs.m_tWait     ;
//			m_pStrand    = rhs.m_pStrand   ;
//			m_CallID     = rhs.m_CallID    ;
//			//m_bCompleted = rhs.m_bCompleted;
//		}
//		void DigitGot(CMessage& msg)
//		{
//			ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(CTelServerEmulator::m_CS);
//			//if (m_bCompleted)
//			//	return;
//			std::wstring sDigit;
//			if (CParam* pDigit = msg.ParamByName(L"Digit"))
//				sDigit = pDigit->AsWideStr();
//			if (sDigit.empty())
//				return;
//			//wchar_t last_char = sDigit[sDigit.length()-1];
//			m_DigitsGot+=sDigit;
//			if (m_TermChar.empty() || !m_TermChar.compare(sDigit))
//			{
//				m_bCompleted = true;
//				msg.SetName(L"GET_DIGITS_COMPLETED");
//				msg[L"DigitsBuffer"]		= m_DigitsGot;
//				TimersCancel();
//				OnEvent(L"TM_DIGIT");
//			}
//		}
//		void Break()
//		{
//			TimersCancel();
//		}
//	private:
//		//void Join()
//		//{
//		//	m_thread.join();
//		//}
//		void OnTimeOut()
//		{
//			// nothing todo here
//			int test = 0;
//		}
//		void TimersCancel()
//		{
//			if (m_tWait.get())
//			{
//				if (m_tWait   ->expires_from_now(boost::posix_time::microsec(1)))
//					m_tWait   ->async_wait(m_pStrand->wrap(boost::bind(&CContentParam::OnTimeOut, this)));
//				else
//				{
//					// Too late, timer has already expired!
//				}
//			}
//			if (m_tSilence.get())
//			{
//				if (m_tSilence->expires_from_now(boost::posix_time::microsec(1)) > 1)
//					m_tSilence->async_wait(m_pStrand->wrap(boost::bind(&CContentParam::OnTimeOut, this)));
//				else
//				{
//					// Too late, timer has already expired!
//				}
//			}
//		}
//
//		//std::wstring GetTermChar()const{return m_TermChar ;}
//		//std::wstring GetDigits  ()const{return m_DigitsGot;}
//
//	private:
//		std::wstring m_TermChar; // terminating char
//		std::wstring m_DigitsGot; // buffer
//		int m_MaxSilence; // between start and first DTMF
//		int m_MaxTime; // all waiting time
//		ObjectId m_CallID;
//
//		bool m_bCompleted;
//
//		boost::asio::io_service m_io;
//		std::shared_ptr<boost::asio::deadline_timer> m_tSilence;
//		std::shared_ptr<boost::asio::deadline_timer> m_tWait;
//		std::shared_ptr<boost::asio::strand> m_pStrand;
//		boost::thread m_thread;
//
//	};
//
//	//class CEngineEvent
//	//{
//	//public:
//	//	CEngineEvent(const CMessage& _msg):m_PostMsg(_msg)
//	//	{}
//	//	CMessage& GetMsg(){return m_PostMsg;};
//	//private:
//	//	CMessage m_PostMsg;
//	//};
//
//	//enum eThreadMsg
//	//{
//	//	tmEvent = WM_USER,
//	//	tmQuit = WM_QUIT
//	//};
//
//	typedef std::shared_ptr<CContentParam> IContentParam;
//	typedef std::map<__int64, IContentParam> DigitsContent;
//	//typedef std::list<CEngineEvent*> EventQueue;
//public:
//	CTelServerEmulator(IEngineCallback* _callback, IEngineVM* _engine):m_pCallBack(_callback)
//	{
//		//pTelServ = this;
//		m_pEngine = _engine;
//		//__super::Create();
//	}
//	CTelServerEmulator::~CTelServerEmulator();
//
//	void SetEvent(PMESSAGEHEADER pMsgHeader);
//
//	BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
//
//	// interface
//	virtual BOOL WINAPI GetScriptName(BSTR* pbstrScriptName){return TRUE;}
//	void OnAuxMsg(const CMessage&);
//
//protected:
//	//virtual DWORD ThreadProc(LPVOID pParams);
//
//private:
//	//void OnThreadMsg(eThreadMsg	msg, void* pParam);
//	//void ProcessEvent(CEngineEvent* pEvt);
//	//void ProcessEvents();
//	//void EmitEvent(CEngineEvent& evt);
//	//void EmitEventImmediately(CEngineEvent& evt);
//	//void PostThreadMsg(eThreadMsg msg, void* pParam = NULL);
//	static void PostMessage(const CMessage& msg);
//
//
//private:
//	IEngineCallback *    m_pCallBack;
//	static IEngineVM*    m_pEngine;
//	DigitsContent        m_DigitsBuffer;
//
//	static ATL::CComAutoCriticalSection m_CS;
//
//public:
//	//static CTelServerEmulator* pTelServ;
//
//};
//
//
///******************************* eof *************************************/