///************************************************************************/
///* Name     : CCXMLCom\TelServerEmulation.cpp                           */
///* Author   : Andrey Alekseev                                           */
///* Company  : Forte-IT                                                  */
///* Date     : 12 Nov 2010                                               */
///************************************************************************/
#include "stdafx.h"
//#include "TelServerEmulation.h"
//
//IEngineVM* CTelServerEmulator::m_pEngine(NULL);
//ATL::CComAutoCriticalSection CTelServerEmulator::m_CS;
//
////CTelServerEmulator* CTelServerEmulator::pTelServ = NULL;
//
//CTelServerEmulator::~CTelServerEmulator()
//{
//	//__super::WaitFor();
//}
//
//void CTelServerEmulator::SetEvent(PMESSAGEHEADER pMsgHeader)
//{
//	CMessage msg(pMsgHeader);
//	if (msg == L"DIGIT_GOT")
//	{
//		ObjectId CallID = msg[L"CallID"].Value.llVal;
//		DigitsContent::iterator it = m_DigitsBuffer.find(CallID);
//		if (it!=m_DigitsBuffer.end())
//		{
//			it->second->DigitGot(msg);
//		}
//	}
//}
//
//BOOL WINAPI CTelServerEmulator::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
//{
//	CMessage msg(pMsgHeader);
//	if (msg == L"PLAY_VIDEO")
//	{
//		//std::wstring sTermChar;
//		//if (CParam* pTermChar = msg.ParamByName(L"DigitMask"))
//		//	sTermChar = pTermChar->AsWideStr();
//
//		//CComVariant Value = msg[L"DigitMask"].Value;
//		ObjectId CallID = msg[L"CallID"].Value.llVal;
//		DigitsContent::iterator it = m_DigitsBuffer.find(CallID);
//		if (it==m_DigitsBuffer.end()) // only new
//			m_DigitsBuffer[msg[L"CallID"].Value.llVal] = IContentParam(new DEBUG_NEW_PLACEMENT CContentParam())/*(L"",sTermChar)*/;
//	}
//	if (msg == L"GET_DIGITS")
//	{
//		ObjectId CallID = msg[L"CallID"].Value.llVal;
//		DigitsContent::iterator it = m_DigitsBuffer.find(CallID);
//		if (it!=m_DigitsBuffer.end())
//		{
//			std::wstring sTermChar;
//			int iMaxSilence = 0, 
//				iMaxTime    = 0;
//			if (CParam* pTermChar = msg.ParamByName(L"DigitMask"))
//				sTermChar = pTermChar->AsWideStr();
//			if (CParam* pMaxSilence = msg.ParamByName(L"MaxSilence"))
//			{
//				iMaxSilence = pMaxSilence->AsInt();
//				msg.Remove(L"MaxSilence");
//			}
//			if (CParam* pMaxTime = msg.ParamByName(L"MaxTime"))
//			{
//				iMaxTime = pMaxTime->AsInt();
//				msg.Remove(L"MaxTime");
//			}
//
//			//it->second.SetTermChar(sTermChar);
//			it->second = IContentParam(new DEBUG_NEW_PLACEMENT CContentParam(/*m_io,*/CallID, L"",sTermChar,iMaxSilence,iMaxTime));
//			
//			// if we send GET_DIGITS to Telserver, we'll always get GET_DIGITS_COMPLETED, 
//			//   even if have made GET_DIGITS_COMPLETED by yourself when one of the timers had over.
//			//   Also Telserver able to translate DIGIT_GOT messages without GET_DIGITS
//			//   so, don't send it
//			//return m_pCallBack->PostAuxMessage(CPackedMsg(msg)()); // new message - without MaxSilence and MaxTime params
//			return TRUE;
//		}
//
//	}
//	if (msg == L"STOP_CHANNEL" || msg == L"STOP_VIDEO")
//	{
//		ObjectId CallID = msg[L"CallID"].Value.llVal;
//		DigitsContent::iterator it = m_DigitsBuffer.find(CallID);
//		if (it!=m_DigitsBuffer.end())
//		{
//			it->second->Break();
//			Sleep(1);
//			m_DigitsBuffer.erase(it);		
//		}
//	}
//
//	return m_pCallBack->PostAuxMessage(pMsgHeader);
//}
//
//void CTelServerEmulator::PostMessage(const CMessage& msg)
//{
//	m_pEngine->SetEvent((CPackedMsg(msg))());
//}
//
////DWORD CTelServerEmulator::ThreadProc(LPVOID pParams)
////{
////	// A message received
////	MSG msg = {0};
////
////	::CoInitialize(NULL);
////	PeekMessageW(&msg, NULL, 0, 0, PM_NOREMOVE);
////
////	m_ThreadEvt.Set();
////
////	for (;;)
////	{
////
////		__try 
////		{ 
////			// TODO: Write to the database
////			while (::PeekMessageW(&msg, NULL, 0, 0, PM_NOREMOVE))
////			{
////				int nRes = ::GetMessageW(&msg, NULL, 0, 0);
////
////				if (nRes == 0)
////				{
////					break;
////				}
////				else if (nRes < 0)
////				{
////					// Do error handling
////				}
////				else 
////				{
////					OnThreadMsg((eThreadMsg)msg.message, (void*)msg.lParam);
////					m_ThreadEvt.Reset();
////				}
////			}
////
////			if (msg.message == tmQuit)
////			{
////				break;
////			}
////		} 
////
////		__finally 
////		{ 
////			m_ThreadEvt.Reset();
////		} 
////	}
////
////
////	return 0;
////}
////
////void CTelServerEmulator::OnAuxMsg(const CMessage& _msg)
////{
////	CEngineEvent evt(_msg);
////	EmitEvent(evt);
////}
////
////void CTelServerEmulator::EmitEvent(CEngineEvent& evt)
////{
////	CEngineEvent* pEvt = new CEngineEvent(evt);
////	m_EvtQ.push_back(pEvt);
////	PostThreadMsg(tmEvent);
////}
////
////void CTelServerEmulator::EmitEventImmediately (CEngineEvent& evt)
////{
////	CEngineEvent* pEvt = new CEngineEvent(evt);
////	m_EvtQ.push_front(pEvt);
////	PostThreadMsg(tmEvent);
////}
////
////
////void CTelServerEmulator::PostThreadMsg(eThreadMsg msg, void* pParam)
////{
////	__super::PostMessage(msg, 0, (LPARAM)pParam);
////}
////
////
////void CTelServerEmulator::OnThreadMsg(eThreadMsg msg, void* pParam)
////{
////	switch (msg)
////	{
////	case tmEvent:
////		ProcessEvents();
////		break;
////	}
////}
////
////void CTelServerEmulator::ProcessEvents()
////{
////	for (;;)
////	{
////		CEngineEvent* pEvt = NULL;
////
////		{
////			ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(m_CS);
////			if (m_EvtQ.empty())
////			{
////				break;
////			}
////			EventQueue::const_iterator it = m_EvtQ.begin();
////			pEvt = *it;
////			if (it == m_EvtQ.end())
////				break;
////		}
////
////		ProcessEvent(pEvt);
////	}
////}
////
////
////void CTelServerEmulator::ProcessEvent(CEngineEvent* pEvt)
////{
////	// delete & post message
////	CMessage msg = pEvt->GetMsg();
////	__int64 iCallID = 0;
////	if (CParam* param = msg.ParamByName(L"CallbackID"))
////		iCallID = param->AsInt64();
////
////	if (iCallID)
////	{
////		// lock
////		DigitsContent::iterator it = m_DigitsBuffer.find(iCallID);
////		if (it!=m_DigitsBuffer.end())
////		{
////			if (msg == L"GET_DIGITS_COMPLETED")
////			{
////				PostMessage(msg);
////			}
////			else if (msg == L"STOP_CHANNEL" || msg == L"STOP_VIDEO")
////			{
////				m_DigitsBuffer.erase(it);
////			}
////				
////		}
////
////	}
////
////	m_EvtQ.remove(pEvt);
////	delete pEvt;
////}
//
//
//
///******************************* eof *************************************/