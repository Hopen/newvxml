// EventBridge.h : Declaration of the CNameTable

#pragma once
#include "resource.h"       // main symbols

#include "VXMLCom_i.h"
#include "VXMLNameTable.h"
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

namespace VXML
{
	// CNameTable
	class CNameTable :
		public InstanceController<CNameTable>,
		public std::enable_shared_from_this <CNameTable>
	{
	public:
		CNameTable(const std::wstring& aName);
		CNameTable(const CMessage& rhs);
		virtual ~CNameTable();

		// copy ctor
		CNameTable(const CNameTable& rhs) = delete;
		CNameTable& operator= (CNameTable const& rhs) = delete;

	private:
		struct wstringLessIC {
			bool operator()(const VXMLString& _X, const std::wstring& _Y) const
			{
				return _wcsicmp(_X.c_str(), _Y.c_str()) < 0;
			}
		};

		using NameContainer = std::map<std::wstring, VAR, wstringLessIC>;

		NameContainer	   m_Names;
		bool			   m_bActAsAssocArray;
		std::wstring	   m_name;

	private:
		const VAR* GetAddressOf(const VXMLString& sName) const;

	public:
		VAR* Find(const VXMLString& sName);
		void SetValue(const VXMLString& sName, const CComVariant& val);
		const VAR GetValue(const VXMLString& sName) const;
		const VAR GetSafeValue(const VXMLString& sName) const;
		VAR& GetUnsafeValue(const VXMLString & sName);

		//const VAR& Add(const VXMLString& sName, const CComVariant& val, bool bReadOnly);
		VAR& Add(const VXMLString& sName, const CComVariant& val, bool bReadOnly);
		const CComVariant AddRef2(const VXMLString& sName, const CComVariant& val, bool bReadOnly);
		bool Del(const VXMLString& sName);
		//const VAR* Find(const VXMLString& sName);
		//const VAR& Lookup(const VXMLString& sName);
		virtual CMessage ToMsg() const;
		//CMessage ToMsg(ExtraLogPtr aLog) const;
		void Clear();
		std::size_t GetSize() const { return m_Names.size(); }
		void Clone(const CNameTable& rhs);


		HRESULT FinalConstruct()
		{
			return S_OK;
		}

		void FinalRelease()
		{
		}

		virtual CComVariant AsVariant()
		{
			try
			{
				return CComVariant(new CComBridge<CNameTable>(shared_from_this()));
			}
			catch (...)
			{
				throw std::runtime_error("EventBridge AsVariant exception");
			}
			return CComVariant();
		}

	private:

		template <class Predicate>
		CComVariant FindByPredicate(Predicate pred) const
		{
			CComVariant result;
			const auto cit = std::find_if(m_Names.cbegin(), m_Names.cend(), pred);
			if (cit != m_Names.cend())
			{
				result = cit->second.vValue.pdispVal;
			}

			return result;
		}

		template <class Predicate>
		void ForEach(Predicate pred)
		{
			std::for_each(m_Names.cbegin(), m_Names.cend(), pred);
		}

	public:
		// IDispatch methods
		virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(
			/* [out] */ UINT *pctinfo);

		virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(
			/* [in] */ UINT iTInfo,
			/* [in] */ LCID lcid,
			/* [out] */ ITypeInfo **ppTInfo);

		STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId);
		STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr);
	};
}
