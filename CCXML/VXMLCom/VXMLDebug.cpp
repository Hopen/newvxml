/************************************************************************/
/* Name     : VXMLCom\VXMLDebug.cpp                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Dec 2011                                               */
/************************************************************************/

#include "stdafx.h"
//#include <string>
//#include "VXMLDebug.h"
//
//
//#include "EngineConfig.h"
//#include "..\Engine\EngineLog.h"
//
//using namespace enginelog;
//
//namespace VXML
//{
//
//	class CVXMLEngineConfig : public CEngineConfig<CVXMLEngineConfig>
//	{
//	public:
//		VXML::TDefaultVXMLSettings VXMLDefSet;
//		VXML::TFolders XMLFolders;
//		VXML::TSystem Systems;
//	
//	
//	protected:
//	
//		void LoadEngineConfig()
//		{
//			try
//			{ 
//				//LiteXML::TElem config   = LiteXML::LoadXML(m_sFileName.c_str()); 
//				this->XMLFolders.sVXMLFolder	= m_config.attr(L"//configuration/Folder/VXMLFolder/Value");
//				this->XMLFolders.sWavFolder		= m_config.attr(L"//configuration/Folder/WavFolder/Value");
//				this->XMLFolders.sGrammarFolder	= m_config.attr(L"//configuration/Folder/GrammarFolder/Value");
//				this->XMLFolders.sVideoFolder	= m_config.attr(L"//configuration/Folder/VideoFolder/Value");
//	
//				this->VXMLDefSet.sTimeout			= m_config.attr(L"//configuration/DefaultVXMLSettings/Timeout/Value");
//				this->VXMLDefSet.sInterdigittimeout = m_config.attr(L"//configuration/DefaultVXMLSettings/Interdigittimeout/Value");
//				this->VXMLDefSet.sWaitTime			= m_config.attr(L"//configuration/DefaultVXMLSettings/WaitTime/Value");
//				this->VXMLDefSet.sTermchar			= m_config.attr(L"//configuration/DefaultVXMLSettings/Termchar/Value");
//	
//				this->Systems.sKeepAliveTimeout      = m_config.attr(L"//configuration/System/KeepAliveTimeout/Value");
//			} 
//			catch (...) 
//			{ 
//			} 
//		}
//	
//	public:
//		CVXMLEngineConfig()
//		{
//			try
//			{
//				LoadEngineConfig();
//			}
//			catch (...)
//			{
//			}
//		}
//		~CVXMLEngineConfig()
//		{
//		}
//	};
//
//	//implementation
//	VXMLDebugBeholder::VXMLDebugBeholder(IEngineCallback* _callback, IEngineVM* _engine): 
//        IDebuger(_callback,_engine),
//		//m_nCounter(0),
//		m_ScriptoGenAddress(0),
//		//m_pSession(NULL),
//		m_debugflag(false),
//		m_forcebreak(false),
//		m_bWaitForRun(true)
//	{
//		m_evtReady.Create();
//		m_evtReady.Reset();
//		m_evtRun.Create();
//		m_evtRun.Reset();
//	};
//
//	VXMLDebugBeholder::~VXMLDebugBeholder()
//	{
//		m_evtReady.Close();
//		m_evtRun.Close();
//	}
//
//	void VXMLDebugBeholder::Add()
//	{
//		SessionPtr2 pSession = m_pSession.lock();
//		pSession->SetState(VMS_WAITING/*VMS_BUSY*/);
//		//++m_nCounter;
//		m_evtReady.Set();
//	}
//	void VXMLDebugBeholder::Release()
//	{
//		//if (m_nCounter)
//			//--m_nCounter;
//	}
//
//	void VXMLDebugBeholder::WaitOffline(unsigned int _line/*, unsigned int _count*/ /* = 1 */)
//	{
//		SessionPtr2 pSession = m_pSession.lock();
//		try
//		{
//
//		//for (unsigned int i=0;i<_count;++i)
//		//{
//			//if (m_nCounter)
//				//Release();
//			//else
//			//{
//				pSession->ChangeState/*SetState*/(VMS_READY);
//				::WaitForSingleObject((HANDLE)m_evtReady,INFINITE);
//				pSession->ChangeState/*SetState*/(VMS_WAITING/*VMS_BUSY*/);
//				m_evtReady.Reset();
//				//Release();
//			//}
//		//}
//		}
//		catch (...)
//		{
//			CEngineLog log;
//			log.Init(CVXMLEngineConfig::GetInstance()->sLevel.c_str(),CVXMLEngineConfig::GetInstance()->sLogFileName.c_str());
//			log.pLog->LogWarning(L"VXMLDebugBeholder::WaitOffline: exception !!! Line: \"%i\", SessionState: \"%i\"", _line, pSession->GetState());
//
//		}
//
//	}
//
//	void VXMLDebugBeholder::WaitOnline(unsigned int _line/*, unsigned int _count*/ /* = 1 */)
//	{
//		SessionPtr2 pSession = m_pSession.lock();
//		try
//		{
//			BreakQueue::const_iterator cit = std::find_if(m_BreakStack.begin(),m_BreakStack.end(),CBreakPointByLineFinder(_line));
//			if (( cit!=m_BreakStack.end()) /*&& 
//				(cit->condition.empty || m_pSession->m_ExecContext.pEng->ExprEvalToBool(cit->condition))*/)
//			{
//					CMessage BreakMsg(L"Break");
//					BreakMsg[L"File"] = cit->filename;
//					BreakMsg[L"Line"] = static_cast<int>(cit->line);
//
//					SendToScriptoGen(BreakMsg);
//					pSession->ChangeState/*SetState*/(VMS_WAITING);
//
//					HANDLE hEvents [2] = {(HANDLE)m_evtReady, (HANDLE)m_evtRun};
//					::WaitForMultipleObjects(2, hEvents,FALSE, INFINITE);
//					//m_evtRun.Reset();
//					m_evtReady.Reset();
//
//			}
//			else if (m_forcebreak)
//			{
//				m_forcebreak = false;
//				CMessage BreakMsg(L"Break");
//				BreakMsg[L"File"] = m_BreakStack.begin()->filename;
//				BreakMsg[L"Line"] = static_cast<int>(_line);
//
//				SendToScriptoGen(BreakMsg);
//				pSession->ChangeState/*SetState*/(VMS_WAITING);
//				HANDLE hEvents [2] = {(HANDLE)m_evtReady, (HANDLE)m_evtRun};
//				::WaitForMultipleObjects(2, hEvents,FALSE, INFINITE);
//				//m_evtRun.Reset();
//				m_evtReady.Reset();
//
//			}
//			else
//			{
//				if (m_bWaitForRun)
//				{
//					::WaitForSingleObject((HANDLE)m_evtRun,INFINITE);
//					m_bWaitForRun = false;
//				}
//				pSession->ChangeState/*SetState*/(VMS_READY);
//				//HANDLE hEvents [2] = {(HANDLE)m_evtReady, (HANDLE)m_evtRun};
//				//::WaitForMultipleObjects(2, hEvents,TRUE, INFINITE);
//				::WaitForSingleObject((HANDLE)m_evtReady,INFINITE);
//				pSession->ChangeState/*SetState*/(VMS_WAITING/*VMS_BUSY*/);
//				m_evtReady.Reset();
//			}
//			
//			//for (unsigned int i=0;i<_count;++i)
//			//{
//				//if (m_nCounter)
//					//Release();
//				//else
//				//{
//					//::WaitForSingleObject((HANDLE)m_evt,INFINITE);
//					//m_pSession->SetState(VMS_READY);
//					//m_evt.Reset();
//					//Release();
//				//}
//			//}
//		}
//		catch (...)
//		{
//			CEngineLog log;
//			log.Init(CVXMLEngineConfig::GetInstance()->sLevel.c_str(), CVXMLEngineConfig::GetInstance()->sLogFileName.c_str());
//			log.pLog->LogWarning(L"VXMLDebugBeholder::WaitOnline: exception !!! Line: \"%i\", SessionState: \"%i\"", _line, pSession->GetState());
//
//		}
//
//	}
//
//	void VXMLDebugBeholder::Wait(unsigned int _line/*, unsigned int _count*//* = 1*/)
//	{
//		m_debugflag?WaitOnline(_line/*, _count*/):WaitOffline(_line/*, _count*/);
//	}
//
//	void VXMLDebugBeholder::Increase(/*unsigned int _count*//* = 1*/)
//	{
//		//for (unsigned int i=0;i<_count;++i)
//		//{
//			Add();
//		//}
//	}
//
//	void VXMLDebugBeholder::SetAuxEvent( CMessage& msg, bool _in )
//	{
//		if (!m_ScriptoGenAddress || !m_debugflag)
//			return;
//
//		SessionPtr2 pSession = m_pSession.lock();
//		if (msg == L"Run")
//		{
//			//m_pSession->SetState(VMS_READY);
//			m_evtRun.Set();
//		}
//		else if (msg == L"Break")
//		{
//			pSession->ChangeState/*SetState*/(VMS_WAITING);
//		}
//		else if (msg == L"SetBreak")
//		{
//			TBreakPoint point;
//			if (CParam* param = msg.ParamByName(L"File"))
//				point.filename = param->AsString();
//			if (CParam* param = msg.ParamByName(L"Line"))
//				point.line = param->AsUInt();
//			if (CParam* param = msg.ParamByName(L"Condition"))
//				point.condition = param->AsString();
//
//			if (point.line)
//			{
//				BreakQueue::const_iterator cit = std::find_if(m_BreakStack.begin(),m_BreakStack.end(),CBreakPointByLineFinder(point.line));
//				if(cit == m_BreakStack.end())
//					m_BreakStack.push_back(point);
//				else
//					m_BreakStack.erase(cit);
//			}
//		}
//		else if (msg == L"StepInto")
//		{
//
//		}
//		else if (msg == L"StepOver")
//		{
//			m_forcebreak = true;
//			pSession->ChangeState/*SetState*/(VMS_READY);
//		}
//		else if (msg == L"StepOut")
//		{
//
//		}
//		else if (msg == L"GetSymInfo")
//		{
//			CMessage SymInfoMsg(L"SymInfo");
//
//			int k = 0;
//			while (true)
//			{
//				std::wstring sParamName = format_wstring(L"SymName%d", k);
//				if (!msg.IsParam(sParamName.c_str()))
//				{
//					break;
//				}
//				std::wstring sSymName = msg[sParamName.c_str()]; 
//				std::wstring sSymInfo = GetSymInfo(sSymName);
//				SymInfoMsg[sParamName.c_str()] = sSymName.c_str();
//				SymInfoMsg[format_wstring(L"Text%d", k).c_str()] = sSymInfo.c_str();
//				k++;
//			}
//
//			SendToScriptoGen(SymInfoMsg);
//		}
//		//else if (msg == L"EndDebug")
//		//{
//		//	m_pSession->DoExit(L"END_DIALOG");
//		//}
//		else
//		{
//			CMessage DbgEventMsg(_in?L"DbgEventIn":L"DbgEventOut");
//			CPackedMsg pMsg(msg);
//			SYSTEMTIME st;
//			FILETIME ft;
//			GetLocalTime(&st);
//			SystemTimeToFileTime(&st,&ft);
//			DbgEventMsg[L"TimeStamp"] = *(__int64*)&ft;
//			DbgEventMsg[L"EventData"].SetRawData(pMsg(),pMsg.Size());
//
//			SendToScriptoGen(DbgEventMsg);
//		}
//	}
//
//	void VXMLDebugBeholder::Init( CMessage& init_msg, SessionPtr _session )
//	{
//		if (CParam * param = init_msg.ParamByName(L"ScrGenAddr"))
//			m_ScriptoGenAddress = param->AsInt64();
//
//		if (CParam* param = init_msg.ParamByName(L"DebugFlag"))
//			m_debugflag = param->AsInt();
//
//		m_pSession = _session;
//	}
//
//	void VXMLDebugBeholder::Log( VXMLString _log )
//	{
//		if (!m_ScriptoGenAddress || !m_debugflag)
//			return;
//
//		CMessage debug_msg(L"DbgLog");
//
//		debug_msg[ L"Text" ] = _log.c_str();
//		SendToScriptoGen(debug_msg);
//	}
//
//	void VXMLDebugBeholder::SendToScriptoGen(CMessage& msg)const
//	{
//		try
//		{
//			msg[ L"AuxEvent"           ] = 1;
//			msg[ L"DestinationAddress" ] = m_ScriptoGenAddress;
//
//			//m_pSession->SendAuxMsg(msg);
//			m_pCallBack->PostAuxMessage(CPackedMsg(msg)());
//
//		}
//		catch (...)
//		{
//			CEngineLog log;
//			log.Init(CVXMLEngineConfig::GetInstance()->sLevel.c_str(), CVXMLEngineConfig::GetInstance()->sLogFileName.c_str());
//			log.pLog->LogWarning(L"VXMLDebugBeholder::SendToScriptoGen: exception !!! m_pCallBack = \"%I64i\"", m_pCallBack? m_pCallBack: NULL);
//		}
//
//	}
//
//
//	BOOL VXMLDebugBeholder::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
//	{
//		try
//		{
//
//			CMessage msg(pMsgHeader);
//			if (msg != L"DbgEventIn" && msg != L"DbgEventOut")
//				SetAuxEvent(CMessage(pMsgHeader),false);
//
//			if (msg == L"END_SCRIPT")
//				EndScript(L"END_SCRIPT incoming", 0);
//
//
//			return m_pCallBack->PostAuxMessage(pMsgHeader);
//		}
//		catch (...)
//		{
//			CEngineLog log;
//			log.Init(CVXMLEngineConfig::GetInstance()->sLevel.c_str(), CVXMLEngineConfig::GetInstance()->sLogFileName.c_str());
//			log.pLog->LogWarning(L"VXMLDebugBeholder::PostAuxMessage: exception !!! m_pCallBack = \"%I64i\"", m_pCallBack? m_pCallBack: NULL);
//		}
//
//		return FALSE;
//	}
//
//	std::wstring VXMLDebugBeholder::GetSymInfo(const std::wstring& _name)const
//	{
//		SessionPtr2 pSession = m_pSession.lock();
//		if (pSession->GetState()!=VMS_READY)
//			return PROCESSNOTAVAILABLE_DBGMSG;
//		return pSession->GetVarValue(_name);
//	}
//
//	void VXMLDebugBeholder::EndScript(const VXMLString& _reason, const int& _code)const
//	{
//		if (!m_debugflag) // HotFix 
//			return;
//
//		CMessage msg(L"EndDebug");
//		msg[L"Code"] = _code;
//		msg[L"Reason"] = _reason.c_str();
//		SendToScriptoGen(msg);
//	}
//
//}
/******************************* eof *************************************/