#pragma once

#include <string>
#include <codecvt>
#include <locale.h>

#include <boost/lexical_cast.hpp>

std::wstring stow( const std::string& in, std::locale loc  = std::locale() );
std::string wtos( const std::wstring& in, std::locale loc  = std::locale() );

inline std::wstring from_utf8(const std::string& _val)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
	return conv.from_bytes(_val);
}

inline std::string to_utf8(const std::wstring& _val)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
	return conv.to_bytes(_val);
}

namespace boost
{
	template<>
	inline std::wstring lexical_cast<std::wstring, std::string>(const std::string& _value)
	{
		return stow(_value);
	}

	template<>
	inline std::string lexical_cast<std::string, std::wstring>(const std::wstring& _value)
	{
		return wtos(_value);
	}
}

