/************************************************************************/
/* Name     : CacheInterface\DocParser.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 Nov 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "DocParser.h"
//#include "CacheInterface_i.h"
//#include "CacheInterface.h"
#include "DocLoader.h"
#include "Log\SystemLog.h"

//using namespace enginelog;

CDocParser::CDocParser(CSystemLog* aLog)
	: m_contentHandler(new DEBUG_NEW_PLACEMENT CSAXContentHandler())
	, m_errorHandler(new DEBUG_NEW_PLACEMENT CSAXErrorHandler())
	, m_pLog(aLog)
{
	m_xmlParser .reset(new DEBUG_NEW_PLACEMENT CSAXXmlParser());
	m_doc = Factory_c::CreateCollector();
	m_doc->SetName(L"VXML_base");
	m_current = m_doc;
}

CDocParser::~CDocParser()
{
}

int CDocParser::Parse2(const std::wstring& _xml, const std::wstring& _schema, bool bFile)
{
	int b = 0; // OK
	__try
	{
		//m_documentName = _xml;
		m_doc->SetUri(_xml);
		b = Parse(_xml, _schema, bFile) ? 0 : 1;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		b = -1;
	}

	return b;
	
}

bool CDocParser::Parse(const std::wstring& _xml, const std::wstring& _schema, bool bFile)
{
	m_pLog->LogString(LEVEL_FINEST, L"schema = \"%s\"", _schema.c_str());

	if (!_schema.empty())
	{
	m_xmlParser->SetParserFeature(L"schema-validation", true);
	m_xmlParser->SetParserFeature(L"use-schema-location", false);

		// decide, which namespace we should use
		int pos = -1;
		std::wstring _file(_schema),schema_namespace;
		if ((pos = _file.rfind('\\')) != -1)
		{
			_file.erase(0,pos+1);
		}
		if ((pos = _file.rfind('.')) != -1)
		{
			WCHAR file_name[MAX_PATH];ZeroMemory(file_name,sizeof(file_name));
			_file._Copy_s(file_name,MAX_PATH,pos);
			schema_namespace=std::wstring(L"x-schema:")+file_name;

		}
		m_pLog->LogString(LEVEL_WARNING,  L"schema_namespace = \"%s\"", schema_namespace.c_str());

		m_xmlParser->AddValidationSchema(schema_namespace, _schema);
	}

	// Set the content handler.
	if ( m_xmlParser->PutContentHandler(m_contentHandler) )
	{
		m_contentHandler->AttachElementHandler(this);
	}

	if ( m_xmlParser->PutErrorHandler(m_errorHandler) )
	{
		m_errorHandler->AttachElementHandler(this);
	}

	if (bFile)
		return m_xmlParser->ParseUrl(_xml);

	return m_xmlParser->Parse(_xml);

}

void CDocParser::OnXmlStartElement(const ISATXMLElement& xmlElement)
{
	auto newTag = Factory_c::CreateCollector(xmlElement);
	newTag->SetUri(m_current->GetUri());

	m_current = m_current->Add(newTag);

	auto parent = m_current->GetParent().lock();
	if (parent)
	{
		std::wstring sParentXML = parent->GetXML();
		sParentXML += L"<" + xmlElement.GetName() + L">";
		m_current->GetParent().lock()->SetXML(sParentXML);
	}
}
//
void CDocParser::OnXmlElementData(const wchar_t* aData, size_t aSize)
{
	ISATXMLElement xmlElement(L"metatext",
		std::wstring(aData, aData + aSize),
		m_current->GetLine(),
		m_current->GetUri());

	//ISATXMLElement xmlElement;
	//xmlElement.SetName(L"metatext");
	//xmlElement.SetLine(m_current->GetLine());
	////xmlElement.SetText(std::wstring(aData, aData + aSize));
	//xmlElement.AppendText(aData, aSize);
	//xmlElement.SetUri(m_current->GetUri());

	//Factory_c::CFTag t(xmlElement);
	//Factory_c::CCollector newtag(xmlElement);

	auto newTag = Factory_c::CreateCollector(xmlElement);
	m_current->Add(newTag);

	//std::wstring sXML = m_current->GetXML();
	//m_current->SetXML(sXML + elementData + L"\n");
	m_current->AppendXML(aData, aSize);
}
//
//void CDocParser::OnXmlEndElement(const ISATXMLElement& /*xmlElement*/)
//{
//	m_current = m_current->GetParent().lock();
//}


//void CDocParser::OnXmlStartElement(const ISATXMLElement& xmlElement)
//{
//	auto newTag = Factory_c::CreateCollector(xmlElement);
//	m_current = m_current->Add(newTag);
//}

//void CDocParser::OnXmlElementData(const wchar_t* aData, size_t aSize)
//{
//	m_current->AppendText(aData, aSize);
//	m_current->AppendXML(aData, aSize);
//}

void CDocParser::OnXmlEndElement(const ISATXMLElement& xmlElement)
{
	m_current = m_current->GetParent().lock();
}
void CDocParser::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
{
	m_pLog->LogString(LEVEL_WARNING, L"Error parsing XML document. Line %d, char %d, reason \"%s\"",
		line, column, errorText.c_str()); 
}

bool CDocParser::OnXmlAbortParse(const ISATXMLElement& xmlElement)
{
	return false;
}

const Factory_c::CollectorPtr& CDocParser::Get() const
{
	return m_current->GetChildren().front();
}

/******************************* eof *************************************/