// CacheInterface.cpp : Implementation of WinMain

#include <stdio.h>
#include "stdafx.h"
#include "resource.h"
#include "CacheInterface_i.h"
#include "CacheInterface.h"
#include "ResourceCache.h"
#include "mdump.h"
//#include "Session.h"
//#include "..\Common\Utils.h"
//#include "sv_helpclasses.h"
//#include "ce_xml.hpp"
//#include "sv_utils.h"
//#include <boost\archive\xml_woarchive.hpp>
//#include <boost\archive\binary_oarchive.hpp>
//#include <boost\archive\binary_iarchive.hpp>

#include "DocParser.h"
//#include "EngineConfig.h"
#include <DbgHelp.h>
#include <ATLComTime.h>

#include "DocLoader.h"
#include "Log\SystemLog.h"
//using namespace enginelog;

/************************************************************************/
/*                         DUMPER                                       */
/************************************************************************/

//LONG WINAPI TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
//{
//	CEngineLog log;
//	log.Init(L"C:\\IS3\\Logs\\vxml_dump_%y-%m-%d.log");
//	log.Log( L"Minidump creation started");
//	CString strDumpName = _T("c:\\is3\\CCXML") + COleDateTime::GetCurrentTime().Format(_T("_%Y-%m-%d_%H-%M-%S")) + _T(".dmp");
//	LONG retval = EXCEPTION_CONTINUE_SEARCH;
//
//	HANDLE hFile = ::CreateFile( strDumpName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
//
//	if (hFile!=INVALID_HANDLE_VALUE)
//	{
//		log.Log( L"Minidump file opened");
//		_MINIDUMP_EXCEPTION_INFORMATION ExInfo;
//
//		ExInfo.ThreadId = ::GetCurrentThreadId();
//		ExInfo.ExceptionPointers = pExceptionInfo;
//		ExInfo.ClientPointers = NULL;
//
//		// write the dump
//		BOOL bOK = ::MiniDumpWriteDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );
//		if (bOK)
//		{
//			log.Log( L"Minidump write done");
//			retval = EXCEPTION_EXECUTE_HANDLER;
//		}
//		else
//			log.Log( L"Minidump write failed");
//
//		::CloseHandle(hFile);
//	}
//	else
//		log.Log( L"INVALID_HANDLE_VALUE");
//	return retval;
//}


//using namespace MSXML2;
/************************************************************************/
/*                         GLOBALS                                      */
/************************************************************************/
HINSTANCE g_hInst;

/************************************************************************/
/*                         STATIC                                       */
/************************************************************************/
//ATL::CComAutoCriticalSection CCacheInterfaceModule::m_CS2;
//
//CCacheInterfaceModule::DocumentCollector CCacheInterfaceModule::m_stack;
//CSession* CCacheInterfaceModule::m_pSes(NULL);
////CEngineLog*  CCacheInterfaceModule::m_pLog(NULL);


//static HRESULT Load(const std::wstring &sResource,  const std::wstring &sSchemaFile, BOOL bFile, MSXML2::IXMLDOMNodePtr& _doc)
//{
//	MSXML2::IXMLDOMDocument2Ptr xmlDoc;
//	MSXML2::IXMLDOMSchemaCollection2Ptr xmlSch;
//
//	VARIANT_BOOL isSuccessful = VARIANT_FALSE;
//	HRESULT hr = E_FAIL;
//
//	//CCacheInterfaceModule::m_pLog->LogString( L"XML CoCreateInstance(__uuidof(MSXML2::DOMDocument60))");
//	if (FAILED(hr = xmlDoc.CreateInstance(__uuidof(MSXML2::DOMDocument60))))
//	{
//		CCacheInterfaceModule::m_pLog->LogString( L"CoCreateInstance(__uuidof(MSXML2::DOMDocument60)) failed (0x%x)", hr);
//		return hr;
//	}
//
//	//CCacheInterfaceModule::m_pLog->LogString( L"load XML document");
//	isSuccessful = bFile ? 
//		xmlDoc->load(_bstr_t(sResource.c_str())) : 
//	xmlDoc->loadXML(_bstr_t(sResource.c_str()));
//
//	if (isSuccessful != VARIANT_TRUE)
//	{
//		MSXML2::IXMLDOMParseErrorPtr err = xmlDoc->parseError;
//
//		CCacheInterfaceModule::m_pLog->LogString( L"Cannot load XML document. Line %d, char %d, reason \"%s\"", 
//			err->line, err->linepos, (LPCWSTR)err->reason);
//		return E_FAIL;
//	}
//
//	if (!sSchemaFile.empty())
//	{
//		// decide, which namespace we should use
//		int pos = -1;
//		std::wstring _file(sSchemaFile),schema_namespace;
//		if ((pos = _file.rfind('\\')) != -1)
//		{
//			_file.erase(0,pos+1);
//		}
//		if ((pos = _file.rfind('.')) != -1)
//		{
//			WCHAR file_name[MAX_PATH];ZeroMemory(file_name,sizeof(file_name));
//			_file._Copy_s(file_name,MAX_PATH,pos);
//			schema_namespace=std::wstring(L"x-schema:")+file_name;
//
//		}
//		// xsd
//		MSXML2::IXMLDOMDocument2Ptr pXSDDoc;
//		//CCacheInterfaceModule::m_pLog->LogString( L"XSD CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60))");
//		if (FAILED(hr = pXSDDoc.CreateInstance(__uuidof(MSXML2::DOMDocument60)))) 
//		{
//			CCacheInterfaceModule::m_pLog->LogString( L"XSD CoCreateInstance(__uuidof(MSXML2::DOMDocument60)) failed (0x%x)", hr);
//			return hr;
//		}
//		pXSDDoc->async = false; 
//		pXSDDoc->validateOnParse = true;  
//		//CCacheInterfaceModule::m_pLog->LogString( L"load XSD document");
//		if(pXSDDoc->load(_variant_t(sSchemaFile.c_str()))!=VARIANT_TRUE)
//		{
//			MSXML2::IXMLDOMParseErrorPtr err = xmlDoc->parseError;
//
//			CCacheInterfaceModule::m_pLog->LogString( L"Cannot load XSD document. Line %d, char %d, reason \"%s\"", 
//				err->line, err->linepos, (LPCWSTR)err->reason);
//		}
//
//
//		// schema
//		//CCacheInterfaceModule::m_pLog->LogString( L"Schema CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60))");
//		if (FAILED(hr = xmlSch.CreateInstance(__uuidof(MSXML2::XMLSchemaCache60))))
//		{
//			CCacheInterfaceModule::m_pLog->LogString( L"CoCreateInstance(__uuidof(MSXML2::XMLSchemaCache60)) failed (0x%x)", hr);
//			return hr;
//		}
//
//		// 
//		//CCacheInterfaceModule::m_pLog->LogString( L"get schema interface");
//		xmlDoc->schemas = xmlSch.GetInterfacePtr();
//
//		//CCacheInterfaceModule::m_pLog->LogString( L"add xsd to schema");
//
//		//hr = xmlSch->add(_bstr_t(L"x-schema:ccxml"),  pXSDDoc.GetInterfacePtr());
//		hr = xmlSch->add(schema_namespace.c_str(),  pXSDDoc.GetInterfacePtr());
//
//
//
//		//CCacheInterfaceModule::m_pLog->LogString( L"get interface");
//		//CCacheInterfaceModule::m_pLog->LogString( L"Validating DOM");
//
//		MSXML2::IXMLDOMParseError2Ptr pError =xmlDoc->validate();
//		if (pError->errorCode != 0) 
//		{
//			//MSXML2::IXMLDOMParseErrorPtr err = xmlDoc->parseError;
//
//			CCacheInterfaceModule::m_pLog->LogString( L"Cannot load XML document. Line %d, char %d, reason \"%s\"", 
//				pError->line, pError->linepos, (LPCWSTR)pError->reason);
//			return hr;
//		}
//
//	}
//
//	_doc = xmlDoc->documentElement;
//	return hr;
//}

/************************************************************************/
/*                   INTERFACE IMPLAMENTATION                           */
/************************************************************************/

//HRESULT CCacheInterfaceModule::PreMessageLoop (int nShowCmd) throw()
//{
//	return CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >::PreMessageLoop (nShowCmd);
//}
//void CCacheInterfaceModule::RunMessageLoop () throw()
//{
//	CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >::RunMessageLoop ();
//}
//void CCacheInterfaceModule::OnContinue () throw()
//{
//	CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >::OnContinue ();
//}
//void CCacheInterfaceModule::OnPause () throw()
//{
//	CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >::OnPause ();
//}
//void CCacheInterfaceModule::OnShutdown () throw()
//{
//	OnStop ();
//}
//void CCacheInterfaceModule::OnStop () throw()
//{
//	CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >::OnStop ();
//}



HRESULT CCacheInterfaceModule::Run(int nShowCmd /* = SW_HIDE */)throw()
{
	//::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	HRESULT hr = S_OK;

	if (1)
	{
		CCacheInterfaceModule* pT = static_cast<CCacheInterfaceModule*>(this);

		hr = pT->PreMessageLoop(nShowCmd);

		if (hr == S_OK)
		{
			singleton_auto_pointer<MiniDumper> dumper;
			singleton_auto_pointer<CDocLoader> loader;
			singleton_auto_pointer<CSystemLog> log;

			if (1)
			{
				//::Sleep(10000);
				log->LogString(LEVEL_INFO, L"VXMLCacheService started...");
				loader->Init(log->GetInstance());

				SetServiceStatus(SERVICE_RUNNING);
				pT->RunMessageLoop();
				log->LogString(LEVEL_INFO, L"VXMLCacheService cleaning...");
				loader->Clear();
			}
		}
		//_CrtDumpMemoryLeaks();

		if (SUCCEEDED(hr))
		{
			hr = pT->PostMessageLoop();
		}
	}

	//::CoUninitialize();
	return hr;
}

//HRESULT CCacheInterfaceModule::GetDocument(BSTR sURI, BSTR sSchemaFile, BOOL bFile, SAFEARRAY **pSA)
//{
//	CEngineLog			log;
//	log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//	try
//	{
//		if(!pSA)
//			return E_POINTER;
//
//
//		/*m_pLog->*/log.Log( L"Parse document: %s, Schema: %s, File: %s", sURI, sSchemaFile, bFile?L"Yes":L"No");
//
//		CHAR *pDOC = NULL;
//		int doc_size = 0;
//
//		if (true)
//		{
//			ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(m_CS2);
//
//			DocumentCollector::iterator it = m_stack.find(sURI);
//
//			if (it!=m_stack.end())
//			{
//				//m_pSes->OnAuxMsg2(sURI,sSchemaFile,bFile,AddDoc2Stack);
//				if (!it->second->pDoc)
//					return E_FAIL;
//
//				pDOC     = it->second->pDoc;
//				doc_size = it->second->doc_size;
//				/*m_pLog->*/log.Log( L"Load document from cache, file_size: \"%i\"", doc_size);
//			}
//			else
//			{
//				// load new document
//				WIN32_FIND_DATA fileData;
//
//				if (bFile && Utils::GetFile(sURI,&fileData))
//				{
//					/*m_pLog->*/log.Log( L"Can't get file attributes, check file location: %s", sURI);
//					return E_FAIL;
//				}
//
//				LoadAndSerilizeDocument(sURI,sSchemaFile,bFile,&pDOC,doc_size);
//			}
//		}
//
//
//
//		if (!pDOC || !doc_size)
//		{
//			//	if (!AddDoc2Stack(sURI,sSchemaFile,bFile,&pDOC,doc_size))
//			return E_FAIL;
//
//		}
//		m_pSes->OnAuxMsg2(sURI,sSchemaFile,bFile/*,&CCacheInterfaceModule::AddDoc2Stack*/);
//		// wrap archive to SAFEARRAY 
//		HRESULT hr = S_OK;
//		*pSA = ::SafeArrayCreateVector(VT_I2, 0, doc_size); 
//		if( *pSA != NULL ) 
//		{ 
//			BYTE* psadata = NULL; 
//			hr = ::SafeArrayAccessData(*pSA, (void**)&psadata); 
//			if( SUCCEEDED(hr) ) 
//			{ 
//				memcpy(psadata, pDOC, doc_size); 
//				hr = ::SafeArrayUnaccessData(*pSA); 
//			} 
//		} 
//		return S_OK;
//	}
//	catch(...)
//	{
//		/*m_pLog->*/log.Log( L"Exception at GetDocument, file location: %s", sURI);
//	}
//
//
//}
//
//
//
//void CCacheInterfaceModule::AddDoc2Stack(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile, char** pDoc, int &doc_size)
//{
//	CEngineLog			log;
//	log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//	try
//	{
//		if (!pDoc)
//			return;
//
//		WIN32_FIND_DATA fileData;
//
//		if (bFile && Utils::GetFile(_uri,&fileData))
//		{
//			/*m_pLog->*/log.Log( L"Can't get file attributes, check file location: %s", _uri.c_str());
//			return;
//		}
//		DocTime tNewFileTime = fileData.ftLastWriteTime;
//
//		ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(m_CS2);
//		DocumentCollector::const_iterator cit = m_stack.find(_uri);
//		if (bFile && cit != m_stack.end() &&
//			tNewFileTime == cit->second->tLastWrite)
//			return;
//
//		if (!LoadAndSerilizeDocument(_uri,_schema,bFile,pDoc,doc_size) || !(*pDoc) || !doc_size)
//			return;
//
//		// clear old document
//		if (bFile && cit != m_stack.end())
//		{
//			TDocParam* doc = (TDocParam*)(cit->second);
//			delete doc; doc = 0;
//			//delete cit->second;
//			//m_stack.erase(cit);
//			CCacheInterfaceModule::DocumentCollector tempStack;
//			std::remove_copy_if(m_stack.begin(),m_stack.end(), std::inserter(tempStack, tempStack.end()), CIsStringToRemove(_uri));
//			m_stack.swap(tempStack);
//		}
//
//		// add new
//		m_stack[_uri] = new TDocParam(tNewFileTime, *pDoc, doc_size);
//	}
//	catch (...)
//	{
//		/*m_pLog->*/log.Log( L"Exception at AddDoc2Stack, file location: %s", _uri.c_str());
//	}
//	
//}
//
//bool CCacheInterfaceModule::LoadAndSerilizeDocument(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile, char** pDoc, int &doc_size)
//{
//	try
//	{
//		bool bLoadSuccessful = false;
//		if (true)
//		{
//			CDocParser doc;
//			if(bLoadSuccessful = doc.Parse(_uri,_schema,bFile))
//			{
//				std::ostringstream os;
//				{
//					boost::archive::binary_oarchive oa(os);
//					const Factory_c::CCollector &rDoc = doc.Get();
//					oa << rDoc;
//					// the stream completed in binary_oarchive destructor calling
//				}
//				std::string  outbound_data_ = os.str();
//				doc_size = outbound_data_.size();
//
//				*pDoc = new CHAR[doc_size];
//				ZeroMemory(*pDoc,doc_size);
//				outbound_data_._Copy_s(*pDoc,doc_size,doc_size);
//			}
//		}
//
//		CEngineLog			log;
//		log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//		if(bLoadSuccessful)
//			log.Log( L"serialization complete, file_size: \"%i\"", doc_size);
//		else
//			log.Log( L"Failed to load and serialize \"%s\"",_uri.c_str());
//
//	}
//	catch (std::exception& e)
//	{
//		CEngineLog			log;
//		log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//		std::wstring reason = _bstr_t(e.what());
//		/*m_pLog->*/log.Log( L"Exception load document: %s", reason.c_str());
//		return false;
//	}
//	catch (std::wstring& e)
//	{
//		CEngineLog			log;
//		log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//		std::wstring reason = e;
//		/*m_pLog->*/log.Log( L"Exception load document: %s", reason.c_str());
//		return false;
//	}
//	catch (_com_error& e)
//	{
//		CEngineLog			log;
//		log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//		std::wstring reason = (e.Error() != E_POINTER)?(LPCWSTR)e.Description():L"unknown";
//		/*m_pLog->*/log.Log( L"Exception load document: %s", reason.c_str());
//		return false;
//	}
//	catch (...)
//	{
//		CEngineLog			log;
//		log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//
//		std::wstring reason = L"Unknown unhandled exception";
//		/*m_pLog->*/log.Log( L"Exception load document: %s", reason.c_str());
//		return false;
//	}
//
//	return true;
//}

/************************************************************************/
/* This function is for testing only                                    */
/************************************************************************/
const int MAX_THREADS = 100;
const int MAX_ITERATIONS = 200;
const int TIME_BETWEEN_ITERATIONS = 500;
const int TIME_BETWEEN_THREADS = 1000;


//void CCacheInterfaceModule::initTests()
//{
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_CDU.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_CHL.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_CHL_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_CHL_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_CHL_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_CHL_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_EKT.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_EKT_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_EKT_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_EKT_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_EKT_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_HMS.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_HMS_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_HMS_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_HMS_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_HMS_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_IGK.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_IGK_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_IGK_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_IGK_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_IGK_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KIR.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KIR_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KIR_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KIR_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KIR_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KRG.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KRG_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KRG_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KRG_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_KRG_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_MGN.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_MGN_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_MGN_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_nonumber.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_Poll_003.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_Poll_004.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_potential.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_prepaid_suspend.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_PRM.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_PRM_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_PRM_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_PRM_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_PRM_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_SME.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_STK.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_STK_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_TUM.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_TUM_corporate.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_TUM_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_TUM_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_TUM_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_uslugi.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_WELCOME.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_YAM.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_YAM_credit.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_YAM_hlc.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\0611_YAM_prepaid.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\AVS.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\disconnect.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\ecomm.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\EWT.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\IVRSS.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\MAIN.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\MR.vxml");
//	m_test_files.push_back(L"D:\\Projects\\EK-VXML\\vxml\\ural_EKT\\operator.vxml");
//
//	srand(GetTickCount());
//
//	for (unsigned int i=0;i<MAX_THREADS;++i)
//	{
//		ThreadPtr new_thread;
//		new_thread.reset(
//			new boost::thread(boost::bind(&CCacheInterfaceModule::ThreadProc, this, m_threads.size()))
//			);
//		m_threads.push_back(new_thread);
//
//		Sleep(TIME_BETWEEN_THREADS);
//	}
//	
//
//}
//
//void CCacheInterfaceModule::ThreadProc(int num_proc)
//{
//	CEngineLog			log;
//	log.Init(CCacheEngineConfig::Instance().sLogFileName.c_str());
//	log.Log( L"ThreadProc%i started...",num_proc);
//
//
//	
//
//	singleton_auto_pointer<CDocLoader>			loader;
//	std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";
//
//	for (int i = 0;i<MAX_ITERATIONS;++i)
//	{
//		SAFEARRAY* pSA = NULL;
//		int file_num = rand() % m_test_files.size();
//		loader->GetDocument(_bstr_t(m_test_files[file_num].c_str()),_bstr_t(sSchemaFile.c_str()),true,&pSA);
//
//		::SafeArrayUnaccessData(pSA);
//		::SafeArrayDestroy(pSA);
//
//		Sleep(TIME_BETWEEN_ITERATIONS);
//	}
//
//}
//


//const std::wstring mrcp_answer = /*L"Completion - Cause:000 success " \
//L"Waveform - URL : http ://web.media.com/session123/audio.wav " \
//L"Content - Type : application / x - nlsml " \
//L"Content - Length : 276 " \*/
//L"<?xml version = \"1.0\"?> \r\n" \
//L"<result x-model = \"http://IdentityModel\" " \
//L"xmlns:xf = \"http://www.w3.org/2000/xforms\" " \
//		L"grammar = \"session:request1@form-level.store\"> "\
//		L"<interpretation> "\
//		L"<xf:instance name = \"Person\"> "\
//		L"<Person> "\
//		L"<Name> Andre Roy </Name> "\
//		L"</Person> "\
//		L"</xf:instance> "\
//		L"<input>   may I speak to Andre Roy </input> "\
//		L"</interpretation> "\
//		L"</result>";

const std::wstring mrcp_answer = /*L"Completion - Cause:000 success " \
								 L"Waveform - URL : http ://web.media.com/session123/audio.wav " \
								 L"Content - Type : application / x - nlsml " \
								 L"Content - Length : 276 " \*/
								 L"<?xml version = \"1.0\"?> \r\n" \
								 L"<result> "\
								 L"<interpretation> "\
								 L"<instance name = \"Person\"> "\
								 L"<Person> "\
								 L"<Name> Andre Roy </Name> "\
								 L"</Person> "\
								 L"</instance> "\
								 L"<input confidence=\"66\">   may I speak to Andre Roy </input> "\
								 L"</interpretation> "\
								 L"</result>";


struct foo
{
	int a = 1;
	int b = 2;

	foo() = default;
	virtual ~foo() = default;

public:
	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			a,
			b
			);
	}
};

struct bar : public foo//, public std::enable_shared_from_this<bar>
{
	int c = 3;

public:

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			//cereal::base_class<foo>(this),
			c
			);
	}
};

CEREAL_REGISTER_TYPE(bar);
CEREAL_REGISTER_POLYMORPHIC_RELATION(foo, bar);

void SerilizationTest()
{
	bar b;
	b.a = 22;
	std::ostringstream os;
	{
		cereal::BinaryOutputArchive oa(os);
		oa(b);
	}
	std::string  outbound_data_ = os.str();
	std::istringstream is(outbound_data_);

	bar b1;
	{
		cereal::BinaryInputArchive iarchive(is);
		iarchive(b1);
	}
	int test = 0;


	//auto parent = Factory_c::CreateCollector();
	//parent->Add(Factory_c::CreateCollector());
	//parent->Add(Factory_c::CreateCollector());

	//std::ostringstream os;
	//{
	//	cereal::BinaryOutputArchive oa(os);
	//	oa(parent);
	//}
	//std::string  outbound_data_ = os.str();

	//std::istringstream is(outbound_data_);

	//Factory_c::CollectorPtr collector;
	//{
	//	cereal::BinaryInputArchive iarchive(is);

	//	iarchive(collector);
	//}

	//if (collector)
	//{
	//	const auto& children = collector->GetChildren();
	//	for(const auto& child : children)
	//	{
	//		child->GetLine();
	//	}
	//}

}
void CCacheInterfaceModule::SendTestEvent()
{
	singleton_auto_pointer<CDocLoader> loader;
	singleton_auto_pointer<CSystemLog> log;

	loader->Init(log->GetInstance());

	std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"ccxml.xsd";
	//std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";

	SAFEARRAY* pSA = NULL;
	//GetDocument(L"D:\\Projects\\vxml_test\\vmlconf.vxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);

	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test30_b.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"C:\\is3\\scripts\\vxml_test\\moscow_VIP\\Includes\\Utils_msk.vxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	loader->GetDocument(L"C:\\is3\\scripts\\ccxml_test\\test_s.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);

	/*for (int i = 0; i < 5000; ++i)
	{
		loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test12.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
		::SafeArrayDestroy(pSA);

		loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test13.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
		::SafeArrayDestroy(pSA);

		loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test14.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
		::SafeArrayDestroy(pSA);

		loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test15.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
		::SafeArrayDestroy(pSA);

		loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test16.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
		::SafeArrayDestroy(pSA);

		::Sleep(10);
	}*/

	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);
	//loader->GetDocument(L"D:\\Projects\\ccxml_test\\test1.ccxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);

	//::SafeArrayUnaccessData(pSA);
	//::SafeArrayDestroy(pSA);

	Sleep(5000);

	loader->Clear();
}

CCacheInterfaceModule _AtlModule;

LPCTSTR c_lpszModuleName = _T("VXMLCacheService.exe");

BEGIN_OBJECT_MAP(ObjectMap)
	OBJECT_ENTRY(CLSID_ResourceCache, CResourceCache)
END_OBJECT_MAP()


//
extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, 
                                LPTSTR lpCmdLine, int nShowCmd)
{
	//SerilizationTest();

	//::CoInitialize(NULL);
	::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	g_hInst = hInstance;
	int retval = 0;

	retval = _AtlModule.WinMain(nShowCmd);
	//_AtlModule.SendTestEvent();
	//_CrtDumpMemoryLeaks();
	::CoUninitialize();
	return retval;

}

