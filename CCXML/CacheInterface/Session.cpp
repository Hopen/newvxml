/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "StdAfx.h"
#include "Log/SystemLog.h"
#include "Session.h"
#include "DocLoader.h"


/************************************************************************/
/*						CSession implamentation                          */
/************************************************************************/
CSession::CSession(CDocLoader *aDocLoader,
	CSystemLog* aLog)
	: m_bThreadStarted(false)
	, m_EventDispatcher(std::make_unique<common::CEventDispatcher<EventObject, EmptyBaseLockGuard>>())
	, m_DocLoader(aDocLoader)
	, m_pLog(aLog)
{
}

CSession::~CSession()
{
	JoinThread();
}

void CSession::Start()
{
	if (m_bThreadStarted)
	{
		throw std::runtime_error("Thread has already started");
	}
	m_ThreadEvt.Create();
	StartThread();
	m_ThreadEvt.Wait();
}


void CSession::PostThreadMsg(eThreadMsg msg, void* pParam)
{
	m_queue.push(msg);
}

void CSession::OnAuxMsg2(const std::wstring &_uri, const std::wstring &_schema, const BOOL& bFile)
{
	EventObject evt(new DEBUG_NEW_PLACEMENT CEvent(_uri, _schema, DocTime(), bFile));
	EmitEvent(evt);
}


void CSession::Exit()
{
	m_queue.clear();

	EventObject pEvt(new DEBUG_NEW_PLACEMENT CEvent());
	m_EventDispatcher->PushBack(pEvt);
	PostThreadMsg(tmQuit);
}


void CSession::EmitEvent(EventObject evt)
{
	m_EventDispatcher->PushBack(evt);
	PostThreadMsg(tmEvent);
}

static std::wstring ThreadMsgToString(const int& _msg)
{
	switch (_msg)
	{
	case WM_USER:
		return L"Set Event";
	case WM_QUIT:
		return L"Quit";
	}

	return L"Unknown thread message";
}

void CSession::OnThreadMsg(eThreadMsg msg, void* pParam)
{
	switch (msg)
	{
	case tmEvent:
		ProcessEvents();
		break;
	}
}

void CSession::ThreadProc()
{
	// A message received
	MSG msg = { 0 };

	::CoInitialize(NULL);

	m_bThreadStarted = true;

	m_ThreadEvt.Set();

	for (;;)
	{
		eThreadMsg& msg = m_queue.front();
		m_queue.pop();

		try
		{
			OnThreadMsg(msg, NULL);
		}
		catch (...)
		{
		}

		if (msg == tmQuit)
		{
			break;
		}
	}

}


void CSession::ProcessEvents()
{
	for (;;)
	{
		EventObject pEvt = m_EventDispatcher->GetFirstEvent();
		if (!pEvt)
		{
			return;
		}
		m_pLog->LogString(LEVEL_FINEST, L"Process Event, stack size: %i", m_EventDispatcher->GetQueueSize());
		ProcessEvent(pEvt);
	}
}


void CSession::ProcessEvent(EventObject pEvt)
{
	auto newDoc = std::make_shared<TDocParam>();
	int doc_size = 0;
	//singleton_auto_pointer<CDocLoader> loader;
	if (!m_DocLoader)
	{
		throw std::runtime_error("DocLoader unavailable");
	}
	m_DocLoader->AddDoc2Stack(pEvt->GetURI(), pEvt->GetSchema(), pEvt->IsFile(), newDoc);
}

void CSession::StartThread()
{
	m_thread.reset(
		new DEBUG_NEW_PLACEMENT boost::thread(boost::bind(&CSession::ThreadProc, this))
		);
}

void CSession::JoinThread()
{
	if (m_bThreadStarted)
	{
		m_thread->join();
	}
	m_bThreadStarted = false;
	m_thread.reset();
}