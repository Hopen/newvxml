// TestMTObj.h : Declaration of the CTestMTObj

#pragma once
#include "resource.h"       // main symbols



#include "CacheInterface_i.h"



using namespace ATL;

//template <typename ThreadingModel>
//class CAutoLocker {
//public:
//private:
//	ThreadingModel::AutoCriticalSection &m_cs;
//public:
//	CAutoLocker(ThreadingModel::AutoCriticalSection& cs) :m_cs(cs) { m_cs.Lock(); }
//	~CAutoLocker() { m_cs.Unlock(); }
//};

// CTestMTObj

class ATL_NO_VTABLE CTestMTObj :
	public CComObjectRootEx<CComMultiThreadModelNoCS>,
	public CComCoClass<CTestMTObj, &CLSID_TestMTObj>,
	public IDispatchImpl<ITestMTObj, &IID_ITestMTObj, &LIBID_CacheInterfaceLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CTestMTObj()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_TESTMTOBJ)


BEGIN_COM_MAP(CTestMTObj)
	COM_INTERFACE_ENTRY(ITestMTObj)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

private:
	CComMultiThreadModel::AutoCriticalSection m_cs;

public:



	STDMETHOD(WriteLog)(BSTR _source);
};

OBJECT_ENTRY_AUTO(__uuidof(TestMTObj), CTestMTObj)
