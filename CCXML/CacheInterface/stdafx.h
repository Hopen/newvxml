// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef STRICT
#define STRICT
#endif

#include "targetver.h"

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include "resource.h"
#include <atlbase.h>
#include <atlcom.h>
#include <atlctl.h>
#include <atlstr.h>
#include <comutil.h>
#include <ATLComTime.h>

#ifdef VLD_DEBUG
#include "..\Common\vld.h"
#endif
#include <windows.h>


//#ifdef _DEBUG
//#include <crtdbg.h>
//#define DEBUG_NEW_PLACEMENT (_NORMAL_BLOCK, __FILE__, __LINE__)
//#else
#define DEBUG_NEW_PLACEMENT
//#endif

using namespace ATL;
