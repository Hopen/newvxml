/************************************************************************/
/* Name     : CacheInterface\DocLoader.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 AUG 2013                                               */
/************************************************************************/
#pragma once
#include <map>
#include <unordered_map>
#include <memory>
#include <mutex>
#include "boost\scoped_ptr.hpp"
#include "boost\shared_array.hpp"
#include "EngineConfig.h"
#include "Patterns/singleton.h"
#include "factory_c.h"

extern HINSTANCE g_hInst;

class CSystemLog;
class CSession;

template<typename Function>
int64_t do_profiling(Function&& _func)
{
	using namespace std::chrono;
	auto start = high_resolution_clock::now();
	_func();
	return duration_cast<milliseconds>(high_resolution_clock::now() - start).count();
}


// some classes
class DocTime :public FILETIME
{
public:
	DocTime()
	{
		this->dwLowDateTime = 0;
		this->dwHighDateTime = 0;
	}
	DocTime(const FILETIME& rhs)
	{
		this->dwLowDateTime = rhs.dwLowDateTime;
		this->dwHighDateTime = rhs.dwHighDateTime;
	}
	bool operator==(const DocTime&rhs)
	{
		if (this->dwLowDateTime == rhs.dwLowDateTime &&
			this->dwHighDateTime == rhs.dwHighDateTime)
			return true;
		return false;
	}

	bool operator!=(const DocTime&rhs)
	{
		if (this->dwLowDateTime != rhs.dwLowDateTime ||
			this->dwHighDateTime != rhs.dwHighDateTime)
			return true;
		return false;
	}

};

struct TDocParam;
using DocParamPtr = std::shared_ptr<TDocParam>;

struct TDocParam
{
	//private:
	//typedef boost::shared_array<CHAR> DocArray;
public:
	DocTime tLastWrite;
	//DocArray pDoc;
	std::string pDoc;
	//size_t doc_size;

	int m_count;
	int m_doc_hash;

	TDocParam();
	//TDocParam(const DocTime&_time, std::string _doc, const int& _size);
};


class DocumentCollector
{
	using TCollector = std::unordered_map<std::wstring, DocParamPtr>;
public:

	DocParamPtr GetDocument(const std::wstring& aUri)
	{
		DocParamPtr result;

		std::lock_guard<std::mutex> lock(m_mutex);
		const auto cit = mCollector.find(aUri);
		if (cit != mCollector.cend())
		{
			result = cit->second;
		}
		return result;
	}

	size_t GetSize() const
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		return mCollector.size();
	}

	template <class TDoc>
	void Emplace(const std::wstring& aUri, TDoc&& aDoc)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		mCollector.emplace(std::make_pair(aUri, std::forward<TDoc>(aDoc)));
	}

	template <class TDoc>
	void Replace(const std::wstring& aUri, const TDoc& aDoc)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		//auto cit = mCollector.find(aUri);
		//if (cit != mCollector.cend())
		//{
		//	mCollector.erase(cit);
		//}
		//mCollector.emplace(std::make_pair(aUri, std::forward<TDoc>(aDoc)));

		auto result = mCollector.emplace(std::make_pair(aUri, aDoc));
		if (!result.second)
		{
			result.first->second = aDoc;
		}
	}

private:
	TCollector mCollector;
	mutable std::mutex m_mutex;
};

class CDocLoader : public static_singleton<CDocLoader>
{
	friend class static_singleton<CDocLoader>;

public:
	CDocLoader();
	virtual ~CDocLoader();

	// functions
	HRESULT GetDocument(BSTR sURI, BSTR sSchemaFile, BSTR bstrScriptID, BOOL bFile, SAFEARRAY **pSA);
	HRESULT ParseT2SMessage(BSTR sSrc, SAFEARRAY **pSA);

public:

private:

	//class CIsStringToRemove
	//{
	//public:
	//	CIsStringToRemove(const std::wstring& _removeStr)
	//	{
	//		m_removeString = _removeStr;
	//	}

	//	bool operator()(std::pair<std::wstring, DocParamPtr> cit)
	//	{
	//		return !cit.first.compare(m_removeString);
	//	}

	//private:
	//	std::wstring m_removeString;
	//};

private:
	// vars
	DocumentCollector m_stack;

	using SessionObj = std::shared_ptr<CSession>;
	SessionObj  m_pSes;
	CSystemLog* m_pLog;

public:

	void AddDoc2Stack(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile, DocParamPtr pDoc/*, int &doc_size*/);
	int LoadAndSerilizeDocument(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile, DocParamPtr pDoc/*, int &doc_size*/);
	int LoadAndSerilizeMrspMsg(const std::wstring& _uri, DocParamPtr pDoc);
	int DeserilizeDocument(SAFEARRAY* pSA, Factory_c::CollectorPtr& outDoc);

	void Init(CSystemLog *log);
	void Clear();
};

/******************************* eof *************************************/