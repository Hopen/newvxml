// ResourceCache.cpp : Implementation of CResourceCache

#include "stdafx.h"
#include "ResourceCache.h"
//#include "CacheInterface.h"
#include "DocLoader.h"


// CResourceCache
HRESULT CResourceCache::FinalConstruct()
{
	//m_Interface.reset(new )
	return S_OK;
}


STDMETHODIMP CResourceCache::GetDocument(BSTR sURI, BSTR sSchemaFile, BSTR bstrURI, BOOL bFile, SAFEARRAY **pSA)
{
	//return CCacheInterfaceModule::GetDocument(sURI,sSchemaFile,bFile,pSA);
	//return IResourceCache::GetDocument(sURI,sSchemaFile,bFile,pSA);
	//return this->GetDocument(sURI,sSchemaFile,bFile,pSA);
	singleton_auto_pointer<CDocLoader>			loader;
	return loader->GetDocument(sURI, sSchemaFile, bstrURI, bFile, pSA);
}

STDMETHODIMP CResourceCache::ParseT2SMessage(BSTR bstrSrc, SAFEARRAY** pSA)
{
	// TODO: Add your implementation code here
	singleton_auto_pointer<CDocLoader>			loader;
	return loader->ParseT2SMessage(bstrSrc, pSA);
}