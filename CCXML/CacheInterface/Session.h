/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once
#include <algorithm>
//#include <string>
//#include <mutex>
#include "sv_thread.h"
#include "sv_handle.h"
#include "sv_sysobj.h"
#include "..\Common\factory_c.h"
#include "..\Common\locked_queue.h"
//#include "..\Engine\EngineLog.h"
#include "Patterns/singleton.h"
#include "DocLoader.h"

using ObjectId = __int64;

#include "eventdispatcher.h"


class CSystemLog;

class CSession
{
private:
	enum eThreadMsg
	{
		tmEvent = WM_USER,
		tmQuit = WM_QUIT
	};


private:
	class CEvent
	{
	public:
		CEvent() {}
		CEvent(const std::wstring& _uri,
			const std::wstring& _schema,
			const DocTime _time,
			const bool& _bFail) : sURI(_uri), sSchema(_schema), tLastChangeFileTime(_time), bFile(_bFail)
		{
		}
		CEvent(const CEvent& rhs)
		{
			sURI = rhs.sURI;
			sSchema = rhs.sSchema;
			tLastChangeFileTime = rhs.tLastChangeFileTime;
			bFile = rhs.bFile;
		}
	public:
		std::wstring GetURI()const { return sURI; }
		std::wstring GetSchema()const { return sSchema; }
		DocTime      GetLastFileWriteTime()const { return tLastChangeFileTime; }
		BOOL         IsFile()const { return bFile; }
	private:
		DocTime       tLastChangeFileTime;
		std::wstring  sURI;
		std::wstring  sSchema;
		BOOL          bFile;
	};

	using EventObject = std::shared_ptr<CEvent>;
	class EmptyBaseLockGuard
	{
	public:
		EmptyBaseLockGuard(const std::wstring&) {}
	};

	using EventDispatcherPtr = std::unique_ptr<common::CEventDispatcher<EventObject, EmptyBaseLockGuard>>;

private:
	typedef std::list<EventObject> EventQueue;

	std::shared_ptr < boost::thread >  m_thread;
	locked_queue      < eThreadMsg    >  m_queue;


private:
	//EventQueue				m_EvtQ;
	EventDispatcherPtr		m_EventDispatcher;
	//ATL::CComAutoCriticalSection m_CS;
	SVSysObj::CEvent		m_ThreadEvt;
	bool m_bThreadStarted;

	CDocLoader* m_DocLoader;
	CSystemLog* m_pLog;

private:
	void PostThreadMsg(eThreadMsg msg, void* pParam = NULL);
	void ProcessEvent(EventObject pEvt);
	void ProcessEvents();
	void EmitEvent(EventObject evt);

public:
	void OnAuxMsg2(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile/*, CallbackFunc pCallBack*/);
	void Exit();


private:
	void OnThreadMsg(eThreadMsg	msg, void* pParam);

	void StartThread();
	void JoinThread();


protected:
	void ThreadProc();

public:
	CSession(
		CDocLoader *aDocLoader,
		CSystemLog* aLog);
	~CSession();

	void Start();
};