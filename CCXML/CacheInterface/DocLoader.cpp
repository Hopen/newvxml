/************************************************************************/
/* Name     : CacheInterface\DocLoader.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 AUG 2013                                               */
/************************************************************************/

#include "stdafx.h"
#include "DocLoader.h"
#include "..\Common\Utils.h"
#include "DocParser.h"
//#include <boost\archive\xml_woarchive.hpp>
//#include <boost\archive\binary_oarchive.hpp>
//#include <boost\archive\binary_iarchive.hpp>
#include <boost\functional\hash.hpp>
#include "BinaryFile.h"
#include "Log/SystemLog.h"
#include "Session.h"
#include "mrcpparser.h"

#include "..\Engine\EngineLog.h"

TDocParam::TDocParam() : /*doc_size(0), */m_doc_hash(0)
{
}

//TDocParam::TDocParam(const DocTime&_time, DocArray _doc, const int& _size) : tLastWrite(_time), pDoc(_doc), doc_size(_size)
//{
//}

CDocLoader::CDocLoader()
{
}

void CDocLoader::Init(CSystemLog *log)
{
	m_pLog = log;
	m_pSes = std::make_shared<CSession>(this, m_pLog);
	m_pSes->Start();
}

CDocLoader::~CDocLoader()
{
	if (m_pSes)
		m_pSes->Exit();
	m_pSes.reset();
}


HRESULT CDocLoader::GetDocument(BSTR sURI, BSTR sSchemaFile, BSTR bstrScriptID, BOOL bFile, SAFEARRAY **pSA)
{
	try
	{
		bool isAlreadyExist = false;
		if (1)
		{
			m_pLog->LogString(LEVEL_INFO, L"[%s] Parse document: %s, Schema: %s, File: %s", bstrScriptID, sURI, sSchemaFile, bFile ? L"Yes" : L"No");

			auto pDocument = m_stack.GetDocument(sURI);
			if (pDocument)
			{
				isAlreadyExist = true;
				m_pLog->LogString(LEVEL_INFO, L"[%s] Load document from cache, file_size: \"%i\", document in stack:\"%i\"", bstrScriptID, pDocument->pDoc.size(), m_stack.GetSize());
			}
			else
			{
				pDocument = std::make_shared<TDocParam>();
				// load new document
				WIN32_FIND_DATA fileData;

				if (bFile && Utils::GetFile(sURI, &fileData))
				{
					m_pLog->LogString(LEVEL_WARNING, L"[%s] Can't get file attributes, check file location: %s", bstrScriptID, sURI);
					return E_FAIL;
				}

				pDocument->tLastWrite = fileData.ftLastWriteTime;

				try
				{
					int ret = LoadAndSerilizeDocument(sURI, sSchemaFile, bFile, pDocument/*->pDoc*//*, doc_size*/);
					if (1 == ret)
					{
						m_pLog->LogString(LEVEL_WARNING, L"[%s] Failed to serialize: %s", bstrScriptID, std::wstring(sURI).c_str());
						return E_FAIL;
					}
					else if (-1 == ret)
					{
						m_pLog->LogString(LEVEL_WARNING, L"SEH EXCEPTION [%s] Failed to serialize: %s", bstrScriptID, std::wstring(sURI).c_str());
						return E_FAIL;
					}
					else if (pDocument->pDoc.empty())
					{
						m_pLog->LogString(LEVEL_WARNING, L"Failed to load: %s", std::wstring(sURI).c_str());
					}
					else
						m_pLog->LogString(LEVEL_INFO, L"[%s] Serialize done: %s, hash: %08X", bstrScriptID, std::wstring(sURI).c_str(), pDocument->m_doc_hash);
				}
				catch (std::exception& e)
				{
					std::wstring reason = _bstr_t(e.what());
					m_pLog->LogString(LEVEL_WARNING, L"[%s] Exception load document: %s", bstrScriptID, reason.c_str());
					return E_FAIL;
				}
				catch (std::wstring& e)
				{
					std::wstring reason = e;
					m_pLog->LogString(LEVEL_WARNING, L"[%s] Exception load document: %s", bstrScriptID, reason.c_str());
					return E_FAIL;
				}
				catch (_com_error& e)
				{
					std::wstring reason = (e.Error() != E_POINTER) ? (LPCWSTR)e.Description() : L"unknown";
					m_pLog->LogString(LEVEL_WARNING, L"[%s] Exception load document: %s", bstrScriptID, reason.c_str());
					return E_FAIL;
				}
				catch (...)
				{
					std::wstring reason = L"Unknown unhandled exception";
					m_pLog->LogString(LEVEL_WARNING, L"[%s] Exception load document: %s", bstrScriptID, reason.c_str());
					return E_FAIL;
				}

				m_stack.Emplace(sURI, pDocument);
			}

			if (pDocument->pDoc.empty())
			{
				m_pLog->LogString(LEVEL_WARNING, L"[%s] Internal error", bstrScriptID);
				return E_FAIL;

			}

			if (isAlreadyExist)
			{
				// Check for changes
				m_pSes->OnAuxMsg2(sURI, sSchemaFile, bFile);
			}

			HRESULT hr = S_OK;
			*pSA = ::SafeArrayCreateVector(VT_I2, 0, pDocument->pDoc.size());
			if (*pSA != NULL)
			{
				BYTE* psadata = NULL;
				hr = ::SafeArrayAccessData(*pSA, (void**)&psadata);
				if (SUCCEEDED(hr))
				{
					memcpy(psadata, pDocument->pDoc.c_str(), pDocument->pDoc.size());
					hr = ::SafeArrayUnaccessData(*pSA);
				}
			}
		}

		return S_OK;
	}
	catch (...)
	{
		m_pLog->LogString(LEVEL_WARNING, L"[%s] Exception at GetDocument, file location: %s", bstrScriptID, sURI);
	}

	return E_FAIL;
}

int CDocLoader::DeserilizeDocument(SAFEARRAY* pSA, Factory_c::CollectorPtr& outDoc)
{
	// get back archive
	char *c1;
	// Get data pointer
	HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

	int image_size = pSA->rgsabound->cElements;
	std::string in_data;
	in_data.append(c1, image_size);

	::SafeArrayUnaccessData(pSA);
	::SafeArrayDestroy(pSA);

	std::istringstream is(in_data);

	{
		cereal::BinaryInputArchive iarchive(is);
		iarchive(outDoc);
	}

	return 0;
}

void CDocLoader::AddDoc2Stack(
	const std::wstring& _uri,
	const std::wstring& _schema,
	const BOOL& bFile,
	DocParamPtr pDocument)
{
	try
	{
		if (!pDocument.get())
			return;

		if (!bFile)
			return;

		WIN32_FIND_DATA fileData;
		if (Utils::GetFile(_uri, &fileData))
		{
			m_pLog->LogString(LEVEL_WARNING, L"Can't get file attributes, check file location: %s", _uri.c_str());
			return;
		}
		pDocument->tLastWrite = fileData.ftLastWriteTime;

		auto currentDoc = m_stack.GetDocument(_uri);
		if (!currentDoc || pDocument->tLastWrite == currentDoc->tLastWrite)
		{
			return;
		}

		//if (1)
		//{
		//	DocumentCollector::const_iterator cit = m_stack.find(_uri);
		//	if (cit == m_stack.end() || cit != m_stack.end() &&
		//		pDocument->tLastWrite == cit->second->tLastWrite)
		//	{
		//		return;
		//	}
		//}

		try
		{
			int ret = LoadAndSerilizeDocument(_uri, _schema, bFile, pDocument/*, doc_size*/);
			if (1 == ret)// && (!pDocument->pDoc.get() || !pDocument->doc_size))
			{
				m_pLog->LogString(LEVEL_WARNING, L"Failed to serialize: %s", _uri.c_str());
				return;
			}
			else if (-1 == ret)// && (!pDocument->pDoc.get() || !pDocument->doc_size))
			{
				m_pLog->LogString(LEVEL_WARNING, L"SEH EXCEPTION - Failed to serialize: %s", _uri.c_str());
				return;
			}
			else if (!pDocument->pDoc.size())
			{
				m_pLog->LogString(LEVEL_WARNING, L"Failed to load: %s", _uri.c_str());
			}
		}
		catch (std::exception& e)
		{
			//CEngineLog			log;
			//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

			std::wstring reason = _bstr_t(e.what());
			m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
			return;
		}
		catch (std::wstring& e)
		{
			//CEngineLog			log;
			//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

			std::wstring reason = e;
			m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
			return;
		}
		catch (_com_error& e)
		{
			//CEngineLog			log;
			//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

			std::wstring reason = (e.Error() != E_POINTER) ? (LPCWSTR)e.Description() : L"unknown";
			m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
			return;
		}
		catch (...)
		{
			//CEngineLog			log;
			//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

			std::wstring reason = L"Unknown unhandled exception";
			m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
			return;
		}

		m_pLog->LogString(LEVEL_INFO, L"Serialization complete: %s, hash: %08X", _uri.c_str(), pDocument->m_doc_hash);

		m_stack.Replace(_uri, pDocument);
		//if (1)
		//{
		//	DocumentCollector tempStack;
		//	std::remove_copy_if(m_stack.begin(),m_stack.end(), std::inserter(tempStack, tempStack.end()), CIsStringToRemove(_uri));
		//	m_stack.swap(tempStack);
		//}

		//// add new
		//m_stack[_uri] = pDocument;// DocParamPtr(new DEBUG_NEW_PLACEMENT TDocParam(tNewFileTime, pDoc, doc_size));
	}
	catch (...)
	{
		/*m_pLog->*/m_pLog->LogString(LEVEL_WARNING, L"Exception at AddDoc2Stack, file location: %s", _uri.c_str());
	}

}

int CDocLoader::LoadAndSerilizeDocument(
	const std::wstring& _uri,
	const std::wstring& _schema,
	const BOOL& bFile,
	DocParamPtr pDocument)
{
	if (!pDocument)
		return 1;

	CDocParser doc(m_pLog);

	int bLoadSuccessful = -1;
	int64_t time_msk = do_profiling([&]()
	{
		bLoadSuccessful = doc.Parse2(_uri, _schema, bFile);
	});
	m_pLog->LogString(LEVEL_INFO, L"SAXX time: %llu ms", time_msk);

	if (!bLoadSuccessful)
	{
		std::ostringstream os;
		time_msk = do_profiling([&]()
		{
			cereal::BinaryOutputArchive oa(os);
			oa(doc.Get());
		});
		m_pLog->LogString(LEVEL_INFO, L"CEREAL time: %i ms", time_msk);

		pDocument->pDoc.clear();
		pDocument->pDoc = os.str();
		boost::hash<std::string> doc_hash;
		pDocument->m_doc_hash = doc_hash(pDocument->pDoc);
	}

	return bLoadSuccessful;
}

HRESULT CDocLoader::ParseT2SMessage(BSTR sSrc, SAFEARRAY **pSA)
{
	if (!pSA /*|| !m_pLog*/)
		return E_POINTER;


	m_pLog->LogString(LEVEL_INFO, L"Parse mrcp message: %s", sSrc);

	DocParamPtr pDocument = std::make_shared<TDocParam>();

	CAtlString shortSrc = CAtlString(sSrc).Left(30);
	shortSrc += "...";

	try
	{
		int ret = LoadAndSerilizeMrspMsg(sSrc, pDocument);

		if (1 == ret)
		{
			m_pLog->LogString(LEVEL_WARNING, L"Failed to serialize: %s", shortSrc);
			return E_FAIL;
		}
		else if (-1 == ret)// || !(pDocument->pDoc) || !pDocument->doc_size)
		{
			m_pLog->LogString(LEVEL_WARNING, L"SEH EXCEPTION Failed to serialize: %s", shortSrc);
			return E_FAIL;
		}
		else if (!pDocument || pDocument->pDoc.empty())
		{
			m_pLog->LogString(LEVEL_WARNING, L"Failed to load: %s", shortSrc);
			return E_FAIL;
		}
		else
			m_pLog->LogString(LEVEL_INFO, L"Serialize done: %s, hash: %08X", shortSrc, pDocument->m_doc_hash);
	}
	catch (std::exception& e)
	{
		//CEngineLog			log;
		//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

		std::wstring reason = _bstr_t(e.what());
		m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
		return E_FAIL;
	}
	catch (std::wstring& e)
	{
		//CEngineLog			log;
		//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

		std::wstring reason = e;
		m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
		return E_FAIL;
	}
	catch (_com_error& e)
	{
		//CEngineLog			log;
		//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

		std::wstring reason = (e.Error() != E_POINTER) ? (LPCWSTR)e.Description() : L"unknown";
		m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
		return E_FAIL;
	}
	catch (...)
	{
		//CEngineLog			log;
		//log.Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

		std::wstring reason = L"Unknown unhandled exception";
		m_pLog->LogString(LEVEL_WARNING, L"Exception load document: %s", reason.c_str());
		return E_FAIL;
	}
	// wrap archive to SAFEARRAY 
	HRESULT hr = S_OK;
	*pSA = ::SafeArrayCreateVector(VT_I2, 0, pDocument->pDoc.size());
	if (*pSA != NULL)
	{
		BYTE* psadata = NULL;
		hr = ::SafeArrayAccessData(*pSA, (void**)&psadata);
		if (SUCCEEDED(hr))
		{
			memcpy(psadata, pDocument->pDoc.c_str(), pDocument->pDoc.size());
			hr = ::SafeArrayUnaccessData(*pSA);
		}
	}
	return S_OK;
}

int CDocLoader::LoadAndSerilizeMrspMsg(const std::wstring& _uri, DocParamPtr pDocument)
{

	int bLoadSuccessful = 1; //means false

	if (!pDocument)
		return bLoadSuccessful;

	CMrcpParser parser;
	int64_t time_msk = do_profiling([&]()
	{
		bLoadSuccessful = parser.Parse2(_uri);
	});
	m_pLog->LogString(LEVEL_INFO, L"SAXX time: %llu ms", time_msk);

	if (!bLoadSuccessful)
	{
		std::ostringstream os;
		time_msk = do_profiling([&]()
		{
			cereal::BinaryOutputArchive oa(os);
			oa(parser.Get());
		});
		m_pLog->LogString(LEVEL_INFO, L"CEREAL time: %i ms", time_msk);

		pDocument->pDoc.clear();
		pDocument->pDoc = os.str();
		boost::hash<std::string> doc_hash;
		pDocument->m_doc_hash = doc_hash(pDocument->pDoc);
	}


	//if (!bLoadSuccessful) // OK
	//{
	//	std::ostringstream os;
	//	{
	//		boost::archive::binary_oarchive oa(os);
	//		const Factory::CMrcpFactory &rDoc = parser.Get();
	//		oa << rDoc;
	//	}
	//	std::string  outbound_data_ = os.str();
	//	pDocument->doc_size = outbound_data_.size();

	//	pDocument->pDoc.reset(new DEBUG_NEW_PLACEMENT CHAR[pDocument->doc_size]);
	//	ZeroMemory(pDocument->pDoc.get(), pDocument->doc_size);
	//	outbound_data_._Copy_s(pDocument->pDoc.get(), pDocument->doc_size, pDocument->doc_size);
	//	//TDocParam::DocHash doc_hash;
	//	boost::hash<std::string> doc_hash;
	//	pDocument->m_doc_hash = doc_hash(outbound_data_);

	//}
	return bLoadSuccessful;
}

void CDocLoader::Clear()
{
	if (m_pSes)
		m_pSes->Exit();
	Sleep(1000);
	m_pSes.reset();
}

/******************************* eof *************************************/