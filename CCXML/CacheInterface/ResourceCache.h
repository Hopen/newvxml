// ResourceCache.h : Declaration of the CResourceCache

#pragma once
#include "resource.h"       // main symbols

#include "CacheInterface_i.h"
//#include "CacheInterface.h"
//#include <map>

//#include "sv_strutils.h"
//#include "sv_utils.h"

// CResourceCache

class ATL_NO_VTABLE CResourceCache :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CResourceCache, &CLSID_ResourceCache>,
	public IDispatchImpl<IResourceCache, &IID_IResourceCache, &LIBID_CacheInterfaceLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:

DECLARE_REGISTRY_RESOURCEID(IDR_RESOURCECACHE)
DECLARE_CLASSFACTORY_SINGLETON(CResourceCache)

BEGIN_COM_MAP(CResourceCache)
	COM_INTERFACE_ENTRY(IResourceCache)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();

	void FinalRelease()
	{
	}

public:

	STDMETHOD(GetDocument)(BSTR bstrURI, BSTR sSchemaFile, BSTR sScriptID, BOOL bFile, SAFEARRAY **pSA);
	STDMETHOD(ParseT2SMessage)(BSTR bstrSrc, SAFEARRAY** pSA);
};

OBJECT_ENTRY_AUTO(__uuidof(ResourceCache), CResourceCache)
