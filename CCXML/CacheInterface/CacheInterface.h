/************************************************************************/
/* Name     : CacheInterface\CacheInterface.h                           */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 27 Jan 2010                                               */
/************************************************************************/
#pragma once
#include <map>
#include "boost\scoped_ptr.hpp"
#include "boost\shared_ptr.hpp"
#include "boost\thread.hpp"
#include <vector>

//extern HINSTANCE g_hInst;
//
//class CCacheEngineConfig : public CEngineConfig<CCacheEngineConfig>
//{
//protected:
//	void LoadEngineConfig()
//	{
//		try
//		{ 
//		} 
//		catch (...) 
//		{ 
//		} 
//	}
//
//public:
//	CCacheEngineConfig()
//	{
//		try
//		{
//			LoadEngineConfig();
//		}
//		catch (...)
//		{
//		}
//	}
//	~CCacheEngineConfig()
//	{
//	}
//};



//class CResourceCache;
class CCacheInterfaceModule : public CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_CacheInterfaceLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CACHEINTERFACE, "{8696DB64-7D54-4CED-87E9-80143509CEA1}")
	HRESULT InitializeSecurity() throw()
	{
		// TODO : Call CoInitializeSecurity and provide the appropriate security settings for 
		// your service
		// Suggested - PKT Level Authentication, 
		// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
		// and an appropiate Non NULL Security Descriptor.
		CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_NONE,
			RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);

		return S_OK;
	}

	HRESULT Run(int nShowCmd /* = SW_HIDE */)throw();

	// ctor
	CCacheInterfaceModule()
	{
	}

	virtual ~CCacheInterfaceModule()
	{
		//DocumentCollector::iterator it = m_stack.begin();
		//while(it!=m_stack.end())
		//{
		//	//TDocParam* pDoc = ;
		//	delete it->second;
		//	++it;
		//}
		//m_stack.clear();
	}

	//// overrides
	//HRESULT PreMessageLoop (int nShowCmd);
	//void RunMessageLoop (void) throw();
	//void OnContinue (void) throw();
	//void OnPause (void) throw();
	//void OnShutdown (void) throw();
	//void OnStop (void) throw();

//	// functions
//	/*static*/ HRESULT GetDocument(BSTR sURI, BSTR sSchemaFile, BOOL bFile, SAFEARRAY **pSA);
//
//	// interface
//	friend class CResourceCache;
//
//private:
//
//	//CEngineLog			m_Log;
//	struct TDocParam
//	{
//		DocTime tLastWrite;
//		CHAR* pDoc;
//		int doc_size;
//		TDocParam():pDoc(NULL),doc_size(0){}
//		TDocParam(const DocTime&_time, char* _doc, const int& _size):tLastWrite(_time),pDoc(_doc),doc_size(_size)
//		{
//		}
//		~TDocParam()
//		{
//			delete [] pDoc;
//		}
//	};
//
//	class CIsStringToRemove
//	{
//	public:
//		CIsStringToRemove(const std::wstring& _removeStr)
//		{
//			m_removeString = _removeStr;
//		}
//
//		bool operator()(std::pair<std::wstring, TDocParam*> cit)
//		{
//			return !cit.first.compare(m_removeString);
//		}
//
//	private:
//		std::wstring m_removeString;
//	};

	//// vars
	///*static */ATL::CComAutoCriticalSection m_CS2;

	//typedef std::map<std::wstring, TDocParam*> DocumentCollector;
	///*static */DocumentCollector m_stack;

	///*static */CSession *m_pSes;

public:
	void SendTestEvent();
	//void initTests();
	//void ThreadProc(int num_proc);

//private:
	//std::vector<std::wstring> m_test_files;

	//typedef boost::shared_ptr < boost::thread > ThreadPtr;
	//std::vector<ThreadPtr> m_threads;
	

	///*static*/ void AddDoc2Stack(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile, char** pDoc, int &doc_size);
	//static bool LoadAndSerilizeDocument(const std::wstring& _uri, const std::wstring& _schema, const BOOL& bFile, char** pDoc, int &doc_size);
};

/******************************* eof *************************************/