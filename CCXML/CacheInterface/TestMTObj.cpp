// TestMTObj.cpp : Implementation of CTestMTObj

#include "stdafx.h"
#include "TestMTObj.h"
#include "Log\SystemLog.h"

// CTestMTObj



STDMETHODIMP CTestMTObj::WriteLog(BSTR _source)
{
	// TODO: Add your implementation code here
	//CAutoLocker<CComMultiThreadModel> lock(m_cs);

	singleton_auto_pointer<CSystemLog> pLog;
	pLog->LogString(LEVEL_FINEST, L"CTestMTObj::WriteLog: \"%s\"", _source);

	return S_OK;
}
