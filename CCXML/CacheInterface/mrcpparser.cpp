/************************************************************************/
/* Name     : CacheInterface\mrcpparser.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 23 Dec 2014                                               */
/************************************************************************/
#include "stdafx.h"
#include "mrcpparser.h"
#include "DocLoader.h"
#include "Log/SystemLog.h"

//using namespace enginelog;

CMrcpParser::CMrcpParser() :m_contentHandler(new DEBUG_NEW_PLACEMENT CSAXContentHandler()), m_errorHandler(new DEBUG_NEW_PLACEMENT CSAXErrorHandler()) 
{
	m_xmlParser.reset(new DEBUG_NEW_PLACEMENT CSAXXmlParser());
	m_mrcpDoc.reset(new DEBUG_NEW_PLACEMENT Factory_c::CMrcpFactory());

	//m_pLog.reset(new DEBUG_NEW_PLACEMENT CEngineLog());
	//m_pLog->Init(CCacheEngineConfig::GetInstance()->sLevel.c_str(), CCacheEngineConfig::GetInstance()->sLogFileName.c_str());

	m_CurNodeName = L"";
}

CMrcpParser::~CMrcpParser()
{
}

int CMrcpParser::Parse2(const std::wstring& _xml)
{
	int b = 0; // OK
	__try
	{
		b = Parse(_xml) ? 0 : 1;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		b = -1;
	}

	return b;

}

bool CMrcpParser::Parse(const std::wstring& _xml)
{
	// Set the content handler.
	if (m_xmlParser->PutContentHandler(m_contentHandler))
	{
		m_contentHandler->AttachElementHandler(this);
	}

	if (m_xmlParser->PutErrorHandler(m_errorHandler))
	{
		m_errorHandler->AttachElementHandler(this);
	}
	return m_xmlParser->Parse(_xml);
}

void CMrcpParser::OnXmlStartElement(const ISATXMLElement& xmlElement)
{
	m_CurNodeName = xmlElement.GetName();
	if (m_CurNodeName == L"input")
	{
		std::wstring sConfidence;
		if (xmlElement.GetAttrVal(L"confidence", sConfidence))
		{
			m_mrcpDoc->SetConfidence(sConfidence);
		}
	}

}

void CMrcpParser::OnXmlElementData(const wchar_t* aData, size_t aSize)
{
	std::wstring elementData(aData, aData + aSize);

	if (m_CurNodeName == L"input")
	{
		m_mrcpDoc->SetInput(elementData);
	}
	else if (m_CurNodeName == L"SWI_meaning")
	{
		m_mrcpDoc->SetSWImeaning(elementData);
	}
}

void CMrcpParser::OnXmlEndElement(const ISATXMLElement& xmlElement)
{
}

void CMrcpParser::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
{
	/*CCacheInterfaceModule::*/m_pLog->LogString(LEVEL_WARNING,  L"Error parsing MRCP answer. Line %d, char %d, reason \"%s\"",
		line, column, errorText.c_str());
}

bool CMrcpParser::OnXmlAbortParse(const ISATXMLElement& xmlElement)
{
	return false;
}

//const Factory_c::CCollector& CDocParser::Get()const
//{
//	// skip VXML_base element
//	Factory_c::CCollector::TagContainer::const_iterator cit = m_current->GetChildren()->begin();
//	return *cit;
//
//}

const Factory_c::CMrcpFactory& CMrcpParser::Get()const
{
	return *m_mrcpDoc.get();
}

//
/******************************* eof *************************************/