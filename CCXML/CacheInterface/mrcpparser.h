/************************************************************************/
/* Name     : CacheInterface\mrcpparser.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 23 Dec 2014                                               */
/************************************************************************/
#pragma once
#include "SAXXmlElementHandler.h"
#include "SAXXmlParser.h"
#include "factory_c.h"
#include <boost/shared_ptr.hpp>
#include "Patterns/singleton.h"

//class CEngineLog;
class CSystemLog;
class CMrcpParser : public ISAXXmlElementHandler
{
public:
	CMrcpParser();
	~CMrcpParser();

	bool Parse(const std::wstring& _xml);
	int Parse2(const std::wstring& _xml);

	// Handle XML content events during parsing.
	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
	//virtual void OnXmlElementData(const std::wstring& elementData, int depth);
	virtual void OnXmlElementData(const wchar_t* aData, size_t aSize);
	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);

	// Handle XML error events during parsing.
	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);

	// Return true to stop parsing earlier.
	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);

	//const Factory_c::CCollector& Get()const;
	const Factory_c::CMrcpFactory& Get()const;

private:
	//boost::shared_ptr< Factory_c::CCollector > m_doc;
	boost::shared_ptr < Factory_c::CMrcpFactory> m_mrcpDoc;
	boost::shared_ptr< CSAXXmlParser       > m_xmlParser;
	//Factory_c::CCollector * m_current;
	//boost::shared_ptr<CEngineLog>  m_pLog;
	singleton_auto_pointer<CSystemLog> m_pLog;
	//Factory_c::CCollector * m_parent;
	std::wstring m_CurNodeName;

	CComPtr<CSAXContentHandler> m_contentHandler;
	CComPtr<CSAXErrorHandler  > m_errorHandler;

};
/******************************* eof *************************************/