#pragma	   once
//#include <windows.h>
//#import "msxml3.dll" no_namespace, raw_interfaces_only, raw_native_types, named_guids
#include <ole2.h>
#include <msxml2.h>
#include <objsafe.h>
#include <objbase.h>
#include <crtdbg.h>
#include <atlbase.h>
#include <comutil.h>

#pragma warning( push )
#pragma warning( disable: 4018 4786)
#include <string>
#include <exception>
#pragma warning( pop )

#ifndef __BORLANDC__
#   pragma comment (lib, "comsupp.lib")
#   pragma comment (lib, "msxml2.lib")
#endif

namespace LiteXML
{
using namespace std;

inline CComPtr<IXMLDOMDocument> CreateXMLDocument() throw( std::wstring )
{
	CComPtr<IXMLDOMDocument>	pXML;
	HRESULT						hRes;
	WCHAR						wsErrDescr[4096];
	
	hRes = pXML.CoCreateInstance(CLSID_DOMDocument);
	if (FAILED(hRes) || pXML==NULL)
	{
		swprintf(wsErrDescr,L"CoCreateInstance(CLSID_DOMDocument) returned 0x%08X",hRes);
		throw std::wstring(wsErrDescr);
	}
	pXML->put_async(VARIANT_FALSE);
	return pXML;
}

inline CComPtr<IXMLDOMDocument> XMLFromText(std::wstring text) throw( std::wstring )
{
	CComPtr<IXMLDOMDocument>	pXML;
	HRESULT						hRes;
	VARIANT_BOOL				vb	= FALSE;
	WCHAR						wsErrDescr[4096];

	pXML = CreateXMLDocument();
	pXML->put_async(VARIANT_FALSE);
	hRes = pXML->loadXML(CComBSTR(text.c_str()),&vb);
	if (FAILED(hRes) || vb == FALSE)
	{
		CComPtr<IXMLDOMParseError>	pIParseError;

		CComBSTR					bsReason;
		CComBSTR					bsSrcText;
		LONG						lErrCode = -1;

		hRes = pXML->get_parseError(&pIParseError);
		if (FAILED(hRes) || pIParseError == NULL)
		{
			swprintf(wsErrDescr,L"pXML->get_parseError(&pIParseError) returned 0x%08X",hRes);
			throw std::wstring(wsErrDescr);
		}

		pIParseError->get_reason(&bsReason);
		pIParseError->get_srcText(&bsSrcText);
		pIParseError->get_errorCode(&lErrCode);
		swprintf(wsErrDescr,
			L"Parse error "
			L"\r\n\t Reason = %s"
			L"\r\n\t SrcText = %s"
			L"\r\n\t ErrCode = %i",
			bsReason.operator BSTR(),
			bsSrcText.operator BSTR(),
			lErrCode);
		throw std::wstring(wsErrDescr);
	}
	return pXML;		  
}


inline CComPtr<IXMLDOMDocument>	LoadXML(bstr_t bsFileName) throw( std::wstring )
{
	CComPtr<IXMLDOMDocument>	pXML;
	HRESULT						hRes;
	VARIANT_BOOL				vb	= FALSE;
	WCHAR						wsErrDescr[4096];

	pXML = CreateXMLDocument();
	pXML->put_async(VARIANT_FALSE);
	hRes = pXML->load(variant_t(bsFileName),&vb);
	if (FAILED(hRes) || vb == FALSE)
	{
		CComPtr<IXMLDOMParseError>	pIParseError;

		CComBSTR					bsURL;
		CComBSTR					bsReason;
		CComBSTR					bsSrcText;

		LONG						lErrCode = -1;
		LONG						lLine	 = -1;
		LONG						lLinePos = -1;
		LONG						lFilePos = -1;

		hRes = pXML->get_parseError(&pIParseError);
		if (FAILED(hRes) || pIParseError == NULL)
		{
			swprintf(wsErrDescr,L"pXML->get_parseError(&pIParseError) returned 0x%08X",hRes);
			throw std::wstring(wsErrDescr);
		}

		pIParseError->get_url(&bsURL);
		pIParseError->get_reason(&bsReason);
		pIParseError->get_srcText(&bsSrcText);
		pIParseError->get_errorCode(&lErrCode);
		pIParseError->get_line(&lLine);
		pIParseError->get_linepos(&lLinePos);
		pIParseError->get_filepos(&lFilePos);
		swprintf(wsErrDescr,
			L"Parse error "
			L"\r\n\t URL = %s"
			L"\r\n\t Reason = %s"
			L"\r\n\t SrcText = %s"
			L"\r\n\t ErrCode = %i"
			L"\r\n\t Line = %i"
			L"\r\n\t LinePos = %i"
			L"\r\n\t FilePos = %i",
			bsURL.operator BSTR(),
			bsReason.operator BSTR(),
			bsSrcText.operator BSTR(),
			lErrCode,
			lLine,
			lLinePos,
			lFilePos);
		throw std::wstring(wsErrDescr);
	}						  
	return pXML;		  
}
						  
						  

// TElem -- a simple class to wrap up IXMLDomElement and iterat its children.
//   name()    - in <item>stuff</item> it returns "item"
//   val()     - in <item>stuff</item> it returns "stuff"
//   attr(s)   - in <item s=L"hello">stuff</item> it returns "hello"
//   subnode(b)- in <item><a>hello</a><b>there</b></item> it returns the TElem <b>there</b>
//   subval(b) - in <item><a>hello</a><b>there</b></item> it returns "there"
//   for (TElem c=e.begin(); c!=e.end(); c++) {...} - iterators over the subnodes
class TElem
{
private:
	CComPtr<IXMLDOMElement>		m_Elem;
	CComPtr<IXMLDOMNodeList>	m_NodeList;
	int							m_iPos;
	long						m_NodeListSize;

	//
	TElem(int clen) : m_Elem(0), m_NodeList(0), m_iPos(-1), m_NodeListSize(clen) {}

	void get()
	{ 
		if (m_iPos!=-1)
		{
			m_Elem=0;
			CComPtr<IXMLDOMNode> inode;
			m_NodeList->get_item(m_iPos,&inode);
			if (!inode) return;
			DOMNodeType type;
			inode->get_nodeType(&type);
			if (type == NODE_ELEMENT)
			{
				CComQIPtr<IXMLDOMElement> e(inode);
				m_Elem=e;
			}
			else
				(*this)++;
		}
		m_NodeListSize=0;
		if (!m_Elem)	return;

		CComPtr<IXMLDOMNodeList> iNodeList = GetChildNodes();
		if (!iNodeList)	return;
		iNodeList->get_length(&m_NodeListSize);
	}

	CComPtr<IXMLDOMNodeList>	GetChildNodes() const
	{
		CComPtr<IXMLDOMNodeList> iNodeList;
		m_Elem->get_childNodes(&iNodeList);
		return iNodeList;
	}
public:
	TElem() : m_Elem(0), m_NodeList(0), m_iPos(-1), m_NodeListSize(0) {}
	TElem(CComPtr<IXMLDOMElement> elem) : m_Elem(elem), m_NodeList(0), m_iPos(-1), m_NodeListSize(0) {get();}
	TElem(CComPtr<IXMLDOMNodeList> nlist) : m_Elem(0), m_NodeList(nlist), m_iPos(0), m_NodeListSize(0) {get();}
	TElem(CComPtr<IXMLDOMDocument> doc)
	{
		if (!doc) *this = TElem();
		CComPtr<IXMLDOMElement>	elem;
		doc->get_documentElement(&elem);
		if (!elem) *this = TElem();
		*this = TElem(elem);
	}

	TElem(const TElem& other) { *this = other; }

	
	//
	wstring name() const
	{
		if (!m_Elem)
			return L"";
		CComBSTR bn;
		m_Elem->get_tagName(&bn);
		return wstring(bn);
	}

	wstring attr(const wstring name) const
	{
		if (!m_Elem)
			return L"";
		
		//now extends
		size_t iLastSlashPos = name.rfind('/');
		if (iLastSlashPos != -1)
		{
			wstring wsSubnode = name.substr(0,iLastSlashPos);
			wstring wsAttr	  = name.substr(iLastSlashPos+1);
			return this->subnode(wsSubnode).attr(wsAttr);
		}

		CComVariant val(VT_EMPTY);
		m_Elem->getAttribute(CComBSTR(name.c_str()),&val);
		if (val.vt==VT_BSTR) return val.bstrVal;
		return L"";
	}
	bool	attrBool(const wstring name,bool def) const
	{
		wstring a = attr(name);
		
		if (a==L"true" || a==L"TRUE") return true;
		else if (a==L"false" || a==L"FALSE") return false;
		else return def;
	}

	int		attrInt(const wstring name, int def) const
	{
		wstring a = attr(name);
		int i, res= swscanf(a.c_str(),L"%i",&i);
		return (res==1) ? i : def;
	}
	float	attrFloat(const wstring name, float def) const
	{
		wstring a = attr(name);
		float f;
		int	res = swscanf(a.c_str(),L"%f",&f);
		return (res==1) ? f : def;
	}

	wstring val() const
	{
		if (!m_Elem) return L"";
		CComVariant val(VT_EMPTY);
		m_Elem->get_nodeTypedValue(&val);
		if (val.vt==VT_BSTR) return val.bstrVal;
		return L"";
	}
	TElem	subnode(const wstring name) const
	{
		if (!m_Elem) return TElem();
		CComPtr<IXMLDOMNode> node;
		m_Elem->selectSingleNode(CComBSTR(name.c_str()),&node);
		if (!node)
			return TElem();
		CComQIPtr<IXMLDOMElement>	el = (IXMLDOMNode*)node;
		return TElem((IXMLDOMElement*)el);
	}
	wstring subval(const wstring name) const
	{
		if (!m_Elem) return L"";
		return subnode(name).val();
	}

	TElem	begin() const
	{
		if (!m_Elem) return TElem();
		return TElem( GetChildNodes() );
	}

	TElem	end() const
	{
		return TElem(m_NodeListSize);
	}

	TElem	operator++(int)
	{
		if (m_iPos!=-1) {m_iPos++; get();}
		return *this;
	}

	bool	operator!=(const TElem &e) const
	{
		if (this->m_Elem == e.m_Elem && e.m_Elem == 0 &&
			this->m_NodeList == e.m_NodeList && e.m_NodeList == 0)
			return false;
		return m_iPos!=e.m_NodeListSize;
	}
	bool	operator!()const
	{
		return !m_Elem;
	}

	
	wstring	get_xml()
	{
		if (!m_Elem) return L"";
		
		CComBSTR	bs;
		m_Elem->get_xml(&bs);
		return wstring(bs);
	}
	void	set_xml(wstring xml)
	{
		TElem					tel = XMLFromText(xml);
		CComPtr<IXMLDOMNode>	parent;
		CComPtr<IXMLDOMNode>	ret;
		
		m_Elem->get_parentNode(&parent);
		parent->replaceChild(tel.m_Elem,m_Elem,&ret);
		ret.Release();
		parent->selectSingleNode(CComBSTR(name().c_str()),&ret);
		m_Elem = CComQIPtr<IXMLDOMElement>(ret);
	}

	TElem	createSubnode(const wstring name)
	{
		if (!m_Elem) return TElem();		
		
		CComQIPtr<IXMLDOMElement>	el;
		CComQIPtr<IXMLDOMNode>		n;

		GetOwnerDocument()->createElement(CComBSTR(name.c_str()),&el);
		m_Elem->appendChild(el,&n);
		el = n;
		return TElem((IXMLDOMElement*)el);
	}

	void	createAttrVariant(const wstring name,CComVariant value)
	{
		if (!m_Elem)
			throw std::wstring(L"Null node, can't crate attribute");
		
		//now extends
		size_t iLastSlashPos = name.rfind('/');
		if (iLastSlashPos != -1)
		{
			wstring wsSubnode = name.substr(0,iLastSlashPos);
			wstring wsAttr	  = name.substr(iLastSlashPos+1);
			this->subnode(wsSubnode).createAttrVariant(wsAttr,value);
			return;
		}
		m_Elem->setAttribute(CComBSTR(name.c_str()),value);
	}
	void	createAttr(const wstring name,const wstring value)
	{
		createAttrVariant(name,CComVariant(value.c_str()));
	}
	void	createAttrBool(const wstring name,bool value)
	{
		createAttr(name,(value) ? L"true" : L"false");
	}

	void	createAttrInt(const wstring name, int value)
	{
		createAttrVariant(name,CComVariant(value));
	}
	void	remove()
	{
		if (!m_Elem)	return;
		CComPtr<IXMLDOMNode>	removedNode;
		CComPtr<IXMLDOMNode>	parent;
		m_Elem->get_parentNode(&parent);
		if (parent)
			parent->removeChild(m_Elem,&removedNode);
	}

	CComQIPtr<IXMLDOMDocument>	GetOwnerDocument()
	{
		CComQIPtr<IXMLDOMDocument>	doc;
		if (m_Elem)
			m_Elem->get_ownerDocument(&doc);
		return (IXMLDOMDocument*)doc;
	}

	CComPtr<IXMLDOMElement> GetElement()
	{
		return m_Elem;
	}
};

}//namespace

// XML config singleton
class CXmlConfigSingleton : public LiteXML::TElem
{
private:
	CXmlConfigSingleton() { };
	~CXmlConfigSingleton() { };

public:
	static void LoadFromFile(const std::wstring sConfigFile)
	{
		CXmlConfigSingleton::Instance() = LiteXML::TElem(LiteXML::LoadXML(sConfigFile.c_str()));
	}
	static LiteXML::TElem& Instance()
	{
		static CXmlConfigSingleton config;
		return config;
	}
};
